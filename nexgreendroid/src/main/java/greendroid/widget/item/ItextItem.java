package greendroid.widget.item;

import android.content.Context;
import android.view.ViewGroup;

import greendroid.widget.itemview.ItemView;

public interface ItextItem {
    public String getText();

    public void setText(String psText);

    public String getHeaderText();

    public void setHeaderText(String psHeaderText);

    public ItemView newView(Context context, ViewGroup parent);
}
