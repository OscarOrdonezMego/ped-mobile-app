package greendroid.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;

import com.cyrilmottier.android.greendroid.R;

import greendroid.graphics.drawable.ActionBarDrawable;

/**
 * Base class representing an {@link ActionBarItem} used in {@link ActionBarGD}s.
 * The base implementation exposes a single Drawable as well as a content
 * description.
 *
 * @author Cyril Mottier
 */
public abstract class ActionBarItem {

    public enum Type {
        GoHome, // A house
        Search, // A magnifying glass
        Talk, // A speech bubble
        Compose, // A sheet of paper with a pen
        Export, // A dot with an arrow
        Share, // A dot with two arrows
        Refresh, // Two curved arrows
        TakePhoto, // A camera
        // PickPhoto, // Two pictures with an arrow
        Locate, // The traditional GMaps pin
        Edit, // A pencil
        Add, // A plus sign
        Star, // A star
        SortBySize, // Some bars
        LocateMyself, // A surrounded dot
        Compass,
        Help,
        Info,
        Settings,
        List,
        Trashcan,
        Eye,
        AllFriends,
        Group,
        Gallery,
        Slideshow,
        Mail,
        SearchResult,
        Linea,
        SubFamilia,
        Familia,
        PedidoRapido,
        Nex_Chart,
        Nex_Exit,
        Nex_ArrowDown,
        Nex_ArrowUp,
        Nex_Back,
        Nex_Business,
        Nex_Cancel,
        Nex_Car,
        Nex_Categories,
        Nex_Clock,
        Nex_Copy,
        Nex_Cut,
        Nex_Database,
        Nex_Dialog,
        Nex_Disc,
        Nex_Flag,
        Nex_Flash,
        Nex_Globe,
        Nex_Laptop,
        Nex_Largetile,
        Nex_Light,
        Nex_Link,
        Nex_More,
        Nex_Ok,
        Nex_Options,
        Nex_Paste,
        Nex_Redo,
        Nex_Resize,
        Nex_Save,
        Nex_Security,
        Nex_Shopping,
        Nex_Undo,
        Nex_ZoomIn,
        Nex_ZoomOut,
        Nex_Camera,
        Nex_MailSend,
        Nex_Phone,
        Nex_Key,
        Nex_Calender,
        Nex_Internet,
        Nex_Walk,
        Nex_CameraSave,
        Nex_Exclusion,
        Nex_CancelarExclusion,
        Nex_SaveFolder,
        Nex_OptionsMenu

    }

    protected Drawable mDrawable;

    protected CharSequence mContentDescription;
    protected View mItemView;

    protected Context mContext;
    protected ActionBarGD mActionBar;

    private int mItemId;

    public void setActionBar(ActionBarGD actionBar) {
        mContext = actionBar.getContext();
        mActionBar = actionBar;
    }

    public Drawable getDrawable() {
        return mDrawable;
    }

    public ActionBarItem setDrawable(int drawableId) {
        return setDrawable(mContext.getResources().getDrawable(drawableId));
    }

    public ActionBarItem setDrawable(Drawable drawable) {
        if (drawable != mDrawable) {
            mDrawable = drawable;
            if (mItemView != null) {
                onDrawableChanged();
            }
        }
        return this;
    }

    public CharSequence getContentDescription() {
        return mContentDescription;
    }

    public ActionBarItem setContentDescription(int contentDescriptionId) {
        return setContentDescription(mContext.getString(contentDescriptionId));
    }

    public ActionBarItem setContentDescription(CharSequence contentDescription) {
        if (contentDescription != mContentDescription) {
            mContentDescription = contentDescription;
            if (mItemView != null) {
                onContentDescriptionChanged();
            }
        }
        return this;
    }

    public View getItemView() {
        if (mItemView == null) {
            mItemView = createItemView();
            prepareItemView();
        }
        return mItemView;
    }

    protected abstract View createItemView();

    protected void prepareItemView() {
    }

    protected void onDrawableChanged() {
    }

    protected void onContentDescriptionChanged() {
    }

    protected void onItemClicked() {
    }

    public void setItemId(int itemId) {
        mItemId = itemId;
    }

    public int getItemId() {
        return mItemId;
    }

    public static ActionBarItem createWithType(ActionBarGD actionBar, ActionBarItem.Type type) {

        int drawableId;
        int descriptionId;

        switch (type) {
            case GoHome:
                drawableId = R.drawable.gd_action_bar_home;
                descriptionId = R.string.gd_go_home;
                break;

            case Search:
                drawableId = R.drawable.gd_action_bar_search;
                descriptionId = R.string.gd_search;
                break;

            case Talk:
                drawableId = R.drawable.gd_action_bar_talk;
                descriptionId = R.string.gd_talk;
                break;

            case Compose:
                drawableId = R.drawable.gd_action_bar_compose;
                descriptionId = R.string.gd_compose;
                break;

            case Export:
                drawableId = R.drawable.gd_action_bar_export;
                descriptionId = R.string.gd_export;
                break;

            case Share:
                drawableId = R.drawable.gd_action_bar_share;
                descriptionId = R.string.gd_share;
                break;

            case Refresh:
                return actionBar.newActionBarItem(LoaderActionBarItem.class)
                        .setDrawable(new ActionBarDrawable(actionBar.getContext(), R.drawable.gd_action_bar_refresh))
                        .setContentDescription(R.string.gd_refresh);

            case TakePhoto:
                drawableId = R.drawable.gd_action_bar_take_photo;
                descriptionId = R.string.gd_take_photo;
                break;
            //
            // case PickPhoto:
            // drawableId = R.drawable.gd_action_bar_pick_photo;
            // descriptionId = R.string.gd_pick_photo;
            // break;

            case Locate:
                drawableId = R.drawable.gd_action_bar_locate;
                descriptionId = R.string.gd_locate;
                break;

            case Edit:
                drawableId = R.drawable.gd_action_bar_edit;
                descriptionId = R.string.gd_edit;
                break;

            case Add:
                drawableId = R.drawable.gd_action_bar_add;
                descriptionId = R.string.gd_add;
                break;

            case Star:
                drawableId = R.drawable.gd_action_bar_star;
                descriptionId = R.string.gd_star;
                break;

            case SortBySize:
                drawableId = R.drawable.gd_action_bar_sort_by_size;
                descriptionId = R.string.gd_sort_by_size;
                break;

            case LocateMyself:
                drawableId = R.drawable.gd_action_bar_locate_myself;
                descriptionId = R.string.gd_locate_myself;
                break;

            case Compass:
                drawableId = R.drawable.gd_action_bar_compass;
                descriptionId = R.string.gd_compass;
                break;

            case Help:
                drawableId = R.drawable.gd_action_bar_help;
                descriptionId = R.string.gd_help;
                break;

            case Info:
                drawableId = R.drawable.gd_action_bar_info;
                descriptionId = R.string.gd_info;
                break;

            case Settings:
                drawableId = R.drawable.gd_action_bar_settings;
                descriptionId = R.string.gd_settings;
                break;

            case List:
                drawableId = R.drawable.gd_action_bar_list;
                descriptionId = R.string.gd_list;
                break;

            case Trashcan:
                drawableId = R.drawable.gd_action_bar_trashcan;
                descriptionId = R.string.gd_trashcan;
                break;

            case Eye:
                drawableId = R.drawable.gd_action_bar_eye;
                descriptionId = R.string.gd_eye;
                break;

            case AllFriends:
                drawableId = R.drawable.gd_action_bar_all_friends;
                descriptionId = R.string.gd_all_friends;
                break;

            case Group:
                drawableId = R.drawable.gd_action_bar_group;
                descriptionId = R.string.gd_group;
                break;

            case Gallery:
                drawableId = R.drawable.gd_action_bar_gallery;
                descriptionId = R.string.gd_gallery;
                break;

            case Slideshow:
                drawableId = R.drawable.gd_action_bar_slideshow;
                descriptionId = R.string.gd_slideshow;
                break;

            case Mail:
                drawableId = R.drawable.gd_action_bar_mail;
                descriptionId = R.string.gd_mail;
                break;

            case SearchResult:
                drawableId = R.drawable.gd_action_bar_search_result;
                descriptionId = R.string.gd_nex_search_result;
                break;
            case Familia:
                drawableId = R.drawable.gd_action_bar_familia;
                descriptionId = R.string.gd_nex_familia;
                break;
            case SubFamilia:
                drawableId = R.drawable.gd_action_bar_subfamilia;
                descriptionId = R.string.gd_nex_subfamilia;
                break;
            case Linea:
                drawableId = R.drawable.gd_action_bar_linea;
                descriptionId = R.string.gd_nex_linea;
                break;
            case PedidoRapido:
                drawableId = R.drawable.gd_action_bar_pedidorapido;
                descriptionId = R.string.gd_nex_pedido_rapido;
                break;
            case Nex_Chart:
                drawableId = R.drawable.gd_nex_action_bar_chart;
                descriptionId = R.string.gd_nex_chart;
                break;
            case Nex_Exit:
                drawableId = R.drawable.gd_nex_action_bar_exit;
                descriptionId = R.string.gd_nex_exit;
                break;
            case Nex_ArrowDown:
                drawableId = R.drawable.gd_nex_action_bar_arrow_down;
                descriptionId = R.string.gd_nex_arrow_down;
                break;
            case Nex_ArrowUp:
                drawableId = R.drawable.gd_nex_action_bar_arrow_up;
                descriptionId = R.string.gd_nex_arrow_up;
                break;
            case Nex_Back:
                drawableId = R.drawable.gd_nex_action_bar_back_icon;
                descriptionId = R.string.gd_nex_back_icon;
                break;
            case Nex_Business:
                drawableId = R.drawable.gd_nex_action_bar_business;
                descriptionId = R.string.gd_nex_business;
                break;
            case Nex_Cancel:
                drawableId = R.drawable.gd_nex_action_bar_cancel;
                descriptionId = R.string.gd_nex_cancel;
                break;
            case Nex_Car:
                drawableId = R.drawable.gd_nex_action_bar_car;
                descriptionId = R.string.gd_nex_car;
                break;
            case Nex_Categories:
                drawableId = R.drawable.gd_nex_action_bar_categories;
                descriptionId = R.string.gd_nex_categories;
                break;
            case Nex_Clock:
                drawableId = R.drawable.gd_nex_action_bar_clock;
                descriptionId = R.string.gd_nex_clock;
                break;
            case Nex_Copy:
                drawableId = R.drawable.gd_nex_action_bar_copy;
                descriptionId = R.string.gd_nex_copy;
                break;
            case Nex_Cut:
                drawableId = R.drawable.gd_nex_action_bar_cut;
                descriptionId = R.string.gd_nex_cut;
                break;
            case Nex_Database:
                drawableId = R.drawable.gd_nex_action_bar_database;
                descriptionId = R.string.gd_nex_database;
                break;
            case Nex_Dialog:
                drawableId = R.drawable.gd_nex_action_bar_dialog;
                descriptionId = R.string.gd_nex_dialog;
                break;
            case Nex_Disc:
                drawableId = R.drawable.gd_nex_action_bar_disc;
                descriptionId = R.string.gd_nex_disc;
                break;
            case Nex_Flag:
                drawableId = R.drawable.gd_nex_action_bar_flag;
                descriptionId = R.string.gd_nex_flag;
                break;
            case Nex_Flash:
                drawableId = R.drawable.gd_nex_action_bar_flash;
                descriptionId = R.string.gd_nex_flash;
                break;
            case Nex_Globe:
                drawableId = R.drawable.gd_nex_action_bar_globe;
                descriptionId = R.string.gd_nex_globe;
                break;
            case Nex_Laptop:
                drawableId = R.drawable.gd_nex_action_bar_laptop;
                descriptionId = R.string.gd_nex_laptop;
                break;
            case Nex_Largetile:
                drawableId = R.drawable.gd_nex_action_bar_largetiles;
                descriptionId = R.string.gd_nex_largetiles;
                break;
            case Nex_Light:
                drawableId = R.drawable.gd_nex_action_bar_light;
                descriptionId = R.string.gd_nex_light;
                break;
            case Nex_Link:
                drawableId = R.drawable.gd_nex_action_bar_link;
                descriptionId = R.string.gd_nex_link;
                break;
            case Nex_More:
                drawableId = R.drawable.gd_nex_action_bar_more;
                descriptionId = R.string.gd_nex_more;
                break;
            case Nex_Ok:
                drawableId = R.drawable.gd_nex_action_bar_ok;
                descriptionId = R.string.gd_nex_ok;
                break;
            case Nex_Options:
                drawableId = R.drawable.gd_nex_action_bar_options;
                descriptionId = R.string.gd_nex_options;
                break;
            case Nex_Paste:
                drawableId = R.drawable.gd_nex_action_bar_paste;
                descriptionId = R.string.gd_nex_paste;
                break;
            case Nex_Redo:
                drawableId = R.drawable.gd_nex_action_bar_redo;
                descriptionId = R.string.gd_nex_redo;
                break;
            case Nex_Resize:
                drawableId = R.drawable.gd_nex_action_bar_resize;
                descriptionId = R.string.gd_nex_resize;
                break;
            case Nex_Save:
                drawableId = R.drawable.gd_nex_action_bar_save;
                descriptionId = R.string.gd_nex_save;
                break;
            case Nex_Security:
                drawableId = R.drawable.gd_nex_action_bar_security;
                descriptionId = R.string.gd_nex_security;
                break;
            case Nex_Shopping:
                drawableId = R.drawable.gd_nex_action_bar_shopping;
                descriptionId = R.string.gd_nex_shopping;
                break;
            case Nex_Undo:
                drawableId = R.drawable.gd_nex_action_bar_undo;
                descriptionId = R.string.gd_nex_undo;
                break;
            case Nex_ZoomIn:
                drawableId = R.drawable.gd_nex_action_bar_zoom_in;
                descriptionId = R.string.gd_nex_zoom_in;
                break;
            case Nex_ZoomOut:
                drawableId = R.drawable.gd_nex_action_bar_zoom_out;
                descriptionId = R.string.gd_nex_zoom_out;
                break;
            case Nex_Camera:
                drawableId = R.drawable.gd_nex_action_bar_camera;
                descriptionId = R.string.gd_nex_camera;
                break;
            case Nex_MailSend:
                drawableId = R.drawable.gd_nex_action_bar_mail_send;
                descriptionId = R.string.gd_nex_send;
                break;
            case Nex_Phone:
                drawableId = R.drawable.gd_nex_action_bar_phone;
                descriptionId = R.string.gd_nex_phone;
                break;
            case Nex_Key:
                drawableId = R.drawable.gd_nex_action_bar_key;
                descriptionId = R.string.gd_nex_key;
                break;
            case Nex_Calender:
                drawableId = R.drawable.gd_nex_action_bar_calender;
                descriptionId = R.string.gd_nex_calender;
                break;
            case Nex_Internet:
                drawableId = R.drawable.gd_nex_action_bar_internet;
                descriptionId = R.string.gd_nex_internet;
                break;
            case Nex_Walk:
                drawableId = R.drawable.gd_nex_action_bar_walk;
                descriptionId = R.string.gd_nex_walk;
                break;
            case Nex_CameraSave:
                drawableId = R.drawable.gd_nex_action_bar_camera_save;
                descriptionId = R.string.gd_nex_camera_save;
                break;
            case Nex_Exclusion:
                drawableId = R.drawable.gd_nex_exclusion;
                descriptionId = R.string.gd_nex_exclusion;
                break;
            case Nex_CancelarExclusion:
                drawableId = R.drawable.gd_nex_cancelar_exclusion;
                descriptionId = R.string.gd_nex_cancelar_exclusion;
                break;

            case Nex_SaveFolder:
                drawableId = R.drawable.gd_nex_action_bar_save_folder;
                descriptionId = R.string.gd_nex_save_folder;
                break;

            case Nex_OptionsMenu:
                drawableId = R.drawable.gd_nex_action_bar_menu_options;
                descriptionId = R.string.gd_nex_menu_options;
                break;

            default:
                // Do nothing but return null
                return null;
        }

        final Drawable d = new ActionBarDrawable(actionBar.getContext(), drawableId);

        return actionBar.newActionBarItem(NormalActionBarItem.class).setDrawable(d)
                .setContentDescription(descriptionId);
    }

    @Override
    public boolean equals(Object o) {
        Log.v("GD", "ActionBarItem.equals");
        if (!(o instanceof ActionBarItem)) {
            return false;
        }
        ActionBarItem oa = (ActionBarItem) o;
        return (getContentDescription().equals(oa.getContentDescription())
                && getDrawable().equals(oa.getDrawable())
                && getItemId() == oa.getItemId());
    }
}
