package pe.entel.android.pedidos.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.security.MessageDigest;
import java.util.Properties;

import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.dal.DalConfiguracion;

public class Configuracion {

    //PERFILES
    public final static String perfilSup = "4";
    public final static String perfilVen = "2";

    //
    public final static String LOGIN_EVENT = "login";
    public final static String PEDIDO_EVENT = "pedido";
    public final static String PENDIENTE_EVENT = "pendiente";
    public final static String COBRANZA_EVENT = "cobranza";
    public final static String SINCRONIZACION_EVENT = "sincronizacion";
    public final static String DOCUMENTO_EVENT = "documento";

    //TABLA CONFIGURACION
    public final static String NAME_TABLE_CONFIGURACION = "TBL_CONFIGURACION";
    public final static String NAME_COLUMN_CODIGO = "CODIGO";
    public final static String NAME_COLUMN_VALOR = "VALOR";
    public final static String NAME_COLUMN_DESCRIPCION = "DESCRIPCION";
    public final static String NAME_COLUMN_CODIGO_PADRE = "COD_CONF_PADRE";

    //TABLA VENDEDOR
    public final static String NAME_TBL_VENDEDOR = "TBL_VENDEDOR";

    //Para mostrar tema : vacio es el default, 0 es cambiable.
    public final static String FLGESTILO = "";

    public final static int TIEMPOTIMEOUT = 120000;
    public final static String ETIQUETA_DSTO_FORMA_PAGO = "Dscto Forma Pago";//334;

    public final static String FLGDIRECCIONPEDIDO = "P";
    public final static String FLGDIRECCIONDESPACHO = "D";
    public final static String FLGDIRECCIONDEFAULT = "";
    public final static String FLGPEDIDO = "P";
    public final static String FLGCOTIZACION = "C";
    public final static String FLGVERDADERO = "T";
    public final static String FLGFALSO = "F";
    public final static String FLGEDITARCLI = "EDC";
    public final static String FLGREGHABILITADO = "1";
    public final static String FLGREGNOHABILITADO = "0";

    public final static int MAXDECIMALMONTO = 3;
    public final static int MAXDECIMALPORC = 2;

    public final static String GENERAL1MOTNOPED = "1";
    public final static String GENERAL2FORMAPAG = "2";
    public final static String GENERAL3BANCO = "3";
    public final static String GENERAL4TIPOPAGO = "4";
    public final static String GENERAL5MOTCANJE = "5";
    public final static String GENERAL6MOTDEVO = "6";
    public final static String GENERAL7TIPOCLIENTE = "7";
    public final static String GENERAL8MOTANULA = "8";
    public final static String GENERAL9MOTCORRELATIVO = "9";
    public final static String GENERAL10MOTNOCOBRANZA = "10";

    public final static int CONSPEDIDO = 1;
    public final static int CONSNOPEDIDO = 2;
    public final static int CONSCOBRANZA = 3;
    public final static int CONSCANJE = 4;
    public final static int CONSDEVOLUC = 5;
    public final static int CONSSTOCK = 6;

    public final static String ESTADO01PORVISITAR = "POR VISITAR";
    public final static String ESTADO02VISITADO = "VISITADO";
    public final static String ESTADO03CONPEDIDO = "CON PEDIDO";

    public final static String SIMBOLOMONEDA2 = "S/.";
    public final static String SIMBOLOMONEDA3 = "$";

    public final static String MONEDASOLES = "Soles"; //@JBELVY
    public final static String MONEDADOLARES = "Dolares"; //@JBELVY

    public final static int TYPE_ALPHANUMERIC = 1;
    public final static int TYPE_CHECKBOX = 2;
    public final static int TYPE_COMBOBOX = 3;
    public final static int TYPE_NUMERIC = 4;
    public final static int TYPE_DATE = 5;
    public final static int TYPE_DECIMAL = 6;
    public final static int TYPE_TIME = 7;
    public final static int TYPE_RADIOBUTTON = 8;

    public final static int FORMULARIO_TODOS = 0;
    public final static int FORMULARIO_INICIO = 1;
    public final static int FORMULARIO_FIN = 2;

    public final static int CANTIDAD_DIGITOS = 20;

    /*-----------------------------------------------------
    CONSTANTES INTENT EXTRA
   -----------------------------------------------------*/
    public final static String EXTRA_NAME_DIRECCION = "fujoDireccion";
    public final static String EXTRA_FORMULARIO = "fujoFormulario";
    public final static String EXTRA_FORMULARIO_INICIO_EDITAR = "editarFormularioInicio";

    /*-----------------------------------------------------
     CONSTANTES PREFERENCAS
    -----------------------------------------------------*/
    public final static String CONSPRECODUSUARIO = "CodUsuario";
    public final static String CONSPRECODGUARDADO = "CodUsuarioGuardado";
    public final static String CONSPREUSURESETEADO = "UsuarioResseteado";

    public final static String CONSTANTE_BUSQUEDA_CODIGO = "C";
    public final static String CONSTANTE_BUSQUEDA_NOMBRE = "N";
    public final static String CONSTANTE_BUSQUEDA_ONLINE = "O";

    public static class TipoArticulo {
        public final static String PRODUCTO = "PRO";
        public final static String PRESENTACION = "PRE";

    }

    public static class TipoBusqueda {
        public final static String PRODUCTO = "PRO";
        public final static String PRESENTACION = "PRE";

    }

    public final static String CURRENT_BEAN_USUARIO = "BeanUsuario";
    public final static String CURRENT_BEAN_PEDIDOCAB = "PedidoCab";
    public final static String CURRENT_FECHA_REPORTE = "fecha_reporte";
    public final static String CURRENT_USUARIO_KPI = "usuarioKpi";
    public final static String CURRENT_TIPO_GENERAL = "reporte_general";

    public final static String DESCUENTO_PORCENTAJE = "P";
    public final static String DESCUENTO_MONTO = "M";

    public final static String NUMERO_TELEFONO = "999999999";

    /*-----------------------------------------------------
    ENUM URL
    -----------------------------------------------------*/
    public static enum EnumUrl {
        LOGIN,
        SINTODO,
        SINCONF,
        BUSQONLINE,
        GRABARPEDIDO,
        GRABARPENDIENTEPED,
        GRABARPENDIENTE,
        GRABARCOBRANZA,
        GRABARCANJE,
        GRABARDEVOLUCION,
        CLIENTEONLINE,
        PRODUCTOONLINE,
        PEDIDOONLINE,
        CONSPEDIDOONLINE,
        GRABARDIRECCION,
        VERIFICAR,
        CONSULTAPRODUCTO,
        CONSULTAPEDIDO,
        VERIFICARPEDIDO,
        EFICIENCIAONLINE,
        CONSPAGOONLINE,
        ENVIOPROSPECTO,
        ACTUALIZARPROCESADO,
        CONSULTARECIBOUSUARIO,
        GRABARDOCUMENTO,
        GRABARNOCOBRANZA,
        EDITARDATOSCLIENTE
    }

    public static enum EnumUrlService {
        GETSUCURSALES
    }

    public static String fnUrlService(Context poContext, EnumUrlService poEnumUrl) {

        Properties loProperties = Utilitario.readProperties(poContext);
        String lsIpServerService = loProperties.getProperty("IP_SERVER_SERVICE");

        String lsUrl = "";

        String lsRuta = "http://" + lsIpServerService + "/";

        if (poEnumUrl == EnumUrlService.GETSUCURSALES) {
            lsUrl = lsRuta + "GetSucursales.ashx";
        }

        return lsUrl;
    }

    /*-----------------------------------------------------
     URLs
    -----------------------------------------------------*/
    public static String fnUrl(Context poContext, EnumUrl poEnumUrl) {

        Properties loProperties = Utilitario.readProperties(poContext);
        String lsIpServer = loProperties.getProperty("IP_SERVER");

        String lsUrl = "";
        SharedPreferences loSharedPreferences = PreferenceManager.getDefaultSharedPreferences(poContext);

        String lsRuta = "http://" + loSharedPreferences.getString(poContext.getString(R.string.keypre_url), lsIpServer) + "/";

        String verifSucursales = loSharedPreferences.getString(poContext.getString(R.string.keyPrefVerifSucursal), "N");

        if (verifSucursales.equals("S")) {
            lsRuta = "http://" + loSharedPreferences.getString(poContext.getString(R.string.keyPrefIPSucursal), lsIpServer);
        }

        if (poEnumUrl == EnumUrl.LOGIN) {
            lsUrl = lsRuta + "Login.ashx";
        } else if (poEnumUrl == EnumUrl.SINTODO) {
            lsUrl = lsRuta + "SinTodo.ashx";
        } else if (poEnumUrl == EnumUrl.BUSQONLINE) {
            lsUrl = lsRuta + "PedidoRealizadoOnline.ashx";
        } else if (poEnumUrl == EnumUrl.GRABARPEDIDO) {
            lsUrl = lsRuta + "GrabarPedido.ashx";
        } else if (poEnumUrl == EnumUrl.GRABARPENDIENTEPED) {
            lsUrl = lsRuta + "GrabarPendientePed.ashx";
        } else if (poEnumUrl == EnumUrl.GRABARPENDIENTE) {
            lsUrl = lsRuta + "GrabarPendiente.ashx";
        } else if (poEnumUrl == EnumUrl.GRABARCOBRANZA) {
            lsUrl = lsRuta + "GrabarCobranza.ashx";
        } else if (poEnumUrl == EnumUrl.GRABARCANJE) {
            lsUrl = lsRuta + "GrabarCanje.ashx";
        } else if (poEnumUrl == EnumUrl.GRABARDEVOLUCION) {
            lsUrl = lsRuta + "GrabarDevolucion.ashx";
        } else if (poEnumUrl == EnumUrl.CLIENTEONLINE) {
            lsUrl = lsRuta + "ConsultaOnline.ashx";
        } else if (poEnumUrl == EnumUrl.PRODUCTOONLINE) {
            lsUrl = lsRuta + "ProductoOnline.ashx";
        } else if (poEnumUrl == EnumUrl.PEDIDOONLINE) {
            lsUrl = lsRuta + "PedidoOnline.ashx";
        } else if (poEnumUrl == EnumUrl.CONSPEDIDOONLINE) {
            lsUrl = lsRuta + "ConsultaPedidosOnline.ashx";
        } else if (poEnumUrl == EnumUrl.GRABARDIRECCION) {
            lsUrl = lsRuta + "GrabarDireccion.ashx";
        } else if (poEnumUrl == EnumUrl.VERIFICAR) {
            lsUrl = lsRuta + "verificar.ashx";
        } else if (poEnumUrl == EnumUrl.CONSULTAPRODUCTO) {
            lsUrl = lsRuta + "ConsultaProducto.ashx";
        } else if (poEnumUrl == EnumUrl.CONSULTAPEDIDO) {
            lsUrl = lsRuta + "ConsultaPedidoEstado.ashx";
        } else if (poEnumUrl == EnumUrl.SINCONF) {
            lsUrl = lsRuta + "SincronizarConfiguracion.ashx";
        } else if (poEnumUrl == EnumUrl.VERIFICARPEDIDO) {
            lsUrl = lsRuta + "VerificarPedido.ashx";
        } else if (poEnumUrl == EnumUrl.EFICIENCIAONLINE) {
            lsUrl = lsRuta + "ConsultaEficienciaOnline.ashx";
        } else if (poEnumUrl == EnumUrl.CONSPAGOONLINE) {
            lsUrl = lsRuta + "ConsultaPagosOnline.ashx";
        } else if (poEnumUrl == EnumUrl.ENVIOPROSPECTO) {
            lsUrl = lsRuta + "GrabarProspecto.ashx";
        } else if (poEnumUrl == EnumUrl.ACTUALIZARPROCESADO) {
            lsUrl = lsRuta + "ActualizarEstadoProcesado.ashx";
        } else if (poEnumUrl == EnumUrl.CONSULTARECIBOUSUARIO) {
            lsUrl = lsRuta + "ConsultaReciboUsuario.ashx";
        } else if  (poEnumUrl == EnumUrl.GRABARDOCUMENTO){
            lsUrl = lsRuta + "GrabarDocumentoHistorial.ashx";
        } else if  (poEnumUrl == EnumUrl.GRABARNOCOBRANZA){
            lsUrl = lsRuta + "GrabarNoCobranza.ashx";
        } else if  (poEnumUrl == EnumUrl.EDITARDATOSCLIENTE){
            lsUrl = lsRuta + "EditarClienteOnline.ashx";
        }

        return lsUrl;
    }
    /*-----------------------------------------------------
    ENUM FLUJO DIRECCION
     -----------------------------------------------------*/

    public static enum EnumFlujoDireccion {
        PEDIDO,
        DESPACHO,
        NO_VISITA;

        public String getCodigo(Context context) {
            return context.getResources().getStringArray(R.array.flujo_direccion_codigo_strings)[ordinal()];
        }

        public String getTitulo(Context context) {
            return context.getResources().getStringArray(R.array.flujo_direccion_titulo_strings)[ordinal()];
        }
    }

    /*-----------------------------------------------------
    ENUM MODULOS
	-----------------------------------------------------*/
    public static enum EnumModulo {
        PEDIDO,
        COBRANZA,
        DEVOLUCION,
        CANJE,
        COTIZACION,
        EDITARCLIENTE;

        public String getCodigo(Context context) {
            return context.getResources().getStringArray(R.array.modulo_strings)[ordinal()];
        }

        public String getValor(Context context) {
            DalConfiguracion dalConfiguracion = new DalConfiguracion(context);
            return dalConfiguracion.fnValorXCodigo(getCodigo(context), Configuracion.NAME_COLUMN_VALOR);
        }

        public String getDescripcion(Context context) {
            DalConfiguracion dalConfiguracion = new DalConfiguracion(context);
            return dalConfiguracion.fnValorXCodigo(getCodigo(context), Configuracion.NAME_COLUMN_DESCRIPCION);
        }

        public String getCliente(Context context) {
            DalConfiguracion dalConfiguracion = new DalConfiguracion(context);
            return dalConfiguracion.fnValorXCodigo(getCodigo(context), Configuracion.NAME_COLUMN_DESCRIPCION);
        }
    }

    /*-----------------------------------------------------
    ENUM MODULOS
	-----------------------------------------------------*/
    public static enum EnumFuncion {
        TOMA_PEDIDO,
        DESCUENTO,  //flgPermisoDescuento
        STOCK,
        PRECIO,
        BONIFICACION, //flgBonificacion
        COBRANZA,
        DEVOLUCION,
        CANJE,
        CLIENTE,
        COTIZACION,
        FRACCIONAMIENTO;

        public String getCodigo(Context context) {
            return context.getResources().getStringArray(R.array.funcion_strings)[ordinal()];
        }

        public String getValor(Context context) {
            DalConfiguracion dalConfiguracion = new DalConfiguracion(context);
            String lsValorFuncion = dalConfiguracion.fnValorXCodigo(getCodigo(context), Configuracion.NAME_COLUMN_VALOR);

            if (lsValorFuncion.equals(Configuracion.FLGVERDADERO)) {

                String lsCodigoPadre = dalConfiguracion.fnValorXCodigo(getCodigo(context), Configuracion.NAME_COLUMN_CODIGO_PADRE);
                lsValorFuncion = dalConfiguracion.fnValorXCodigo(lsCodigoPadre, Configuracion.NAME_COLUMN_VALOR);

            }
            return lsValorFuncion;
        }

        public String getDescripcion(Context context) {
            DalConfiguracion dalConfiguracion = new DalConfiguracion(context);
            return dalConfiguracion.fnValorXCodigo(getCodigo(context), Configuracion.NAME_COLUMN_DESCRIPCION);
        }

    }

    /*-----------------------------------------------------
       ENUM CONFIGURACION
       -----------------------------------------------------*/
    public static enum EnumConfiguracion {
        HABILITAR_DESCUENTO,
        DESCUENTO_PORCENTAJE, //flgPermisoDescPorc
        RANGO_DESCUENTO_MINIMO, //flgPermisoDescMin
        RANGO_DESCUENTO_MAXIMO, //flgPermisoDescMax
        STOCK,  // flgPermisoStockLinea ESTO ES PARA LA SINCRONIZACION
        VALIDAR_STOCK, //flgPermisoStockValidar
        DESCONTAR_STOCK, //flgDescuentaStock
        CONSULTAR_STOCK_LINEA, //flgConsultarStockLinea  //boton de consultar en linea en detalle de pedido
        STOCK_RESTRICTIVO, //flgRestrictivo //para validar la cantidad con el stock
        VALIDAR_PRECIO,
        PRECIO_EDITABLE, //flgPermisoEditarPrecio
        PRECIO_TIPO_CLIENTE, //flgPermisoPrecioTipoCli
        PRECIO_CONDICION_VENTA,
        BONIFICACION_MANUAL,
        MOSTRAR_CONDICION_VENTA,
        COBRANZA,
        DEVOLUCION,
        CANJE,
        GPS,
        EFICIENCIA,
        SEGUIMIENTO_RUTA,
        EMBEBIDO,
        NUM_DECIMALES_VISTA,
        NUM_DECIMALES_DESCARGA,
        PAGINACION,
        LONGITUD_COD_PRODUCTO,
        DESCUENTO_MONTO, //flgPermisoDescMonto
        SIMBOLO_MONEDA,
        MOSTRAR_ICONO,
        IMAGEN_ICONO,
        HABILITAR_DESCUENTO_PRODUCTO,//NNUEVO
        HABILITAR_DESCUENTO_GENERAL, //NUEVO
        CLIENTE_COLUMNA_ADICIONAL_1,
        CLIENTE_COLUMNA_ADICIONAL_2,
        CLIENTE_COLUMNA_ADICIONAL_3,
        CLIENTE_COLUMNA_ADICIONAL_4,
        CLIENTE_COLUMNA_ADICIONAL_5,
        COTIZACION,
        MOSTRAR_DIRECCION_PEDIDO,
        MOSTRAR_DIRECCION_DESPACHO,
        MOSTRAR_DIRECCION_NO_VISITA,
        CREAR_DIRECCION_PEDIDO,
        MOSTRAR_ALMACEN,
        FRACC_SIMPLE,
        FRACC_MULTIPLE,
        MOSTRAR_CABECERA_PEDIDO,
        MOSTRAR_FIN_PEDIDO,
        CONSULTAR_PRODUCTO,
        BONIFICACION_AUTOMATICA,
        SEGUIMIENTO_PEDIDO,
        MAXIMO_NUMERODECIMALES,
        TECLADO_ALFANUMERICO,
        VERIFICACION_PEDIDO,
        COBRANZA_VENCIDA,
        LIMITE_CREDITO,
        COBRANZA_CLIENTE_FUERA_RUTA,
        MAXIMO_ITEMS_PEDIDO,
        MONTO_MAXIMO_PEDIDO,
        MONTO_MINIMO_PEDIDO,
        BUSQUEDA_NUMERICA_CLIENTES,
        BUSQUEDA_NUMERICA_ARTICULOS,
        MODO_FUERA_DE_COBERTURA;

        public String getCodigo(Context context) {
            return context.getResources().getStringArray(R.array.configuracion_strings)[ordinal()];
        }

        public String getValor(Context context) {

            DalConfiguracion dalConfiguracion = new DalConfiguracion(context);
            String lsValorConfiguracion = dalConfiguracion.fnValorXCodigo(getCodigo(context), Configuracion.NAME_COLUMN_VALOR);

            if (lsValorConfiguracion.equals(Configuracion.FLGVERDADERO)) {

                String lsCodigoPadre = dalConfiguracion.fnValorXCodigo(getCodigo(context), Configuracion.NAME_COLUMN_CODIGO_PADRE);
                if (lsCodigoPadre.equals("FGEN")) {
                    lsValorConfiguracion = "T";
                } else {

                    lsValorConfiguracion = dalConfiguracion.fnValorXCodigo(lsCodigoPadre, Configuracion.NAME_COLUMN_VALOR);

                    if (lsValorConfiguracion.equals(Configuracion.FLGVERDADERO)) {
                        lsCodigoPadre = dalConfiguracion.fnValorXCodigo(lsCodigoPadre, Configuracion.NAME_COLUMN_CODIGO_PADRE);
                        lsValorConfiguracion = dalConfiguracion.fnValorXCodigo(lsCodigoPadre, Configuracion.NAME_COLUMN_VALOR);
                    }
                }
            }
            return lsValorConfiguracion;
        }

        public String getDescripcion(Context context) {
            DalConfiguracion dalConfiguracion = new DalConfiguracion(context);
            return dalConfiguracion.fnValorXCodigo(getCodigo(context),
                    Configuracion.NAME_COLUMN_DESCRIPCION);
        }


    }


    /**
     * Método que codifica una cadena a formato cifrado
     *
     * @param var cadena a ser convertida
     * @return cadena cifrada
     */
    public static String convertToSHA1(String var) {
        try {
            String datHex = "";
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] arrData = var.getBytes();

            md.update(arrData, 0, arrData.length);
            byte[] digest = new byte[100];
            int val = md.digest(digest, 0, digest.length);

            for (int i = 0; i < val; i++) {
                String hex = Integer.toHexString(digest[i]);
                if (hex.length() == 1)
                    hex = "0" + hex;
                hex = hex.substring(hex.length() - 2);
                datHex = datHex + hex;
            }

            return datHex.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
