package pe.entel.android.pedidos.act;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;

import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;

public class ActConfirmationMessage extends NexActivity {
    private static final int DIACONFIR = 0;

    public final static String KEY_TITLE = "title";
    public final static String KEY_MESSAGE = "message";
    public final static String KEY_YESNO = "yesno";
    public final static String KEY_ICON = "icon";
    public final static String KEY_LAYOUT = "layout";

    public static OnConfirmationMessageListener LISTENER = null;

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LISTENER = null;
    }

    private EnumLayoutResource ObtainMessageLayout() {
        EnumLayoutResource _layout;
        try {
            _layout = EnumLayoutResource.valueOf(getIntent().getStringExtra(
                    KEY_LAYOUT));
        } catch (Exception e) {
            _layout = EnumLayoutResource.DESIGN01;
        }
        return _layout;
    }

    @Override
    protected final void onPostResume() {
        super.onPostResume();
        showDialog(DIACONFIR);
    }

    @Override
    protected final void subIniActionBar() {
        getActionBarGD().setVisibility(View.GONE);
    }

    @Override
    protected final void subSetControles() {
        LayoutParams _params = new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT);
        LinearLayout _linLayout = new LinearLayout(this);
        _linLayout.setLayoutParams(_params);
        setActionBarContentView(_linLayout);
    }

    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        DialogFragmentYesNo loDialog;
        switch (id) {
            case DIACONFIR:
                int _iconResource = getIntent().getIntExtra(KEY_ICON, -1);
                if (_iconResource != -1) {
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG
                                    .concat("DIACONFIR"), getIntent()
                                    .getStringExtra(KEY_TITLE), getIntent()
                                    .getStringExtra(KEY_MESSAGE), _iconResource,
                            getIntent().getBooleanExtra(KEY_YESNO, false),
                            ObtainMessageLayout());
                } else {
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG
                                    .concat("DIACONFIR"), getIntent()
                                    .getStringExtra(KEY_TITLE), getIntent()
                                    .getStringExtra(KEY_MESSAGE), getIntent()
                                    .getBooleanExtra(KEY_YESNO, false),
                            ObtainMessageLayout());
                }
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                if (ActConfirmationMessage.LISTENER != null)
                                    LISTENER.performButtonYes(dialog);
                                setResult(RESULT_OK);
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                                if (ActConfirmationMessage.LISTENER != null)
                                    LISTENER.performButtonNo(dialog);
                                setResult(RESULT_CANCELED);
                            }
                        });

            default:
                return super.onCreateNexDialog(id);
        }
    }

    public static interface OnConfirmationMessageListener {
        public void performButtonYes(DialogFragmentYesNo dialog);

        public void performButtonNo(DialogFragmentYesNo dialog);
    }
}
