package pe.entel.android.pedidos;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.codehaus.jackson.type.TypeReference;

import java.util.List;
import java.util.Properties;

import cat.ereza.customactivityoncrash.config.CaocConfig;
import greendroid.app.GDApplication;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumRolPreferencia;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTheme;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.act.ActMenu;
import pe.entel.android.pedidos.act.ActPreferencias;
import pe.entel.android.pedidos.bean.BeanArticulo;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanCobranzaOnline;
import pe.entel.android.pedidos.bean.BeanConsultaProducto;
import pe.entel.android.pedidos.bean.BeanEnvCanjeCab;
import pe.entel.android.pedidos.bean.BeanEnvDevolucionCab;
import pe.entel.android.pedidos.bean.BeanEnvDocumento;
import pe.entel.android.pedidos.bean.BeanEnvItemDet;
import pe.entel.android.pedidos.bean.BeanEnvNoCobranza;
import pe.entel.android.pedidos.bean.BeanEnvPago;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanKPI;
import pe.entel.android.pedidos.bean.BeanRespConsultaPedido;
import pe.entel.android.pedidos.bean.BeanRespListaEfecienciaOnline;
import pe.entel.android.pedidos.bean.BeanRespVerificarPedidoGeneral;
import pe.entel.android.pedidos.bean.BeanResponseConsultaClienteOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.bean.BeanUsuarioOnline;
import pe.entel.android.pedidos.bean.BeanValidaPedidoAndroid;
import pe.entel.android.pedidos.util.Configuracion;

public class AplicacionEntel extends GDApplication {
    private BeanUsuario currentUsuario = null;
    private BeanCliente currentCliente = null;
    private BeanEnvPago currentPago = null;
    private BeanEnvCanjeCab currentCanje = null;
    private BeanEnvDevolucionCab currentDevolucion = null;
    private List<BeanCobranzaOnline> currentListaCobranzasOnline = null;
    private BeanResponseConsultaClienteOnline currentConsultaClienteOnline = null;
    private BeanEnvDocumento currentDocumento = null;
    private BeanEnvNoCobranza currentNoCobranza = null;

    private BeanEnvItemDet currentItem = null;

    private BeanKPI currentKpi = null;

    private List<BeanArticulo> currentlistaArticulo = null;
    private BeanEnvPedidoCab currentPedido = null;
    private BeanConsultaProducto currentConsultaProducto = null;

    private BeanValidaPedidoAndroid currentValidaPedido = null;

    private int currentCodFlujo = 0;
    private int clienteOnline = 0;
    private boolean primeraVez;

    private BeanRespConsultaPedido currentRespConsultaPedido = null;
    private BeanRespVerificarPedidoGeneral currentRespVerificarPedidoGeneral = null;
    private BeanRespListaEfecienciaOnline currentRespListaEfecienciaOnline = null;
    private BeanUsuarioOnline currentRecibo = null;

    @Override
    public void onCreate() {
        super.onCreate();
        //Fabric.with(this, new Crashlytics());

        CaocConfig.Builder.create()
                //.errorActivity(ErrorActivity.class) //default: null (default error activity)
                .apply();
    }

    public void inicializarIndicators(String numero) {
        Properties loProperties = Utilitario.readProperties(this);
        String urlIndicators = loProperties.getProperty("URL_INDICATORS");
        String applicationId = loProperties.getProperty("APPLICATION_ID");
    }

    public void uploadEvent(String tag) {

    }

    public void uploadStatus() {

    }

    @Override
    public Class<?> getHomeActivityClass() {
        return ActMenu.class;
    }

    @Override
    public Intent getMainApplicationIntent() {
        return new Intent(Intent.ACTION_DEFAULT);
    }

    public BeanResponseConsultaClienteOnline getCurrentConsultaClienteOnline() {
        return currentConsultaClienteOnline;
    }

    public void setCurrentConsultaClienteOnline(BeanResponseConsultaClienteOnline currentConsultaClienteOnline) {
        this.currentConsultaClienteOnline = currentConsultaClienteOnline;
    }

    public List<BeanCobranzaOnline> getCurrentListaCobranzasOnline() {
        return currentListaCobranzasOnline;
    }

    public void setCurrentListaCobranzasOnline(List<BeanCobranzaOnline> currentListaCobranzasOnline) {
        this.currentListaCobranzasOnline = currentListaCobranzasOnline;
    }

    /**
     * @return the currentUsuario
     */
    public BeanUsuario getCurrentUsuario() {
        return currentUsuario;
    }

    /**
     * @param currentUsuario the currentUsuario to set
     */
    public void setCurrentUsuario(BeanUsuario currentUsuario) {
        this.currentUsuario = currentUsuario;
    }

    public BeanCliente getCurrentCliente() {

        if (currentCliente == null) {
            String lsBeanClienteCurrent = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE)
                    .getString("BeanClienteOnline", "");
            if (!lsBeanClienteCurrent.equals(""))
                currentCliente = (BeanCliente) BeanMapper.fromJson(
                        lsBeanClienteCurrent, BeanCliente.class);
        }

        return currentCliente;
    }

    public void setCurrentCliente(BeanCliente currentCliente) {

        this.currentCliente = currentCliente;

        String lsBeanCliente = "";
        try {
            lsBeanCliente = BeanMapper.toJson(currentCliente, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            lsBeanCliente = "";
        }

        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("BeanClienteOnline", lsBeanCliente);
        loEditor.commit();

    }


    /**
     * @return the currentPago
     */
    public BeanEnvPago getCurrentPago() {

        if (currentPago == null) {
            String lsBeanEnvPago = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE)
                    .getString("BeanEnvPagoCurrent", "");
            if (!lsBeanEnvPago.equals(""))
                currentPago = (BeanEnvPago) BeanMapper.fromJson(lsBeanEnvPago,
                        BeanEnvPago.class);
        }

        return currentPago;
    }

    /**
     * @param currentPago the currentPago to set
     */
    public void setCurrentPago(BeanEnvPago currentPago) {
        this.currentPago = currentPago;

        String lsBeanEnvPago = "";
        try {
            lsBeanEnvPago = BeanMapper.toJson(currentPago, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            lsBeanEnvPago = "";
        }

        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("BeanEnvPagoCurrent", lsBeanEnvPago);
        loEditor.commit();
    }

    /**
     * @return the currentCanje
     */
    public BeanEnvCanjeCab getCurrentCanje() {
        if (currentCanje == null) {
            String lsBeanEnvCanjeCab = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE)
                    .getString("BeanEnvCanjeCabCurrent", "");
            if (!lsBeanEnvCanjeCab.equals(""))
                currentCanje = (BeanEnvCanjeCab) BeanMapper.fromJson(
                        lsBeanEnvCanjeCab, BeanEnvCanjeCab.class);
        }

        return currentCanje;
    }

    /**
     * @param currentCanje the currentCanje to set
     */
    public void setCurrentCanje(BeanEnvCanjeCab currentCanje) {
        this.currentCanje = currentCanje;

        String lsBeanEnvCanjeCab = "";
        try {
            lsBeanEnvCanjeCab = BeanMapper.toJson(currentCanje, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            lsBeanEnvCanjeCab = "";
        }

        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("BeanEnvCanjeCabCurrent", lsBeanEnvCanjeCab);
        loEditor.commit();
    }

    /**
     * @return the currentDevolucion
     */
    public BeanEnvDevolucionCab getCurrentDevolucion() {
        if (currentDevolucion == null) {
            String lsBeanEnvDevolucionCab = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE)
                    .getString("BeanEnvDevolucionCabCurrent", "");
            if (!lsBeanEnvDevolucionCab.equals(""))
                currentDevolucion = (BeanEnvDevolucionCab) BeanMapper.fromJson(
                        lsBeanEnvDevolucionCab, BeanEnvDevolucionCab.class);
        }

        return currentDevolucion;
    }


    /**
     * @param currentDevolucion the currentDevolucion to set
     */
    public void setCurrentDevolucion(BeanEnvDevolucionCab currentDevolucion) {
        this.currentDevolucion = currentDevolucion;

        String lsBeanEnvDevolucionCab = "";
        try {
            lsBeanEnvDevolucionCab = BeanMapper
                    .toJson(currentDevolucion, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e("setCurrentDevolucion", "error", e);
            lsBeanEnvDevolucionCab = "";
        }

        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("BeanEnvDevolucionCabCurrent",
                lsBeanEnvDevolucionCab);
        loEditor.commit();
    }

    public BeanEnvItemDet getCurrentItem() {

        if (currentItem == null) {
            String lsBeanEnvItemDetCurrent = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE)
                    .getString("BeanEnvItemDetCurrent", "");
            if (!lsBeanEnvItemDetCurrent.equals(""))
                currentItem = (BeanEnvItemDet) BeanMapper.fromJson(
                        lsBeanEnvItemDetCurrent, BeanEnvItemDet.class);
        }

        return currentItem;
    }

    public void setCurrentItem(BeanEnvItemDet currentItem) {
        this.currentItem = currentItem;

        String lsBeanEnvItemDetCurrent = "";
        try {
            lsBeanEnvItemDetCurrent = BeanMapper.toJson(currentItem, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            lsBeanEnvItemDetCurrent = "";
        }

        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("BeanEnvItemDetCurrent", lsBeanEnvItemDetCurrent);
        loEditor.commit();
    }

    public BeanKPI getCurrentKpi() {

        if (currentKpi == null) {
            currentKpi = new BeanKPI();
        }

        return currentKpi;
    }

    public void setCurrentKpi(BeanKPI currentKpi) {

        this.currentKpi = currentKpi;
    }

    /**
     * @return the currentCodFlujo
     */
    public int getCurrentCodFlujo() {

        if (currentCodFlujo == 0)
            currentCodFlujo = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getInt(
                    "currentCodFlujo", 0);

        return currentCodFlujo;
    }

    /**
     * @param currentCodFlujo the currentCodFlujo to set
     */
    public void setCurrentCodFlujo(int currentCodFlujo) {
        this.currentCodFlujo = currentCodFlujo;

        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putInt("currentCodFlujo", currentCodFlujo);
        loEditor.commit();
    }

    /**
     * @return the clienteOnline
     */
    public int getClienteOnline() {
        if (clienteOnline == 0)
            clienteOnline = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getInt(
                    "clienteOnline", 0);
        return clienteOnline;
    }

    /**
     * @param clienteOnline the clienteOnline to set
     */
    public void setClienteOnline(int clienteOnline) {
        this.clienteOnline = clienteOnline;

        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putInt("clienteOnline", clienteOnline);
        loEditor.commit();
    }

    public List<BeanArticulo> getCurrentlistaArticulo() {
        return currentlistaArticulo;
    }

    public void setCurrentlistaArticulo(List<BeanArticulo> currentlistaArticulo) {
        this.currentlistaArticulo = currentlistaArticulo;
    }

    public BeanConsultaProducto getCurrentConsultaProducto() {
        return currentConsultaProducto;
    }

    public void setCurrentConsultaProducto(BeanConsultaProducto currentConsultaProducto) {
        this.currentConsultaProducto = currentConsultaProducto;
    }

    public BeanValidaPedidoAndroid getCurrentValidaPedido() {
        return currentValidaPedido;
    }

    public void setCurrentValidaPedido(BeanValidaPedidoAndroid currentValidaPedido) {
        this.currentValidaPedido = currentValidaPedido;
    }

    public void setCurrentPedido(BeanEnvPedidoCab currentPedido) {
        this.currentPedido = currentPedido;

        String lsBeanPedidoCurrent = "";
        try {
            lsBeanPedidoCurrent = BeanMapper.toJson(currentPedido, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            lsBeanPedidoCurrent = "";
        }

        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("BeanPedidoCurrent", lsBeanPedidoCurrent);
        loEditor.commit();
    }

    public BeanEnvPedidoCab getCurrentPedido() {
        if (currentPedido == null) {
            String lsBeanPedidoCurrent = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE)
                    .getString("BeanPedidoCurrent", "");
            if (!lsBeanPedidoCurrent.equals(""))
                currentPedido = (BeanEnvPedidoCab) BeanMapper.fromJson(
                        lsBeanPedidoCurrent, BeanEnvPedidoCab.class);
        }
        return currentPedido;
    }

    public BeanRespVerificarPedidoGeneral getCurrentRespVerificarPedidoGeneral() {
        if (currentRespVerificarPedidoGeneral == null) {
            String lsBeanRespVerificarPedidoGeneral = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE)
                    .getString("BeanRespVerificarPedidoGeneral", "");

            if (!lsBeanRespVerificarPedidoGeneral.equals(""))
                currentRespVerificarPedidoGeneral = (BeanRespVerificarPedidoGeneral) BeanMapper.fromJson(
                        lsBeanRespVerificarPedidoGeneral, new TypeReference<List<BeanRespVerificarPedidoGeneral>>() {
                        }
                );
        }
        return currentRespVerificarPedidoGeneral;
    }

    public void setCurrentRespVerificarPedidoGeneral(BeanRespVerificarPedidoGeneral currentRespVerificarPedidoGeneral) {
        this.currentRespVerificarPedidoGeneral = currentRespVerificarPedidoGeneral;
        String lsBeanRespVerificarPedidoGeneral = "";

        try {
            lsBeanRespVerificarPedidoGeneral = BeanMapper.toJson(currentRespVerificarPedidoGeneral, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("BeanRespVerificarPedidoGeneral", lsBeanRespVerificarPedidoGeneral);
        loEditor.commit();
    }

    public BeanRespListaEfecienciaOnline getCurrentRespListaEfecienciaOnline() {
        if (currentRespListaEfecienciaOnline == null) {
            String lsBeanRespListaEficienciaOnline = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE)
                    .getString("BeanRespListaEfecienciaOnline", "");

            if (!lsBeanRespListaEficienciaOnline.equals(""))
                currentRespListaEfecienciaOnline = (BeanRespListaEfecienciaOnline) BeanMapper.fromJson(
                        lsBeanRespListaEficienciaOnline, new TypeReference<BeanRespListaEfecienciaOnline>() {
                        }
                );
        }
        return currentRespListaEfecienciaOnline;
    }

    public void setCurrentRespListaEfecienciaOnline(BeanRespListaEfecienciaOnline currentRespListaEfecienciaOnline) {
        this.currentRespListaEfecienciaOnline = currentRespListaEfecienciaOnline;
        String lsBeanRespListaEficienciaOnline = "";

        try {
            lsBeanRespListaEficienciaOnline = BeanMapper.toJson(currentRespListaEfecienciaOnline, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("BeanRespListaEfecienciaOnline", lsBeanRespListaEficienciaOnline);
        loEditor.commit();
    }

    public boolean isPrimeraVez() {

        primeraVez = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getBoolean(
                "PrimeraVez", true);


        return primeraVez;
    }

    public void setPrimeraVez(boolean primeraVez) {

        this.primeraVez = primeraVez;

        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putBoolean("PrimeraVez", primeraVez);
        loEditor.commit();
    }

    //Verificacion de version
    public static interface AplicationNextelListener {
        public void finishActivity();

        public void startFromActivity(final Class<?> klass, String message);
    }

    public static void subMostrarNotificacionVersion(Context context, BeanUsuario _usuario) throws Exception {
        if (_usuario.getCurrentVersion() != null
                && !_usuario.getCurrentVersion().equals("0.0.0")
                && !Utilitario.fnVersion(context).contains(_usuario.getCurrentVersion())) {
            String _message = context.getString(R.string.notifacation_newversion).replace("{0}", _usuario.getCurrentVersion());
            NotificationCompat.Builder loBuilder = new NotificationCompat.Builder(context)
                    .setTicker(_message)
                    .setAutoCancel(true)
                    .setContentText(_message)
                    .setContentTitle(context.getString(R.string.app_name))
                    .setWhen(System.currentTimeMillis())
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(_message));
            Intent loIntent = new Intent();
            loIntent.setClass(context, ActPreferencias.class);
            loIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            loIntent.putExtra(context.getString(R.string.sp_permiso),
                    EnumRolPreferencia.USUARIO.ordinal());
            loBuilder.setSmallIcon(Utilitario.obtainResourceId(context,
                    R.style.Theme_Pedidos_Master,
                    EnumTheme.getCurrentTheme(context).getAttrRes(),
                    R.attr.NServicesNotificationIconAdvertencia));
            PendingIntent loPendingIntent = PendingIntent.getActivity(context, 0, loIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            loBuilder.setContentIntent(loPendingIntent);

            ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(R.string.notifacation_newversion, loBuilder.build());
        }
    }

    public void verifyCurrentVersion(Context context, AplicationNextelListener listener) throws Exception {
        BeanUsuario _usuario = currentUsuario;
        if (_usuario.getCurrentVersion() != null
                && !_usuario.getCurrentVersion().equals("0.0.0")
                && !Utilitario.fnVersion(context).contains(_usuario.getCurrentVersion())) {
            AplicacionEntel.subMostrarNotificacionVersion(getApplicationContext(), _usuario);
            if (listener != null)
                listener.startFromActivity(getHomeActivityClass(), context.getString(R.string.toast_debeactualizaraplicacion).replace("{0}", _usuario.getCurrentVersion()));
            if (listener != null)
                listener.finishActivity();
        }
    }

    public BeanRespConsultaPedido getCurrentRespConsultaPedido() {
        return currentRespConsultaPedido;
    }

    public void setCurrentRespConsultaPedido(BeanRespConsultaPedido currentRespConsultaPedido) {
        this.currentRespConsultaPedido = currentRespConsultaPedido;
    }


    public void activarRecuperarPedido(boolean flag) {
        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("flagRecuperarPedido", (flag) ? Configuracion.FLGVERDADERO : Configuracion.FLGFALSO);
        loEditor.commit();

    }

    public String getActivarRecuperarPedido() {
        return getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "flagRecuperarPedido", "");
    }

    public void activarRecalcularMontoPedido(boolean flag) {
        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("flagRecalcularMontoPedido", (flag) ? Configuracion.FLGVERDADERO : Configuracion.FLGFALSO);
        loEditor.commit();

    }

    public String getActivarRecalcularMontoPedido() {
        return getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "flagRecalcularMontoPedido", "");
    }

    public BeanUsuarioOnline getCurrentRecibo() {
        return currentRecibo;
    }

    public void setCurrentRecibo(BeanUsuarioOnline currentRecibo) {
        this.currentRecibo = currentRecibo;
    }

    /**
     * @return the currentPago
     */
    public BeanEnvDocumento getCurrentDocumento() {

        if (currentDocumento == null) {
            String lsBeanEnvDocumento = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE)
                    .getString("BeanEnvDocumentoCurrent", "");
            if (!lsBeanEnvDocumento.equals(""))
                currentDocumento = (BeanEnvDocumento) BeanMapper.fromJson(lsBeanEnvDocumento,
                        BeanEnvDocumento.class);
        }

        return currentDocumento;
    }

    public void setCurrentDocumento(BeanEnvDocumento currentDocumento) {
        this.currentDocumento = currentDocumento;

        String lsBeanEnvDocumento = "";
        try {
            lsBeanEnvDocumento = BeanMapper.toJson(currentDocumento, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            lsBeanEnvDocumento = "";
        }

        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("BeanEnvDocumentoCurrent", lsBeanEnvDocumento);
        loEditor.commit();
    }

    /**
     * @return the currentNoCobranza
     */
    public BeanEnvNoCobranza getCurrentNoCobranza() {

        if (currentNoCobranza == null) {
            String lsBeanEnvNoCobranza = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE)
                    .getString("BeanEnvNoCobranzaCurrent", "");
            if (!lsBeanEnvNoCobranza.equals(""))
                currentNoCobranza = (BeanEnvNoCobranza) BeanMapper.fromJson(lsBeanEnvNoCobranza,
                        BeanEnvNoCobranza.class);
        }

        return currentNoCobranza;
    }

    public void setCurrentNoCobranza(BeanEnvNoCobranza currentNoCobranza) {
        this.currentNoCobranza = currentNoCobranza;

        String lsBeanEnvNoCobranza = "";
        try {
            lsBeanEnvNoCobranza = BeanMapper.toJson(currentNoCobranza, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            lsBeanEnvNoCobranza = "";
        }

        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("BeanEnvNoCobranzaCurrent", lsBeanEnvNoCobranza);
        loEditor.commit();
    }
}