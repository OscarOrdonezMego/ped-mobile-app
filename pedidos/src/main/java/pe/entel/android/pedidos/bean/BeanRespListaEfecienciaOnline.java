package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Created by tkongr on 14/10/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanRespListaEfecienciaOnline {

    @JsonProperty
    private String resultado = "";
    @JsonProperty
    private int idResultado = 0;
    @JsonProperty
    private List<BeanEficienciaOnline> listaEficiencia;

    @JsonProperty
    private String usuario = "";
    @JsonProperty
    private List<String> usuariosVendedores;
    @JsonProperty
    private BeanKPI kpi;

    public BeanRespListaEfecienciaOnline() {
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public int getIdResultado() {
        return idResultado;
    }

    public void setIdResultado(int idResultado) {
        this.idResultado = idResultado;
    }

    public List<BeanEficienciaOnline> getListaEficiencia() {
        return listaEficiencia;
    }

    public void setListaEficiencia(List<BeanEficienciaOnline> listaEficiencia) {
        this.listaEficiencia = listaEficiencia;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public List<String> getUsuariosVendedores() {
        return usuariosVendedores;
    }

    public void setUsuariosVendedores(List<String> usuariosVendedores) {
        this.usuariosVendedores = usuariosVendedores;
    }

    public BeanKPI getKpi() {
        return kpi;
    }

    public void setKpi(BeanKPI kpi) {
        this.kpi = kpi;
    }
}
