package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.LinkedList;
import java.util.List;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanSucursal;

/**
 * Created by tkongr on 07/01/2016.
 */
public class DalSucursal {

    private Context ioContext;

    public DalSucursal(Context psClase) {
        ioContext = psClase;
    }

    public List<BeanSucursal> fnSelSucursales() {
        List<BeanSucursal> loLista = new LinkedList<BeanSucursal>();
        SQLiteDatabase myDB = ioContext.openOrCreateDatabase(ioContext.getString(R.string.db_name), Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"IDSUCURSAL", "CODIGO", "DESCRIPCION", "URL"};
        String lsTablas = "TBL_SUCURSAL";

        Cursor loCursor = myDB.query(lsTablas, laColumnas, null, null, null, null, "IDSUCURSAL");

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    BeanSucursal beanSucursal = new BeanSucursal();
                    beanSucursal.setIdSucursal(loCursor.getString(0));
                    beanSucursal.setCodigo(loCursor.getString(1));
                    beanSucursal.setNombre(loCursor.getString(2));
                    beanSucursal.setUrl(loCursor.getString(3));
                    loLista.add(beanSucursal);
                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loLista;
    }

    public String fnSelURLSucursal(String codSucursal) {

        SQLiteDatabase myDB = null;
        String IP_SERVER_SUCURSAL = "";

        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"URL"};

        String lsTablas = "TBL_SUCURSAL";
        String lsWhere = "CODIGO= '" + String.valueOf(codSucursal) + "'";

        Cursor loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    IP_SERVER_SUCURSAL = loCursor.getString(0);
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return IP_SERVER_SUCURSAL;
    }

}
