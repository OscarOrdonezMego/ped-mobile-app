package pe.entel.android.pedidos.bean;

import android.util.Log;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.pedidos.util.Configuracion;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanEnvCanjeCab {
    private String canpk;
    @JsonProperty
    private String idCuenta;
    @JsonProperty
    private String idCliente;
    @JsonProperty
    private String idUsuario;
    @JsonProperty
    private String numeroDocumento = "";
    @JsonProperty
    private String latitud;
    @JsonProperty
    private String longitud;
    @JsonProperty
    private String fechaMovil;
    @JsonProperty
    private String celda;
    @JsonProperty
    private int errorConexion;
    @JsonProperty
    private int errorPosicion;
    @JsonProperty
    private List<BeanEnvItemDet> lstCanjeDet;
    @JsonProperty
    private String flgEnCobertura;
    @JsonProperty
    private String tipoArticulo;

    public BeanEnvCanjeCab() {
        canpk = "";
        idCuenta = "";
        idCliente = "";
        idUsuario = "";
        numeroDocumento = "";
        latitud = "0";
        longitud = "0";
        fechaMovil = "";
        celda = "";
        errorConexion = 0;
        errorPosicion = 0;
        setLstCanjeDet(new ArrayList<BeanEnvItemDet>());
        flgEnCobertura = "1";
        tipoArticulo = Configuracion.TipoArticulo.PRODUCTO;
    }

    public void agregarProductoCanje(BeanEnvItemDet dCanje) {
        lstCanjeDet.add(dCanje);
    }

    public void actualizarProductoCanje(BeanEnvItemDet dCanje) {
        BeanEnvItemDet bean = null;

        for (int i = lstCanjeDet.size() - 1; i >= 0; i--) {
            bean = (BeanEnvItemDet) lstCanjeDet.get(i);

            if (bean.getIdArticulo().equals(dCanje.idArticulo)) {
                lstCanjeDet.set(i, dCanje);
                return;
            }
        }
        lstCanjeDet.add(dCanje);
    }

    public String calcularCantTotalItems() {
        double total = 0.0;
        BeanEnvItemDet bean = null;

        for (int i = lstCanjeDet.size() - 1; i >= 0; i--) {
            bean = (BeanEnvItemDet) lstCanjeDet.get(i);
            total = total + Double.parseDouble(bean.getCantidad());
        }

        return String.valueOf(total);
    }


    public void calcularMontos() {
    }

    public boolean existeProducto(String idProducto) {

        boolean flg = false;
        BeanEnvItemDet bean = null;

        for (int i = lstCanjeDet.size() - 1; i >= 0; i--) {
            bean = (BeanEnvItemDet) lstCanjeDet.get(i);

            if (bean.getIdArticulo().equals(String.valueOf(idProducto))) {
                flg = true;
                break;
            }
        }

        return flg;

    }

    public BeanEnvItemDet obtenerDetalle(long idProducto) {
        Log.v("XXZ", "" + idProducto);
        BeanEnvItemDet bean = null;

        for (int i = lstCanjeDet.size() - 1; i >= 0; i--) {
            bean = (BeanEnvItemDet) lstCanjeDet.get(i);

            if (bean.getIdArticulo().equals(String.valueOf(idProducto))) {
                Log.v("XXZ", bean.getIdArticulo());
                return bean;
            }
        }
        return null;
    }

    public void eliminarProducto(long idProducto) {

        BeanEnvItemDet bean = null;

        for (int i = lstCanjeDet.size() - 1; i >= 0; i--) {
            bean = (BeanEnvItemDet) lstCanjeDet.get(i);

            if (bean.getIdArticulo().equals(String.valueOf(idProducto))) {
                lstCanjeDet.remove(i);
                break;
            }
        }
    }

    private double round(double d) {
        return (Math.floor(d * 100.0d + 0.5d) / 100.0d);
    }

    /**
     * @return the canpk
     */
    public String getCanpk() {
        return canpk;
    }

    /**
     * @param canpk the canpk to set
     */
    public void setCanpk(String canpk) {
        this.canpk = canpk;
    }

    /**
     * @return the idCuenta
     */
    public String getIdCuenta() {
        return idCuenta;
    }

    /**
     * @param idCuenta the idCuenta to set
     */
    public void setIdCuenta(String idCuenta) {
        this.idCuenta = idCuenta;
    }

    /**
     * @return the idCliente
     */
    public String getIdCliente() {
        return idCliente;
    }

    /**
     * @param idCliente the idCliente to set
     */
    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    /**
     * @return the idUsuario
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the numeroDocumento
     */
    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    /**
     * @param numeroDocumento the numeroDocumento to set
     */
    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    /**
     * @return the latitud
     */
    public String getLatitud() {
        return latitud;
    }

    /**
     * @param latitud the latitud to set
     */
    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    /**
     * @return the longitud
     */
    public String getLongitud() {
        return longitud;
    }

    /**
     * @param longitud the longitud to set
     */
    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    /**
     * @return the fechaMovil
     */
    public String getFechaMovil() {
        return fechaMovil;
    }

    /**
     * @param fechaMovil the fechaMovil to set
     */
    public void setFechaMovil(String fechaMovil) {
        this.fechaMovil = fechaMovil;
    }

    /**
     * @return the celda
     */
    public String getCelda() {
        return celda;
    }

    /**
     * @param celda the celda to set
     */
    public void setCelda(String celda) {
        this.celda = celda;
    }

    /**
     * @return the errorConexion
     */
    public int getErrorConexion() {
        return errorConexion;
    }

    /**
     * @param errorConexion the errorConexion to set
     */
    public void setErrorConexion(int errorConexion) {
        this.errorConexion = errorConexion;
    }

    /**
     * @return the errorPosicion
     */
    public int getErrorPosicion() {
        return errorPosicion;
    }

    /**
     * @param errorPosicion the errorPosicion to set
     */
    public void setErrorPosicion(int errorPosicion) {
        this.errorPosicion = errorPosicion;
    }

    /**
     * @return the lstCanjeDet
     */
    public List<BeanEnvItemDet> getLstCanjeDet() {
        return lstCanjeDet;
    }

    /**
     * @param lstCanjeDet the lstCanjeDet to set
     */
    public void setLstCanjeDet(List<BeanEnvItemDet> lstCanjeDet) {
        this.lstCanjeDet = lstCanjeDet;
    }

    public String getFlgEnCobertura() {
        return flgEnCobertura;
    }

    public void setFlgEnCobertura(String flgEnCobertura) {
        this.flgEnCobertura = flgEnCobertura;
    }

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }
}