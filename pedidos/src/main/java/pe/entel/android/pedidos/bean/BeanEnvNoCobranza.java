package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonProperty;

public class BeanEnvNoCobranza {
    @JsonProperty
    private String idDocTipoMotivo;
    @JsonProperty
    private String fechaMovil;
    @JsonProperty
    private String idUsuario;
    @JsonProperty
    private String idCliente;
    @JsonProperty
    private String latitud;
    @JsonProperty
    private String longitud;
    @JsonProperty
    private String observacion;

    public String getIdDocTipoMotivo() {
        return idDocTipoMotivo;
    }

    public void setIdDocTipoMotivo(String idDocTipoMotivo) {
        this.idDocTipoMotivo = idDocTipoMotivo;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getFechaMovil() {
        return fechaMovil;
    }

    public void setFechaMovil(String fechaMovil) {
        this.fechaMovil = fechaMovil;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}
