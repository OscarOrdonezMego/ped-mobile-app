package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanClienteDireccion {

    @JsonProperty
    private String codInc = "";
    @JsonProperty
    private String pk = "";
    @JsonProperty
    private String codCliente = "";
    @JsonProperty
    private String codDireccion;
    @JsonProperty
    private String nombre = "";
    @JsonProperty
    private String latitud = "";
    @JsonProperty
    private String longitud = "";
    @JsonProperty
    private String tipo = "";
    @JsonProperty
    private String flgEnviado = "";
    @JsonProperty
    private String fechaCreacion = "";
    @JsonProperty
    private int error = -1;
    @JsonProperty
    private String mensaje = "";

    public BeanClienteDireccion() {

    }

    public String getHtttpResponseObject() {
        return this.error + ";" + this.mensaje;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    public String getCodDireccion() {
        return codDireccion;
    }

    public void setCodDireccion(String codDireccion) {
        this.codDireccion = codDireccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFlgEnviado() {
        return flgEnviado;
    }

    public void setFlgEnviado(String flgEnviado) {
        this.flgEnviado = flgEnviado;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getCodInc() {
        return codInc;
    }

    public void setCodInc(String codInc) {
        this.codInc = codInc;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return this.nombre;
    }
}
