package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanAlmacen;

public class AdapPopupBeanAlmacen extends ArrayAdapter<BeanAlmacen> {

    int resource;
    Context context;

    List<BeanAlmacen> originalList;

    BeanAlmacen ioItem;

    int cont = 0;

    public AdapPopupBeanAlmacen(Context poContext, int piResource,
                                List<BeanAlmacen> poLista, BeanAlmacen poItem) {
        super(poContext, piResource, poLista);
        originalList = new ArrayList<BeanAlmacen>();
        originalList.addAll(poLista);
        resource = piResource;
        context = poContext;
        ioItem = poItem;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BeanAlmacen item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        ((TextView) nuevaVista.findViewById(R.id.listapopup_item_txtNombre))
                .setText(item.getNombre());

        if (ioItem != null)
            ((TextView) nuevaVista
                    .findViewById(R.id.listapopup_item_txtNombre))
                    .setEnabled(!ioItem.getCodigo().equals(item.getCodigo()));
        else
            ((TextView) nuevaVista
                    .findViewById(R.id.listapopup_item_txtNombre))
                    .setEnabled(true);
        return nuevaVista;
    }

}
