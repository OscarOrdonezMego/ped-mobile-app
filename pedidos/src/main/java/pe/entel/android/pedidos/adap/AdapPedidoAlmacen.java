package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanAlmacen;

public class AdapPedidoAlmacen extends
        ArrayAdapter<BeanAlmacen> {
    int resource;
    Context context;

    List<BeanAlmacen> originalList;

    int cont = 0;

    public AdapPedidoAlmacen(Context _context, int _resource,
                             List<BeanAlmacen> lista) {
        super(_context, _resource, lista);
        originalList = lista;
        resource = _resource;
        context = _context;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BeanAlmacen item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        ((TextView) nuevaVista
                .findViewById(R.id.actpedidoalmacen_textTitulo))
                .setText(item.getNombre());

        return nuevaVista;
    }

}
