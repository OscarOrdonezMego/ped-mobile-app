package pe.entel.android.pedidos.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanConsulta;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumUrl;

public class HttpPedidoRealizado extends HttpConexion {
    private BeanConsulta ioBeanConsulta;
    private Context ioContext;

    // CONSTRUCTOR
    public HttpPedidoRealizado(BeanConsulta poBeanConsulta, Context poContext, EnumTipActividad poEnumTipActividad) {
        super(poContext, poContext
                .getString(R.string.hhtppedidorealizado_dlgconexion), poEnumTipActividad);
        ioBeanConsulta = poBeanConsulta;
        ioContext = poContext;
    }

    @Override
    public boolean OnPreConnect() {
        if (!Utilitario.fnVerSignal(ioContext)) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            BeanConsulta loBeanConsulta = (BeanConsulta) getHttpResponseObject();
            int liIdHttprespuesta = loBeanConsulta.getIdResultado();
            String lsHttpRespuesta = loBeanConsulta.getResultado();
            Log.v("URL", liIdHttprespuesta + "");

            if (liIdHttprespuesta == ConfiguracionNextel.CONSRESSERVIDOROK) {
                String lsBeanConsulta = "";
                try {
                    lsBeanConsulta = BeanMapper.toJson(loBeanConsulta, false);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                SharedPreferences loSharedPreferences = ioContext.getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
                SharedPreferences.Editor loEditor = loSharedPreferences.edit();
                loEditor.putString("BeanConsulta", lsBeanConsulta);
                loEditor.commit();
            }
            setHttpResponseIdMessage(liIdHttprespuesta, lsHttpRespuesta);
        }
    }

    public void subConHttp() {
        try {
            //RestTemplate restTemplate = new RestTemplate();
            //restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            Log.v("URL", Configuracion.fnUrl(ioContext, EnumUrl.BUSQONLINE));
            Log.v("URL", BeanMapper.toJson(ioBeanConsulta, false));
            //String lsObjeto = restTemplate.postForObject(Configuracion.fnUrl(ioContext, EnumUrl.BUSQONLINE), ioBeanConsulta, String.class);
            String lsObjeto = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, EnumUrl.BUSQONLINE), ioBeanConsulta);
            BeanConsulta loBeanConsulta = (BeanConsulta) BeanMapper.fromJson(lsObjeto, BeanConsulta.class);
            setHttpResponseObject(loBeanConsulta);
        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, "Error HTTP: " + e.getMessage());
        }
    }
}
