package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanRespPedidoOnline {
    @JsonProperty
    private String resultado = "";
    @JsonProperty
    private int idResultado = 0;
    @JsonProperty
    private BeanEnvPedidoCab pedido;

    public BeanRespPedidoOnline() {
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public int getIdResultado() {
        return idResultado;
    }

    public void setIdResultado(int idResultado) {
        this.idResultado = idResultado;
    }

    public BeanEnvPedidoCab getPedido() {
        return pedido;
    }

    public void setPedido(BeanEnvPedidoCab loPedido) {
        this.pedido = loPedido;
    }

}
