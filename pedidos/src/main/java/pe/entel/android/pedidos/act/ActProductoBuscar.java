package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import java.util.ArrayList;
import java.util.List;

import greendroid.widget.ActionBarItem;
import greendroid.widget.item.ThumbnailItem;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.actividad.NexInvalidOperatorActivity.NexInvalidOperatorException;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.adap.AdapProductoBuscarConStock;
import pe.entel.android.pedidos.adap.AdapProductoBuscarSinStock;
import pe.entel.android.pedidos.bean.BeanArticulo;
import pe.entel.android.pedidos.bean.BeanArticuloOnline;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanMostrarProducto;
import pe.entel.android.pedidos.bean.BeanPresentacion;
import pe.entel.android.pedidos.bean.BeanProducto;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalAlmacen;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.dal.DalProducto;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpProductoOnline;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumConfiguracion;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

public class ActProductoBuscar extends NexActivity implements
        RadioGroup.OnCheckedChangeListener, OnItemClickListener, TextWatcher,
        NexMenuCallbacks, MenuSlidingListener {

    private EditText txtTexto;
    private Button btnBuscar;
    private RadioGroup rbtGruFiltro;
    private RadioButton rbtCodigo;
    private RadioButton rbtNombre;
    private ListView lstPedidos;
    private LinearLayout lnProgress;

    private final int CONSBACK = 1;
    private final int CONSICOVALIDAR = 2;
    private final int CONSPRODUCTOAGREGADO = 3;
    private final int CONSOTRO = -1;

    List<BeanArticulo> listaConStock = new ArrayList<BeanArticulo>();
    List<BeanMostrarProducto> listaSinStock = new ArrayList<BeanMostrarProducto>();

    AdapProductoBuscarConStock adapterConStock;
    AdapProductoBuscarSinStock adapterSinStock;

    private String codCondVta = "";
    BeanEnvPedidoCab loBeanPedidoCab = null;

    private AdapMenuPrincipalSliding _menu;

    BeanUsuario loBeanUsuario = null;

    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDAHOME = 11;
    private final int VALIDASINCRONIZACION = 12;
    private final int ENVIOPENDIENTES = 13;
    private final int VALIDASINCRO = 14;
    private final int CONSSALIR = 15;
    private final int NOHAYPENDIENTES = 16;

    boolean estadoSincronizar = false;
    int estadoSalir = 0;

    // Integracion Ntrack
    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro

    //PARAMETROS DE CONFIGURACION
    private String isTipoCliente;
    private String isPrecionCondVenta;
    private String isSimboloMoneda;
    private String isSimboloMonedaDolar;
    private String isNumeroDecimales;
    private String isMostrarAlmacen;
    private String isStock;
    private String isValidarPrecio;
    private String isPresentacion;
    private String sharePreferenceBeanPedidoCab;
    private String isValidarStock;
    TaskLlenarLista ioTaskLlenarLista;

    String flgTipoBusqueda;
    private String isBusquedaNumericaArticulo;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        ValidacionServicioUtil.validarServicio(this);
        int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();

        if (flujo != Configuracion.CONSSTOCK) {
            String lsBeanVisitaCab = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("beanPedidoCabVerificarPedido", "");
            sharePreferenceBeanPedidoCab = "beanPedidoCabVerificarPedido";

            if (lsBeanVisitaCab.equals("")) {
                lsBeanVisitaCab = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("beanPedidoCab", "");
                sharePreferenceBeanPedidoCab = "beanPedidoCab";
            }

            loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper.fromJson(
                    lsBeanVisitaCab, BeanEnvPedidoCab.class);

            if (loBeanPedidoCab != null)
                codCondVta = loBeanPedidoCab.getCondicionVenta();
        }

        String lsBeanRepresentante = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanRepresentante, BeanUsuario.class);

        isSimboloMoneda = EnumConfiguracion.SIMBOLO_MONEDA.getValor(this);
        isSimboloMonedaDolar = "$.";
        isNumeroDecimales = EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(this);

        isTipoCliente = EnumConfiguracion.PRECIO_TIPO_CLIENTE.getValor(this);
        isPrecionCondVenta = EnumConfiguracion.PRECIO_CONDICION_VENTA.getValor(this);

        isMostrarAlmacen = Configuracion.EnumConfiguracion.MOSTRAR_ALMACEN.getValor(this);
        isStock = EnumConfiguracion.STOCK.getValor(this);
        isValidarStock = EnumConfiguracion.VALIDAR_STOCK.getValor(this);
        isValidarPrecio = EnumConfiguracion.VALIDAR_PRECIO.getValor(this);
        isPresentacion = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(this);

        isBusquedaNumericaArticulo = EnumConfiguracion.BUSQUEDA_NUMERICA_ARTICULOS.getValor(this);

        setUsingNexMenuDrawerLayout(this);

        super.onCreate(savedInstanceState);

        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));

        subIniMenu();

    }

    @Override
    protected void subIniActionBar() {
        this.getActionBarGD().setTitle(
                getString(R.string.actproductobuscar_bartitulo));
    }

    @SuppressWarnings("unused")
    @Override
    protected void subSetControles() {
        setActionBarContentView(R.layout.productobuscar2);

        btnBuscar = (Button) findViewById(R.id.actproductobuscar_btnAceptar);
        txtTexto = (EditText) findViewById(R.id.actproductobuscar_txtTexto);
        rbtGruFiltro = (RadioGroup) findViewById(R.id.actproductobuscar_rbtGruFiltro);
        rbtCodigo = (RadioButton) findViewById(R.id.actproductobuscar_rbtCodigo);
        rbtNombre = (RadioButton) findViewById(R.id.actproductobuscar_rbtNombre);
        lstPedidos = (ListView) findViewById(R.id.actproductobuscar_lista);
        lnProgress = (LinearLayout) findViewById(R.id.actproductobuscar_lnProgress);

        rbtGruFiltro.setOnCheckedChangeListener(this);
        lstPedidos.setOnItemClickListener(this);
        txtTexto.addTextChangedListener(this);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                subBuscar();
            }
        });

        txtTexto.requestFocus();

        int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();

        if (flujo != Configuracion.CONSSTOCK) {
            btnBuscar.setVisibility(View.GONE);
            ioTaskLlenarLista = new TaskLlenarLista();
            ioTaskLlenarLista.execute();
        } else if (((AplicacionEntel) getApplication()).getCurrentlistaArticulo() != null) {

            listaConStock = ((AplicacionEntel) getApplication()).getCurrentlistaArticulo();
            ioTaskLlenarLista = new TaskLlenarLista();
            ioTaskLlenarLista.execute();
        }
    }

    private void subSetPreferenceBusqueda() {

        String preferenceBusqueda = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("TipoBusqueda", "");

        if (!preferenceBusqueda.equals(Configuracion.CONSTANTE_BUSQUEDA_CODIGO)) {
            //Búsqueda por nombre
            rbtNombre.setChecked(true);
            txtTexto.setInputType(InputType.TYPE_CLASS_TEXT);
        } else {
            //Búsqueda por codigo
            rbtCodigo.setChecked(true);
            if (isBusquedaNumericaArticulo.equals(Configuracion.FLGVERDADERO)) {
                txtTexto.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_PHONE);
            } else {
                txtTexto.setInputType(InputType.TYPE_CLASS_TEXT);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (ioTaskLlenarLista != null)
            ioTaskLlenarLista.detach();
    }

    class DoneOnEditorActionListener implements OnEditorActionListener {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                subBuscar();
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
            return false;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();

        if (flujo == Configuracion.CONSSTOCK) {
            listaConStock = new ArrayList<BeanArticulo>();
            txtTexto.setText("");
        } else {
            adapterSinStock.getFilter(rbtCodigo.isChecked() ? 0 : 1).filter(txtTexto.getText().toString());
        }

        flgTipoBusqueda = "";

        if (rbtCodigo.isChecked()) {
            flgTipoBusqueda = Configuracion.CONSTANTE_BUSQUEDA_CODIGO;

            if (isBusquedaNumericaArticulo.equals(Configuracion.FLGVERDADERO)) {
                txtTexto.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_PHONE);
            } else {
                txtTexto.setInputType(InputType.TYPE_CLASS_TEXT);
            }

        } else if (rbtNombre.isChecked()) {
            flgTipoBusqueda = Configuracion.CONSTANTE_BUSQUEDA_NOMBRE;
            txtTexto.setInputType(InputType.TYPE_CLASS_TEXT);
        }

        SharedPreferences loSharedPreferences = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("TipoBusqueda", flgTipoBusqueda);
        loEditor.commit();
    }


    private class TaskLlenarLista extends AsyncTask<Void, Void, Void> {
        int iiFlujo;

        ActProductoBuscar ioActivity;

        public TaskLlenarLista() {
            attach(ActProductoBuscar.this);
        }

        public void attach(ActProductoBuscar poActivity) {
            this.ioActivity = poActivity;
        }

        public void detach() {
            this.ioActivity = null;
        }

        @Override
        protected void onPreExecute() {
            ioActivity.lnProgress.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... par) {
            iiFlujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();

            if (iiFlujo == Configuracion.CONSSTOCK) {
                //CONSULTAR STOCK
                if (!TextUtils.isEmpty(txtTexto.getText().toString())) {
                    listaConStock = ((AplicacionEntel) getApplication())
                            .getCurrentlistaArticulo();
                }

            } else if (iiFlujo == Configuracion.CONSPEDIDO) {
                //PEDIDO
                DalProducto loDalProducto = new DalProducto(ActProductoBuscar.this);
                DalAlmacen dalAlmacen = new DalAlmacen(ActProductoBuscar.this);

                if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                    if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO) && dalAlmacen.fnSelectAllPresentacion().size() > 0) {
                        listaSinStock = loDalProducto.fnListSelProductosPorAlmacenPorPresentacion(txtTexto.getText().toString(),
                                (rbtCodigo.isChecked()) ? EnumTipBusqueda.CODIGO
                                        : EnumTipBusqueda.NOMBRE, isValidarStock, isValidarPrecio);
                    } else {
                        listaSinStock = loDalProducto.fnListSelPresentacion(txtTexto.getText().toString(),
                                (rbtCodigo.isChecked()) ? EnumTipBusqueda.CODIGO
                                        : EnumTipBusqueda.NOMBRE, isValidarStock, isValidarPrecio);
                    }
                } else {
                    if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO) && dalAlmacen.fnSelectAllProducto().size() > 0)
                        listaSinStock = loDalProducto.fnListSelProductosByAlmacen(txtTexto.getText().toString(),
                                (rbtCodigo.isChecked()) ? EnumTipBusqueda.CODIGO
                                        : EnumTipBusqueda.NOMBRE, isValidarStock, isValidarPrecio);
                    else
                        listaSinStock = loDalProducto.fnListSelProductos(txtTexto.getText().toString(),
                                (rbtCodigo.isChecked()) ? EnumTipBusqueda.CODIGO
                                        : EnumTipBusqueda.NOMBRE, isValidarStock, isValidarPrecio);
                }
            } else {
                //DEVOLUCION Y CANJE
                DalProducto loDalProducto = new DalProducto(ActProductoBuscar.this);
                if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                    listaSinStock = loDalProducto.fnListSelPresentacion(txtTexto.getText().toString(),
                            (rbtCodigo.isChecked()) ? EnumTipBusqueda.CODIGO
                                    : EnumTipBusqueda.NOMBRE, isValidarStock, isValidarPrecio);
                } else {
                    listaSinStock = loDalProducto.fnListSelProductos(txtTexto.getText().toString(),
                            (rbtCodigo.isChecked()) ? EnumTipBusqueda.CODIGO
                                    : EnumTipBusqueda.NOMBRE, isValidarStock, isValidarPrecio);
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            if (ioActivity != null) {
                ioActivity.lnProgress.setVisibility(View.GONE);
                if (iiFlujo == Configuracion.CONSSTOCK) {
                    adapterConStock = new AdapProductoBuscarConStock(ActProductoBuscar.this,
                            R.layout.productobuscar_item, listaConStock, isPresentacion, isMostrarAlmacen);
                    lstPedidos.setAdapter(adapterConStock);
                } else {
                    adapterSinStock = new AdapProductoBuscarSinStock(ActProductoBuscar.this,
                            R.layout.productobuscar_item, listaSinStock, isPresentacion);

                    lstPedidos.setAdapter(adapterSinStock);
                }
                subSetPreferenceBusqueda();
            }
        }
    }

    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        DialogFragmentYesNo loDialog;
        switch (id) {
            case DIALOG_NSERVICES_DOWNLOAD:
                String lsNServices;
                if (ioDALNservices == null)
                    try {
                        ioDALNservices = DALNServices.getInstance(ActProductoBuscar.this, R.string.db_name);
                    } catch (NexInvalidOperatorException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                try {
                    lsNServices = ioDALNservices.fnSelNServicesLabel();
                } catch (Exception e) {
                    Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                    lsNServices = getString(R.string.nservices_label_default);
                }
                loDialog = DialogFragmentYesNo.newInstance(
                        getFragmentManager(),
                        DialogFragmentYesNo.TAG + DIALOG_NSERVICES_DOWNLOAD,
                        getString(R.string.nservices_consultingdownlad).replace(
                                "{0}", lsNServices),
                        EnumLayoutResource.getDefaultIconHelp(this));
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {
                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    subIniNServicesDownload(ioDALNservices.fnSelNServicesAPK());
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                            }
                        });
            case CONSICOVALIDAR:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSICOVALIDAR"),
                        getString(R.string.actproductobuscar_dlgingdatos),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                txtTexto.requestFocus();
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            case CONSOTRO:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSOTRO"),
                        getString(R.string.actclientebuscar_dlgnodatos),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                txtTexto.requestFocus();
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            case CONSPRODUCTOAGREGADO:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSPRODUCTOAGREGADO"),
                        getString(R.string.actproductoresultado_titexisteproducto),
                        getString(R.string.actproductoresultado_msgexisteproducto),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    Intent loInstent = new Intent(ActProductoBuscar.this, ActProductoBuscar.class);
                                    ioBeanExtras.setFiltro("");
                                    ioBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);

                                    String lsFiltro = "";
                                    lsFiltro = BeanMapper.toJson(ioBeanExtras, false);
                                    loInstent.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
                                    finish();
                                    startActivity(loInstent);
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.actmenu_dlgsincronizar),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    String lsBeanUsuario = getSharedPreferences("Actual", MODE_PRIVATE).getString("BeanUsuario", "");
                                    BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario, BeanUsuario.class);
                                    estadoSincronizar = true;
                                    new HttpSincronizar(loBeanUsuario.getCodVendedor(), ActProductoBuscar.this, fnTipactividad()).execute();
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            case VALIDASINCRONIZACION:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"),
                        getString(R.string.actmenuvalsincro_titulo),
                        getString(R.string.actmenuvalsincro_mensaje),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case VALIDASINCRO:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                        getString(R.string.msgregpendientesenvinfo),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case ENVIOPENDIENTES:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"),
                        getString(R.string.msgdeseaenviarpendientes),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                Intent loIntentEspera = new Intent(ActProductoBuscar.this, ActGrabaPendiente.class);
                                startActivity(loIntentEspera);
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            case CONSSALIR:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSSALIR"),
                        getString(R.string.actproductopedido_dlgsalir),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                salir();
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                            }
                        });
            default:
                return super.onCreateNexDialog(id);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case NOHAYPENDIENTES:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case CONSICOVALIDAR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.actproductobuscar_dlgingdatos))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        txtTexto.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;
            case CONSOTRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.actclientebuscar_dlgnodatos))

                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        txtTexto.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;

            case CONSPRODUCTOAGREGADO:
                AlertDialog loAlertDialog1 = null;
                loAlertDialog1 = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actproductoresultado_titexisteproducto))
                        .setMessage(getString(R.string.actproductoresultado_msgexisteproducto))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        Intent loInstent = new Intent(ActProductoBuscar.this, ActProductoBuscar.class);
                                        ioBeanExtras.setFiltro("");
                                        ioBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);

                                        String lsFiltro = "";
                                        try {
                                            lsFiltro = BeanMapper.toJson(ioBeanExtras, false);
                                        } catch (Exception e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                        loInstent.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
                                        finish();
                                        startActivity(loInstent);
                                    }
                                }).create();
                return loAlertDialog1;
            case VALIDAHOME:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        Intent intentl = new Intent(ActProductoBuscar.this, ActMenu.class);
                                        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intentl);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences("Actual", MODE_PRIVATE).getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario, BeanUsuario.class);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario.getCodVendedor(),
                                                    ActProductoBuscar.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActProductoBuscar.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSSALIR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgsalir))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        salir();
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                    }
                                }).create();
                return loAlertDialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    private void subBuscar() {
        if (txtTexto.getText().toString().length() < 2) {
            showDialog(CONSICOVALIDAR);
        } else {
            int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();

            if (flujo == Configuracion.CONSSTOCK) {
                subConexionHttp();
            } else {
                ioTaskLlenarLista = new TaskLlenarLista();
                ioTaskLlenarLista.execute();
            }
        }
    }

    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        switch (item.getItemId()) {
            case CONSBACK:
                Intent loIntent = null;
                if (((AplicacionEntel) getApplication()).getCurrentCodFlujo() == Configuracion.CONSPEDIDO) {
                    loIntent = new Intent(this, ActProductoPedido.class);
                } else if (((AplicacionEntel) getApplication())
                        .getCurrentCodFlujo() == Configuracion.CONSSTOCK) {
                    loIntent = new Intent(this, ActMenu.class);
                } else {
                    loIntent = new Intent(this, ActProductoItemLista.class);
                }
                finish();
                startActivity(loIntent

                );
                break;

            default:
                return super.onHandleActionBarItemClick(item, position);
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        Intent loIntent;
        if (((AplicacionEntel) getApplication()).getCurrentCodFlujo() == Configuracion.CONSPEDIDO) {
            loIntent = new Intent(this, ActProductoPedido.class);
        } else if (((AplicacionEntel) getApplication()).getCurrentCodFlujo() == Configuracion.CONSSTOCK) {
            loIntent = new Intent(this, ActProductoListaOnline.class);
        } else {
            loIntent = new Intent(this, ActProductoItemLista.class);
        }
        finish();
        startActivity(loIntent);
    }

    private void subConexionHttp() {
        BeanArticulo loBeanArticulo = null;
        DalAlmacen dalAlmacen = new DalAlmacen(this);
        boolean existeproductoalmacen = false;

        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            loBeanArticulo = new BeanPresentacion();
            if (dalAlmacen.fnSelectAllPresentacion().size() > 0) {
                existeproductoalmacen = true;
            }
        } else {
            loBeanArticulo = new BeanProducto();
            if (dalAlmacen.fnSelectAllProducto().size() > 0) {
                existeproductoalmacen = true;
            }
        }

        if (rbtCodigo.isChecked()) {
            loBeanArticulo.setCodigo(txtTexto.getText().toString());
        } else {
            loBeanArticulo.setNombre(txtTexto.getText().toString());
        }
        if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO) && existeproductoalmacen)
            loBeanArticulo.setCodAlmacen("-1");
        else
            loBeanArticulo.setCodAlmacen("0");

        BeanArticuloOnline loBeanArticuloOnline = loBeanArticulo.fnObtenerBeanArticuloOnline();
        loBeanArticuloOnline.setTipoBusqueda(Configuracion.TipoBusqueda.PRODUCTO);
        new HttpProductoOnline(loBeanArticuloOnline, this, this.fnTipactividad()).execute();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {

        Log.i("subaccdesmensaje", "entrroo");
        if (estadoSincronizar) {
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActMenu.class);
                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }
        } else if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK
                || getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {
            ioTaskLlenarLista = new TaskLlenarLista();
            ioTaskLlenarLista.execute();
        } else {
            showDialog(CONSOTRO);
            return;
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub
        int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();

        if (flujo == Configuracion.CONSSTOCK) {
            StringBuffer mensaje = new StringBuffer();
            BeanArticulo bp = (BeanArticulo) lstPedidos.getItemAtPosition(arg2);
            mensaje.append(" C\u00F3digo: ").append(bp.getCodigo()).append('\n')
                    .append(" Nombre: ").append(bp.getNombre()).append('\n')
                    .append(" Stock : ").append(bp.getStock());

            AlertDialog.Builder ale = new AlertDialog.Builder(this);
            ale.setMessage(mensaje.toString());
            ale.setCancelable(true);
            ale.setPositiveButton("Aceptar",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            ale.show();

        } else {
            String lsId = ((BeanMostrarProducto) (lstPedidos.getItemAtPosition(arg2))).getId();

            if (!existeProducto(lsId)) {
                if (flujo == Configuracion.CONSPEDIDO) {
                    DalProducto loDalProducto = new DalProducto(this);
                    String strcodCodVenta = "";
                    String codTipoCli = "";

                    // revisamos si es que el producto tiene lista de precios
                    // para el canal del cliente

                    // VERIFICAR SI ES POR TIPO CLIENTE O POR CONDVTA

                    if (isTipoCliente.equals(Configuracion.FLGVERDADERO))
                        codTipoCli = ((AplicacionEntel) getApplication())
                                .getCurrentCliente().getTclicod();
                    else
                        codTipoCli = "";

                    if (isPrecionCondVenta.equals(Configuracion.FLGVERDADERO))
                        strcodCodVenta = codCondVta;
                    else
                        strcodCodVenta = "";

                    List<ThumbnailItem> listaPrecios = new ArrayList<ThumbnailItem>();

                    if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                        listaPrecios = loDalProducto
                                .fnSelListPrecioXCodPresentacionXCanalXCondVta(
                                        lsId, codTipoCli, strcodCodVenta,
                                        isSimboloMoneda, isSimboloMonedaDolar, isNumeroDecimales);
                    } else {
                        listaPrecios = loDalProducto
                                .fnSelListPrecioXCodProductoXCanalXCondVta(
                                        lsId, codTipoCli, strcodCodVenta,
                                        isSimboloMoneda, isSimboloMonedaDolar, isNumeroDecimales);

                    }
                    int sizeLista = listaPrecios.size();

                    if (listaPrecios.size() == 0) { // Si no tiene precios,
                        // entonces se va directamente al detalle del producto y se indica que "PrcProducto" es vacio
                        // (esto se emplea para indicar que se debe emplear el precio base del producto.
                        Intent loIntent = new Intent(this, ActProductoDetalle.class);
                        loIntent.putExtra("IdArticulo", lsId);
//						loIntent.putExtra("PrcProducto", "");
                        loIntent.putExtra("PrcProducto", "");
                        loIntent.putExtra("idListaPrecio", "");
                        finish();
                        startActivity(loIntent);
                    } else if (listaPrecios.size() == 1) { // Si tiene un precio
                        // asociado, entonces se dirige al detalle del producto y se indica ese precio en el "PrcProducto"
                        ThumbnailItem loitemprod = (ThumbnailItem) listaPrecios.get(0);
                        String lsIdPrecio = String.valueOf(loitemprod.getTag());
                        Intent loIntent = new Intent(this, ActProductoDetalle.class);
                        loIntent.putExtra("IdArticulo", lsId);
                        loIntent.putExtra("idListaPrecio", lsIdPrecio);
                        finish();
                        startActivity(loIntent);
                    } else { // Si tiene lista de precios, entonces se dirige a
                        // la pantalla de "precio lista"
                        Intent loIntent = new Intent(this, ActPrecioLista.class);
                        loIntent.putExtra("IdArticulo", lsId);
                        loIntent.putExtra("CodCondVta", codCondVta);
                        finish();
                        startActivity(loIntent);
                    }
                } else {
                    ActProductoItemDetalle.ESTADO_ACTIVITY = ActProductoItemDetalle.NUEVO_ACTIVITY;
                    Intent loIntent = new Intent(this, ActProductoItemDetalle.class);
                    loIntent.putExtra("IdArticulo", lsId);
                    finish();
                    startActivity(loIntent);
                }

            } else {
                showDialog(CONSPRODUCTOAGREGADO);
            }
        }
    }

    public boolean existeProducto(String idProducto) {
        boolean flag = false;
        int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();

        if (flujo == Configuracion.CONSPEDIDO) {
            // llamamos al beanPedidoCab
            BeanEnvPedidoCab loBeanPedidoCab = null;
            String lsBeanPedidoCab = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE)
                    .getString(sharePreferenceBeanPedidoCab, "");

            loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper.fromJson(lsBeanPedidoCab, BeanEnvPedidoCab.class);

            flag = loBeanPedidoCab.existeProducto(idProducto);
        } else if (flujo == Configuracion.CONSCANJE) {
            flag = ((AplicacionEntel) getApplication()).getCurrentCanje()
                    .existeProducto(idProducto);
        } else if (flujo == Configuracion.CONSDEVOLUC) {
            flag = ((AplicacionEntel) getApplication()).getCurrentDevolucion()
                    .existeProducto(idProducto);
        }

        return flag;
    }

    @Override
    public void afterTextChanged(Editable s) {
        // TODO Auto-generated method stub
        int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();

        if (flujo == Configuracion.CONSSTOCK && adapterConStock != null)
            adapterConStock.getFilter(rbtCodigo.isChecked() ? 0 : 1).filter(txtTexto.getText().toString());
        else if (adapterSinStock != null)
            adapterSinStock.getFilter(rbtCodigo.isChecked() ? 0 : 1).filter(
                    txtTexto.getText().toString());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // TODO Auto-generated method stub
    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario)).setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir, null);

        Button btnSalir = (Button) _salir.findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                estadoSalir = 3;
                showDialog(CONSSALIR);
            }
        });
    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            int flujo = ((AplicacionEntel) getApplication())
                    .getCurrentCodFlujo();

            if (flujo != Configuracion.CONSSTOCK)
                _menu = new AdapMenuPrincipalSliding(this, this,
                        EnumMenuPrincipalSlidingItem.INICIO.getPosition());
            else
                _menu = new AdapMenuPrincipalSliding(this, this,
                        EnumMenuPrincipalSlidingItem.STOCK.getPosition());

        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();
        if (flujo == Configuracion.CONSPEDIDO) {
            showDialog(VALIDAHOME);

        } else {
            Intent intentl = new Intent(ActProductoBuscar.this, ActMenu.class);
            intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

            startActivity(intentl);
        }

    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub
        int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();

        if (flujo == Configuracion.CONSSTOCK) {
            DalCliente loDalCliente = new DalCliente(this);

            // Verificamos si existe la tabla.
            boolean lbResult = loDalCliente.existeTable();

            if (lbResult) {
                Intent loInstent = new Intent(ActProductoBuscar.this, ActClienteBuscar.class);
                loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                BeanExtras loBeanExtras = new BeanExtras();
                loBeanExtras.setFiltro("");
                loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
                String lsFiltro = "";
                try {
                    lsFiltro = BeanMapper.toJson(loBeanExtras, false);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                loInstent.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
                finish();
                startActivity(loInstent);
            } else {
                showDialog(VALIDASINCRONIZACION);
            }
        }
    }

    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = null;
            DalPedido loDalPedido = new DalPedido(this);
            loList = loDalPedido.fnEnvioPedidosPendientes(loBeanUsuario.getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);

                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();

                showDialog(CONSDIASINCRONIZAR);
            } else {
                showDialog(VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub
        estadoSalir = 1;

        int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();

        if (flujo != Configuracion.CONSSTOCK)
            showDialog(CONSSALIR);
        else
            salir();
    }

    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();

        if (flujo != Configuracion.CONSSTOCK) {
            estadoSalir = 2;
            showDialog(CONSSALIR);
        }
    }

    private void salir() {

        if (estadoSalir == 2) {
            ((AplicacionEntel) getApplication()).setCurrentCliente(null);
            ((AplicacionEntel) getApplication()).setCurrentCodFlujo(Configuracion.CONSSTOCK);
            ((AplicacionEntel) getApplication()).setCurrentCodFlujo(Configuracion.CONSSTOCK);

            Intent loIntento = new Intent(ActProductoBuscar.this, ActProductoListaOnline.class);
            loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            BeanExtras loBeanExtras = new BeanExtras();
            loBeanExtras.setFiltro("");
            loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
            String lsFiltro = "";
            try {
                lsFiltro = BeanMapper.toJson(loBeanExtras, false);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
            finish();
            startActivity(loIntento);
        } else if (estadoSalir == 1) {

            String lsBeanUsuario = getSharedPreferences("Actual", MODE_PRIVATE).getString("BeanUsuario", "");
            loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario, BeanUsuario.class);
            this.setMsgOk("Eficiencia Online");

            BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
            loBeanListaEficienciaOnline.setIdResultado(0);
            loBeanListaEficienciaOnline.setResultado("");
            loBeanListaEficienciaOnline.setListaEficiencia(null);
            loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

            new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

            Intent loInstent = new Intent(this, ActKpiDetalle.class);
            loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(loInstent);
        } else if (estadoSalir == 3) {
            Intent loIntentLogin = new Intent(ActProductoBuscar.this, ActLogin.class);
            loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loIntentLogin);
        }
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio().equals(Configuracion.FLGVERDADERO));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(
                    getString(R.string.ml_sincronizarantes)));
        }

    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */
    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this, R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

}