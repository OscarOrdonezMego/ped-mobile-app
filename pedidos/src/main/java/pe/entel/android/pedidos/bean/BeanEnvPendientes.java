package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanEnvPendientes {
    @JsonProperty
    private List<BeanEnvPedidoCab> listPedidos;
    @JsonProperty
    private List<BeanEnvPago> listPagos;
    @JsonProperty
    private List<BeanEnvCanjeCab> listCanjes;
    @JsonProperty
    private List<BeanEnvDevolucionCab> listDevoluciones;
    @JsonProperty
    private List<BeanClienteDireccion> listDirecciones;
    @JsonProperty
    private List<BeanProspecto> listProspectos;
    @JsonProperty
    private String errorConexion;
    @JsonProperty
    private String errorPosicion;

    public BeanEnvPendientes() {
        listPedidos = new ArrayList<BeanEnvPedidoCab>();
        listPagos = new ArrayList<BeanEnvPago>();
        listCanjes = new ArrayList<BeanEnvCanjeCab>();
        listDevoluciones = new ArrayList<BeanEnvDevolucionCab>();
        listDirecciones = new ArrayList<BeanClienteDireccion>();
        listProspectos = new ArrayList<BeanProspecto>();
        errorConexion = "0";
        errorPosicion = "0";
    }

    public List<BeanEnvPedidoCab> getListPedidos() {
        return listPedidos;
    }

    public void setListPedidos(List<BeanEnvPedidoCab> listPedidos) {
        this.listPedidos = listPedidos;
    }

    public List<BeanEnvPago> getListPagos() {
        return listPagos;
    }

    public void setListPagos(List<BeanEnvPago> listPagos) {
        this.listPagos = listPagos;
    }

    public List<BeanEnvCanjeCab> getListCanje() {
        return listCanjes;
    }

    public void setListCanje(List<BeanEnvCanjeCab> listCanje) {
        this.listCanjes = listCanje;
    }

    public List<BeanEnvDevolucionCab> getListDevolucion() {
        return listDevoluciones;
    }

    public void setListDevolucion(List<BeanEnvDevolucionCab> listDevolucion) {
        this.listDevoluciones = listDevolucion;
    }

    public List<BeanEnvCanjeCab> getListCanjes() {
        return listCanjes;
    }

    public void setListCanjes(List<BeanEnvCanjeCab> listCanjes) {
        this.listCanjes = listCanjes;
    }

    public List<BeanEnvDevolucionCab> getListDevoluciones() {
        return listDevoluciones;
    }

    public void setListDevoluciones(List<BeanEnvDevolucionCab> listDevoluciones) {
        this.listDevoluciones = listDevoluciones;
    }

    public List<BeanClienteDireccion> getListDirecciones() {
        return listDirecciones;
    }

    public void setListDirecciones(List<BeanClienteDireccion> listDirecciones) {
        this.listDirecciones = listDirecciones;
    }

    public List<BeanProspecto> getListProspectos() {
        return listProspectos;
    }

    public void setListProspectos(List<BeanProspecto> listProspectos) {
        this.listProspectos = listProspectos;
    }

    public String getErrorConexion() {
        return errorConexion;
    }

    public void setErrorConexion(String errorConexion) {
        this.errorConexion = errorConexion;
    }

    public String getErrorPosicion() {
        return errorPosicion;
    }

    public void setErrorPosicion(String errorPosicion) {
        this.errorPosicion = errorPosicion;
    }
} 