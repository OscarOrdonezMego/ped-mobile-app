package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import pe.com.nextel.android.actividad.support.NexFragmentActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.Gps;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexToast;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.bean.BeanEnvNoCobranza;
import pe.entel.android.pedidos.bean.BeanEnvPago;
import pe.entel.android.pedidos.bean.BeanGeneral;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalGeneral;
import pe.entel.android.pedidos.http.HttpGrabaNoCobranza;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;
import pe.entel.android.pedidos.widget.ListaPopup;
import pe.entel.android.pedidos.widget.ListaPopup.OnItemClickPopup;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

public class ActNoCobranza extends NexFragmentActivity implements OnClickListener, NexMenuCallbacks, MenuSlidingListener, OnItemClickPopup {

    private Button spiMotivo, btnGenerar;
    private TextView txtFecVisita, txtobservacion;

    boolean estadoSincronizar = false;
    private final int CONSBACK = 3;
    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDAHOME = 11;
    private final int VALIDASINCRONIZACION = 12;
    private final int ENVIOPENDIENTES = 13;
    private final int VALIDASINCRO = 14;
    private final int CONSSALIR = 15;
    private final int NOHAYPENDIENTES = 16;
    private final int ERRORGENERAR = 17;
    private final int ERRORAFTERDATE = 18;

    int estadoSalir = 0;
    Bundle savedInstanceState;

    String fecha = "";
    private CaldroidFragment dialogCaldroidFragment;

    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;
    public static final int REQUEST_NSERVICES = 204;

    BeanEnvPago loBeanPago = null;
    BeanUsuario loBeanUsuario = null;
    BeanEnvNoCobranza loBeanNoCobranza = null;

    private AdapMenuPrincipalSliding _menu;

    private final int POPUP_MOTIVO = 1;
    private DalGeneral loDalGeneral;
    private List ioLista;
    static BeanGeneral motivo;
    public static boolean showPopupMot = false;

    long iiIdCliente;
    Date iifecha;

    //PARAMETROS DE CONFIGURACION
    private String flgGps;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);
        this.savedInstanceState = savedInstanceState;

        String lsBeanUsuario = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario,
                BeanUsuario.class);
        flgGps = Configuracion.EnumConfiguracion.GPS.getValor(this);

        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);

        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));

        subIniMenu();
    }

    @Override
    public void subSetControles() {
        setActionBarContentView(R.layout.nocobranzadetalle);
        super.subSetControles();
        btnGenerar = findViewById(R.id.actclienteeditar_btnfinalizar);
        spiMotivo = findViewById(R.id.actnocobranza_cmbmotivo);
        txtFecVisita = findViewById(R.id.actclienteeditar_txtnuevavisita);
        txtobservacion = findViewById(R.id. actpedidofin_edtobservaciones);
        spiMotivo.setOnClickListener(this);
        btnGenerar.setOnClickListener(this);
        subCargaDetalle();
    }

    private void subCargaDetalle() {
        loBeanPago = ((AplicacionEntel) getApplication()).getCurrentPago();
        loBeanNoCobranza = ((AplicacionEntel) getApplication()).getCurrentNoCobranza();
        txtFecVisita.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showDatePickerConsultaPagos();
            }
        });

        btnGenerar.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                grabar();
            }
        });
    }

    public void showDatePickerConsultaPagos() {
        dialogCaldroidFragment = new CaldroidFragment();

        TypedArray ta = obtainStyledAttributes(new int[]{R.attr.PagosPopupCalendar});
        dialogCaldroidFragment.setDrawableLogo(ta.getResourceId(0, 0));
        ta.recycle();

        dialogCaldroidFragment.setCancelable(true);
        dialogCaldroidFragment.setCaldroidListener(new CaldroidListener() {
            @Override
            public void onSelectDate(Date date, View view) {

                fecha = DateFormat.format("dd-MM-yyyy", date).toString();
                GregorianCalendar selectDate = new GregorianCalendar();
                selectDate.setTime(date);
                GregorianCalendar currentDate = new GregorianCalendar();
                currentDate.setTime(new Date());

                if (selectDate.after(currentDate)) {
                    txtFecVisita.setText(fnFecha(date));
                    iifecha = date;
                    dialogCaldroidFragment.dismiss();
                } else {
                    showDialog(ERRORAFTERDATE);
                }
            }
        });
        final String dialogTag = "CALDROID_DIALOG_FRAGMENT";
        final Bundle statebun = savedInstanceState;
        if (statebun != null) {
            dialogCaldroidFragment.restoreDialogStatesFromKey(
                    getSupportFragmentManager(), statebun,
                    "DIALOG_CALDROID_SAVED_STATE", dialogTag);
            Bundle args = dialogCaldroidFragment.getArguments();
            if (args == null) {
                args = new Bundle();
                dialogCaldroidFragment.setArguments(args);
            }

        } else {
            // Setup arguments
            Bundle bundle = new Bundle();
            // Setup dialogTitle

            dialogCaldroidFragment.setArguments(bundle);
        }

        dialogCaldroidFragment.show(getSupportFragmentManager(), dialogTag);
    }

    private String fnFecha(Date date) {

        String lsRetorno = DateFormat.format("dd/MM/yyyy", date).toString();
        return lsRetorno;

    }

    private void grabar(){
        BeanGeneral ioMotivo;
        ioMotivo = motivo;
        if(ioMotivo == null || ioMotivo.getCodigo().equals("")){
            Toast.makeText(getApplicationContext(),"Seleccione un tipo de motivo", Toast.LENGTH_LONG).show();
            return;
        }

        if(txtFecVisita.getText() == null || txtFecVisita.getText().equals("") || txtFecVisita.getText().length() == 0){
            Toast.makeText(getApplicationContext(),"Seleccione una fecha de visita", Toast.LENGTH_LONG).show();
            return;
        }

        Calendar c = Calendar.getInstance();
        Date loDate = c.getTime();
        final String sFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate).toString();

        StringBuffer loStb = new StringBuffer();
        loStb.append(getString(R.string.actnocobranza_dlgfin)).append('\n');
        loStb.append(" Nva. Visita " + txtFecVisita.getText()).append('\n');
        loStb.append(" Tipo Motivo : ").append(motivo.getDescripcion()).append('\n');
        loStb.append(sFecha).append('\n');
        AlertDialog loAlertDialog = null;
        loAlertDialog = new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(getString(R.string.actnocobranza_bardetalle ))
                .setMessage(loStb.toString())

                .setPositiveButton(getString(R.string.dlg_btnsi),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                try {
                                    subGrabar();
                                    enviarServer();
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }
                        })
                .setNegativeButton(getString(R.string.dlg_btncancelar),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {

                            }
                        }).create();
        loAlertDialog.show();
    }

    private void subGrabar(){

        BeanGeneral ioMotivo;
        ioMotivo = motivo;
        loBeanNoCobranza.setIdDocTipoMotivo(ioMotivo.getCodigo());
        loBeanNoCobranza.setFechaMovil(DateFormat.format("dd/MM/yyyy kk:mm:ss", iifecha).toString());
        loBeanNoCobranza.setObservacion(txtobservacion.getText().toString());
        // Se guarda GPS si tiene el flag habilitado
        Location location = null;
        try {
            location = Gps.getLocation();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (flgGps.equals(
                Configuracion.FLGVERDADERO)
                && location != null) {
            loBeanNoCobranza.setLatitud(String.valueOf(location.getLatitude()));
            loBeanNoCobranza.setLongitud(String.valueOf(location.getLongitude()));
        } else {
            loBeanNoCobranza.setLatitud("0");
            loBeanNoCobranza.setLongitud("0");
        }
    }

    private void enviarServer(){
        ((AplicacionEntel) getApplication()).uploadEvent(Configuracion.DOCUMENTO_EVENT);
        new HttpGrabaNoCobranza(loBeanNoCobranza, this, fnTipactividad()).execute();
    }

    @Override
    public void setOnItemClickPopup(Object poObject, int piTipo) {
        // TODO Auto-generated method stub

        switch (piTipo) {
            case POPUP_MOTIVO:
                motivo = (BeanGeneral) poObject;
                spiMotivo.setText(motivo.getDescripcion());
                showPopupMot = false;
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View arg0) {
        if (arg0 == spiMotivo) {
            if (ioLista == null) {
                loDalGeneral = new DalGeneral(this);
                ioLista = loDalGeneral.fnSelGeneral(Configuracion.GENERAL10MOTNOCOBRANZA);
            }
            showPopupMot = true;

            new ListaPopup().dialog(ActNoCobranza.this, ioLista,
                    POPUP_MOTIVO, ActNoCobranza.this,
                    getString(R.string.listapopup_seleccionemotivonocobranza), motivo,
                    R.attr.PedidosPopupLista);
        }
    }

    @Override
    public void onBackPressed() {
        Intent loIntent = new Intent(this, ActClienteDetalle.class);
        loIntent.putExtra("IdCliente", iiIdCliente);
        startActivity(loIntent);
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setTitle(
                getString(R.string.actnocobranza_bardetalle));
    }

    @Override
    public void subAccDesMensaje() {

        if (estadoSincronizar) {
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActMenu.class);

                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }
        } else {
            try {
                if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK
                        || getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {
                    if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                        setResult(RESULT_OK);
                        finish();
                    }
                    else {
                        showDialog(ERRORGENERAR);
                    }
                }
            } catch (Exception e) {
                subShowException(e);
            }
        }
    }

    protected void NexShowDialog(int id) {
        pe.com.nextel.android.util.DialogFragmentYesNo loDialog;
        switch (id) {
            case VALIDAHOME:
                loDialog = pe.com.nextel.android.util.DialogFragmentYesNo.newInstance(getFragmentManager(),
                        pe.com.nextel.android.util.DialogFragmentYesNo.TAG.concat("VALIDAHOME"),
                        getString(R.string.actproductopedido_dlgback),
                        R.drawable.boton_ayuda, pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        pe.com.nextel.android.util.DialogFragmentYesNo.TAG.concat("VALIDAHOME"))
                        == pe.com.nextel.android.util.DialogFragmentYesNo.YES) {

                    Intent intentl = new Intent(ActNoCobranza.this,
                            ActMenu.class);
                    intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    startActivity(intentl);
                }
                break;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loDialog = pe.com.nextel.android.util.DialogFragmentYesNo.newInstance(getFragmentManager(),
                        pe.com.nextel.android.util.DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.actmenu_dlgsincronizar),
                        R.drawable.boton_ayuda, pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        pe.com.nextel.android.util.DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"))
                        == pe.com.nextel.android.util.DialogFragmentYesNo.YES) {
                    try {
                        String lsBeanUsuario = getSharedPreferences("Actual",
                                MODE_PRIVATE).getString("BeanUsuario", "");
                        BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                .fromJson(lsBeanUsuario, BeanUsuario.class);
                        estadoSincronizar = true;
                        new HttpSincronizar(loBeanUsuario.getCodVendedor(),
                                ActNoCobranza.this, fnTipactividad())
                                .execute();
                    } catch (Exception e) {
                        subShowException(e);
                    }
                }
                break;

            case VALIDASINCRONIZACION:
                loDialog = pe.com.nextel.android.util.DialogFragmentYesNo.newInstance(getFragmentManager(),
                        pe.com.nextel.android.util.DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"),
                        getString(R.string.actmenuvalsincro_titulo),
                        getString(R.string.actmenuvalsincro_mensaje),
                        R.drawable.boton_error, false, pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),
                        pe.com.nextel.android.util.DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"));
                break;

            case VALIDASINCRO:
                loDialog = pe.com.nextel.android.util.DialogFragmentYesNo.newInstance(getFragmentManager(),
                        pe.com.nextel.android.util.DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                        getString(R.string.msgregpendientesenvinfo),
                        R.drawable.boton_error, false, pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),
                        pe.com.nextel.android.util.DialogFragmentYesNo.TAG.concat("VALIDASINCRO"));
                break;

            case ENVIOPENDIENTES:
                loDialog = pe.com.nextel.android.util.DialogFragmentYesNo.newInstance(getFragmentManager(),
                        pe.com.nextel.android.util.DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"),
                        getString(R.string.msgdeseaenviarpendientes),
                        R.drawable.boton_ayuda, pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        pe.com.nextel.android.util.DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"))
                        == pe.com.nextel.android.util.DialogFragmentYesNo.YES) {
                    UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActNoCobranza.this);
                }
                break;

            case CONSSALIR:
                loDialog = pe.com.nextel.android.util.DialogFragmentYesNo.newInstance(getFragmentManager(),
                        pe.com.nextel.android.util.DialogFragmentYesNo.TAG.concat("CONSSALIR"),
                        getString(R.string.actproductopedido_dlgsalir),
                        R.drawable.boton_ayuda, pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        pe.com.nextel.android.util.DialogFragmentYesNo.TAG.concat("CONSSALIR"))
                        == DialogFragmentYesNo.YES) {
                    salir();
                }
                break;
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case ERRORAFTERDATE:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("INFORMACIÓN")
                        .setMessage(getString(R.string.msgdianacualafter))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDAHOME:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        Intent intentl = new Intent(
                                                ActNoCobranza.this,
                                                ActMenu.class);
                                        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                        startActivity(intentl);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences(
                                                    "Actual", MODE_PRIVATE)
                                                    .getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                                    .fromJson(lsBeanUsuario,
                                                            BeanUsuario.class);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActNoCobranza.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActNoCobranza.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSBACK:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        Intent loIntent = new Intent(ActNoCobranza.this, ActProductoPedido.class);
                                        finish();
                                        startActivity(loIntent);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case CONSSALIR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgsalir))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        salir();
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case ERRORGENERAR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titerror))
                        .setMessage(getString(R.string.actdocumentocorrelativo_dlgerrorcorrelativo))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        Intent intent = new Intent(ActNoCobranza.this, ActCobranzaMonto.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                    }
                                }).create();
                return loAlertDialog;
            default:
                return super.onCreateDialog(id);
        }

    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                estadoSalir = 3;
                showDialog(CONSSALIR);
            }
        });
    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        showDialog(VALIDAHOME);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @SuppressWarnings("deprecation")
    @Override
    public void subSincronizarFromSliding() {
        try {
            showDialog(VALIDASINCRO);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subPendientesFromSliding() {
        try {

        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub
        estadoSalir = 1;
        showDialog(CONSSALIR);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        estadoSalir = 2;
        showDialog(CONSSALIR);
    }

    private void salir() {

        if (estadoSalir == 2) {
            ((AplicacionEntel) getApplication()).setCurrentCliente(null);
            ((AplicacionEntel) getApplication())
                    .setCurrentCodFlujo(Configuracion.CONSSTOCK);
            ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
            Intent loIntento = new Intent(ActNoCobranza.this,
                    ActCobranzaMonto.class);
            loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            BeanExtras loBeanExtras = new BeanExtras();
            loBeanExtras.setFiltro("");
            loBeanExtras.setTipo(ConfiguracionNextel.EnumTipBusqueda.NOMBRE);
            String lsFiltro = "";
            try {
                lsFiltro = BeanMapper.toJson(loBeanExtras, false);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
            startActivity(loIntento);
        } else if (estadoSalir == 1) {

            String lsBeanUsuario = getSharedPreferences(
                    "Actual", MODE_PRIVATE).getString(
                    "BeanUsuario", "");
            loBeanUsuario = (BeanUsuario) BeanMapper
                    .fromJson(lsBeanUsuario,
                            BeanUsuario.class);
            this.setMsgOk("Eficiencia Online");

            BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
            loBeanListaEficienciaOnline.setIdResultado(0);
            loBeanListaEficienciaOnline.setResultado("");
            loBeanListaEficienciaOnline.setListaEficiencia(null);
            loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

            new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

            Intent loInstent = new Intent(this, ActKpiDetalle.class);
            loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(loInstent);
        } else if (estadoSalir == 3) {
            Intent loIntentLogin = new Intent(ActNoCobranza.this,
                    ActLogin.class);
            loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loIntentLogin);
        }
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(
                    getString(R.string.ml_sincronizarantes)));
        }
    }

    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK,
                R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */
    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), NexToast.EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {

                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

}