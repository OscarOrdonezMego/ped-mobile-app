package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanRespListaProdOnline {

    @JsonProperty
    private String resultado = "";
    @JsonProperty
    private int idResultado = 0;
    @JsonProperty
    private List<BeanArticuloOnline> listaProd;

    public BeanRespListaProdOnline() {
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public int getIdResultado() {
        return idResultado;
    }

    public void setIdResultado(int idResultado) {
        this.idResultado = idResultado;
    }

    public List<BeanArticuloOnline> getListaProd() {
        return listaProd;
    }

    public void setListaProd(List<BeanArticuloOnline> listaProd) {
        this.listaProd = listaProd;
    }
}