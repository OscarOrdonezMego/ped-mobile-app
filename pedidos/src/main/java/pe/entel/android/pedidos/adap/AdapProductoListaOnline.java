package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

import pe.entel.android.pedidos.R;

public class AdapProductoListaOnline extends ArrayAdapter<String> {
    int resource;
    Context context;
    TextView rowTextView;

    LinkedList<String> originalList;

    int cont = 0;

    public AdapProductoListaOnline(Context _context, int _resource,
                                   LinkedList<String> lista) {
        super(_context, _resource, lista);
        originalList = lista;
        resource = _resource;
        context = _context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        String item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        ((TextView) nuevaVista
                .findViewById(R.id.actproductolistaonlineitem_textTitulo))
                .setText(item);

        return nuevaVista;
    }

}
