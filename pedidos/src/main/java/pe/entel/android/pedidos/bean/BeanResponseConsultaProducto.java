package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Dsb Mobile on 10/06/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanResponseConsultaProducto {

    @JsonProperty("Codigo")
    private int codigo;
    @JsonProperty("General")
    private BeanConsultaProducto general;
    @JsonProperty("Mensaje")
    private String mensaje;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public BeanConsultaProducto getGeneral() {
        return general;
    }

    public void setGeneral(BeanConsultaProducto general) {
        this.general = general;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
