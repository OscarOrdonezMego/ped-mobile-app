package pe.entel.android.pedidos.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;

import pe.entel.android.pedidos.R;

/**
 * Created by rtamayov on 25/05/2015.
 */
@SuppressLint("AppCompatCustomView")
public class MiEditText extends EditText {
    private void inflate() {

        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 10, 0, 10);
        setLayoutParams(layoutParams);

        setTextColor(getResources().getColor(R.color.pedidos_txt));
        setTextSize(getResources().getDimension(R.dimen.txt_form_small));

        setBackgroundResource(R.drawable.pedidos_pedidofin_edt_bg);
    }

    public MiEditText(Context context) {
        super(context);
        inflate();
    }

    public MiEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate();
    }
}
