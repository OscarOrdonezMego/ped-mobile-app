package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rtamayov on 24/09/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanVerificarPedidoControl {

    @JsonProperty("EtiquetaControl")
    private String etiquetaControl;

    @JsonProperty("IdControl")
    private String idControl;

    @JsonProperty("IdFormulario")
    private String idFormulario;

    @JsonProperty("IdTipoControl")
    private String idTipoControl;

    @JsonProperty("ValorControl")
    private String valorControl;


    public String getEtiquetaControl() {
        return etiquetaControl;
    }

    public void setEtiquetaControl(String etiquetaControl) {
        this.etiquetaControl = etiquetaControl;
    }

    public String getIdControl() {
        return idControl;
    }

    public void setIdControl(String idControl) {
        this.idControl = idControl;
    }

    public String getIdFormulario() {
        return idFormulario;
    }

    public void setIdFormulario(String idFormulario) {
        this.idFormulario = idFormulario;
    }

    public String getIdTipoControl() {
        return idTipoControl;
    }

    public void setIdTipoControl(String idTipoControl) {
        this.idTipoControl = idTipoControl;
    }

    public String getValorControl() {
        return valorControl;
    }

    public void setValorControl(String valorControl) {
        this.valorControl = valorControl;
    }

    public static List<BeanVerificarPedidoControl> mapperListaVerificarPedidoControl(List<BeanEnvPedidoCtrl> controles) {
        List<BeanVerificarPedidoControl> verificados = new ArrayList<BeanVerificarPedidoControl>();

        for (BeanEnvPedidoCtrl control : controles) {
            BeanVerificarPedidoControl verificado = new BeanVerificarPedidoControl();
            verificado.setEtiquetaControl(control.getEtiquetaControl());
            verificado.setIdControl(control.getIdControl());
            verificado.setIdFormulario(control.getIdFormulario());
            verificado.setIdTipoControl(control.getIdTipoControl());
            verificado.setValorControl(control.getValorControl());
            verificados.add(verificado);
        }


        return verificados;
    }


}
