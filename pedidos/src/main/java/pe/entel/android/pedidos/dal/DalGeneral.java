package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import greendroid.widget.item.DrawableItem;
import greendroid.widget.item.Item;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanGeneral;
import pe.entel.android.pedidos.util.Configuracion;

public class DalGeneral {

    private Context ioContext;

    public DalGeneral(Context psClase) {
        ioContext = psClase;
    }

    public LinkedList<BeanGeneral> fnSelCondicionVenta() {

        SQLiteDatabase myDB = null;
        LinkedList<BeanGeneral> loLista = null;

        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"Codigo", "Descripcion"};
        String lsTablas = "tbl_general";
        String lsWhere = "Grupo = '2'";

        Cursor loCursor;

        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                "upper(descripcion)");

        loLista = new LinkedList<BeanGeneral>();
        BeanGeneral bean = null;

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    bean = new BeanGeneral();
                    bean.setCodigo(loCursor.getString(0));
                    bean.setDescripcion(loCursor.getString(1));
                    loLista.add(bean);
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loLista;
    }

    public BeanGeneral fnSelFormaPagoxCodigo(String codigo) {

        SQLiteDatabase myDB = null;

        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"Codigo", "Descripcion", "FlagBanco", "FlagDocumento", "FlagFechaDiferida"};
        String lsTablas = "tbl_general";
        String lsWhere = "Grupo = '4' and Codigo ='" + codigo + "'";

        Cursor loCursor;

        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                "upper(descripcion)");

        BeanGeneral bean = null;

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    bean = new BeanGeneral();
                    bean.setCodigo(loCursor.getString(0));
                    bean.setDescripcion(loCursor.getString(1));
                    bean.setFlg_banco(loCursor.getString(2));
                    bean.setFlg_nrodocumento(loCursor.getString(3));
                    bean.setFlg_fechadiferida(loCursor.getString(4));
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return bean;
    }

    public String fnDescFormaPagoXCod(String codFormaPago) {

        SQLiteDatabase myDB = null;
        String loDesc = "";

        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"Codigo", "Descripcion"};
        String lsTablas = "tbl_general";
        String lsWhere = "Grupo = '2' AND Codigo = '" + codFormaPago + "'";

        Cursor loCursor;

        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                "upper(descripcion)");

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loDesc = loCursor.getString(1);
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loDesc;
    }

    public List<Item> fnSelMotNoPedido() {
        SQLiteDatabase myDB = null;
        List<Item> loLista = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"Codigo", "Descripcion"};
        String lsTablas = "tbl_general";
        String lsWhere = "Grupo = '" + Configuracion.GENERAL1MOTNOPED + "'";
        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                "upper(Descripcion)");
        loLista = new ArrayList<Item>(loCursor.getCount());

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    String lsMostrar = loCursor.getString(1);
                    String liKey = loCursor.getString(0);
                    DrawableItem loDrawableItem = new DrawableItem(lsMostrar, 0);
                    loDrawableItem.setTag(liKey);
                    loLista.add(loDrawableItem);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return loLista;
    }

    public ArrayList<DrawableItem> fnSelMotNoPedidoDrawable() {
        SQLiteDatabase myDB = null;
        ArrayList<DrawableItem> loLista = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"Codigo", "Descripcion"};
        String lsTablas = "tbl_general";
        String lsWhere = "Grupo = '" + Configuracion.GENERAL1MOTNOPED + "'";
        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                "upper(Descripcion)");
        loLista = new ArrayList<DrawableItem>(loCursor.getCount());

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    String lsMostrar = loCursor.getString(1);
                    String liKey = loCursor.getString(0);
                    DrawableItem loDrawableItem = new DrawableItem(lsMostrar, 0);

                    loDrawableItem.setTag(liKey);
                    loLista.add(loDrawableItem);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return loLista;
    }

    public LinkedList<BeanGeneral> fnSelGeneral(String grupoGeneral) {

        SQLiteDatabase myDB = null;
        LinkedList<BeanGeneral> loLista = null;

        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"Codigo", "Descripcion", "FlagBanco", "FlagDocumento", "FlagFechaDiferida"};
        String lsTablas = "tbl_general";
        String lsWhere = "Grupo = '" + grupoGeneral + "'";
        Log.v("XXX", "lsWhere : " + lsWhere);
        Cursor loCursor;

        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                "upper(Descripcion)");

        loLista = new LinkedList<BeanGeneral>();
        BeanGeneral bean = null;

        if (loCursor != null) {

            if (loCursor.moveToFirst()) {

                do {
                    bean = new BeanGeneral();
                    bean.setCodigo(loCursor.getString(0));
                    bean.setDescripcion(loCursor.getString(1));
                    bean.setFlg_banco(loCursor.getString(2));
                    bean.setFlg_nrodocumento(loCursor.getString(3));
                    bean.setFlg_fechadiferida(loCursor.getString(4));
                    loLista.add(bean);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loLista;
    }

    public LinkedList<BeanGeneral> fnSelGrupoEconomico() {

        SQLiteDatabase myDB = null;
        LinkedList<BeanGeneral> loLista = null;

        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"Codigo", "Descripcion"};
        String lsTablas = "tbl_general";
        String lsWhere = "Grupo = '11'";

        Cursor loCursor;

        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                "upper(descripcion)");

        loLista = new LinkedList<>();
        BeanGeneral bean;

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    bean = new BeanGeneral();
                    bean.setCodigo(loCursor.getString(0));
                    bean.setDescripcion(loCursor.getString(1));
                    loLista.add(bean);
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loLista;
    }
}