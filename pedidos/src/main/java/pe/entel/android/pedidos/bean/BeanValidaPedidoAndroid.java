package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class BeanValidaPedidoAndroid {
    @JsonProperty
    public String resultado = "";
    @JsonProperty
    public int idResultado = 0;
    @JsonProperty
    public String codPedidoServidor = "0";
    @JsonProperty
    public int numReservados = 0;
    @JsonProperty
    public List<BeanEnvPedidoDet> listDetPedido;

    public BeanValidaPedidoAndroid() {

    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public int getIdResultado() {
        return idResultado;
    }

    public void setIdResultado(int idResultado) {
        this.idResultado = idResultado;
    }

    public String getCodPedidoServidor() {
        return codPedidoServidor;
    }

    public void setCodPedidoServidor(String codPedidoServidor) {
        this.codPedidoServidor = codPedidoServidor;
    }

    public int getNumReservados() {
        return numReservados;
    }

    public void setNumReservados(int numReservados) {
        this.numReservados = numReservados;
    }

    public List<BeanEnvPedidoDet> getListDetPedido() {
        return listDetPedido;
    }

    public void setListDetPedido(List<BeanEnvPedidoDet> listDetPedido) {
        this.listDetPedido = listDetPedido;
    }


}
