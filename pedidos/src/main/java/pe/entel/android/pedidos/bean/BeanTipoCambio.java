package pe.entel.android.pedidos.bean;

public class BeanTipoCambio {

    private String idTipoCambio = "";
    private String codigo = "";
    private String valor = "";

    public BeanTipoCambio() {
    }

    public String getIdTipoCambio() {
        return idTipoCambio;
    }

    public void setIdTipoCambio(String idTipoCambio) {
        this.idTipoCambio = idTipoCambio;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
