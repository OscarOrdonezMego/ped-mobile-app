package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import pe.com.nextel.android.dal.DALGestor;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanBonificacion;
import pe.entel.android.pedidos.bean.BeanBonificacionDet;

/**
 * Created by Dsb Mobile on 22/06/2015.
 */
public class DalBonificacion extends DALGestor {
    private Context ioContext;

    public DalBonificacion(Context psClase) throws Exception {
        super(psClase, psClase.getString(R.string.db_name));
        ioContext = psClase;
    }

    /**
     * Devuelve la lista de BeanBonificacion dependiendo del atributo TipoArticulo
     *
     * @param psTipoArticulo tipo de articulo "PRO" o "PRE"
     * @return lista de BeanBonificacion
     */
    public List<BeanBonificacion> fnListBonificacionXTipoArticulo(String psTipoArticulo) {

        List<BeanBonificacion> loLstBeanBonificacion = new ArrayList<BeanBonificacion>();
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"ID", "IdProducto", "IdPresentacion", "CodigoArticulo",
                "NombreArticulo", "Cantidad", "Maximo", "Editable", "TipoArticulo", "Prioridad"};
        String lsTablas = "TBL_BONIFICACION";
        String lsWhere = "TipoArticulo = ?";
        String[] lsWhereArgs = new String[]{psTipoArticulo};

        Cursor loCursor;
        loCursor = loDB.query(lsTablas, laColumnas, lsWhere, lsWhereArgs, null, null,
                "Prioridad ASC");
        if (loCursor != null) {

            if (loCursor.moveToFirst()) {
                do {
                    BeanBonificacion loBeanBonificacion = new BeanBonificacion();
                    loBeanBonificacion.setId(loCursor.getInt(0));
                    loBeanBonificacion.setIdProducto(loCursor.getInt(1));
                    loBeanBonificacion.setIdPresentacion(loCursor.getInt(2));
                    loBeanBonificacion.setCodigoArticulo(loCursor.getString(3));
                    loBeanBonificacion.setNombreArticulo(loCursor.getString(4));
                    loBeanBonificacion.setCantidad(loCursor.getString(5));
                    loBeanBonificacion.setMaximo(loCursor.getString(6));
                    loBeanBonificacion.setEditable(loCursor.getString(7));
                    loBeanBonificacion.setTipoArticulo(loCursor.getString(8));
                    loBeanBonificacion.setPrioridad(loCursor.getInt(9));
                    loBeanBonificacion.setLstBonificacionDet(fnListBonificacionDetXIdBonificacion(loBeanBonificacion.getId()));
                    loLstBeanBonificacion.add(loBeanBonificacion);
                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return loLstBeanBonificacion;
    }

    /**
     * Devuelve la lista de BeanBonificacionDet dependiendo del atributo IdBonificacion
     *
     * @param piIdBonificacion Id de BeanBonificacion
     * @return lista de BeanBonificacionDet
     */
    public List<BeanBonificacionDet> fnListBonificacionDetXIdBonificacion(int piIdBonificacion) {

        List<BeanBonificacionDet> loLstBeanBonificacionDet = new ArrayList<BeanBonificacionDet>();
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"ID", "IdBonificacion", "IdProducto", "IdPresentacion",
                "CodigoArticulo", "NombreArticulo", "Cantidad", "Existe"};
        String lsTablas = "TBL_BONIFICACION_DETALLE";
        String lsWhere = "IdBonificacion = ?";
        String[] lsWhereArgs = new String[]{String.valueOf(piIdBonificacion)};

        Cursor loCursor;
        loCursor = loDB.query(lsTablas, laColumnas, lsWhere, lsWhereArgs, null, null,
                null);
        if (loCursor != null) {

            if (loCursor.moveToFirst()) {
                do {
                    BeanBonificacionDet loBeanBonificacionDet = new BeanBonificacionDet();
                    loBeanBonificacionDet.setId(loCursor.getInt(0));
                    loBeanBonificacionDet.setIdBonificacion(loCursor.getInt(1));
                    loBeanBonificacionDet.setIdProducto(loCursor.getInt(2));
                    loBeanBonificacionDet.setIdPresentacion(loCursor.getInt(3));
                    loBeanBonificacionDet.setCodigoArticulo(loCursor.getString(4));
                    loBeanBonificacionDet.setNombreArticulo(loCursor.getString(5));
                    loBeanBonificacionDet.setCantidad(loCursor.getString(6));
                    loBeanBonificacionDet.setExiste(loCursor.getString(7));
                    loLstBeanBonificacionDet.add(loBeanBonificacionDet);
                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return loLstBeanBonificacionDet;
    }

    /**
     * Devuelve BeanBonificacion dependiendo del IdBonificacion
     *
     * @param psIdBonficacion id de la bonificacion
     * @return BeanBonificacion
     */
    public BeanBonificacion fnBonificacionXId(String psIdBonficacion) {

        BeanBonificacion loBeanBonificacion = null;
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"ID", "IdProducto", "IdPresentacion", "CodigoArticulo",
                "NombreArticulo", "Cantidad", "Maximo", "Editable", "TipoArticulo", "Prioridad"};
        String lsTablas = "TBL_BONIFICACION";
        String lsWhere = "ID = ?";
        String[] lsWhereArgs = new String[]{psIdBonficacion};

        Cursor loCursor;
        loCursor = loDB.query(lsTablas, laColumnas, lsWhere, lsWhereArgs, null, null,
                "Prioridad ASC");
        if (loCursor != null) {

            if (loCursor.moveToFirst()) {
                do {
                    loBeanBonificacion = new BeanBonificacion();
                    loBeanBonificacion.setId(loCursor.getInt(0));
                    loBeanBonificacion.setIdProducto(loCursor.getInt(1));
                    loBeanBonificacion.setIdPresentacion(loCursor.getInt(2));
                    loBeanBonificacion.setCodigoArticulo(loCursor.getString(3));
                    loBeanBonificacion.setNombreArticulo(loCursor.getString(4));
                    loBeanBonificacion.setCantidad(loCursor.getString(5));
                    loBeanBonificacion.setMaximo(loCursor.getString(6));
                    loBeanBonificacion.setEditable(loCursor.getString(7));
                    loBeanBonificacion.setTipoArticulo(loCursor.getString(8));
                    loBeanBonificacion.setPrioridad(loCursor.getInt(9));
                    loBeanBonificacion.setLstBonificacionDet(fnListBonificacionDetXIdBonificacion(loBeanBonificacion.getId()));
                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return loBeanBonificacion;
    }

}
