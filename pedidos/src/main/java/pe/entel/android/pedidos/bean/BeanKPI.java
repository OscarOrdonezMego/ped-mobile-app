package pe.entel.android.pedidos.bean;

/**
 * Key Performance Indicators
 * Clase que agrupa los indicadores de clave de desempeño del usuario
 *
 * @author oaulestia
 */
public class BeanKPI {
    /**
     * Tiempo total de pedidos
     */
    public long tiempoPed = 0;

    /**
     * variable para marcar inicio de pedido
     */
    public long tiempoInicioTemporal = 0;

    /**
     * Numero total de clientes de la ruta
     */
    public String numCli = "0";

    /**
     * Numero total de clientes de la ruta con pedido
     */
    public String numCliPed = "0";

    /**
     * Numero total de pedidos registrados
     */
    public int numPed = 0;

    /**
     * Numero total de pedidos registrados dentro de ruta
     */
    public int numPedRuta = 0;

    /**
     * Numero total de no pedidos registrados
     */
    public int numNoPed = 0;

    /**
     * Numero total de clientes de la ruta visitados (sin pedido)
     */
    /**
     * Numero total de no pedidos registrados denttro de ruta
     */
    public int numNoPedRuta = 0;

    /**
     * Numero total de clientes de la ruta visitados (sin pedido)
     */
    public int numCliVis = 0;

    /**
     * Monto total registrado en los pedidos
     */
    public String mntPed = "0.00";

    /**
     * Cantidad total de items de los pedidos
     */
    //public int canPed = 0;
    public double canPed = 0;

    /**
     * Monto total recaudado en las cobranzas
     */
    public String mntCob = "0.00";
    /**
     * Monto total por cobrar
     */
    public String mntporCob = "0.00";
    //</editor-fold>

    public double eficiencia = 0;

    public String numPedidos = "0";
    public String numCobranzas = "0";
    public String numNoPedidos = "0";
    public String numCanjes = "0";
    public String numDevoluciones = "0";

}
