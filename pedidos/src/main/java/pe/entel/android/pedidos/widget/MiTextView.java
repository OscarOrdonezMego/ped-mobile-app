package pe.entel.android.pedidos.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import pe.entel.android.pedidos.R;

/**
 * Created by rtamayov on 25/05/2015.
 */
@SuppressLint("AppCompatCustomView")
public class MiTextView extends TextView {
    private void inflate() {

        LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 5, 0, 5);
        setLayoutParams(layoutParams);

        TypedArray ta = getContext().obtainStyledAttributes(new int[]{R.attr.PedidosTextColor});

        setTextColor(getResources().getColor(ta.getResourceId(0, 0)));
        setTextSize(getResources().getDimension(R.dimen.txt_form_small));
    }

    public MiTextView(Context context) {
        super(context);
        inflate();
    }

    public MiTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate();
    }
}
