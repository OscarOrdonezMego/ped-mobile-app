package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanRspPendientes {

    @JsonProperty
    private List<BeanClienteDireccion> listDirecciones;
    @JsonProperty
    private String error;

    public List<BeanClienteDireccion> getListDirecciones() {
        return listDirecciones;
    }

    public void setListDirecciones(List<BeanClienteDireccion> listDirecciones) {
        this.listDirecciones = listDirecciones;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}