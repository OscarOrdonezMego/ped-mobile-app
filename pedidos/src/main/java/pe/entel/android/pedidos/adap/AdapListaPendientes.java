package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

import pe.entel.android.pedidos.R;

/**
 * Created by tkongr on 18/03/2016.
 */
public class AdapListaPendientes extends ArrayAdapter<String> {

    int resource;
    Context context;
    LinkedList<String> originalList;

    public AdapListaPendientes(Context context, int item, LinkedList<String> lista) {
        super(context, item, lista);
        originalList = new LinkedList<String>();
        originalList.addAll(lista);
        this.resource = item;
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        String item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        ((TextView) nuevaVista.findViewById(R.id.actlistapendientelistaitem_textTitulo)).setText(item);

        return nuevaVista;
    }
}
