package pe.entel.android.pedidos.util;

import android.net.TrafficStats;

import pe.com.nextel.android.util.Logger;

public class TrafficMonitor {

    private long iiBytesTxBoot, iiBytesRxBoot;
    private long iiPaquetesTxBoot, iiPaquetesRxBoot;
    private long iiBytesMovilTxBoot, iiBytesMovilRxBoot;
    private int iiUid;
    private String label;

    public TrafficMonitor() {
        this(null);
    }

    public TrafficMonitor(String label) {
        iiUid = android.os.Process.myUid();
        iiBytesRxBoot = TrafficStats.getUidRxBytes(iiUid);
        iiBytesTxBoot = TrafficStats.getUidTxBytes(iiUid);
        iiPaquetesRxBoot = TrafficStats.getUidRxPackets(iiUid);
        iiPaquetesTxBoot = TrafficStats.getUidTxPackets(iiUid);
        iiBytesMovilRxBoot = TrafficStats.getMobileRxBytes();
        iiBytesMovilTxBoot = TrafficStats.getMobileTxBytes();
        this.label = label;
        if (this.label != null)
            Logger.d("TrafficMonitor", "Monitoring traffic: " + this.label);
    }

    public String printTraffic() {
        return printTraffic(true);
    }

    public String printTraffic(boolean printLogger) {
        StringBuilder loSB = new StringBuilder();
        if (this.label != null)
            loSB.append("Monitoring traffic: " + this.label).append('\n');
        loSB.append("BYTES TX/RX ").append(getEnvoyBytes()).append("/").append(getReceivedBytes());
        loSB.append('\n');
        loSB.append("PACKETS TX/RX ").append(getEnvoyPackets()).append("/").append(getReceivedPackets());
        loSB.append('\n');
        loSB.append("MOBILE BYTES TX/RX ").append(getEnvoyMobileBytes()).append("/").append(getReceivedMobileBytes());
        loSB.append('\n');
        if (printLogger)
            Logger.v(loSB.toString());
        return loSB.toString();
    }

    public long getEnvoyBytes() {
        long liBytesTxBoot = TrafficStats.getUidTxBytes(iiUid);
        if (liBytesTxBoot != TrafficStats.UNSUPPORTED) {
            return liBytesTxBoot - iiBytesTxBoot;
        }
        throw new UnsupportedOperationException("This handset doesn't support this operation");
    }

    public long getReceivedBytes() {
        long liBytesRxBoot = TrafficStats.getUidRxBytes(iiUid);
        if (liBytesRxBoot != TrafficStats.UNSUPPORTED) {
            return liBytesRxBoot - iiBytesRxBoot;
        }
        throw new UnsupportedOperationException("This handset doesn't support this operation");
    }

    public long getEnvoyMobileBytes() {
        long liBytesMovilTxBoot = TrafficStats.getMobileTxBytes();
        if (liBytesMovilTxBoot != TrafficStats.UNSUPPORTED) {
            return liBytesMovilTxBoot - iiBytesMovilTxBoot;
        }
        throw new UnsupportedOperationException("This handset doesn't support this operation");
    }

    public long getReceivedMobileBytes() {
        long liBytesMovilRxBoot = TrafficStats.getMobileRxBytes();
        if (liBytesMovilRxBoot != TrafficStats.UNSUPPORTED) {
            return liBytesMovilRxBoot - iiBytesMovilRxBoot;
        }
        throw new UnsupportedOperationException("This handset doesn't support this operation");
    }

    public long getEnvoyPackets() {
        long liPaquetesTxBoot = TrafficStats.getUidTxPackets(iiUid);
        if (liPaquetesTxBoot != TrafficStats.UNSUPPORTED) {
            return liPaquetesTxBoot - iiPaquetesTxBoot;
        }
        throw new UnsupportedOperationException("This handset doesn't support this operation");
    }

    public long getReceivedPackets() {
        long liPaquetesRxBoot = TrafficStats.getUidRxPackets(iiUid);
        if (liPaquetesRxBoot != TrafficStats.UNSUPPORTED) {
            return liPaquetesRxBoot - iiPaquetesRxBoot;
        }
        throw new UnsupportedOperationException("This handset doesn't support this operation");
    }
}