package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanEnvPedidoDet;
import pe.entel.android.pedidos.util.Configuracion;

public class AdapProductoPedido extends ArrayAdapter<BeanEnvPedidoDet> {
    int resource;
    Context context;
    TextView rowTextView;

    String numeroDecimales;
    String flgBonificacion;
    String moneda, monedaDolar ;
    String mostrarAlmacen;
    String mostrarPresentacion;
    String isTipoArticulo;
    String maximoNumeroDecimales;
    int iiMaximoNumeroDecimales;

    ArrayList<BeanEnvPedidoDet> originalList;

    int cont = 0;

    public AdapProductoPedido(Context _context, int _resource,
                              ArrayList<BeanEnvPedidoDet> lista) {
        super(_context, _resource, lista);
        originalList = new ArrayList<BeanEnvPedidoDet>();
        originalList.addAll(lista);
        resource = _resource;
        context = _context;
        isTipoArticulo = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(context)
                .equals(Configuracion.FLGVERDADERO) ? Configuracion.TipoArticulo.PRESENTACION
                : Configuracion.TipoArticulo.PRODUCTO;
        numeroDecimales = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(context);
        flgBonificacion = Configuracion.EnumFuncion.BONIFICACION.getValor(context);
        moneda = Configuracion.EnumConfiguracion.SIMBOLO_MONEDA.getValor(context);
        monedaDolar = "$";
        mostrarAlmacen = Configuracion.EnumConfiguracion.MOSTRAR_ALMACEN.getValor(context);
        mostrarPresentacion = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(context);
        maximoNumeroDecimales = Configuracion.EnumConfiguracion.MAXIMO_NUMERODECIMALES.getValor(context);
        setMaximoNumeroDecimales();
    }

    private void setMaximoNumeroDecimales() {
        iiMaximoNumeroDecimales = 0;
        try {
            iiMaximoNumeroDecimales = Integer.parseInt(maximoNumeroDecimales);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ActProductoDetalle", "Error conversion maximo numero de decimales: " + e.getMessage());
        }
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BeanEnvPedidoDet item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }
        String nombreArticulo = "";
        if (mostrarPresentacion.equals(Configuracion.FLGVERDADERO)) {
            nombreArticulo = item.getNombreArticulo().toUpperCase() + " - " + item.getCantidadPre()
                    + " " + item.getProducto().getUnidadDefecto();
            ((TextView) nuevaVista
                    .findViewById(R.id.actproductopedidoitem_txtnombreproducto))
                    .setVisibility(View.VISIBLE);
            ((TextView) nuevaVista
                    .findViewById(R.id.actproductopedidoitem_txtnombreproducto))
                    .setText(item.getProducto().getCodigo().toUpperCase() + " - " + item.getProducto().getNombre().toUpperCase());
        } else {
            nombreArticulo = item.getNombreArticulo().toUpperCase();
            ((TextView) nuevaVista
                    .findViewById(R.id.actproductopedidoitem_txtnombreproducto))
                    .setVisibility(View.GONE);
        }


        ((TextView) nuevaVista
                .findViewById(R.id.actproductopedidoitem_txtnombrearticulo))
                .setText(nombreArticulo);
        if (item.fnCantidadTotalMayorStock()) {

            ((ImageView) nuevaVista
                    .findViewById(R.id.actproductopedidoitem_imgCircle))
                    .setImageResource(R.drawable.pedidos_circle_red);

        } else {
            ((ImageView) nuevaVista
                    .findViewById(R.id.actproductopedidoitem_imgCircle))
                    .setImageResource(R.drawable.pedidos_circle_green);
        }

        if (item.getCodBonificacion() == 0) {
            ((LinearLayout) nuevaVista.findViewById(R.id.actproductoitem_lncontent))
                    .setBackgroundResource(R.drawable.pedidos_productopedido_listview_bg);
        } else {
            ((LinearLayout) nuevaVista.findViewById(R.id.actproductoitem_lncontent))
                    .setBackgroundResource(R.drawable.pedidos_productopedido_bonificacion_listview_bg);
        }

        //stock
        ((TextView) nuevaVista
                .findViewById(R.id.actproductopedidoitem_txtstock))
                .setText(item.fnMostrarStock(isTipoArticulo, iiMaximoNumeroDecimales));


        //cantidad
        ((TextView) nuevaVista
                .findViewById(R.id.actproductopedidoitem_txtcantidad))
                .setText(item.fnMostrarCantidad(isTipoArticulo,
                        isTipoArticulo.equals(Configuracion.TipoArticulo.PRESENTACION) ? Integer.valueOf(numeroDecimales) : iiMaximoNumeroDecimales));

        if (mostrarAlmacen.equals(Configuracion.FLGVERDADERO)) {
            ((TextView) nuevaVista
                    .findViewById(R.id.actproductopedidoitem_txtFlete))
                    .setText(moneda + " " + item.getFlete());
            ((LinearLayout) nuevaVista
                    .findViewById(R.id.actproductopedidoitem_lnFlete)).setVisibility(View.VISIBLE);
        } else {
            ((LinearLayout) nuevaVista
                    .findViewById(R.id.actproductopedidoitem_lnFlete)).setVisibility(View.GONE);
        }


        if (flgBonificacion.equals(Configuracion.FLGVERDADERO)) {
            ((TextView) nuevaVista.findViewById(R.id.actproductopedidoitem_txtbonificacion)).setText(
                    Utilitario.fnRound(item.getBonificacion(), iiMaximoNumeroDecimales));
        } else {
            ((TextView) nuevaVista.findViewById(R.id.actproductopedidoitem_lblbonificacion)).setVisibility(View.GONE);
            ((TextView) nuevaVista.findViewById(R.id.actproductopedidoitem_txtbonificacion)).setVisibility(View.GONE);
        }


        String tipoMoneda = item.getTipoMoneda(); //@JBELVY

        ((TextView) nuevaVista
                .findViewById(R.id.actproductopedidoitem_txtprecio))
                .setText((tipoMoneda.equals("1") ? moneda : monedaDolar)
                        + " "
                        + Utilitario.fnRound((tipoMoneda.equals("1") ? item.getPrecioBaseSoles() : item.getPrecioBaseDolares()), Integer
                        .valueOf(numeroDecimales)));

        ((TextView) nuevaVista
                .findViewById(R.id.actproductopedidoitem_txtsubtotal))
                .setText((tipoMoneda.equals("1") ? moneda : monedaDolar)
                        + " "
                        + Utilitario.fnRound((tipoMoneda.equals("1") ? item.getMontoSoles(): item.getMontoDolar()), Integer
                        .valueOf(isTipoArticulo.equals(Configuracion.TipoArticulo.PRESENTACION) ? Integer.valueOf(numeroDecimales) : iiMaximoNumeroDecimales)));

        return nuevaVista;
    }
}
