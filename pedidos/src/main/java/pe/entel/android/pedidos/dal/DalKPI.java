package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanKPI;

/**
 * Key Performance Indicators DAL que persiste los indicadores de clave de
 * desempeño del usuario
 *
 * @author oaulestia
 */
public class DalKPI {

    private Context ioContext;

    public DalKPI(Context psClase) {
        ioContext = psClase;
    }

    /**
     * Elimina el registro del KPI No usar por separado, sirve para la funcion
     * fnNuevoKPI
     *
     * @return si se elimino o no
     */
    private boolean fnDelKPI() {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("DELETE from TrKPI");
        Log.v("XXX", "eliminando KPI..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        } finally {
            myDB.close();
        }

        return lbResult;
    }

    /**
     * Borra el registro de KPI existente, inserta uno nuevo con el numero de
     * clientes y el resto de valores en cero
     *
     * @param numClientes el numero de clientes
     * @return el bean actual insertado
     */
    public BeanKPI fnNuevoKPI(int numClientes) {
        // Elimina el registro existente
        fnDelKPI();
        // Se inserta nuevo registro con el numero de clientes y el resto en
        // cero
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("INSERT INTO TrKPI (")
                .append("tiempoPed, numCli, numCliPed, numPed, numNoPed, numCliVis, mntPed, canPed, mntCob")
                .append(") VALUES (").append("0,").append(numClientes)
                .append(",0,0,0,0,'0.00',0,'0.00')");

        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            return null;
        } finally {
            myDB.close();
        }

        // Se devuelve el registro insertado
        return fnSelCurrentKPI();
    }

    /**
     * Devuelve el bean KPI existente
     *
     * @return el bean con los datos actuales
     */
    public BeanKPI fnSelCurrentKPI() {
        SQLiteDatabase myDB = null;
        BeanKPI bkpi = null;
        // List loLista = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"tiempoPed", "numCli",
                "numCliPed", "numPed", "numNoPed", "numCliVis", "mntPed",
                "canPed", "mntCob"};
        String lsTablas = "TrKPI";
        String lsWhere = "";
        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                bkpi = new BeanKPI();
                bkpi.tiempoInicioTemporal = 0;
                bkpi.tiempoPed = loCursor.getInt(0);
                bkpi.numCli = String.valueOf(loCursor.getInt(1));
                bkpi.numCliPed = String.valueOf(loCursor.getInt(2));
                bkpi.numPed = loCursor.getInt(3);
                bkpi.numNoPed = loCursor.getInt(4);
                bkpi.numCliVis = loCursor.getInt(5);
                bkpi.mntPed = loCursor.getString(6);
                bkpi.canPed = loCursor.getInt(7);
                bkpi.mntCob = loCursor.getString(8);
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return bkpi;
    }

    /**
     * Actualiza el registro actual del KPI con los valores actualizados
     *
     * @param bkpi el bean actualizado
     * @return si se realizo la actualizacion o no
     */
    public boolean fnUpdKPI(BeanKPI bkpi) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrKPI SET ")
                .append("tiempoPed=")
                .append(bkpi.tiempoPed)
                .append(", ")
                .append("numCliPed=").append(bkpi.numCliPed).append(", ")
                .append("numPed=").append(bkpi.numPed).append(", ")
                .append("numNoPed=").append(bkpi.numNoPed).append(", ")
                .append("numCliVis=").append(bkpi.numCliVis).append(", ")
                .append("mntPed='").append(bkpi.mntPed).append("', ")
                .append("canPed=").append(bkpi.canPed).append(", ")
                .append("mntCob='").append(bkpi.mntCob).append("'");
        Log.v("XXX", "actualizando KPI..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        } finally {
            myDB.close();
        }

        return lbResult;
    }
}