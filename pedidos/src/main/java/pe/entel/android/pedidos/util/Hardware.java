package pe.entel.android.pedidos.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.IOException;

import pe.com.nextel.android.util.Logger;

public class Hardware {
    private Context context;

    public Hardware(Context context) {
        this.context = context;
    }

    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public boolean isConnected() {
        Log.d("XXX", "Hardware -> isConnected");
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = null;
        if (cm != null) {
            activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null) {
                if (activeNetwork.getState() == NetworkInfo.State.CONNECTED) {
                    Log.d("XXX", "isConnected - NetworkInfo.State : CONNECTED");
                    Log.d("XXX", "isWiFi = " + (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI));
                    Log.d("XXX", "isMobile = " + (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE));
                } else if (activeNetwork.getState() == NetworkInfo.State.CONNECTING) {
                    Log.d("XXX", "isConnected - NetworkInfo.State : CONNECTING");
                } else if (activeNetwork.getState() == NetworkInfo.State.DISCONNECTED) {
                    Log.d("XXX", "isConnected - NetworkInfo.State : DISCONNECTED");
                } else if (activeNetwork.getState() == NetworkInfo.State.DISCONNECTING) {
                    Log.d("XXX", "isConnected - NetworkInfo.State : DISCONNECTING");
                } else if (activeNetwork.getState() == NetworkInfo.State.SUSPENDED) {
                    Log.d("XXX", "isConnected - NetworkInfo.State : SUSPENDED");
                } else if (activeNetwork.getState() == NetworkInfo.State.UNKNOWN) {
                    Log.d("XXX", "isConnected - NetworkInfo.State : UNKNOWN");
                }
            }
        }
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public String getNetworkType() {
        String tipoDeRed = "Notfound";
        TelephonyManager mTelephonyManager = (TelephonyManager)
                context.getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = 0;
        if (mTelephonyManager != null) {
            networkType = mTelephonyManager.getNetworkType();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    tipoDeRed = "2g";
                    break;
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    /*
                     From this link https://goo.gl/R2HOjR ..NETWORK_TYPE_EVDO_0 & NETWORK_TYPE_EVDO_A
                     EV-DO is an evolution of the CDMA2000 (IS-2000) standard that supports high data rates.

                     Where CDMA2000 https://goo.gl/1y10WI .CDMA2000 is a family of 3G[1] mobile technology standards for sending voice,
                     data, and signaling data between mobile phones and cell sites.
                     */
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    //Log.d("Type", "3g");
                    //For 3g HSDPA , HSPAP(HSPA+) are main  networktype which are under 3g Network
                    //But from other constants also it will 3g like HSPA,HSDPA etc which are in 3g case.
                    //Some cases are added after  testing(real) in device with 3g enable data
                    //and speed also matters to decide 3g network type
                    //http://goo.gl/bhtVT
                    tipoDeRed = "3g";
                    break;
                case TelephonyManager.NETWORK_TYPE_LTE:
                    //No specification for the 4g but from wiki
                    //I found(LTE (Long-Term Evolution, commonly marketed as 4G LTE))
                    //https://goo.gl/9t7yrR
                    tipoDeRed = "4g";
                    break;
                default:
                    tipoDeRed = "Notfound";
            }
        }
        return tipoDeRed;
    }

    public String getVersionApp() {
        String versionApp = "Error";
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionApp = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Logger.e("XXX", "Hardware -> Error" + e);
        }
        return versionApp;
    }

    public boolean isGpsHabilitado() {
        boolean habilitado = false;
        LocationManager loLocationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (loLocationManager != null) {
            habilitado = loLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }
        Log.d("XXX", "Hardware -> isGpsHabilitado : " + habilitado);
        return habilitado;
    }

    private static int exifToDegrees(int exifOrientation) {
        Log.d("XXX", "Hardware -> exifToDegrees");
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180;
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270;
        }
        return 0;
    }

    private static String getRealPathFromURI(Context context, String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        Log.d("XXX", "Hardware -> calculateInSampleSize");
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }
}
