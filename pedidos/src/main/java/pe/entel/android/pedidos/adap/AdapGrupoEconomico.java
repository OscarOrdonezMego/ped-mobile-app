package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanGeneral;

public class AdapGrupoEconomico extends ArrayAdapter<BeanGeneral> {
    int resource;
    Context context;
    TextView rowTextView;

    LinkedList<BeanGeneral> originalList;

    int cont = 0;

    public AdapGrupoEconomico(Context _context, int _resource,
                                    LinkedList<BeanGeneral> lista) {
        super(_context, _resource, lista);
        originalList = new LinkedList<>();
        originalList.addAll(lista);
        resource = _resource;
        context = _context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BeanGeneral item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        ((TextView) nuevaVista
                .findViewById(R.id.actclientegrupoeconomico_textTitulo))
                .setText(item.getDescripcion());

        return nuevaVista;
    }
}
