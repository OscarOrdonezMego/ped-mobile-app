package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import greendroid.widget.ActionBarItem;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.actividad.NexInvalidOperatorActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.Gps;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout;
import pe.com.nextel.android.widget.NexToast;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanGeneral;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanProspecto;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalGeneral;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.http.HttpEnviarProspecto;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.UtilitarioData;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;
import pe.entel.android.pedidos.widget.ListaPopup;

public class ActProspecto extends NexActivity implements View.OnClickListener, NexMenuDrawerLayout.NexMenuCallbacks,
        AdapMenuPrincipalSliding.MenuSlidingListener, ListaPopup.OnItemClickPopup {

    private BeanUsuario loBeanUsuario = null;
    private BeanProspecto beanProspecto = null;

    private EditText etxtCodigo, etxtNombre, etxtDireccion, etxtGiro, etxtObservacion, etxtColAdic1, etxtColAdic2, etxtColAdic3, etxtColAdic4, etxtColAdic5;
    private TextView txtColAdic1, txtColAdic2, txtColAdic3, txtColAdic4, txtColAdic5;
    private LinearLayout lnlyColAdic1, lnlyColAdic2, lnlyColAdic3, lnlyColAdic4, lnlyColAdic5;
    private Button btnEnviar, spiTipoCliente;

    private String isDesColumAdic1, isDesColumAdic2, isDesColumAdic3, isDesColumAdic4, isDesColumAdic5;
    private String isValColumAdic1, isValColumAdic2, isValColumAdic3, isValColumAdic4, isValColumAdic5;

    private final int CONTEXT_MENU_ID = 99;
    private final int CONSBACK = 1;
    private final int CONSDIASINCRONIZAR = 2;
    private final int VALIDAHOME = 3;
    private final int VALIDASINCRONIZACION = 4;
    private final int ENVIOPENDIENTES = 5;
    private final int VALIDASINCRO = 6;
    private final int CONSSALIR = 7;
    private final int NOHAYPENDIENTES = 8;

    private int estadoSalir = 0;
    private boolean estadoSincronizar = false;
    private static BeanGeneral tiposcliente;
    private final int POPUP_TIPOCLIENTE = 10;
    private DalGeneral loDalGeneral;
    private List ioLista;

    // Integracion Ntrack
    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro

    private AdapMenuPrincipalSliding _menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);
        String lsBeanRepresentante = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanRepresentante,
                BeanUsuario.class);

        isValColumAdic1 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_1.getValor(this);
        isValColumAdic2 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_2.getValor(this);
        isValColumAdic3 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_3.getValor(this);
        isValColumAdic4 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_4.getValor(this);
        isValColumAdic5 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_5.getValor(this);
        isDesColumAdic1 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_1.getDescripcion(this);
        isDesColumAdic2 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_2.getDescripcion(this);
        isDesColumAdic3 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_3.getDescripcion(this);
        isDesColumAdic4 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_4.getDescripcion(this);
        isDesColumAdic5 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_5.getDescripcion(this);

        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);
        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));
        subIniMenu();
    }

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    public void subSetControles() {
        setActionBarContentView(R.layout.act_prospecto);
        super.subSetControles();

        etxtCodigo = (EditText) findViewById(R.id.actprospecto_txtcodigo);
        etxtNombre = (EditText) findViewById(R.id.actprospecto_txtnombre);
        etxtDireccion = (EditText) findViewById(R.id.actprospecto_txtdireccion);
        etxtGiro = (EditText) findViewById(R.id.actprospecto_txtgiro);
        etxtObservacion = (EditText) findViewById(R.id.actprospecto_txtobservacion);
        btnEnviar = (Button) findViewById(R.id.actprospecto_btnenviar);
        spiTipoCliente = (Button) findViewById(R.id.actprospecto_cmbTipoCliente);

        lnlyColAdic1 = (LinearLayout) findViewById(R.id.actprospecto_lnlyColAdic1);
        lnlyColAdic2 = (LinearLayout) findViewById(R.id.actprospecto_lnlyColAdic2);
        lnlyColAdic3 = (LinearLayout) findViewById(R.id.actprospecto_lnlyColAdic3);
        lnlyColAdic4 = (LinearLayout) findViewById(R.id.actprospecto_lnlyColAdic4);
        lnlyColAdic5 = (LinearLayout) findViewById(R.id.actprospecto_lnlyColAdic5);

        txtColAdic1 = (TextView) findViewById(R.id.actprospecto_lblColAdic1);
        txtColAdic2 = (TextView) findViewById(R.id.actprospecto_lblColAdic2);
        txtColAdic3 = (TextView) findViewById(R.id.actprospecto_lblColAdic3);
        txtColAdic4 = (TextView) findViewById(R.id.actprospecto_lblColAdic4);
        txtColAdic5 = (TextView) findViewById(R.id.actprospecto_lblColAdic5);

        etxtColAdic1 = (EditText) findViewById(R.id.actprospecto_txtColAdic1);
        etxtColAdic2 = (EditText) findViewById(R.id.actprospecto_txtColAdic2);
        etxtColAdic3 = (EditText) findViewById(R.id.actprospecto_txtColAdic3);
        etxtColAdic4 = (EditText) findViewById(R.id.actprospecto_txtColAdic4);
        etxtColAdic5 = (EditText) findViewById(R.id.actprospecto_txtColAdic5);

        mostrarCamposAdicionales();

        btnEnviar.setOnClickListener(this);

        spiTipoCliente.setOnClickListener(this);
        cargarDetalle();
    }

    private void cargarDetalle() {
        beanProspecto = new BeanProspecto();
        if (ioLista == null) {
            loDalGeneral = new DalGeneral(this);
            ioLista = loDalGeneral.fnSelGeneral(String
                    .valueOf(Configuracion.GENERAL7TIPOCLIENTE));
            ioLista.add(0, new BeanGeneral());
        }
        tiposcliente = new BeanGeneral();
        spiTipoCliente.setText(tiposcliente.getDescripcion());


    }

    @Override
    public void setOnItemClickPopup(Object poObject, int piTipo) {
        // TODO Auto-generated method stub

        switch (piTipo) {
            case POPUP_TIPOCLIENTE:
                tiposcliente = (BeanGeneral) poObject;
                spiTipoCliente.setText(tiposcliente.getDescripcion());
                break;
            default:

                break;
        }

    }

    private void mostrarCamposAdicionales() {
        if (isValColumAdic1.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic1.setVisibility(View.VISIBLE);
            txtColAdic1.setText(isDesColumAdic1);
        } else
            lnlyColAdic1.setVisibility(View.GONE);

        if (isValColumAdic2.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic2.setVisibility(View.VISIBLE);
            txtColAdic2.setText(isDesColumAdic2);
        } else
            lnlyColAdic2.setVisibility(View.GONE);

        if (isValColumAdic3.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic3.setVisibility(View.VISIBLE);
            txtColAdic3.setText(isDesColumAdic3);
        } else
            lnlyColAdic3.setVisibility(View.GONE);

        if (isValColumAdic4.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic4.setVisibility(View.VISIBLE);
            txtColAdic4.setText(isDesColumAdic4);
        } else
            lnlyColAdic4.setVisibility(View.GONE);

        if (isValColumAdic5.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic5.setVisibility(View.VISIBLE);
            txtColAdic5.setText(isDesColumAdic5);
        } else
            lnlyColAdic5.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        showDialog(CONSBACK);
    }

    private void irBusquedaCliente() {
        finish();
        startActivity(new Intent(this, ActClienteBuscar.class));
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setTitle(getString(R.string.actprospecto_bartitle));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.actprospecto_btnenviar) {
            if (String.valueOf(etxtCodigo.getText()).equals("")
                    || String.valueOf(etxtNombre.getText()).equals("")
                    || String.valueOf(etxtDireccion.getText()).equals("")) {
                Toast.makeText(this, "Debe de llenar todos campos del formulario", Toast.LENGTH_SHORT).show();
            } else {


                beanProspecto.setCodigo(String.valueOf(etxtCodigo.getText()));
                beanProspecto.setNombre(String.valueOf(etxtNombre.getText()));
                beanProspecto.setDireccion(String.valueOf(etxtDireccion.getText()));
                beanProspecto.setCodUsuario(loBeanUsuario.getCodVendedor());
                beanProspecto.setTcli_cod(tiposcliente.getCodigo());
                beanProspecto.setGiro(String.valueOf(etxtGiro.getText()));
                beanProspecto.setObservacion(String.valueOf(etxtObservacion.getText()));
                beanProspecto.setCampoAdicional1(String.valueOf(etxtColAdic1.getText()));
                beanProspecto.setCampoAdicional2(String.valueOf(etxtColAdic2.getText()));
                beanProspecto.setCampoAdicional3(String.valueOf(etxtColAdic3.getText()));
                beanProspecto.setCampoAdicional4(String.valueOf(etxtColAdic4.getText()));
                beanProspecto.setCampoAdicional5(String.valueOf(etxtColAdic5.getText()));
                beanProspecto.setFecha(UtilitarioData.fnObtenerFechaddMMyyyykkmmss());

                try {
                    beanProspecto.setLatitud(String.valueOf(Gps.getLocation().getLatitude()));
                    beanProspecto.setLongitud(String.valueOf(Gps.getLocation().getLongitude()));
                } catch (Exception e) {
                    e.printStackTrace();
                    beanProspecto.setLatitud("0");
                    beanProspecto.setLongitud("0");
                }

                try {
                    subGrabarProspecto();
                    new HttpEnviarProspecto(beanProspecto, ActProspecto.this, fnTipactividad()).execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } else if (v == spiTipoCliente) {
            if (ioLista == null) {
                loDalGeneral = new DalGeneral(this);
                ioLista = loDalGeneral.fnSelGeneral(String
                        .valueOf(Configuracion.GENERAL7TIPOCLIENTE));
                ioLista.add(0, new BeanGeneral());
            }
            new ListaPopup().dialog(ActProspecto.this, ioLista,
                    POPUP_TIPOCLIENTE, ActProspecto.this,
                    getString(R.string.listapopup_txtseleccionetipocliente), tiposcliente,
                    R.attr.PedidosPopupLista);
        }
    }

    protected void NexShowDialog(int id) {
        DialogFragmentYesNo loDialog;
        switch (id) {

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.actmenu_dlgsincronizar),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"))
                        == DialogFragmentYesNo.YES) {
                    try {
                        String lsBeanUsuario = getSharedPreferences("Actual",
                                MODE_PRIVATE).getString("BeanUsuario", "");
                        BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                .fromJson(lsBeanUsuario, BeanUsuario.class);

                        new HttpSincronizar(loBeanUsuario.getCodVendedor(),
                                ActProspecto.this, fnTipactividad())
                                .execute();
                    } catch (Exception e) {
                        subShowException(e);
                    }
                }
                break;

            case VALIDASINCRONIZACION:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"),
                        getString(R.string.actmenuvalsincro_titulo),
                        getString(R.string.actmenuvalsincro_mensaje),
                        R.drawable.boton_error, false, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"));
                break;

            case VALIDASINCRO:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                        getString(R.string.msgregpendientesenvinfo),
                        R.drawable.boton_error, false, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"));
                break;

            case ENVIOPENDIENTES:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"),
                        getString(R.string.msgdeseaenviarpendientes),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"))
                        == DialogFragmentYesNo.YES) {
                    Intent loIntentEspera = new Intent(ActProspecto.this,
                            ActGrabaPendiente.class);
                    startActivity(loIntentEspera);
                }
                break;

            case CONSSALIR:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSSALIR"),
                        getString(R.string.actproductopedido_dlgsalir),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSSALIR"))
                        == DialogFragmentYesNo.YES) {
                    salir();
                }
                break;

            case CONSBACK:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSBACK"),
                        getString(R.string.actproductopedido_dlgsalir),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSBACK"))
                        == DialogFragmentYesNo.YES) {
                    irBusquedaCliente();
                }
                break;

        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {
        if (estadoSincronizar) {
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActMenu.class);
                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }
        } else {
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                DalCliente loDalCliente = new DalCliente(this);
                loDalCliente.fnUpdEstadoProspectoPendienteXId(beanProspecto.getId());
                irBusquedaCliente();
            } else {
                new NexToast(this, R.string.actgrabapedido_dlgerrorpedido,
                        NexToast.EnumType.ERROR).show();
                irBusquedaCliente();
            }
        }
    }

    private void subGrabarProspecto() throws Exception {
        // Grabamos en la BD del equipo
        DalCliente loDalCliente = new DalCliente(this);
        String id = loDalCliente.fnGrabarProspecto(beanProspecto);
        beanProspecto.setId(id);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        switch (item.getItemId()) {
            case CONTEXT_MENU_ID:
                showDialog(CONTEXT_MENU_ID);
                break;
            default:
                return super.onHandleActionBarItemClick(item, position);
        }
        return true;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case Constantes.NOSUPERAMONTOMINIMO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_montoMinimoNoSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.SUPERAMONTOMAXIMO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_montoMaximoSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.SUPEROMAXIMOITEMS:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_itemsSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSBACK:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        finish();
                                        startActivity(new Intent(ActProspecto.this, ActClienteBuscar.class));
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case VALIDAHOME:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);

                                        Intent intentl = new Intent(
                                                ActProspecto.this,
                                                ActMenu.class);
                                        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                        startActivity(intentl);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);

                                        try {
                                            BeanUsuario loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(ActProspecto.this);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActProspecto.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActProspecto.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSSALIR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgsalir))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        salir();
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            default:
                return super.onCreateDialog(id);
        }

    }

    @Override
    protected DialogFragmentYesNo.DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        DialogFragmentYesNo loDialog;
        switch (id) {
            case DIALOG_NSERVICES_DOWNLOAD:
                String lsNServices;
                if (ioDALNservices == null)
                    try {
                        ioDALNservices = DALNServices.getInstance(this,
                                R.string.db_name);
                    } catch (NexInvalidOperatorActivity.NexInvalidOperatorException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                try {
                    lsNServices = ioDALNservices.fnSelNServicesLabel();
                } catch (Exception e) {
                    Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                    lsNServices = getString(R.string.nservices_label_default);
                }
                loDialog = DialogFragmentYesNo.newInstance(
                        getFragmentManager(),
                        DialogFragmentYesNo.TAG + DIALOG_NSERVICES_DOWNLOAD,
                        getString(R.string.nservices_consultingdownlad).replace(
                                "{0}", lsNServices),
                        DialogFragmentYesNo.EnumLayoutResource.getDefaultIconHelp(this));
                return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog,
                        new DialogFragmentYesNo.OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    subIniNServicesDownload(ioDALNservices.fnSelNServicesAPK());
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                            }
                        });

            case CONSBACK:

                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSBACK"),
                        getString(R.string.actproductopedido_dlgback),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog,
                        new DialogFragmentYesNo.OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {

                                finish();
                                startActivity(new Intent(ActProspecto.this, ActClienteBuscar.class));
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            case VALIDAHOME:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSBACK"),
                        getString(R.string.actproductopedido_dlgback),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog,
                        new DialogFragmentYesNo.OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {

                                ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);

                                Intent intentl = new Intent(
                                        ActProspecto.this, ActMenu.class);
                                intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                startActivity(intentl);
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.actmenu_dlgsincronizar),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog,
                        new DialogFragmentYesNo.OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {

                                ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);

                                try {
                                    BeanUsuario loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(ActProspecto.this);
                                    estadoSincronizar = true;
                                    new HttpSincronizar(loBeanUsuario
                                            .getCodVendedor(),
                                            ActProspecto.this,
                                            fnTipactividad()).execute();
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                            }
                        });

            case VALIDASINCRONIZACION:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"),
                        getString(R.string.actmenuvalsincro_titulo),
                        getString(R.string.actmenuvalsincro_mensaje),
                        R.drawable.boton_error, false, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog, null);

            case VALIDASINCRO:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                        getString(R.string.msgregpendientesenvinfo),
                        R.drawable.boton_informacion, false,
                        DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog, null);

            case ENVIOPENDIENTES:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"),
                        getString(R.string.msgdeseaenviarpendientes),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog,
                        new DialogFragmentYesNo.OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActProspecto.this);
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                            }
                        });

            case CONSSALIR:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSSALIR"),
                        getString(R.string.actproductopedido_dlgsalir),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog,
                        new DialogFragmentYesNo.OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                salir();
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });
            default:
                return super.onCreateNexDialog(id);
        }
    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                estadoSalir = 3;
                showDialog(CONSSALIR);
            }
        });
    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        showDialog(VALIDAHOME);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = new DalPedido(this).fnEnvioPedidosPendientes(loBeanUsuario.getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);
                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();
                UtilitarioPedidos.mostrarDialogo(this, CONSDIASINCRONIZAR);
            } else {
                UtilitarioPedidos.mostrarDialogo(this, VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub
        estadoSalir = 1;
        showDialog(CONSSALIR);
    }

    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        estadoSalir = 2;
        showDialog(CONSSALIR);
    }

    private void salir() {

        if (estadoSalir == 2) {
            ((AplicacionEntel) getApplication()).setCurrentCliente(null);
            ((AplicacionEntel) getApplication()).setCurrentCodFlujo(Configuracion.CONSSTOCK);
            Intent loIntento = new Intent(ActProspecto.this, ActProductoListaOnline.class);
            loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            BeanExtras loBeanExtras = new BeanExtras();
            loBeanExtras.setFiltro("");
            loBeanExtras.setTipo(ConfiguracionNextel.EnumTipBusqueda.NOMBRE);
            String lsFiltro = "";
            try {
                lsFiltro = BeanMapper.toJson(loBeanExtras, false);
            } catch (Exception e) {
                e.printStackTrace();
            }

            loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
            startActivity(loIntento);
        } else if (estadoSalir == 1) {

            loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);
            this.setMsgOk("Eficiencia Online");

            BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
            loBeanListaEficienciaOnline.setIdResultado(0);
            loBeanListaEficienciaOnline.setResultado("");
            loBeanListaEficienciaOnline.setListaEficiencia(null);
            loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

            new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

            Intent loInstent = new Intent(this, ActKpiDetalle.class);
            loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(loInstent);
        } else if (estadoSalir == 3) {
            Intent loIntentLogin = new Intent(ActProspecto.this, ActLogin.class);
            loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loIntentLogin);
        }
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(
                    getString(R.string.ml_sincronizarantes)));
        }
    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */
    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), NexToast.EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

}
