package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rtamayov on 24/09/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanVerificarPedidoDetalle {

    @JsonProperty("CodigoPedido")
    private String codigoPedido;
    @JsonProperty("Atendible")
    private String atendible;
    @JsonProperty("Bonificacion")
    private String bonificacion;
    @JsonProperty("BonificacionFrac")
    private String bonificacionFrac;
    @JsonProperty("CantBonificacion")
    private int cantBonificacion;
    @JsonProperty("Cantidad")
    private String cantidad;
    @JsonProperty("CantidadFrac")
    private String cantidadFrac;
    @JsonProperty("CantidadPre")
    private String cantidadPre;
    @JsonProperty("CodAlmacen")
    private String codAlmacen;
    @JsonProperty("CodBonificacion")
    private int codBonificacion;
    @JsonProperty("CodListaArticulo")
    private String codListaArticulo;
    @JsonProperty("CodPedido")
    private String codPedido;
    @JsonProperty("CodigoArticulo")
    private String codigoArticulo;
    @JsonProperty("Descuento")
    private String descuento;
    @JsonProperty("Empresa")
    private String empresa;
    @JsonProperty("FechaUltVenta")
    private String fechaUltVenta;
    @JsonProperty("Flete")
    private String flete;
    @JsonProperty("IdArticulo")
    private String idArticulo;
    @JsonProperty("IdListaPrecio")
    private String idListaPrecio;
    @JsonProperty("Monto")
    private String monto;
    @JsonProperty("MontoSinDescuento")
    private String montoSinDescuento;
    @JsonProperty("MultBonificacion")
    private int multBonificacion;
    @JsonProperty("NombreArticulo")
    private String nombreArticulo;
    @JsonProperty("Observacion")
    private String observacion;
    @JsonProperty("PrecioBase")
    private String precioBase;
    @JsonProperty("PrecioFrac")
    private String precioFrac;
    @JsonProperty("Stock")
    private String stock;
    @JsonProperty("Tipo")
    private String tipo;
    @JsonProperty("TipoDescuento")
    private String tipoDescuento;
    @JsonProperty("UltPrecio")
    private String ultPrecio;
    @JsonProperty("EditBonificacion")
    private boolean editBonificacion;
    @JsonProperty("Producto")
    public BeanProducto producto;


    public String getCodigoPedido() {
        return codigoPedido;
    }

    public void setCodigoPedido(String codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public String getAtendible() {
        return atendible;
    }

    public void setAtendible(String atendible) {
        this.atendible = atendible;
    }

    public String getBonificacion() {
        return bonificacion;
    }

    public void setBonificacion(String bonificacion) {
        this.bonificacion = bonificacion;
    }

    public String getBonificacionFrac() {
        return bonificacionFrac;
    }

    public void setBonificacionFrac(String bonificacionFrac) {
        this.bonificacionFrac = bonificacionFrac;
    }

    public int getCantBonificacion() {
        return cantBonificacion;
    }

    public void setCantBonificacion(int cantBonificacion) {
        this.cantBonificacion = cantBonificacion;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getCantidadFrac() {
        return cantidadFrac;
    }

    public void setCantidadFrac(String cantidadFrac) {
        this.cantidadFrac = cantidadFrac;
    }

    public String getCantidadPre() {
        return cantidadPre;
    }

    public void setCantidadPre(String cantidadPre) {
        this.cantidadPre = cantidadPre;
    }

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public int getCodBonificacion() {
        return codBonificacion;
    }

    public void setCodBonificacion(int codBonificacion) {
        this.codBonificacion = codBonificacion;
    }

    public String getCodListaArticulo() {
        return codListaArticulo;
    }

    public void setCodListaArticulo(String codListaArticulo) {
        this.codListaArticulo = codListaArticulo;
    }

    public String getCodPedido() {
        return codPedido;
    }

    public void setCodPedido(String codPedido) {
        this.codPedido = codPedido;
    }

    public String getCodigoArticulo() {
        return codigoArticulo;
    }

    public void setCodigoArticulo(String codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getFechaUltVenta() {
        return fechaUltVenta;
    }

    public void setFechaUltVenta(String fechaUltVenta) {
        this.fechaUltVenta = fechaUltVenta;
    }

    public String getFlete() {
        return flete;
    }

    public void setFlete(String flete) {
        this.flete = flete;
    }

    public String getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(String idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getIdListaPrecio() {
        return idListaPrecio;
    }

    public void setIdListaPrecio(String idListaPrecio) {
        this.idListaPrecio = idListaPrecio;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getMontoSinDescuento() {
        return montoSinDescuento;
    }

    public void setMontoSinDescuento(String montoSinDescuento) {
        this.montoSinDescuento = montoSinDescuento;
    }

    public int getMultBonificacion() {
        return multBonificacion;
    }

    public void setMultBonificacion(int multBonificacion) {
        this.multBonificacion = multBonificacion;
    }

    public String getNombreArticulo() {
        return nombreArticulo;
    }

    public void setNombreArticulo(String nombreArticulo) {
        this.nombreArticulo = nombreArticulo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(String precioBase) {
        this.precioBase = precioBase;
    }

    public String getPrecioFrac() {
        return precioFrac;
    }

    public void setPrecioFrac(String precioFrac) {
        this.precioFrac = precioFrac;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipoDescuento() {
        return tipoDescuento;
    }

    public void setTipoDescuento(String tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }

    public String getUltPrecio() {
        return ultPrecio;
    }

    public void setUltPrecio(String ultPrecio) {
        this.ultPrecio = ultPrecio;
    }

    public boolean isEditBonificacion() {
        return editBonificacion;
    }

    public void setEditBonificacion(boolean editBonificacion) {
        this.editBonificacion = editBonificacion;
    }

    public BeanProducto getProducto() {
        return producto;
    }

    public void setProducto(BeanProducto producto) {
        this.producto = producto;
    }

    public static List<BeanVerificarPedidoDetalle> mapperListaVerificarPedidoDetalle(List<BeanEnvPedidoDet> detalles) {
        List<BeanVerificarPedidoDetalle> verificados = new ArrayList<BeanVerificarPedidoDetalle>();

        for (BeanEnvPedidoDet detalle : detalles) {
            BeanVerificarPedidoDetalle verificado = new BeanVerificarPedidoDetalle();
            verificado.setCodigoPedido(detalle.getCodPedido());
            verificado.setAtendible(detalle.getAtendible());
            verificado.setBonificacion(detalle.getBonificacion());
            verificado.setBonificacionFrac(detalle.getBonificacionFrac());
            verificado.setCantBonificacion(detalle.getCodBonificacion());
            verificado.setCantidad(detalle.getCantidad());
            verificado.setCantidadFrac(detalle.getCantidadFrac());
            verificado.setCantidadPre(detalle.getCantidadPre());
            verificado.setCodAlmacen(detalle.getCodAlmacen());
            verificado.setCodBonificacion(detalle.getCodBonificacion());
            verificado.setCodListaArticulo(detalle.getCodListaArticulo());
            verificado.setCodPedido(detalle.getCodPedido());
            verificado.setCodigoArticulo(detalle.getCodigoArticulo());
            verificado.setDescuento(detalle.getDescuento());
            verificado.setEmpresa(detalle.getEmpresa());
            verificado.setFechaUltVenta(detalle.getFechaUltVenta());
            verificado.setFlete(detalle.getFlete());
            verificado.setIdArticulo(detalle.getIdArticulo());
            verificado.setIdListaPrecio(detalle.getIdListaPrecio());
            verificado.setMonto(detalle.getMonto());
            verificado.setMontoSinDescuento(detalle.getMontoSinDescuento());
            verificado.setMultBonificacion(detalle.getMultBonificacion());
            verificado.setNombreArticulo(detalle.getNombreArticulo());
            verificado.setObservacion(detalle.getObservacion());
            verificado.setPrecioBase(detalle.getPrecioBase());
            verificado.setPrecioFrac(detalle.getPrecioFrac());
            verificado.setStock(detalle.getStock());
            verificado.setTipo(detalle.getTipo());
            verificado.setTipoDescuento(detalle.getTipoDescuento());
            verificado.setUltPrecio(detalle.getUltPrecio());
            verificado.setEditBonificacion(detalle.isEditBonificacion());
            verificados.add(verificado);
        }

        return verificados;
    }


}
