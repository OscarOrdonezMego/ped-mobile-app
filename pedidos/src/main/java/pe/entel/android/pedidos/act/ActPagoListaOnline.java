package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.actividad.NexInvalidOperatorActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout;
import pe.com.nextel.android.widget.NexToast;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapPagoListaOnline;
import pe.entel.android.pedidos.bean.BeanConsulta;
import pe.entel.android.pedidos.bean.BeanEnvDocumento;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanPagoOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalCobranza;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.http.HttpConsultaPagoOnline;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;
import android.widget.AdapterView.OnItemClickListener;

/**
 * Created by tkongr on 15/10/2015.
 */
public class ActPagoListaOnline extends NexActivity implements
        NexMenuDrawerLayout.NexMenuCallbacks, AdapMenuPrincipalSliding.MenuSlidingListener, OnItemClickListener {

    long iiIdCliente;
    private ListView lista;

    private LinkedList<BeanPagoOnline> ioLista;

    BeanUsuario loBeanUsuario = null;
    BeanPagoOnline pagoOnline = null;

    private AdapMenuPrincipalSliding _menu;

    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDASINCRONIZACION = 11;
    private final int ENVIOPENDIENTES = 12;
    private final int VALIDASINCRO = 13;
    private final int NOHAYPENDIENTES = 14;
    private final int CONSNOREGISTROPAGO = 17;
    boolean estadoSincronizar = false;
    private String isSeguimientoPedido;

    Bundle savedInstanceState;

    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);
        this.savedInstanceState = savedInstanceState;

        String lsBeanUsuario = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario,
                BeanUsuario.class);

        isSeguimientoPedido = Configuracion.EnumConfiguracion.SEGUIMIENTO_PEDIDO.getValor(this);

        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);

        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));

        subIniMenu();
    }

    @Override
    protected void subSetControles() {
        // TODO Auto-generated method stub
        super.subSetControles();
        Log.i("subSetControles", "ingrese");

        try {
            setActionBarContentView(R.layout.pagolistaonline);
            lista = (ListView) findViewById(R.id.actpagolistaonline_lista);
            lista.setOnItemClickListener(this);
            if (!isUsingDialogFragment()) {
                registerForContextMenu(lista);
            }

            subLLenarLista();

        } catch (Exception e) {
            Log.e("subSetControles", e.toString());
            subShowException(e);
        }
        Log.i("subSetControles", "acabee");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 5 && resultCode == RESULT_OK){
            if (new DalCobranza(getApplicationContext()).fnCountPagosOnline() > 0) {
                subLLenarLista();
            } else {
                lista.setAdapter(null);
                UtilitarioPedidos.mostrarDialogo(this,CONSNOREGISTROPAGO);
            }

        }
    }

    public void subLLenarLista() {
        try {

            DalCobranza loDalcobranza = new DalCobranza(this);
            ioLista = loDalcobranza.fnListarPagosOnline();
            Log.i("LISTA", ioLista.get(0).getPag_pk() + " - " + ioLista.get(0).getCli_nombre());
            AdapPagoListaOnline adapter = new AdapPagoListaOnline(this,
                    R.layout.pagolistaonline_item, ioLista);
            lista.setAdapter(adapter);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                            long arg3) {

        try {
            StringBuffer loStb = new StringBuffer();
            loStb.append(getString(R.string.actdocumentopagoanulado_dlgfin)).append('\n');
            AlertDialog loAlertDialog = null;
            pagoOnline = ioLista.get(position);
            loAlertDialog = new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(getString(R.string.actdocumentopagoanulado_bardetalle))
                    .setMessage(loStb.toString())
                    .setPositiveButton(getString(R.string.dlg_btnsi),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                    try {
                                        Intent intent = new Intent(ActPagoListaOnline.this, ActDocumentoOnline.class);
                                        BeanEnvDocumento bdocumento = new BeanEnvDocumento();
                                        bdocumento.setIdDocTipo("P");
                                        bdocumento.setIdPago(pagoOnline.getPag_pk());
                                        bdocumento.setMontoSoles(pagoOnline.getMonto_pagado_fecha());
                                        bdocumento.setMontoDolares(pagoOnline.getMonto_pagado_fechadolares());
                                        bdocumento.setIdPago(pagoOnline.getPag_pk());
                                        bdocumento.setIdUsuario(loBeanUsuario.getCodVendedor());
                                        bdocumento.setIdCliente(pagoOnline.getCli_codigo());
                                        bdocumento.setCliente(pagoOnline.getCli_nombre());
                                        bdocumento.setFechaPago(pagoOnline.getFecha());
                                        bdocumento.setSerie(pagoOnline.getSerie());
                                        bdocumento.setCorrelativo(pagoOnline.getCorrelativo());
                                        bdocumento.setPagoOnline(pagoOnline);
                                        Bundle bundle = new Bundle();
                                        bundle.putString("fecha", getIntent().getExtras().getString("fecha", ""));
                                        intent.putExtras(bundle);
                                        ((AplicacionEntel) getApplication()).setCurrentDocumento(bdocumento);
                                        startActivityForResult(intent,5);
                                    } catch (Exception e) {
                                        subShowException(e);
                                    }
                                }
                            })
                    .setNegativeButton(getString(R.string.dlg_btncancelar),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {

                                }
                            }).create();
            loAlertDialog.show();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {
        Log.i("subaccdesmensaje", "entrroo");
        if (estadoSincronizar) {
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActMenu.class);

                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }
        }
    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent loIntentLogin = new Intent(ActPagoListaOnline.this,
                        ActLogin.class);
                loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(loIntentLogin);
            }
        });
    }

    @Override
    public void subIniActionBar() {

        this.getActionBarGD().setTitle(
                getString(R.string.actpagoonline_bartitulo) + " ("
                        + getIntent().getExtras().getString("fecha", "") + ")"
                        + " - ("
                        + DateFormat.format("dd-MM-yyyy", Calendar.getInstance().getTime()).toString() + ")"
        );
    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem.STOCK.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        Intent intentl = new Intent(this, ActMenu.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intentl);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub
        DalCliente loDalCliente = new DalCliente(this);

        // Verificamos si existe la tabla.
        boolean lbResult = loDalCliente.existeTable();

        if (lbResult) {
            Intent loInstent = new Intent(ActPagoListaOnline.this,
                    ActClienteBuscar.class);
            loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            BeanExtras loBeanExtras = new BeanExtras();
            loBeanExtras.setFiltro("");
            loBeanExtras.setTipo(ConfiguracionNextel.EnumTipBusqueda.NOMBRE);
            String lsFiltro = "";
            try {
                lsFiltro = BeanMapper.toJson(loBeanExtras, false);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            loInstent.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
            finish();
            startActivity(loInstent);
        } else {
            showDialog(VALIDASINCRONIZACION);
        }

    }

    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = null;
            DalPedido loDalPedido = new DalPedido(this);
            loList = loDalPedido.fnEnvioPedidosPendientes(loBeanUsuario
                    .getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);

                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();

                showDialog(CONSDIASINCRONIZAR);
            } else {
                showDialog(VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub
        String lsBeanUsuario = getSharedPreferences(
                "Actual", MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper
                .fromJson(lsBeanUsuario,
                        BeanUsuario.class);
        this.setMsgOk("Eficiencia Online");

        BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
        loBeanListaEficienciaOnline.setIdResultado(0);
        loBeanListaEficienciaOnline.setResultado("");
        loBeanListaEficienciaOnline.setListaEficiencia(null);
        loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

        new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

        Intent loInstent = new Intent(this, ActKpiDetalle.class);
        loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loInstent);
    }

    @Override
    public void subStockFromSliding() {

    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(
                    getString(R.string.ml_sincronizarantes)));
        }

    }

    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), NexToast.EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }

    @Override
    protected DialogFragmentYesNo.DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        DialogFragmentYesNo loDialog;
        switch (id) {
            case DIALOG_NSERVICES_DOWNLOAD:
                String lsNServices;
                if (ioDALNservices == null)
                    try {
                        ioDALNservices = DALNServices.getInstance(
                                ActPagoListaOnline.this, R.string.db_name);
                    } catch (NexInvalidOperatorActivity.NexInvalidOperatorException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                try {
                    lsNServices = ioDALNservices.fnSelNServicesLabel();
                } catch (Exception e) {
                    Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                    lsNServices = getString(R.string.nservices_label_default);
                }
                loDialog = DialogFragmentYesNo.newInstance(
                        getFragmentManager(),
                        DialogFragmentYesNo.TAG + DIALOG_NSERVICES_DOWNLOAD,
                        getString(R.string.nservices_consultingdownlad).replace(
                                "{0}", lsNServices),
                        DialogFragmentYesNo.EnumLayoutResource.getDefaultIconHelp(this));
                return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog,
                        new DialogFragmentYesNo.OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    subIniNServicesDownload(ioDALNservices
                                            .fnSelNServicesAPK());
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                            }
                        });
            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.actmenu_dlgsincronizar),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog,
                        new DialogFragmentYesNo.OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    String lsBeanUsuario = getSharedPreferences(
                                            "Actual", MODE_PRIVATE).getString(
                                            "BeanUsuario", "");
                                    BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                            .fromJson(lsBeanUsuario,
                                                    BeanUsuario.class);
                                    estadoSincronizar = true;
                                    Log.i("NEXDIALOGYESNO", "tipo actividad: "
                                            + fnTipactividad().ordinal());
                                    new HttpSincronizar(loBeanUsuario
                                            .getCodVendedor(),
                                            ActPagoListaOnline.this,
                                            fnTipactividad()).execute();
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            case VALIDASINCRONIZACION:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"),
                        getString(R.string.actmenuvalsincro_titulo),
                        getString(R.string.actmenuvalsincro_mensaje),
                        R.drawable.boton_error, false, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog, null);

            case VALIDASINCRO:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                        getString(R.string.msgregpendientesenvinfo),
                        R.drawable.boton_informacion, false,
                        DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog, null);

            case ENVIOPENDIENTES:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"),
                        getString(R.string.msgdeseaenviarpendientes),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog,
                        new DialogFragmentYesNo.OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                Intent loIntentEspera = new Intent(
                                        ActPagoListaOnline.this,
                                        ActGrabaPendiente.class);
                                startActivity(loIntentEspera);
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            default:
                return super.onCreateNexDialog(id);
        }

    }

    @Override
    protected Dialog onCreateDialog(int id) {

        AlertDialog loAlertDialog = null;

        switch (id) {
            case CONSNOREGISTROPAGO:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("INFORMACIÓN")
                        .setMessage(getString(R.string.msg_error_pagoonline))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(ActPagoListaOnline.this, ActProductoListaOnline.class));
                                    }
                                }).create();
                return loAlertDialog;
            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences(
                                                    "Actual", MODE_PRIVATE)
                                                    .getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                                    .fromJson(lsBeanUsuario,
                                                            BeanUsuario.class);
                                            estadoSincronizar = true;
                                            Log.i("ALERT DIALOG",
                                                    "tipo actividad: "
                                                            + fnTipactividad()
                                                            .ordinal());
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActPagoListaOnline.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        // .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(
                                getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActPagoListaOnline.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            default:
                return super.onCreateDialog(id);

        }

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }

}
