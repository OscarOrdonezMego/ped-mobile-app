package pe.entel.android.pedidos.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalConfiguracion;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumUrl;
import pe.entel.android.pedidos.util.UtilitarioFirebase;

public class HttpLogin extends HttpConexion {

    private BeanUsuario ioBeanUsuario;
    private Context ioContext;

    // CONSTRUCTOR
    public HttpLogin(Context poContext, BeanUsuario poBeanUsuario) {
        super(poContext, poContext.getString(R.string.hhtplogin_dlglogueo));
        ioBeanUsuario = poBeanUsuario;
        ioContext = poContext;
    }

    @Override
    public boolean OnPreConnect() {
        if (!Utilitario.fnVerSignal(ioContext)) {
            // logueo fuera de linea
            String lsBeanUsuario = ioContext.getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE)
                    .getString("BeanUsuario", "");

            BeanUsuario loBeanUsuario = new Gson().fromJson(lsBeanUsuario, BeanUsuario.class);

            if (ioBeanUsuario != null && ioBeanUsuario.getClave().equals(loBeanUsuario.getClave())) {
                setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDOROK, ioContext.getString(R.string.msg_fuecoblogcorrecto));

                UtilitarioFirebase.logueo(ioContext, "OFF");
            } else {
                setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, ioContext.getString(R.string.msg_fuecoblogincorrecto));
            }
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            BeanUsuario loBeanUsuario = (BeanUsuario) getHttpResponseObject();

            if (loBeanUsuario.getIdResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {

                loBeanUsuario.setFlgEstilo(Configuracion.FLGESTILO);

                String lsBeanUsuario = "";
                try {
                    lsBeanUsuario = new Gson().toJson(loBeanUsuario);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                SharedPreferences loSharedPreferences = ioContext
                        .getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
                SharedPreferences.Editor loEditor = loSharedPreferences.edit();
                loEditor.putString("BeanUsuario", lsBeanUsuario);
                loEditor.commit();

                UtilitarioFirebase.logueo(ioContext, "HTTP");
            }
            setHttpResponseIdMessage(loBeanUsuario.getIdResultado(), loBeanUsuario.getResultado());
        }
    }

    private void subConHttp() {
        try {
            //RestTemplate restTemplate = new RestTemplate();
            //restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            Log.v("URL", Configuracion.fnUrl(ioContext, EnumUrl.LOGIN));
            Log.v("REQUEST:", new Gson().toJson(ioBeanUsuario));

            // Se conecta al Servidor
            //String lsObjeto = restTemplate.postForObject(Configuracion.fnUrl(ioContext, EnumUrl.LOGIN), ioBeanUsuario, String.class);
            String lsObjeto = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, EnumUrl.LOGIN), ioBeanUsuario);

            Log.v("RESPONSE:", lsObjeto);

            BeanUsuario loBeanUsuario = (BeanUsuario) new Gson().fromJson(lsObjeto, BeanUsuario.class);

            DalConfiguracion dalConfiguracion = new DalConfiguracion(ioContext);
            dalConfiguracion.executeQuery(loBeanUsuario.getConfiguracion());

            loBeanUsuario.setConfiguracion(null);

            setHttpResponseObject(loBeanUsuario);

        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, "Error HTTP: " + e.getMessage());
        }
    }

}