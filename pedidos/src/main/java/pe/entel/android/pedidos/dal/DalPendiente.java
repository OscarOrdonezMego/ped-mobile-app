package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import pe.com.nextel.android.dal.DALGestor;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.util.Configuracion;

public class DalPendiente extends DALGestor {
    private Context ioContext;
    int cantPedidos = 0;
    int cantNoPedidos = 0;
    int cantPagos = 0;
    int cantCanjes = 0;
    int cantDev = 0;
    int cantDir = 0;
    int cantProspectos = 0;

    public DalPendiente(Context psClase) throws Exception {
        super(psClase, psClase.getString(R.string.db_name));
        ioContext = psClase;
    }

    public boolean fnExistePendientes(String codigoVendedor) {
        calcularExistenPendientes(codigoVendedor);
        return (cantPedidos == 0 && cantNoPedidos == 0 && cantPagos == 0 && cantCanjes == 0 && cantDev == 0 && cantDir == 0 && cantProspectos == 0) ? false : true;
    }

    public void calcularExistenPendientes(String codigoVendedor) {
        SQLiteDatabase myDB = ioContext.openOrCreateDatabase(ioContext.getString(R.string.db_name), Context.MODE_PRIVATE, null);
        existenNoPedidosPendientes(myDB, codigoVendedor);
        existenPedidosPendientes(myDB, codigoVendedor);
        existenPagosPendientes(myDB, codigoVendedor);
        existenCanjesPendientes(myDB, codigoVendedor);
        existenDevolucionesPendientes(myDB, codigoVendedor);
        existenProspectosPendientes(myDB, codigoVendedor);
        myDB.close();
    }

    public void existenPedidosPendientes(SQLiteDatabase myDB, String codigoVendedor) {

        String[] laColumnas = new String[]{"CodigoPedido", "Empresa",
                "CodigoUsuario", "FechaInicio", "FechaFin", "CondicionVenta",
                "CodigoCliente", "CodigoMotivo", "MontoTotal", "CodDireccion",
                "Latitud", "Longitud", "Celda", "ErrorConexion",
                "ErrorPosicion", "Observacion", "CantidadTotal", "FlgEnviado",
                "MontoTotalSoles", "MontoTotalDolar"}; //@JBELVY

        String lsTablas = "TrCabPedido";
        String lsWhere = "FlgEnviado= '" + Configuracion.FLGREGNOHABILITADO + "' AND CodigoUsuario = '" + codigoVendedor + "'" +
                "and CodigoMotivo = ''";

        Cursor loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    cantPedidos++;
                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
    }

    public void existenNoPedidosPendientes(SQLiteDatabase myDB, String codigoVendedor) {

        String[] laColumnas = new String[]{"CodigoPedido", "Empresa",
                "CodigoUsuario", "FechaInicio", "FechaFin", "CondicionVenta",
                "CodigoCliente", "CodigoMotivo", "MontoTotal", "CodDireccion",
                "Latitud", "Longitud", "Celda", "ErrorConexion",
                "ErrorPosicion", "Observacion", "CantidadTotal", "FlgEnviado",
                "MontoTotalSoles", "MontoTotalDolar"}; //@JBELVY

        String lsTablas = "TrCabPedido";
        String lsWhere = "FlgEnviado= '" + Configuracion.FLGREGNOHABILITADO + "' AND CodigoUsuario = '" + codigoVendedor + "'" +
                "and CodigoMotivo <> ''";

        Cursor loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    cantNoPedidos++;
                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
    }

    public void existenPagosPendientes(SQLiteDatabase myDB, String codigoVendedor) {
        String[] laColumnasPagos = new String[]{"PAG_PK", "COB_CODIGO",
                "USR_CODIGO", "PAG_MONTOPAGO", "PAG_TIPOPAGO",
                "PAG_CODIGOBANCO", "PAG_VOUCHER", "Latitud", "Longitud",
                "Celda", "FechaMovil", "ErrorConexion", "ErrorPosicion",
                "FlgEnviado"};
        String lsTablasPagos = "TrPago";
        String lsWherePagos = "FlgEnviado='" + Configuracion.FLGREGNOHABILITADO + "' and USR_CODIGO = '" + codigoVendedor + "'";

        Cursor loCursorPagos = myDB.query(lsTablasPagos, laColumnasPagos, lsWherePagos, null, null, null, null);

        if (loCursorPagos != null) {
            if (loCursorPagos.moveToFirst()) {
                do {
                    cantPagos++;
                } while (loCursorPagos.moveToNext());
            }
        }

        loCursorPagos.deactivate();
        loCursorPagos.close();
    }

    public void existenCanjesPendientes(SQLiteDatabase myDB, String codigoVendedor) {
        String[] laColumnasCanje = new String[]{"CodigoCanje", "idCuenta",
                "idCliente", "idUsuario", "numeroDocumento", "Latitud",
                "Longitud", "Celda", "fechaMovil", "ErrorConexion",
                "ErrorPosicion"};
        String lsTablasCanje = "TrCabCanje";
        String lsWhereCanje = "FlgEnviado='" + Configuracion.FLGREGNOHABILITADO
                + "' and idUsuario = '" + codigoVendedor + "'";

        Cursor loCursorCanje = myDB.query(lsTablasCanje, laColumnasCanje, lsWhereCanje, null, null, null, null);

        if (loCursorCanje != null) {
            if (loCursorCanje.moveToFirst()) {
                do {
                    cantCanjes++;
                } while (loCursorCanje.moveToNext());
            }
        }

        loCursorCanje.deactivate();
        loCursorCanje.close();
    }

    public void existenDevolucionesPendientes(SQLiteDatabase myDB, String codigoVendedor) {

        String[] laColumnasDev = new String[]{"CodigoDevolucion", "idCuenta",
                "idCliente", "idUsuario", "Latitud", "Longitud", "Celda",
                "fechaMovil", "ErrorConexion", "ErrorPosicion"};
        String lsTablasDev = "TrCabDevolucion";
        String lsWhereDev = "FlgEnviado='" + Configuracion.FLGREGNOHABILITADO
                + "' and idUsuario = '" + codigoVendedor + "'";

        Cursor loCursorDev;
        loCursorDev = myDB.query(lsTablasDev, laColumnasDev, lsWhereDev, null,
                null, null, null);

        if (loCursorDev != null) {
            if (loCursorDev.moveToFirst()) {
                do {
                    cantDev++;

                } while (loCursorDev.moveToNext());
            }
        }

        if (new DalCliente(ioContext).existeTableDireccion()) {
            // PENDIENTE ----------------- DIRECCIONES
            String[] laColumnasDir = new String[]{"CLI_CODIGO"};
            String lsTablasDir = "TBL_DIRECCION";
            String lsWhereDir = "FLGENVIADO = '" + Configuracion.FLGFALSO + "'";

            Cursor loCursorDir = myDB.query(lsTablasDir, laColumnasDir, lsWhereDir, null, null, null, null);

            if (loCursorDir != null) {
                if (loCursorDir.moveToFirst()) {
                    do {
                        cantDir++;
                    } while (loCursorDev.moveToNext());
                }
            }
        }


        loCursorDev.deactivate();
        loCursorDev.close();

    }

    public void existenProspectosPendientes(SQLiteDatabase myDB, String codigoVendedor) {
        try {
            String[] laColumnas = new String[]{"Id", "Codigo"};
            String lsTablas = "TrProspecto";
            String lsWhere = "FlgEnviado='" + Configuracion.FLGFALSO + "'";

            Cursor loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null, null);

            if (loCursor != null) {
                if (loCursor.moveToFirst()) {
                    do {
                        cantProspectos++;
                    } while (loCursor.moveToNext());
                }
            }

            loCursor.deactivate();
            loCursor.close();
        } catch (Exception ex) {
            cantProspectos = 0;
        }
    }

    public int calcularPendientesPedidos(String codigoVendedor) {
        cantPedidos = 0;
        SQLiteDatabase myDB = ioContext.openOrCreateDatabase(ioContext.getString(R.string.db_name), Context.MODE_PRIVATE, null);
        existenPedidosPendientes(myDB, codigoVendedor);
        myDB.close();
        return cantPedidos;
    }

    public int calularPendientesOtros(String codigoVendedor) {
        cantPagos = 0;
        cantCanjes = 0;
        cantDev = 0;
        cantDir = 0;
        cantNoPedidos = 0;
        cantProspectos = 0;

        SQLiteDatabase myDB = ioContext.openOrCreateDatabase(ioContext.getString(R.string.db_name), Context.MODE_PRIVATE, null);
        existenNoPedidosPendientes(myDB, codigoVendedor);
        existenPagosPendientes(myDB, codigoVendedor);
        existenCanjesPendientes(myDB, codigoVendedor);
        existenDevolucionesPendientes(myDB, codigoVendedor);
        existenProspectosPendientes(myDB, codigoVendedor);
        myDB.close();

        return cantPagos + cantCanjes + cantDev + cantDir + cantNoPedidos + cantProspectos;
    }
}
