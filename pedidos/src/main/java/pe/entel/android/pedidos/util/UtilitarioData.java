package pe.entel.android.pedidos.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.channels.FileChannel;
import java.util.Calendar;
import java.util.Date;

import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.NexPhoneStateListener;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.act.ActLogin;


/**
 * Created by alexander on 31/01/2015.
 */
public class UtilitarioData {

    /**
     * Convierte un {@link InputStream} a {String}
     *
     * @param is         - {@link InputStream}
     * @param piLongitud - Longitud de la cadena
     * @return {String}
     * @throws IOException
     */
    public static String fnInputStreamToString(InputStream is, int piLongitud) throws IOException {
        byte[] bytes = new byte[1000];
        StringBuilder x = new StringBuilder(piLongitud);
        int numRead = 0;
        while ((numRead = is.read(bytes)) >= 0) {
            x.append(new String(bytes, 0, numRead));
        }
        return x.toString();
    }

    /**
     * Verifica el estado de la se&#241;al del equipo
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> en caso exista se&#241;al</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnVerSignal(Context poContext) {
        boolean lbResultado;
        boolean lbCon3g = false;
        boolean lbconData = false;
        boolean lbWifi = false;

        TelephonyManager loTelephonyManager; //Para ver redes 3G
        ConnectivityManager loConnectivityManager; //Para ver conectividad en general
        NetworkInfo loNetworkInfo;  //Para ver la informacion de la red (Usado con ConnectivityManager)
        loConnectivityManager = (ConnectivityManager) poContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        loTelephonyManager = (TelephonyManager) poContext.getSystemService(Context.TELEPHONY_SERVICE);
        Log.v("Utilitario", "Operador Net:" + loTelephonyManager.getNetworkOperator());
        Log.v("Utilitario", "Operador Net Name:" + loTelephonyManager.getNetworkOperatorName());
        Log.v("Utilitario", "Operador Sim:" + loTelephonyManager.getSimOperator());
        Log.v("Utilitario", "Operador Sim Name:" + loTelephonyManager.getSimOperatorName());

        if (loTelephonyManager.getDataState() == TelephonyManager.DATA_CONNECTED) {
            lbCon3g = true;

        } else if (loTelephonyManager.getDataState() == TelephonyManager.DATA_DISCONNECTED) {
            lbCon3g = false;
        }
        try {
            //Verifica data
            loNetworkInfo = loConnectivityManager.getActiveNetworkInfo();
            if (loNetworkInfo.isConnected()) {
                lbconData = true;
            } else {
                lbconData = false;
            }

        } catch (SecurityException e) {
            lbconData = false;
        } catch (NullPointerException e) {
            lbconData = false;

        }
        try {
            //verifica wifi
            NetworkInfo loNetworkInfo2 = loConnectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (loNetworkInfo2.isConnected()) {
                lbWifi = true;
            } else {
                lbWifi = false;
            }
        } catch (SecurityException e) {
            Log.v("XXX", e.getMessage());
            lbWifi = false;
        } catch (NullPointerException e) {
            Log.v("XXX", e.getMessage());
            lbWifi = false;

        }
        lbResultado = lbCon3g || lbconData || lbWifi;
        return lbResultado;

    }


    /**
     * Realiza un backup de la base de datos del SQLite de un paquete
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @throws IOException
     */
    @Deprecated
    public static void backupDatabase(Context poContext) throws IOException {
        //Open your local db as the input stream
        String inFileName = "/data/data/" + poContext.getPackageName() + "/databases/database.sqlite";
        File dbFile = new File(inFileName);
        FileInputStream fis = new FileInputStream(dbFile);

        String outFileName = Environment.getExternalStorageDirectory() + "/database.sqlite";
        //Open the empty db as the output stream
        OutputStream output = new FileOutputStream(outFileName);
        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = fis.read(buffer)) > 0) {
            output.write(buffer, 0, length);
        }
        //Close the streams
        output.flush();
        output.close();
        fis.close();
    }

    public static String fnExportDatabaseFileTask(Context ioContext, String isDatabaseName) {
        String mensaje = "";
        try {
            File dbFile = new File(Environment.getDataDirectory() + "/data/" + ioContext.getApplicationContext().getPackageName() + "/databases/" + isDatabaseName);
            File exportDir = new File(Environment.getExternalStorageDirectory(), "");
            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }
            File file = new File(exportDir, dbFile.getName());
            file.createNewFile();

            FileChannel inChannel = new FileInputStream(dbFile).getChannel();
            FileChannel outChannel = new FileOutputStream(file).getChannel();

            inChannel.transferTo(0, inChannel.size(), outChannel);

            if (inChannel != null)
                inChannel.close();
            if (outChannel != null)
                outChannel.close();

            mensaje = "Se ha exportado la Base de Datos exitosamente";

        } catch (Exception e) {
            mensaje = "Error: " + e.getMessage();
        }
        return mensaje;
    }

    /**
     * Redondea un decimal
     *
     * @param pdValor     - N&#250;mero decimal
     * @param piPrecision - Precisi&#243;n de redondeo
     * @return double
     */
    @Deprecated
    public static double fnRedondeo(double pdValor, int piPrecision) {
        BigDecimal bd = new BigDecimal(pdValor);
        BigDecimal rounded = bd.setScale(piPrecision, BigDecimal.ROUND_HALF_UP);
        return rounded.doubleValue();
    }

    /**
     * Retorna una fecha en formato DD/MM/YYYY
     *
     * @param psFecha - Fecha en formato militar YYYYMMDD
     * @return {String}
     */
    @Deprecated
    public static String fnFechaFormato(String psFecha) {
        StringBuffer loSB = new StringBuffer();
        loSB.append(psFecha.substring(6, 8));
        loSB.append("/");
        loSB.append(psFecha.substring(4, 6));
        loSB.append("/");
        loSB.append(psFecha.substring(0, 4));
        return loSB.toString();
    }

    public static int fnSignalLevel(Context context) {
        int signal = 0;
        NexPhoneStateListener listener;
        listener = Utilitario.fnSignalState(context);

        signal = listener.getSignalLevel(context);

        Log.v("XXX", "Señal: " + signal);

        return signal;
    }

    public static boolean fnVerificarSignalDialog(Context context) {

        Activity activity = (Activity) context;

        if (!Utilitario.fnVerSignal(activity) || UtilitarioData.fnSignalLevel(activity) < 2) {

            AlertDialog.Builder ale = new AlertDialog.Builder(activity);
            ale.setTitle(activity.getString(R.string.titlenohatpendiente));
            ale.setMessage(activity.getString(R.string.msg_fueracobertura));
            ale.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            ale.show();

            activity.startActivity(new Intent(activity, ActLogin.class));
            return true;
        }
        return false;
    }

    public static boolean fnVerificarSignalDialogSinLogin(Context context) {

        Activity activity = (Activity) context;
        if (!Utilitario.fnVerSignal(activity) || UtilitarioData.fnSignalLevel(activity) < 2) {
            return true;
        }
        return false;
    }

    public static void fnCrearPreferencesPutString(Context context, String key, String value) {
        SharedPreferences loSharedPreferences = context.getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString(key, value);
        loEditor.commit();
    }

    public static void fnCrearPreferencesPutBoolean(Context context, String key, boolean value) {
        SharedPreferences loSharedPreferences = context.getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putBoolean(key, value);
        loEditor.commit();
    }

    public static String fnObtenerPreferencesString(Context context, String key) {
        return context.getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, context.MODE_PRIVATE).
                getString(key, "");
    }


    public static Boolean fnObtenerPreferencesBoolean(Context context, String key) {
        return context.getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, context.MODE_PRIVATE).
                getBoolean(key, false);
    }

    public static String fnObtenerFechaddMMyyyykkmmss() {
        try {
            Date loDate = Calendar.getInstance().getTime();
            return DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate)
                    .toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static boolean isGoogleMapsInstalled(Context context) {
        try {
            ApplicationInfo info = context.getPackageManager().getApplicationInfo("com.google.android.apps.maps", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }


}
