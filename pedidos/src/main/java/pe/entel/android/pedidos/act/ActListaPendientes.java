package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.LinkedList;

import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.dal.DALNServices;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapListaPendientes;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.MenuLateralAdapter;
import pe.entel.android.pedidos.util.pedidos.MenuLateralPendientesListener;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

/**
 * Created by tkongr on 18/03/2016.
 */
public class ActListaPendientes extends NexActivity implements AdapterView.OnItemClickListener {

    private ListView lista;
    private LinkedList<String> ioLista;
    BeanUsuario loBeanUsuario = null;
    Bundle savedInstanceState;
    private AdapMenuPrincipalSliding menuAdapter;
    private DALNServices ioDALNservices;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);
        this.savedInstanceState = savedInstanceState;
        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);

        setUsingNexMenuDrawerLayout(new MenuLateralAdapter(this, menuAdapter, new MenuLateralPendientesListener(this, loBeanUsuario, ioDALNservices)));
        super.onCreate(savedInstanceState);
        UtilitarioPedidos.IndicarMensajeOkError(this);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(ActListaPendientes.this, ActLogin.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        };

        UtilitarioPedidos.obtenerMenuLateral(this, loBeanUsuario, listener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        subLLenarLista();
    }

    @Override
    protected void subSetControles() {
        super.subSetControles();

        try {
            setActionBarContentView(R.layout.listapendientes);
            lista = (ListView) findViewById(R.id.actlistapendiente_lista);
            lista.setOnItemClickListener(this);
            subLLenarLista();

        } catch (Exception e) {
            subShowException(e);
        }
    }

    public void subLLenarLista() {
        try {
            DalPendiente dalPendiente = new DalPendiente(this);
            ioLista = new LinkedList<String>();
            ioLista.add("Pedidos ( " + dalPendiente.calcularPendientesPedidos(loBeanUsuario.getCodVendedor()) + " )");
            ioLista.add("Otros ( " + dalPendiente.calularPendientesOtros(loBeanUsuario.getCodVendedor()) + " )");

            lista.setAdapter(new AdapListaPendientes(this, R.layout.listapendiente_item, ioLista));

        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setTitle(getString(R.string.actpendientes_bartitulo));
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, ActMenu.class));
        finish();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (ioLista.get(position).contains("Pedidos")) {
            startActivity(new Intent(ActListaPendientes.this, ActListaPedidosPendientes.class));
        } else {
            startActivity(new Intent(ActListaPendientes.this, ActGrabaPendiente.class));
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case Constantes.CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            //estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario.getCodVendedor(), ActListaPendientes.this, fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            //estadoSincronizar = false;
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;

            default:
                return super.onCreateDialog(id);
        }

    }

}
