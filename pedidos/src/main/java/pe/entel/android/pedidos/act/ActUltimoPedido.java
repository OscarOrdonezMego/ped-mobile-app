package pe.entel.android.pedidos.act;

import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanEnvPedidoDet;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.http.HttpUltPedido;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

@SuppressWarnings("unused")
public class ActUltimoPedido extends NexActivity {

    private final int EDIT_ID = 1;
    private final int DELETE_ID = 2;
    private final int CANCEL_ID = 3;

    private final int CONSADDPROD = 0;
    private final int CONSFINAL = 1;
    private final int CONSBACK = 3;
    private final int CONSNOITEMS = 4;
    private final int CONSLIMITEEXCEDIDO = 5;
    private final int CONSVALEDIT = 6;
    private final int CONSOTRO = -1;

    private String iiCodCliente;

    private String isPresentacion;

    BeanEnvPedidoCab loBeanPedidoCab = null;
    BeanEnvPedidoDet loBeanPedidoDet = null;

    BeanUsuario loBeanUsuario = null;
    BeanCliente loBeanCliente = null;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);
        String lsBeanRepresentante = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanRepresentante,
                BeanUsuario.class);

        isPresentacion = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(this);

        super.onCreate(savedInstanceState);
        try {

            Bundle loExtras = getIntent().getExtras();
            iiCodCliente = loExtras.getString("CodCliente");

            loBeanPedidoCab = new BeanEnvPedidoCab();
            loBeanPedidoCab.setCodigoCliente(iiCodCliente);
            loBeanPedidoCab.setTipoArticulo(isPresentacion.equals(Configuracion.FLGVERDADERO)
                    ? Configuracion.TipoArticulo.PRESENTACION
                    : Configuracion.TipoArticulo.PRODUCTO);

            new HttpUltPedido(loBeanPedidoCab, ActUltimoPedido.this,
                    fnTipactividad()).execute();
        } catch (Exception e) {
            subShowException(e);
        }

    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {
        if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK
                || getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {
            Intent loIntent = new Intent(ActUltimoPedido.this, ActProductoUltPedido.class);

            startActivity(loIntent);
        } else if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDORERROR
                || getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDORERROR) {

            loBeanCliente = ((AplicacionEntel) getApplication())
                    .getCurrentCliente();
            Intent loIntent = new Intent(this, ActClienteDetalle.class);
            loIntent.putExtra("IdCliente",
                    Long.parseLong(loBeanCliente.getClipk()));
            finish();
            startActivity(loIntent);

        } else {

            showDialog(CONSOTRO);
            return;
        }
    }

    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        DialogFragmentYesNo loDialog;
        switch (id) {
            case CONSOTRO:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSOTRO"),
                        getString(R.string.actclientebuscar_dlgnodatos),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            default:
                return super.onCreateNexDialog(id);
        }

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case CONSOTRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.actclientebuscar_dlgnodatos))

                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            default:
                return super.onCreateDialog(id);
        }

    }

}
