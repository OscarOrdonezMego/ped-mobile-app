package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by rtamayov on 25/05/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanControl {

    @JsonProperty
    private int codigo;
    @JsonProperty
    private String etiquetaTexto;
    @JsonProperty
    private int Tipo; //ALPHANUMERIC CHECKBOX COMBOBOX DATE DECIMAL NUMBERIC PHOTO DIST__NEX
    @JsonProperty
    private String valor;
    //-----------------------------------------------------//
    @JsonProperty
    private int MaxCaracteres;
    @JsonProperty
    private int orden;
    @JsonProperty
    private int grupo; //
    @JsonProperty
    private String isObligatorio;
    @JsonProperty
    private String valordisplay;
    @JsonProperty
    private int formulario;
    @JsonProperty
    private String isEditable;
    public String getEtiquetaTexto() {
        return etiquetaTexto;
    }

    public void setEtiquetaTexto(String etiquetaTexto) {
        this.etiquetaTexto = etiquetaTexto;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public int getMaxCaracteres() {
        return MaxCaracteres;
    }

    public void setMaxCaracteres(int maxCaracteres) {
        MaxCaracteres = maxCaracteres;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public String getIsObligatorio() {
        return isObligatorio;
    }

    public void setIsObligatorio(String isObligatorio) {
        this.isObligatorio = isObligatorio;
    }

    public String getValordisplay() {
        return valordisplay;
    }

    public void setValordisplay(String valordisplay) {
        this.valordisplay = valordisplay;
    }

    public int getFormulario() {
        return formulario;
    }

    public void setFormulario(int formulario) {
        this.formulario = formulario;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getTipo() {
        return Tipo;
    }

    public void setTipo(int tipo) {
        Tipo = tipo;
    }

    public int getGrupo() {
        return grupo;
    }

    public void setGrupo(int grupo) {
        this.grupo = grupo;
    }

    public String getIsEditable() {
        return isEditable;
    }

    public void setIsEditable(String isEditable) {
        this.isEditable = isEditable;
    }
}
