package pe.entel.android.pedidos.bean;

import android.text.TextUtils;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

import pe.entel.android.pedidos.util.Configuracion;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanUsuario {

    @JsonProperty
    private long idVendedor = 0;
    @JsonProperty
    private String codVendedor = "";
    @JsonProperty
    private String usuario = "";
    @JsonProperty
    private String clave = "";
    @JsonProperty
    private String perfil = "";
    @JsonProperty
    private String nombre = "";
    @JsonProperty
    private String numero = "";

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    @JsonProperty
    private String codCompania = "";
    @JsonProperty
    private String direccion = "";
    @JsonProperty
    private String codCanalVenta = "";
    @JsonProperty
    private String codTipoVendedor = "";
    private String flgEstilo = "";
    //Control de version
    @JsonProperty
    private String currentVersion = "0.0.0";
    @JsonProperty
    private List<String> configuracion;
    @JsonProperty
    private String resultado = "";
    @JsonProperty
    private int idResultado = 0;
    @JsonProperty
    private String flgNServicesObligatorio = Configuracion.FLGVERDADERO;// T

    public List<String> getConfiguracion() {
        return configuracion;
    }

    public void setConfiguracion(List<String> configuracion) {
        this.configuracion = configuracion;
    }

    public String getFlgNServicesObligatorio() {
        return flgNServicesObligatorio;
    }

    public void setFlgNServicesObligatorio(String flgNServicesObligatorio) {
        this.flgNServicesObligatorio = flgNServicesObligatorio;
    }

    public long getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(long idVendedor) {
        this.idVendedor = idVendedor;
    }

    public String getCodVendedor() {
        return codVendedor;
    }

    public void setCodVendedor(String codVendedor) {
        this.codVendedor = codVendedor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public int getIdResultado() {
        return idResultado;
    }

    public void setIdResultado(int idResultado) {
        this.idResultado = idResultado;
    }

    public String getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(String currentVersion) {
        this.currentVersion = currentVersion;
    }

    //    /*inicio comentario*/
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodCanalVenta() {
        return codCanalVenta;
    }

    public void setCodCanalVenta(String codCanalVenta) {
        this.codCanalVenta = codCanalVenta;
    }

    public String getCodTipoVendedor() {
        return codTipoVendedor;
    }

    public void setCodTipoVendedor(String codTipoVendedor) {
        this.codTipoVendedor = codTipoVendedor;
    }

    public String getCodCompania() {
        return codCompania;
    }

    public void setCodCompania(String codCompania) {
        this.codCompania = codCompania;
    }

    public String getFlgEstilo() {
        return flgEstilo;
    }

    public void setFlgEstilo(String flgEstilo) {
        this.flgEstilo = flgEstilo;
    }

}