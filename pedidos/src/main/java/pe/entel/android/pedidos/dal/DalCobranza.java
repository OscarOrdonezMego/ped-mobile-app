package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanCobranzaOnline;
import pe.entel.android.pedidos.bean.BeanEnvPago;
import pe.entel.android.pedidos.bean.BeanPagoOnline;
import pe.entel.android.pedidos.widget.HeadedTextItem;

public class DalCobranza {
    private Context ioContext;

    public DalCobranza(Context psClase) {
        ioContext = psClase;
    }

    public boolean existeTable() {
        boolean lbResult = false;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        String[] laColumnasMed = new String[]{"name"};
        String lsTabla = "sqlite_master";
        String lsWhere = "upper(name)='TBL_COBRANZA'";
        Cursor loCursor = myDB.query(true, lsTabla, laColumnasMed, lsWhere,
                null, null, null, null, null);
        if (loCursor.getCount() == 1) {
            lbResult = true;
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return lbResult;
    }

    public HeadedTextItem[] fnBuscarCobranzas(long poIdCliente,
                                              String psNumDecimales) {
        HeadedTextItem loHeaValor[] = null;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"cob_pk", "cob_numdocumento",
                "cob_montopagado", "cob_montototal", "COB_FECVENCIMIENTO",
                "COB_MONTOPAGADOSOLES", "COB_MONTOTOTALSOLES", "COB_MONTOPAGADODOLARES", "COB_MONTOTOTALDOLARES"};
        String lsTablas = "tbl_cobranza";
        String lsWhere = "cli_pk = " + String.valueOf(poIdCliente);

        Cursor loCursor;

        try {
            loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                    null, "COB_FECVENCIMIENTO");
            loHeaValor = new HeadedTextItem[loCursor.getCount()];

            if (loCursor != null) {
                int liContador = 0;
                if (loCursor.moveToFirst()) {
                    do {
                        Double deuda = new Double(0);
                        Double deudaSoles = new Double(0);
                        Double deudaDolares = new Double(0);

                        deuda = Double.parseDouble(loCursor.getString(3)) - Double.parseDouble(loCursor.getString(2));
                        deudaSoles = Double.parseDouble(loCursor.getString(6)) - Double.parseDouble(loCursor.getString(5));
                        deudaDolares = Double.parseDouble(loCursor.getString(8)) - Double.parseDouble(loCursor.getString(7));

                        loHeaValor[liContador] = new HeadedTextItem(
                                loCursor.getString(1)
                                        + " "
                                        + loCursor.getString(4)
                                        + " : "
                                        + "S/. " + Utilitario.fnRound(deudaSoles, Integer.valueOf(psNumDecimales))
                                        + " 1$. " + Utilitario.fnRound(deudaDolares, Integer.valueOf(psNumDecimales)));

                        loHeaValor[liContador].setTag(loCursor.getLong(0));
                        liContador++;
                    } while (loCursor.moveToNext());
                }
            }
            loCursor.deactivate();
            loCursor.close();
            myDB.close();
        } catch (Exception e) {
            Log.e("ERR", e.getMessage());
        }

        return loHeaValor;
    }

    public String[] fnBuscarClientesConCobranzasSegunUsuario() {
        String loValor[] = null;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"CLI_CODIGO"};
        String lsTablas = "tbl_cobranza";

        Cursor loCursor;

        try {
            loCursor = myDB.query(lsTablas, laColumnas, null, null, null, null, null);
            loValor = new String[loCursor.getCount()];

            if (loCursor != null) {
                int liContador = 0;
                if (loCursor.moveToFirst()) {
                    do {
                        loValor[liContador] = new String(loCursor.getString(0));
                        liContador++;
                    } while (loCursor.moveToNext());
                }
            }
            loCursor.deactivate();
            loCursor.close();
            myDB.close();
        } catch (Exception e) {
            Log.e("ERR", e.getMessage());
        }

        return loValor;
    }

    public ArrayList<HeadedTextItem> fnListBuscarCobranzas(long poIdCliente,
                                                           String numeroDecimales) {
        ArrayList<HeadedTextItem> loHeaValor = new ArrayList<HeadedTextItem>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"cob_pk", "cob_numdocumento",
                "cob_montopagado", "cob_montototal", "COB_FECVENCIMIENTO",
                "COB_MONTOPAGADOSOLES", "COB_MONTOTOTALSOLES", "COB_MONTOPAGADODOLARES", "COB_MONTOTOTALDOLARES"};
        String lsTablas = "tbl_cobranza";

        String lsWhere = "cli_pk = " + String.valueOf(poIdCliente);

        Cursor loCursor;

        try {
            loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                    null, "COB_FECVENCIMIENTO");

            if (loCursor != null) {

                if (loCursor.moveToFirst()) {
                    do {
                        Double deuda = new Double(0);
                        Double deudaSoles = new Double(0);
                        Double deudaDolares = new Double(0);

                        String txtSoles;
                        String txtDolares;
                        String VGuin;

                        deuda = Double.parseDouble(loCursor.getString(3)) - Double.parseDouble(loCursor.getString(2));
                        deudaSoles = Double.parseDouble(loCursor.getString(6)) - Double.parseDouble(loCursor.getString(5));
                        deudaDolares = Double.parseDouble(loCursor.getString(8)) - Double.parseDouble(loCursor.getString(7));

                        if(deudaSoles == 0){
                            txtSoles = "";
                        } else{
                            txtSoles = "S/. " + Utilitario.fnRound(deudaSoles, Integer.valueOf(numeroDecimales));
                        }
                        if(deudaDolares == 0){
                            txtDolares = "";
                        } else{
                            txtDolares = "$ " + Utilitario.fnRound(deudaDolares, Integer.valueOf(numeroDecimales));
                        }

                        if(txtSoles.equals("") && txtDolares.length() > 0){
                            VGuin = "";
                        } else if(txtDolares.equals("") && txtSoles.length() > 0){
                            VGuin = "";
                        } else{
                            VGuin = " - ";
                        }

                        String montos = txtSoles + VGuin + txtDolares;
                        if(!montos.trim().equals("-")) {
                            HeadedTextItem io = new HeadedTextItem(
                                    loCursor.getString(1)
                                            + " "
                                            + loCursor.getString(4)
                                            + " : "
                                            + montos);
                            io.setTag(loCursor.getLong(0));
                            loHeaValor.add(io);
                        }

                    } while (loCursor.moveToNext());
                }
            }
            loCursor.deactivate();
            loCursor.close();
            myDB.close();
        } catch (Exception e) {
            Log.e("ERR", e.getMessage());
        }

        return loHeaValor;
    }

    public BeanEnvPago fnSelxIdCobranza(long piIdCobranza) {
        BeanEnvPago loBeanPago = new BeanEnvPago();
        SQLiteDatabase myDB = null;

        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"cob_pk", "cob_codigo",
                "cob_numdocumento", "cob_montopagado", "cob_montototal",
                "saldo", "cob_fecvencimiento",
                "COB_MONTOPAGADOSOLES", "COB_MONTOTOTALSOLES", "SALDOSOLES",
                "COB_MONTOPAGADODOLARES", "COB_MONTOTOTALDOLARES", "SALDODOLARES" };
        String lsTablas = "tbl_cobranza";
        String lsWhere = "cob_pk = " + String.valueOf(piIdCobranza);
        Cursor loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanPago.setCob_pk(loCursor.getString(0));
                    loBeanPago.setIdDocCobranza(loCursor.getString(1));
                    loBeanPago.setNroDocumento(loCursor.getString(2));
                    loBeanPago.setCob_montopagado(loCursor.getString(3));
                    loBeanPago.setCob_montototal(loCursor.getString(4));
                    loBeanPago.setSaldo(loCursor.getString(5));
                    loBeanPago.setFechaDocumento(loCursor.getString(6));
                    loBeanPago.setCob_montopagadosoles(loCursor.getString(7));
                    loBeanPago.setCob_montototalsoles(loCursor.getString(8));
                    loBeanPago.setSaldosoles(loCursor.getString(9));
                    loBeanPago.setCob_montopagadodolares(loCursor.getString(10));
                    loBeanPago.setCob_montototaldolares(loCursor.getString(11));
                    loBeanPago.setSaldodolares(loCursor.getString(12));
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return loBeanPago;
    }

    /**
     * Actualiza la cobranza
     *
     * @param piIdCob identificador de cliente
     * @param monto   la nueva deuda
     * @return si se realizo correctamente
     */

    public boolean fnUpdCobranzaSoles(String piIdCob, String monto, String saldo) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE tbl_cobranza SET COB_MONTOPAGADOSOLES='")
                .append(monto).append("', SALDOSOLES = '" + saldo)
                .append("' WHERE cob_pk = " + piIdCob);
        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnUpdCobranzaDolares(String piIdCob, String monto, String saldo) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE tbl_cobranza SET COB_MONTOPAGADODOLARES='")
                .append(monto).append("', SALDODOLARES = '" + saldo)
                .append("' WHERE cob_pk = " + piIdCob);
        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public int fnCountPagosOnline() {

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"PAG_PK", "FECHA", "CLI_CODIGO", "CLI_NOMBRE", "COB_NUMDOCUMENTO",
                "COB_TIPODOCUMENTO", "COB_FECVENCIMIENTO", "MONTO_TOTAL", "MONTO_PAGADO_TOTAL",
                "MONTO_PAGADO_FECHA", "MONTO_SALDO", "TIPO_PAGO"};

        String lsTablas = "TBL_REP_PAGO";

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, null, null, null, null,
                "PAG_PK");

        int count = loCursor.getCount();
        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return count;
    }

    public LinkedList<BeanPagoOnline> fnListarPagosOnline() {
        LinkedList<BeanPagoOnline> loLista = new LinkedList<BeanPagoOnline>();

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"PAG_PK", "FECHA", "CLI_CODIGO", "CLI_NOMBRE", "COB_NUMDOCUMENTO",
                "COB_TIPODOCUMENTO", "COB_FECVENCIMIENTO", "MONTO_TOTAL", "MONTO_PAGADO_TOTAL",
                "MONTO_PAGADO_FECHA", "MONTO_SALDO", "TIPO_PAGO",
                "MONTO_TOTAL_DOLARES", "MONTO_PAGADO_TOTAL_DOLARES", "MONTO_PAGADO_FECHA_DOLARES", "MONTO_SALDO_DOLARES",
                "SERIE", "CORRELATIVO", "COB_PK"};
        String lsTablas = "TBL_REP_PAGO";

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, null, null, null, null,
                "PAG_PK");

        if (loCursor != null) {

            if (loCursor.moveToFirst()) {
                do {

                    BeanPagoOnline pago = new BeanPagoOnline();
                    pago.setPag_pk(loCursor.getString(0));
                    pago.setFecha(loCursor.getString(1));
                    pago.setCli_codigo(loCursor.getString(2));
                    pago.setCli_nombre(loCursor.getString(3));
                    pago.setCob_numdocumento(loCursor.getString(4));
                    pago.setCob_tipodocumento(loCursor.getString(5));
                    pago.setCob_fecvencimiento(loCursor.getString(6));
                    pago.setMonto_total(loCursor.getString(7));
                    pago.setMonto_pagado_total(loCursor.getString(8));
                    pago.setMonto_pagado_fecha(loCursor.getString(9));
                    pago.setMonto_saldo(loCursor.getString(10));
                    pago.setTipo_pago(loCursor.getString(11));
                    pago.setMonto_totaldolares(loCursor.getString(12));
                    pago.setMonto_pagado_totaldolares(loCursor.getString(13));
                    pago.setMonto_pagado_fechadolares(loCursor.getString(14));
                    pago.setMonto_saldodolares(loCursor.getString(15));
                    pago.setSerie(loCursor.getString(16));
                    pago.setCorrelativo(loCursor.getString(17));
                    pago.setCobranzaPK(loCursor.getString(18));

                    loLista.add(pago);
                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loLista;
    }

    public int fnBuscarCobranzaVencida(String piIdCliente) {
        BeanEnvPago loBeanPago = new BeanEnvPago();
        SQLiteDatabase myDB = null;
        int vencida = 0;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"cob_pk", "cob_codigo",
                "cob_numdocumento", "cob_montopagado", "cob_montototal",
                "saldo", "cob_fecvencimiento",
                "COB_MONTOPAGADOSOLES", "COB_MONTOTOTALSOLES", "SALDOSOLES",
                "COB_MONTOPAGADODOLARES", "COB_MONTOTOTALDOLARES", "SALDODOLARES" };
        String lsTablas = "tbl_cobranza";
        String lsWhere = "cli_codigo = '" + piIdCliente + "'";
        Cursor loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        Date fechaDoc, fechaMov;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanPago.setCob_pk(loCursor.getString(0));
                    loBeanPago.setIdDocCobranza(loCursor.getString(1));
                    loBeanPago.setNroDocumento(loCursor.getString(2));
                    loBeanPago.setCob_montopagado(loCursor.getString(3));
                    loBeanPago.setCob_montototal(loCursor.getString(4));
                    loBeanPago.setSaldo(loCursor.getString(5));
                    loBeanPago.setFechaDocumento(loCursor.getString(6)); //fechadevencimiento
                    loBeanPago.setCob_montopagadosoles(loCursor.getString(7));
                    loBeanPago.setCob_montototalsoles(loCursor.getString(8));
                    loBeanPago.setSaldosoles(loCursor.getString(9));
                    loBeanPago.setCob_montopagadodolares(loCursor.getString(10));
                    loBeanPago.setCob_montototaldolares(loCursor.getString(11));
                    loBeanPago.setSaldodolares(loCursor.getString(12));
                    loBeanPago.setFechaMovil(new SimpleDateFormat("dd/MM/yyyy").format(new Date()));

                    try {
                        fechaDoc = dateFormat.parse(loBeanPago.getFechaDocumento());
                        fechaMov = dateFormat.parse(loBeanPago.getFechaMovil());
                        Log.i("loBeanPago.getSaldo", loBeanPago.getSaldo());
                        if (fechaMov.after(fechaDoc) && Float.parseFloat(loBeanPago.getSaldo()) > 0) {
                            vencida++;
                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return vencida;
    }

    public boolean fnInsertarCobranzaOnline(BeanCobranzaOnline pBeanCobranza) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();

        if (!fnExisteCobranzaOnline(pBeanCobranza.getCobPk())) {
            loStbCab.append("INSERT INTO tbl_cobranza (COB_PK,COB_CODIGO,COB_NUMDOCUMENTO,COB_TIPODOCUMENTO,COB_MONTOPAGADO,COB_MONTOTOTAL,COB_FECVENCIMIENTO,COB_SERIE,CLI_CODIGO,COB_EMPRESA,COB_USUARIO,CLI_PK,SALDO,COB_MONTOPAGADOSOLES,COB_MONTOTOTALSOLES,SALDOSOLES,COB_MONTOPAGADODOLARES,COB_MONTOTOTALDOLARES,SALDODOLARES)")
                    .append("VALUES ('")
                    .append(pBeanCobranza.getCobPk()).append("','")
                    .append(pBeanCobranza.getCobCodigo()).append("','")
                    .append(pBeanCobranza.getCobNumDocumento()).append("','")
                    .append(pBeanCobranza.getCobTipoDocumento()).append("','")
                    .append(pBeanCobranza.getCobMontoPagado()).append("','")
                    .append(pBeanCobranza.getCobMontoTotal()).append("','")
                    .append(pBeanCobranza.getCobFecVencimiento()).append("','")
                    .append(pBeanCobranza.getCobSerie()).append("','")
                    .append(pBeanCobranza.getCliCodigo()).append("','")
                    .append(pBeanCobranza.getCobEmpresa()).append("','")
                    .append(pBeanCobranza.getCobUsuario()).append("','")
                    .append(pBeanCobranza.getCliPK()).append("','")
                    .append(pBeanCobranza.getSaldo()).append("','")
                    .append(pBeanCobranza.getCobMontoPagadosoles()).append("','")
                    .append(pBeanCobranza.getCobMontoTotalsoles()).append("','")
                    .append(pBeanCobranza.getSaldosoles()).append("','")
                    .append(pBeanCobranza.getCobMontoPagadodolares()).append("','")
                    .append(pBeanCobranza.getCobMontoTotaldolares()).append("','")
                    .append(pBeanCobranza.getSaldodolares()).append("');");

            Log.v("XXX", "insertando..." + loStbCab.toString());
            try {
                myDB.execSQL(loStbCab.toString());
            } catch (Exception e) {
                lbResult = false;
            }
            myDB.close();
        }
        return lbResult;
    }

    public boolean fnExisteCobranzaOnline(String piIdCobranza) {
        SQLiteDatabase myDB = null;

        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"cob_pk", "cob_codigo",
                "cob_numdocumento", "cob_montopagado", "cob_montototal",
                "saldo", "cob_fecvencimiento",
                "COB_MONTOPAGADOSOLES", "COB_MONTOTOTALSOLES", "SALDOSOLES",
                "COB_MONTOPAGADODOLARES", "COB_MONTOTOTALDOLARES", "SALDODOLARES"};
        String lsTablas = "tbl_cobranza";
        String lsWhere = "cob_pk = " + piIdCobranza;

        Cursor loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                return true;
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return false;
    }

}
