package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rtamayov on 24/09/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanRespVerificarPedidoGeneral {

    @JsonProperty("DetallesPedidos")
    private List<BeanVerificarPedidoDetalle> detallesPedidos;
    @JsonProperty("Mensajes")
    private List<BeanVerificarPedidoMensaje> mensajes;

    public List<BeanVerificarPedidoDetalle> getDetallesPedidos() {
        return detallesPedidos;
    }

    public void setDetallesPedidos(List<BeanVerificarPedidoDetalle> detallesPedidos) {
        this.detallesPedidos = detallesPedidos;
    }

    public List<BeanVerificarPedidoMensaje> getMensajes() {
        return mensajes;
    }

    public void setMensajes(List<BeanVerificarPedidoMensaje> mensajes) {
        this.mensajes = mensajes;
    }

    public List<BeanEnvPedidoDet> mapperListaVerificarPedidoABeanEnvPedidoDetalle() {
        List<BeanEnvPedidoDet> beans = new ArrayList<BeanEnvPedidoDet>();

        for (BeanVerificarPedidoDetalle detalle : detallesPedidos) {
            BeanEnvPedidoDet bean = new BeanEnvPedidoDet();
            bean.setAtendible(detalle.getAtendible());
            bean.setBonificacion(detalle.getBonificacion());
            bean.setBonificacionFrac(detalle.getBonificacionFrac());
            bean.setCantBonificacion(detalle.getCodBonificacion());
            bean.setCantidad(detalle.getCantidad());
            bean.setCantidadFrac(detalle.getCantidadFrac());
            bean.setCantidadPre(detalle.getCantidadPre());
            bean.setCodAlmacen(detalle.getCodAlmacen());
            bean.setCodBonificacion(detalle.getCodBonificacion());
            bean.setCodListaArticulo(detalle.getCodListaArticulo());
            bean.setCodPedido(detalle.getCodPedido());
            bean.setCodigoArticulo(detalle.getCodigoArticulo());
            bean.setDescuento(detalle.getDescuento());
            bean.setEmpresa(detalle.getEmpresa());
            bean.setFechaUltVenta(detalle.getFechaUltVenta());
            bean.setFlete(detalle.getFlete());
            bean.setIdArticulo(detalle.getIdArticulo());
            bean.setIdListaPrecio(detalle.getIdListaPrecio());
            bean.setMonto(detalle.getMonto());
            bean.setMontoSinDescuento(detalle.getMontoSinDescuento());
            bean.setMultBonificacion(detalle.getMultBonificacion());
            bean.setNombreArticulo(detalle.getNombreArticulo());
            bean.setObservacion(detalle.getObservacion());
            bean.setPrecioBase(detalle.getPrecioBase());
            bean.setPrecioFrac(detalle.getPrecioFrac());
            bean.setStock(detalle.getStock());
            bean.setTipo(detalle.getTipo());
            bean.setTipoDescuento(detalle.getTipoDescuento());
            bean.setUltPrecio(detalle.getUltPrecio());
            bean.setEditBonificacion(detalle.isEditBonificacion());
            bean.setProducto(detalle.getProducto());
            beans.add(bean);
        }

        return beans;
    }
}
