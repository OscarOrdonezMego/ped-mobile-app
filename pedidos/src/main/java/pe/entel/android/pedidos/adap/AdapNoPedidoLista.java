package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import greendroid.widget.item.DrawableItem;
import pe.entel.android.pedidos.R;

public class AdapNoPedidoLista extends ArrayAdapter<DrawableItem> {
    int resource;
    Context context;
    TextView rowTextView;

    ArrayList<DrawableItem> originalList;

    int cont = 0;

    public AdapNoPedidoLista(Context _context, int _resource,
                             ArrayList<DrawableItem> lista) {
        super(_context, _resource, lista);
        originalList = new ArrayList<DrawableItem>();
        originalList.addAll(lista);
        resource = _resource;
        context = _context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        DrawableItem item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        ((TextView) nuevaVista
                .findViewById(R.id.actmotnopedidolista_textTitulo))
                .setText(item.getText());

        return nuevaVista;
    }

}
