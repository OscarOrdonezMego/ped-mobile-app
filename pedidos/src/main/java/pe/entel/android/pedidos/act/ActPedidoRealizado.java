package pe.entel.android.pedidos.act;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.Toast;

import greendroid.widget.ActionBarGD.Type;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanConsulta;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.http.HttpPedidoRealizado;

public class ActPedidoRealizado extends NexActivity {

    BeanUsuario loBeanUsuario = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String lsBeanRepresentante = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanRepresentante,
                BeanUsuario.class);

        BeanConsulta oConsulta = new BeanConsulta();
        oConsulta.setCodVendedor(loBeanUsuario.getCodVendedor());

        new HttpPedidoRealizado(oConsulta, this, this.fnTipactividad())
                .execute();

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            default:
                return super.onCreateDialog(id);
        }
    }

    public void subIniActionBar() {
        this.getActionBarGD().setType(Type.Empty);
    }

    @Override
    public void subSetControles() {
        setActionBarContentView(R.layout.enviohttp);
        super.subSetControles();
    }

    @Override
    public void subAccDesMensaje() {
        Intent intentl = new Intent(ActPedidoRealizado.this,
                ActDetalleOnline.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP); // JUAN

        if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
            finish();
            startActivity(intentl);
        } else {
            Toast loToast = Toast.makeText(this,
                    getString(R.string.actpedidorealizado_dlgerror),
                    Toast.LENGTH_LONG);
            loToast.setGravity(Gravity.CENTER, 0, 0);
            loToast.show();
            finish();
            startActivity(new Intent(ActPedidoRealizado.this, ActMenu.class));
        }
    }
}
