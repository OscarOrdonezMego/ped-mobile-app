package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.Gps;
import pe.com.nextel.android.util.Logger;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout;
import pe.com.nextel.android.widget.NexToast;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanGeneral;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanProspecto;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalGeneral;
import pe.entel.android.pedidos.http.HttpEditarCliente;
import pe.entel.android.pedidos.http.HttpEnviarProspecto;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.UtilitarioData;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

import android.view.View.OnClickListener;
import android.widget.Toast;

import java.util.List;

public class ActClienteEditar extends NexActivity implements OnClickListener, NexMenuCallbacks, MenuSlidingListener {

    private BeanUsuario loBeanUsuario = null;
    private BeanProspecto beanEditCliente = null;
    BeanCliente loBeanCliente;
    private long iiIdCliente;

    private EditText etxtCodigo, etxtNombre, etxtDireccion, etxtGiro, etxtObservacion, etxtColAdic1, etxtColAdic2, etxtColAdic3, etxtColAdic4, etxtColAdic5;
    private TextView lblObservacion, txtColAdic1, txtColAdic2, txtColAdic3, txtColAdic4, txtColAdic5;
    private LinearLayout lnlyColAdic1, lnlyColAdic2, lnlyColAdic3, lnlyColAdic4, lnlyColAdic5;
    private Button btnEnviar, spiTipoCliente;

    private String isDesColumAdic1, isDesColumAdic2, isDesColumAdic3, isDesColumAdic4, isDesColumAdic5;
    private String isValColumAdic1, isValColumAdic2, isValColumAdic3, isValColumAdic4, isValColumAdic5;

    private final int CONSBACK = 3;
    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDAHOME = 11;
    private final int VALIDASINCRONIZACION = 12;
    private final int ENVIOPENDIENTES = 13;
    private final int VALIDASINCRO = 14;
    private final int CONSSALIR = 15;
    private final int NOHAYPENDIENTES = 16;
    private final int ERRORGENERAR = 17;
    public int RESULTHTTP = 1;
    int estadoSalir = 0;
    boolean estadoSincronizar = false;
    private String flgGps;
    private AdapMenuPrincipalSliding _menu;
    private DALNServices ioDALNservices;

    private final int POPUP_TIPOCLIENTE = 10;
    private DalGeneral loDalGeneral;
    private List ioLista;
    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);
        String lsBeanRepresentante = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanRepresentante,
                BeanUsuario.class);

        isValColumAdic1 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_1.getValor(this);
        isValColumAdic2 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_2.getValor(this);
        isValColumAdic3 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_3.getValor(this);
        isValColumAdic4 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_4.getValor(this);
        isValColumAdic5 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_5.getValor(this);
        isDesColumAdic1 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_1.getDescripcion(this);
        isDesColumAdic2 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_2.getDescripcion(this);
        isDesColumAdic3 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_3.getDescripcion(this);
        isDesColumAdic4 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_4.getDescripcion(this);
        isDesColumAdic5 = Configuracion.EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_5.getDescripcion(this);

        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);
        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));
        subIniMenu();
    }

    @Override
    public void subSetControles() {
        setActionBarContentView(R.layout.act_prospecto);
        super.subSetControles();

        etxtCodigo = findViewById(R.id.actprospecto_txtcodigo);
        etxtNombre = findViewById(R.id.actprospecto_txtnombre);
        etxtDireccion = findViewById(R.id.actprospecto_txtdireccion);
        etxtGiro = findViewById(R.id.actprospecto_txtgiro);

        lblObservacion = findViewById(R.id.actprospecto_lblobservacion);
        etxtObservacion = findViewById(R.id.actprospecto_txtobservacion);
        btnEnviar = findViewById(R.id.actprospecto_btnenviar);
        spiTipoCliente = findViewById(R.id.actprospecto_cmbTipoCliente);

        lnlyColAdic1 = findViewById(R.id.actprospecto_lnlyColAdic1);
        lnlyColAdic2 = findViewById(R.id.actprospecto_lnlyColAdic2);
        lnlyColAdic3 = findViewById(R.id.actprospecto_lnlyColAdic3);
        lnlyColAdic4 = findViewById(R.id.actprospecto_lnlyColAdic4);
        lnlyColAdic5 = findViewById(R.id.actprospecto_lnlyColAdic5);

        txtColAdic1 = findViewById(R.id.actprospecto_lblColAdic1);
        txtColAdic2 = findViewById(R.id.actprospecto_lblColAdic2);
        txtColAdic3 = findViewById(R.id.actprospecto_lblColAdic3);
        txtColAdic4 = findViewById(R.id.actprospecto_lblColAdic4);
        txtColAdic5 = findViewById(R.id.actprospecto_lblColAdic5);

        etxtColAdic1 = findViewById(R.id.actprospecto_txtColAdic1);
        etxtColAdic2 = findViewById(R.id.actprospecto_txtColAdic2);
        etxtColAdic3 = findViewById(R.id.actprospecto_txtColAdic3);
        etxtColAdic4 = findViewById(R.id.actprospecto_txtColAdic4);
        etxtColAdic5 = findViewById(R.id.actprospecto_txtColAdic5);

        mostrarCamposAdicionales();

        btnEnviar.setOnClickListener(this);
        cargarDetalle();
    }

    private void cargarDetalle() {
        Log.d("XXX", "ActClienteDetalle -> subCargaDetalle ");
        if (((AplicacionEntel) getApplication()).getCurrentCliente() != null) {
            if (((AplicacionEntel) getApplication()).getClienteOnline() == 0) {
                loBeanCliente = ((AplicacionEntel) getApplication()).getCurrentCliente();
            }

        } else {

            if (((AplicacionEntel) getApplication()).getClienteOnline() == 0) {
                Bundle loExtras = getIntent().getExtras();
                iiIdCliente = loExtras.getLong("IdCliente");
                loBeanCliente = new DalCliente(this).fnSelxIdCliente(iiIdCliente);
                ((AplicacionEntel) getApplication()).setCurrentCliente(loBeanCliente);
            } else {
                loBeanCliente = UtilitarioPedidos.obtenerBeanClienteOnlineDePreferences(this);
                ((AplicacionEntel) getApplication()).setCurrentCliente(loBeanCliente);
            }
        }
        beanEditCliente = new BeanProspecto();
        etxtCodigo.setText(loBeanCliente.getClicod());
        etxtNombre.setText(loBeanCliente.getClinom());
        etxtDireccion.setText(loBeanCliente.getClidireccion());
        etxtGiro.setText(loBeanCliente.getCligiro());
        spiTipoCliente.setText(loBeanCliente.getTclinombre());
        etxtColAdic1.setText(loBeanCliente.getCampoAdicional1());
        etxtColAdic2.setText(loBeanCliente.getCampoAdicional2());
        etxtColAdic3.setText(loBeanCliente.getCampoAdicional3());
        etxtColAdic4.setText(loBeanCliente.getCampoAdicional4());
        etxtColAdic5.setText(loBeanCliente.getCampoAdicional5());

        etxtCodigo.setEnabled(false);
        etxtNombre.setEnabled(false);
        spiTipoCliente.setEnabled(false);
        lblObservacion.setVisibility(View.GONE);
        etxtObservacion.setVisibility(View.GONE);
    }

    private void mostrarCamposAdicionales() {
        if (isValColumAdic1.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic1.setVisibility(View.VISIBLE);
            txtColAdic1.setText(isDesColumAdic1);
        } else
            lnlyColAdic1.setVisibility(View.GONE);

        if (isValColumAdic2.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic2.setVisibility(View.VISIBLE);
            txtColAdic2.setText(isDesColumAdic2);
        } else
            lnlyColAdic2.setVisibility(View.GONE);

        if (isValColumAdic3.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic3.setVisibility(View.VISIBLE);
            txtColAdic3.setText(isDesColumAdic3);
        } else
            lnlyColAdic3.setVisibility(View.GONE);

        if (isValColumAdic4.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic4.setVisibility(View.VISIBLE);
            txtColAdic4.setText(isDesColumAdic4);
        } else
            lnlyColAdic4.setVisibility(View.GONE);

        if (isValColumAdic5.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic5.setVisibility(View.VISIBLE);
            txtColAdic5.setText(isDesColumAdic5);
        } else
            lnlyColAdic5.setVisibility(View.GONE);
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setTitle(getString(R.string.acteditarcliente_bartitle));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.actprospecto_btnenviar) {
            if (String.valueOf(etxtCodigo.getText()).equals("")
                    || String.valueOf(etxtDireccion.getText()).equals("")
                    || String.valueOf(etxtGiro.getText()).equals("")) {
                Toast.makeText(this, "Debe de llenar todos campos del formulario", Toast.LENGTH_SHORT).show();
            } else {


                beanEditCliente.setCodigo(String.valueOf(etxtCodigo.getText()));
                beanEditCliente.setNombre(String.valueOf(etxtNombre.getText()));
                beanEditCliente.setDireccion(String.valueOf(etxtDireccion.getText()));
                beanEditCliente.setCodUsuario(loBeanUsuario.getCodVendedor());
                beanEditCliente.setGiro(String.valueOf(etxtGiro.getText()));
                beanEditCliente.setCampoAdicional1(String.valueOf(etxtColAdic1.getText()));
                beanEditCliente.setCampoAdicional2(String.valueOf(etxtColAdic2.getText()));
                beanEditCliente.setCampoAdicional3(String.valueOf(etxtColAdic3.getText()));
                beanEditCliente.setCampoAdicional4(String.valueOf(etxtColAdic4.getText()));
                beanEditCliente.setCampoAdicional5(String.valueOf(etxtColAdic5.getText()));
                beanEditCliente.setFecha(UtilitarioData.fnObtenerFechaddMMyyyykkmmss());

                try {
                    beanEditCliente.setLatitud(String.valueOf(Gps.getLocation().getLatitude()));
                    beanEditCliente.setLongitud(String.valueOf(Gps.getLocation().getLongitude()));
                } catch (Exception e) {
                    e.printStackTrace();
                    beanEditCliente.setLatitud("0");
                    beanEditCliente.setLongitud("0");
                }

                try {
                    new HttpEditarCliente(beanEditCliente, ActClienteEditar.this, fnTipactividad()).execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void subAccDesMensaje() {
        if (estadoSincronizar) {
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActClienteDetalle.class);
                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }
        } else {
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                setResult(RESULT_OK);
                finish();
            } else {
                new NexToast(this, R.string.acteditacliente_dlgerrorsave,
                        NexToast.EnumType.ERROR).show();
            }
        }
    }


    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new View.OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                estadoSalir = 3;
                showDialog(CONSSALIR);
            }
        });
    }
    
    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        showDialog(VALIDAHOME);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @SuppressWarnings("deprecation")
    @Override
    public void subSincronizarFromSliding() {
        try {
            showDialog(VALIDASINCRO);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subPendientesFromSliding() {
        try {

        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub
        estadoSalir = 1;
        showDialog(CONSSALIR);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        estadoSalir = 2;
        showDialog(CONSSALIR);
    }

    private void salir() {

        if (estadoSalir == 2) {
            ((AplicacionEntel) getApplication()).setCurrentCliente(null);
            ((AplicacionEntel) getApplication())
                    .setCurrentCodFlujo(Configuracion.CONSSTOCK);
            ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
            Intent loIntento = new Intent(ActClienteEditar.this,
                    ActCobranzaMonto.class);
            loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            BeanExtras loBeanExtras = new BeanExtras();
            loBeanExtras.setFiltro("");
            loBeanExtras.setTipo(ConfiguracionNextel.EnumTipBusqueda.NOMBRE);
            String lsFiltro = "";
            try {
                lsFiltro = BeanMapper.toJson(loBeanExtras, false);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
            startActivity(loIntento);
        } else if (estadoSalir == 1) {

            String lsBeanUsuario = getSharedPreferences(
                    "Actual", MODE_PRIVATE).getString(
                    "BeanUsuario", "");
            loBeanUsuario = (BeanUsuario) BeanMapper
                    .fromJson(lsBeanUsuario,
                            BeanUsuario.class);
            this.setMsgOk("Eficiencia Online");

            BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
            loBeanListaEficienciaOnline.setIdResultado(0);
            loBeanListaEficienciaOnline.setResultado("");
            loBeanListaEficienciaOnline.setListaEficiencia(null);
            loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

            new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

            Intent loInstent = new Intent(this, ActKpiDetalle.class);
            loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(loInstent);
        } else if (estadoSalir == 3) {
            Intent loIntentLogin = new Intent(ActClienteEditar.this,
                    ActLogin.class);
            loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loIntentLogin);
        }
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(
                    getString(R.string.ml_sincronizarantes)));
        }
    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */
    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), NexToast.EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        showDialog(Constantes.DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */
    @Override
    protected DialogFragmentYesNo.DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        try {
            DialogFragmentYesNo loDialog;
            switch (id) {
                case Constantes.DIALOG_NSERVICES_DOWNLOAD:
                    String lsNServices;
                    if (ioDALNservices == null)
                        ioDALNservices = DALNServices.getInstance(this, R.string.db_name);
                    try {
                        lsNServices = ioDALNservices.fnSelNServicesLabel();
                    } catch (Exception e) {
                        Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                        lsNServices = getString(R.string.nservices_label_default);
                    }
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG
                                    + Constantes.DIALOG_NSERVICES_DOWNLOAD,
                            getString(R.string.nservices_consultingdownlad)
                                    .replace("{0}", lsNServices),
                            DialogFragmentYesNo.EnumLayoutResource.getDefaultIconHelp(this));
                    return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog,
                            new DialogFragmentYesNo.OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(
                                        DialogFragmentYesNo dialog) {
                                    try {
                                        subIniNServicesDownload(ioDALNservices.fnSelNServicesAPK());
                                    } catch (Exception e) {
                                        subShowException(e);
                                    }
                                }

                                @Override
                                public void performButtonNo(
                                        DialogFragmentYesNo dialog) {
                                }
                            });
                case CONSBACK:
                    loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                            DialogFragmentYesNo.TAG.concat("CONSBACK"),
                            getString(R.string.actproductopedido_dlgback),
                            R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog,
                            new DialogFragmentYesNo.OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(DialogFragmentYesNo dialog) {
                                    finish();
                                    startActivity(new Intent(ActClienteEditar.this, ActClienteDetalle.class));
                                }

                                @Override
                                public void performButtonNo(DialogFragmentYesNo dialog) {

                                }
                            });
                default:
                    return super.onCreateNexDialog(id);
            }
        } catch (Exception e) {
            subShowException(e);
            return super.onCreateNexDialog(id);
        }
    }

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                Constantes.REQUEST_NSERVICES);
    }

    protected void NexShowDialog(int id) {
        DialogFragmentYesNo loDialog;
        switch (id) {
            case VALIDAHOME:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDAHOME"),
                        getString(R.string.actproductopedido_dlgback),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDAHOME"))
                        == DialogFragmentYesNo.YES) {

                    Intent intentl = new Intent(ActClienteEditar.this,
                            ActMenu.class);
                    intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    startActivity(intentl);
                }
                break;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.actmenu_dlgsincronizar),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"))
                        == DialogFragmentYesNo.YES) {
                    try {
                        String lsBeanUsuario = getSharedPreferences("Actual",
                                MODE_PRIVATE).getString("BeanUsuario", "");
                        BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                .fromJson(lsBeanUsuario, BeanUsuario.class);
                        estadoSincronizar = true;
                        new HttpSincronizar(loBeanUsuario.getCodVendedor(),
                                ActClienteEditar.this, fnTipactividad())
                                .execute();
                    } catch (Exception e) {
                        subShowException(e);
                    }
                }
                break;

            case VALIDASINCRONIZACION:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"),
                        getString(R.string.actmenuvalsincro_titulo),
                        getString(R.string.actmenuvalsincro_mensaje),
                        R.drawable.boton_error, false, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"));
                break;

            case VALIDASINCRO:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                        getString(R.string.msgregpendientesenvinfo),
                        R.drawable.boton_error, false, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"));
                break;

            case ENVIOPENDIENTES:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"),
                        getString(R.string.msgdeseaenviarpendientes),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"))
                        == DialogFragmentYesNo.YES) {
                    UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActClienteEditar.this);
                }
                break;

            case CONSSALIR:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSSALIR"),
                        getString(R.string.actproductopedido_dlgsalir),
                        R.drawable.boton_ayuda, DialogFragmentYesNo.EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSSALIR"))
                        == DialogFragmentYesNo.YES) {
                    salir();
                }
                break;
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDAHOME:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        Intent intentl = new Intent(
                                                ActClienteEditar.this,
                                                ActMenu.class);
                                        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                        startActivity(intentl);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences(
                                                    "Actual", MODE_PRIVATE)
                                                    .getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                                    .fromJson(lsBeanUsuario,
                                                            BeanUsuario.class);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActClienteEditar.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActClienteEditar.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSBACK:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        Intent loIntent = new Intent(ActClienteEditar.this, ActProductoPedido.class);
                                        finish();
                                        startActivity(loIntent);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case CONSSALIR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgsalir))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        salir();
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case ERRORGENERAR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titerror))
                        .setMessage(getString(R.string.actdocumentocorrelativo_dlgerrorcorrelativo))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        Intent intent = new Intent(ActClienteEditar.this, ActCobranzaMonto.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                    }
                                }).create();
                return loAlertDialog;
            default:
                return super.onCreateDialog(id);
        }

    }
}
