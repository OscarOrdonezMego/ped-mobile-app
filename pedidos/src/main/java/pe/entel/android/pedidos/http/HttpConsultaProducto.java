package pe.entel.android.pedidos.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanConsultaProducto;
import pe.entel.android.pedidos.bean.BeanResponseConsultaProducto;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumUrl;
import pe.entel.android.pedidos.util.UtilitarioData;

public class HttpConsultaProducto extends HttpConexion {

    private BeanConsultaProducto ioBeanConsultaProducto;
    private Context ioContext;

    // CONSTRUCTOR
    public HttpConsultaProducto(BeanConsultaProducto poBeanConsultaProducto, Context poContext, EnumTipActividad poEnumTipActividad) {
        super(poContext, poContext
                .getString(R.string.httpproductoonline_dlgbuscqueda), poEnumTipActividad);
        ioBeanConsultaProducto = poBeanConsultaProducto;
        ioContext = poContext;
    }

    public HttpConsultaProducto(BeanConsultaProducto poBeanConsultaProducto, Context poContext, EnumTipActividad poEnumTipActividad, String mensaje) {
        super(poContext, mensaje, poEnumTipActividad);
        ioBeanConsultaProducto = poBeanConsultaProducto;
        ioContext = poContext;
    }

    public void subConHttp() {
        try {
            //RestTemplate restTemplate = new RestTemplate();
            //restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            Log.v("URL", Configuracion.fnUrl(ioContext, EnumUrl.CONSULTAPRODUCTO));
            Log.v("REQUEST", BeanMapper.toJson(ioBeanConsultaProducto, false));

            //String lsObjeto = restTemplate.postForObject(Configuracion.fnUrl(ioContext, EnumUrl.CONSULTAPRODUCTO), ioBeanConsultaProducto, String.class);
            String lsObjeto = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, EnumUrl.CONSULTAPRODUCTO), ioBeanConsultaProducto);
            Log.v("RESPONSE", BeanMapper.toJson(lsObjeto, false));

            BeanResponseConsultaProducto loBeanResponse = (BeanResponseConsultaProducto) BeanMapper.fromJson(lsObjeto, BeanResponseConsultaProducto.class);

            setHttpResponseObject(loBeanResponse);
        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, "Error HTTP: " + e.getMessage());
        }
    }

    @Override
    public boolean OnPreConnect() {
        if (!UtilitarioData.fnObtenerPreferencesBoolean(ioContext, "pedidoPendiente")) {
            SharedPreferences loSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ioContext);
            if (loSharedPreferences.getBoolean(ioContext.getString(R.string.keypre_cobertura), false)) {
                setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ALGUNERROR, ioContext.getString(R.string.msg_modofueracobertura_consultaproducto));
                return false;
            }
        }
        if (!Utilitario.fnVerSignal(ioContext)) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORALGUNERROR, ioContext.getString(R.string.error_consultaproducto_fueradecobertura));
            return false;
        }
        return true;
    }


    @Override
    public void OnConnect() {
        subConHttp();
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            BeanResponseConsultaProducto loBeanResponse = (BeanResponseConsultaProducto) getHttpResponseObject();
            int liIdHttprespuesta = loBeanResponse.getCodigo();
            String lsHttpRespuesta = loBeanResponse.getMensaje();

            if (liIdHttprespuesta == ConfiguracionNextel.CONSRESSERVIDOROK
                    || liIdHttprespuesta == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {

                ((AplicacionEntel) ioContext.getApplicationContext()).setCurrentConsultaProducto(loBeanResponse.getGeneral());
                if (TextUtils.isEmpty(loBeanResponse.getGeneral().getStock()) || TextUtils.isEmpty(loBeanResponse.getGeneral().getPrecio())) {
                    liIdHttprespuesta = ConfiguracionNextel.CONSRESSERVIDORERROR;
                    lsHttpRespuesta = ioContext.getString(R.string.error_consultaproducto_data);
                }
            } else {
                ((AplicacionEntel) ioContext.getApplicationContext()).setCurrentConsultaProducto(new BeanConsultaProducto());
            }
            setHttpResponseIdMessage(liIdHttprespuesta, lsHttpRespuesta);
        }
    }
}