package pe.entel.android.pedidos.util;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.CustomEvent;

import pe.com.nextel.android.util.Logger;
import pe.com.nextel.android.util.Utilitario;

import static pe.entel.android.pedidos.util.Configuracion.NUMERO_TELEFONO;

public class FabricLibrary {

    public static void CrashlyticsLog(Context context, String log) {
        try {
            InicializarFabric(context);
            Crashlytics.log(log);
        } catch (Exception ex) {
            Logger.e("Error de FabricLibrary", ex);
        }
    }

    public static void InicializarFabric(Context context) {
        if (!io.fabric.sdk.android.Fabric.isInitialized()) {
            io.fabric.sdk.android.Fabric.with(context, new Crashlytics());
        }
    }

    public static void CrashlyticsLogException(Context context, Exception ex) {
        try {
            InicializarFabric(context);
            Crashlytics.logException(ex);
        } catch (Exception e) {
            Logger.e("Error de FabricLibrary", e);
        }
    }

    public static void rastrearEvento(Context context, EnumCategoriaAnalytics poCategory, EnumAccionAnalytics poAction) {
        try {
            InicializarFabric(context);
            Answers.getInstance().logCustom(new CustomEvent(poCategory.getEtiqueta())
                    .putCustomAttribute("Accion", poAction.getEtiqueta())
                    .putCustomAttribute("Instancia", Utilitario.readProperties(context).getProperty("IP_SERVER"))
                    .putCustomAttribute("Telefono", NUMERO_TELEFONO /*Service.getNumeroTelefonico(context)*/ /*Utilitario.fnNumEquipo(context)*/));
        } catch (Exception e) {
            Logger.e("Error al inicializar enviar evento. " + e);
        }
    }

    public static void rastrearContenido(Context poContext, EnumContentViewAnalytics tipo, String name, String id) {
        try {
            InicializarFabric(poContext);
            Answers.getInstance().logContentView(new ContentViewEvent()
                    .putContentName(name)
                    .putContentType(tipo.getEtiqueta())
                    .putContentId(id)
                    .putCustomAttribute("Instancia", Utilitario.readProperties(poContext).getProperty("IP_SERVER"))
                    .putCustomAttribute("Telefono", NUMERO_TELEFONO /*Service.getNumeroTelefonico(poContext)*/ /*;Utilitario.fnNumEquipo(poContext)*/));
            Logger.i("Trackeando contenido: " + poContext.getClass().getName());
        } catch (Exception e) {
            Logger.e("Error al inicializar enviar evento.", e);
        }
    }

    public static void rastrearPantalla(Context poContext) {
        try {
            InicializarFabric(poContext);
            Answers.getInstance().logContentView(new ContentViewEvent()
                    .putContentName(poContext.getClass().getSimpleName())
                    .putContentType(EnumContentViewAnalytics.ACTIVITY.getEtiqueta())
                    .putContentId(poContext.getClass().getName())
                    .putCustomAttribute("Instancia", Utilitario.readProperties(poContext).getProperty("IP_SERVER"))
                    .putCustomAttribute("Telefono", NUMERO_TELEFONO /*Service.getNumeroTelefonico(poContext)*/ /*;Utilitario.fnNumEquipo(poContext)*/));
            Logger.i("Trackeando pantalla: " + poContext.getClass().getName());
        } catch (Exception ex) {
            Logger.e("Error al inicializar enviar evento.", ex);
        }
    }

    public enum EnumAccionAnalytics {
        LOGIN("login"),
        SINTODO("sincronizar"),
        GRABARACTIVIDAD("transaccion"),
        GRABARPENDENTE("pendiente"),
        GRABARTRABAJO("inicio/fin dia"),
        INICIARDIA("Iniciar Dia"),
        FINDIA("Fin dia"),
        GRABARFOTO("foto"),
        SINIMAGEN("sin imagen"),
        AVANCE("avance"),
        IDIOMA("idioma"),
        GRABARACTIVIDADMULTIPLE("transaccion multiple"), VERIFICAR("verificar"),
        NOVISITA("no visita"),
        LISTARCLIENTE("Lista Cliente"),
        SALIR("Salir de la Aplicación"),
        CONSULTA("Consulta Dinamica"),
        GPS("GPS"),
        ENVIARFOTO("Enviar Foto"),
        ENVIARACTIVIDAD("Enviar Actividad"),
        ENVIARACTIVIDADMULTIPLE("Enviar Actividad Multiple"),
        ENVIARNOVISITAMULTIPLE("Enviar No Visita Multiple"),
        ENVIARSESION("Enviar Sesion");
        private String isEtiqueta;

        EnumAccionAnalytics(String etiqueta) {
            isEtiqueta = etiqueta;
        }

        public String getEtiqueta() {
            return isEtiqueta;
        }
    }


    public enum EnumContentViewAnalytics {
        ACTIVITY("Activity"), CONTENIDO("Contenido");
        private String isEtiqueta;

        EnumContentViewAnalytics(String etiqueta) {
            isEtiqueta = etiqueta;
        }

        public String getEtiqueta() {
            return isEtiqueta;
        }
    }

    public enum EnumCategoriaAnalytics {
        HTTP("Conexion"), INTERACCION("Interaccion"), ERROR("Error");
        private String isEtiqueta;

        EnumCategoriaAnalytics(String etiqueta) {
            isEtiqueta = etiqueta;
        }

        public String getEtiqueta() {
            return isEtiqueta;
        }
    }
}
