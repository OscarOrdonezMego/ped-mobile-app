package pe.entel.android.pedidos.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import pe.entel.android.verification.util.Preferences;

/**
 * Created by jvasqueza on 18/06/2015.
 */
public class NotificacionReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("pe.entel.android.verification.intent.action.NOTIFICATION_SEND")) {
            /*IMPLEMENTAR CODIGO NECESARIO AQUI. POR EJEMPLO FINALIZAR PROCESOS O SERVICIOS EN BACKGROUND CUANDO SE DESHABILITE EL ACCESO*/
            SharedPreferences loSharedPreferences = context.getSharedPreferences(Preferences.PREFERENCE, Context.MODE_PRIVATE);
            String flag = loSharedPreferences.getString(Preferences.PREFERENCE_FLAG, "");
            if (flag != null && flag.equals(Preferences.FLAG_FALSE)) {
                Log.d("NotificationReceiver", "Flag: " + flag);
            }
        }
    }
}
