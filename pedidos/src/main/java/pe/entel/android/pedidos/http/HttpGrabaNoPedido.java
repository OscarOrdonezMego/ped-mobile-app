package pe.entel.android.pedidos.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanClienteDireccion;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanValidaPedidoAndroid;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumUrl;
import pe.entel.android.pedidos.util.UtilitarioData;

public class HttpGrabaNoPedido extends HttpConexion {
    private BeanEnvPedidoCab ioBeanPedidoCab;
    private Context ioContext;

    public HttpGrabaNoPedido(BeanEnvPedidoCab psBeanVisitaCab, Context poContext, EnumTipActividad poEnumTipActividad) {
        super(poContext, poContext.getString(R.string.httpgrabarpedido_dlg), poEnumTipActividad);
        ioBeanPedidoCab = psBeanVisitaCab;
        ioContext = poContext;
    }

    @Override
    public boolean OnPreConnect() {
        SharedPreferences loSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ioContext);
        if (loSharedPreferences.getBoolean(ioContext.getString(R.string.keypre_cobertura), false)) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_modofueracobertura));
            return false;
        }
        int signal = UtilitarioData.fnSignalLevel(ioContext);
        if (!Utilitario.fnVerSignal(ioContext) || signal < 2) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        // TODO Auto-generated method stub
        subConHttp();
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            BeanValidaPedidoAndroid loBeanPedido = (BeanValidaPedidoAndroid) getHttpResponseObject();
            int liIdHttprespuesta = loBeanPedido.getIdResultado();
            String lsHttpRespuesta = loBeanPedido.getResultado();

            if (liIdHttprespuesta == ConfiguracionNextel.CONSRESSERVIDOROK
                    || liIdHttprespuesta == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {
                ((AplicacionEntel) ioContext.getApplicationContext()).setCurrentValidaPedido(loBeanPedido);
            } else {
                ((AplicacionEntel) ioContext.getApplicationContext()).setCurrentValidaPedido(loBeanPedido);
            }
            setHttpResponseIdMessage(liIdHttprespuesta, lsHttpRespuesta);
        }
    }

    public void subConHttp() {
        try {
            boolean flgDireccion = true;
            BeanValidaPedidoAndroid loBeanPedido = new BeanValidaPedidoAndroid();

            //RestTemplate restTemplate = new RestTemplate();
            //restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

            if (ioBeanPedidoCab.getFlgNuevoDireccion().equals(Configuracion.FLGVERDADERO)) {

                DalCliente loDalCliente = new DalCliente(ioContext);
                BeanClienteDireccion loBeanClienteDireccion = loDalCliente
                        .fnSelDireccionxCodInc(ioBeanPedidoCab.getCodDireccion());

                Log.v("URL: ", Configuracion.fnUrl(ioContext, EnumUrl.GRABARDIRECCION));
                Log.v("REQUEST: ", BeanMapper.toJson(loBeanClienteDireccion, false));

                //String lsResultado = restTemplate.postForObject(Configuracion.fnUrl(ioContext, EnumUrl.GRABARDIRECCION), loBeanClienteDireccion, String.class);
                String lsResultado = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, EnumUrl.GRABARDIRECCION), loBeanClienteDireccion);
                BeanClienteDireccion loResponseBeanClienteDireccion = (BeanClienteDireccion) BeanMapper.fromJson(lsResultado, BeanClienteDireccion.class);

                if (loResponseBeanClienteDireccion.getError() == ConfiguracionNextel.CONSRESSERVIDOROK) {

                    loDalCliente.fnUpdEnviadoDireccion(
                            loResponseBeanClienteDireccion.getPk(),
                            loResponseBeanClienteDireccion.getCodDireccion(),
                            loBeanClienteDireccion.getCodInc()
                    );

                    DalPedido loDalPedido = new DalPedido(ioContext);
                    loDalPedido.fnUpdDireccion(
                            loResponseBeanClienteDireccion.getPk(),
                            loBeanClienteDireccion.getCodInc()
                    );

                    ioBeanPedidoCab.setFlgNuevoDireccion(Configuracion.FLGFALSO);
                    ioBeanPedidoCab.setCodDireccion(loResponseBeanClienteDireccion.getPk());
                    ioBeanPedidoCab.setCodigoDireccion(loResponseBeanClienteDireccion.getCodDireccion());

                } else {
                    flgDireccion = false;
                    loBeanPedido.setIdResultado(-1);
                    loBeanPedido.setResultado("Error al Guardar el Pedido");
                }
            }

            if (flgDireccion) {
                Log.v("URL: ", Configuracion.fnUrl(ioContext, EnumUrl.GRABARPEDIDO));
                Log.v("REQUEST: ", BeanMapper.toJson(ioBeanPedidoCab, false));

                //String lsObjeto = restTemplate.postForObject(Configuracion.fnUrl(ioContext, EnumUrl.GRABARPEDIDO), ioBeanPedidoCab, String.class);
                String lsObjeto = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, EnumUrl.GRABARPEDIDO), ioBeanPedidoCab);
                loBeanPedido = (BeanValidaPedidoAndroid) BeanMapper.fromJson(lsObjeto, BeanValidaPedidoAndroid.class);
            }

            setHttpResponseObject(loBeanPedido);
        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, "Error HTTP: " + e.getMessage());
        }
    }
}