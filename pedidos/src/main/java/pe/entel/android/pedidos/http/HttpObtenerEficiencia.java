package pe.entel.android.pedidos.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanRespListaEfecienciaOnline;
import pe.entel.android.pedidos.dal.DalConfiguracion;
import pe.entel.android.pedidos.util.Configuracion;

/**
 * Created by tkongr on 14/10/2015.
 */
public class HttpObtenerEficiencia extends HttpConexion {

    private BeanListaEficienciaOnline ioBeanListaEficienciaOnline;
    private Context ioContext;

    public HttpObtenerEficiencia(BeanListaEficienciaOnline poBeanListaEficienciaOnline, Context poContext, ConfiguracionNextel.EnumTipActividad poEnumTipActividad) {
        super(poContext, poContext.getString(R.string.httpobtenereficiencia_dlg), poEnumTipActividad);
        ioBeanListaEficienciaOnline = poBeanListaEficienciaOnline;
        ioContext = poContext;
    }

    public void subConHttp() {
        try {
            //RestTemplate restTemplate = new RestTemplate();
            //restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            Log.v("URL", Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.EFICIENCIAONLINE));
            Log.v("REQUEST", BeanMapper.toJson(ioBeanListaEficienciaOnline, false));

            //String lsObjeto = restTemplate.postForObject(Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.EFICIENCIAONLINE), ioBeanListaEficienciaOnline, String.class);
            String lsObjeto = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.EFICIENCIAONLINE), ioBeanListaEficienciaOnline);
            Log.v("RESPONSE", BeanMapper.toJson(lsObjeto, false));

            BeanRespListaEfecienciaOnline loBeanResponse = (BeanRespListaEfecienciaOnline) BeanMapper.fromJson(lsObjeto, BeanRespListaEfecienciaOnline.class);

            String lsBean = "";
            try {
                lsBean = BeanMapper.toJson(loBeanResponse, false);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (ioBeanListaEficienciaOnline.getPerfilUsuario().equals(Configuracion.perfilVen)) {
                SharedPreferences loSharedPreferences = ioContext.getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
                SharedPreferences.Editor loEditor = loSharedPreferences.edit();
                loEditor.putString("BeanRespListaEfecienciaOnline", lsBean);

                loEditor.commit();
            } else {
                SharedPreferences loSharedPreferencesSupervisor = ioContext.getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
                SharedPreferences.Editor loEditor = loSharedPreferencesSupervisor.edit();
                loEditor.putString("BeanRespListaEfecienciaOnlineSupervisor", lsBean);

                loEditor.commit();
            }
            setHttpResponseObject(loBeanResponse);

        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, "Error HTTP: " + e.getMessage());
        }
    }

    @Override
    public boolean OnPreConnect() {
        if (!Utilitario.fnVerSignal(ioContext)) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            BeanRespListaEfecienciaOnline loBeanResponse = (BeanRespListaEfecienciaOnline) getHttpResponseObject();

            if (loBeanResponse.getIdResultado() == ConfiguracionNextel.CONSRESSERVIDORALGUNERROR) {
                loBeanResponse.setIdResultado(ConfiguracionNextel.CONSRESSERVIDORERROR);
                if (loBeanResponse.getResultado().indexOf("*") > -1)
                    loBeanResponse.setResultado(ioContext
                            .getString(R.string.msg_error) + loBeanResponse.getResultado());
                else
                    loBeanResponse.setResultado(ioContext
                            .getString(R.string.msg_scripterror) + loBeanResponse.getResultado());
            } else if (loBeanResponse.getIdResultado() == ConfiguracionNextel.CONSRESSERVIDORERROR) {
                loBeanResponse.setResultado(ioContext
                        .getString(R.string.msg_httperror) + loBeanResponse.getResultado());
            }

            DalConfiguracion dalConfiguracion = new DalConfiguracion(ioContext);
            dalConfiguracion.executeQuery(loBeanResponse.getUsuariosVendedores());
            setHttpResponseIdMessage(loBeanResponse.getIdResultado(), loBeanResponse.getResultado());
        }
    }
}
