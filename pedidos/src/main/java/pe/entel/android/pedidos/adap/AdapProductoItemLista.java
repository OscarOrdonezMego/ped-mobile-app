package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanEnvItemDet;
import pe.entel.android.pedidos.util.Configuracion;

public class AdapProductoItemLista extends ArrayAdapter<BeanEnvItemDet> {
    private int resource;
    private Context context;
    private List<BeanEnvItemDet> originalList;
    private String maximoNumeroDecimales;
    private int iiMaximoNumeroDecimales;

    public AdapProductoItemLista(Context _context, int _resource,
                                 List<BeanEnvItemDet> lista) {
        super(_context, _resource, lista);
        originalList = new ArrayList<BeanEnvItemDet>();
        originalList.addAll(lista);
        resource = _resource;
        context = _context;
        maximoNumeroDecimales = Configuracion.EnumConfiguracion.MAXIMO_NUMERODECIMALES.getValor(context);
        setMaximoNumeroDecimales();
    }

    private void setMaximoNumeroDecimales() {
        iiMaximoNumeroDecimales = 0;
        try {
            iiMaximoNumeroDecimales = Integer.parseInt(maximoNumeroDecimales);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ActProductoDetalle", "Error conversion maximo numero de decimales: " + e.getMessage());
        }
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BeanEnvItemDet item = getItem(position);

        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        ((TextView) nuevaVista
                .findViewById(R.id.actproductoitemlistaitem_txtNombreArticulo))
                .setText(item.fnMostrarArticulo(iiMaximoNumeroDecimales));

        if (Configuracion.TipoArticulo.PRODUCTO.equals(item.getTipoArticulo())) {
            ((LinearLayout) nuevaVista
                    .findViewById(R.id.actproductoitemlistaitem_lnPre)).setVisibility(View.GONE);
            ((TextView) nuevaVista
                    .findViewById(R.id.actproductoitemlistaitem_txtNombreArticuloPro))
                    .setVisibility(View.GONE);


        } else {
            ((LinearLayout) nuevaVista
                    .findViewById(R.id.actproductoitemlistaitem_lnPre))
                    .setVisibility(View.VISIBLE);
            ((TextView) nuevaVista
                    .findViewById(R.id.actproductoitemlistaitem_txtNombreArticuloPro))
                    .setVisibility(View.VISIBLE);

            ((TextView) nuevaVista
                    .findViewById(R.id.actproductoitemlistaitem_txtNombreArticuloPro))
                    .setText(item.fnMostrarArticuloPro());

            ((TextView) nuevaVista
                    .findViewById(R.id.actproductoitemlistaitem_txtCant))
                    .setText(item.getCantidad());

            ((TextView) nuevaVista
                    .findViewById(R.id.actproductoitemlistaitem_txtFracc))
                    .setText(item.getCantidadFrac());

            ((TextView) nuevaVista
                    .findViewById(R.id.actproductoitemlistaitem_txtMotivo))
                    .setText(item.getNombreMotivo());


        }


        return nuevaVista;
    }
}
