package pe.entel.android.pedidos.http;

import android.content.Context;
import android.util.Log;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanClienteDireccion;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanEnvPendientes;
import pe.entel.android.pedidos.bean.BeanRspPendientes;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumUrl;
import pe.entel.android.pedidos.util.UtilitarioData;
import pe.entel.android.pedidos.util.UtilitarioFirebase;

public class HttpGrabaPendiente extends HttpConexion {
    private BeanEnvPendientes ioBeanEnvPendientes;
    private Context ioContext;

    public HttpGrabaPendiente(BeanEnvPendientes psBeanEnvioPend, Context poContext, EnumTipActividad poEnumTipActividad) {
        super(poContext, poContext.getString(R.string.httpgrabapendiente_dlg), poEnumTipActividad);
        ioBeanEnvPendientes = psBeanEnvioPend;
        ioContext = poContext;
    }

    @Override
    public boolean OnPreConnect() {
        int signal = UtilitarioData.fnSignalLevel(ioContext);
        if (!Utilitario.fnVerSignal(ioContext) || signal < 2) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            String lsResultado = (String) getHttpResponseObject();
            String[] laRespuesta = lsResultado.split(";");
            int liIdHttpRespuesta = Integer.valueOf(laRespuesta[0]);
            String lsHttpRespuesta = laRespuesta[1];
            setHttpResponseIdMessage(liIdHttpRespuesta, lsHttpRespuesta);
        }
    }

    public void subConHttp() {
        try {
            //RestTemplate restTemplate = new RestTemplate();
            //restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

            Log.v("XXX", Configuracion.fnUrl(ioContext, EnumUrl.GRABARPENDIENTE));
            Log.v("XXX", BeanMapper.toJson(ioBeanEnvPendientes, false));

            //String lsResultado = restTemplate.postForObject(Configuracion.fnUrl(ioContext, EnumUrl.GRABARPENDIENTE), ioBeanEnvPendientes, String.class);
            String lsResultado = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.GRABARPENDIENTE), ioBeanEnvPendientes);
            BeanRspPendientes loRspBeanPendientes = (BeanRspPendientes) BeanMapper.fromJson(lsResultado, BeanRspPendientes.class);

            String[] laRespuesta = loRspBeanPendientes.getError().split(";");

            if (laRespuesta[0].equals("1")) {

                DalCliente loDalCliente = new DalCliente(ioContext);
                DalPedido loDalPedido = new DalPedido(ioContext);

                for (BeanClienteDireccion loBeanClienteDireccion : loRspBeanPendientes.getListDirecciones()) {

                    loDalCliente.fnUpdEnviadoDireccion(
                            loBeanClienteDireccion.getPk(),
                            loBeanClienteDireccion.getCodDireccion(),
                            loBeanClienteDireccion.getCodInc()
                    );

                    loDalPedido.fnUpdDireccion(
                            loBeanClienteDireccion.getPk(),
                            loBeanClienteDireccion.getCodInc()
                    );
                }

                for (BeanEnvPedidoCab ioBeanPedidoCab : ioBeanEnvPendientes.getListPedidos()
                ) {
                    UtilitarioFirebase.registrarPedido(ioContext, ioBeanPedidoCab);
                }
            }
            setHttpResponseObject(loRspBeanPendientes.getError());
        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, "Error HTTP: " + e.getMessage());
        }
    }
}