/*
 * Copyright (C) 2010 Cyril Mottier (http://www.cyrilmottier.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pe.entel.android.pedidos.widget;

import android.content.Context;
import android.view.ViewGroup;

import greendroid.widget.item.ItextItem;
import greendroid.widget.item.TextItem;
import greendroid.widget.itemview.ItemView;
import pe.entel.android.pedidos.R;

public class HeadedTextItem extends TextItem implements ItextItem {

    private String headerText;

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String psHeaderText) {
        headerText = psHeaderText;
    }

    public HeadedTextItem(String text) {
        super(text);
    }

    @Override
    public ItemView newView(Context context, ViewGroup parent) {
        return createCellFromXml(context, R.layout.headed_text_item_view,
                parent);

    }

}
