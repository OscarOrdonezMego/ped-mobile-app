package pe.entel.android.pedidos.util;

import android.content.Context;

import pe.com.nextel.android.util.SharedPreferenceGestor;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanUsuario;

/**
 * Created by csotob on 27/12/2016.
 */
public class Preferencias {

    public static BeanUsuario getUsuario(Context pocontext) {
        BeanUsuario usuario = null;
        try {
            usuario = (BeanUsuario) SharedPreferenceGestor.fnSharedPreferJSON(
                    pocontext, Configuracion.CURRENT_BEAN_USUARIO, BeanUsuario.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return usuario;
    }

    public static void setUsuario(Context pocontext, BeanUsuario usuario) {
        try {
            SharedPreferenceGestor.subIniSharedPreferJSON(pocontext, Configuracion.CURRENT_BEAN_USUARIO, usuario);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static BeanEnvPedidoCab getPedidoCab(Context pocontext) {
        BeanEnvPedidoCab pedidocab = null;
        try {
            pedidocab = (BeanEnvPedidoCab) SharedPreferenceGestor.fnSharedPreferJSON(
                    pocontext, Configuracion.CURRENT_BEAN_PEDIDOCAB, BeanEnvPedidoCab.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pedidocab;
    }

    public static void setPedidoCab(Context pocontext, BeanEnvPedidoCab pedidocab) {
        try {
            SharedPreferenceGestor.subIniSharedPreferJSON(pocontext, Configuracion.CURRENT_BEAN_PEDIDOCAB, pedidocab);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void fnGuardarCodigo(Context pocontext, String identificador, String codigo) {
        try {
            SharedPreferenceGestor.subIniSharedPrefer(pocontext, identificador, codigo);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String fnObtenerCodigo(Context pocontext, String identificador) {
        try {
            return (String) SharedPreferenceGestor.fnSharedPrefer(pocontext, identificador, String.class);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }
}
