package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by tkongr on 30/10/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanCobranzaOnline {

    @JsonProperty
    private String cobPk;
    @JsonProperty
    private String cobCodigo;
    @JsonProperty
    private String cobNumDocumento;
    @JsonProperty
    private String cobTipoDocumento;
    @JsonProperty
    private String cobMontoPagado;
    @JsonProperty
    private String cobMontoTotal;
    @JsonProperty
    private String cobFecVencimiento;
    @JsonProperty
    private String cobSerie;
    @JsonProperty
    private String cliCodigo;
    @JsonProperty
    private String cobEmpresa;
    @JsonProperty
    private String cobUsuario;
    @JsonProperty
    private String cliPK;
    @JsonProperty
    private String saldo;
    @JsonProperty
    private String cobMontoPagadosoles;
    @JsonProperty
    private String cobMontoTotalsoles;
    @JsonProperty
    private String saldosoles;
    @JsonProperty
    private String cobMontoPagadodolares;
    @JsonProperty
    private String cobMontoTotaldolares;
    @JsonProperty
    private String saldodolares;

    public BeanCobranzaOnline() {
    }

    public String getCobPk() {
        return cobPk;
    }

    public void setCobPk(String cobPk) {
        this.cobPk = cobPk;
    }

    public String getCobCodigo() {
        return cobCodigo;
    }

    public void setCobCodigo(String cobCodigo) {
        this.cobCodigo = cobCodigo;
    }

    public String getCobNumDocumento() {
        return cobNumDocumento;
    }

    public void setCobNumDocumento(String cobNumDocumento) {
        this.cobNumDocumento = cobNumDocumento;
    }

    public String getCobTipoDocumento() {
        return cobTipoDocumento;
    }

    public void setCobTipoDocumento(String cobTipoDocumento) {
        this.cobTipoDocumento = cobTipoDocumento;
    }

    public String getCobMontoPagado() {
        return cobMontoPagado;
    }

    public void setCobMontoPagado(String cobMontoPagado) {
        this.cobMontoPagado = cobMontoPagado;
    }

    public String getCobMontoTotal() {
        return cobMontoTotal;
    }

    public void setCobMontoTotal(String cobMontoTotal) {
        this.cobMontoTotal = cobMontoTotal;
    }

    public String getCobFecVencimiento() {
        return cobFecVencimiento;
    }

    public void setCobFecVencimiento(String cobFecVencimiento) {
        this.cobFecVencimiento = cobFecVencimiento;
    }

    public String getCobSerie() {
        return cobSerie;
    }

    public void setCobSerie(String cobSerie) {
        this.cobSerie = cobSerie;
    }

    public String getCliCodigo() {
        return cliCodigo;
    }

    public void setCliCodigo(String cliCodigo) {
        this.cliCodigo = cliCodigo;
    }

    public String getCobEmpresa() {
        return cobEmpresa;
    }

    public void setCobEmpresa(String cobEmpresa) {
        this.cobEmpresa = cobEmpresa;
    }

    public String getCobUsuario() {
        return cobUsuario;
    }

    public void setCobUsuario(String cobUsuario) {
        this.cobUsuario = cobUsuario;
    }

    public String getCliPK() {
        return cliPK;
    }

    public void setCliPK(String cliPK) {
        this.cliPK = cliPK;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getCobMontoPagadosoles() {
        return cobMontoPagadosoles;
    }

    public void setCobMontoPagadosoles(String cobMontoPagadosoles) {
        this.cobMontoPagadosoles = cobMontoPagadosoles;
    }

    public String getCobMontoTotalsoles() {
        return cobMontoTotalsoles;
    }

    public void setCobMontoTotalsoles(String cobMontoTotalsoles) {
        this.cobMontoTotalsoles = cobMontoTotalsoles;
    }

    public String getSaldosoles() {
        return saldosoles;
    }

    public void setSaldosoles(String saldosoles) {
        this.saldosoles = saldosoles;
    }

    public String getCobMontoPagadodolares() {
        return cobMontoPagadodolares;
    }

    public void setCobMontoPagadodolares(String cobMontoPagadodolares) {
        this.cobMontoPagadodolares = cobMontoPagadodolares;
    }

    public String getCobMontoTotaldolares() {
        return cobMontoTotaldolares;
    }

    public void setCobMontoTotaldolares(String cobMontoTotaldolares) {
        this.cobMontoTotaldolares = cobMontoTotaldolares;
    }

    public String getSaldodolares() {
        return saldodolares;
    }

    public void setSaldodolares(String saldodolares) {
        this.saldodolares = saldodolares;
    }

}
