package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.Gps;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout;
import pe.com.nextel.android.widget.NexToast;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanClienteDireccion;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.http.HttpGrabaDireccion;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

/**
 * Created by Dsb Mobile on 09/04/2015.
 */
public class ActPedidoClienteGrabarDireccion extends NexActivity implements
        NexMenuDrawerLayout.NexMenuCallbacks, AdapMenuPrincipalSliding.MenuSlidingListener,
        View.OnClickListener {

    private BeanUsuario loBeanUsuario;
    private BeanCliente loBeanCliente;
    private BeanClienteDireccion ioBeanClienteDireccion;

    private TextView txtCodigo, txtNombre;
    private EditText edtDireccion;
    private Button btnEnviar;

    //CONSTANTES
    private AdapMenuPrincipalSliding _menu;
    private DALNServices ioDALNservices;
    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDASINCRONIZACION = 11;
    private final int ENVIOPENDIENTES = 12;
    private final int VALIDASINCRO = 13;
    private final int NOHAYPENDIENTES = 14;
    private final int CONSICOFIN = 0;
    private final int CONSBACK = 3;

    private boolean estadoSincronizar = false;

    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro

    private String isGps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);
        String lsBeanUsuario = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario,
                BeanUsuario.class);

        String lsBeanClienteOnline = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanClienteOnline", "");
        loBeanCliente = (BeanCliente) BeanMapper.fromJson(lsBeanClienteOnline,
                BeanCliente.class);

        isGps = Configuracion.EnumConfiguracion.GPS.getValor(this);

        super.onCreate(savedInstanceState);

        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);

        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));

        subIniMenu();

    }

    @Override
    protected void subSetControles() {
        super.subSetControles();
        setActionBarContentView(R.layout.pedidoclientegrabardireccion);

        txtCodigo = (TextView) findViewById(R.id.actpedidodirecciongrabar_txtcodigo);
        txtNombre = (TextView) findViewById(R.id.actpedidodirecciongrabar_txtnombre);
        edtDireccion = (EditText) findViewById(R.id.actpedidodirecciongrabar_edtoDireccion);
        btnEnviar = (Button) findViewById(R.id.actpedidodirecciongrabar_btnenviar);

        btnEnviar.setOnClickListener(this);

        txtCodigo.setText(loBeanCliente.getClicod());
        txtNombre.setText(loBeanCliente.getClinom());

    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnEnviar)) {

            if (TextUtils.isEmpty(edtDireccion.getText().toString())) {
                AlertDialog loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actpedidodirecciongrabar_lbltitle))
                        .setMessage("Debe ingresar una Dirección")
                        .create();
                loAlertDialog.show();

            } else {

                StringBuffer loStb = new StringBuffer();
                loStb.append(getString(R.string.actpedidodirecciongrabar_confirmacion))
                        .append('\n');
                loStb.append("C\u00F3digo: ")
                        .append(loBeanCliente.getClicod())
                        .append('\n');
                loStb.append("Nombre: ")
                        .append(loBeanCliente.getClinom())
                        .append('\n');
                loStb.append("Direcci\u00F3n: ")
                        .append(edtDireccion.getText().toString().toUpperCase())
                        .append('\n');

                AlertDialog loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actpedidodirecciongrabar_lbltitle))
                        .setMessage(loStb.toString())

                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            subGrabar();
                                            enviarDireccionServer();
                                        } catch (Exception e) {

                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btncancelar),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        return;
                                    }
                                })

                        .create();
                loAlertDialog.show();
            }
        }
    }

    private void subGrabar() {

        Calendar loCalendar = Calendar.getInstance();
        Date loDate = loCalendar.getTime();
        final String lsFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate)
                .toString();

        ioBeanClienteDireccion = new BeanClienteDireccion();
        ioBeanClienteDireccion.setCodCliente(loBeanCliente.getClicod());
        ioBeanClienteDireccion.setNombre(edtDireccion.getText().toString().toUpperCase());
        ioBeanClienteDireccion.setTipo(Configuracion
                .EnumFlujoDireccion.PEDIDO.getCodigo(this));
        ioBeanClienteDireccion.setFlgEnviado(Configuracion.FLGFALSO);
        ioBeanClienteDireccion.setFechaCreacion(lsFecha);

        Location location = null;

        try {
            location = Gps.getLocation();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (isGps.equals(
                Configuracion.FLGVERDADERO)
                && location != null) {
            ioBeanClienteDireccion.setLatitud(String.valueOf(location.getLatitude()));
            ioBeanClienteDireccion
                    .setLongitud(String.valueOf(location.getLongitude()));
        } else {
            ioBeanClienteDireccion.setLatitud("0");
            ioBeanClienteDireccion.setLongitud("0");
        }

        DalCliente loDalCliente = new DalCliente(this);
        loDalCliente.fnGrabarDireccion(ioBeanClienteDireccion);


    }

    private void enviarDireccionServer() {

        new HttpGrabaDireccion(ioBeanClienteDireccion, this,
                fnTipactividad()).execute();
    }

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences(
                                                    "Actual", MODE_PRIVATE)
                                                    .getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                                    .fromJson(lsBeanUsuario,
                                                            BeanUsuario.class);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActPedidoClienteGrabarDireccion.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActPedidoClienteGrabarDireccion.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setTitle(
                getString(R.string.actpedidodirecciongrabar_lbltitle));
    }


    @SuppressWarnings("deprecation")
    public void subAccDesMensaje() {
        try {
            Intent intentl = new Intent(this, ActPedidoClienteDireccionLista.class);
            intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intentl.putExtra(
                    Configuracion.EXTRA_NAME_DIRECCION,
                    Configuracion.EnumFlujoDireccion.PEDIDO.name()
            );

            if (estadoSincronizar) {
                if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                    Intent loIntent = new Intent(this, ActMenu.class);

                    loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loIntent);
                }
            } else if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                // como grabo bien en el servidor, hago un update al registro

                startActivity(intentl);
                finish();
            } else {
                new NexToast(this, R.string.actgrabapedido_dlgerrorpedido,
                        NexToast.EnumType.ERROR).show();
                // finish();
                startActivity(intentl);
                finish();
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void onBackPressed() {

        Intent loIntent = new Intent(this, ActClienteDetalle.class);
        loIntent.putExtra("IdCliente", Long
                .parseLong(((AplicacionEntel) getApplication())
                        .getCurrentCliente().getClipk()));

        finish();
        startActivity(loIntent);
    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent loIntentLogin = new Intent(ActPedidoClienteGrabarDireccion.this,
                        ActLogin.class);
                loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(loIntentLogin);
            }
        });

    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        Intent intentl = new Intent(this, ActMenu.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intentl);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @SuppressWarnings("deprecation")
    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = null;
            DalPedido loDalPedido = new DalPedido(this);
            loList = loDalPedido.fnEnvioPedidosPendientes(loBeanUsuario
                    .getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);

                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();
                showDialog(CONSDIASINCRONIZAR);
            } else {
                showDialog(VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub
        String lsBeanUsuario = getSharedPreferences(
                "Actual", MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper
                .fromJson(lsBeanUsuario,
                        BeanUsuario.class);
        this.setMsgOk("Eficiencia Online");

        BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
        loBeanListaEficienciaOnline.setIdResultado(0);
        loBeanListaEficienciaOnline.setResultado("");
        loBeanListaEficienciaOnline.setListaEficiencia(null);
        loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

        new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

        Intent loInstent = new Intent(this, ActKpiDetalle.class);
        loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loInstent);
    }

    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        ((AplicacionEntel) getApplication()).setCurrentCliente(null);
        ((AplicacionEntel) getApplication())
                .setCurrentCodFlujo(Configuracion.CONSSTOCK);
        ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
        Intent loIntento = new Intent(ActPedidoClienteGrabarDireccion.this,
                ActProductoListaOnline.class);
        loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        BeanExtras loBeanExtras = new BeanExtras();
        loBeanExtras.setFiltro("");
        loBeanExtras.setTipo(ConfiguracionNextel.EnumTipBusqueda.NOMBRE);
        String lsFiltro = "";
        try {
            lsFiltro = BeanMapper.toJson(loBeanExtras, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
        startActivity(loIntento);
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(getString(R.string.ml_sincronizarantes)));
        }

    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }


    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */

    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), NexToast.EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        //showNexDialog(DIALOG_NSERVICES_DOWNLOAD);
                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */
    @Override
    protected DialogFragmentYesNo.DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        try {
            DialogFragmentYesNo loDialog;
            switch (id) {
                case DIALOG_NSERVICES_DOWNLOAD:
                    String lsNServices;
                    if (ioDALNservices == null)
                        ioDALNservices = DALNServices.getInstance(this,
                                R.string.db_name);
                    try {
                        lsNServices = ioDALNservices.fnSelNServicesLabel();
                    } catch (Exception e) {
                        Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                e);
                        lsNServices = getString(R.string.nservices_label_default);
                    }
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG
                                    + DIALOG_NSERVICES_DOWNLOAD,
                            getString(R.string.nservices_consultingdownlad)
                                    .replace("{0}", lsNServices),
                            DialogFragmentYesNo.EnumLayoutResource.getDefaultIconHelp(this));
                    return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog,
                            new DialogFragmentYesNo.OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(
                                        DialogFragmentYesNo dialog) {
                                    try {
                                        subIniNServicesDownload(ioDALNservices
                                                .fnSelNServicesAPK());
                                    } catch (Exception e) {
                                        subShowException(e);
                                    }
                                }

                                @Override
                                public void performButtonNo(
                                        DialogFragmentYesNo dialog) {
                                }
                            });

                default:
                    return super.onCreateNexDialog(id);
            }
        } catch (Exception e) {
            subShowException(e);
            return super.onCreateNexDialog(id);
        }
    }

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

}
