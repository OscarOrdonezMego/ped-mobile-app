package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanGeneral;

public class AdapPopupBeanGeneral extends ArrayAdapter<BeanGeneral> {
    int resource;
    Context context;
    TextView rowTextView;

    List<BeanGeneral> originalList;

    BeanGeneral ioGeneral;

    int cont = 0;

    public AdapPopupBeanGeneral(Context _context, int _resource,
                                List<BeanGeneral> lista, BeanGeneral _ioGeneral) {
        super(_context, _resource, lista);
        originalList = new ArrayList<BeanGeneral>();
        originalList.addAll(lista);
        resource = _resource;
        context = _context;
        ioGeneral = _ioGeneral;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BeanGeneral item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        ((TextView) nuevaVista.findViewById(R.id.listapopup_item_txtNombre))
                .setText(item.getDescripcion());

        if (ioGeneral != null)
            ((TextView) nuevaVista
                    .findViewById(R.id.listapopup_item_txtNombre))
                    .setEnabled(!ioGeneral.getCodigo().equals(item.getCodigo()));
        else
            ((TextView) nuevaVista
                    .findViewById(R.id.listapopup_item_txtNombre))
                    .setEnabled(true);
        return nuevaVista;
    }

}
