package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanClienteDireccion;
import pe.entel.android.pedidos.util.Configuracion;

public class AdapPedidoClienteDireccion extends
        ArrayAdapter<BeanClienteDireccion> {
    int resource;
    Context context;
    TextView rowTextView;
    Configuracion.EnumFlujoDireccion ioFlujoDireccion;

    LinkedList<BeanClienteDireccion> originalList;

    int cont = 0;

    public AdapPedidoClienteDireccion(Context _context, int _resource,
                                      LinkedList<BeanClienteDireccion> lista, Configuracion.EnumFlujoDireccion poFlujoDireccion) {
        super(_context, _resource, lista);
        originalList = lista;
        resource = _resource;
        context = _context;
        ioFlujoDireccion = poFlujoDireccion;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BeanClienteDireccion item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        ((TextView) nuevaVista
                .findViewById(R.id.actpedidoclientedireccion_textTitulo))
                .setText(item.getNombre());

        TextView txtTipo = ((TextView) nuevaVista
                .findViewById(R.id.actpedidoclientedireccion_txtTipo));

        if (ioFlujoDireccion.equals(Configuracion.EnumFlujoDireccion.NO_VISITA)) {
            txtTipo.setVisibility(View.VISIBLE);
            txtTipo.setText(item.getTipo());
            txtTipo.setBackgroundColor(
                    context.getResources().getColor(
                            item.getTipo().equals(Configuracion.FLGDIRECCIONPEDIDO)
                                    ? R.color.pedidos_direccion_pedido
                                    : item.getTipo().equals(Configuracion.FLGDIRECCIONDEFAULT)
                                    ? R.color.pedidos_direccion_default
                                    : R.color.pedidos_direccion_despacho
                    )
            );

        } else txtTipo.setVisibility(View.GONE);


        return nuevaVista;
    }

}
