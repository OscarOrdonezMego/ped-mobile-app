package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import greendroid.widget.ActionBarItem;
import greendroid.widget.ActionBarItem.Type;
import greendroid.widget.item.ThumbnailItem;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.actividad.NexInvalidOperatorActivity.NexInvalidOperatorException;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.util.Gps;
import pe.com.nextel.android.util.Logger;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.adap.AdapProductoPedido;
import pe.entel.android.pedidos.bean.BeanArticulo;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanEnvPedidoDet;
import pe.entel.android.pedidos.bean.BeanGeneral;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanPrecio;
import pe.entel.android.pedidos.bean.BeanPresentacion;
import pe.entel.android.pedidos.bean.BeanProducto;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.bean.BeanValidaPedidoAndroid;
import pe.entel.android.pedidos.dal.DalAlmacen;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.dal.DalProducto;
import pe.entel.android.pedidos.http.HttpActualizarEstadoPedido;
import pe.entel.android.pedidos.http.HttpGrabaPedido;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumConfiguracion;
import pe.entel.android.pedidos.util.Configuracion.EnumFuncion;
import pe.entel.android.pedidos.util.UtilitarioData;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;
import pe.entel.android.pedidos.widget.ListaPopup;
import pe.entel.android.pedidos.widget.ListaPopup.OnItemClickPopup;

public class ActProductoPedido extends NexActivity implements
        OnItemClickListener, android.view.View.OnClickListener,
        NexMenuCallbacks, MenuSlidingListener, OnItemLongClickListener,
        OnItemClickPopup {

    private final int EDIT_ID = 1;
    private final int DELETE_ID = 2;
    private final int CANCEL_ID = 3;

    @SuppressWarnings("unused")
    private final int CONSVALIDASTOCK = 6;
    private final int CONSVALBONIFICACION = 7;
    private final int CONSICOVALIDAR = 2;
    private final int CONSADDPROD = 0;
    @SuppressWarnings("unused")
    private final int CONSFINAL = 1;
    private final int CONSBACK = 3;
    private final int CONSNOITEMS = 4;
    private final int CONSLIMITEEXCEDIDO = 5;
    private final int CONSVALEDIT = 6;
    private final int CONSELIMINARDETALLES = 7;
    private final int CONSCREDITOSUPERADO = 8;
    private final int CONSBACKFORMULARIOINICIO = 9;
    private final int ERRORPEDIDO = 20;
    private final int CREDITOSUPERADO = 21;
    private final int CONSBACKPENDIENTES = 22;
    private BeanEnvPedidoCab loBeanPedidoCab = null;
    private ArrayList<BeanEnvPedidoDet> iolista = null;
    private String isConsultarProducto;
    private TextView txtcantproductos, txtmontototal,
            txtmontototalDolar, //@JBELVY
            txttotalitems, txtfletetotal;
    private Button btnagregarproducto, btnfinalizar, btneliminar;
    private ListView lstproductopedido;
    @SuppressWarnings("unused")
    private LinearLayout lnContentHeader, lnLista, lnFlete;

    AdapProductoPedido adapter;

    private AdapMenuPrincipalSliding _menu;

    BeanUsuario loBeanUsuario = null;

    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDAHOME = 11;
    private final int VALIDASINCRONIZACION = 12;
    private final int ENVIOPENDIENTES = 13;
    private final int VALIDASINCRO = 14;
    private final int CONSSALIR = 15;
    private final int NOHAYPENDIENTES = 16;
    private final int DETALLE = 17;
    private final int ACTBARUP = 31;
    private final int ACTBARDOWN = 32;
    private final int ACTBARDESCARTAR = 25;
    private final int CONSDESCARTAR = 51;

    boolean estadoSincronizar = false;
    boolean editoPedido = false;
    int estadoSalir = 0;
    private String codigoProdPedRap;
    private String isTipoCliente;
    private String isPrecionCondVenta;
    //PARAMETROS DE CONFIGURACION
    private String isSimboloMoneda = "";
    private String isSimboloMonedaDolar = "";
    private String isFraccionamiento = "";
    private String isNumeroDecimales = "";
    private String isStockRestrictivo;
    private String isBonificacion;
    private String isDescuento;
    private String isGps;
    private String isMostrarDirDespacho;
    private String isMostrarAlmacen;
    private String isPresentacion;
    private int isMaximoItemsPedido;
    private String isMostrarCaberceraPedido;
    private double isMontoMinimoPedido;
    private double isMontoMaximoPedido;
    private String sharePreferenceBeanPedidoCab;
    private String isBonificacionAutomatica;
    BeanArticulo ioBeanArticulo = null;
    private boolean creditoSuperado = false;
    private boolean editarFormulario = false;
    boolean pedidopendiente = false;
    private TextView idNomProd, txtCodProdPred;
    private EditText edittextoCodProd;
    private TextWatcher textWatcher = null;
    private EditText edittextoCodCant;
    private String isMaximoNumeroDecimales;
    private int iiMaximoNumeroDecimales;
    private BeanProducto beanProducto = new BeanProducto();
    private BeanPresentacion beanPresentacion = new BeanPresentacion();
    private boolean estadoActualizar = false;

    Animation animSlideUp;
    Animation animSideDown;

    // Integracion Ntrack
    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Logger.d("XXX", "ActProductoPedido");
        ValidacionServicioUtil.validarServicio(this);
        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);

        isSimboloMoneda = EnumConfiguracion.SIMBOLO_MONEDA.getValor(this);
        isSimboloMonedaDolar = "$"; //@JBELVY
        isNumeroDecimales = EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(this);
        isStockRestrictivo = EnumConfiguracion.STOCK_RESTRICTIVO.getValor(this);
        isBonificacion = EnumFuncion.BONIFICACION.getValor(this);
        isDescuento = EnumFuncion.DESCUENTO.getValor(this);
        isGps = EnumConfiguracion.GPS.getValor(this);
        isTipoCliente = EnumConfiguracion.PRECIO_TIPO_CLIENTE.getValor(this);
        isPrecionCondVenta = EnumConfiguracion.PRECIO_CONDICION_VENTA.getValor(this);
        isConsultarProducto = Configuracion.EnumConfiguracion.CONSULTAR_PRODUCTO.getValor(this);
        isMostrarDirDespacho = EnumConfiguracion.MOSTRAR_DIRECCION_DESPACHO.getValor(this);
        isMostrarAlmacen = EnumConfiguracion.MOSTRAR_ALMACEN.getValor(this);
        isBonificacionAutomatica = EnumConfiguracion.BONIFICACION_AUTOMATICA.getValor(this);
        isPresentacion = EnumFuncion.FRACCIONAMIENTO.getValor(this);
        isMostrarCaberceraPedido = EnumConfiguracion.MOSTRAR_CABECERA_PEDIDO.getValor(this);
        isMaximoNumeroDecimales = EnumConfiguracion.MAXIMO_NUMERODECIMALES.getValor(this);
        isFraccionamiento = EnumFuncion.FRACCIONAMIENTO.getValor(this);
        isMaximoItemsPedido = Integer.parseInt(EnumConfiguracion.MAXIMO_ITEMS_PEDIDO.getValor(this));
        obtenerValorMontoPedido();
        setMaximoNumeroDecimales();

        pedidopendiente = UtilitarioData.fnObtenerPreferencesBoolean(this, "pedidoPendiente");

        sharePreferenceBeanPedidoCab = "beanPedidoCabVerificarPedido";
        String lsBeanVisitaCab = UtilitarioData.fnObtenerPreferencesString(this, sharePreferenceBeanPedidoCab);

        if (lsBeanVisitaCab.equals("") || editarFormulario) {
            if (!pedidopendiente) {
                sharePreferenceBeanPedidoCab = "beanPedidoCab";
            } else {
                sharePreferenceBeanPedidoCab = "beanPedidoCabPendiente";
            }
            ((AplicacionEntel) getApplication()).setCurrentCodFlujo(Configuracion.CONSPEDIDO);
            lsBeanVisitaCab = UtilitarioData.fnObtenerPreferencesString(this, sharePreferenceBeanPedidoCab);
        }

        loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper.fromJson(lsBeanVisitaCab, BeanEnvPedidoCab.class);
        BeanCliente beanCliente;

        if (((AplicacionEntel) getApplication()).getClienteOnline() == 0) {
            beanCliente = new DalCliente(this).fnSelxCodCliente(loBeanPedidoCab.getCodigoCliente());

        } else {
            beanCliente = ((AplicacionEntel) getApplication()).getCurrentCliente();
        }

        ((AplicacionEntel) getApplication()).setCurrentCliente(beanCliente);

        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);
        UtilitarioPedidos.IndicarMensajeOkError(this);
        subIniMenu();
        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(true);


    }

    private void obtenerValorMontoPedido() {

        if (Configuracion.EnumConfiguracion.MONTO_MAXIMO_PEDIDO.getValor(this).equals(""))
            isMontoMaximoPedido = 0.0;
        else
            isMontoMaximoPedido = Double.parseDouble(Configuracion.EnumConfiguracion.MONTO_MAXIMO_PEDIDO.getValor(this));

        if (Configuracion.EnumConfiguracion.MONTO_MINIMO_PEDIDO.getValor(this).equals(""))
            isMontoMinimoPedido = 0.0;
        else
            isMontoMinimoPedido = Double.parseDouble(Configuracion.EnumConfiguracion.MONTO_MINIMO_PEDIDO.getValor(this));

    }

    public void subIniActionBar(Type type, int itemId) {
        addActionBarItem(type, itemId);
    }

    @Override
    protected void onResume() {
        super.onResume();

        String lsBeanVisitaCab = UtilitarioData.fnObtenerPreferencesString(this, sharePreferenceBeanPedidoCab);

        if (lsBeanVisitaCab.equals("")) {
            sharePreferenceBeanPedidoCab = (!pedidopendiente) ? "beanPedidoCab" : "beanPedidoCabPendiente";
            lsBeanVisitaCab = UtilitarioData.fnObtenerPreferencesString(this, sharePreferenceBeanPedidoCab);
        }

        loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper.fromJson(lsBeanVisitaCab, BeanEnvPedidoCab.class);
        actualizarBoton();

        try {
            popular();
            llenarLista();
        } catch (Exception e) {
            Log.e("OnResumen", e.toString());
        }
    }

    public void llenarLista() {

        try {
            iolista = new DalProducto(this).fnListNoDBProducto(loBeanPedidoCab);
            String flgVerificacion = UtilitarioData.fnObtenerPreferencesString(this, "isHttpVerificarPedido");

            for (BeanEnvPedidoDet bean : iolista) {
                if (flgVerificacion.trim().equals("true")) {
                    bean.CalcularMontoSolesItemVerificarPedido();
                    bean.CalcularMontoDolarItemVerificarPedido();
                } else {
                    bean.calcularMontoItem();
                    bean.calcularMontoDolarItem();
                }
            }

            loBeanPedidoCab.EliminarListaDetallePedido();
            loBeanPedidoCab.setListaDetPedido(iolista);

            if (flgVerificacion.equals("true")) {
                loBeanPedidoCab.calcularMontoCompletoVerificarPedido();
                loBeanPedidoCab.calcularMontoSolesCompletoVerificarPedido();
                loBeanPedidoCab.calcularMontoDolarCompletoVerificarPedido();
            } else {
                loBeanPedidoCab.calcularMontoCompleto();
                loBeanPedidoCab.calcularMontoSolesCompleto();
                loBeanPedidoCab.calcularMontoDolarCompleto();
            }

            UtilitarioData.fnCrearPreferencesPutString(this, sharePreferenceBeanPedidoCab, BeanMapper.toJson(loBeanPedidoCab, false));
            adapter = new AdapProductoPedido(this, R.layout.productopedido_item, iolista);
            lstproductopedido.setAdapter(adapter);
            ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(true);

        } catch (Exception e) {
            Log.e("llenarLista", e.toString());
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onBackPressed() {

        if (isMostrarCaberceraPedido.equals(Configuracion.FLGVERDADERO)) {
            showDialog(CONSBACKFORMULARIOINICIO);
        } else {
            if (pedidopendiente)
                showDialog(CONSBACKPENDIENTES);
            else
                showDialog(CONSBACK);
        }
    }

    @Override
    protected void subIniActionBar() {
        this.getActionBarGD().setTitle(getString(loBeanPedidoCab.getFlgTipo().equals(Configuracion.FLGPEDIDO) ? R.string.actproductodetalle_bandeja_producto : R.string.actproductodetalle_cotizacion));
        if (loBeanPedidoCab.isEnEdicion()) addActionBarItem(Type.Nex_Shopping, ACTBARDESCARTAR);
        addActionBarItem(Type.Nex_Shopping, ACTBARDOWN);
    }

    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        final Dialog dialog = new Dialog(this, R.style.cust_dialog);

        dialog.setContentView(R.layout.dialogo_personalizado);
        dialog.setTitle("Pedido Rápido");

        dialog.show();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        Button buttonAceptar = (Button) dialog.findViewById(R.id.botonAgregarProd);
        Button buttonCancelar = (Button) dialog.findViewById(R.id.botonCancelarProd);
        txtCodProdPred = (TextView) dialog.findViewById(R.id.txtCodProdPred);
        if (isFraccionamiento.equals(Configuracion.FLGVERDADERO)) {
            txtCodProdPred.setText("Codigo Presentacion :");
        }
        edittextoCodProd = (EditText) dialog.findViewById(R.id.edittextoCodProd);
        edittextoCodCant = (EditText) dialog.findViewById(R.id.edittextoCodCant);
        idNomProd = (TextView) dialog.findViewById(R.id.idNomProd);
        edittextoCodCant.setEnabled(false);
        idNomProd.setText("Producto no encontrado");


        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                codigoProdPedRap = edittextoCodProd.getText().toString();
                DalProducto loDalProducto = new DalProducto(ActProductoPedido.this);
                if (isFraccionamiento.equals(Configuracion.FLGVERDADERO)) {
                    beanPresentacion = loDalProducto.fnListSelProductosPresentacionPedidoRapido(codigoProdPedRap);
                } else {
                    beanProducto = loDalProducto.fnListSelProductosPedidoRapido(codigoProdPedRap);
                }
                if (isFraccionamiento.equals(Configuracion.FLGVERDADERO)) {
                    if (beanPresentacion.getNombre() != ("")) {
                        idNomProd.setText(beanPresentacion.getNombre());
                        edittextoCodCant.requestFocus();
                        edittextoCodCant.setEnabled(true);
                    } else {
                        idNomProd.setText("Presentacion no encontrada");
                        edittextoCodCant.setText("");
                        edittextoCodCant.setEnabled(false);
                        edittextoCodProd.requestFocus();
                    }
                } else {
                    if (beanProducto.getNombre() != ("")) {
                        idNomProd.setText(beanProducto.getNombre());
                        edittextoCodCant.requestFocus();
                        edittextoCodCant.setEnabled(true);
                    } else {
                        idNomProd.setText("Producto no encontrado");
                        edittextoCodCant.setText("");
                        edittextoCodCant.setEnabled(false);
                        edittextoCodProd.requestFocus();
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        edittextoCodProd.addTextChangedListener(textWatcher);

        buttonAceptar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edittextoCodCant.getText().toString().length() == 0 && edittextoCodProd.getText().toString().length() == 0) {
                    Toast.makeText(getApplicationContext(), "Debe completar los campos", Toast.LENGTH_SHORT).show();
                } else {

                    if (idNomProd.getText().toString().equals("Producto no encontrado") && edittextoCodCant.getText().toString().length() == 0) {
                        Toast.makeText(getApplicationContext(), "Producto no existe", Toast.LENGTH_SHORT).show();
                    } else {
                        if (edittextoCodCant.getText().toString().length() == 0 || edittextoCodProd.getText().toString().length() == 0) {
                            Toast.makeText(getApplicationContext(), "Debe completar los campos", Toast.LENGTH_SHORT).show();

                        } else {
                            if (isFraccionamiento.equals(Configuracion.FLGVERDADERO)) {

                                AgregarPresentacionPedidoRapido();
                            } else {
                                AgregarProductoPedidoRapido();
                            }
                            edittextoCodCant.setText("");
                            edittextoCodProd.setText("");
                            edittextoCodProd.requestFocus();
                            idNomProd.setText("Producto no encontrado");
                        }
                    }
                }
            }
        });
        buttonCancelar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        return true;
    }

    public void AgregarPresentacionPedidoRapido() {
        DalProducto loDalProducto = new DalProducto(this);
        String isIdArticulo = beanPresentacion.getId();
        String isListaPrecio = listaPrecio(isIdArticulo);
        Boolean stocksuperado = false;
        String txtcantidad = edittextoCodCant.getText().toString();
        BeanEnvPedidoDet loBeanPedidoDet = new BeanEnvPedidoDet();
        ioBeanArticulo = loDalProducto.fnSelxIdPresentacion(isIdArticulo);
        BeanPrecio ioBeanPrecio = new BeanPrecio();
        ioBeanPrecio = TextUtils.isEmpty(isListaPrecio) ?
                ioBeanArticulo.fnBeanPrecio() :
                isPresentacion.equals(Configuracion.FLGVERDADERO) ?
                        loDalProducto.fnSelPrecioPresentacionXListaPrecioXCodigo(isListaPrecio)
                        : loDalProducto.fnSelPrecioXListaPrecioXCodigo(isListaPrecio);

        boolean productoexiste = validaproducto(isIdArticulo);
        if (isStockRestrictivo.equals(Configuracion.FLGVERDADERO)) {
            if (Double.parseDouble(beanPresentacion.getStock()) < Double.parseDouble(txtcantidad)) {
                stocksuperado = true;
            }
        }
        if (stocksuperado == true) {
            Toast.makeText(getApplicationContext(), "Cantidad supera stock disponible", Toast.LENGTH_SHORT).show();
        }
        if (productoexiste == false && stocksuperado == false) {
            double montoBruto = Double.parseDouble(txtcantidad) * Double.parseDouble(ioBeanPrecio.getPrecio());
            double montoBrutoSoles = Double.parseDouble(txtcantidad) * Double.parseDouble(ioBeanPrecio.getPrecioSoles()); //@JBELVY
            double montoBrutoDolares = Double.parseDouble(txtcantidad) * Double.parseDouble(ioBeanPrecio.getPrecioDolares()); //@JBELVY

            //@JBELVY I
            String valorSoles = ioBeanPrecio.getPrecioSoles();
            String valorMoneda = "1";
            if(valorSoles.equals("0") || valorSoles.equals("0.0") || valorSoles.equals("0.00") || valorSoles.equals("")){
                valorMoneda = "2";
            }  //@JBELVY F

            loBeanPedidoDet.setCodPedido("");
            loBeanPedidoDet.setAtendible("1");
            loBeanPedidoDet.setBonificacion("0.00");
            loBeanPedidoDet.setBonificacionFrac("0");
            loBeanPedidoDet.setCantBonificacion(0.00);
            loBeanPedidoDet.setCantidad(txtcantidad);
            loBeanPedidoDet.setCantidadFrac("0");
            loBeanPedidoDet.setCantidadPre(String.valueOf(beanPresentacion.getCantidad()));
            loBeanPedidoDet.setCodAlmacen("0");
            loBeanPedidoDet.setCodBonificacion(0);
            loBeanPedidoDet.setCodListaArticulo("");
            loBeanPedidoDet.setCodigoArticulo(beanPresentacion.getCodigo());
            loBeanPedidoDet.setDescuento("0");
            loBeanPedidoDet.setEditBonificacion(true);
            loBeanPedidoDet.setEmpresa(loBeanPedidoCab.getEmpresa());
            loBeanPedidoDet.setFechaUltVenta("");
            loBeanPedidoDet.setFlete("0.00");
            loBeanPedidoDet.setIdArticulo(beanPresentacion.getId());
            loBeanPedidoDet.setIdListaPrecio(isListaPrecio);
            loBeanPedidoDet.setMonto((Utilitario.fnRound(montoBruto, Integer.valueOf(isNumeroDecimales))));
            loBeanPedidoDet.setMontoSoles((Utilitario.fnRound(montoBrutoSoles, Integer.valueOf(isNumeroDecimales)))); //@JBELVY
            loBeanPedidoDet.setMontoDolar((Utilitario.fnRound(montoBrutoDolares, Integer.valueOf(isNumeroDecimales)))); //@JBELVY
            loBeanPedidoDet.setMontoSinDescuento("0");
            loBeanPedidoDet.setMultBonificacion(0);
            loBeanPedidoDet.setNombreArticulo(beanPresentacion.getNombre());
            loBeanPedidoDet.setObservacion("");
            loBeanPedidoDet.setPrecioBase(ioBeanPrecio.getPrecio());
            loBeanPedidoDet.setPrecioBaseSoles(ioBeanPrecio.getPrecioSoles()); //@JBELVY
            loBeanPedidoDet.setPrecioBaseDolares(ioBeanPrecio.getPrecioDolares()); //@JBELVY
            loBeanPedidoDet.setTipoMoneda(valorMoneda); //@JBELVY
            loBeanPedidoDet.setPrecioFrac("0");
            loBeanPedidoDet.setProducto(beanPresentacion.getProducto());
            loBeanPedidoDet.setStock(beanPresentacion.getStock());
            loBeanPedidoDet.setTipo("P");
            loBeanPedidoDet.setTipoDescuento(Configuracion.DESCUENTO_PORCENTAJE);
            loBeanPedidoCab.agregarProductoPedido(loBeanPedidoDet);

            if (isBonificacionAutomatica.equals(Configuracion.FLGVERDADERO)) {
                try {
                    loBeanPedidoCab = new DalPedido(this).fnValidarBonificacion(loBeanPedidoCab, iiMaximoNumeroDecimales, isStockRestrictivo);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            UtilitarioData.fnCrearPreferencesPutBoolean(this, "editoPedido", true);
            llenarLista();
            popular();
            Toast.makeText(getApplicationContext(), "Se agrego la presentación", Toast.LENGTH_SHORT).show();
            actualizarBoton();
        } else {
            if (productoexiste == true) {
                Toast.makeText(getApplicationContext(), "La presentación ya existe", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void AgregarProductoPedidoRapido() {
        DalProducto loDalProducto = new DalProducto(this);

        String isIdArticulo = beanProducto.getId();
        String isListaPrecio = listaPrecio(isIdArticulo);
        ioBeanArticulo = loDalProducto.fnSelxIdProducto(isIdArticulo);


        BeanPrecio ioBeanPrecio = new BeanPrecio();
        ioBeanPrecio = TextUtils.isEmpty(isListaPrecio) ?
                ioBeanArticulo.fnBeanPrecio() :
                isPresentacion.equals(Configuracion.FLGVERDADERO) ?
                        loDalProducto.fnSelPrecioPresentacionXListaPrecioXCodigo(isListaPrecio)
                        : loDalProducto.fnSelPrecioXListaPrecioXCodigo(isListaPrecio);


        Boolean stocksuperado = false;
        String txtcantidad = edittextoCodCant.getText().toString();
        BeanEnvPedidoDet loBeanPedidoDet = new BeanEnvPedidoDet();

        boolean productoexiste = validaproducto(isIdArticulo);
        if (isStockRestrictivo.equals(Configuracion.FLGVERDADERO)) {
            if (Double.parseDouble(beanProducto.getStock()) < Double.parseDouble(txtcantidad)) {
                stocksuperado = true;
            }
        }
        if (stocksuperado == true) {
            Toast.makeText(getApplicationContext(), "Cantidad supera stock disponible", Toast.LENGTH_SHORT).show();
        }
        if (productoexiste == false && stocksuperado == false) {
            double montoBruto = Double.parseDouble(txtcantidad) * Double.parseDouble(ioBeanPrecio.getPrecio());
            double montoBrutoSoles = Double.parseDouble(txtcantidad) * Double.parseDouble(ioBeanPrecio.getPrecioSoles()); //@JBELVY
            double montoBrutoDolar = Double.parseDouble(txtcantidad) * Double.parseDouble(ioBeanPrecio.getPrecioDolares()); //@JBELVY

            //@JBELVY I
            String valorSoles = ioBeanPrecio.getPrecioSoles();
            String valorMoneda = "1";
            if(valorSoles.equals("0") || valorSoles.equals("0.0") || valorSoles.equals("0.00") || valorSoles.equals("")){
                valorMoneda = "2";
            } //@JBELVY F

            loBeanPedidoDet.setCodPedido("");
            loBeanPedidoDet.setAtendible("1");
            loBeanPedidoDet.setBonificacion("0.00");
            loBeanPedidoDet.setBonificacionFrac("0");
            loBeanPedidoDet.setCantBonificacion(0.00);
            loBeanPedidoDet.setCantidad(txtcantidad);
            loBeanPedidoDet.setCantidadFrac("0");
            loBeanPedidoDet.setCantidadPre("0");
            loBeanPedidoDet.setCodAlmacen("0");
            loBeanPedidoDet.setCodBonificacion(0);
            loBeanPedidoDet.setCodListaArticulo("");
            loBeanPedidoDet.setCodigoArticulo(beanProducto.getCodigo());
            loBeanPedidoDet.setDescuento("0");
            loBeanPedidoDet.setEditBonificacion(true);
            loBeanPedidoDet.setEmpresa(loBeanPedidoCab.getEmpresa());
            loBeanPedidoDet.setFechaUltVenta("");
            loBeanPedidoDet.setFlete("0.00");
            loBeanPedidoDet.setIdArticulo(beanProducto.getId());
            loBeanPedidoDet.setIdListaPrecio(isListaPrecio);
            loBeanPedidoDet.setMonto((Utilitario.fnRound(montoBruto, Integer.valueOf(isNumeroDecimales))));
            loBeanPedidoDet.setMontoSoles((Utilitario.fnRound(montoBrutoSoles, Integer.valueOf(isNumeroDecimales)))); //@JBELVY
            loBeanPedidoDet.setMontoDolar((Utilitario.fnRound(montoBrutoDolar, Integer.valueOf(isNumeroDecimales)))); //@JBELVY
            loBeanPedidoDet.setMontoSinDescuento("0");
            loBeanPedidoDet.setMultBonificacion(0);
            loBeanPedidoDet.setNombreArticulo(beanProducto.getNombre());
            loBeanPedidoDet.setObservacion("");
            loBeanPedidoDet.setPrecioBase(ioBeanPrecio.getPrecio());
            loBeanPedidoDet.setPrecioBaseSoles(ioBeanPrecio.getPrecioSoles()); //@JBELVY
            loBeanPedidoDet.setPrecioBaseDolares(ioBeanPrecio.getPrecioDolares()); //@JBELVY
            loBeanPedidoDet.setTipoMoneda(valorMoneda); //@JBELVY
            loBeanPedidoDet.setPrecioFrac("0");
            loBeanPedidoDet.setProducto(null);
            loBeanPedidoDet.setStock(beanProducto.getStock());
            loBeanPedidoDet.setTipo("P");
            loBeanPedidoDet.setTipoDescuento(Configuracion.DESCUENTO_PORCENTAJE);
            loBeanPedidoCab.agregarProductoPedido(loBeanPedidoDet);

            if (isBonificacionAutomatica.equals(Configuracion.FLGVERDADERO)) {
                try {
                    loBeanPedidoCab = new DalPedido(this).fnValidarBonificacion(loBeanPedidoCab, iiMaximoNumeroDecimales, isStockRestrictivo);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            UtilitarioData.fnCrearPreferencesPutBoolean(this, "editoPedido", true);
            llenarLista();
            popular();
            Toast.makeText(getApplicationContext(), "Se agrego el producto", Toast.LENGTH_SHORT).show();
            actualizarBoton();
        } else {
            if (productoexiste == true) {
                Toast.makeText(getApplicationContext(), "El producto ya existe", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public boolean validaproducto(String codigo) {
        Boolean existeproducto = false;
        if (loBeanPedidoCab.getListaDetPedido().size() > 0) {
            for (int i = 0; i < loBeanPedidoCab.getListaDetPedido().size(); i++) {
                if (codigo.equals(loBeanPedidoCab.getListaDetPedido().get(i).getIdArticulo())) {
                    existeproducto = true;
                    break;
                }
            }
        }
        return existeproducto;
    }

    ;

    private String listaPrecio(String idProducto) {
        String idlistaPrecio;
        String codCondVta = "";
        String codTipoCli = "";
        String strcodCodVenta = "";
        if (loBeanPedidoCab != null) {
            codCondVta = loBeanPedidoCab.getCondicionVenta();

        }
        if (isTipoCliente.equals(
                Configuracion.FLGVERDADERO))
            codTipoCli = ((AplicacionEntel) getApplication())
                    .getCurrentCliente().getTclicod();
        else
            codTipoCli = "";

        if (isPrecionCondVenta.equals(
                Configuracion.FLGVERDADERO))
            strcodCodVenta = codCondVta;
        else
            strcodCodVenta = "";


        List<ThumbnailItem> listaPrecios = new ArrayList<ThumbnailItem>();
        DalProducto loDalProducto = new DalProducto(this);
        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            listaPrecios = loDalProducto
                    .fnSelListPrecioXCodPresentacionXCanalXCondVta(
                            idProducto, codTipoCli, strcodCodVenta,
                            isSimboloMoneda, isSimboloMonedaDolar, isNumeroDecimales);
        } else {
            listaPrecios = loDalProducto
                    .fnSelListPrecioXCodProductoXCanalXCondVta(
                            idProducto, codTipoCli, strcodCodVenta,
                            isSimboloMoneda, isSimboloMonedaDolar, isNumeroDecimales);

        }

        if (listaPrecios.size() == 0) {
            return idlistaPrecio = "";

        } else {
            ThumbnailItem loitemprod = (ThumbnailItem) listaPrecios.get(0);
            idlistaPrecio = String.valueOf(loitemprod.getTag());
            return idlistaPrecio;
        }

    }

    @Override
    public void setOnItemClickPopup(Object poGeneral, int piTipo) {
        try {
            BeanGeneral loGeneral = (BeanGeneral) poGeneral;
            switch (Integer.parseInt(loGeneral.getCodigo())) {
                case EDIT_ID:// Edita producto
                    subModificar(Integer.parseInt(loGeneral.getExtra()));
                    return;
                case DELETE_ID:// borra producto
                    subEliminar(Integer.parseInt(loGeneral.getExtra()));
                    return;
                case CANCEL_ID:
                    return;
            }
        } catch (NumberFormatException e) {
            Log.e("XXX", "ActProductoPedido2.setOnItemClickPopup", e);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View convertView, int position, long id) {

        List<BeanGeneral> _lstGeneral = new ArrayList<BeanGeneral>();

        BeanEnvPedidoDet loBeanEnvPedidoDet = loBeanPedidoCab.getListaDetPedido().get(position);

        Boolean isProductoSincronizado;
        DalProducto dalProducto = new DalProducto(this);
        String codigoArticulo = loBeanEnvPedidoDet.getCodigoArticulo();

        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            isProductoSincronizado = dalProducto.fnExistePresentacionSincronizado(codigoArticulo);
        } else {
            isProductoSincronizado = dalProducto.fnExisteProductoSincronizado(codigoArticulo);
        }

        if (isProductoSincronizado) {
            if (loBeanEnvPedidoDet.isEditBonificacion()) {
                _lstGeneral.add(new BeanGeneral(String.valueOf(EDIT_ID),
                        getString(R.string.actproductopedido_opceditarproducto), String
                        .valueOf(position)));
            }

            _lstGeneral.add(new BeanGeneral(String.valueOf(DELETE_ID),
                    getString(R.string.actproductopedido_opcborrarproducto), String
                    .valueOf(position)));

            _lstGeneral.add(new BeanGeneral(String.valueOf(CANCEL_ID),
                    getString(R.string.actproductopedido_opccancelarproducto),
                    String.valueOf(position)));

            new ListaPopup().dialog(this, _lstGeneral, 0, this, "",
                    new BeanGeneral(), R.drawable.popup_lista);
        } else {
            showDialog(DETALLE);
        }

        return true;
    }

    @Override
    protected void subSetControles() {
        // TODO Auto-generated method stub
        super.subSetControles();

        try {
            setActionBarContentView(R.layout.productopedido);

            txtcantproductos = (TextView) findViewById(R.id.actproductopedido_txtcantproductos);
            txtmontototal = (TextView) findViewById(R.id.actproductopedido_txtmontototal);
            txtmontototalDolar = (TextView) findViewById(R.id.actproductopedido_txtmontototalDolar); //@JBELVY
            txttotalitems = (TextView) findViewById(R.id.actproductopedido_txttotalitems);
            //  txtfletetotal = (TextView) findViewById(R.id.actproductopedido_txtfletetotal);
            lstproductopedido = (ListView) findViewById(R.id.actproductopedido_lstproductopedido);
            btnagregarproducto = (Button) findViewById(R.id.actproductopedido_btnagregarproducto);
            btnfinalizar = (Button) findViewById(R.id.actproductopedido_btnfinalizar);
            btneliminar = (Button) findViewById(R.id.actproductopedido_btneliminar);
            lnContentHeader = (LinearLayout) findViewById(R.id.lnContentHeader);
            //lnFlete = (LinearLayout) findViewById(R.id.actproductopedido_lnflete);
            lnLista = (LinearLayout) findViewById(R.id.lnLista);

            View empty = findViewById(R.id.empty);

            lstproductopedido.setEmptyView(empty);
            lstproductopedido.setOnItemClickListener(this);
            btnagregarproducto.setOnClickListener(this);
            btnfinalizar.setOnClickListener(this);
            btneliminar.setOnClickListener(this);

            actualizarBoton();
            animations();

            if (!isUsingDialogFragment()) {
                registerForContextMenu(lstproductopedido);
            } else {
                lstproductopedido.setOnItemLongClickListener(this);
            }
        } catch (Exception e) {
            Log.e("subSetControles", e.toString());
            subShowException(e);
        }
    }

    private void setMaximoNumeroDecimales() {
        iiMaximoNumeroDecimales = 0;
        try {
            iiMaximoNumeroDecimales = Integer.parseInt(isMaximoNumeroDecimales);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ActProductoDetalle", "Error conversion maximo numero de decimales: " + e.getMessage());
        }
    }

    private void animations() {
        animSideDown = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);

        animSlideUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);

        animSlideUp.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                lnContentHeader.setVisibility(View.GONE);
                subIniActionBar(Type.Nex_ArrowUp, ACTBARUP);
            }
        });
        animSideDown.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                lnContentHeader.setVisibility(View.VISIBLE);
                subIniActionBar(Type.Nex_ArrowDown, ACTBARDOWN);
            }
        });

    }

    private void actualizarBoton() {

        //actualizar valor del flag para verificar si hubo edicion
        editoPedido = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getBoolean("editoPedido", true);

        boolean pedidoValido = true;

        try {
            pedidoValido = loBeanPedidoCab.pedidoValidado();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isStockRestrictivo.equals(
                Configuracion.FLGFALSO)) {
            btneliminar.setVisibility(View.GONE);
        } else {
            if (pedidoValido) {
                btneliminar.setVisibility(View.GONE);
                if (!editoPedido) {
                    btnfinalizar.setText(getString(R.string.actproductopedido_lblterminar));
                } else {
                    btnfinalizar.setText(getString(R.string.actproductopedido_lblverificar));
                }
            } else {
                btneliminar.setVisibility(View.VISIBLE);
                btnfinalizar.setText(getString(R.string.actproductopedido_lblverificar));
            }
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {
        Log.i("subaccdesmensaje", "entrroo");
        Intent loInstent;

        if (estadoSincronizar) {
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActMenu.class);
                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }
        } else if (estadoActualizar) {
            estadoActualizar = false;
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {
                DalPedido loDalPedido = null;
                try {
                    loDalPedido = new DalPedido(ActProductoPedido.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                loDalPedido.fnUpdEstadoPedidoOnline(loBeanPedidoCab.getCodigoPedidoServidor());
            }
            finish();
            startActivity(new Intent(ActProductoPedido.this, ActPedidoListaOnline.class));
        } else {//validacion de grilla
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {

                if (loBeanPedidoCab.getFlgEliminar().equals(Configuracion.FLGVERDADERO)) {
                    DalPedido loDalPedido = null;

                    try {
                        loDalPedido = new DalPedido(ActProductoPedido.this);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    if (!loBeanPedidoCab.getCodigoPedido().equals("0")) {
                        loDalPedido.fnDelTrCabPedidoCodigo(loBeanPedidoCab.getCodigoPedido());
                    }

                    irclientedetalle();

                } else {

                    BeanValidaPedidoAndroid lobean = ((AplicacionEntel) getApplication()).getCurrentValidaPedido();
                    loBeanPedidoCab.setCodigoPedidoServidor(lobean.getCodPedidoServidor());
                    loBeanPedidoCab = subupStockDetalle(lobean.getListDetPedido(), loBeanPedidoCab);

                    //actualizar stock
                    if ((isMostrarAlmacen.equals(Configuracion.FLGVERDADERO))) {
                        DalAlmacen loDalAlmacen = new DalAlmacen(this);
                        if ((isPresentacion.equals(Configuracion.FLGVERDADERO))) {
                            loDalAlmacen.subupStockPresentacion(loBeanPedidoCab.getListaDetPedido());
                        } else {
                            loDalAlmacen.subupStockProductos(loBeanPedidoCab.getListaDetPedido());
                        }
                    } else {
                        DalProducto loDalProducto = new DalProducto(this);
                        if ((isPresentacion.equals(Configuracion.FLGVERDADERO))) {
                            loDalProducto.subupStockPresentacion(loBeanPedidoCab.getListaDetPedido());
                        } else {
                            loDalProducto.subupStockProductos(loBeanPedidoCab.getListaDetPedido());
                        }
                    }

                    String lsPedidoCab1 = "";
                    try {
                        lsPedidoCab1 = BeanMapper.toJson(loBeanPedidoCab, false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    UtilitarioData.fnCrearPreferencesPutString(this, sharePreferenceBeanPedidoCab, lsPedidoCab1);
                    llenarLista();
                    UtilitarioData.fnCrearPreferencesPutBoolean(this, "editoPedido", false);
                    actualizarBoton();
                }
            } else {

                if (loBeanPedidoCab.getFlgEliminar().equals(Configuracion.FLGVERDADERO)) {
                    DalPedido loDalPedido = null;

                    try {
                        loDalPedido = new DalPedido(ActProductoPedido.this);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    if (!loBeanPedidoCab.getCodigoPedido().equals("0")) {
                        loDalPedido.fnDelTrCabPedidoCodigo(loBeanPedidoCab.getCodigoPedido());
                    }
                    irclientedetalle();
                } else {

                    String lsPedidoCab1 = "";
                    try {
                        lsPedidoCab1 = BeanMapper.toJson(loBeanPedidoCab, false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    UtilitarioData.fnCrearPreferencesPutString(this, sharePreferenceBeanPedidoCab, lsPedidoCab1);

                    showDialog(ERRORPEDIDO);
                }

            }
            btnfinalizar.setEnabled(true);
        }
    }

    public BeanEnvPedidoCab subupStockDetalle(List<BeanEnvPedidoDet> ListRptaDetPedido, BeanEnvPedidoCab beanPedido) {
        BeanEnvPedidoDet detalle;
        for (int i = 0; i < ListRptaDetPedido.size(); i++) {
            detalle = ListRptaDetPedido.get(i);
            beanPedido.actualizarStock(detalle.getCodigoArticulo(), detalle.getStock(), detalle.getAtendible());
        }
        return beanPedido;
    }

    private void popular() {

      /*  if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO)) {
           lnFlete.setVisibility(View.VISIBLE);
            txtfletetotal.setText(isSimboloMoneda
                    + " "
                    + Utilitario.fnRound(loBeanPedidoCab.calcularTotalFlete(),
                    Integer.valueOf(isNumeroDecimales)));
        } else
            lnFlete.setVisibility(View.GONE);*/


        txtcantproductos.setText(String.valueOf(loBeanPedidoCab
                .getListaDetPedido().size()));

        String flgVerificacion = getApplicationContext().getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("isHttpVerificarPedido", "");

        String isTipoArticulo = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(this)
                .equals(Configuracion.FLGVERDADERO) ? Configuracion.TipoArticulo.PRESENTACION
                : Configuracion.TipoArticulo.PRODUCTO;

        txttotalitems.setText(loBeanPedidoCab.calcularCantFracTotalItems(isTipoArticulo,
                isTipoArticulo.equals(Configuracion.TipoArticulo.PRESENTACION) ? Integer.valueOf(isNumeroDecimales) : iiMaximoNumeroDecimales));

        String montoTotal = "";
        String montoTotalSoles = "";
        String montoTotalDolar = "";

        if (flgVerificacion.equals("true")) {
            montoTotal = loBeanPedidoCab.calcularMontoTotalVerificarPedido();
            montoTotalSoles = loBeanPedidoCab.calcularMontoTotalVerificarPedidoSoles();
            montoTotalDolar = loBeanPedidoCab.calcularMontoTotalVerificarPedidoDolar();
        } else {
            montoTotal = loBeanPedidoCab.calcularMontoTotal();
            montoTotalSoles = loBeanPedidoCab.calcularMontoTotalSoles();
            montoTotalDolar = loBeanPedidoCab.calcularMontoTotalDolar();
        }

        Logger.d("XXX", "loBeanPedidoCab.calcularCantTotalItems() : " + loBeanPedidoCab.calcularCantTotalItems());
        txtmontototal.setText(isSimboloMoneda
                + " "
                + Utilitario.fnRound(montoTotalSoles, //@JBELVY Before montoTotal
                Integer.valueOf(isNumeroDecimales)));

        txtmontototalDolar.setText(isSimboloMonedaDolar
                + " "
                + Utilitario.fnRound(montoTotalDolar,
                Integer.valueOf(isNumeroDecimales))); //@JBELVY

     /*   txtcantproductos.setCompoundDrawablesWithIntrinsicBounds(
                R.drawable.icono_productos, 0, 0, 0);*/

        //actualizar valor del flag para verificar si hubo edicion
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub

        StringBuffer mensaje = new StringBuffer();
        // Se le quita uno adicional por la cabecera negra
        BeanEnvPedidoDet bp = (BeanEnvPedidoDet) loBeanPedidoCab
                .getListaDetPedido().get(arg2);

        String descuento = "";
        String tipoMoneda = bp.getTipoMoneda();
        if (bp.getTipoDescuento().compareTo("M") == 0) {
            descuento = getString(R.string.moneda) + " " + bp.getDescuento();
        } else if (bp.getTipoDescuento().compareTo("P") == 0) {
            descuento = bp.getDescuento() + " %";
        }
        double stock = 0;
        if (TextUtils.isEmpty(bp.getStock())) {
            stock = Double.valueOf(bp.getStock());
        }
        mensaje.append(" C\u00F3digo : ")
                .append(bp.getCodigoArticulo())
                .append('\n')
                .append(" Nombre : ")
                .append(bp.getNombreArticulo())
                .append('\n')
                .append(" Stock : ")
                .append("" + ((int) stock))
                .append('\n')
                .append(" Precio Base (")
                .append(getString((tipoMoneda.equals("1") ? R.string.moneda : R.string.monedaDolares)))
                .append("): ")
                .append((tipoMoneda.equals("1") ? bp.getPrecioBaseSoles() : bp.getPrecioBaseDolares())) //@JBELVY Before getPrecioBase
                .append('\n')
                .append(" Cantidad : ")
                .append(bp.getCantidad())
                .append('\n');


        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            mensaje.append(" Frac : ")
                    .append(bp.getCantidadFrac())
                    .append('\n');
        }

        if (isBonificacion.equals(Configuracion.FLGVERDADERO)) {
            mensaje.append(" Bonificaci\u00F3n : ")
                    .append(bp.getBonificacion())
                    .append('\n');
        }
        if (isDescuento.equals(Configuracion.FLGVERDADERO)) {
            mensaje.append(" Descuento : ")
                    .append(descuento)
                    .append('\n');
        }
        mensaje.append(" Subtotal (")
                .append((tipoMoneda.equals("1") ? isSimboloMoneda : isSimboloMonedaDolar))
                .append("): ")
                .append(Utilitario.fnRound((tipoMoneda.equals("1") ? bp.getMontoSoles() : bp.getMontoDolar()), //@JBELVY Before getMonto
                        Integer.valueOf(isNumeroDecimales)))
                .append('\n').append(" Observaci\u00F3n : ")
                .append(bp.getObservacion());

        AlertDialog.Builder ale = new AlertDialog.Builder(this);
        ale.setMessage(mensaje.toString());
        ale.setPositiveButton("Aceptar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        ale.show();
    }

    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderIcon(android.R.drawable.ic_dialog_info);
        menu.setHeaderTitle("Menu");

        menu.add(0, EDIT_ID, 0,
                getString(R.string.actproductopedido_opceditarproducto));
        menu.add(0, DELETE_ID, 0,
                getString(R.string.actproductopedido_opcborrarproducto));
        menu.add(0, CANCEL_ID, 0,
                getString(R.string.actproductopedido_opccancelarproducto));
    }

    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
                .getMenuInfo();
        switch (item.getItemId()) {
            case EDIT_ID:// Edita producto
                subModificar(info);
                return true;
            case DELETE_ID:// borra producto
                subEliminar(info);
                return true;
            case CANCEL_ID:
                return false;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void subModificar(AdapterContextMenuInfo psAdapterContextMenuInfo) {

        modificarBeanPedidoCabVerificarPedido();
        BeanEnvPedidoDet bp = (BeanEnvPedidoDet) loBeanPedidoCab.getListaDetPedido().get(psAdapterContextMenuInfo.position);
        String lsProducto = bp.getIdArticulo();
        Intent loIntent = new Intent(this, ActProductoEditar.class);
        loIntent.putExtra("IdArticulo", lsProducto);
        loIntent.putExtra("CodBonificacion", bp.getCodBonificacion());
        finish();
        startActivity(loIntent);
    }

    private void subModificar(int position) {

        modificarBeanPedidoCabVerificarPedido();
        BeanEnvPedidoDet bp = (BeanEnvPedidoDet) loBeanPedidoCab.getListaDetPedido().get(position);
        String lsProducto = bp.getIdArticulo();
        Intent loIntent = new Intent(this, ActProductoEditar.class);
        loIntent.putExtra("IdArticulo", lsProducto);
        loIntent.putExtra("CodBonificacion", bp.getCodBonificacion());
        finish();
        startActivity(loIntent);
    }

    private void modificarBeanPedidoCabVerificarPedido() {
        if (sharePreferenceBeanPedidoCab.equals("beanPedidoCabVerificarPedido")) {
            String lsBeanPedidoCab = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA,
                    MODE_PRIVATE).getString(sharePreferenceBeanPedidoCab, "");
            loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper.fromJson(lsBeanPedidoCab, BeanEnvPedidoCab.class);
            UtilitarioData.fnCrearPreferencesPutString(this, "MensajeVerificarPedido", "");
            UtilitarioData.fnCrearPreferencesPutString(this, "isHttpVerificarPedido", "");
        }
    }

    private void subEliminar(AdapterContextMenuInfo psAdapterContextMenuInfo) {
        modificarBeanPedidoCabVerificarPedido();
        BeanEnvPedidoDet bp = (BeanEnvPedidoDet) loBeanPedidoCab.getListaDetPedido().get(psAdapterContextMenuInfo.position);
        loBeanPedidoCab.eliminarProducto(bp.getIdArticulo(), bp.getCodBonificacion());
        String lsPedidoCab = "";

        try {
            if (bp.getCodBonificacion() == 0) {
                DalPedido loDalPedido = new DalPedido(this);
                loBeanPedidoCab.subLimparBonificacion();
                loBeanPedidoCab = loDalPedido.fnValidarBonificacion(loBeanPedidoCab, iiMaximoNumeroDecimales, isStockRestrictivo);
            }
            lsPedidoCab = BeanMapper.toJson(loBeanPedidoCab, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        UtilitarioData.fnCrearPreferencesPutString(this, sharePreferenceBeanPedidoCab, lsPedidoCab);
        UtilitarioData.fnCrearPreferencesPutBoolean(this, "editoPedido", true);

        finish();
        startActivity(new Intent(this, ActProductoPedido.class));
    }

    private void subEliminar(int position) {
        modificarBeanPedidoCabVerificarPedido();

        BeanEnvPedidoDet bp = (BeanEnvPedidoDet) loBeanPedidoCab
                .getListaDetPedido().get(position);

        loBeanPedidoCab.eliminarProducto(bp.getIdArticulo(), bp.getCodBonificacion());

        String lsPedidoCab = "";
        try {
            if (bp.getCodBonificacion() == 0) {
                DalPedido loDalPedido = new DalPedido(this);
                loBeanPedidoCab.subLimparBonificacion();
                loBeanPedidoCab = loDalPedido.fnValidarBonificacion(loBeanPedidoCab, iiMaximoNumeroDecimales, isStockRestrictivo);
            }
            lsPedidoCab = BeanMapper.toJson(loBeanPedidoCab, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        UtilitarioData.fnCrearPreferencesPutString(this, sharePreferenceBeanPedidoCab, lsPedidoCab);

        llenarLista();
        actualizarBoton();
        popular();
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        Intent loInstent;
        BeanExtras loBeanExtras;
        String lsFiltro = "";

        int tamDetalle = 0;
        if (iolista != null) {
            for (int d = 0; d < iolista.size(); d++) {
                if (iolista.get(d) instanceof BeanEnvPedidoDet) {
                    tamDetalle++;
                }
            }
        }

        if (v.getId() == btnagregarproducto.getId()) {
            if (isMontoMaximoPedido > 0.0 && Double.parseDouble(loBeanPedidoCab.getMontoTotal()) > isMontoMaximoPedido) {
                UtilitarioPedidos.mostrarDialogo(this, Constantes.SUPERAMONTOMAXIMO);

            } else if (isMaximoItemsPedido > 0 && iolista.size() >= isMaximoItemsPedido) {
                UtilitarioPedidos.mostrarDialogo(this, Constantes.SUPEROMAXIMOITEMS);

            } else if (tamDetalle <= Integer.parseInt(getString(R.string.actproductopedido_cantmaxregistros)) - 1) {
                ((AplicacionEntel) getApplication())
                        .setCurrentlistaArticulo(null);
                loInstent = new Intent(this, ActProductoBuscar.class);
                loBeanExtras = new BeanExtras();
                loBeanExtras.setFiltro("");
                loBeanExtras.setFiltro2("");
                loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
                try {
                    lsFiltro = BeanMapper.toJson(loBeanExtras, false);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                loInstent.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO,
                        lsFiltro);
                finish();
                startActivity(loInstent);
            } else {
                showDialog(CONSLIMITEEXCEDIDO);
            }
        } else if (v.getId() == btnfinalizar.getId()) {

            if (isMontoMinimoPedido > 0.0 && Double.parseDouble(loBeanPedidoCab.getMontoTotal()) < isMontoMinimoPedido) {
                UtilitarioPedidos.mostrarDialogo(this, Constantes.NOSUPERAMONTOMINIMO);

            } else if (isMontoMaximoPedido > 0.0 && Double.parseDouble(loBeanPedidoCab.getMontoTotal()) > isMontoMaximoPedido) {
                UtilitarioPedidos.mostrarDialogo(this, Constantes.SUPERAMONTOMAXIMO);

            } else {
                String limiteCredito = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE).getString("LimiteCredito", "");

                if (limiteCredito.equals(Configuracion.FLGVERDADERO)) {
                    BeanCliente beanCliente = ((AplicacionEntel) getApplication()).getCurrentCliente();
                    Double saldo = Double.parseDouble(beanCliente.getSaldoCredito());
                    Double utilizado = Double.parseDouble(loBeanPedidoCab.getMontoTotal());

                    if (utilizado > saldo) {
                        creditoSuperado = true;
                        showDialog(CREDITOSUPERADO);
                    }
                }

                if (!creditoSuperado) {
                    if (isStockRestrictivo.equals(Configuracion.FLGFALSO)) {
                        irNextActivity();
                    } else {
                        if (loBeanPedidoCab.pedidoValidado()) {
                            if (!editoPedido) {
                                irNextActivity();
                            } else {
                                verificarStock();
                            }
                        } else {
                            verificarStock();
                        }
                    }

                    UtilitarioData.fnCrearPreferencesPutBoolean(this, "editoPedido", false);

                } else {
                    showDialog(CREDITOSUPERADO);
                }
            }
        } else if (v.getId() == btneliminar.getId()) {
            UtilitarioPedidos.mostrarDialogo(this, CONSELIMINARDETALLES);
        }
    }

    private void eliminarDetalles() {
        BeanEnvPedidoDet loBeanEnvPedidoDet;
        int count = loBeanPedidoCab.getListaDetPedido().size();
        while (count > 0) {
            loBeanEnvPedidoDet = loBeanPedidoCab.getListaDetPedido().get(count - 1);
            if (loBeanEnvPedidoDet.getAtendible().equals(Configuracion.FLGREGNOHABILITADO)) {
                Log.v("ATENDIBLE", "" + (count - 1));
                loBeanPedidoCab.eliminarProductoCodigo(loBeanEnvPedidoDet.getCodigoArticulo());
            }
            count--;
        }

        loBeanPedidoCab.setCantidadTotal(Utilitario.fnRound(loBeanPedidoCab.calcularCantTotalItems(), iiMaximoNumeroDecimales));

        String lsPedidoCab1 = "";
        try {
            lsPedidoCab1 = BeanMapper.toJson(loBeanPedidoCab, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        UtilitarioData.fnCrearPreferencesPutString(this, sharePreferenceBeanPedidoCab, lsPedidoCab1);

        llenarLista();
        actualizarBoton();
        popular();
    }

    private void verificarStock() {
        loBeanPedidoCab.setFlgTerminado(Configuracion.FLGFALSO);
        try {
            subGrabar();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (creditoSuperado) {
            showDialog(CONSCREDITOSUPERADO);
        } else {
            btnfinalizar.setEnabled(false);
            ValidarPedidoServer();
        }
    }

    private void irNextActivity() {
        Intent loIntent;

        if (loBeanPedidoCab.getListaDetPedido().size() > 0) {

            if (isMostrarDirDespacho.equals(Configuracion.FLGVERDADERO) &&
                    new DalCliente(ActProductoPedido.this)
                            .fnSelNumClienteDireccion(loBeanPedidoCab
                                    .getCodigoCliente(), Configuracion
                                    .EnumFlujoDireccion.DESPACHO
                                    .getCodigo(ActProductoPedido.this)) > 0) {
                loIntent = new Intent(this,
                        ActPedidoClienteDireccionLista.class);
                loIntent.putExtra(Configuracion.EXTRA_NAME_DIRECCION,
                        Configuracion.EnumFlujoDireccion.DESPACHO.name());
            } else
                loIntent = new Intent(this, ActPedidoFin.class);

            startActivity(loIntent);
            return;
        } else {
            showDialog(CONSNOITEMS);
        }
    }

    private void subGrabarValorActual() {
        // Obtenemos la fecha del sistema
        Calendar c = Calendar.getInstance();
        Date loDate = c.getTime();
        String sFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate)
                .toString();
        loBeanPedidoCab.setFechaFin(sFecha);

        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);
        // Se guarda GPS si tiene el flag habilitado
        Location location = null;

        try {
            location = Gps.getLocation();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (isGps.equals(
                Configuracion.FLGVERDADERO)
                && location != null) {
            loBeanPedidoCab.setLatitud(String.valueOf(location.getLatitude()));
            loBeanPedidoCab
                    .setLongitud(String.valueOf(location.getLongitude()));
            loBeanPedidoCab.setCelda(location.getProvider().substring(0, 1)
                    .toUpperCase());
        } else {
            loBeanPedidoCab.setLatitud("0");
            loBeanPedidoCab.setLongitud("0");
            loBeanPedidoCab.setCelda("-");
        }

        loBeanPedidoCab.setFlgEnCobertura(Utilitario.fnVerSignal(this) ? "1" : "0");
        loBeanPedidoCab.setFlgTerminado(Configuracion.FLGFALSO);

        String lsBeanVisitaCab = "";
        try {
            lsBeanVisitaCab = BeanMapper.toJson(loBeanPedidoCab, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        UtilitarioData.fnCrearPreferencesPutString(this, sharePreferenceBeanPedidoCab, lsBeanVisitaCab);
    }

    private void subGrabar() throws Exception {
        subGrabarValorActual();
        /*
        DalPedido loDalPedido = new DalPedido(this);
        if (!loBeanPedidoCab.getCodigoPedido().equals("0") && !loBeanPedidoCab.getCodigoPedido().equals(""))
            loDalPedido.fnDeletePedido(loBeanPedidoCab.getCodigoPedido());
        String codPed = loDalPedido.fnGrabarPedido(loBeanPedidoCab);
        loBeanPedidoCab.setCodigoPedido(codPed);
        */
    }

    private void ValidarPedidoServer() {
        new HttpGrabaPedido(loBeanPedidoCab, this, fnTipactividad(), getString(R.string.httpverificar_dlg)).execute();
    }

    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        DialogFragmentYesNo loDialog;
        switch (id) {
            case DIALOG_NSERVICES_DOWNLOAD:
                String lsNServices;
                if (ioDALNservices == null)
                    try {
                        ioDALNservices = DALNServices.getInstance(this,
                                R.string.db_name);
                    } catch (NexInvalidOperatorException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                try {
                    lsNServices = ioDALNservices.fnSelNServicesLabel();
                } catch (Exception e) {
                    Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                    lsNServices = getString(R.string.nservices_label_default);
                }
                loDialog = DialogFragmentYesNo.newInstance(
                        getFragmentManager(),
                        DialogFragmentYesNo.TAG + DIALOG_NSERVICES_DOWNLOAD,
                        getString(R.string.nservices_consultingdownlad).replace(
                                "{0}", lsNServices),
                        EnumLayoutResource.getDefaultIconHelp(this));
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    subIniNServicesDownload(ioDALNservices.fnSelNServicesAPK());
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                            }
                        });
            case CONSVALEDIT:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSVALEDIT"),
                        getString(R.string.actproductopedido_dlgnomodificar),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case CONSLIMITEEXCEDIDO:
                loDialog = DialogFragmentYesNo
                        .newInstance(
                                getFragmentManager(),
                                DialogFragmentYesNo.TAG
                                        .concat("CONSLIMITEEXCEDIDO"),
                                getString(R.string.actproductopedido_dlglimiteexcedido)
                                        + getString(R.string.actproductopedido_cantmaxregistros),
                                R.drawable.boton_informacion, false,
                                EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case CONSNOITEMS:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSNOITEMS"),
                        getString(R.string.actproductopedido_noitems),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case CONSBACK:

                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSBACK"),
                        getString(R.string.actproductopedido_dlgback),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {

                                ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);

                                if (!loBeanPedidoCab.getCodigoPedidoServidor().equals("0") && isStockRestrictivo.equals(Configuracion.FLGVERDADERO)) {
                                    loBeanPedidoCab.setFlgEliminar(Configuracion.FLGVERDADERO);
                                    new HttpGrabaPedido(loBeanPedidoCab, ActProductoPedido.this, fnTipactividad(), getString(R.string.httpeliminar_dlg)).execute();
                                } else {
                                    irclientedetalle();
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });
            case CONSELIMINARDETALLES:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSELIMINARDETALLES"),
                        getString(R.string.actproductopedido_dlgeliminar),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                eliminarDetalles();
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });


            case VALIDAHOME:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDAHOME"),
                        getString(R.string.actproductopedido_dlgback),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {

                                ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);

                                Intent intentl = new Intent(
                                        ActProductoPedido.this, ActMenu.class);
                                intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                startActivity(intentl);
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.actmenu_dlgsincronizar),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {

                                ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);

                                try {
                                    BeanUsuario loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(ActProductoPedido.this);
                                    estadoSincronizar = true;
                                    new HttpSincronizar(loBeanUsuario
                                            .getCodVendedor(),
                                            ActProductoPedido.this,
                                            fnTipactividad()).execute();
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                            }
                        });

            case VALIDASINCRONIZACION:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"),
                        getString(R.string.actmenuvalsincro_titulo),
                        getString(R.string.actmenuvalsincro_mensaje),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case VALIDASINCRO:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                        getString(R.string.msgregpendientesenvinfo),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case ENVIOPENDIENTES:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"),
                        getString(R.string.msgdeseaenviarpendientes),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActProductoPedido.this);
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                            }
                        });

            case CONSSALIR:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSSALIR"),
                        getString(R.string.actproductopedido_dlgsalir),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                salir();
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });
            case CONSCREDITOSUPERADO:
                loDialog = DialogFragmentYesNo
                        .newInstance(
                                getFragmentManager(),
                                DialogFragmentYesNo.TAG
                                        .concat(""),
                                getString(R.string.titlenohatpendiente),
                                getString(R.string.actproductopedido_creditoSuperado),
                                R.drawable.boton_informacion, false,
                                EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG
                                .concat(""));
                eliminarDetalles();

            case CONSDESCARTAR:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDESCARTAR"),
                        getString(R.string.actproductopedido_dlgdescartar),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                actualizarEstadoServidor();
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                            }
                        });

            case CONSBACKPENDIENTES:

                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSBACKPENDIENTES"),
                        getString(R.string.actproductopedido_dlgdescartarpendiente),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);
                                startActivity(new Intent(ActProductoPedido.this, ActListaPedidosPendientes.class));
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            default:
                return super.onCreateNexDialog(id);
        }
    }

    private void actualizarEstadoServidor() {
        estadoActualizar = true;
        BeanEnvPedidoCab pedido = new BeanEnvPedidoCab();
        pedido.setCodigoPedidoServidor(loBeanPedidoCab.getCodigoPedidoServidor());
        pedido.setProcesado(true);
        new HttpActualizarEstadoPedido(pedido, ActProductoPedido.this, fnTipactividad()).execute();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case Constantes.NOSUPERAMONTOMINIMO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_montoMinimoNoSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.SUPERAMONTOMAXIMO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_montoMaximoSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.SUPEROMAXIMOITEMS:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_itemsSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CREDITOSUPERADO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_creditoSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case ERRORPEDIDO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titerror))
                        .setMessage(getString(R.string.actgrabapedido_dlgerrorpedido))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        irNextActivity();
                                    }
                                }).create();
                return loAlertDialog;
            case DETALLE:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage("Producto no sincronizado, no puede ser editado")
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSVALEDIT:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actproductopedido_dlgnomodificar))

                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSLIMITEEXCEDIDO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actproductopedido_dlglimiteexcedido)
                                        + getString(R.string.actproductopedido_cantmaxregistros))

                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case CONSNOITEMS:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.actproductopedido_noitems))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case CONSBACKFORMULARIOINICIO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgbackformularioinicio))
                        .setPositiveButton(getString(R.string.dlg_btneditarformulario),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);

                                        editarFormulario = true;

                                        String mensajeVerificarPedido = UtilitarioData.fnObtenerPreferencesString(ActProductoPedido.this, "MensajeVerificarPedido");

                                        if (mensajeVerificarPedido.equals("")) {
                                            String lsBeanPedidoCab = null;
                                            try {
                                                lsBeanPedidoCab = BeanMapper.toJson(loBeanPedidoCab, false);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            UtilitarioData.fnCrearPreferencesPutString(ActProductoPedido.this, "beanPedidoCab", lsBeanPedidoCab);
                                        }

                                        UtilitarioData.fnCrearPreferencesPutBoolean(ActProductoPedido.this, "editarFormularioInicio", true);
                                        UtilitarioData.fnCrearPreferencesPutString(ActProductoPedido.this, "beanPedidoCabVerificarPedido", "");
                                        UtilitarioData.fnCrearPreferencesPutString(ActProductoPedido.this, "MensajeVerificarPedido", "");
                                        Intent loIntent = new Intent(ActProductoPedido.this, ActPedidoFormulario.class);
                                        loIntent.putExtra(Configuracion.EXTRA_FORMULARIO, Configuracion.FORMULARIO_INICIO);
                                        loIntent.putExtra(Configuracion.EXTRA_FORMULARIO_INICIO_EDITAR, true);
                                        finish();
                                        startActivity(loIntent);

                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btncancelarpedido),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);
                                        if (!loBeanPedidoCab.getCodigoPedidoServidor().equals("0") && isStockRestrictivo.equals(Configuracion.FLGVERDADERO)) {
                                            loBeanPedidoCab.setFlgEliminar(Configuracion.FLGVERDADERO);
                                            new HttpGrabaPedido(loBeanPedidoCab, ActProductoPedido.this, fnTipactividad(), getString(R.string.httpeliminar_dlg)).execute();
                                        } else {
                                            irclientedetalle();
                                        }
                                    }
                                }).create();
                return loAlertDialog;


            case CONSBACK:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);

                                        if (!loBeanPedidoCab.getCodigoPedidoServidor().equals("0") && isStockRestrictivo.equals(Configuracion.FLGVERDADERO)) {
                                            loBeanPedidoCab.setFlgEliminar(Configuracion.FLGVERDADERO);
                                            new HttpGrabaPedido(loBeanPedidoCab, ActProductoPedido.this, fnTipactividad(), getString(R.string.httpeliminar_dlg)).execute();
                                        } else {
                                            irclientedetalle();
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case CONSELIMINARDETALLES:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgeliminar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        eliminarDetalles();
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;


            case VALIDAHOME:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);

                                        Intent intentl = new Intent(
                                                ActProductoPedido.this,
                                                ActMenu.class);
                                        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                        startActivity(intentl);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);

                                        try {
                                            BeanUsuario loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(ActProductoPedido.this);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActProductoPedido.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActProductoPedido.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSSALIR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgsalir))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        salir();
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case CONSDESCARTAR:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgdescartar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        actualizarEstadoServidor();
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSBACKPENDIENTES:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgdescartarpendiente))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);
                                        startActivity(new Intent(ActProductoPedido.this, ActListaPedidosPendientes.class));
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            default:
                return super.onCreateDialog(id);
        }

    }

    public void irclientedetalle() {
        Intent loIntent = null;
        if (loBeanPedidoCab.isEnEdicion()) {
            loIntent = new Intent(ActProductoPedido.this, ActPedidoListaOnline.class);
        } else if (pedidopendiente) {
            loIntent = new Intent(ActProductoPedido.this, ActListaPedidosPendientes.class);
        } else {
            loIntent = new Intent(this, ActClienteDetalle.class);
            loIntent.putExtra("IdCliente", Long.parseLong(loBeanPedidoCab.getClipk()));
        }
        finish();
        startActivity(loIntent);
    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                estadoSalir = 3;
                showDialog(CONSSALIR);
            }
        });
    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        showDialog(VALIDAHOME);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = new DalPedido(this).fnEnvioPedidosPendientes(loBeanUsuario.getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);
                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();
                UtilitarioPedidos.mostrarDialogo(this, CONSDIASINCRONIZAR);
            } else {
                UtilitarioPedidos.mostrarDialogo(this, VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub
        estadoSalir = 1;
        showDialog(CONSSALIR);
    }

    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        estadoSalir = 2;
        showDialog(CONSSALIR);
    }

    private void salir() {

        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);

        if (!loBeanPedidoCab.getCodigoPedidoServidor().equals("0") && isStockRestrictivo.equals(Configuracion.FLGVERDADERO)) {
            loBeanPedidoCab.setFlgEliminar(Configuracion.FLGVERDADERO);
            new HttpGrabaPedido(loBeanPedidoCab, this, fnTipactividad(), getString(R.string.httpeliminar_dlg)).execute();
        }

        DalPedido loDalPedido = null;
        try {
            loDalPedido = new DalPedido(ActProductoPedido.this);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        if (!loBeanPedidoCab.getCodigoPedido().equals("0")) {
            loDalPedido.fnDelTrCabPedidoCodigo(loBeanPedidoCab.getCodigoPedido());
        }

        if (estadoSalir == 2) {
            ((AplicacionEntel) getApplication()).setCurrentCliente(null);
            ((AplicacionEntel) getApplication()).setCurrentCodFlujo(Configuracion.CONSSTOCK);
            Intent loIntento = new Intent(ActProductoPedido.this, ActProductoListaOnline.class);
            loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            BeanExtras loBeanExtras = new BeanExtras();
            loBeanExtras.setFiltro("");
            loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
            String lsFiltro = "";
            try {
                lsFiltro = BeanMapper.toJson(loBeanExtras, false);
            } catch (Exception e) {
                e.printStackTrace();
            }

            loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
            startActivity(loIntento);
        } else if (estadoSalir == 1) {

            loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);
            this.setMsgOk("Eficiencia Online");

            BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
            loBeanListaEficienciaOnline.setIdResultado(0);
            loBeanListaEficienciaOnline.setResultado("");
            loBeanListaEficienciaOnline.setListaEficiencia(null);
            loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

            new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

            Intent loInstent = new Intent(this, ActKpiDetalle.class);
            loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(loInstent);
        } else if (estadoSalir == 3) {
            Intent loIntentLogin = new Intent(ActProductoPedido.this, ActLogin.class);
            loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loIntentLogin);
        }
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(
                    getString(R.string.ml_sincronizarantes)));
        }
    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */
    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

}