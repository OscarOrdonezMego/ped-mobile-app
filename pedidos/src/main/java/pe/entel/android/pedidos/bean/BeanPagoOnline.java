package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

/**
 * Created by tkongr on 15/10/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanPagoOnline {

    private String pag_pk;
    private String fecha;
    private String hora;
    private String cli_codigo;
    private String cli_nombre;
    private String cob_numdocumento;
    private String cob_tipodocumento;
    private String cob_fecvencimiento;
    private String monto_total;
    private String monto_pagado_total;
    private String monto_pagado_fecha;
    private String monto_saldo;
    private String tipo_pago;
    private String monto_totaldolares;
    private String monto_pagado_totaldolares;
    private String monto_pagado_fechadolares;
    private String monto_saldodolares;
    private String serie;
    private String correlativo;
    private String cob_pk;

    public String getPag_pk() {
        return pag_pk;
    }

    public void setPag_pk(String pag_pk) {
        this.pag_pk = pag_pk;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCli_codigo() {
        return cli_codigo;
    }

    public void setCli_codigo(String cli_codigo) {
        this.cli_codigo = cli_codigo;
    }

    public String getCli_nombre() {
        return cli_nombre;
    }

    public void setCli_nombre(String cli_nombre) {
        this.cli_nombre = cli_nombre;
    }

    public String getCob_numdocumento() {
        return cob_numdocumento;
    }

    public void setCob_numdocumento(String cob_numdocumento) {
        this.cob_numdocumento = cob_numdocumento;
    }

    public String getCob_tipodocumento() {
        return cob_tipodocumento;
    }

    public void setCob_tipodocumento(String cob_tipodocumento) {
        this.cob_tipodocumento = cob_tipodocumento;
    }

    public String getCob_fecvencimiento() {
        return cob_fecvencimiento;
    }

    public void setCob_fecvencimiento(String cob_fecvencimiento) {
        this.cob_fecvencimiento = cob_fecvencimiento;
    }

    public String getMonto_total() {
        return monto_total;
    }

    public void setMonto_total(String monto_total) {
        this.monto_total = monto_total;
    }

    public String getMonto_pagado_total() {
        return monto_pagado_total;
    }

    public void setMonto_pagado_total(String monto_pagado_total) {
        this.monto_pagado_total = monto_pagado_total;
    }

    public String getMonto_pagado_fecha() {
        return monto_pagado_fecha;
    }

    public void setMonto_pagado_fecha(String monto_pagado_fecha) {
        this.monto_pagado_fecha = monto_pagado_fecha;
    }

    public String getMonto_saldo() {
        return monto_saldo;
    }

    public void setMonto_saldo(String monto_saldo) {
        this.monto_saldo = monto_saldo;
    }

    public String getTipo_pago() {
        return tipo_pago;
    }

    public void setTipo_pago(String tipo_pago) {
        this.tipo_pago = tipo_pago;
    }

    public String getMonto_totaldolares() {
        return monto_totaldolares;
    }

    public void setMonto_totaldolares(String monto_totaldolares) {
        this.monto_totaldolares = monto_totaldolares;
    }

    public String getMonto_pagado_totaldolares() {
        return monto_pagado_totaldolares;
    }

    public void setMonto_pagado_totaldolares(String monto_pagado_totaldolares) {
        this.monto_pagado_totaldolares = monto_pagado_totaldolares;
    }

    public String getMonto_pagado_fechadolares() {
        return monto_pagado_fechadolares;
    }

    public void setMonto_pagado_fechadolares(String monto_pagado_fechadolares) {
        this.monto_pagado_fechadolares = monto_pagado_fechadolares;
    }

    public String getMonto_saldodolares() {
        return monto_saldodolares;
    }

    public void setMonto_saldodolares(String monto_saldodolares) {
        this.monto_saldodolares = monto_saldodolares;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(String correlativo) {
        this.correlativo = correlativo;
    }

    public String getCobranzaPK() {
        return cob_pk;
    }

    public void setCobranzaPK(String cob_pk) {
        this.cob_pk = cob_pk;
    }

}
