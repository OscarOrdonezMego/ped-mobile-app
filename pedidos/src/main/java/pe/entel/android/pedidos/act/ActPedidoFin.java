package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import greendroid.widget.ActionBarItem;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.util.Gps;
import pe.com.nextel.android.util.Logger;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanEnvPedidoDet;
import pe.entel.android.pedidos.bean.BeanGeneral;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanRespVerificarPedidoGeneral;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.bean.BeanVerificarPedido;
import pe.entel.android.pedidos.dal.DalAlmacen;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalGeneral;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.dal.DalProducto;
import pe.entel.android.pedidos.http.HttpGrabaPedido;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.http.HttpVerificarPedido;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.UtilitarioData;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

public class ActPedidoFin extends NexActivity implements OnClickListener,
        NexMenuCallbacks, MenuSlidingListener {

    private TextView txtcantproductos, txttotalitems, txtmontototal,
            txtmontototalDolar; //@JBELVY
    private EditText edtobservaciones;
    private Button btnenviar, btnverificar;
    BeanUsuario loBeanUsuario = null;
    BeanEnvPedidoCab loBeanPedidoCab = null;

    @SuppressWarnings("unused")
    private LinkedList<BeanGeneral> ioLista;

    @SuppressWarnings("unused")
    private DalGeneral loDalGeneral = new DalGeneral(this);

    private AdapMenuPrincipalSliding _menu;

    boolean estadoSincronizar = false;
    int estadoSalir = 0;

    // Integracion Ntrack
    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 14;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 24;// pueden asignar otro


    //PARAMETROS DE CONFIGURACION
    private String isNumeroDecimales;
    private String isMoneda, isMonedaDolar;
    private String isGps;
    private String isDescontarStock;
    private String isBonificacion;
    private String isMostrarAlmacen;
    private String isPresentacion;
    private String isMostrarFinPedido;

    private int isMaximoItemsPedido;
    private double isMontoMinimoPedido;
    private double isMontoMaximoPedido;

    private String sharePreferenceBeanPedidoCab;
    private String isTitle;

    private String isMaximoNumeroDecimales;
    private int iiMaximoNumeroDecimales;

    private String isVerificarPedido;
    private boolean isHttpVerificarPedido = false;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logger.d("XXX", "ActPedidoFin");
        ValidacionServicioUtil.validarServicio(this);

        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);

        sharePreferenceBeanPedidoCab = "beanPedidoCabVerificarPedido";

        String lsBeanVisitaCab = UtilitarioData.fnObtenerPreferencesString(this, sharePreferenceBeanPedidoCab);

        if (lsBeanVisitaCab.equals("")) {
            if (UtilitarioData.fnObtenerPreferencesBoolean(this, "pedidoPendiente")) {
                sharePreferenceBeanPedidoCab = "beanPedidoCabPendiente";
                lsBeanVisitaCab = UtilitarioData.fnObtenerPreferencesString(this, "beanPedidoCabPendiente");
            } else {
                lsBeanVisitaCab = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("beanPedidoCab", "");
                sharePreferenceBeanPedidoCab = "beanPedidoCab";
            }
        }

        loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper.fromJson(
                lsBeanVisitaCab, BeanEnvPedidoCab.class);

        isTitle = getString(loBeanPedidoCab.getFlgTipo()
                .equals(Configuracion.FLGPEDIDO)
                ? R.string.actpedidofin_bardetalle_pedido
                : R.string.actpedidofin_bardetalle_cotizacion);

        isNumeroDecimales = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(this);
        isMoneda = Configuracion.EnumConfiguracion.SIMBOLO_MONEDA.getValor(this);
        isMonedaDolar = "$. ";
        isGps = Configuracion.EnumConfiguracion.GPS.getValor(this);
        isDescontarStock = Configuracion.EnumConfiguracion.DESCONTAR_STOCK.getValor(this);
        isBonificacion = Configuracion.EnumFuncion.BONIFICACION.getValor(this);
        isMostrarAlmacen = Configuracion.EnumConfiguracion.MOSTRAR_ALMACEN.getValor(this);
        isPresentacion = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(this);
        isMostrarFinPedido = Configuracion.EnumConfiguracion.MOSTRAR_FIN_PEDIDO.getValor(this);
        isMaximoNumeroDecimales = Configuracion.EnumConfiguracion.MAXIMO_NUMERODECIMALES.getValor(this);
        isVerificarPedido = Configuracion.EnumConfiguracion.VERIFICACION_PEDIDO.getValor(this);
        isMaximoItemsPedido = Integer.parseInt(Configuracion.EnumConfiguracion.MAXIMO_ITEMS_PEDIDO.getValor(this));

        obtenerValorMontoPedido();
        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);

        this.setMsgOk(getString(R.string.actmenu_dlghttpmensaje));
        this.setMsgError(getString(R.string.actmenu_dlghttperrormensaje));

        subIniMenu();
    }

    private void obtenerValorMontoPedido() {

        if (Configuracion.EnumConfiguracion.MONTO_MAXIMO_PEDIDO.getValor(this).equals(""))
            isMontoMaximoPedido = 0.0;
        else
            isMontoMaximoPedido = Double.parseDouble(Configuracion.EnumConfiguracion.MONTO_MAXIMO_PEDIDO.getValor(this));

        if (Configuracion.EnumConfiguracion.MONTO_MINIMO_PEDIDO.getValor(this).equals(""))
            isMontoMinimoPedido = 0.0;
        else
            isMontoMinimoPedido = Double.parseDouble(Configuracion.EnumConfiguracion.MONTO_MINIMO_PEDIDO.getValor(this));

    }

    @Override
    public void onBackPressed() {
        ((AplicacionEntel) getApplicationContext()).activarRecalcularMontoPedido(false);
        subGrabarValorActual();
        Intent loIntent = new Intent(this, ActProductoPedido.class);
        startActivity(loIntent);
        finish();
    }

    public void subIniActionBar() {

        this.getActionBarGD().setTitle(
                isTitle);
    }

    @Override
    public void subSetControles() {
        setActionBarContentView(R.layout.pedidofin2);
        super.subSetControles();

        txtcantproductos = (TextView) findViewById(R.id.actpedidofin_txtcantproductos);
        txttotalitems = (TextView) findViewById(R.id.actpedidofin_txttotalitems);
        txtmontototal = (TextView) findViewById(R.id.actpedidofin_txtmontototal);
        txtmontototalDolar = (TextView) findViewById(R.id.actpedidofin_txtmontototalDolar); //@JBELVY
        edtobservaciones = (EditText) findViewById(R.id.actpedidofin_edtobservaciones);
        btnenviar = (Button) findViewById(R.id.actpedidofin_btnenviar);
        btnverificar = (Button) findViewById(R.id.actpedidofin_btnverificar);
        btnenviar.setOnClickListener(this);
        btnverificar.setOnClickListener(this);

        setMaximoNumeroDecimales();
        subCargaDetalle();

        String mensajeHttpVerificar = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE).getString("MensajeVerificarPedido", "");
        subMostrarVerificar(mensajeHttpVerificar.equals("1") ? false : true);
    }

    private void subMostrarVerificar(boolean pbVerificarPedido) {

        if (isVerificarPedido.equals(Configuracion.FLGVERDADERO) && isMostrarFinPedido.equals(Configuracion.FLGFALSO)
                && pbVerificarPedido) {
            btnverificar.setVisibility(View.VISIBLE);
            btnenviar.setVisibility(View.GONE);
        } else {
            btnverificar.setVisibility(View.GONE);
            btnenviar.setVisibility(View.VISIBLE);
        }
    }

    private void setMaximoNumeroDecimales() {
        iiMaximoNumeroDecimales = 0;
        try {
            iiMaximoNumeroDecimales = Integer.parseInt(isMaximoNumeroDecimales);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ActProductoDetalle", "Error conversion maximo numero de decimales: " + e.getMessage());
        }
    }

    private void subCargaDetalle() {

        txtcantproductos.setText(String.valueOf(loBeanPedidoCab
                .getListaDetPedido().size()));
        txttotalitems.setText(Utilitario.fnRound(loBeanPedidoCab.calcularCantTotalItems(), iiMaximoNumeroDecimales));

        String flgVerificacion = getApplicationContext().getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("isHttpVerificarPedido", "");

        if (flgVerificacion.equals("true")) {
            txtmontototal.setText(Utilitario.fnRound(loBeanPedidoCab.calcularMontoSolesCompletoVerificarPedido(), Integer.valueOf(isNumeroDecimales))); //@JBELVY Before calcularMontoCompletoVerificarPedido

            txtmontototalDolar.setText(Utilitario.fnRound(loBeanPedidoCab.calcularMontoDolarCompletoVerificarPedido(), Integer.valueOf(isNumeroDecimales)));
        } else {
            txtmontototal.setText(Utilitario.fnRound(loBeanPedidoCab.calcularMontoSolesCompleto(), //@JBELVY Before calcularMontoCompleto
                    Integer.valueOf(isNumeroDecimales)));

            txtmontototalDolar.setText(Utilitario.fnRound(loBeanPedidoCab.calcularMontoDolarCompleto(),
                    Integer.valueOf(isNumeroDecimales)));
        }

        if (loBeanPedidoCab.getListaDetPedido().size() != 0)
            txtcantproductos.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.icono_productos, 0, 0, 0);
        else
            txtcantproductos.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.icono_ceroproductos, 0, 0, 0);

        edtobservaciones.setText(loBeanPedidoCab.getObservacion());
    }

    private void subGrabarValorActual() {
        Log.e("subGrabarValorActual", "fsdaf");
        // Obtenemos la fecha del sistema
        Calendar c = Calendar.getInstance();
        Date loDate = c.getTime();
        String sFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate)
                .toString();
        loBeanPedidoCab.setObservacion(edtobservaciones.getText().toString().trim());
        loBeanPedidoCab.ObtenerIdTipoCambioSoles(this);
        loBeanPedidoCab.ObtenerIdTipoCambioDolares(this);

        String flgVerificacion = getApplicationContext().getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("isHttpVerificarPedido", "");

        if (flgVerificacion.equals("true")) {
            loBeanPedidoCab.setMontoTotalSoles(Utilitario.fnRound(  //@JBELVY Before setMontoTotal
                    loBeanPedidoCab.calcularMontoTotalVerificarPedidoSoles(), //@JBELVY Before calcularMontoTotalVerificarPedido()
                    Integer.valueOf(isNumeroDecimales)));

            loBeanPedidoCab.setMontoTotalDolar(Utilitario.fnRound(
                    loBeanPedidoCab.calcularMontoTotalVerificarPedidoDolar(),
                    Integer.valueOf(isNumeroDecimales))); //@JBELVY
        } else {
            loBeanPedidoCab.setMontoTotalSoles(Utilitario.fnRound(  //@JBELVY Before setMontoTotal
                    loBeanPedidoCab.calcularMontoTotalSoles(), //@JBELVY Before calcularMontoTotal()
                    Integer.valueOf(isNumeroDecimales)));

            loBeanPedidoCab.setMontoTotalDolar(Utilitario.fnRound(
                    loBeanPedidoCab.calcularMontoTotalDolar(),
                    Integer.valueOf(isNumeroDecimales))); //@JBELVY
        }
        loBeanPedidoCab.setCantidadTotal(Utilitario.fnRound(loBeanPedidoCab
                .calcularCantTotalItems(), iiMaximoNumeroDecimales));
        loBeanPedidoCab.setBonificacionTotal(Utilitario.fnRound(loBeanPedidoCab
                .calcularBonificacionTotal(), iiMaximoNumeroDecimales));
        loBeanPedidoCab.setFleteTotal(loBeanPedidoCab
                .calcularTotalFlete());
        loBeanPedidoCab.setFechaFin(sFecha);

        String limiteCredito = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE).getString("LimiteCredito", "");

        if (limiteCredito.equals(Configuracion.FLGVERDADERO)) {
            BeanCliente beanCliente = ((AplicacionEntel) getApplication()).getCurrentCliente();
            Double saldo = Double.parseDouble(beanCliente.getSaldoCredito());
            Double utilizado = Double.parseDouble(loBeanPedidoCab.getMontoTotal());

            beanCliente.setSaldoCredito(String.valueOf(saldo - utilizado));
            loBeanPedidoCab.setCreditoUtilizado(String.valueOf(saldo - utilizado));
            ((AplicacionEntel) getApplication()).setCurrentCliente(beanCliente);
        }

        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);

        // Se guarda GPS si tiene el flag habilitado
        Location location = null;

        try {
            location = Gps.getLocation();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (isGps
                .equals(Configuracion.FLGVERDADERO)
                && location != null) {
            loBeanPedidoCab.setLatitud(String.valueOf(location.getLatitude()));
            loBeanPedidoCab
                    .setLongitud(String.valueOf(location.getLongitude()));
            loBeanPedidoCab.setCelda(location.getProvider().substring(0, 1)
                    .toUpperCase());
        } else {
            loBeanPedidoCab.setLatitud("0");
            loBeanPedidoCab.setLongitud("0");
            loBeanPedidoCab.setCelda("-");
        }

        loBeanPedidoCab.setFlgEnCobertura(Utilitario.fnVerSignal(this) ? "1" : "0");
        loBeanPedidoCab.setFlgTerminado(Configuracion.FLGVERDADERO);

        String lsBeanVisitaCab = "";
        try {
            lsBeanVisitaCab = BeanMapper.toJson(loBeanPedidoCab, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        UtilitarioData.fnCrearPreferencesPutString(this, sharePreferenceBeanPedidoCab, lsBeanVisitaCab);

    }

    private void subGrabar() throws Exception {
        subGrabarValorActual();
        // Grabamos en la BD del equipo
        DalPedido loDalPedido = new DalPedido(this);

        if (loBeanPedidoCab.isEnEdicion()) {
            if (!loBeanPedidoCab.getCodigoPedidoServidor().equals("") && !loBeanPedidoCab.getCodigoPedidoServidor().equals("0"))
                loDalPedido.fnDeletePedidoXcodigoServidor(loBeanPedidoCab.getCodigoPedidoServidor());
            String codigoPedido = loDalPedido.fnGrabarPedido(loBeanPedidoCab);
            loBeanPedidoCab.setCodigoPedido(codigoPedido);
        } else {
            if (!loBeanPedidoCab.getCodigoPedido().equals("0") && !loBeanPedidoCab.getCodigoPedido().equals(""))
                loDalPedido.fnDeletePedido(loBeanPedidoCab.getCodigoPedido());
            String codPed = loDalPedido.fnGrabarPedido(loBeanPedidoCab);
            loBeanPedidoCab.setCodigoPedido(codPed);
        }

        BeanCliente loBeanCliente = ((AplicacionEntel) getApplication())
                .getCurrentCliente();

        // Actualizo el estado del currentcliente
        loBeanCliente.setEstado(Configuracion.ESTADO03CONPEDIDO);
        ((AplicacionEntel) getApplication()).setCurrentCliente(loBeanCliente);

        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);//pedido grabado en memoria
    }

    private void eliminarPedido() throws Exception {
        new DalPedido(this).fnDeletePedido(loBeanPedidoCab.getCodigoPedido());
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        Log.d("123", "ID : " + item.getItemId());
        switch (item.getItemId()) {
            case Constantes.CONSGRABAR:

                if (loBeanPedidoCab.getListaDetPedido().size() <= 0) {
                    showDialog(Constantes.CONSVALIDAR);
                } else {
                    showDialog(Constantes.CONSGRABAR);
                }
                break;

            default:
                return super.onHandleActionBarItemClick(item, position);
        }
        return true;
    }

    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog;
        switch (id) {
            case Constantes.NOSUPERAMONTOMINIMO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_montoMinimoNoSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.SUPERAMONTOMAXIMO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_montoMaximoSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.SUPEROMAXIMOITEMS:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_itemsSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.CONSBACKFORMULARIOINICIO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgbackformularioinicio))
                        .setPositiveButton(getString(R.string.dlg_btneditarformulario),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        String mensajeVerificarPedido = UtilitarioData.fnObtenerPreferencesString(ActPedidoFin.this, "MensajeVerificarPedido");

                                        if (mensajeVerificarPedido.equals("")) {
                                            String lsBeanPedidoCab = null;
                                            try {
                                                lsBeanPedidoCab = BeanMapper.toJson(loBeanPedidoCab, false);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            UtilitarioData.fnCrearPreferencesPutString(ActPedidoFin.this, "beanPedidoCab", lsBeanPedidoCab);
                                        }

                                        UtilitarioData.fnCrearPreferencesPutBoolean(ActPedidoFin.this, "editarFormularioInicio", true);
                                        UtilitarioData.fnCrearPreferencesPutString(ActPedidoFin.this, "beanPedidoCabVerificarPedido", "");
                                        UtilitarioData.fnCrearPreferencesPutString(ActPedidoFin.this, "MensajeVerificarPedido", "");

                                        Intent loIntent = new Intent(ActPedidoFin.this, ActPedidoFormulario.class);
                                        loIntent.putExtra(Configuracion.EXTRA_FORMULARIO, Configuracion.FORMULARIO_INICIO);
                                        loIntent.putExtra(Configuracion.EXTRA_FORMULARIO_INICIO_EDITAR, true);
                                        finish();
                                        startActivity(loIntent);

                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btncancelarpedido),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        startActivity(new Intent(ActPedidoFin.this, ActListaPedidosPendientes.class));
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.CREDITOSUPERADO:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_creditoSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(ActPedidoFin.this, ActProductoPedido.class));
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        startActivity(new Intent(ActPedidoFin.this, ActClienteDetalle.class));
                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.ERRORPEDIDO:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.dlg_titerror))
                        .setMessage(getString(R.string.actgrabapedido_dlgerrorpedido))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        Intent intentl = new Intent(ActPedidoFin.this, ActClienteDetalle.class);
                                        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        if (loBeanPedidoCab.isEnEdicion())
                                            intentl = new Intent(ActPedidoFin.this, ActPedidoListaOnline.class);
                                        startActivity(intentl);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.CONSGRABAR:
                Calendar c = Calendar.getInstance();
                Date loDate = c.getTime();
                final String sFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss",
                        loDate).toString();
                String lsBeanVisitaCab = UtilitarioData.fnObtenerPreferencesString(this, sharePreferenceBeanPedidoCab);
                loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper.fromJson(lsBeanVisitaCab, BeanEnvPedidoCab.class);

                BeanCliente lobecli = ((AplicacionEntel) getApplication()).getCurrentCliente();
                String flgVerificacion = getApplicationContext().getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("isHttpVerificarPedido", "");
                String montototal = "";
                String montototalSoles = ""; //@JBELVY
                String montototalDolar = ""; //@JBELVY

                if (flgVerificacion.equals("true")) {
                    montototal = loBeanPedidoCab.calcularMontoTotalVerificarPedido();
                    montototalSoles = loBeanPedidoCab.calcularMontoTotalVerificarPedidoSoles(); //@JBELVY
                    montototalDolar = loBeanPedidoCab.calcularMontoTotalVerificarPedidoDolar(); //@JBELVY
                } else {
                    montototal = loBeanPedidoCab.calcularMontoTotal();
                    montototalSoles = loBeanPedidoCab.calcularMontoTotalSoles(); //@JBELVY
                    montototalDolar = loBeanPedidoCab.calcularMontoTotalDolar(); //@JBELVY
                }

                StringBuffer loStb = new StringBuffer();
                loStb.append(getString(R.string.actpedidofin_dlgfin)).append('\n');
                loStb.append(" C\u00F3digo : ").append(lobecli.getClicod()).append('\n');
                loStb.append(" Nombre : ").append(lobecli.getClinom()).append('\n');
                loStb.append(" Monto S/: ")
                        .append(" ").append(Utilitario.fnRound(montototalSoles, Integer.valueOf(isNumeroDecimales)))
                        .append('\n');
                loStb.append(" Monto $: ")
                        .append(" ").append(Utilitario.fnRound(montototalDolar, Integer.valueOf(isNumeroDecimales)))
                        .append('\n');
                loStb.append(" Items Pedido : ").append(loBeanPedidoCab.getListaDetPedido().size()).append('\n');

                if (isBonificacion
                        .equals(Configuracion.FLGVERDADERO)) {
                    loStb.append(" Items Bonificados : ")
                            .append(loBeanPedidoCab.calcularCantTotalItemsBonificados())
                            .append('\n');
                }

                loStb.append(sFecha).append('\n');

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(isTitle)
                        .setMessage(loStb.toString())
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            Log.e("CREATE DIALOG", "");
                                            subGrabar();
                                            validarStock();
                                            enviarPedidoServer();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btncancelar),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        return;
                                    }
                                })
                        .create();
                return loAlertDialog;

            case Constantes.CONSVALIDAR:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(isTitle)
                        .setMessage(getString(R.string.actpedidofin_msnvalidacion))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.VALIDAHOME:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        Intent intentl = new Intent(
                                                ActPedidoFin.this, ActMenu.class);
                                        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                        startActivity(intentl);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences(
                                                    "Actual", MODE_PRIVATE)
                                                    .getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                                    .fromJson(lsBeanUsuario,
                                                            BeanUsuario.class);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActPedidoFin.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActPedidoFin.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.CONSSALIR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgsalir))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        salir();
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.CONSDIALOGVERIFICARPEDIDONOCOBERTURA:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgbackformularioinicio))
                        .setPositiveButton(getString(R.string.actpedidofin_dlgbtnguardarpedido),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            subGrabar();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                        if (loBeanPedidoCab.isEnEdicion()) {
                                            startActivity(new Intent(ActPedidoFin.this, ActPedidoListaOnline.class));
                                        } else {
                                            Intent loIntent = new Intent(ActPedidoFin.this, ActClienteBuscar.class);
                                            finish();
                                            startActivity(loIntent);
                                        }

                                    }
                                })
                        .setNegativeButton(getString(R.string.actpedidofin_dlgbtncancelarpedido),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if (loBeanPedidoCab.isEnEdicion()) {
                                            startActivity(new Intent(ActPedidoFin.this, ActPedidoListaOnline.class));
                                        } else {
                                            Intent loIntent = new Intent(ActPedidoFin.this, ActClienteBuscar.class);
                                            finish();
                                            startActivity(loIntent);
                                        }
                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.CONSERRORACTUALIZAR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.actgrabapedido_dlgerroractualizar))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        finish();
                                        startActivity(new Intent(ActPedidoFin.this, ActPedidoListaOnline.class));
                                    }
                                }).create();
                return loAlertDialog;

            default:
                return super.onCreateDialog(id);
        }

    }

    private void validarStock() {
        if (!UtilitarioData.fnObtenerPreferencesBoolean(ActPedidoFin.this, "pedidoPendiente")
                && !loBeanPedidoCab.isEnEdicion()) {
            if (isDescontarStock.equals(Configuracion.FLGVERDADERO))
                actualizarStock();
        }
    }

    @SuppressWarnings("unused")
    private void direccionarFlujo() {
        new NexToast(this, R.string.actpedidofin_msnpedidograbado,
                EnumType.ACCEPT).show();
        Intent intentF = new Intent(this, ActClienteDetalle.class);
        intentF.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intentF);
    }

    public void actualizarStock() {

        DalProducto loDalProducto = new DalProducto(this);
        DalAlmacen loDalAlmacen = new DalAlmacen(this);
        for (BeanEnvPedidoDet loBeanEnvPedidoDet : loBeanPedidoCab.getListaDetPedido()) {

            double ldStockFinal = 0;
            double ldStock = 0;
            double ldCantidad = 0;
            double ldBonificacion = 0;
            double ldFrac = 0;
            double ldBonificacionFrac = 0;

            ldStock = Double.parseDouble(Utilitario.fnRound(
                    loBeanEnvPedidoDet.getStock(), iiMaximoNumeroDecimales));

            if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                ldCantidad = Double.parseDouble(Utilitario.fnRound(loBeanEnvPedidoDet.getCantidad(), iiMaximoNumeroDecimales));
                ldCantidad *= Double.parseDouble(Utilitario.fnRound(loBeanEnvPedidoDet.getCantidadPre(), iiMaximoNumeroDecimales));
            } else {
                ldCantidad = Double.parseDouble(Utilitario.fnRound(loBeanEnvPedidoDet.getCantidad(), iiMaximoNumeroDecimales));
            }

            try {
                ldBonificacion = Double.parseDouble(Utilitario.fnRound(loBeanEnvPedidoDet.getBonificacion(), iiMaximoNumeroDecimales));
                if (isPresentacion.equals(Configuracion.FLGVERDADERO))
                    ldBonificacion *= Double.parseDouble(Utilitario.fnRound(loBeanEnvPedidoDet.getCantidadPre(), iiMaximoNumeroDecimales));
            } catch (Exception e) {
                ldBonificacion = 0;
            }

            try {
                ldFrac = Double.parseDouble(Utilitario.fnRound(loBeanEnvPedidoDet.getCantidadFrac(), iiMaximoNumeroDecimales));
            } catch (Exception e) {
                ldFrac = 0;
            }

            try {
                ldBonificacionFrac = Double.parseDouble(Utilitario.fnRound(loBeanEnvPedidoDet.getBonificacionFrac(), iiMaximoNumeroDecimales));
            } catch (Exception e) {
                ldBonificacionFrac = 0;
            }

            double stockFinal = ldStock - ldCantidad - ldBonificacion - ldFrac - ldBonificacionFrac;

            String psStockFinal = String.valueOf(Utilitario.fnRound(stockFinal, iiMaximoNumeroDecimales));
            //actualizar stock
            if ((isMostrarAlmacen.equals(Configuracion.FLGVERDADERO))) {
                if ((isPresentacion.equals(Configuracion.FLGVERDADERO))) {
                    loDalAlmacen.fnUpdPreStock(loBeanEnvPedidoDet.getCodAlmacen(),
                            loBeanEnvPedidoDet.getIdArticulo(), psStockFinal);
                } else {
                    loDalAlmacen.fnUpdStock(loBeanEnvPedidoDet.getCodAlmacen(),
                            loBeanEnvPedidoDet.getIdArticulo(), psStockFinal);
                }

            } else {
                if ((isPresentacion.equals(Configuracion.FLGVERDADERO))) {
                    loDalProducto.fnUpdStockPresentacion(loBeanEnvPedidoDet.getIdArticulo(),
                            psStockFinal);
                } else {
                    loDalProducto.fnUpdStockProducto(loBeanEnvPedidoDet.getIdArticulo(),
                            psStockFinal);
                }
            }
        }
    }

    public void enviarPedidoServer() {
        ((AplicacionEntel) getApplication()).uploadEvent(Configuracion.PEDIDO_EVENT);
        new HttpGrabaPedido(loBeanPedidoCab, this, fnTipactividad()).execute();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {

        if (isHttpVerificarPedido) {

            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                try {
                    BeanRespVerificarPedidoGeneral general = ((AplicacionEntel) getApplication())
                            .getCurrentRespVerificarPedidoGeneral();

                    if (general.getMensajes().get(0).getCodigo().equals("-1")) {
                        subMostrarVerificar(true);
                        if (UtilitarioData.fnObtenerPreferencesBoolean(ActPedidoFin.this, "pedidoPendiente"))
                            showDialog(Constantes.CONSBACKFORMULARIOINICIO);

                    } else {
                        subMostrarVerificar(false);

                        if (general.getMensajes().get(0).getCodigo().equals("0")) {
                            loBeanPedidoCab.setListaDetPedido(general.mapperListaVerificarPedidoABeanEnvPedidoDetalle());
                            UtilitarioData.fnCrearPreferencesPutString(ActPedidoFin.this, "beanPedidoCabVerificarPedido", BeanMapper.toJson(loBeanPedidoCab, false));
                            UtilitarioData.fnCrearPreferencesPutString(ActPedidoFin.this, "MensajeVerificarPedido", "1");
                            subCargaDetalle();
                            startActivity(new Intent(this, ActProductoPedido.class));
                        } else if (general.getMensajes().get(0).getCodigo().equals("1")) {
                            isHttpVerificarPedido = false;
                            new DalPedido(this).fnUpdEstadoPedido(loBeanUsuario.getCodVendedor());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDORERROR &&
                    (getHttpResultado().equals(this.getString(R.string.msg_fueracobertura)) ||
                            (getHttpResultado().equals(this.getString(R.string.msg_modofueracobertura))))) {
                if (loBeanPedidoCab.isEnEdicion()) {//no se generan pendientes mediante edicion
                    DalPedido loDalPedido = null;
                    try {
                        loDalPedido = new DalPedido(this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    loDalPedido.fnUpdEstadoPedido(loBeanPedidoCab.getCodigoPedido());
                    showDialog(Constantes.CONSERRORACTUALIZAR);
                } else
                    showDialog(Constantes.CONSDIALOGVERIFICARPEDIDONOCOBERTURA);
            }
        } else {
            try {
                Intent intentl = new Intent(this, ActClienteDetalle.class);
                intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                if (estadoSincronizar) {
                    if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                        Intent loIntent = new Intent(this, ActMenu.class);

                        loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(loIntent);
                    }
                } else if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                    // como grabo bien en el servidor, hago un update al registro
                    DalPedido loDalPedido = new DalPedido(this);
                    DalCliente loDalCliente = new DalCliente(this);
                    BeanUsuario loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);

                    boolean lbResult;
                    if (UtilitarioData.fnObtenerPreferencesBoolean(ActPedidoFin.this, "pedidoPendiente")) {
                        lbResult = loDalPedido.fnUpdEstadoPedidoPendiente(loBeanPedidoCab.getCodigoPedido(), loBeanUsuario.getCodVendedor());
                    } else {
                        lbResult = loDalPedido.fnUpdEstadoPedido(loBeanPedidoCab.getCodigoPedido());
                    }
                    boolean lbResultUpdCli = loDalCliente.fnUpdFecUltPed(
                            loBeanPedidoCab.getFechaFin(),
                            loBeanPedidoCab.getCodigoCliente(),
                            loBeanUsuario.getNombre());

                    if (lbResult && lbResultUpdCli) {
                        BeanCliente lobecli = ((AplicacionEntel) getApplication())
                                .getCurrentCliente();
                        lobecli.setCodUsrUltPedido(loBeanUsuario.getNombre());
                        lobecli.setFecUltPedido(loBeanPedidoCab.getFechaFin());
                        ((AplicacionEntel) getApplication())
                                .setCurrentCliente(lobecli);
                        UtilitarioData.fnCrearPreferencesPutBoolean(this, "editarFormularioInicio", false);
                        if (UtilitarioData.fnObtenerPreferencesBoolean(this, "pedidoPendiente")) {
                            UtilitarioData.fnCrearPreferencesPutBoolean(this, "pedidoPendiente", false);
                            UtilitarioData.fnCrearPreferencesPutString(this, "beanPedidoCabPendiente", "");
                            startActivity(new Intent(this, ActListaPendientes.class));

                        } else {
                            if (loBeanPedidoCab.isEnEdicion()) {
                                startActivity(new Intent(this, ActProductoListaOnline.class).
                                        setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                            } else startActivity(intentl);
                        }
                    } else {
                        if (loBeanPedidoCab.isEnEdicion()) {//no se generan pendientes mediante edicion
                            loDalPedido.fnUpdEstadoPedido(loBeanPedidoCab.getCodigoPedido());
                            showDialog(Constantes.CONSERRORACTUALIZAR);
                        } else
                            showDialog(Constantes.ERRORPEDIDO);
                    }

                } else {
                    if (loBeanPedidoCab.isEnEdicion()) {//no se generan pendientes mediante edicion
                        DalPedido loDalPedido = new DalPedido(this);
                        loDalPedido.fnUpdEstadoPedido(loBeanPedidoCab.getCodigoPedido());
                        showDialog(Constantes.CONSERRORACTUALIZAR);
                    } else
                        showDialog(Constantes.ERRORPEDIDO);
                }
            } catch (Exception e) {
                subShowException(e);
            }
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onClick(View v) {
        if (btnenviar.getId() == v.getId()) {
            if (isMontoMinimoPedido > 0.0 && Double.parseDouble(loBeanPedidoCab.getMontoTotal()) < isMontoMinimoPedido) {
                UtilitarioPedidos.mostrarDialogo(this, Constantes.NOSUPERAMONTOMINIMO);

            } else if (isMontoMaximoPedido > 0.0 && Double.parseDouble(loBeanPedidoCab.getMontoTotal()) > isMontoMaximoPedido) {
                UtilitarioPedidos.mostrarDialogo(this, Constantes.SUPERAMONTOMAXIMO);

            } else if (isMaximoItemsPedido > 0 && loBeanPedidoCab.getListaDetPedido().size() > isMaximoItemsPedido) {
                UtilitarioPedidos.mostrarDialogo(this, Constantes.SUPEROMAXIMOITEMS);

            } else if (isMostrarFinPedido.equals(Configuracion.FLGVERDADERO)) {

                loBeanPedidoCab.setObservacion(edtobservaciones.getText().toString().trim());
                String lsBeanVisitaCab = "";
                try {
                    lsBeanVisitaCab = BeanMapper.toJson(loBeanPedidoCab, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                UtilitarioData.fnCrearPreferencesPutString(this, sharePreferenceBeanPedidoCab, lsBeanVisitaCab);

                Intent loIntent = new Intent(ActPedidoFin.this, ActPedidoFormulario.class);
                loIntent.putExtra(Configuracion.EXTRA_FORMULARIO, Configuracion.FORMULARIO_FIN);

                finish();
                startActivity(loIntent);

            } else if (loBeanPedidoCab.getListaDetPedido().size() <= 0) {
                showDialog(Constantes.CONSVALIDAR);
            } else {
                UtilitarioPedidos.mostrarDialogo(this, Constantes.CONSGRABAR);
            }

        } else if (v == btnverificar) {
            loBeanPedidoCab.setObservacion(edtobservaciones.getText().toString().trim());
            BeanEnvPedidoCab pedidoAux = loBeanPedidoCab;
            pedidoAux.setMontoTotal(Utilitario.fnRound(loBeanPedidoCab.calcularMontoTotal(), Integer.valueOf(isNumeroDecimales)));
            pedidoAux.setListaCtrlPedido(loBeanPedidoCab.getListaCtrlPedido());

            isHttpVerificarPedido = true;
            UtilitarioData.fnCrearPreferencesPutString(this, "isHttpVerificarPedido", "true");

            try {
                UtilitarioData.fnCrearPreferencesPutString(this, "beanPedidoCab", BeanMapper.toJson(pedidoAux, false));
            } catch (Exception e) {
                e.printStackTrace();
            }

            new HttpVerificarPedido(BeanVerificarPedido.mapperVerificarPedido(pedidoAux), this, this.fnTipactividad()).execute();
        }
    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                estadoSalir = 3;
                UtilitarioPedidos.mostrarDialogo(ActPedidoFin.this, Constantes.CONSSALIR);
            }
        });
    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        UtilitarioPedidos.mostrarDialogo(this, Constantes.VALIDAHOME);
    }

    @Override
    public void subInicioFromSliding() {


    }

    @SuppressWarnings("deprecation")
    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = new DalPedido(this).fnEnvioPedidosPendientes(loBeanUsuario.getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);

                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();
                UtilitarioPedidos.mostrarDialogo(this, Constantes.CONSDIASINCRONIZAR);
            } else {
                UtilitarioPedidos.mostrarDialogo(this, Constantes.VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subEficienciaFromSliding() {
        estadoSalir = 1;
        UtilitarioPedidos.mostrarDialogo(this, Constantes.CONSSALIR);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subStockFromSliding() {
        estadoSalir = 2;
        UtilitarioPedidos.mostrarDialogo(this, Constantes.CONSSALIR);
    }

    private void salir() {

        if (estadoSalir == 2) {
            ((AplicacionEntel) getApplication()).setCurrentCliente(null);
            ((AplicacionEntel) getApplication())
                    .setCurrentCodFlujo(Configuracion.CONSSTOCK);
            ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
            Intent loIntento = new Intent(ActPedidoFin.this,
                    ActProductoListaOnline.class);
            loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            BeanExtras loBeanExtras = new BeanExtras();
            loBeanExtras.setFiltro("");
            loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
            String lsFiltro = "";
            try {
                lsFiltro = BeanMapper.toJson(loBeanExtras, false);
            } catch (Exception e) {
                e.printStackTrace();
            }

            loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
            startActivity(loIntento);
        } else if (estadoSalir == 1) {

            loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);
            this.setMsgOk("Eficiencia Online");

            BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
            loBeanListaEficienciaOnline.setIdResultado(0);
            loBeanListaEficienciaOnline.setResultado("");
            loBeanListaEficienciaOnline.setListaEficiencia(null);
            loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

            new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

            Intent loInstent = new Intent(this, ActKpiDetalle.class);
            loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(loInstent);
        } else if (estadoSalir == 3) {
            Intent loIntentLogin = new Intent(ActPedidoFin.this,
                    ActLogin.class);
            loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loIntentLogin);
        }
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(
                    getString(R.string.ml_sincronizarantes)));
        }

    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */
    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */
    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        try {
            DialogFragmentYesNo loDialog;
            switch (id) {
                case DIALOG_NSERVICES_DOWNLOAD:
                    String lsNServices;
                    if (ioDALNservices == null)
                        ioDALNservices = DALNServices.getInstance(this,
                                R.string.db_name);
                    try {
                        lsNServices = ioDALNservices.fnSelNServicesLabel();
                    } catch (Exception e) {
                        Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                e);
                        lsNServices = getString(R.string.nservices_label_default);
                    }
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG
                                    + DIALOG_NSERVICES_DOWNLOAD,
                            getString(R.string.nservices_consultingdownlad)
                                    .replace("{0}", lsNServices),
                            EnumLayoutResource.getDefaultIconHelp(this));
                    return new DialogFragmentYesNoPairListener(loDialog,
                            new OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(
                                        DialogFragmentYesNo dialog) {
                                    try {
                                        subIniNServicesDownload(ioDALNservices
                                                .fnSelNServicesAPK());
                                    } catch (Exception e) {
                                        subShowException(e);
                                    }
                                }

                                @Override
                                public void performButtonNo(
                                        DialogFragmentYesNo dialog) {
                                }
                            });

                default:
                    return super.onCreateNexDialog(id);
            }
        } catch (Exception e) {
            subShowException(e);
            return super.onCreateNexDialog(id);
        }
    }

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

}