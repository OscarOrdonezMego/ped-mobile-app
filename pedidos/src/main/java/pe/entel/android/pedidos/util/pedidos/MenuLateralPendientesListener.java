package pe.entel.android.pedidos.util.pedidos;

import android.content.Context;
import android.content.Intent;

import java.util.List;

import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.act.ActKpiDetalle;
import pe.entel.android.pedidos.act.ActMenu;
import pe.entel.android.pedidos.act.ActProductoListaOnline;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.util.Configuracion;

/**
 * Created by tkongr on 21/03/2016.
 */
public class MenuLateralPendientesListener implements AdapMenuPrincipalSliding.MenuSlidingListener {

    Context context;
    NexActivity activity;
    BeanUsuario beanUsuario;
    DALNServices dalnServices;

    public MenuLateralPendientesListener(Context context, BeanUsuario beanUsuario, DALNServices dalnServices) {
        this.context = context;
        activity = (NexActivity) context;
        this.beanUsuario = beanUsuario;
        this.dalnServices = dalnServices;
    }

    @Override
    public void subHomeFromSliding() {
        Intent intentl = new Intent(context, ActMenu.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        activity.startActivity(intentl);
    }

    @Override
    public void subInicioFromSliding() {

    }

    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = new DalPedido(context).fnEnvioPedidosPendientes(beanUsuario.getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(context);

                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();
                UtilitarioPedidos.mostrarDialogo(context, Constantes.CONSDIASINCRONIZAR);

            } else {
                UtilitarioPedidos.mostrarDialogo(context, Constantes.VALIDASINCRO);
            }
        } catch (Exception e) {
            activity.subShowException(e);
        }
    }

    @Override
    public void subPendientesFromSliding() {
    }

    @Override
    public void subEficienciaFromSliding() {
        beanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(context);
        activity.setMsgOk("Eficiencia Online");

        BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
        loBeanListaEficienciaOnline.setIdResultado(0);
        loBeanListaEficienciaOnline.setResultado("");
        loBeanListaEficienciaOnline.setListaEficiencia(null);
        loBeanListaEficienciaOnline.setUsuario(beanUsuario.getCodVendedor());

        new HttpObtenerEficiencia(loBeanListaEficienciaOnline, context, activity.fnTipactividad()).execute();

        Intent loInstent = new Intent(context, ActKpiDetalle.class);
        loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(loInstent);
    }

    @Override
    public void subStockFromSliding() {
        ((AplicacionEntel) activity.getApplication()).setCurrentCliente(null);
        ((AplicacionEntel) activity.getApplication()).setCurrentCodFlujo(Configuracion.CONSSTOCK);
        ((AplicacionEntel) activity.getApplication()).setCurrentlistaArticulo(null);

        Intent loIntento = new Intent(activity, ActProductoListaOnline.class);
        loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        BeanExtras loBeanExtras = new BeanExtras();
        loBeanExtras.setFiltro("");
        loBeanExtras.setTipo(ConfiguracionNextel.EnumTipBusqueda.NOMBRE);
        String lsFiltro = "";
        try {
            lsFiltro = BeanMapper.toJson(loBeanExtras, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
        activity.startActivity(loIntento);
    }

    @Override
    public void subNServicesFromSliding() {
        try {
            UtilitarioPedidos.subCallNServices(context, dalnServices, false, beanUsuario.getFlgNServicesObligatorio().equals(Configuracion.FLGVERDADERO));
        } catch (Exception e) {
            activity.subShowException(new Exception(activity.getString(R.string.ml_sincronizarantes)));
        }
    }
}
