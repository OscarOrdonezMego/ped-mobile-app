package pe.entel.android.pedidos.act;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.Logger;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanConsulta;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;

public class ActDetalleOnline extends NexActivity {
    private TextView txtNroPedido, txtNroNoPedido, txtMntAcumulado, txtVisita,
            txtEVA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logger.d("XXX", "ActDetalleOnline");
        ValidacionServicioUtil.validarServicio(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        Intent loIntent = new Intent(this, ActMenu.class);
        finish();
        startActivity(loIntent);

        return;
    }

    public void subIniActionBar() {
        this.setTitle(getString(R.string.actpedidorealizado_bardetalle));
    }

    @Override
    public void subSetControles() {
        setActionBarContentView(R.layout.pedidorealizado);
        super.subSetControles();
        txtNroPedido = (TextView) findViewById(R.id.actpedidorealizado_txtNroPedido);
        txtNroNoPedido = (TextView) findViewById(R.id.actpedidorealizado_txtNroNoPedido);
        txtMntAcumulado = (TextView) findViewById(R.id.actpedidorealizado_txtMntAcumulado);
        txtVisita = (TextView) findViewById(R.id.actpedidorealizado_txtVisEfect);
        txtEVA = (TextView) findViewById(R.id.actpedidorealizado_txteva);

        subCargaDetalle();
    }

    private void subCargaDetalle() {
        String lsBeanConsulta = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("BeanConsulta", "");
        BeanConsulta loBeanConsulta = (BeanConsulta) BeanMapper.fromJson(lsBeanConsulta, BeanConsulta.class);

        long cantPedidos = loBeanConsulta.getCantPedidos();
        long cantVisitasEfectivas = loBeanConsulta.getCantVisitasEfectivas();
        double eva = 0;

        if (cantVisitasEfectivas != 0)
            eva = cantPedidos / cantVisitasEfectivas * 100;

        txtNroPedido.setText(String.valueOf(cantPedidos));
        txtNroNoPedido.setText(String.valueOf(loBeanConsulta.getCantNoPedidos()));

        String strMntAcumuladoFormat;

        try {
            double dblMntAcumuladoFormat = Double.parseDouble(loBeanConsulta.getMntAcumulado());
            strMntAcumuladoFormat = String.valueOf(Utilitario.fnRound(dblMntAcumuladoFormat, Configuracion.MAXDECIMALMONTO));
            ;
        } catch (Exception e) {
            strMntAcumuladoFormat = loBeanConsulta.getMntAcumulado();
        }

        txtMntAcumulado.setText(getString(R.string.moneda) + " " + strMntAcumuladoFormat);
        txtVisita.setText(String.valueOf(cantVisitasEfectivas));
        txtEVA.setText(String.valueOf(Utilitario.fnRound(eva, Configuracion.MAXDECIMALPORC)));
    }
}
