package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanRespConsultaPedido {
    @JsonProperty
    private List<String> ListaCabecera;
    @JsonProperty
    private List<Lista> ListaDetalle;
    //private List<List<String>> ListaDetalle;

    public List<String> getListaCabecera() {
        return ListaCabecera;
    }

    public void setListaCabecera(List<String> listaCabecera) {
        ListaCabecera = listaCabecera;
    }

    public List<List<String>> getListaDetalle() {
        List<List<String>> _ListaDetalle = new ArrayList<List<String>>();
        for (int i = 0; i < ListaDetalle.size(); i++) {
            _ListaDetalle.add(ListaDetalle.get(i).getListaFila());
        }
        return _ListaDetalle;
    }

    public void setListaDetalle(List<List<String>> listaDetalle) {
        List<Lista> _lista = new ArrayList<Lista>();
        Lista fila;
        for (int i = 0; i < listaDetalle.size(); i++) {
            fila = new Lista();
            fila.setListaFila(listaDetalle.get(i));
            _lista.add(fila);
        }
        ListaDetalle = _lista;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class Lista {
        @JsonProperty
        private List<String> ListaFila;

        public List<String> getListaFila() {
            return ListaFila;
        }

        public void setListaFila(List<String> listaFila) {
            ListaFila = listaFila;
        }
    }
}
