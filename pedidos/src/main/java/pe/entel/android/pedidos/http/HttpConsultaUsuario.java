package pe.entel.android.pedidos.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;


import com.google.gson.Gson;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.bean.BeanUsuarioOnline;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.UtilitarioData;

public class HttpConsultaUsuario extends HttpConexion {
    private Context ioContext;
    private BeanUsuario ioBeanUsuario;

    public HttpConsultaUsuario(BeanUsuario poBeanUsuario, Context poContext, ConfiguracionNextel.EnumTipActividad poEnumTipActividad, String mensaje) {
        super(poContext, mensaje, poEnumTipActividad);
        ioBeanUsuario = poBeanUsuario;
        ioContext = poContext;
    }


    @Override
    public boolean OnPreConnect() {
        if (!UtilitarioData.fnObtenerPreferencesBoolean(ioContext, "recibo")) {
            SharedPreferences loSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ioContext);
            if (loSharedPreferences.getBoolean(ioContext.getString(R.string.keypre_cobertura), false)) {
                setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_modofueracobertura));
                return false;
            }
        }
        if (!Utilitario.fnVerSignal(ioContext)) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            BeanUsuarioOnline loBeanUsuarioOnline = (BeanUsuarioOnline) getHttpResponseObject();

            int liIdHttprespuesta = loBeanUsuarioOnline.getIdResultado();
            String lsHttpRespuesta = loBeanUsuarioOnline.getResultado();

            if (liIdHttprespuesta == ConfiguracionNextel.CONSRESSERVIDOROK
                    || liIdHttprespuesta == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {
                BeanUsuarioOnline usuarioonline = new BeanUsuarioOnline();
                usuarioonline.setSerie(loBeanUsuarioOnline.getSerie());
                usuarioonline.setCorrelativo(loBeanUsuarioOnline.getCorrelativo());
                ((AplicacionEntel) ioContext.getApplicationContext()).setCurrentRecibo(usuarioonline);
            } else {
                ((AplicacionEntel) ioContext.getApplicationContext()).setCurrentRecibo(new BeanUsuarioOnline());
            }
            setHttpResponseIdMessage(liIdHttprespuesta, lsHttpRespuesta);
        }
    }

    public void subConHttp() {
        try {
            Log.v("URL", Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.CONSULTARECIBOUSUARIO));
            Log.v("URL", new Gson().toJson(ioBeanUsuario));

            String lsObjeto = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.CONSULTARECIBOUSUARIO), ioBeanUsuario);

            BeanUsuarioOnline loBeanUsuarioOnline = new Gson().fromJson(lsObjeto, BeanUsuarioOnline.class);
            setHttpResponseObject(loBeanUsuarioOnline);
        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, "Error HTTP: " + e.getMessage());
        }
    }
}
