package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by tkongr on 11/01/2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanProspecto {
    private String id;
    @JsonProperty
    private String codigo;
    @JsonProperty
    private String nombre;
    @JsonProperty
    private String direccion;
    @JsonProperty
    private String latitud;
    @JsonProperty
    private String longitud;
    @JsonProperty
    private String codUsuario;
    @JsonProperty
    private String tcli_cod;
    @JsonProperty
    private String giro;
    @JsonProperty
    private String campoAdicional1;
    @JsonProperty
    private String campoAdicional2;
    @JsonProperty
    private String campoAdicional3;
    @JsonProperty
    private String campoAdicional4;
    @JsonProperty
    private String campoAdicional5;
    @JsonProperty
    private String observacion;
    @JsonProperty
    private String fecha;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getCampoAdicional1() {
        return campoAdicional1;
    }

    public String getTcli_cod() {
        return tcli_cod;
    }

    public void setTcli_cod(String tcli_cod) {
        this.tcli_cod = tcli_cod;
    }

    public String getGiro() {
        return giro;
    }

    public void setGiro(String giro) {
        this.giro = giro;
    }

    public void setCampoAdicional1(String campoAdicional1) {
        this.campoAdicional1 = campoAdicional1;
    }

    public String getCampoAdicional2() {
        return campoAdicional2;
    }

    public void setCampoAdicional2(String campoAdicional2) {
        this.campoAdicional2 = campoAdicional2;
    }

    public String getCampoAdicional3() {
        return campoAdicional3;
    }

    public void setCampoAdicional3(String campoAdicional3) {
        this.campoAdicional3 = campoAdicional3;
    }

    public String getCampoAdicional4() {
        return campoAdicional4;
    }

    public void setCampoAdicional4(String campoAdicional4) {
        this.campoAdicional4 = campoAdicional4;
    }

    public String getCampoAdicional5() {
        return campoAdicional5;
    }

    public void setCampoAdicional5(String campoAdicional5) {
        this.campoAdicional5 = campoAdicional5;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
