package pe.entel.android.pedidos.bean;

import android.content.Context;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import pe.entel.android.pedidos.dal.DalProducto;
import pe.entel.android.pedidos.util.Configuracion;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanProducto extends BeanArticulo {


    @JsonProperty
    private String unidadDefecto = "";

    public BeanProducto() {
        super(Configuracion.TipoArticulo.PRODUCTO);
    }

    public String fnMostrarStock() {
        return this.getStock();
    }

    public String getUnidadDefecto() {
        return unidadDefecto;
    }

    public void setUnidadDefecto(String unidadDefecto) {
        this.unidadDefecto = unidadDefecto;
    }

    public BeanPresentacion getPresentacion(String codigoArticulo, Context context) {
        DalProducto dalProducto = new DalProducto(context);
        BeanPresentacion beanPresentacion = dalProducto.fnSelxCodigoPresentacion(codigoArticulo);
        return beanPresentacion;
    }

}