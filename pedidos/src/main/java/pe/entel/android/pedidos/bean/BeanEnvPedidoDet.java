package pe.entel.android.pedidos.bean;

import android.content.Context;
import android.text.TextUtils;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.dal.DalProducto;
import pe.entel.android.pedidos.util.Configuracion;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanEnvPedidoDet {

    private String idArticulo;
    @JsonProperty
    private String empresa;
    @JsonProperty
    private String codigoArticulo;
    @JsonProperty
    private String cantidad;
    @JsonProperty
    private String bonificacion;
    @JsonProperty
    private String monto;
    @JsonProperty
    private String precioBase;
    @JsonProperty
    private String stock;
    @JsonProperty
    private String descuento;
    @JsonProperty
    private String CodPedido;
    @JsonProperty
    private String nombreArticulo;
    @JsonProperty
    private String observacion;
    @JsonProperty
    private String tipoDescuento;
    @JsonProperty
    private String atendible;
    @JsonProperty
    private String codListaArticulo;
    @JsonProperty
    private String flete;
    @JsonProperty
    private String codAlmacen;
    @JsonProperty
    private String cantidadPre;
    @JsonProperty
    private String precioFrac;
    @JsonProperty
    private String tipo;
    @JsonProperty
    private String fechaUltVenta;
    @JsonProperty
    private String ultPrecio;
    @JsonProperty
    private int codBonificacion;
    @JsonProperty
    private double cantBonificacion;
    @JsonProperty
    private int multBonificacion;
    public String cantidadFrac;
    @JsonProperty
    public String montoSinDescuento;
    @JsonIgnore
    public BeanProducto producto;
    @JsonIgnore
    public String idListaPrecio;
    @JsonIgnore
    public boolean editBonificacion;
    @JsonProperty
    public String bonificacionFrac;
    @JsonProperty
    private String montoSoles; //@JBELVY
    @JsonProperty
    private String montoDolar; //@JBELVY
    @JsonProperty
    private String precioBaseSoles; //@JBELVY
    @JsonProperty
    private String precioBaseDolares; //@JBELVY
    @JsonProperty
    private String tipoMoneda; //@JBELVY


    public BeanEnvPedidoDet() {
        empresa = "";
        codigoArticulo = "";
        cantidad = "0";
        bonificacion = "0";
        monto = "0";
        precioBase = "0";
        stock = "0";
        descuento = "0";
        CodPedido = "";
        nombreArticulo = "";
        observacion = "";
        tipoDescuento = "";
        atendible = "1";
        codListaArticulo = "";
        flete = "0";
        codAlmacen = "0";
        precioFrac = "0";
        cantidadFrac = "0";
        cantidadPre = "0";
        fechaUltVenta = "";
        ultPrecio = "";
        tipo = "P";
        codBonificacion = 0;
        cantBonificacion = 0;
        montoSinDescuento = "0";
        idListaPrecio = "";
        editBonificacion = true;
        bonificacionFrac = "0";
        montoSoles = "0"; //@JBELVY
        montoDolar = "0"; //@JBELVY
        precioBaseSoles = "0"; //@JBELVY
        precioBaseDolares = "0"; //@JBELVY
        tipoMoneda = "1"; //@JBELVY
    }

    public boolean fnCantidadTotalMayorStock() {
        double ldStock;
        try {
            ldStock = Double.parseDouble(stock);
        } catch (Exception e) {
            e.printStackTrace();
            ldStock = 0;
        }
        return fnCantidadTotal() > ldStock;
    }

    public double fnCantidadTotal() {
        double ldCantidad;
        double ldBonificacion;
        double ldFrac;


        try {
            ldCantidad = Double.parseDouble(cantidad);
        } catch (Exception e) {
            e.printStackTrace();
            ldCantidad = 0;
        }

        try {
            ldBonificacion = Double.parseDouble(bonificacion);
        } catch (Exception e) {
            e.printStackTrace();
            ldBonificacion = 0;
        }

        try {
            ldFrac = Double.parseDouble(cantidadFrac);
        } catch (Exception e) {
            e.printStackTrace();
            ldFrac = 0;
        }
        return ldCantidad + ldBonificacion + ldFrac;
    }

    public String fnMostrarCantidad(String psTipoArticulo, int piNumeroDecimales) {

        String lsCantidad = "";

        if (psTipoArticulo.equals(Configuracion.TipoArticulo.PRESENTACION)) {

            double ldFrac;

            if (cantidadPre.equals("\t0\t")) {
                ldFrac = Double.parseDouble("0");
            } else {
                ldFrac = Double.parseDouble("".equals(cantidadFrac) ? "0.0" : cantidadFrac) / Double.parseDouble(cantidadPre);
            }

            lsCantidad = Utilitario.fnRound(Double.parseDouble(cantidad) + ldFrac, piNumeroDecimales);

        } else {
            lsCantidad = cantidad;
        }

        return lsCantidad;
    }

    public String fnMostrarStock(String psTipoArticulo) {
        String lsStock;
        double ldStock = 0;
        if (!TextUtils.isEmpty(stock)) {
            ldStock = Double.parseDouble(stock);
        }

        if (psTipoArticulo.equals(Configuracion.TipoArticulo.PRESENTACION)) {

            long llstock = (long) ldStock / Long.parseLong(cantidadPre);
            long llUnidades = (long) ldStock % Long.parseLong(cantidadPre);
            lsStock = Utilitario.fnRound(llstock, 0) + " - " + Utilitario.fnRound(llUnidades, 0);
        } else {
            lsStock = Utilitario.fnRound(ldStock, 0);
        }
        return lsStock;
    }

    public String fnMostrarStock(String psTipoArticulo, int piNumDecimales) {
        String lsStock;
        double ldStock = 0;
        if (!TextUtils.isEmpty(stock)) {
            ldStock = Double.parseDouble(stock);
        }

        if (psTipoArticulo.equals(Configuracion.TipoArticulo.PRESENTACION)) {
            double ldstock = ldStock / Double.parseDouble(cantidadPre);
            double ldUnidades = ldStock % Double.parseDouble(cantidadPre);
            lsStock = Utilitario.fnRound(ldstock, 0) + " - " + Utilitario.fnRound(ldUnidades, 00);
        } else {
            lsStock = Utilitario.fnRound(ldStock, piNumDecimales);
        }
        return lsStock;
    }

    public double fnCantidadABonificar(String psTipoArticulo) {
        double ldCantidad = 0;
        double ldCantidadFrac = 0;
        double ldCantidadPre = 0;
        try {
            ldCantidad = Double.valueOf(cantidad);
        } catch (Exception e) {
            ldCantidad = 0;
        }
        try {
            ldCantidadPre = Double.parseDouble(cantidadPre);
        } catch (Exception e) {
            ldCantidadPre = 0;
        }
        try {
            ldCantidadFrac = Double.valueOf(cantidadFrac);
        } catch (Exception e) {
            ldCantidadFrac = 0;
        }
        double ldCantidadFinal = 0;
        if (psTipoArticulo.equals(Configuracion.TipoArticulo.PRESENTACION))
            ldCantidadFinal = ldCantidad * ldCantidadPre + ldCantidadFrac;
        else
            ldCantidadFinal = ldCantidad + ldCantidadFrac;

        return ldCantidadFinal - cantBonificacion;
    }

    public int fnMultiploBonificacion(BeanBonificacionDet poBeanBonificacionDet, String psTipoArticulo) {

        double ldMultiplo = 0;
        if (poBeanBonificacionDet.getExiste().equals(Configuracion.FLGVERDADERO)) {
            ldMultiplo = 1;
        } else {
            double ldCantBonificacion = Double.valueOf(poBeanBonificacionDet.getCantidad());
            if (psTipoArticulo.equals(Configuracion.TipoArticulo.PRESENTACION)) {
                ldCantBonificacion *= Double.parseDouble(cantidadPre);
            }
            ldMultiplo = fnCantidadABonificar(psTipoArticulo) / ldCantBonificacion;
        }


        return (int) ldMultiplo;
    }

    public void subModificarCantBonificacion(BeanBonificacionDet poBeanBonificacionDet, int piMultiplo, String psTipoArticulo) {
        double ldCantBonificacion = 0;
        if (poBeanBonificacionDet.getExiste().equals(Configuracion.FLGVERDADERO))
            ldCantBonificacion = 1;
        else
            ldCantBonificacion = Double.valueOf(poBeanBonificacionDet.getCantidad());

        cantBonificacion += ldCantBonificacion * piMultiplo;

        if (psTipoArticulo.equals(Configuracion.TipoArticulo.PRESENTACION)) {
            cantBonificacion *= Double.parseDouble(cantidadPre);
        }

    }

    public boolean fnValidarBonificacion(BeanBonificacionDet poBeanBonificacionDet, String psTipoArticulo) {

        double ldCantPuedoBonifcar = fnCantidadABonificar(psTipoArticulo);
        double ldCantBonificacion = 0;

        if (poBeanBonificacionDet.getExiste().equals(Configuracion.FLGVERDADERO))
            ldCantBonificacion = 1;
        else
            ldCantBonificacion = Double.valueOf(poBeanBonificacionDet.getCantidad());

        if (psTipoArticulo.equals(Configuracion.TipoArticulo.PRESENTACION)) {
            ldCantBonificacion *= Double.parseDouble(cantidadPre);
        }

        return codBonificacion == 0
                && poBeanBonificacionDet.getCodigoArticulo().equals(codigoArticulo)
                && ldCantPuedoBonifcar >= ldCantBonificacion;
    }


    public String getTipoDescuento() {
        return tipoDescuento;
    }


    public void setTipoDescuento(String tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }


    public String getIdArticulo() {
        return idArticulo;
    }


    public void setIdArticulo(String idArticulo) {
        this.idArticulo = idArticulo;
    }


    public String getEmpresa() {
        return empresa;
    }


    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }


    public String getCodigoArticulo() {
        return codigoArticulo;
    }


    public void setCodigoArticulo(String codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }


    public String getCantidad() {
        return cantidad;
    }


    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getBonificacion() {
        return bonificacion;
    }


    public void setBonificacion(String bonificacion) {
        this.bonificacion = bonificacion;
    }


    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getMontoSoles() {
        return montoSoles;
    } //@JBELVY

    public void setMontoSoles(String montoSoles) {
        this.montoSoles = montoSoles;
    } //@JBELVY

    public String getMontoDolar() {
        return montoDolar;
    } //@JBELVY

    public void setMontoDolar(String montoDolar) {
        this.montoDolar = montoDolar;
    } //@JBELVY

    public String getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(String precioBase) {
        this.precioBase = precioBase;
    }

    public String getPrecioBaseSoles() {
        return precioBaseSoles;
    } //@JBELVY

    public void setPrecioBaseSoles(String precioBaseSoles) {
        this.precioBaseSoles = precioBaseSoles;
    } //@JBELVY

    public String getPrecioBaseDolares() {
        return precioBaseDolares;
    } //@JBELVY

    public void setPrecioBaseDolares(String precioBaseDolares) {
        this.precioBaseDolares = precioBaseDolares;
    } //@JBELVY

    public String getTipoMoneda() {
        return tipoMoneda;
    } //@JBELVY

    public void setTipoMoneda(String tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    } //@JBELVY

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getCodPedido() {
        return CodPedido;
    }

    public void setCodPedido(String codPedido) {
        CodPedido = codPedido;
    }

    public String getNombreArticulo() {
        return nombreArticulo;
    }

    public void setNombreArticulo(String nombreArticulo) {
        this.nombreArticulo = nombreArticulo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getAtendible() {
        return atendible;
    }

    public void setAtendible(String atendible) {
        this.atendible = atendible;
    }

    public String getCodListaArticulo() {
        return codListaArticulo;
    }

    public void setCodListaArticulo(String codListaArticulo) {
        this.codListaArticulo = codListaArticulo;
    }

    public String getFlete() {
        return flete;
    }

    public void setFlete(String flete) {
        this.flete = flete;
    }

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public String getPrecioFrac() {
        return precioFrac;
    }

    public void setPrecioFrac(String precioFrac) {
        this.precioFrac = precioFrac;
    }

    public String getCantidadFrac() {
        return cantidadFrac;
    }

    public void setCantidadFrac(String cantidadFrac) {
        this.cantidadFrac = cantidadFrac;
    }

    public String getCantidadPre() {
        return cantidadPre;
    }

    public void setCantidadPre(String cantidadPre) {
        this.cantidadPre = cantidadPre;
    }

    public BeanProducto getProducto() {
        return producto;
    }

    public void setProducto(BeanProducto producto) {
        this.producto = producto;
    }

    public String getUltPrecio() {
        return ultPrecio;
    }

    public void setUltPrecio(String ultPrecio) {
        this.ultPrecio = ultPrecio;
    }

    public String getFechaUltVenta() {
        return fechaUltVenta;
    }

    public void setFechaUltVenta(String fechaUltVenta) {
        this.fechaUltVenta = fechaUltVenta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCodBonificacion() {
        return codBonificacion;
    }

    public void setCodBonificacion(int codBonificacion) {
        this.codBonificacion = codBonificacion;
    }

    public double getCantBonificacion() {
        return cantBonificacion;
    }

    public void setCantBonificacion(double cantBonificacion) {
        this.cantBonificacion = cantBonificacion;
    }

    public int getMultBonificacion() {
        return multBonificacion;
    }

    public void setMultBonificacion(int multBonificacion) {
        this.multBonificacion = multBonificacion;
    }

    public String getMontoSinDescuento() {
        return montoSinDescuento;
    }

    public void setMontoSinDescuento(String montoSinDescuento) {
        this.montoSinDescuento = montoSinDescuento;
    }

    public String getIdListaPrecio() {
        return idListaPrecio;
    }

    public void setIdListaPrecio(String idListaPrecio) {
        this.idListaPrecio = idListaPrecio;

    }

    public boolean isEditBonificacion() {
        return editBonificacion;
    }

    public void setEditBonificacion(boolean editBonificacion) {
        this.editBonificacion = editBonificacion;
    }

    public String getBonificacionFrac() {
        return bonificacionFrac;
    }

    public void setBonificacionFrac(String bonificacionFrac) {
        this.bonificacionFrac = bonificacionFrac;
    }

    public void calcularMontoItem() {
        monto = String.valueOf(calcularMontoTotal() + calcularTotalFlete());
        montoSoles = String.valueOf(calcularMontoSolesTotal() + calcularTotalFlete());
    }

    public void calcularMontoDolarItem() {
        montoDolar = String.valueOf(calcularMontoDolarTotal() + calcularTotalFlete());
    }

    public void CalcularMontoItemVerificarPedido() {
        double valor = 0.0;
        if (getMonto().equals("") || getMonto() == null) {
            valor = 0;
        } else {
            valor = Double.parseDouble(getMonto());
        }

        monto = String.valueOf(valor + calcularTotalFlete());
    }

    public void CalcularMontoSolesItemVerificarPedido() {
        double valor = 0.0;
        if (getMontoSoles().equals("") || getMontoSoles() == null) {
            valor = 0;
        } else {
            valor = Double.parseDouble(getMontoSoles());
        }

        montoSoles = String.valueOf(valor + calcularTotalFlete());
    }

    public void CalcularMontoDolarItemVerificarPedido() {
        double valor = 0.0;
        if (getMontoDolar().equals("") || getMontoDolar() == null) {
            valor = 0;
        } else {
            valor = Double.parseDouble(getMontoDolar());
        }

        montoDolar = String.valueOf(valor + calcularTotalFlete());
    }

    public Double calcularMontoTotal() {
        double montoBruto = Double.valueOf(precioBase) * Double.valueOf(cantidad);
        double montoFrac;
        double montoDesc = obtenerDescuento(montoBruto);
        if (cantidadPre.equals("0") || cantidadPre.equals("")) {
            montoFrac = 0.0;
        } else {
            montoFrac = Double.valueOf(Double.valueOf(precioBase) * Double.parseDouble(cantidadFrac) / Double.parseDouble(cantidadPre));
        }
        return montoBruto - montoDesc + montoFrac;
    }

    public Double calcularMontoSolesTotal() {
        double montoBruto = Double.valueOf(precioBaseSoles) * Double.valueOf(cantidad);
        double montoFrac;
        double montoDesc = obtenerDescuento(montoBruto);
        if (cantidadPre.equals("0") || cantidadPre.equals("")) {
            montoFrac = 0.0;
        } else {
            montoFrac = Double.valueOf(Double.valueOf(precioBaseSoles) * Double.parseDouble(cantidadFrac) / Double.parseDouble(cantidadPre));
        }
        return montoBruto - montoDesc + montoFrac;
    }

    public Double calcularMontoDolarTotal() {
        double montoBruto = Double.valueOf(precioBaseDolares) * Double.valueOf(cantidad);
        double montoFrac;
        double montoDesc = obtenerDescuento(montoBruto);
        if (cantidadPre.equals("0") || cantidadPre.equals("")) {
            montoFrac = 0.0;
        } else {
            montoFrac = Double.valueOf(Double.valueOf(precioBaseDolares) * Double.parseDouble(cantidadFrac) / Double.parseDouble(cantidadPre));
        }
        return montoBruto - montoDesc + montoFrac;
    }

    private Double obtenerDescuento(Double montoBruto) {
        double montoDesc = 0.0;
        if (tipoDescuento.equals(Configuracion.DESCUENTO_PORCENTAJE)) {
            montoDesc = (Double.parseDouble(descuento) / 100)
                    * (montoBruto);
        } else {
            montoDesc = Double.parseDouble(descuento);
        }
        return montoDesc;
    }

    public Double calcularTotalFlete() {
        return Double.valueOf(flete);
    }

    public void obtenerProducto(Context context, String tipoArticulo) {
        DalProducto dalProducto = new DalProducto(context);

        if (tipoArticulo.equals(Configuracion.TipoArticulo.PRESENTACION)) {
            BeanPresentacion beanPresentacion = dalProducto.fnSelxCodigoPresentacion(codigoArticulo);
            idArticulo = beanPresentacion.getId();
            producto = beanPresentacion.getProducto();

        } else {
            producto = dalProducto.fnSelxCodProductoxCodigo(codigoArticulo);
            indicarIdArticulo();
        }
    }

    public void indicarIdArticulo() {
        idArticulo = producto.getId();
    }

}
