package pe.entel.android.pedidos.util.pedidos;

/**
 * Created by tkongr on 18/03/2016.
 */
public class Constantes {

    public static final int CONSDIASINCRONIZAR = 1;
    public static final int VALIDASINCRONIZACION = 2;
    public static final int ENVIOPENDIENTES = 3;
    public static final int VALIDASINCRO = 4;
    public static final int CONSRECUPERARPEDIDO = 5;
    public static final int CONSSALIR = 6;
    public static final int CONSDIAINGDATO = 7;
    public static final int NOHAYPENDIENTES = 8;
    public static final int DEUDAVENCIDA = 9;
    public static final int CREDITOSUPERADO = 10;
    public static final int CONSICOVALIDAR = 11;
    public static final int CONSOTRO = 12;
    public static final int CONSBACKFORMULARIOINICIO = 13;
    public static final int VALIDAHOME = 14;
    public static final int ERRORPEDIDO = 15;
    public static final int CONSOKENVIO = 17;
    public static final int CONSERRORENVIO = 18;
    public static final int CONSGRABAR = 19;
    public static final int CONSVALIDAR = 20;
    public static final int CONSCOBRA = 21;
    public static final int CONSCANCEL = 22;
    public static final int CONSREGCONDVTA = 23;

    public static final int CONSDIALOGVERIFICARPEDIDONOCOBERTURA = 28;

    @SuppressWarnings("unused")
    public static final int CONSBACK = 3;
    @SuppressWarnings("unused")
    private static final int CONSMENU = 4;

    //Administrador Login
    public static final int CONSMENCONFIGURACION = 1;
    public static final int CONSMENSUCURSALES = 2;

    //Ntrack
    public static final int DIALOG_NSERVICES_DOWNLOAD = 14;
    public static final int REQUEST_NSERVICES = 24;

    public static final int SUPEROMAXIMOITEMS = 25;
    public static final int SUPERAMONTOMAXIMO = 26;
    public static final int NOSUPERAMONTOMINIMO = 27;
    public static final int CONSERRORACTUALIZAR = 29;
}
