package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.Gps;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout;
import pe.com.nextel.android.widget.NexToast;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanControl;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCtrl;
import pe.entel.android.pedidos.bean.BeanEnvPedidoDet;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanRespVerificarPedidoGeneral;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.bean.BeanVerificarPedido;
import pe.entel.android.pedidos.dal.DalAlmacen;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.dal.DalProducto;
import pe.entel.android.pedidos.http.HttpGrabaPedido;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.http.HttpVerificarPedido;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.ControlesDinamicos;
import pe.entel.android.pedidos.util.UtilitarioData;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

/**
 * Created by rtamayov on 25/05/2015.
 */
public class ActPedidoFormulario extends NexActivity implements OnClickListener, NexMenuDrawerLayout.NexMenuCallbacks,
        AdapMenuPrincipalSliding.MenuSlidingListener {

    private LinearLayout layout;
    private Button btnGuardar;
    private Button btnVerificar;
    private List<BeanControl> ioListaControles;

    BeanUsuario loBeanUsuario = null;
    private BeanEnvPedidoCab loBeanPedidoCab = null;

    private AdapMenuPrincipalSliding _menu;
    private final int CONSDIASINCRONIZAR = 1;
    private final int VALIDASINCRONIZACION = 2;
    private final int ENVIOPENDIENTES = 3;
    private final int VALIDASINCRO = 4;
    private final int NOHAYPENDIENTES = 5;
    private final int ERRORPEDIDO = 30;
    private final int CONSBACK = 6;
    private final int CONSVALIDAR = 7;
    private final int CONSGRABAR = 8;
    private final int CONSBACKFORMULARIOINICIO = 50;
    private final int CONSBACKPEDIDOPENDIENTE = 51;

    private String isMostrarCondVenta;
    boolean estadoSincronizar = false;
    // Integracion Ntrack
    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro


    private String isTitle;
    //PARAMETROS DE CONFIGURACION
    private String isNumeroDecimales;
    private String isMoneda;
    private String isMonedaDolar;
    private String isGps;
    private String isDescontarStock;
    private String isBonificacion;
    private String isMostrarAlmacen;
    private String isPresentacion;
    private double isMontoMinimoPedido;
    private double isMontoMaximoPedido;
    private int isMaximoItemsPedido;

    private String isMaximoNumeroDecimales;
    private int iiMaximoNumeroDecimales;
    private String isVerificarPedido;
    private boolean isHttpVerificarPedido = false;
    private String isStockRestrictivo;

    private String isRestriccionCobertura;
    private String sharePreferenceBeanPedidoCab;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);
        sharePreferenceBeanPedidoCab = "beanPedidoCabVerificarPedido";
        String lsBeanVisitaCab = UtilitarioData.fnObtenerPreferencesString(this, sharePreferenceBeanPedidoCab);

        if (lsBeanVisitaCab.equals("")) {
            if (!UtilitarioData.fnObtenerPreferencesBoolean(this, "pedidoPendiente")) {
                sharePreferenceBeanPedidoCab = "beanPedidoCab";
            } else {
                sharePreferenceBeanPedidoCab = "beanPedidoCabPendiente";
                ((AplicacionEntel) getApplication()).setCurrentCodFlujo(Configuracion.CONSPEDIDO);
            }
            lsBeanVisitaCab = UtilitarioData.fnObtenerPreferencesString(this, sharePreferenceBeanPedidoCab);
        }


        loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper.fromJson(
                lsBeanVisitaCab, BeanEnvPedidoCab.class);

        isTitle = getString(loBeanPedidoCab.getFlgTipo()
                .equals(Configuracion.FLGPEDIDO)
                ? R.string.actpedidofin_bardetalle_pedido
                : R.string.actpedidofin_bardetalle_cotizacion);

        isMostrarCondVenta = Configuracion.EnumConfiguracion
                .MOSTRAR_CONDICION_VENTA.getValor(this);

        isNumeroDecimales = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(this);
        isMoneda = Configuracion.EnumConfiguracion.SIMBOLO_MONEDA.getValor(this);
        isMonedaDolar = "$.";
        isGps = Configuracion.EnumConfiguracion.GPS.getValor(this);
        isDescontarStock = Configuracion.EnumConfiguracion.DESCONTAR_STOCK.getValor(this);
        isBonificacion = Configuracion.EnumFuncion.BONIFICACION.getValor(this);
        isMostrarAlmacen = Configuracion.EnumConfiguracion.MOSTRAR_ALMACEN.getValor(this);
        isPresentacion = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(this);
        isMaximoNumeroDecimales = Configuracion.EnumConfiguracion.MAXIMO_NUMERODECIMALES.getValor(this);
        isVerificarPedido = Configuracion.EnumConfiguracion.VERIFICACION_PEDIDO.getValor(this);
        isMaximoItemsPedido = Integer.parseInt(Configuracion.EnumConfiguracion.MAXIMO_ITEMS_PEDIDO.getValor(this));
        isStockRestrictivo = Configuracion.EnumConfiguracion.STOCK_RESTRICTIVO.getValor(this);

        obtenerValorMontoPedido();
        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);
        subIniMenu();
    }

    public void subIniActionBar() {

        String titulo = "";
        if (getIntent().getExtras().getInt(Configuracion.EXTRA_FORMULARIO) == Configuracion.FORMULARIO_INICIO) {
            titulo = getString(R.string.actpedidoformulario_ttlinicio);
        } else if (getIntent().getExtras().getInt(Configuracion.EXTRA_FORMULARIO) == Configuracion.FORMULARIO_FIN) {
            titulo = getString(R.string.actpedidoformulario_ttlfin);
        }

        this.getActionBarGD().setTitle(titulo);
    }

    private void obtenerValorMontoPedido() {

        if (Configuracion.EnumConfiguracion.MONTO_MAXIMO_PEDIDO.getValor(this).equals(""))
            isMontoMaximoPedido = 0.0;
        else
            isMontoMaximoPedido = Double.parseDouble(Configuracion.EnumConfiguracion.MONTO_MAXIMO_PEDIDO.getValor(this));

        if (Configuracion.EnumConfiguracion.MONTO_MINIMO_PEDIDO.getValor(this).equals(""))
            isMontoMinimoPedido = 0.0;
        else
            isMontoMinimoPedido = Double.parseDouble(Configuracion.EnumConfiguracion.MONTO_MINIMO_PEDIDO.getValor(this));

    }

    @Override
    public void onBackPressed() {

        if (getIntent().getExtras().getInt(Configuracion.EXTRA_FORMULARIO) == Configuracion.FORMULARIO_INICIO) {
            if (UtilitarioData.fnObtenerPreferencesBoolean(this, "pedidoPendiente"))
                showDialog(CONSBACKPEDIDOPENDIENTE);
            else
                showDialog(CONSBACK);
        } else if (getIntent().getExtras().getInt(Configuracion.EXTRA_FORMULARIO) == Configuracion.FORMULARIO_FIN) {
            super.onBackPressed();
        }
    }

    @Override
    protected void subSetControles() {
        super.subSetControles();
        setActionBarContentView(R.layout.pedidoformulario);
        layout = (LinearLayout) findViewById(R.id.actListDinamyc);
        btnGuardar = (Button) findViewById(R.id.actactividaddynamic_btnGuardar);
        btnVerificar = (Button) findViewById(R.id.actactividaddynamic_btnVerificar);

        if (getIntent().getExtras().getInt(Configuracion.EXTRA_FORMULARIO) == Configuracion.FORMULARIO_INICIO) {
            btnGuardar.setText(getString(R.string.actpedidoformulario_btnguardarinicio));
        } else {
            String mensajeHttpVerificar = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE).getString("MensajeVerificarPedido", "");
            subMostrarVerificar(!mensajeHttpVerificar.equals("1"));
        }

        btnGuardar.setOnClickListener(this);
        btnVerificar.setOnClickListener(this);
        setMaximoNumeroDecimales();
        subLlenarListaControles();
        subCrearControlesDinamicos();
    }

    private void subMostrarVerificar(boolean pbVerificarPedido) {

        if (isVerificarPedido.equals(Configuracion.FLGVERDADERO) && pbVerificarPedido) {
            btnVerificar.setVisibility(View.VISIBLE);
            btnGuardar.setVisibility(View.GONE);
        } else {
            btnVerificar.setVisibility(View.GONE);
            btnGuardar.setVisibility(View.VISIBLE);
        }
    }

    private void setMaximoNumeroDecimales() {
        iiMaximoNumeroDecimales = 0;
        try {
            iiMaximoNumeroDecimales = Integer.parseInt(isMaximoNumeroDecimales);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ActProductoDetalle", "Error conversion maximo numero de decimales: " + e.getMessage());
        }
    }

    private void subLlenarListaControles() {
        try {
            DalPedido dalPedido = new DalPedido(this);
            ioListaControles = dalPedido.fnSelControles(getIntent().getExtras().getInt(Configuracion.EXTRA_FORMULARIO));

        } catch (Exception e) {
            Log.v("Error-ListDynamic", "Error consultando controles");
        }
    }

    private void subCrearControlesDinamicos() {

        try {
            if (layout != null) {
                layout.removeAllViews();
            }

            for (Iterator<BeanControl> iterator = ioListaControles.iterator(); iterator.hasNext(); ) {
                BeanControl loControl = (BeanControl) iterator.next();
                ControlesDinamicos.ingresarControles(this, loBeanPedidoCab, loControl, layout);
            }

        } catch (Exception e) {
            Log.e("Error-ListDynamic", "ERROR", e);
        }

    }

    private void grabarControles() {
        try {
            String mensajeValidar = ControlesDinamicos.subGuardarControles(this, layout, ioListaControles);

            if (mensajeValidar.equals("")) {
                grabarFormulario();
            } else {
                Toast loToast = Toast.makeText(this, mensajeValidar, Toast.LENGTH_SHORT);
                loToast.setGravity(Gravity.CENTER, 0, 0);
                loToast.show();
            }
        } catch (Exception e) {
            Log.e("Error-ListDynamic", "ERROR", e);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == btnGuardar) {
            try {
                String mensajeValidar = ControlesDinamicos.subGuardarControles(this, layout, ioListaControles);
                if (mensajeValidar.equals("")) {
                    subSiguientePantalla();
                } else {
                    Toast loToast = Toast.makeText(this, mensajeValidar, Toast.LENGTH_SHORT);
                    loToast.setGravity(Gravity.CENTER, 0, 0);
                    loToast.show();
                }
            } catch (Exception e) {
                Log.e("Error-ListDynamic", "ERROR", e);
            }
        } else if (view == btnVerificar) {
            Log.i("ACTPEDIDOFORMULARIO", "");
            isHttpVerificarPedido = true;
            grabarControles();
            UtilitarioData.fnCrearPreferencesPutString(this, "isHttpVerificarPedido", "true");
            BeanEnvPedidoCab pedidoAux = loBeanPedidoCab;
            pedidoAux.setMontoTotal(Utilitario.fnRound(
                    loBeanPedidoCab.calcularMontoTotal(),
                    Integer.valueOf(isNumeroDecimales)));
            pedidoAux.setMontoTotalSoles(Utilitario.fnRound(
                    loBeanPedidoCab.calcularMontoTotalSoles(),
                    Integer.valueOf(isNumeroDecimales)));
            pedidoAux.setMontoTotalDolar(Utilitario.fnRound(
                    loBeanPedidoCab.calcularMontoTotalDolar(),
                    Integer.valueOf(isNumeroDecimales)));
            pedidoAux.setListaControl(loBeanPedidoCab.getListaCtrlPedido());
            new HttpVerificarPedido(BeanVerificarPedido.mapperVerificarPedido(pedidoAux), this, this.fnTipactividad()).execute();
        }
    }

    private void subSiguientePantalla() {
        grabarFormulario();
        if (getIntent().getExtras().getInt(Configuracion.EXTRA_FORMULARIO) == Configuracion.FORMULARIO_INICIO) {
            if (loBeanPedidoCab.isEnEdicion() || UtilitarioData.fnObtenerPreferencesBoolean(this, "pedidoPendiente")) {
                finish();
                startActivity(new Intent(ActPedidoFormulario.this, ActProductoPedido.class));
            } else {
                Intent loIntent = new Intent(ActPedidoFormulario.this,
                        isMostrarCondVenta.equals(
                                Configuracion.FLGVERDADERO)
                                ? ActPedidoCondicionVenta.class
                                : ActProductoPedido.class);
                loIntent.putExtra("IdCliente", getIntent().getExtras().getString("IdCliente"));
                finish();
                startActivity(loIntent);
            }
        } else if (getIntent().getExtras().getInt(Configuracion.EXTRA_FORMULARIO) == Configuracion.FORMULARIO_FIN) {
            if (isMontoMinimoPedido > 0.0 && Double.parseDouble(loBeanPedidoCab.getMontoTotal()) < isMontoMinimoPedido)
                UtilitarioPedidos.mostrarDialogo(this, Constantes.NOSUPERAMONTOMINIMO);

            else if (isMontoMaximoPedido > 0.0 && Double.parseDouble(loBeanPedidoCab.getMontoTotal()) > isMontoMaximoPedido)
                UtilitarioPedidos.mostrarDialogo(this, Constantes.SUPERAMONTOMAXIMO);

            else if (isMaximoItemsPedido > 0 && loBeanPedidoCab.getListaDetPedido().size() == isMaximoItemsPedido)
                UtilitarioPedidos.mostrarDialogo(this, Constantes.SUPEROMAXIMOITEMS);
            else
                subFinalizarPedido();
        }
    }

    private void grabarFormulario() {
        if (!UtilitarioData.fnObtenerPreferencesBoolean(this, "pedidoPendiente")) {
            String lsBeanVisitaCab = UtilitarioData.fnObtenerPreferencesString(this, sharePreferenceBeanPedidoCab);
            loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper.fromJson(lsBeanVisitaCab, BeanEnvPedidoCab.class);
        }
        loBeanPedidoCab.setListaControl(convertToListaControles(ioListaControles));
        String lsBeanVisitaCab = "";
        try {
            lsBeanVisitaCab = BeanMapper.toJson(loBeanPedidoCab, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        UtilitarioData.fnCrearPreferencesPutString(this, sharePreferenceBeanPedidoCab, lsBeanVisitaCab);
    }

    private void subFinalizarPedido() {
        if (loBeanPedidoCab.getListaDetPedido().size() <= 0) {
            UtilitarioPedidos.mostrarDialogo(this, CONSVALIDAR);
        } else {
            UtilitarioPedidos.mostrarDialogo(this, CONSGRABAR);
        }
    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            //TODO Auto-generated method stub
            Intent loIntentLogin = new Intent(ActPedidoFormulario.this,
                    ActLogin.class);
            loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loIntentLogin);
            }
        });

    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        Intent intentl = new Intent(this, ActMenu.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intentl);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = null;
            DalPedido loDalPedido = new DalPedido(this);
            loList = loDalPedido.fnEnvioPedidosPendientes(loBeanUsuario
                    .getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);

                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();
                showDialog(CONSDIASINCRONIZAR);
            } else {
                showDialog(VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub

        String lsBeanUsuario = getSharedPreferences(
                "Actual", MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper
                .fromJson(lsBeanUsuario,
                        BeanUsuario.class);
        this.setMsgOk("Eficiencia Online");

        BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
        loBeanListaEficienciaOnline.setIdResultado(0);
        loBeanListaEficienciaOnline.setResultado("");
        loBeanListaEficienciaOnline.setListaEficiencia(null);
        loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

        new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

        Intent loInstent = new Intent(this, ActKpiDetalle.class);
        loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loInstent);
    }

    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        ((AplicacionEntel) getApplication()).setCurrentCliente(null);
        ((AplicacionEntel) getApplication())
                .setCurrentCodFlujo(Configuracion.CONSSTOCK);
        ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
        Intent loIntento = new Intent(ActPedidoFormulario.this,
                ActProductoListaOnline.class);
        loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        BeanExtras loBeanExtras = new BeanExtras();
        loBeanExtras.setFiltro("");
        loBeanExtras.setTipo(ConfiguracionNextel.EnumTipBusqueda.NOMBRE);
        String lsFiltro = "";
        try {
            lsFiltro = BeanMapper.toJson(loBeanExtras, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
        startActivity(loIntento);
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(getString(R.string.ml_sincronizarantes)));
        }

    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }


    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), NexToast.EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }


    @Override
    protected DialogFragmentYesNo.DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        try {
            DialogFragmentYesNo loDialog;
            switch (id) {
                case DIALOG_NSERVICES_DOWNLOAD:
                    String lsNServices;
                    if (ioDALNservices == null)
                        ioDALNservices = DALNServices.getInstance(this,
                                R.string.db_name);
                    try {
                        lsNServices = ioDALNservices.fnSelNServicesLabel();
                    } catch (Exception e) {
                        Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                e);
                        lsNServices = getString(R.string.nservices_label_default);
                    }
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG
                                    + DIALOG_NSERVICES_DOWNLOAD,
                            getString(R.string.nservices_consultingdownlad)
                                    .replace("{0}", lsNServices),
                            DialogFragmentYesNo.EnumLayoutResource.getDefaultIconHelp(this));
                    return new DialogFragmentYesNo.DialogFragmentYesNoPairListener(loDialog,
                            new DialogFragmentYesNo.OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(
                                        DialogFragmentYesNo dialog) {
                                    try {
                                        subIniNServicesDownload(ioDALNservices
                                                .fnSelNServicesAPK());
                                    } catch (Exception e) {
                                        subShowException(e);
                                    }
                                }

                                @Override
                                public void performButtonNo(
                                        DialogFragmentYesNo dialog) {
                                }
                            });

                default:
                    return super.onCreateNexDialog(id);
            }
        } catch (Exception e) {
            subShowException(e);
            return super.onCreateNexDialog(id);
        }
    }

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        AlertDialog loAlertDialog = null;

        switch (id) {
            case Constantes.NOSUPERAMONTOMINIMO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_montoMinimoNoSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.SUPERAMONTOMAXIMO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_montoMaximoSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.SUPEROMAXIMOITEMS:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_itemsSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSBACKPEDIDOPENDIENTE:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgdescartarpendiente))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);
                                        startActivity(new Intent(ActPedidoFormulario.this, ActListaPedidosPendientes.class));
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;
            case CONSBACKFORMULARIOINICIO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgbackformularioinicio))
                        .setPositiveButton(getString(R.string.dlg_btneditarformulario),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        String mensajeVerificarPedido = UtilitarioData.fnObtenerPreferencesString(ActPedidoFormulario.this, "MensajeVerificarPedido");

                                        if (mensajeVerificarPedido.equals("")) {
                                            String lsBeanPedidoCab = null;
                                            try {
                                                lsBeanPedidoCab = BeanMapper.toJson(loBeanPedidoCab, false);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            UtilitarioData.fnCrearPreferencesPutString(ActPedidoFormulario.this, "beanPedidoCab", lsBeanPedidoCab);
                                        }

                                        UtilitarioData.fnCrearPreferencesPutBoolean(ActPedidoFormulario.this, "editarFormularioInicio", true);
                                        UtilitarioData.fnCrearPreferencesPutString(ActPedidoFormulario.this, "beanPedidoCabVerificarPedido", "");
                                        UtilitarioData.fnCrearPreferencesPutString(ActPedidoFormulario.this, "MensajeVerificarPedido", "");

                                        Intent loIntent = new Intent(ActPedidoFormulario.this, ActPedidoFormulario.class);
                                        loIntent.putExtra(Configuracion.EXTRA_FORMULARIO, Configuracion.FORMULARIO_INICIO);
                                        loIntent.putExtra(Configuracion.EXTRA_FORMULARIO_INICIO_EDITAR, true);
                                        finish();
                                        startActivity(loIntent);

                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btncancelarpedido),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        startActivity(new Intent(ActPedidoFormulario.this, ActListaPedidosPendientes.class));
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.ERRORPEDIDO:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.dlg_titerror))
                        .setMessage(getString(R.string.actgrabapedido_dlgerrorpedido))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        Intent intentl = new Intent(ActPedidoFormulario.this, ActClienteDetalle.class);
                                        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intentl);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;


            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            BeanUsuario loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(ActPedidoFormulario.this);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActPedidoFormulario.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            estadoSincronizar = false;
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActPedidoFormulario.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSBACK:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);
                                        if (!loBeanPedidoCab.getCodigoPedidoServidor().equals("0") && isStockRestrictivo.equals(Configuracion.FLGVERDADERO)) {
                                            loBeanPedidoCab.setFlgEliminar(Configuracion.FLGVERDADERO);
                                            new HttpGrabaPedido(loBeanPedidoCab, ActPedidoFormulario.this, fnTipactividad(), getString(R.string.httpeliminar_dlg)).execute();
                                        } else {
                                            irclientedetalle();
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case CONSVALIDAR:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(isTitle)
                        .setMessage(getString(R.string.actpedidofin_msnvalidacion))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSGRABAR:
                Calendar c = Calendar.getInstance();
                Date loDate = c.getTime();
                final String sFecha = DateFormat.format(
                        "dd/MM/yyyy kk:mm:ss", loDate).toString();

                if (!UtilitarioData.fnObtenerPreferencesBoolean(this, "pedidoPendiente")) {
                    String lsBeanVisitaCab = UtilitarioData.fnObtenerPreferencesString(this, sharePreferenceBeanPedidoCab);
                    loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper.fromJson(lsBeanVisitaCab, BeanEnvPedidoCab.class);
                }

                //loBeanPedidoCab.setListaCtrlPedido(subCrearListaControl());
                loBeanPedidoCab.setListaControl(convertToListaControles(ioListaControles));
                BeanCliente lobecli = ((AplicacionEntel) getApplication())
                        .getCurrentCliente();

                StringBuffer loStb = new StringBuffer();
                loStb.append(getString(R.string.actpedidofin_dlgfin))
                        .append('\n');
                loStb.append(" C\u00F3digo : ").append(lobecli.getClicod())
                        .append('\n');
                loStb.append(" Nombre : ").append(lobecli.getClinom())
                        .append('\n');
                loStb.append(" Monto : ")
                        .append(isMoneda)
                        .append(" ")
                        .append(Utilitario.fnRound(loBeanPedidoCab.calcularMontoSolesCompleto(),
                                Integer.valueOf(isNumeroDecimales)))
                        .append('\n');
                loStb.append(" Monto : ")
                        .append(isMonedaDolar)
                        .append(" ")
                        .append(Utilitario.fnRound(loBeanPedidoCab.calcularMontoDolarCompleto(),
                                Integer.valueOf(isNumeroDecimales)))
                        .append('\n');
                loStb.append(" Items Pedido : ")
                        .append(loBeanPedidoCab.getListaDetPedido().size())
                        .append('\n');

                if (isBonificacion
                        .equals(Configuracion.FLGVERDADERO)) {
                    loStb.append(" Items Bonificados : ").append(loBeanPedidoCab.calcularCantTotalItemsBonificados())
                            .append('\n');
                }

                loStb.append(sFecha).append('\n');
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(isTitle)
                        .setMessage(loStb.toString())

                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {

                                            subGrabar();
                                            validarStock();
                                            enviarPedidoServer();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btncancelar),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        return;
                                    }
                                })
                        .create();
                return loAlertDialog;

            case Constantes.CONSERRORACTUALIZAR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.actgrabapedido_dlgerroractualizar))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        finish();
                                        startActivity(new Intent(ActPedidoFormulario.this, ActPedidoListaOnline.class));
                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.CONSDIALOGVERIFICARPEDIDONOCOBERTURA:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgbackformularioinicio))
                        .setPositiveButton(getString(R.string.actpedidofin_dlgbtnguardarpedido),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            subGrabar();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                        if (loBeanPedidoCab.isEnEdicion()) {
                                            startActivity(new Intent(ActPedidoFormulario.this, ActPedidoListaOnline.class));
                                        } else {
                                            Intent loIntent = new Intent(ActPedidoFormulario.this, ActClienteBuscar.class);
                                            finish();
                                            startActivity(loIntent);
                                        }

                                    }
                                })
                        .setNegativeButton(getString(R.string.actpedidofin_dlgbtncancelarpedido),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        if (loBeanPedidoCab.isEnEdicion()) {
                                            startActivity(new Intent(ActPedidoFormulario.this, ActPedidoListaOnline.class));
                                        } else {
                                            Intent loIntent = new Intent(ActPedidoFormulario.this, ActClienteBuscar.class);
                                            finish();
                                            startActivity(loIntent);
                                        }
                                    }
                                }).create();
                return loAlertDialog;

            default:
                return super.onCreateDialog(id);

        }

    }

    private void validarStock() {
        if (!UtilitarioData.fnObtenerPreferencesBoolean(ActPedidoFormulario.this, "pedidoPendiente")
                && !loBeanPedidoCab.isEnEdicion()) {
            if (isDescontarStock.equals(Configuracion.FLGVERDADERO))
                actualizarStock();
        }
    }

    public void irclientedetalle() {
        Intent loIntent = null;
        if (loBeanPedidoCab.isEnEdicion()) {
            loIntent = new Intent(ActPedidoFormulario.this, ActPedidoListaOnline.class);
        } else {
            loIntent = new Intent(this, ActClienteDetalle.class);
            loIntent.putExtra("IdCliente", Long.parseLong(loBeanPedidoCab.getClipk()));
        }
        finish();
        startActivity(loIntent);
    }

    private List<BeanEnvPedidoCtrl> convertToListaControles(List<BeanControl> lstControl) {
        try {
            if (lstControl == null)
                lstControl = new DalPedido(ActPedidoFormulario.this).fnSelControles(getIntent().getExtras().getInt(Configuracion.EXTRA_FORMULARIO));
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<BeanEnvPedidoCtrl> lista = new ArrayList<BeanEnvPedidoCtrl>();
        for (int i = 0; i < lstControl.size(); i++) {
            BeanEnvPedidoCtrl beanControl = new BeanEnvPedidoCtrl();
            beanControl.setIdControl(Integer.toString(lstControl.get(i).getCodigo()));
            beanControl.setEtiquetaControl(lstControl.get(i).getEtiquetaTexto());
            beanControl.setIdFormulario(Integer.toString(lstControl.get(i).getFormulario()));
            beanControl.setIdTipoControl(Integer.toString(lstControl.get(i).getTipo()));
            beanControl.setValorControl(lstControl.get(i).getValor());
            lista.add(beanControl);
        }
        return lista;
    }

    private void subGrabar() throws Exception {
        subGrabarValorActual();
        // Grabamos en la BD del equipo
        DalPedido loDalPedido = new DalPedido(this);

        if (loBeanPedidoCab.isEnEdicion()) {
            if (!loBeanPedidoCab.getCodigoPedidoServidor().equals("") && !loBeanPedidoCab.getCodigoPedidoServidor().equals("0"))
                loDalPedido.fnDeletePedidoXcodigoServidor(loBeanPedidoCab.getCodigoPedidoServidor());
            String codigoPedido = loDalPedido.fnGrabarPedido(loBeanPedidoCab);
            loBeanPedidoCab.setCodigoPedido(codigoPedido);
        } else {
            if (!loBeanPedidoCab.getCodigoPedido().equals("0") && !loBeanPedidoCab.getCodigoPedido().equals(""))
                loDalPedido.fnDeletePedido(loBeanPedidoCab.getCodigoPedido());
            String codPed = loDalPedido.fnGrabarPedido(loBeanPedidoCab);
            loBeanPedidoCab.setCodigoPedido(codPed);
        }

        BeanCliente loBeanCliente = ((AplicacionEntel) getApplication())
                .getCurrentCliente();

        // Actualizo el estado del currentcliente
        loBeanCliente.setEstado(Configuracion.ESTADO03CONPEDIDO);
        ((AplicacionEntel) getApplication()).setCurrentCliente(loBeanCliente);


        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);//pedido grabado en memoria
    }

    private void subGrabarValorActual() {

        // Obtenemos la fecha del sistema
        Calendar c = Calendar.getInstance();
        Date loDate = c.getTime();
        String sFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate)
                .toString();

        loBeanPedidoCab.setMontoTotal(Utilitario.fnRound(
                loBeanPedidoCab.calcularMontoTotal(),
                Integer.valueOf(isNumeroDecimales)));
        loBeanPedidoCab.setMontoTotalSoles(Utilitario.fnRound(
                loBeanPedidoCab.calcularMontoTotalSoles(), //@JBELVY
                Integer.valueOf(isNumeroDecimales)));
        loBeanPedidoCab.setMontoTotalDolar(Utilitario.fnRound(
                loBeanPedidoCab.calcularMontoTotalDolar(), //@JBELVY
                Integer.valueOf(isNumeroDecimales)));
        loBeanPedidoCab.setCantidadTotal(Utilitario.fnRound(loBeanPedidoCab
                .calcularCantTotalItems(), iiMaximoNumeroDecimales));
        loBeanPedidoCab.setBonificacionTotal(Utilitario.fnRound(loBeanPedidoCab
                .calcularBonificacionTotal(), iiMaximoNumeroDecimales));
        loBeanPedidoCab.setFleteTotal(loBeanPedidoCab
                .calcularTotalFlete());
        loBeanPedidoCab.setFechaFin(sFecha);

        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);

        // Se guarda GPS si tiene el flag habilitado
        Location location = null;

        try {
            location = Gps.getLocation();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (isGps
                .equals(Configuracion.FLGVERDADERO)
                && location != null) {
            loBeanPedidoCab.setLatitud(String.valueOf(location.getLatitude()));
            loBeanPedidoCab
                    .setLongitud(String.valueOf(location.getLongitude()));
            loBeanPedidoCab.setCelda(location.getProvider().substring(0, 1)
                    .toUpperCase());
        } else {
            loBeanPedidoCab.setLatitud("0");
            loBeanPedidoCab.setLongitud("0");
            loBeanPedidoCab.setCelda("-");
        }

        loBeanPedidoCab.setFlgEnCobertura(Utilitario.fnVerSignal(this) ? "1" : "0");
        loBeanPedidoCab.setFlgTerminado(Configuracion.FLGVERDADERO);

        String lsBeanVisitaCab = "";
        try {
            lsBeanVisitaCab = BeanMapper.toJson(loBeanPedidoCab, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        UtilitarioData.fnCrearPreferencesPutString(this, sharePreferenceBeanPedidoCab, lsBeanVisitaCab);
    }

    public void actualizarStock() {

        DalProducto loDalProducto = new DalProducto(this);
        DalAlmacen loDalAlmacen = new DalAlmacen(this);
        for (BeanEnvPedidoDet loBeanEnvPedidoDet : loBeanPedidoCab.getListaDetPedido()) {

            double ldStockFinal = 0;
            double ldStock = 0;
            double ldCantidad = 0;
            double ldBonificacion = 0;
            double ldFrac = 0;
            double ldBonificacionFrac = 0;


            ldStock = Double.parseDouble(Utilitario.fnRound(
                    loBeanEnvPedidoDet.getStock(), iiMaximoNumeroDecimales));

            if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                ldCantidad = Double.parseDouble(Utilitario.fnRound(loBeanEnvPedidoDet.getCantidad(), iiMaximoNumeroDecimales));
                ldCantidad *= Double.parseDouble(Utilitario.fnRound(loBeanEnvPedidoDet.getCantidadPre(), iiMaximoNumeroDecimales));
            } else {
                ldCantidad = Double.parseDouble(Utilitario.fnRound(loBeanEnvPedidoDet.getCantidad(), iiMaximoNumeroDecimales));
            }

            try {
                ldBonificacion = Double.parseDouble(Utilitario.fnRound(loBeanEnvPedidoDet.getBonificacion(), iiMaximoNumeroDecimales));
                if (isPresentacion.equals(Configuracion.FLGVERDADERO))
                    ldBonificacion *= Double.parseDouble(Utilitario.fnRound(loBeanEnvPedidoDet.getCantidadPre(), iiMaximoNumeroDecimales));
            } catch (Exception e) {
                ldBonificacion = 0;
            }

            try {
                ldFrac = Double.parseDouble(Utilitario.fnRound(loBeanEnvPedidoDet.getCantidadFrac(), iiMaximoNumeroDecimales));
            } catch (Exception e) {
                ldFrac = 0;
            }

            try {
                ldBonificacionFrac = Double.parseDouble(Utilitario.fnRound(loBeanEnvPedidoDet.getBonificacionFrac(), iiMaximoNumeroDecimales));
            } catch (Exception e) {
                ldBonificacionFrac = 0;
            }

            double stockFinal = ldStock - ldCantidad - ldBonificacion - ldFrac - ldBonificacionFrac;
            String psStockFinal = String.valueOf(Utilitario.fnRound(stockFinal, iiMaximoNumeroDecimales));


            //actualizar stock
            if ((isMostrarAlmacen.equals(Configuracion.FLGVERDADERO))) {
                if ((isPresentacion.equals(Configuracion.FLGVERDADERO))) {
                    loDalAlmacen.fnUpdPreStock(loBeanEnvPedidoDet.getCodAlmacen(),
                            loBeanEnvPedidoDet.getIdArticulo(), psStockFinal);
                } else {
                    loDalAlmacen.fnUpdStock(loBeanEnvPedidoDet.getCodAlmacen(),
                            loBeanEnvPedidoDet.getIdArticulo(), psStockFinal);
                }

            } else {
                if ((isPresentacion.equals(Configuracion.FLGVERDADERO))) {
                    loDalProducto.fnUpdStockPresentacion(loBeanEnvPedidoDet.getIdArticulo(),
                            psStockFinal);
                } else {
                    loDalProducto.fnUpdStockProducto(loBeanEnvPedidoDet.getIdArticulo(),
                            psStockFinal);
                }
            }
        }
    }

    public void enviarPedidoServer() {
        ((AplicacionEntel) getApplication()).uploadEvent(Configuracion.PEDIDO_EVENT);
        new HttpGrabaPedido(loBeanPedidoCab, this, fnTipactividad()).execute();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {

        if (isHttpVerificarPedido) {
            isHttpVerificarPedido = false;

            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {

                BeanRespVerificarPedidoGeneral general = ((AplicacionEntel) getApplication())
                        .getCurrentRespVerificarPedidoGeneral();

                if (general.getMensajes().get(0).getCodigo().equals("-1")) {
                    subMostrarVerificar(true);
                    showDialog(CONSBACKFORMULARIOINICIO);

                } else if (general.getMensajes().get(0).getCodigo().equals("0")) {
                    subMostrarVerificar(false);
                    String lsBeanPedidoCab = null;
                    loBeanPedidoCab.setListaDetPedido(general.mapperListaVerificarPedidoABeanEnvPedidoDetalle());
                    try {
                        lsBeanPedidoCab = BeanMapper.toJson(loBeanPedidoCab, false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    UtilitarioData.fnCrearPreferencesPutString(ActPedidoFormulario.this, "beanPedidoCabVerificarPedido", lsBeanPedidoCab);
                    UtilitarioData.fnCrearPreferencesPutString(ActPedidoFormulario.this, "MensajeVerificarPedido", "1");
                    startActivity(new Intent(this, ActProductoPedido.class));
                } else if (general.getMensajes().get(0).getCodigo().equals("1")) {
                    isHttpVerificarPedido = false;
                    subMostrarVerificar(false);
                }
            } else if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDORERROR &&
                    (getHttpResultado().equals(this.getString(R.string.msg_fueracobertura)) ||
                            (getHttpResultado().equals(this.getString(R.string.msg_modofueracobertura))))) {
                if (loBeanPedidoCab.isEnEdicion()) {//no se generan pendientes mediante edicion
                    DalPedido loDalPedido = null;
                    try {
                        loDalPedido = new DalPedido(this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    loDalPedido.fnUpdEstadoPedido(loBeanPedidoCab.getCodigoPedido());
                    showDialog(Constantes.CONSERRORACTUALIZAR);
                } else
                    showDialog(Constantes.CONSDIALOGVERIFICARPEDIDONOCOBERTURA);
            }

        } else {
            try {
                Intent intentl = new Intent(this, ActClienteDetalle.class);
                intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                if (estadoSincronizar) {
                    if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                        Intent loIntent = new Intent(this, ActMenu.class);

                        loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(loIntent);
                    }
                } else if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                    if (loBeanPedidoCab.getFlgEliminar().equals(Configuracion.FLGVERDADERO)) {
                        DalPedido loDalPedido = null;
                        try {
                            loDalPedido = new DalPedido(ActPedidoFormulario.this);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                        if (!loBeanPedidoCab.getCodigoPedido().equals("0")) {
                            loDalPedido.fnDelTrCabPedidoCodigo(loBeanPedidoCab.getCodigoPedido());
                        }
                        irclientedetalle();
                    } else {

                        // como grabo bien en el servidor, hago un update al registro
                        BeanUsuario loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);
                        DalPedido dalPedido = new DalPedido(this);
                        boolean lbResult = true;
                        boolean lbResultUpdCli = true;
                        if (UtilitarioData.fnObtenerPreferencesBoolean(ActPedidoFormulario.this, "pedidoPendiente")) {
                            lbResult = dalPedido.fnUpdEstadoPedidoPendiente(loBeanPedidoCab.getCodigoPedido(), loBeanUsuario.getCodVendedor());
                        } else {
                            lbResult = dalPedido.fnUpdEstadoPedido(loBeanPedidoCab.getCodigoPedido());
                        }

                        lbResultUpdCli = new DalCliente(this).fnUpdFecUltPed(
                                loBeanPedidoCab.getFechaFin(),
                                loBeanPedidoCab.getCodigoCliente(),
                                loBeanUsuario.getNombre());


                        if (lbResult && lbResultUpdCli) {
                            BeanCliente lobecli = ((AplicacionEntel) getApplication()).getCurrentCliente();
                            lobecli.setCodUsrUltPedido(loBeanUsuario.getNombre());
                            lobecli.setFecUltPedido(loBeanPedidoCab.getFechaFin());
                            ((AplicacionEntel) getApplication()).setCurrentCliente(lobecli);

                            UtilitarioData.fnCrearPreferencesPutBoolean(this, "editarFormularioInicio", false);

                            if (UtilitarioData.fnObtenerPreferencesBoolean(ActPedidoFormulario.this, "pedidoPendiente")) {
                                startActivity(new Intent(ActPedidoFormulario.this, ActListaPendientes.class));
                            } else {
                                startActivity(intentl);
                            }
                        } else {
                            if (loBeanPedidoCab.isEnEdicion()) {//no se generan pendientes mediante edicion
                                dalPedido.fnUpdEstadoPedido(loBeanPedidoCab.getCodigoPedido());
                                showDialog(Constantes.CONSERRORACTUALIZAR);
                            } else
                                showDialog(Constantes.ERRORPEDIDO);
                        }
                    }
                } else {
                    if (loBeanPedidoCab.getFlgEliminar().equals(Configuracion.FLGVERDADERO)) {
                        DalPedido loDalPedido = null;
                        try {
                            loDalPedido = new DalPedido(ActPedidoFormulario.this);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                        if (!loBeanPedidoCab.getCodigoPedido().equals("0")) {
                            loDalPedido.fnDelTrCabPedidoCodigo(loBeanPedidoCab.getCodigoPedido());
                        }
                        irclientedetalle();
                    } else {
                        if (loBeanPedidoCab.isEnEdicion()) {//no se generan pendientes mediante edicion
                            DalPedido loDalPedido = new DalPedido(this);
                            loDalPedido.fnUpdEstadoPedido(loBeanPedidoCab.getCodigoPedido());
                            showDialog(Constantes.CONSERRORACTUALIZAR);
                        } else
                            showDialog(Constantes.ERRORPEDIDO);
                    }
                }
            } catch (Exception e) {
                subShowException(e);
            }
        }
    }
}
