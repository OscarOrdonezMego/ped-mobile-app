package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import pe.entel.android.pedidos.util.Configuracion;


@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanUsuarioOnline {
    @JsonProperty
    private String serie = "";
    @JsonProperty
    private String correlativo = "";
    @JsonProperty
    private String resultado = "";
    @JsonProperty
    private int idResultado = 0;
    @JsonProperty
    private String flgNServicesObligatorio = Configuracion.FLGVERDADERO;

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(String correlativo) {
        this.correlativo = correlativo;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public int getIdResultado() {
        return idResultado;
    }

    public void setIdResultado(int idResultado) {
        this.idResultado = idResultado;
    }
}
