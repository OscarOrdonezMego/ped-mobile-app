package pe.entel.android.pedidos.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanResponseConsultaClienteOnline;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumUrl;

public class HttpClienteOnline extends HttpConexion {

    private BeanCliente ioBeanCliente;
    private Context ioContext;

    // CONSTRUCTOR
    public HttpClienteOnline(BeanCliente poBeanCliente, Context poContext, EnumTipActividad poEnumTipActividad) {
        super(poContext, poContext
                .getString(R.string.httpclienteonline_dlgbuscqueda), poEnumTipActividad);
        ioBeanCliente = poBeanCliente;
        ioContext = poContext;
    }

    @Override
    public boolean OnPreConnect() {
        SharedPreferences loSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ioContext);
        if (loSharedPreferences.getBoolean(ioContext.getString(R.string.keypre_cobertura), false)) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_modofueracobertura));
            return false;
        }

        if (!Utilitario.fnVerSignal(ioContext)) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            BeanResponseConsultaClienteOnline loBeanCliente = (BeanResponseConsultaClienteOnline) getHttpResponseObject();
            int liIdHttprespuesta = loBeanCliente.getIdResultado();
            String lsHttpRespuesta = loBeanCliente.getResultado();

            if (liIdHttprespuesta == ConfiguracionNextel.CONSRESSERVIDOROK) {
                String lsBeanCliente = "";
                try {
                    lsBeanCliente = BeanMapper.toJson(loBeanCliente, false);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                SharedPreferences loSharedPreferences = ioContext.getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
                SharedPreferences.Editor loEditor = loSharedPreferences.edit();
                loEditor.putString("BeanClienteOnline", lsBeanCliente);

                loEditor.commit();
            }
            setHttpResponseIdMessage(liIdHttprespuesta, lsHttpRespuesta);
        }
    }

    public void subConHttp() {
        try {
            //RestTemplate restTemplate = new RestTemplate();
            //restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            Log.v("URL", Configuracion.fnUrl(ioContext, EnumUrl.CLIENTEONLINE));
            Log.v("URL", BeanMapper.toJson(ioBeanCliente, false));

            // Se conecta al Servidor
            //String lsObjeto = restTemplate.postForObject(Configuracion.fnUrl(ioContext, EnumUrl.CLIENTEONLINE), ioBeanCliente, String.class);
            String lsObjeto = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, EnumUrl.CLIENTEONLINE), ioBeanCliente);
            Log.v("RESPONSE", BeanMapper.toJson(lsObjeto, true));
            Log.v("RESPONSE", lsObjeto);

            BeanResponseConsultaClienteOnline loBeanResponse = (BeanResponseConsultaClienteOnline) BeanMapper.fromJson(lsObjeto, BeanResponseConsultaClienteOnline.class);
            setHttpResponseObject(loBeanResponse);
        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, "Error HTTP: " + e.getMessage());
        }
    }
}