package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import greendroid.widget.ActionBarItem;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.util.Gps;
import pe.com.nextel.android.util.Logger;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanEnvCanjeCab;
import pe.entel.android.pedidos.bean.BeanEnvDevolucionCab;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanGeneral;
import pe.entel.android.pedidos.bean.BeanKPI;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalCanje;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalDevolucion;
import pe.entel.android.pedidos.dal.DalGeneral;
import pe.entel.android.pedidos.dal.DalKPI;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.http.HttpGrabaCanje;
import pe.entel.android.pedidos.http.HttpGrabaDevolucion;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

public class ActPedidoItemFin extends NexActivity implements NexMenuCallbacks,
        MenuSlidingListener {
    private TextView txtcantproductos, txttotalitems, txtcodigo, txtnombre;
    private Button btnenviar;

    BeanUsuario loBeanUsuario = null;
    private AdapMenuPrincipalSliding _menu;

    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDASINCRONIZACION = 11;
    private final int ENVIOPENDIENTES = 12;
    private final int VALIDASINCRO = 13;
    private final int NOHAYPENDIENTES = 14;
    private final int ERRORPEDIDO = 15;

    boolean estadoSincronizar = false;

    BeanEnvCanjeCab loBeanCanjeCab = null;
    BeanEnvDevolucionCab loBeanDevoCab = null;

    StringBuffer loStb1 = null;

    @SuppressWarnings("unused")
    private LinkedList<BeanGeneral> ioLista;

    @SuppressWarnings("unused")
    private DalGeneral loDalGeneral = new DalGeneral(this);

    private final int CONSGRABAR = 1;
    private final int CONSVALIDAR = 2;

    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro

    //PARAMETROS DE CONFIGURACION
    private String isGps;

    private String isMaximoNumeroDecimales;
    private int iiMaximoNumeroDecimales;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logger.d("XXX", "ActPedidoItemFin");
        ValidacionServicioUtil.validarServicio(this);
        String lsBeanUsuario = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario,
                BeanUsuario.class);

        isGps = Configuracion.EnumConfiguracion.GPS.getValor(this);
        isMaximoNumeroDecimales = Configuracion.EnumConfiguracion.MAXIMO_NUMERODECIMALES.getValor(this);
        setMaximoNumeroDecimales();
        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);

        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));

        subIniMenu();
    }

    private void setMaximoNumeroDecimales() {
        iiMaximoNumeroDecimales = 0;
        try {
            iiMaximoNumeroDecimales = Integer.parseInt(isMaximoNumeroDecimales);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ActProductoDetalle", "Error conversion maximo numero de decimales: " + e.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        Intent loIntent1 = new Intent(this, ActProductoItemLista.class);
        startActivity(loIntent1);
    }

    public void subIniActionBar() {
        int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();
        if (flujo == Configuracion.CONSCANJE)
            this.getActionBarGD().setTitle(
                    getString(R.string.actpedidoitemfin_bardetalleCanje));
        else
            this.getActionBarGD().setTitle(
                    getString(R.string.actpedidoitemfin_bardetalleDevolucion));

    }

    @Override
    public void subSetControles() {
        setActionBarContentView(R.layout.pedidoitemfin2);
        super.subSetControles();

        txtcantproductos = (TextView) findViewById(R.id.actpedidoitemfin_txtcantproductos);
        txttotalitems = (TextView) findViewById(R.id.actpedidoitemfin_txttotalitems);
        txtcodigo = (TextView) findViewById(R.id.actpedidoitemfin_txtcodigo);
        txtnombre = (TextView) findViewById(R.id.actpedidoitemfin_txtnombre);
        btnenviar = (Button) findViewById(R.id.actpedidoitemfin_btnenviar);

        subCargaDetalle();

        btnenviar.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                int flujo = ((AplicacionEntel) getApplication())
                        .getCurrentCodFlujo();
                if (flujo == Configuracion.CONSCANJE) {
                    if (loBeanCanjeCab.getLstCanjeDet().size() <= 0) {
                        showDialog(CONSVALIDAR);
                    } else {
                        showDialog(CONSGRABAR);
                    }
                } else {
                    if (loBeanDevoCab.getLstDevolucionDet().size() <= 0) {
                        showDialog(CONSVALIDAR);
                    } else {
                        showDialog(CONSGRABAR);
                    }
                }

            }
        });
    }

    private void subCargaDetalle() {
        int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();

        BeanCliente lobecli = ((AplicacionEntel) getApplication())
                .getCurrentCliente();

        loStb1 = new StringBuffer();

        loStb1.append(" Codigo : ").append(lobecli.getClicod()).append('\n');
        loStb1.append(" Nombre : ").append(lobecli.getClinom()).append('\n');

        if (flujo == Configuracion.CONSCANJE) {
            loBeanCanjeCab = ((AplicacionEntel) getApplication())
                    .getCurrentCanje();

            txtcantproductos.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.icono_canje_vistagrande, 0, 0, 0);

            double totalItems = Double.valueOf(loBeanCanjeCab
                    .calcularCantTotalItems());
            txttotalitems.setText(Utilitario.fnRound(totalItems, iiMaximoNumeroDecimales));

            txtcantproductos.setText(String.valueOf(loBeanCanjeCab
                    .getLstCanjeDet().size()));
            loStb1.append(" Items Canje : ")
                    .append(loBeanCanjeCab.getLstCanjeDet().size())
                    .append('\n');

        } else {
            txtcantproductos.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.icono_devolucion_vistagrande, 0, 0, 0);
            loBeanDevoCab = ((AplicacionEntel) getApplication())
                    .getCurrentDevolucion();

            double totalItems = Double.valueOf(loBeanDevoCab
                    .calcularCantTotalItems());
            txttotalitems.setText(Utilitario.fnRound(totalItems, iiMaximoNumeroDecimales));

            txtcantproductos.setText(String.valueOf(loBeanDevoCab
                    .getLstDevolucionDet().size()));
            loStb1.append(" Items Devolucion : ")
                    .append(loBeanDevoCab.getLstDevolucionDet().size())
                    .append('\n');
        }

        txtcodigo.setText(lobecli.getClicod());
        txtnombre.setText(lobecli.getClinom());
    }


    private void subGrabar() throws Exception {
        // Obtenemos la fecha del sistema
        Calendar c = Calendar.getInstance();
        Date loDate = c.getTime();
        String sFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate)
                .toString();

        int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();
        if (flujo == Configuracion.CONSCANJE) {
            loBeanCanjeCab.setFechaMovil(sFecha);

            loBeanCanjeCab.setIdUsuario(loBeanUsuario.getCodVendedor());
            loBeanCanjeCab.setIdCliente(((AplicacionEntel) getApplication())
                    .getCurrentCliente().getClicod());
            // Se guarda GPS si tiene el flag habilitado
            Location location = null;

            try {
                location = Gps.getLocation();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (isGps.equals(
                    Configuracion.FLGVERDADERO)
                    && location != null) {
                loBeanCanjeCab
                        .setLatitud(String.valueOf(location.getLatitude()));
                loBeanCanjeCab.setLongitud(String.valueOf(location
                        .getLongitude()));
                loBeanCanjeCab.setCelda(location.getProvider().substring(0, 1)
                        .toUpperCase());
            } else {
                loBeanCanjeCab.setLatitud("0");
                loBeanCanjeCab.setLongitud("0");
                loBeanCanjeCab.setCelda("-");
            }

            loBeanCanjeCab.setFlgEnCobertura(Utilitario.fnVerSignal(this) ? "1" : "0");

            // Grabamos en la BD del equipo
            DalCanje loDalPedido = new DalCanje(this);
            String codPed = loDalPedido.fnGrabarCanje(loBeanCanjeCab);
            loBeanCanjeCab.setCanpk(codPed);
        } else {
            loBeanDevoCab.setFechaMovil(sFecha);
            loBeanDevoCab.setIdUsuario(loBeanUsuario.getCodVendedor());
            loBeanDevoCab.setIdCliente(((AplicacionEntel) getApplication())
                    .getCurrentCliente().getClicod());
            // Se guarda GPS si tiene el flag habilitado

            Location location = null;

            try {
                location = Gps.getLocation();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (isGps.equals(
                    Configuracion.FLGVERDADERO)
                    && location != null) {
                loBeanDevoCab
                        .setLatitud(String.valueOf(location.getLatitude()));
                loBeanDevoCab.setLongitud(String.valueOf(location
                        .getLongitude()));
                loBeanDevoCab.setCelda(location.getProvider().substring(0, 1)
                        .toUpperCase());
            } else {
                loBeanDevoCab.setLatitud("0");
                loBeanDevoCab.setLongitud("0");
                loBeanDevoCab.setCelda("-");
            }

            loBeanDevoCab.setFlgEnCobertura(Utilitario.fnVerSignal(this) ? "1" : "0");

            // Grabamos en la BD del equipo
            DalDevolucion loDalPedido = new DalDevolucion(this);
            String codPed = loDalPedido.fnGrabarDevolucion(loBeanDevoCab);
            loBeanDevoCab.setDevopk(codPed);
        }

        BeanKPI kpi = ((AplicacionEntel) getApplication()).getCurrentKpi();
        // Actualizamos estado
        if (((AplicacionEntel) getApplication()).getClienteOnline() == 0) {
            DalCliente loDalCliente = new DalCliente(this);
            // Actualizo estado del cliente en base de datos interna
            if (((AplicacionEntel) getApplication()).getCurrentCliente()
                    .getEstado().equals(Configuracion.ESTADO01PORVISITAR)) {
                loDalCliente.fnUpdEstadoVisitaCliente(Long
                                .parseLong(((AplicacionEntel) getApplication())
                                        .getCurrentCliente().getClipk()),
                        Configuracion.ESTADO02VISITADO);
                // Un cliente mas visitado
                kpi.numCliVis += 1;
                ((AplicacionEntel) getApplication()).setCurrentKpi(kpi);
            }

            // Se actualiza bean KPI
            DalKPI lodkpi = new DalKPI(this);
            lodkpi.fnUpdKPI(((AplicacionEntel) getApplication())
                    .getCurrentKpi());
        }
        // Actualizo estado del currentcliente
        if (((AplicacionEntel) getApplication()).getCurrentCliente()
                .getEstado().equals(Configuracion.ESTADO01PORVISITAR)) {
            BeanCliente cliente = ((AplicacionEntel) getApplication())
                    .getCurrentCliente();

            cliente.setEstado(Configuracion.ESTADO02VISITADO);
            ((AplicacionEntel) getApplication()).setCurrentCliente(cliente);

        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        switch (item.getItemId()) {

            case CONSGRABAR:
                int flujo = ((AplicacionEntel) getApplication())
                        .getCurrentCodFlujo();
                if (flujo == Configuracion.CONSCANJE) {
                    if (loBeanCanjeCab.getLstCanjeDet().size() <= 0) {
                        showDialog(CONSVALIDAR);
                    } else {
                        showDialog(CONSGRABAR);
                    }
                } else {
                    if (loBeanDevoCab.getLstDevolucionDet().size() <= 0) {
                        showDialog(CONSVALIDAR);
                    } else {
                        showDialog(CONSGRABAR);
                    }
                }

                break;

            default:
                return super.onHandleActionBarItemClick(item, position);
        }
        return true;
    }

    protected void NexShowDialog(int id) {
        DialogFragmentYesNo loDialog;
        switch (id) {
            case CONSGRABAR:
                Calendar c = Calendar.getInstance();
                Date loDate = c.getTime();
                final String sFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss",
                        loDate).toString();

                StringBuffer loStb = new StringBuffer();
                String titulo;
                int flujo = ((AplicacionEntel) getApplication())
                        .getCurrentCodFlujo();
                if (flujo == Configuracion.CONSCANJE) {
                    loStb.append(getString(R.string.actpedidoitemfin_dlgcanjefin))
                            .append('\n');
                    titulo = getString(R.string.actpedidoitemfin_bardetalleCanje);
                } else {
                    loStb.append(getString(R.string.actpedidoitemfin_dlgdevofin))
                            .append('\n');
                    titulo = getString(R.string.actpedidoitemfin_bardetalleDevolucion);
                }
                loStb.append(loStb1.toString());
                loStb.append(sFecha).append('\n');

                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSGRABAR"), titulo,
                        loStb.toString(), R.drawable.boton_ayuda,
                        EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSGRABAR")) == DialogFragmentYesNo.YES) {
                    try {
                        subGrabar();
                        enviarPedidoServer();
                    } catch (Exception e) {
                        subShowException(e);
                    }
                }

                break;

            case CONSVALIDAR:

                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSVALIDAR"),
                        getString(R.string.actpedidofin_bardetalle),
                        getString(R.string.actpedidofin_msnvalidacion),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSVALIDAR"));

                break;

            case CONSDIASINCRONIZAR:// SINCRONIZAR

                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.actmenu_dlgsincronizar),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR")) == DialogFragmentYesNo.YES) {
                    try {
                        String lsBeanUsuario = getSharedPreferences("Actual",
                                MODE_PRIVATE).getString("BeanUsuario", "");
                        BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                .fromJson(lsBeanUsuario, BeanUsuario.class);
                        estadoSincronizar = true;
                        new HttpSincronizar(loBeanUsuario.getCodVendedor(),
                                ActPedidoItemFin.this, fnTipactividad()).execute();
                    } catch (Exception e) {
                        subShowException(e);
                    }
                }

                break;

            case VALIDASINCRONIZACION:

                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"),
                        getString(R.string.actmenuvalsincro_titulo),
                        getString(R.string.actmenuvalsincro_mensaje),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"));

                break;

            case VALIDASINCRO:

                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                        getString(R.string.msgregpendientesenvinfo),
                        R.drawable.boton_ayuda, false, EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"));

                break;

            case ENVIOPENDIENTES:

                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"),
                        getString(R.string.msgdeseaenviarpendientes),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES")) == DialogFragmentYesNo.YES) {
                    Intent loIntentEspera = new Intent(ActPedidoItemFin.this,
                            ActGrabaPendiente.class);
                    startActivity(loIntentEspera);
                }

                break;

        }
    }

    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog;
        switch (id) {
            case ERRORPEDIDO:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.dlg_titerror))
                        .setMessage(getString(R.string.actgrabapedido_dlgerrorpedido))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        Intent intentl = new Intent(ActPedidoItemFin.this, ActClienteDetalle.class);
                                        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intentl);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSGRABAR:
                Calendar c = Calendar.getInstance();
                Date loDate = c.getTime();
                final String sFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss",
                        loDate).toString();

                StringBuffer loStb = new StringBuffer();
                String titulo;
                int flujo = ((AplicacionEntel) getApplication())
                        .getCurrentCodFlujo();
                if (flujo == Configuracion.CONSCANJE) {
                    loStb.append(getString(R.string.actpedidoitemfin_dlgcanjefin))
                            .append('\n');
                    titulo = getString(R.string.actpedidoitemfin_bardetalleCanje);
                } else {
                    loStb.append(getString(R.string.actpedidoitemfin_dlgdevofin))
                            .append('\n');
                    titulo = getString(R.string.actpedidoitemfin_bardetalleDevolucion);
                }
                loStb.append(loStb1.toString());
                loStb.append(sFecha).append('\n');

                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(titulo)
                        .setMessage(loStb.toString())
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            subGrabar();
                                            enviarPedidoServer();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btncancelar),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        return;
                                    }
                                })

                        .create();
                return loAlertDialog;

            case CONSVALIDAR:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actpedidofin_bardetalle))
                        .setMessage(getString(R.string.actpedidofin_msnvalidacion))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences(
                                                    "Actual", MODE_PRIVATE)
                                                    .getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                                    .fromJson(lsBeanUsuario,
                                                            BeanUsuario.class);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActPedidoItemFin.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActPedidoItemFin.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    @SuppressWarnings("unused")
    private void direccionarFlujo() {
        new NexToast(this, R.string.actpedidofin_msnpedidograbado,
                EnumType.ACCEPT).show();
        Intent intentF = new Intent(this, ActClienteDetalle.class);
        intentF.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intentF);
    }

    public void enviarPedidoServer() {
        Log.v("XXX", "AHORA GRABO EN SERVIDOR");
        int flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();
        if (flujo == Configuracion.CONSCANJE) {
            new HttpGrabaCanje(loBeanCanjeCab, this, fnTipactividad())
                    .execute();
        } else {
            new HttpGrabaDevolucion(loBeanDevoCab, this, fnTipactividad())
                    .execute();
        }
        Log.v("XXX", "YAAAAA GRABO EN SERVIDOR");
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {
        try {
            Intent intentl = new Intent(this, ActClienteDetalle.class);
            intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            boolean lbResult = false;
            if (estadoSincronizar) {
                if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                    Intent loIntent = new Intent(this, ActMenu.class);

                    loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loIntent);
                }
            }
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                // como grabo bien en el servidor, hago un update al registro
                int flujo = ((AplicacionEntel) getApplication())
                        .getCurrentCodFlujo();
                if (flujo == Configuracion.CONSCANJE) {
                    DalCanje loDalCanje = new DalCanje(this);
                    lbResult = loDalCanje.fnUpdEstadoCanje(loBeanCanjeCab
                            .getCanpk());
                } else {
                    DalDevolucion loDalCanje = new DalDevolucion(this);
                    lbResult = loDalCanje.fnUpdEstadoDevolucion(loBeanDevoCab
                            .getDevopk());

                    loDalCanje.fnValidarStock(loBeanDevoCab.getLstDevolucionDet());

                }

                if (lbResult) {
                    startActivity(intentl);
                } else {
                    showDialog(ERRORPEDIDO);

                }

            } else {
                showDialog(ERRORPEDIDO);

            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent loIntentLogin = new Intent(ActPedidoItemFin.this,
                        ActLogin.class);
                loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(loIntentLogin);
            }
        });

    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        Intent intentl = new Intent(this, ActMenu.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intentl);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @SuppressWarnings("deprecation")
    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = null;
            DalPedido loDalPedido = new DalPedido(this);
            loList = loDalPedido.fnEnvioPedidosPendientes(loBeanUsuario
                    .getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);

                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();

                showDialog(CONSDIASINCRONIZAR);

            } else {

                showDialog(VALIDASINCRO);

            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub

        String lsBeanUsuario = getSharedPreferences(
                "Actual", MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper
                .fromJson(lsBeanUsuario,
                        BeanUsuario.class);
        this.setMsgOk("Eficiencia Online");

        BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
        loBeanListaEficienciaOnline.setIdResultado(0);
        loBeanListaEficienciaOnline.setResultado("");
        loBeanListaEficienciaOnline.setListaEficiencia(null);
        loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

        new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

        Intent loInstent = new Intent(this, ActKpiDetalle.class);
        loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loInstent);
    }

    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        ((AplicacionEntel) getApplication()).setCurrentCliente(null);
        ((AplicacionEntel) getApplication())
                .setCurrentCodFlujo(Configuracion.CONSSTOCK);
        ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
        Intent loIntento = new Intent(ActPedidoItemFin.this,
                ActProductoListaOnline.class);
        loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        BeanExtras loBeanExtras = new BeanExtras();
        loBeanExtras.setFiltro("");
        loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
        String lsFiltro = "";
        try {
            lsFiltro = BeanMapper.toJson(loBeanExtras, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
        startActivity(loIntento);
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(
                    getString(R.string.ml_sincronizarantes)));
        }

    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */
    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */
    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        try {
            DialogFragmentYesNo loDialog;
            switch (id) {
                case DIALOG_NSERVICES_DOWNLOAD:
                    String lsNServices;
                    if (ioDALNservices == null)
                        ioDALNservices = DALNServices.getInstance(this,
                                R.string.db_name);
                    try {
                        lsNServices = ioDALNservices.fnSelNServicesLabel();
                    } catch (Exception e) {
                        Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                e);
                        lsNServices = getString(R.string.nservices_label_default);
                    }
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG
                                    + DIALOG_NSERVICES_DOWNLOAD,
                            getString(R.string.nservices_consultingdownlad)
                                    .replace("{0}", lsNServices),
                            EnumLayoutResource.getDefaultIconHelp(this));
                    return new DialogFragmentYesNoPairListener(loDialog,
                            new OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(
                                        DialogFragmentYesNo dialog) {
                                    try {
                                        subIniNServicesDownload(ioDALNservices
                                                .fnSelNServicesAPK());
                                    } catch (Exception e) {
                                        subShowException(e);
                                    }
                                }

                                @Override
                                public void performButtonNo(
                                        DialogFragmentYesNo dialog) {
                                }
                            });

                default:
                    return super.onCreateNexDialog(id);
            }
        } catch (Exception e) {
            subShowException(e);
            return super.onCreateNexDialog(id);
        }
    }

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

}