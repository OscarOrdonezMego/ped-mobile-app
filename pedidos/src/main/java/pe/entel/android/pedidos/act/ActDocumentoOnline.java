package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.actividad.NexInvalidOperatorActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.util.Gps;
import pe.com.nextel.android.util.Logger;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanConsulta;
import pe.entel.android.pedidos.bean.BeanEnvDocumento;
import pe.entel.android.pedidos.bean.BeanGeneral;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanPagoOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalCobranza;
import pe.entel.android.pedidos.dal.DalGeneral;
import pe.entel.android.pedidos.http.HttpConsultaPagoOnline;
import pe.entel.android.pedidos.http.HttpGrabaDocumento;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;
import pe.entel.android.pedidos.widget.ListaPopup;
import pe.entel.android.pedidos.widget.ListaPopup.OnItemClickPopup;

public class ActDocumentoOnline extends NexActivity implements OnClickListener, NexMenuCallbacks, MenuSlidingListener, OnItemClickPopup {

    private TextView txtCorrelativo, lblCorrelativo, txtPagoCliente,
            txtPagoFechaHora, txtPagoMontoSoles, txtPagoMontoDolares;

    private EditText txtRangoCorrelativo;

    private Button spiMotivo, btnGenerar;

    private LinearLayout ioRowlyCorrelavito, ioRowlyRangoCorrelavito,
            ioRowlyNombreCliente, ioRowlyFechaPago, ioRowlyMontoSoles, ioRowlyMontoDolares;

    private  View ioLinviewNombreCliente, ioLinviewFechaHora, ioLinviewMsoles, ioLinviewMdolares;
    
    TextWatcher textWatcherCorrelativo;

    private final int CONSBACK = 3;

    private AdapMenuPrincipalSliding _menu;

    private BeanUsuario loBeanUsuario = null;
    private String isNumeroDecimales;
    private String flgNumDecVista;
    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDAHOME = 11;
    private final int VALIDASINCRONIZACION = 12;
    private final int ENVIOPENDIENTES = 13;
    private final int VALIDASINCRO = 14;
    private final int CONSSALIR = 15;
    private final int NOHAYPENDIENTES = 16;
    private final int ERRORGENERAR = 17;
    public int RESULTHTTP = 1;

    boolean estadoSincronizar = false;
    boolean tipodocumento = false;
    private String flgGps;

    int estadoSalir = 0;
    String numeroserie = "";
    long numerocorrelativo = 0;

    // Integracion Ntrack
    private DALNServices ioDALNservices;

    private final int POPUP_MOTIVO = 1;
    private DalGeneral loDalGeneral;
    private List ioLista;
    static BeanGeneral motivo;
    public static boolean showPopupMot = false;

    BeanEnvDocumento loBeanDocumento = null;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logger.d("XXX", "ActDocumentoOnline");
        ValidacionServicioUtil.validarServicio(this);
        isNumeroDecimales = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(this);
        flgNumDecVista = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(this);
        String lsBeanUsuario = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario, BeanUsuario.class);
        flgGps = Configuracion.EnumConfiguracion.GPS.getValor(this);
        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);
        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));
        subIniMenu();
    }


    @Override
    public void setOnItemClickPopup(Object poObject, int piTipo) {
        // TODO Auto-generated method stub

        switch (piTipo) {
            case POPUP_MOTIVO:
                motivo = (BeanGeneral) poObject;
                spiMotivo.setText(motivo.getDescripcion());
                showPopupMot = false;
                break;
            default:
                break;
        }
    }

    @Override
    public void subSetControles() {
        setActionBarContentView(R.layout.clientedocumentomotivo);
        super.subSetControles();
        txtCorrelativo = findViewById(R.id.actdocumentocorrelativo_txtCorrelativo);
        lblCorrelativo = findViewById(R.id.actdocumentocorrelativo_lblCorrelativo);
        txtRangoCorrelativo = findViewById(R.id.actdocumentocorrelativo_txtNroRangoEditar);
        txtPagoCliente = findViewById(R.id.actdocumentocpago_txtCliente);
        txtPagoFechaHora = findViewById(R.id.actdocumentocpago_txtFechaHora);
        txtPagoMontoSoles = findViewById(R.id.actdocumentocpago_txtMontoSoles);
        txtPagoMontoDolares = findViewById(R.id.actdocumentocpago_txtMontoDolares);
        spiMotivo = findViewById(R.id.actdocumento_cmbmotivo);
        btnGenerar = findViewById(R.id.actdocumento_btnfinalizar);
        ioRowlyCorrelavito = findViewById(R.id.actdocumento_lycorrelativo);
        ioRowlyRangoCorrelavito = findViewById(R.id.actdocumento_lyrangocorrelativo);
        ioRowlyNombreCliente = findViewById(R.id.actdocumento_lydatoscliente);
        ioLinviewNombreCliente = findViewById(R.id.actdocumento_viewdatoscliente);
        ioLinviewFechaHora = findViewById(R.id.actdocumento_viewfechahora);
        ioLinviewMsoles = findViewById(R.id.actdocumento_viewmsoles);
        ioLinviewMdolares = findViewById(R.id.actdocumento_viewmsoles);
        
        ioRowlyFechaPago = findViewById(R.id.actdocumento_lydfechapago);
        ioRowlyMontoSoles = findViewById(R.id.actdocumento_lydmontosoles);
        ioRowlyMontoDolares = findViewById(R.id.actdocumento_lydmontodolares);
        subCargaDetalle();

        textWatcherCorrelativo = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void afterTextChanged(Editable s) {
                long rangocorr;
                try {
                    rangocorr = Long.parseLong(txtRangoCorrelativo.getText().toString());
                } catch (Exception e) {
                    rangocorr = 0;
                }
                try {
                    if(rangocorr < numerocorrelativo){
                        Toast.makeText(getApplicationContext(),
                                "Ingrese un correlativo mayor o igual al actual",
                                Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {

                }
            }
        };

        txtRangoCorrelativo.addTextChangedListener(textWatcherCorrelativo);
        spiMotivo.setOnClickListener(this);

        btnGenerar.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                grabar();
            }
        });
    }

    @SuppressWarnings("unused")
    private void subCargaDetalle() {
        Bundle loExtras = getIntent().getExtras();
        loBeanDocumento = ((AplicacionEntel) getApplication()).getCurrentDocumento();

        if(loBeanDocumento.getIdDocTipo().equals("C")) {
            String correlativo = loBeanDocumento.getCorrelativo();
            numeroserie = loBeanDocumento.getSerie();
            Long corr;
            if (correlativo.equals("0")) {
                Integer newCorr = Integer.parseInt(correlativo);
                correlativo = String.format("%010d", (newCorr));
                txtCorrelativo.setText(correlativo);
                corr = (long) 1;
            } else {
                txtCorrelativo.setText(correlativo);
                corr = Long.parseLong(correlativo);
            }
            txtRangoCorrelativo.setText(corr.toString());
            numerocorrelativo = corr;
            ioRowlyNombreCliente.setVisibility(View.GONE);
            ioRowlyFechaPago.setVisibility(View.GONE);
            ioRowlyMontoSoles.setVisibility(View.GONE);
            ioRowlyMontoDolares.setVisibility(View.GONE);
            ioLinviewNombreCliente.setVisibility(View.GONE);
            ioLinviewFechaHora.setVisibility(View.GONE);
            ioLinviewMsoles.setVisibility(View.GONE);
            ioLinviewMdolares.setVisibility(View.GONE);
            txtCorrelativo.setGravity(Gravity.CENTER);

        }else{
            lblCorrelativo.setText(R.string.actcobranzamonto_txtnrorecibo);
            ioRowlyRangoCorrelavito.setVisibility(View.GONE);
            txtCorrelativo.setText(loBeanDocumento.getSerie() + " - " + loBeanDocumento.getCorrelativo());
            txtPagoCliente.setText(loBeanDocumento.getCliente());
            txtPagoFechaHora.setText(loBeanDocumento.getFechaPago());
            txtPagoMontoSoles.setText(loBeanDocumento.getMontoSoles());
            txtPagoMontoDolares.setText(loBeanDocumento.getMontoDolares());
            if(Double.parseDouble(loBeanDocumento.getMontoSoles()) <= 0){
                ioRowlyMontoSoles.setVisibility(View.GONE);
                ioLinviewMsoles.setVisibility(View.GONE);
            }
            if(Double.parseDouble(loBeanDocumento.getMontoDolares()) <= 0){
                ioRowlyMontoDolares.setVisibility(View.GONE);
                ioLinviewMdolares.setVisibility(View.GONE);
            }
        }
        this.getActionBarGD().setTitle(getString(loBeanDocumento.getIdDocTipo().equals("C") ? R.string.actcllientecorrelativo_editarcorrelativo : R.string.actdocumentocorrelativo_anularcobranza ));
    }

    @Override
    public void subIniActionBar() {

    }

    @Override
    public void onClick(View arg0) {
        if (arg0 == spiMotivo) {
            if (ioLista == null) {
                loDalGeneral = new DalGeneral(this);
                ioLista = loDalGeneral.fnSelGeneral((loBeanDocumento.getIdDocTipo().equals("C") ? Configuracion.GENERAL9MOTCORRELATIVO : Configuracion.GENERAL8MOTANULA));
            }
            showPopupMot = true;

            new ListaPopup().dialog(ActDocumentoOnline.this, ioLista,
                    POPUP_MOTIVO, ActDocumentoOnline.this,
                    getString(loBeanDocumento.getIdDocTipo().equals("C") ? R.string.listapopup_txtseleccionemotivocorrelativo : R.string.listapopup_txtseleccionemotivoanular), motivo,
                    R.attr.PedidosPopupLista);
        }
    }
    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {

        if (estadoSincronizar) {
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActMenu.class);

                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }
        } else {
            try {
                if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK
                        || getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {
                    if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                        if(RESULTHTTP == 1 && loBeanDocumento.getIdDocTipo().equals("C")) {
                            setResult(RESULT_OK);
                            finish();
                        } else if(loBeanDocumento.getIdDocTipo().equals("P")) {
                            UpdateListPagos();
                        }
                    }
                    else if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG && RESULTHTTP == 2) {
                        setResult(RESULT_OK);
                        finish();
                    }
                    else {
                        showDialog(ERRORGENERAR);
                    }
                }
            } catch (Exception e) {
                subShowException(e);
            }
        }
    }

    public static String ParseFecha(String fecha)
    {
        SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(fecha);
        }
        catch (ParseException ex)
        {
            System.out.println(ex);
        }
        return DateFormat.format("yyyyMMdd", fechaDate).toString();
    }

    public void enviarDocumentoServer() {
        ((AplicacionEntel) getApplication()).uploadEvent(Configuracion.DOCUMENTO_EVENT);
        new HttpGrabaDocumento(loBeanDocumento, this, fnTipactividad()).execute();
    }

    private void UpdateListPagos(){
        DalCobranza loDalCobranza = new DalCobranza(this);
        DalCliente loDalCliente = new DalCliente(this);
        BeanCliente loBeanCliente = null;
        BeanPagoOnline pagoOnline = loBeanDocumento.getPagoOnline();
        loBeanCliente = loDalCliente.fnSelxCodCliente(loBeanDocumento.getIdCliente());

        String pagoanulasoles = pagoOnline.getMonto_pagado_fecha();
        if(Double.parseDouble(pagoanulasoles) > 0) {
            Double totald = Double.parseDouble(loBeanCliente.getClideuda());
            String saldosoles = pagoOnline.getMonto_saldo();
            String totalsoles = pagoOnline.getMonto_total();
            double montopagadosoles = (Double.parseDouble(totalsoles) - ((Double.parseDouble(pagoanulasoles) + Double.parseDouble(saldosoles))));
            double nuevosaldosoles = (Double.parseDouble(pagoanulasoles) + Double.parseDouble(saldosoles));
            totald += Double.parseDouble(pagoanulasoles);
            loDalCliente.fnUpdDeudaCliente(Long.parseLong(loBeanCliente.getClipk()), Utilitario.fnRound(totald, Configuracion.MAXDECIMALMONTO), "1");

            loDalCobranza.fnUpdCobranzaSoles(pagoOnline.getCobranzaPK(),
                    Utilitario.fnRound(montopagadosoles, Integer.valueOf(isNumeroDecimales)),
                    Utilitario.fnRound(nuevosaldosoles, Integer.valueOf(isNumeroDecimales)));
        }

        String pagoanuladolares = pagoOnline.getMonto_pagado_fechadolares();
        if(Double.parseDouble(pagoanuladolares) > 0) {
            Double totald = Double.parseDouble(loBeanCliente.getClideudadolares());
            double montopagadodolares = 0;
            double nuevosaldodolares = 0;
            String saldodolares = pagoOnline.getMonto_saldodolares();
            String totaldolares = pagoOnline.getMonto_totaldolares();
            montopagadodolares = (Double.parseDouble(totaldolares) - ((Double.parseDouble(pagoanuladolares) + Double.parseDouble(saldodolares))));
            nuevosaldodolares = (Double.parseDouble(pagoanuladolares) + Double.parseDouble(saldodolares));

            totald += Double.parseDouble(pagoanuladolares);
            loDalCliente.fnUpdDeudaCliente(Long.parseLong(loBeanCliente.getClipk()), Utilitario.fnRound(totald, Configuracion.MAXDECIMALMONTO), "2");
            loDalCobranza.fnUpdCobranzaDolares(pagoOnline.getCobranzaPK(),
                    Utilitario.fnRound(montopagadodolares, Integer.valueOf(isNumeroDecimales)),
                    Utilitario.fnRound(nuevosaldodolares, Integer.valueOf(isNumeroDecimales)));
        }

        BeanConsulta oConsulta = new BeanConsulta();
        oConsulta.setCodVendedor(loBeanUsuario.getCodVendedor());
        String fecha = getIntent().getExtras().getString("fecha", "");
        oConsulta.setFechaConsulta(ParseFecha(fecha));
        try {
            new HttpConsultaPagoOnline(oConsulta, ActDocumentoOnline.this, fnTipactividad()).execute();
            RESULTHTTP = 2;
        } catch (NexInvalidOperatorActivity.NexInvalidOperatorException e) {
                e.printStackTrace();
        }
    }

    private void grabar() {
        if(loBeanDocumento.getIdDocTipo().equals("C")) {
            int rangocorr;
            try {
                rangocorr = Integer.parseInt(txtRangoCorrelativo.getText().toString());
            } catch (Exception e) {
                rangocorr = 0;
            }
            if (rangocorr < numerocorrelativo) {
                Toast.makeText(getApplicationContext(), "Ingrese un correlativo mayor o igual al actual", Toast.LENGTH_LONG).show();
                return;
            }
        }
        BeanGeneral ioMotivo;
        ioMotivo = motivo;
        if(ioMotivo == null || ioMotivo.getCodigo().equals("")){
            Toast.makeText(getApplicationContext(),"Seleccione un tipo de motivo", Toast.LENGTH_LONG).show();
            return;
        }

        Calendar c = Calendar.getInstance();
        Date loDate = c.getTime();
        final String sFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate).toString();

        StringBuffer loStb = new StringBuffer();

        loStb.append(getString(loBeanDocumento.getIdDocTipo().equals("C") ? R.string.actdocumentocorrelativo_dlgfin : R.string.actdocumentopagoanulado_dlgfin)).append('\n');
        loStb.append(" Serie : ").append(loBeanDocumento.getSerie()).append('\n');
        loStb.append(" Correlativo " + (loBeanDocumento.getIdDocTipo().equals("C") ? "Actual" : "") + " : ").append(loBeanDocumento.getCorrelativo()).append('\n');

        if(loBeanDocumento.getIdDocTipo().equals("C")) {
            String correlativo = String.format("%010d", (Integer.parseInt(txtRangoCorrelativo.getText().toString())));
            loStb.append(" Anular Hasta : ").append(correlativo).append('\n');
        } else {
            if(Double.parseDouble(loBeanDocumento.getMontoSoles()) > 0){
                loStb.append(" Monto S/. : ").append(loBeanDocumento.getMontoSoles()).append('\n');
            }
            if(Double.parseDouble(loBeanDocumento.getMontoDolares()) > 0) {
                loStb.append(" Monto $ : ").append(loBeanDocumento.getMontoDolares()).append('\n');
            }
        }
        loStb.append(" Tipo Motivo : ").append(motivo.getDescripcion()).append('\n');
        loStb.append(sFecha).append('\n');

        AlertDialog loAlertDialog = null;
        loAlertDialog = new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(getString(loBeanDocumento.getIdDocTipo().equals("C") ? R.string.actdocumentocorrelativo_bardetalle : R.string.actdocumentopagoanulado_bardetalle))
                .setMessage(loStb.toString())

                .setPositiveButton(getString(R.string.dlg_btnsi),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                try {
                                    subGrabar();
                                    enviarDocumentoServer();
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }
                        })
                .setNegativeButton(getString(R.string.dlg_btncancelar),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {

                            }
                        }).create();
        loAlertDialog.show();
    }

    private void subGrabar(){
        Calendar c = Calendar.getInstance();
        Date loDate = c.getTime();
        loBeanDocumento.setFechaMovil(DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate).toString());
        loBeanDocumento.setRangocorrelativo(txtRangoCorrelativo.getText().toString());

        BeanGeneral ioMotivo;
        ioMotivo = motivo;
        loBeanDocumento.setIdDocTipoMotivo(ioMotivo.getCodigo());

        // Se guarda GPS si tiene el flag habilitado
        Location location = null;
        try {
            location = Gps.getLocation();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (flgGps.equals(
                Configuracion.FLGVERDADERO)
                && location != null) {
            loBeanDocumento.setLatitud(String.valueOf(location.getLatitude()));
            loBeanDocumento.setLongitud(String.valueOf(location.getLongitude()));
        } else {
            loBeanDocumento.setLatitud("0");
            loBeanDocumento.setLongitud("0");
        }
    }

    protected void NexShowDialog(int id) {
        DialogFragmentYesNo loDialog;
        switch (id) {
            case VALIDAHOME:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDAHOME"),
                        getString(R.string.actproductopedido_dlgback),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDAHOME"))
                        == DialogFragmentYesNo.YES) {

                    Intent intentl = new Intent(ActDocumentoOnline.this,
                            ActMenu.class);
                    intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    startActivity(intentl);
                }
                break;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.actmenu_dlgsincronizar),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"))
                        == DialogFragmentYesNo.YES) {
                    try {
                        String lsBeanUsuario = getSharedPreferences("Actual",
                                MODE_PRIVATE).getString("BeanUsuario", "");
                        BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                .fromJson(lsBeanUsuario, BeanUsuario.class);
                        estadoSincronizar = true;
                        new HttpSincronizar(loBeanUsuario.getCodVendedor(),
                                ActDocumentoOnline.this, fnTipactividad())
                                .execute();
                    } catch (Exception e) {
                        subShowException(e);
                    }
                }
                break;

            case VALIDASINCRONIZACION:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"),
                        getString(R.string.actmenuvalsincro_titulo),
                        getString(R.string.actmenuvalsincro_mensaje),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"));
                break;

            case VALIDASINCRO:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                        getString(R.string.msgregpendientesenvinfo),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"));
                break;

            case ENVIOPENDIENTES:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"),
                        getString(R.string.msgdeseaenviarpendientes),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"))
                        == DialogFragmentYesNo.YES) {
                    UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActDocumentoOnline.this);
                }
                break;

            case CONSSALIR:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSSALIR"),
                        getString(R.string.actproductopedido_dlgsalir),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSSALIR"))
                        == DialogFragmentYesNo.YES) {
                    salir();
                }
                break;
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDAHOME:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        Intent intentl = new Intent(
                                                ActDocumentoOnline.this,
                                                ActMenu.class);
                                        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                        startActivity(intentl);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences(
                                                    "Actual", MODE_PRIVATE)
                                                    .getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                                    .fromJson(lsBeanUsuario,
                                                            BeanUsuario.class);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActDocumentoOnline.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActDocumentoOnline.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSBACK:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        Intent loIntent = new Intent(ActDocumentoOnline.this, ActProductoPedido.class);
                                        finish();
                                        startActivity(loIntent);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case CONSSALIR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgsalir))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        salir();
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case ERRORGENERAR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titerror))
                        .setMessage(getString(R.string.actdocumentocorrelativo_dlgerrorcorrelativo))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        Intent intent = new Intent(ActDocumentoOnline.this, ActCobranzaMonto.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                    }
                                }).create();
                return loAlertDialog;
            default:
                return super.onCreateDialog(id);
        }

    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                estadoSalir = 3;
                showDialog(CONSSALIR);
            }
        });
    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        showDialog(VALIDAHOME);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @SuppressWarnings("deprecation")
    @Override
    public void subSincronizarFromSliding() {
        try {
            showDialog(VALIDASINCRO);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subPendientesFromSliding() {
        try {


        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub
        estadoSalir = 1;
        showDialog(CONSSALIR);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        estadoSalir = 2;
        showDialog(CONSSALIR);
    }

    private void salir() {

        if (estadoSalir == 2) {
            ((AplicacionEntel) getApplication()).setCurrentCliente(null);
            ((AplicacionEntel) getApplication())
                    .setCurrentCodFlujo(Configuracion.CONSSTOCK);
            ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
            Intent loIntento = new Intent(ActDocumentoOnline.this,
                    ActCobranzaMonto.class);
            loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            BeanExtras loBeanExtras = new BeanExtras();
            loBeanExtras.setFiltro("");
            loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
            String lsFiltro = "";
            try {
                lsFiltro = BeanMapper.toJson(loBeanExtras, false);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
            startActivity(loIntento);
        } else if (estadoSalir == 1) {

            String lsBeanUsuario = getSharedPreferences(
                    "Actual", MODE_PRIVATE).getString(
                    "BeanUsuario", "");
            loBeanUsuario = (BeanUsuario) BeanMapper
                    .fromJson(lsBeanUsuario,
                            BeanUsuario.class);
            this.setMsgOk("Eficiencia Online");

            BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
            loBeanListaEficienciaOnline.setIdResultado(0);
            loBeanListaEficienciaOnline.setResultado("");
            loBeanListaEficienciaOnline.setListaEficiencia(null);
            loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

            new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

            Intent loInstent = new Intent(this, ActKpiDetalle.class);
            loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(loInstent);
        } else if (estadoSalir == 3) {
            Intent loIntentLogin = new Intent(ActDocumentoOnline.this,
                    ActLogin.class);
            loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loIntentLogin);
        }
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(
                    getString(R.string.ml_sincronizarantes)));
        }
    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */
    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        showDialog(Constantes.DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */
    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        try {
            DialogFragmentYesNo loDialog;
            switch (id) {
                case Constantes.DIALOG_NSERVICES_DOWNLOAD:
                    String lsNServices;
                    if (ioDALNservices == null)
                        ioDALNservices = DALNServices.getInstance(this, R.string.db_name);
                    try {
                        lsNServices = ioDALNservices.fnSelNServicesLabel();
                    } catch (Exception e) {
                        Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                        lsNServices = getString(R.string.nservices_label_default);
                    }
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG
                                    + Constantes.DIALOG_NSERVICES_DOWNLOAD,
                            getString(R.string.nservices_consultingdownlad)
                                    .replace("{0}", lsNServices),
                            EnumLayoutResource.getDefaultIconHelp(this));
                    return new DialogFragmentYesNoPairListener(loDialog,
                            new OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(
                                        DialogFragmentYesNo dialog) {
                                    try {
                                        subIniNServicesDownload(ioDALNservices.fnSelNServicesAPK());
                                    } catch (Exception e) {
                                        subShowException(e);
                                    }
                                }

                                @Override
                                public void performButtonNo(
                                        DialogFragmentYesNo dialog) {
                                }
                            });
                case CONSBACK:
                    loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                            DialogFragmentYesNo.TAG.concat("CONSBACK"),
                            getString(R.string.actproductopedido_dlgback),
                            R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNoPairListener(loDialog,
                            new OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(DialogFragmentYesNo dialog) {
                                    finish();
                                    startActivity(new Intent(ActDocumentoOnline.this, ActProductoPedido.class));
                                }

                                @Override
                                public void performButtonNo(DialogFragmentYesNo dialog) {

                                }
                            });
                default:
                    return super.onCreateNexDialog(id);
            }
        } catch (Exception e) {
            subShowException(e);
            return super.onCreateNexDialog(id);
        }
    }

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                Constantes.REQUEST_NSERVICES);
    }

}
