package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Dsb Mobile on 23/03/2015.
 */
public class BeanConfiguracion {

    @JsonProperty
    private String codigo = "";
    @JsonProperty
    private String valor = "";

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
