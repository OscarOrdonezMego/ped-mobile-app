package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.widget.HeadedTextItem;

public class DalUsuario {

    private Context ioContext;

    public DalUsuario(Context psClase) {

        ioContext = psClase;
    }

    public boolean existeTable() {
        boolean lbResult = false;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        String[] laColumnasMed = new String[]{"name"};
        String lsTabla = "sqlite_master";
        String lsWhere = "upper(name)='TBL_VENDEDOR'";
        Cursor loCursor = myDB.query(true, lsTabla, laColumnasMed, lsWhere,
                null, null, null, null, null);
        if (loCursor.getCount() == 1) {
            lbResult = true;
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return lbResult;
    }

    public ArrayList<HeadedTextItem> fnBuscarListVendedor(String psFiltro,
                                                          EnumTipBusqueda poTipBusqueda) {
        ArrayList<HeadedTextItem> loHeaValor = new ArrayList<HeadedTextItem>();

        existeTable();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        boolean lbPriLetra = true;

        String[] laColumnas = new String[]{"CODIGO", "NOMBRE",
                "upper(NOMBRE)"};
        String lsTablas = Configuracion.NAME_TBL_VENDEDOR;

        String lsWhere = (poTipBusqueda == EnumTipBusqueda.NOMBRE) ? "upper(NOMBRE) like "
                + "'%" + ((lbPriLetra) ? "" : "%") + psFiltro + "%'"
                : "trim((CODIGO)) = " + "'" + psFiltro.toUpperCase() + "' ";
        Cursor loCursor;

        if (psFiltro.equals("")) {
            loCursor = myDB.query(lsTablas, laColumnas, null, null, null, null,
                    "CODIGO");
        } else {
            loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                    null, "NOMBRE");
        }
        if (loCursor != null) {
            int liContador = 0;
            if (loCursor.moveToFirst()) {
                do {
                    HeadedTextItem he = new HeadedTextItem(
                            (loCursor.getString(0) + " | " + loCursor
                                    .getString(1)).toUpperCase());
                    he.setTag(loCursor.getString(0));
                    loHeaValor.add(he);
                    liContador++;

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loHeaValor;
    }

}