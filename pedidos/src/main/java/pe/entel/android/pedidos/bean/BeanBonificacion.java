package pe.entel.android.pedidos.bean;

import java.util.List;

/**
 * Created by Dsb Mobile on 22/06/2015.
 */
public class BeanBonificacion {

    private int id;
    private int idProducto;
    private int idPresentacion;
    private String codigoArticulo;
    private String nombreArticulo;
    private String cantidad;
    private String maximo;
    private String editable;
    private String tipoArticulo;
    private int prioridad;
    private List<BeanBonificacionDet> lstBonificacionDet;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getIdPresentacion() {
        return idPresentacion;
    }

    public void setIdPresentacion(int idPresentacion) {
        this.idPresentacion = idPresentacion;
    }

    public String getCodigoArticulo() {
        return codigoArticulo;
    }

    public void setCodigoArticulo(String codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }

    public String getNombreArticulo() {
        return nombreArticulo;
    }

    public void setNombreArticulo(String nombreArticulo) {
        this.nombreArticulo = nombreArticulo;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getMaximo() {
        return maximo;
    }

    public void setMaximo(String maximo) {
        this.maximo = maximo;
    }

    public String getEditable() {
        return editable;
    }

    public void setEditable(String editable) {
        this.editable = editable;
    }

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    public List<BeanBonificacionDet> getLstBonificacionDet() {
        return lstBonificacionDet;
    }

    public void setLstBonificacionDet(List<BeanBonificacionDet> lstBonificacionDet) {
        this.lstBonificacionDet = lstBonificacionDet;


    }
}
