package pe.entel.android.pedidos.act;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;
import java.util.Properties;

import greendroid.widget.ActionBarItem;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.actividad.NexInvalidOperatorActivity;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexToast;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapListaSucursales;
import pe.entel.android.pedidos.bean.BeanSucursal;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalSucursal;
import pe.entel.android.pedidos.http.HttpSucursalConfiguracion;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;

public class ActListaSucursales extends NexActivity implements
        AdapterView.OnItemClickListener {

    private final int CONSBACK = 1;
    BeanUsuario loBeanUsuario = null;
    private ListView lista;
    private List<BeanSucursal> ioLista;
    private boolean sincronizarConfiguracion;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);
        String lsBeanUsuario = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario,
                BeanUsuario.class);

        super.onCreate(savedInstanceState);

        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));
    }

    @Override
    protected void subSetControles() {
        super.subSetControles();
        try {
            sincronizarConfiguracion = false;
            setActionBarContentView(R.layout.activity_act_lista_sucurcales);
            lista = (ListView) findViewById(R.id.actlistasucursales_lista);
            lista.setOnItemClickListener(this);
        } catch (Exception e) {
            Log.e("subSetControles", e.toString());
            subShowException(e);
        }
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setType(greendroid.widget.ActionBarGD.Type.Empty);
        addActionBarItem(ActionBarItem.Type.Nex_Back, CONSBACK);
        this.getActionBarGD().setTitle(getString(R.string.actsucursallista_bartitulo));
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {
        if (sincronizarConfiguracion) {
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {
                finish();
                startActivity(new Intent(this, ActLogin.class));
            } else if (getIdHttpResultado() != ConfiguracionNextel.CONSRESSERVIDOROK) {
                SharedPreferences.Editor loEditor = PreferenceManager.getDefaultSharedPreferences(ActListaSucursales.this).edit();
                loEditor.putString(getString(R.string.keyPrefVerifSucursal), "N");
                loEditor.commit();

                Toast loToast = Toast.makeText(this, getString(R.string.actlistasucursales_dlgerror), Toast.LENGTH_LONG);
                loToast.setGravity(Gravity.CENTER, 0, 0);
                loToast.show();
                finish();
                startActivity(new Intent(ActListaSucursales.this, ActLogin.class));
            } else if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Properties loProperties = Utilitario.readProperties(this);
                String lsIpServer = loProperties.getProperty("IP_SERVER");
                String nuevoIpServer = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(getString(R.string.keyPrefIPSucursal), lsIpServer);
                loProperties.setProperty("IP_SERVER", nuevoIpServer);
                String lsIdEmpresa = loProperties.getProperty("IP_SERVER");
                finish();
                startActivity(new Intent(ActListaSucursales.this, ActLogin.class));
            }
        }

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        subLLenarLista();
    }

    @Override
    public void onBackPressed() {
        finish();
        startActivity(new Intent(this, ActLogin.class));
    }

    public void subLLenarLista() {
        ioLista = new DalSucursal(this).fnSelSucursales();

        if (ioLista.size() == 0) {
            new NexToast(this, R.string.actsucursallista_dlgsindatos, NexToast.EnumType.INFORMATION).show();
            startActivity(new Intent(this, ActLogin.class));

        } else {
            lista.setAdapter(new AdapListaSucursales(this, R.layout.actlistasucursaleslista_item, ioLista));
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                            long arg3) {

        BeanSucursal beanSucursal = ioLista.get(position);
        String IPSucursal = new DalSucursal(this).fnSelURLSucursal(beanSucursal.getCodigo());
        SharedPreferences.Editor loEditor = PreferenceManager.getDefaultSharedPreferences(ActListaSucursales.this).edit();
        loEditor.putString(getString(R.string.keyPrefIPSucursal), IPSucursal);
        loEditor.putString(getString(R.string.keyPrefVerifSucursal), "S");
        loEditor.commit();

        try {
            sincronizarConfiguracion = true;
            new HttpSucursalConfiguracion(ActListaSucursales.this, fnTipactividad()).execute();
        } catch (NexInvalidOperatorActivity.NexInvalidOperatorException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        switch (item.getItemId()) {

            case CONSBACK:
                finish();
                startActivity(new Intent(this, ActLogin.class));
                break;

            default:
                return super.onHandleActionBarItemClick(item, position);
        }
        return true;
    }


}
