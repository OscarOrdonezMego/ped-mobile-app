package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanAlmacen;
import pe.entel.android.pedidos.bean.BeanEnvPedidoDet;

/**
 * Created by Dsb Mobile on 20/04/2015.
 */
public class DalAlmacen {

    private Context ioContext;

    public DalAlmacen(Context psClase) {

        ioContext = psClase;
    }

    public BeanAlmacen fnSelectXIdAlmacen(String psIdAlmacen) {

        BeanAlmacen loBeanAlmacen = null;
        SQLiteDatabase loDB = null;

        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"ID", "ALM_CODIGO", "ALM_NOMBRE",
                "ALM_DIRECCION"
        };
        String lsTablas = "TBL_ALMACEN";
        String lsWhere = "ID = " + psIdAlmacen;

        Cursor loCursor = loDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanAlmacen = new BeanAlmacen();
                    loBeanAlmacen.setPk(loCursor.getInt(0));
                    loBeanAlmacen.setCodigo(loCursor.getString(1));
                    loBeanAlmacen.setNombre(loCursor.getString(2));
                    loBeanAlmacen.setDireccion(loCursor.getString(3));

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return loBeanAlmacen;
    }


    public List<BeanAlmacen> fnSelectAll() {
        List<BeanAlmacen> laBeanAlmacenes = new ArrayList<BeanAlmacen>();
        BeanAlmacen loBeanAlmacen = null;
        SQLiteDatabase loDB = null;

        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"ID", "ALM_CODIGO", "ALM_NOMBRE",
                "ALM_DIRECCION"
        };
        String lsTablas = "TBL_ALMACEN";

        Cursor loCursor = loDB.query(lsTablas, laColumnas, null, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanAlmacen = new BeanAlmacen();
                    loBeanAlmacen.setPk(loCursor.getInt(0));
                    loBeanAlmacen.setCodigo(loCursor.getString(1));
                    loBeanAlmacen.setNombre(loCursor.getString(2));
                    loBeanAlmacen.setDireccion(loCursor.getString(3));
                    laBeanAlmacenes.add(loBeanAlmacen);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return laBeanAlmacenes;
    }

    public List<BeanAlmacen> fnSelectAllPresentacion() {
        List<BeanAlmacen> laBeanAlmacenes = new ArrayList<BeanAlmacen>();
        BeanAlmacen loBeanAlmacen = null;
        SQLiteDatabase loDB = null;

        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String sql = "SELECT DISTINCT ALM.ID, ALM.ALM_CODIGO, ALM.ALM_NOMBRE, ALM.ALM_DIRECCION " +
                "FROM TBL_ALMACEN ALM " +
                "INNER JOIN TBL_ALMACEN_PRESENTACION ALMPRE ON ALMPRE.IdAlmacen = ALM.ID";

        Cursor loCursor = loDB.rawQuery(sql, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {


                    loBeanAlmacen = new BeanAlmacen();
                    loBeanAlmacen.setPk(loCursor.getInt(0));
                    loBeanAlmacen.setCodigo(loCursor.getString(1));
                    loBeanAlmacen.setNombre(loCursor.getString(2));
                    loBeanAlmacen.setDireccion(loCursor.getString(3));
                    laBeanAlmacenes.add(loBeanAlmacen);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return laBeanAlmacenes;
    }

    public List<BeanAlmacen> fnSelectAllProducto() {
        List<BeanAlmacen> laBeanAlmacenes = new ArrayList<BeanAlmacen>();
        BeanAlmacen loBeanAlmacen = null;
        SQLiteDatabase loDB = null;

        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String sql = "SELECT DISTINCT ALM.ID, ALM.ALM_CODIGO, ALM.ALM_NOMBRE, ALM.ALM_DIRECCION " +
                "FROM TBL_ALMACEN ALM INNER JOIN TBL_ALMACEN_PRODUCTO ALMPRO ON ALMPRO.ALM_PK = ALM.ID";

        Cursor loCursor = loDB.rawQuery(sql, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanAlmacen = new BeanAlmacen();
                    loBeanAlmacen.setPk(loCursor.getInt(0));
                    loBeanAlmacen.setCodigo(loCursor.getString(1));
                    loBeanAlmacen.setNombre(loCursor.getString(2));
                    loBeanAlmacen.setDireccion(loCursor.getString(3));
                    laBeanAlmacenes.add(loBeanAlmacen);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return laBeanAlmacenes;
    }

    public long fnCountXCodProducto(String poCodProducto) {
        long llCount = 0;
        SQLiteDatabase loDB = null;

        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String sql = "SELECT DISTINCT COUNT(*) " +
                "FROM TBL_ALMACEN ALM " +
                "INNER JOIN TBL_ALMACEN_PRODUCTO ALM_PRO " +
                "ON ALM.ID = ALM_PRO.ALM_PK " +
                "WHERE ALM_PRO.PRO_PK =" + poCodProducto;

        Cursor loCursor = loDB.rawQuery(sql, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {

                    llCount = loCursor.getLong(0);


                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return llCount;
    }

    public List<BeanAlmacen> fnSelectXCodProducto(String poCodProducto) {
        List<BeanAlmacen> laBeanAlmacenes = new ArrayList<BeanAlmacen>();
        BeanAlmacen loBeanAlmacen = null;
        SQLiteDatabase loDB = null;

        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String sql = "SELECT DISTINCT ALM.ID, ALM.ALM_CODIGO, ALM.ALM_NOMBRE," +
                "ALM.ALM_DIRECCION, ALM_PRO.ALM_PRO_STOCK " +
                "FROM TBL_ALMACEN ALM " +
                "INNER JOIN TBL_ALMACEN_PRODUCTO ALM_PRO " +
                "ON ALM.ID = ALM_PRO.ALM_PK " +
                "WHERE ALM_PRO.PRO_PK =" + poCodProducto;

        Cursor loCursor = loDB.rawQuery(sql, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanAlmacen = new BeanAlmacen();
                    loBeanAlmacen.setPk(loCursor.getInt(0));
                    loBeanAlmacen.setCodigo(loCursor.getString(1));
                    loBeanAlmacen.setNombre(loCursor.getString(2));
                    loBeanAlmacen.setDireccion(loCursor.getString(3));
                    loBeanAlmacen.setStock(loCursor.getString(4));
                    if (TextUtils.isEmpty(loBeanAlmacen.getStock())) {
                        loBeanAlmacen.setStock("0");
                    }
                    laBeanAlmacenes.add(loBeanAlmacen);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return laBeanAlmacenes;
    }

    public long fnCountXCodPresentacion(String psCodPresentacion) {
        long llCount = 0;
        SQLiteDatabase loDB = null;

        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String sql = "SELECT DISTINCT count(*) " +
                "FROM TBL_ALMACEN ALM " +
                "INNER JOIN TBL_ALMACEN_PRESENTACION ALM_PRE " +
                "ON ALM.ID = ALM_PRE.IdAlmacen " +
                "WHERE ALM_PRE.IdPresentacion = " + psCodPresentacion;

        Cursor loCursor = loDB.rawQuery(sql, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {

                    llCount = loCursor.getLong(0);


                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return llCount;
    }

    public List<BeanAlmacen> fnSelectXCodPresentacion(String psCodPresentacion) {
        List<BeanAlmacen> laBeanAlmacenes = new ArrayList<BeanAlmacen>();
        BeanAlmacen loBeanAlmacen = null;
        SQLiteDatabase loDB = null;

        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String sql = "SELECT DISTINCT ALM.ID, ALM.ALM_CODIGO, ALM.ALM_NOMBRE," +
                "ALM.ALM_DIRECCION, ALM_PRE.Stock " +
                "FROM TBL_ALMACEN ALM " +
                "INNER JOIN TBL_ALMACEN_PRESENTACION ALM_PRE " +
                "ON ALM.ID = ALM_PRE.IdAlmacen " +
                "WHERE ALM_PRE.IdPresentacion = " + psCodPresentacion;

        Cursor loCursor = loDB.rawQuery(sql, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanAlmacen = new BeanAlmacen();
                    loBeanAlmacen.setPk(loCursor.getInt(0));
                    loBeanAlmacen.setCodigo(loCursor.getString(1));
                    loBeanAlmacen.setNombre(loCursor.getString(2));
                    loBeanAlmacen.setDireccion(loCursor.getString(3));
                    loBeanAlmacen.setStock(loCursor.getString(4));
                    if (TextUtils.isEmpty(loBeanAlmacen.getStock())) {
                        loBeanAlmacen.setStock("0");
                    }
                    laBeanAlmacenes.add(loBeanAlmacen);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return laBeanAlmacenes;
    }

    public BeanAlmacen fnSelectAlmacenProducto(String psCodProducto,
                                               String psCodAlmacen) {
        BeanAlmacen loBeanAlmacen = fnSelectXCodProductoXCodAlmacen(psCodProducto, psCodAlmacen);
        if (loBeanAlmacen == null) {
            List<BeanAlmacen> laBeanAlmacenes = fnSelectXCodProducto(psCodProducto);
            if (laBeanAlmacenes.size() > 0)
                loBeanAlmacen = laBeanAlmacenes.get(0);
        }

        return loBeanAlmacen;

    }

    public BeanAlmacen fnSelectAlmacenPresentacion(String psCodPresentacion,
                                                   String psCodAlmacen) {
        BeanAlmacen loBeanAlmacen = fnSelectXCodPresentacionXCodAlmacen(psCodPresentacion, psCodAlmacen);
        if (loBeanAlmacen == null) {
            List<BeanAlmacen> laBeanAlmacenes = fnSelectXCodPresentacion(psCodPresentacion);
            if (laBeanAlmacenes.size() > 0)
                loBeanAlmacen = laBeanAlmacenes.get(0);
        }

        return loBeanAlmacen;

    }

    public BeanAlmacen fnSelectXCodProductoXCodAlmacen(String psCodProducto,
                                                       String psCodAlmacen) {
        BeanAlmacen loBeanAlmacen = null;
        SQLiteDatabase loDB = null;


        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String sql = "SELECT DISTINCT ALM.ID, ALM.ALM_CODIGO, ALM.ALM_NOMBRE," +
                "ALM.ALM_DIRECCION, ALM_PRO.ALM_PRO_STOCK " +
                "FROM TBL_ALMACEN ALM " +
                "INNER JOIN TBL_ALMACEN_PRODUCTO ALM_PRO " +
                "ON ALM.ID = ALM_PRO.ALM_PK " +
                "WHERE ALM_PRO.PRO_PK = " + psCodProducto + " " +
                "AND ALM_PRO.ALM_PK = " + psCodAlmacen;

        Cursor loCursor = loDB.rawQuery(sql, null);


        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanAlmacen = new BeanAlmacen();
                    loBeanAlmacen.setPk(loCursor.getInt(0));
                    loBeanAlmacen.setCodigo(loCursor.getString(1));
                    loBeanAlmacen.setNombre(loCursor.getString(2));
                    loBeanAlmacen.setDireccion(loCursor.getString(3));
                    loBeanAlmacen.setStock(loCursor.getString(4));
                    if (TextUtils.isEmpty(loBeanAlmacen.getStock())) {
                        loBeanAlmacen.setStock("0");
                    }
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return loBeanAlmacen;
    }

    public BeanAlmacen fnSelectXCodPresentacionXCodAlmacen(String psCodPresentacion,
                                                           String psCodAlmacen) {
        BeanAlmacen loBeanAlmacen = null;
        SQLiteDatabase loDB = null;


        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String sql = "SELECT DISTINCT ALM.ID, ALM.ALM_CODIGO, ALM.ALM_NOMBRE," +
                "ALM.ALM_DIRECCION, ALM_PRE.Stock " +
                "FROM TBL_ALMACEN ALM " +
                "INNER JOIN TBL_ALMACEN_PRESENTACION ALM_PRE " +
                "ON ALM.ID = ALM_PRE.IdAlmacen " +
                "WHERE ALM_PRE.IdPresentacion = " + psCodPresentacion +
                " AND ALM_PRE.IdAlmacen = " + psCodAlmacen;

        Cursor loCursor = loDB.rawQuery(sql, null);


        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanAlmacen = new BeanAlmacen();
                    loBeanAlmacen.setPk(loCursor.getInt(0));
                    loBeanAlmacen.setCodigo(loCursor.getString(1));
                    loBeanAlmacen.setNombre(loCursor.getString(2));
                    loBeanAlmacen.setDireccion(loCursor.getString(3));
                    loBeanAlmacen.setStock(loCursor.getString(4));
                    if (TextUtils.isEmpty(loBeanAlmacen.getStock())) {
                        loBeanAlmacen.setStock("0");
                    }

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return loBeanAlmacen;
    }

    public void subupStockProductos(List<BeanEnvPedidoDet> lstDetallePedidos) {
        BeanEnvPedidoDet detalle;
        for (int i = 0; i < lstDetallePedidos.size(); i++) {
            detalle = lstDetallePedidos.get(i);
            fnUpdStock(detalle.getCodAlmacen(), detalle.getIdArticulo(), detalle.getStock());
        }
    }

    public void subupStockPresentacion(List<BeanEnvPedidoDet> poLstDetallePedido) {
        BeanEnvPedidoDet detalle;
        for (int i = 0; i < poLstDetallePedido.size(); i++) {
            detalle = poLstDetallePedido.get(i);
            fnUpdPreStock(detalle.getCodAlmacen(), detalle.getIdArticulo(), detalle.getStock());
        }
    }

    public boolean fnUpdStock(String psCodAlmacen, String psCodProducto, String psStock) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TBL_ALMACEN_PRODUCTO SET ALM_PRO_STOCK='" + psStock
                + "' WHERE ALM_PK = " + psCodAlmacen + " AND PRO_PK = " + psCodProducto);
        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            Log.e("DalAlmacen", "fnUpdStock", e);
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnUpdPreStock(String psCodAlmacen, String psCodPresentacion, String psStock) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TBL_ALMACEN_PRESENTACION SET STOCK = '" + psStock
                + "' WHERE IdAlmacen = " + psCodAlmacen + " AND IdPresentacion = " + psCodPresentacion);
        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            Log.e("DalAlmacen", "fnUpdPreStock", e);
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }


    public void fnGrabarAlmacenPresentacion(String psCodAlmacen, String psCodPresentacion, String psStock) {
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);


        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("INSERT INTO TBL_ALMACEN_PRESENTACION ("
                + "IdAlmacen,IdPresentacion,Stock) VALUES ('");
        loStbCab.append(psCodAlmacen);
        loStbCab.append("','");
        loStbCab.append(psCodPresentacion);
        loStbCab.append("','");
        loStbCab.append(psStock);
        loStbCab.append("')");

        try {

            loDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            Log.e("DalAlmacen", "fnGrabarAlmacenPresentacion", e);

        } finally {
            loDB.close();
        }
    }

    public void fnGrabarAlmacenProducto(String psCodAlmacen, String psCodProducto, String psStock) {
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);


        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("INSERT INTO TBL_ALMACEN_PRODUCTO ("
                + "ALM_PK,PRO_PK,ALM_PRO_STOCK) VALUES ('");
        loStbCab.append(psCodAlmacen);
        loStbCab.append("','");
        loStbCab.append(psCodProducto);
        loStbCab.append("','");
        loStbCab.append(psStock);
        loStbCab.append("')");

        try {

            loDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            Log.e("DalAlmacen", "fnGrabarAlmacenProducto", e);

        } finally {
            loDB.close();
        }
    }
}
