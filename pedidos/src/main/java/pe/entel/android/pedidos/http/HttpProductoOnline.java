package pe.entel.android.pedidos.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanAlmacen;
import pe.entel.android.pedidos.bean.BeanArticulo;
import pe.entel.android.pedidos.bean.BeanArticuloOnline;
import pe.entel.android.pedidos.bean.BeanPresentacion;
import pe.entel.android.pedidos.bean.BeanProducto;
import pe.entel.android.pedidos.bean.BeanRespListaProdOnline;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumUrl;
import pe.entel.android.pedidos.util.UtilitarioData;

public class HttpProductoOnline extends HttpConexion {

    private BeanArticuloOnline ioBeanArticuloOnline;
    private Context ioContext;

    // CONSTRUCTOR
    public HttpProductoOnline(BeanArticuloOnline poBeanArticuloOnline, Context poContext, EnumTipActividad poEnumTipActividad) {
        super(poContext, poContext
                .getString(R.string.httpproductoonline_dlgbuscqueda), poEnumTipActividad);
        ioBeanArticuloOnline = poBeanArticuloOnline;
        ioContext = poContext;
    }

    public HttpProductoOnline(BeanArticuloOnline poBeanArticuloOnline, Context poContext, EnumTipActividad poEnumTipActividad, String mensaje) {
        super(poContext, mensaje, poEnumTipActividad);
        ioBeanArticuloOnline = poBeanArticuloOnline;
        ioContext = poContext;
    }

    public void subConHttp() {
        try {
            //RestTemplate restTemplate = new RestTemplate();
            //restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            Log.v("URL", Configuracion.fnUrl(ioContext, EnumUrl.PRODUCTOONLINE));
            Log.v("URL", BeanMapper.toJson(ioBeanArticuloOnline, false));

            //String lsObjeto = restTemplate.postForObject(Configuracion.fnUrl(ioContext, EnumUrl.PRODUCTOONLINE), ioBeanArticuloOnline, String.class);
            String lsObjeto = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, EnumUrl.PRODUCTOONLINE), ioBeanArticuloOnline);
            BeanRespListaProdOnline loBeanProducto = (BeanRespListaProdOnline) BeanMapper.fromJson(lsObjeto, BeanRespListaProdOnline.class);

            setHttpResponseObject(loBeanProducto);
        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, "Error HTTP: " + e.getMessage());
        }
    }

    @Override
    public boolean OnPreConnect() {
        if (!UtilitarioData.fnObtenerPreferencesBoolean(ioContext, "pedidoPendiente")) {
            SharedPreferences loSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ioContext);
            if (loSharedPreferences.getBoolean(ioContext.getString(R.string.keypre_cobertura), false)) {
                setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_modofueracobertura));
                return false;
            }
        }
        if (!Utilitario.fnVerSignal(ioContext)) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            BeanRespListaProdOnline loBeanRespListaProdOnline = (BeanRespListaProdOnline) getHttpResponseObject();
            int liIdHttprespuesta = loBeanRespListaProdOnline.getIdResultado();
            String lsHttpRespuesta = loBeanRespListaProdOnline.getResultado();

            if (liIdHttprespuesta == ConfiguracionNextel.CONSRESSERVIDOROK
                    || liIdHttprespuesta == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {

                List<BeanArticulo> loLstBeanArticulo = new ArrayList<BeanArticulo>();

                for (BeanArticuloOnline loBeanArticuloOnline : loBeanRespListaProdOnline.getListaProd()) {

                    BeanArticulo loBeanArticulo = null;

                    if (!TextUtils.isEmpty(loBeanArticuloOnline.getCodigoProducto())) {
                        loBeanArticulo = new BeanPresentacion();

                        BeanProducto loBeanProducto = new BeanProducto();
                        loBeanProducto.setCodigo(loBeanArticuloOnline.getCodigoProducto());
                        loBeanProducto.setNombre(loBeanArticuloOnline.getNombreProducto());
                        loBeanProducto.setUnidadDefecto(loBeanArticuloOnline.getUnidadDefecto());
                        ((BeanPresentacion) loBeanArticulo).setProducto(loBeanProducto);

                        long llCantidadPresentacion = 0;
                        try {
                            llCantidadPresentacion = Long.parseLong(loBeanArticuloOnline.getCantidadPresentacion());
                        } catch (Exception e) {
                            llCantidadPresentacion = 0;
                        }
                        ((BeanPresentacion) loBeanArticulo).setCantidad(llCantidadPresentacion);
                    } else {
                        loBeanArticulo = new BeanProducto();
                    }

                    loBeanArticulo.setCodigo(loBeanArticuloOnline.getCodigo());
                    loBeanArticulo.setNombre(loBeanArticuloOnline.getNombre());
                    loBeanArticulo.setStock(loBeanArticuloOnline.getStock());

                    if (!TextUtils.isEmpty(loBeanArticuloOnline.getCodAlmacen())) {
                        BeanAlmacen loBeanAlmacen = new BeanAlmacen();
                        loBeanAlmacen.setCodigo(loBeanArticuloOnline.getCodAlmacen());
                        loBeanAlmacen.setNombre(loBeanArticuloOnline.getNombreAlmacen());

                        loBeanArticulo.setAlmacen(loBeanAlmacen);
                    }

                    loLstBeanArticulo.add(loBeanArticulo);
                }

                ((AplicacionEntel) ioContext.getApplicationContext()).setCurrentlistaArticulo(loLstBeanArticulo);
            } else {
                ((AplicacionEntel) ioContext.getApplicationContext()).setCurrentlistaArticulo(new ArrayList<BeanArticulo>());
            }
            setHttpResponseIdMessage(liIdHttprespuesta, lsHttpRespuesta);
        }
    }
}