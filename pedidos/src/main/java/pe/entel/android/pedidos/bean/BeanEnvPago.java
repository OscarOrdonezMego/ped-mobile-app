package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanEnvPago {
    private String codPago; //Id del pago generado para identificarlo en el movil
    @JsonProperty
    private String cuenta;//compañia
    @JsonProperty
    private String idDocCobranza;
    @JsonProperty
    private String idUsuario;
    @JsonProperty
    private String idCliente;
    @JsonProperty
    private String pago;
    @JsonProperty
    private String pagosoles;
    @JsonProperty
    private String pagodolares;
    @JsonProperty
    private String idFormaPago;
    @JsonProperty
    private String nroDocumento;//voucher
    @JsonProperty
    private String idBanco;
    @JsonProperty
    private String fechaDocumento;
    @JsonProperty
    private String latitud;
    @JsonProperty
    private String longitud;
    @JsonProperty
    private String fechaMovil;
    @JsonProperty
    private String celda;
    @JsonProperty
    private String errorConexion;
    @JsonProperty
    private String errorPosicion;
    //campos de cobranza para mostrar
    private String cob_pk;
    private String cob_montototal;
    private String cob_montopagado;
    private String saldo;

    @JsonProperty
    private String flgEnCobertura;

    @JsonProperty
    private String fechaDiferida;

    private String idtipocambiosoles;
    private String idtipocambiodolares;
    //campos de cobranza soles para mostrar
    private String cob_montopagadosoles;
    private String cob_montototalsoles;
    private String saldosoles;
    private String tipomonedasoles;

    //campos de cobranza dolares para mostrar
    private String  cob_montototaldolares;
    private String cob_montopagadodolares;
    private String saldodolares;
    private String tipomonedadolares;


    public String getFechaDiferida() {
        return fechaDiferida;
    }

    public void setFechaDiferida(String fechaDiferida) {
        this.fechaDiferida = fechaDiferida;
    }

    public BeanEnvPago() {
        codPago = "";
        cuenta = "";
        idDocCobranza = "";
        idUsuario = "";
        idCliente = "";
        pago = "0";
        pagosoles = "0";
        pagodolares = "0";
        idFormaPago = "";
        nroDocumento = "";
        idBanco = "";
        fechaDocumento = "";
        latitud = "0";
        longitud = "0";
        fechaMovil = "";
        celda = "";
        cob_pk = "0";
        cob_montototal = "0";
        cob_montopagado = "0";
        saldo = "0";
        errorConexion = "0";
        errorPosicion = "0";
        flgEnCobertura = "1";
        fechaDiferida = "";
        cob_montototalsoles = "0";
        cob_montopagadosoles = "0";
        saldosoles = "0";
        tipomonedasoles = "0";
        cob_montototaldolares = "0";
        cob_montopagadodolares = "0";
        saldodolares = "0";
        tipomonedadolares = "0";
        idtipocambiosoles = "0";
        idtipocambiodolares = "0";
    }

    private double round(double d) {
        return (Math.floor(d * 100.0d + 0.5d) / 100.0d);
    }


    public String getCodPago() {
        return codPago;
    }


    public void setCodPago(String codPago) {
        this.codPago = codPago;
    }


    public String getCuenta() {
        return cuenta;
    }


    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }


    public String getIdDocCobranza() {
        return idDocCobranza;
    }


    public void setIdDocCobranza(String idDocCobranza) {
        this.idDocCobranza = idDocCobranza;
    }


    public String getIdUsuario() {
        return idUsuario;
    }


    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }


    public String getIdCliente() {
        return idCliente;
    }


    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }


    public String getPago() {
        return pago;
    }


    public void setPago(String pago) {
        this.pago = pago;
    }


    public String getIdFormaPago() {
        return idFormaPago;
    }


    public void setIdFormaPago(String idFormaPago) {
        this.idFormaPago = idFormaPago;
    }


    public String getNroDocumento() {
        return nroDocumento;
    }


    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }


    public String getIdBanco() {
        return idBanco;
    }


    public void setIdBanco(String idBanco) {
        this.idBanco = idBanco;
    }


    public String getFechaDocumento() {
        return fechaDocumento;
    }


    public void setFechaDocumento(String fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }


    public String getFechaMovil() {
        return fechaMovil;
    }


    public void setFechaMovil(String fechaMovil) {
        this.fechaMovil = fechaMovil;
    }


    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getCelda() {
        return celda;
    }

    public void setCelda(String celda) {
        this.celda = celda;
    }


    public String getErrorConexion() {
        return errorConexion;
    }


    public void setErrorConexion(String errorConexion) {
        this.errorConexion = errorConexion;
    }


    public String getErrorPosicion() {
        return errorPosicion;
    }


    public void setErrorPosicion(String errorPosicion) {
        this.errorPosicion = errorPosicion;
    }


    public String getCob_pk() {
        return cob_pk;
    }


    public void setCob_pk(String cob_pk) {
        this.cob_pk = cob_pk;
    }


    public String getCob_montototal() {
        return cob_montototal;
    }


    public void setCob_montototal(String cob_montototal) {
        this.cob_montototal = cob_montototal;
    }


    public String getCob_montopagado() {
        return cob_montopagado;
    }


    public void setCob_montopagado(String cob_montopagado) {
        this.cob_montopagado = cob_montopagado;
    }


    public String getSaldo() {
        return saldo;
    }


    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }


    public String getFlgEnCobertura() {
        return flgEnCobertura;
    }


    public void setFlgEnCobertura(String flgEnCobertura) {
        this.flgEnCobertura = flgEnCobertura;
    }


    public String getCob_montototalsoles() {
        return cob_montototalsoles;
    }


    public void setCob_montototalsoles(String cob_montototalsoles) {
        this.cob_montototalsoles = cob_montototalsoles;
    }


    public String getCob_montopagadosoles() {
        return cob_montopagadosoles;
    }


    public void setCob_montopagadosoles(String cob_montopagadosoles) {
        this.cob_montopagadosoles = cob_montopagadosoles;
    }


    public String getSaldosoles() {
        return saldosoles;
    }


    public void setSaldosoles(String saldosoles) {
        this.saldosoles = saldosoles;
    }


    public String getTipomonedasoles() {
        return tipomonedasoles;
    }


    public void setTipomonedasoles(String tipomonedasoles) {
        this.tipomonedasoles = tipomonedasoles;
    }


    public String getCob_montototaldolares() {
        return cob_montototaldolares;
    }


    public void setCob_montototaldolares(String cob_montototaldolares) {
        this.cob_montototaldolares = cob_montototaldolares;
    }


    public String getCob_montopagadodolares() {
        return cob_montopagadodolares;
    }


    public void setCob_montopagadodolares(String cob_montopagadodolares) {
        this.cob_montopagadodolares = cob_montopagadodolares;
    }


    public String getSaldodolares() {
        return saldodolares;
    }


    public void setSaldodolares(String saldodolares) {
        this.saldodolares = saldodolares;
    }


    public String getTipomonedadolares() {
        return tipomonedadolares;
    }


    public void setTipomonedadolares(String tipomonedadolares) {
        this.tipomonedadolares = tipomonedadolares;
    }


    public String getIdtipocambiosoles() {
        return idtipocambiosoles;
    }


    public void setIdtipocambiosoles(String idtipocambiosoles) {
        this.idtipocambiosoles = idtipocambiosoles;
    }


    public String getIdtipocambiodolares() {
        return idtipocambiodolares;
    }


    public void setIdtipocambiodolares(String idtipocambiodolares) {
        this.idtipocambiodolares = idtipocambiodolares;
    }


    public String getPagosoles() {
        return pagosoles;
    }


    public void setPagosoles(String pagosoles) {
        this.pagosoles = pagosoles;
    }


    public String getPagodolares() {
        return pagodolares;
    }


    public void setPagodolares(String pagodolares) {
        this.pagodolares = pagodolares;
    }
}