package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.util.Configuracion;

/**
 * Created by tkongr on 18/03/2016.
 */
public class AdapListaPedidoPendiente extends ArrayAdapter<BeanEnvPedidoCab> {

    int resource;
    Context context;
    LinkedList<BeanEnvPedidoCab> originalList;

    int cont = 0;
    String flgMoneda;
    String flgNumDecVista;

    public AdapListaPedidoPendiente(Context context, int item, List<BeanEnvPedidoCab> lista) {
        super(context, item, lista);
        originalList = new LinkedList<BeanEnvPedidoCab>();
        originalList.addAll(lista);
        this.resource = item;
        this.context = context;

        flgMoneda = Configuracion.EnumConfiguracion.SIMBOLO_MONEDA.getValor(context);
        flgNumDecVista = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(context);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BeanEnvPedidoCab item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        String nomCliente = "[" + item.getCantidadTotal() + "] "
                + item.getNombreCliente(context);

        ((TextView) nuevaVista
                .findViewById(R.id.actlistapedidopendienteitem_txtnombreCliente))
                .setText(nomCliente);
        ((TextView) nuevaVista
                .findViewById(R.id.actlistapedidopendienteitem_txtfecha))
                .setText(item.getFechaFin());
        ((TextView) nuevaVista
                .findViewById(R.id.actlistapedidopendienteitem_txtmonto))
                .setText(flgMoneda
                        + " "
                        + Utilitario.fnRound(item.getMontoTotal(), Integer
                        .valueOf(flgNumDecVista)));
        ((TextView) nuevaVista
                .findViewById(R.id.actlistapedidopendienteitem_txttipopedido))
                .setBackgroundColor(context.getResources()
                        .getColor(item.getFlgTipo()
                                .equals(Configuracion.FLGPEDIDO)
                                ? R.color.pedidos_online_pedido
                                : R.color.pedidos_online_cotizacion));
        ((TextView) nuevaVista
                .findViewById(R.id.actlistapedidopendienteitem_txttipopedido))
                .setText(item.getFlgTipo());

        return nuevaVista;
    }
}
