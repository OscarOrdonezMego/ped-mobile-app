package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;

import java.util.Properties;

import greendroid.widget.ActionBarGD.Type;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumRolPreferencia;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.ExportDatabaseFileTask;
import pe.com.nextel.android.util.Gps;
import pe.com.nextel.android.util.Logger;
import pe.com.nextel.android.util.NexActivityGestor;
import pe.com.nextel.android.util.NexPhoneStateListener;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexEditText;
import pe.com.nextel.android.widget.NexKeyboardView.OnPerformReturn;
import pe.com.nextel.android.widget.NexNumericKeyboard;
import pe.com.nextel.android.widget.NexNumericKeyboard.NexNumericKeyboardCallback;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.http.HttpLogin;
import pe.entel.android.pedidos.http.HttpSincronizarConfiguracion;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.UtilitarioData;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;
import pe.entel.android.pedidos.widget.NexKeyboardViewPedidos;
import pe.entel.android.verification.util.Service;

public class ActLogin extends NexActivity implements OnPerformReturn, NexNumericKeyboardCallback {

    private NexEditText txtUsuario, txtPassword;
    private CheckBox chkGraUsuario;
    private TextView lblLogVersion;
    private TextView lblLogVersionSuite;
    private TextView link;

    BeanUsuario loBeanUsuario = null;
    private final int CONSDIAINGDATO = 1;
    private final int CONSSALIR = 2;
    private final int CONSMENCONFIGURACION = 1;
    private final int CONSMENSUCURSALES = 2;
    private Button btnadministracion;
    @SuppressWarnings("unused")
    private String isCodUsuRegistrado;

    private NexNumericKeyboard _tecladoNumerico;
    private NexKeyboardViewPedidos _tecladoAlfanumerico;
    private NexPhoneStateListener listener;
    private final int CONSRECUPERARPEDIDO = 5;
    private final byte CONSHTTPLOGIN = 1;
    private final byte CONSHTTPSINCRO = 2;
    private final byte CONSHTTPSUCURSALES = 3;
    private byte iiCurrentHttp;
    private String esAlfanumerico = "F";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Trace myTrace = FirebasePerformance.getInstance().newTrace("ActLogin_onCreate");
        myTrace.start();

        ValidacionServicioUtil.validarServicio(this);
        String lsBeanRepresentante = UtilitarioData.fnObtenerPreferencesString(this, "BeanUsuario");

        if (!lsBeanRepresentante.equals(""))
            loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanRepresentante, BeanUsuario.class);

        super.onCreate(savedInstanceState);

        //if (!Utilitario.fnVerOperador(this))
        //    startActivity(new Intent(this, ActOpeInvalido.class));

        Logger.initialize(getString(R.string.app_name), Utilitario.fnReadParamProperty(this, "LOG_MODE"), Utilitario.fnReadParamProperty(this, "LOG_LEVEL"));

        UtilitarioData.fnCrearPreferencesPutString(this, "MensajeVerificarPedido", "");
        UtilitarioData.fnCrearPreferencesPutString(this, "isHttpVerificarPedido", "");
        UtilitarioData.fnCrearPreferencesPutString(this, "beanPedidoCabVerificarPedido", "");

        this.setMsgOk(getString(R.string.actlogin_dlghttpok));
        this.setMsgError(getString(R.string.actlogin_dlghttperror));
        lblLogVersion.setText(Utilitario.fnVersion(this));
        listener = Utilitario.fnSignalState(this);

        myTrace.stop();
    }

    public void MenuAdministracion() {

        btnadministracion = (Button) findViewById(R.id.administracion);
        btnadministracion.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                Intent loIntent = new Intent(ActLogin.this, ActPreferencias.class);
                if (txtUsuario.getText().toString().equals(getString(R.string.keypreadm_usuario)) && txtPassword.getText().toString().equals(getString(R.string.keypreadm_password))) {
                    loIntent.putExtra(getString(R.string.sp_permiso), EnumRolPreferencia.ADMINISTRADOR.ordinal());
                } else {
                    loIntent.putExtra(getString(R.string.sp_permiso), EnumRolPreferencia.USUARIO.ordinal());
                }
                startActivity(loIntent);
                //  popup.show();//showing popup menu*/
            }
        });//closing the setOnClickListener method*/
    }


    private void validarPrimerInicio() {
        if (((AplicacionEntel) getApplication()).isPrimeraVez()) {
            try {
                SharedPreferences loSharedPreferences = getSharedPreferences(
                        ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
                SharedPreferences.Editor loEditor = loSharedPreferences.edit();
                loEditor.putString("PreferenceVersionSuite", "");
                loEditor.commit();
                iiCurrentHttp = CONSHTTPSINCRO;
                new HttpSincronizarConfiguracion(this, this.fnTipactividad()).execute();
            } catch (Exception e) {
                subShowException(e);
            }
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case CONSDIAINGDATO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.actlogin_dlgingdatos))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        if (txtUsuario.getText().toString().equals("")) {
                                            txtUsuario.requestFocus();
                                        } else if (txtPassword.getText().toString().equals("")) {
                                            txtPassword.requestFocus();
                                        }
                                    }
                                }).create();
                break;
            case CONSSALIR:// SALIR
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsalir))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        try {
                                            Gps.stopListening();
                                        } catch (Exception e) {
                                        }

                                        try {
                                            NServicesDownloadService.stopService(ActLogin.this);
                                        } catch (Exception ex) {
                                            Logger.e(ex);
                                        }

                                        NexActivityGestor.getInstance().exitApplication(ActLogin.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                break;

            case CONSRECUPERARPEDIDO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgrecuperarpedido))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        cargarPedido();
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        startActivity(new Intent(ActLogin.this, ActMenu.class));
                                    }
                                }).create();
                break;

            default:
                return super.onCreateDialog(id);
        }
        return loAlertDialog;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {
        if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK && iiCurrentHttp == CONSHTTPLOGIN) {
            subRealizarLogin();
        } else if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {
            if (iiCurrentHttp == CONSHTTPSUCURSALES) {
                finish();
                startActivity(new Intent(ActLogin.this, ActListaSucursales.class));
            }
            if (iiCurrentHttp == CONSHTTPSINCRO) {
                ((AplicacionEntel) getApplication()).setPrimeraVez(false);
                subSetControles();
                lblLogVersion.setText(Utilitario.fnVersion(this));
                lblLogVersionSuite.setText(getString(R.string.login_suiteversion) + " " + getSharedPreferences(
                        ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("PreferenceVersionSuite", ""));
                txtUsuario.requestFocus();
            }
        }
    }

    private void subRealizarLogin() {
        Trace myTrace = FirebasePerformance.getInstance().newTrace("ActLogin_subRealizarLogin");
        myTrace.start();
        BeanUsuario loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);

        if (isCodUsuRegistrado.equals("")) {

            UtilitarioData.fnCrearPreferencesPutString(this, Configuracion.CONSPRECODUSUARIO, chkGraUsuario.isChecked() ? loBeanUsuario.getUsuario() : "");
            Logger.v(chkGraUsuario.isChecked() ? "checkeado" : "no checkeado");
            UtilitarioData.fnCrearPreferencesPutBoolean(this, Configuracion.CONSPREUSURESETEADO, false);
            UtilitarioData.fnCrearPreferencesPutString(this, Configuracion.CONSPRECODGUARDADO, loBeanUsuario.getUsuario());
        }

        try {
            AplicacionEntel.subMostrarNotificacionVersion(getApplicationContext(), loBeanUsuario);
        } catch (Exception e) {
        }

        InputMethodManager imm = (InputMethodManager) txtPassword.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txtPassword.getWindowToken(), 0);

        logUser(loBeanUsuario);
        myTrace.stop();
        startActivity(new Intent(ActLogin.this, ActMenu.class));

    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(loBeanUsuario.getCodVendedor());
        Crashlytics.setUserEmail(loBeanUsuario.getFlgNServicesObligatorio());
        Crashlytics.setUserName(loBeanUsuario.getNombre());
    }

    private void cargarPedido() {
        ((AplicacionEntel) getApplication()).setCurrentCodFlujo(Configuracion.CONSPEDIDO);
        startActivity(new Intent(ActLogin.this, ActProductoPedido.class));
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setType(Type.Empty);
        String lscliente = Utilitario.fnReadParamProperty(this.getApplicationContext(), "CLIENTE");

        if (!lscliente.equals("")) {
            lscliente = lscliente + " - ";
        }

        this.setTitle(lscliente + getString(R.string.actlogin_bartitulo));
        getActionBarGD().setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        isCodUsuRegistrado = UtilitarioData.fnObtenerPreferencesString(this, Configuracion.CONSPRECODUSUARIO);

        if (isCodUsuRegistrado.equals("")) {
            txtUsuario.setText("");
            chkGraUsuario.setVisibility(View.VISIBLE);
        } else {
            txtUsuario.setText(isCodUsuRegistrado);
            chkGraUsuario.setVisibility(View.GONE);
        }

        txtPassword.setText("");

        Log.v("XXX", "RESUMEN");

        validarPrimerInicio();
    }

    @Override
    public void subSetControles() {

        if (!((AplicacionEntel) getApplication()).isPrimeraVez()) {
            esAlfanumerico = Configuracion.EnumConfiguracion.TECLADO_ALFANUMERICO.getValor(this);
        }

        setActionBarContentView(R.layout.login2);
        MenuAdministracion();
        super.subSetControles();
        txtUsuario = (NexEditText) findViewById(R.id.actlogin_txtUsuario);
        txtPassword = (NexEditText) findViewById(R.id.actlogin_txtPassword);
        chkGraUsuario = (CheckBox) findViewById(R.id.actLogin_chkGraUsuario);
        lblLogVersion = (TextView) findViewById(R.id.lblLogVersion);
        lblLogVersionSuite = (TextView) findViewById(R.id.lblloginSuiteVersion);
        link = (TextView) findViewById(R.id.customactivityoncrash_error_link);
        link.setText(Html.fromHtml("<a href=\"" + Utilitario.readProperties(this).getProperty("PORTAL_AUTOAYUDA") + "\">" + getResources().getString(R.string.customactivityoncrash_error_activity_error_link) + "</a>"));
        link.setClickable(true);
        link.setMovementMethod(LinkMovementMethod.getInstance());

        txtUsuario.setOnEditorActionListener(new DoneOnEditorActionListener());
        txtPassword.setOnEditorActionListener(new DoneOnEditorActionListener());
        _tecladoNumerico = (NexNumericKeyboard) findViewById(R.id.loginNumericokeyboard);
        _tecladoNumerico.setCallback(this);

        _tecladoAlfanumerico = (NexKeyboardViewPedidos) findViewById(R.id.loginAlfanumericokeyboard);

        isCodUsuRegistrado = UtilitarioData.fnObtenerPreferencesString(this, Configuracion.CONSPRECODUSUARIO);

        if (isCodUsuRegistrado.equals(""))// vacio, no grabado o reseteado
        {
            chkGraUsuario.setVisibility(UtilitarioData.fnObtenerPreferencesBoolean(this, Configuracion.CONSPREUSURESETEADO)
                    ? View.VISIBLE : View.GONE);

        } else {
            chkGraUsuario.setVisibility(View.GONE);
            txtUsuario.setText(isCodUsuRegistrado);
            txtPassword.requestFocus();
        }

        if (esAlfanumerico.equals("T")) {
            _tecladoNumerico.setVisibility(View.GONE);
            _tecladoAlfanumerico.setVisibility(View.VISIBLE);
            _tecladoAlfanumerico.registerEditText(txtUsuario);
            _tecladoAlfanumerico.registerEditText(txtPassword, this);

            txtUsuario.setInputType(InputType.TYPE_CLASS_TEXT);
            txtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        } else {
            _tecladoAlfanumerico.setVisibility(View.GONE);
            _tecladoNumerico.setVisibility(View.VISIBLE);
            _tecladoNumerico.registerEditText(txtUsuario);
            _tecladoNumerico.registerEditText(txtPassword);

            txtUsuario.setInputType(InputType.TYPE_CLASS_NUMBER);
            txtPassword.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        }
    }

    private void subConexionHttp() {
        BeanUsuario loBeanUsuario = new BeanUsuario();
        loBeanUsuario.setUsuario(txtUsuario.getText().toString());
        loBeanUsuario.setClave(Utilitario.convertToSHA1(txtPassword.getText().toString()));
        loBeanUsuario.setNumero(Service.getNumeroTelefonico(this));
        iiCurrentHttp = CONSHTTPLOGIN;

        ((AplicacionEntel) getApplication()).uploadEvent(Configuracion.LOGIN_EVENT);
        new HttpLogin(this, loBeanUsuario).execute();
    }

    @SuppressWarnings("deprecation")
    private void subLogueo() {

        if (txtUsuario.getText().toString().equals("") || txtPassword.getText().toString().equals("")) {
            if (!isUsingDialogFragment()) {
                showDialog(CONSDIAINGDATO);
            } else {
                DialogFragmentYesNo loDialog = DialogFragmentYesNo.newInstance(
                        getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIAINGDATO"),
                        getString(R.string.actlogin_dlgingdatos),
                        R.drawable.boton_error, false,
                        EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),                        DialogFragmentYesNo.TAG.concat("CONSDIAINGDATO"));
            }
        } else {
            subConexionHttp();
        }
    }

    private class DoneOnEditorActionListener implements OnEditorActionListener {
        @Override
        public boolean onEditorAction(TextView poTextView, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (poTextView == txtUsuario) {
                    txtPassword.requestFocus();
                } else if (poTextView == txtPassword) {
                    subLogueo();
                }
                return true;
            }
            return false;
        }
    }

    private boolean fnUtilitario() {
        Logger.v("xxxxxx");
        boolean lbResultado = false;

        // Backup de base de datos
        if (txtPassword.getText().toString().toLowerCase().equals(getString(R.string.Backup)))
            lbResultado = utilitarioProceso(getString(R.string.Backup));

        // Eliminar las transaccionales
        if (txtPassword.getText().toString().toLowerCase().equals(getString(R.string.BorrarTemporales)))
            lbResultado = utilitarioProceso(getString(R.string.BorrarTemporales));

        // Restaura base de datos
        if (txtPassword.getText().toString().toLowerCase().equals(getString(R.string.ResBackup)))
            lbResultado = utilitarioProceso(getString(R.string.ResBackup));


        return lbResultado;
    }

    private boolean utilitarioProceso(String caso) {

        if (caso.equals(getString(R.string.Backup))) {
            new ExportDatabaseFileTask(ActLogin.this, getString(R.string.db_name)).execute();

        } else if (caso.equals(getString(R.string.BorrarTemporales))) {
            new NexToast(this, "se elimino las tablas transaccionales", EnumType.ACCEPT).show();

        } else {
            UtilitarioData.fnCrearPreferencesPutBoolean(this, "SinTxt", true);
            new NexToast(this, "sincroniza txt", EnumType.ACCEPT).show();
        }

        txtPassword.setText("");
        return true;

    }


    @Override
    public void onBackPressed() {
        try {
            NServicesDownloadService.stopService(ActLogin.this);
        } catch (Exception ex) {
            Logger.e(ex);
        }
        NexActivityGestor.getInstance().exitApplication(this);
    }

    @Override
    public void performReturn() {

        if (_tecladoNumerico.getCurrentEditText() == txtUsuario) {
            txtPassword.requestFocus();
            return;
        }
        if (fnUtilitario()) {
            return;
        }
        String lsCodGuardado = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                Configuracion.CONSPRECODGUARDADO, "");
        if (!lsCodGuardado.equals(txtUsuario.getText().toString())
                && !txtUsuario.getText().toString().equals("")
                && !txtPassword.getText().toString().equals("")
                && !getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA,
                MODE_PRIVATE).getBoolean(
                Configuracion.CONSPREUSURESETEADO, true)) {
            new NexToast(this, getString(R.string.actlogin_toausunoasociado),
                    EnumType.CANCEL).show();
        } else {
            subLogueo();
        }
    }

    @Override
    public void onPerformReturn(NexEditText nexEditText) {
        if (fnUtilitario()) {
            return;
        }
        String lsCodGuardado = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                Configuracion.CONSPRECODGUARDADO, "");
        if (!lsCodGuardado.equals(txtUsuario.getText().toString())
                && !txtUsuario.getText().toString().equals("")
                && !txtPassword.getText().toString().equals("")
                && !getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA,
                MODE_PRIVATE).getBoolean(
                Configuracion.CONSPREUSURESETEADO, true)) {
            new NexToast(this, getString(R.string.actlogin_toausunoasociado),
                    EnumType.CANCEL).show();
        } else {
            subLogueo();
        }
    }


    private void logUser(BeanUsuario loBeanUsuario) {
        Crashlytics.setString("version_compilada", "2.5.1");
        Properties loProperties = Utilitario.readProperties(this);
        Crashlytics.setString("ip_server", loProperties.getProperty("IP_SERVER"));
        Crashlytics.setString("url_actualizar", loProperties.getProperty("URL_ACTUALIZAR"));

        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier(loBeanUsuario.getUsuario());
        Crashlytics.setUserName(loBeanUsuario.getNombre());
    }

}
