package pe.entel.android.pedidos.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanPresentacion;
import pe.entel.android.pedidos.bean.BeanProducto;
import pe.entel.android.pedidos.bean.BeanRespVerificarPedido;
import pe.entel.android.pedidos.bean.BeanRespVerificarPedidoGeneral;
import pe.entel.android.pedidos.bean.BeanVerificarPedido;
import pe.entel.android.pedidos.bean.BeanVerificarPedidoDetalle;
import pe.entel.android.pedidos.bean.BeanVerificarPedidoMensaje;
import pe.entel.android.pedidos.dal.DalProducto;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumUrl;
import pe.entel.android.pedidos.util.UtilitarioData;

/**
 * Created by rtamayov on 24/09/2015.
 */
public class HttpVerificarPedido extends HttpConexion {

    private BeanVerificarPedido ioBeanVerificarPedido;
    private Context ioContext;

    public HttpVerificarPedido(BeanVerificarPedido poBeanVerificarPedido, Context poContext,
                               ConfiguracionNextel.EnumTipActividad poEnumTipActividad) {
        super(poContext, poContext.getString(R.string.httpverificarpedido_dlg), poEnumTipActividad);
        ioBeanVerificarPedido = poBeanVerificarPedido;
        ioContext = poContext;
    }

    public void subConHttp() {
        try {
            //RestTemplate restTemplate = new RestTemplate();
            //restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

            String lsRequest = new Gson().toJson(ioBeanVerificarPedido);
            Log.v("URL", Configuracion.fnUrl(ioContext, EnumUrl.VERIFICARPEDIDO));
            Log.v("REQUEST", lsRequest);

            //String lsObjeto = restTemplate.postForObject(Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.VERIFICARPEDIDO), ioBeanVerificarPedido, String.class);
            String lsObjeto = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, EnumUrl.VERIFICARPEDIDO), ioBeanVerificarPedido);
            String json = new Gson().toJson(lsObjeto);
            Log.v("RESPONSE", new Gson().toJson(lsObjeto));

            BeanRespVerificarPedido loBeanResponse = (BeanRespVerificarPedido) new Gson().fromJson(lsObjeto, BeanRespVerificarPedido.class);

            setHttpResponseObject(loBeanResponse);

        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, "Error HTTP: " + e.getMessage());
        }
    }

    @Override
    public boolean OnPreConnect() {
        if (!UtilitarioData.fnObtenerPreferencesBoolean(ioContext, "pedidoPendiente")) {
            SharedPreferences loSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ioContext);
            if (loSharedPreferences.getBoolean(ioContext.getString(R.string.keypre_cobertura), false)) {
                setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_modofueracobertura));
                return false;
            }
        }
        if (!Utilitario.fnVerSignal(ioContext)) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    public BeanProducto crearBeanProductoNoSincronizado(BeanVerificarPedidoDetalle verificar) {
        BeanProducto beanProducto = new BeanProducto();
        beanProducto.setId(verificar.getIdArticulo());
        beanProducto.setCodigo(verificar.getCodigoArticulo());
        beanProducto.setNombre(verificar.getNombreArticulo());
        beanProducto.setStock(verificar.getStock());
        beanProducto.setUnidadDefecto("UND");
        beanProducto.setPrecioBase(verificar.getPrecioBase());
        beanProducto.setDescuentoMinimo("0");
        beanProducto.setDescuentoMaximo("0");
        return beanProducto;
    }

    public BeanPresentacion crearBeanPresentacionNoSincronizado(BeanVerificarPedidoDetalle verificar) {
        BeanPresentacion beanPresentacion = new BeanPresentacion();
        beanPresentacion.setId(verificar.getIdArticulo());
        beanPresentacion.setProducto(crearBeanProductoNoSincronizado(verificar));
        beanPresentacion.setNombre(verificar.getNombreArticulo());
        beanPresentacion.setStock(verificar.getStock());
        beanPresentacion.setCantidad(Long.parseLong(verificar.getCantidadPre().trim()));
        beanPresentacion.setPrecioBase(verificar.getPrecioBase());
        beanPresentacion.setDescuentoMinimo("0");
        beanPresentacion.setDescuentoMaximo("0");
        beanPresentacion.setUnidadFraccionamiento(1);
        beanPresentacion.setCodigo(verificar.getCodigoArticulo());
        return beanPresentacion;
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            BeanRespVerificarPedido loBeanResponse = (BeanRespVerificarPedido) getHttpResponseObject();
            int liIdHttprespuesta = loBeanResponse.getCodigo();
            String isPresentacion = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(ioContext);

            BeanProducto beanProducto;
            DalProducto dalProducto = new DalProducto(ioContext);
            List<BeanVerificarPedidoDetalle> listaDetalles = loBeanResponse.getGeneral().getDetallesPedidos();

            if (liIdHttprespuesta == ConfiguracionNextel.CONSRESSERVIDOROK
                    || liIdHttprespuesta == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {

                for (BeanVerificarPedidoDetalle verificar : listaDetalles) {

                    if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                        BeanPresentacion beanPresentacion;
                        if (dalProducto.fnExistePresentacionSincronizado(verificar.getCodigoArticulo())) {
                            beanPresentacion = dalProducto.fnSelxCodigoPresentacion(verificar.getCodigoArticulo());
                        } else {
                            beanPresentacion = crearBeanPresentacionNoSincronizado(verificar);
                        }

                        beanProducto = beanPresentacion.getProducto();
                    } else {
                        if (dalProducto.fnExisteProductoSincronizado(verificar.getCodigoArticulo())) {
                            beanProducto = dalProducto.fnSelxCodProductoxCodigo(verificar.getCodigoArticulo());
                        } else {
                            beanProducto = crearBeanProductoNoSincronizado(verificar);
                        }
                    }
                    verificar.setProducto(beanProducto);
                }
                ((AplicacionEntel) ioContext.getApplicationContext()).setCurrentRespVerificarPedidoGeneral(loBeanResponse.getGeneral());
            } else {
                ((AplicacionEntel) ioContext.getApplicationContext()).setCurrentRespVerificarPedidoGeneral(new BeanRespVerificarPedidoGeneral());
            }

            String codigomensaje = loBeanResponse.getGeneral().getMensajes().get(0).getCodigo();
            UtilitarioData.fnCrearPreferencesPutString(ioContext, "MensajeVerificarPedido", codigomensaje);

            String mensaje = "";
            for (BeanVerificarPedidoMensaje beanmensaje : loBeanResponse.getGeneral().getMensajes()) {
                mensaje = mensaje + "\n" + beanmensaje.getDescripcion();
            }

            setHttpResponseIdMessage(liIdHttprespuesta, mensaje);
        }
    }
}
