package pe.entel.android.pedidos.act;

import android.os.Bundle;

import greendroid.widget.ActionBarGD.Type;
import greendroid.widget.ActionBarItem;
import pe.com.nextel.android.R;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.util.NexActivityGestor;

public class ActOpeInvalido extends NexActivity {
    private final int CONSICOSALIR = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setType(Type.Empty);
        this.setTitle(getString(R.string.actopeinvalido_bartitulo));
        addActionBarItem(greendroid.widget.ActionBarItem.Type.Export, CONSICOSALIR);
    }

    @Override
    protected void subSetControles() {
        setActionBarContentView(R.layout.opeinvalido);
    }

    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        switch (item.getItemId()) {
            case CONSICOSALIR:
                NexActivityGestor.getInstance().exitApplication(this);
                break;
            default:
                return super.onHandleActionBarItemClick(item, position);
        }
        return true;
    }
}

