package pe.entel.android.pedidos.bean;

/**
 * Created by tkongr on 07/01/2016.
 */
public class BeanSucursal {

    private String idSucursal = "";
    private String codigo = "";
    private String nombre = "";
    private String url = "";

    public BeanSucursal() {
    }

    public String getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(String idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
