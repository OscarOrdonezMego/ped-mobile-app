
package pe.entel.android.pedidos.bean;

/**
 * Created by Dsb Mobile on 14/05/2015.
 */
public class BeanFraccionamiento {

    private long numerador;
    private long denomindador;
    private double division;
    private String mostrarDivision;
    private String unidadDeferecto;

    public void fnActualizarDivision() {
        if (numerador == 0 || denomindador == 0)
            division = 0;
        else
            division = (double) numerador / denomindador;
    }

    @Override
    public String toString() {
        if (numerador == 0 || denomindador == 0)
            return "Sin Fraccionamiento";
        else
            return numerador + " " + unidadDeferecto + " (" + numerador + "/" + denomindador + ")";
    }

    public boolean fraccionamientoValido(double pdUnidadFraccionamiento) {
        return ((division * denomindador) % pdUnidadFraccionamiento) == 0;
    }

    public double fnObtenerMontoFracc(double precio) {
        return division * precio;
    }

    public long getNumerador() {
        return numerador;
    }

    public void setNumerador(long numerador) {
        this.numerador = numerador;
    }

    public long getDenomindador() {
        return denomindador;
    }

    public void setDenomindador(long denomindador) {
        this.denomindador = denomindador;
    }

    public double getDivision() {
        return division;
    }

    public void setDivision(double division) {
        this.division = division;
    }

    public String getMostrarDivision() {
        return mostrarDivision;
    }

    public void setMostrarDivision(String mostrarDivision) {
        this.mostrarDivision = mostrarDivision;
    }

    public String getUnidadDeferecto() {
        return unidadDeferecto;
    }

    public void setUnidadDeferecto(String unidadDeferecto) {
        this.unidadDeferecto = unidadDeferecto;
    }
}
