package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanClienteDireccion;
import pe.entel.android.pedidos.bean.BeanProspecto;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.widget.HeadedTextItem;

public class DalCliente {

    private Context ioContext;

    public DalCliente(Context psClase) {

        ioContext = psClase;
    }

    public boolean existeTable() {
        boolean lbResult = false;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        String[] laColumnasMed = new String[]{"name"};
        String lsTabla = "sqlite_master";
        String lsWhere = "upper(name)='TBL_CLIENTE'";
        Cursor loCursor = myDB.query(true, lsTabla, laColumnasMed, lsWhere,
                null, null, null, null, null);
        if (loCursor.getCount() == 1) {
            lbResult = true;
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return lbResult;
    }

    public boolean existeTableDireccion() {
        boolean lbResult = false;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        String[] laColumnasMed = new String[]{"name"};
        String lsTabla = "sqlite_master";
        String lsWhere = "upper(name)='TBL_DIRECCION'";
        Cursor loCursor = myDB.query(true, lsTabla, laColumnasMed, lsWhere,
                null, null, null, null, null);
        if (loCursor.getCount() == 1) {
            lbResult = true;
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return lbResult;
    }

    public HeadedTextItem[] fnBuscarCliente(String psFiltro,
                                            EnumTipBusqueda poTipBusqueda, BeanUsuario poUsuario) {
        HeadedTextItem loHeaValor[];
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        boolean lbPriLetra = true;

        String[] laColumnas = new String[]{"cli_pk", "cli_cod",
                "upper(cli_Nombre)"};
        String lsTablas = "tbl_cliente";

        String lsWhere = (poTipBusqueda == EnumTipBusqueda.NOMBRE) ? "upper(cli_Nombre) like "
                + "'%" + ((lbPriLetra) ? "" : "%") + psFiltro + "%'"
                : "trim((cli_cod)) = " + "'" + psFiltro.toUpperCase() + "' ";
        Cursor loCursor;

        if (psFiltro.equals("")) {
            loCursor = myDB.query(lsTablas, laColumnas, null, null, null, null,
                    "cli_Nombre");
        } else {
            loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                    null, "cli_Nombre");
        }
        loHeaValor = new HeadedTextItem[loCursor.getCount()];

        if (loCursor != null) {
            int liContador = 0;
            if (loCursor.moveToFirst()) {
                do {
                    loHeaValor[liContador] = new HeadedTextItem(
                            loCursor.getString(1) + " - "
                                    + loCursor.getString(2));
                    loHeaValor[liContador].setTag(loCursor.getLong(0));
                    liContador++;

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loHeaValor;
    }

    public ArrayList<HeadedTextItem> fnBuscarListCliente(String psFiltro,
                                                         EnumTipBusqueda poTipBusqueda, BeanUsuario poUsuario) {
        ArrayList<HeadedTextItem> loHeaValor = new ArrayList<HeadedTextItem>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        boolean lbPriLetra = true;

        String[] laColumnas = new String[]{"cli_pk", "cli_cod",
                "upper(cli_Nombre)"};
        String lsTablas = "tbl_cliente";

        String lsWhere = (poTipBusqueda == EnumTipBusqueda.NOMBRE) ? "upper(cli_Nombre) like "
                + "'%" + ((lbPriLetra) ? "" : "%") + psFiltro + "%'"
                : "trim((cli_cod)) = " + "'" + psFiltro.toUpperCase() + "' ";
        Cursor loCursor;

        if (psFiltro.equals("")) {
            loCursor = myDB.query(lsTablas, laColumnas, null, null, null, null,
                    "cli_Nombre");
        } else {
            loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                    null, "cli_Nombre");
        }

        if (loCursor != null) {
            int liContador = 0;
            if (loCursor.moveToFirst()) {
                do {
                    HeadedTextItem he = new HeadedTextItem(
                            (loCursor.getString(1) + " | " + loCursor
                                    .getString(2)).toUpperCase());
                    he.setTag(loCursor.getLong(0));

                    loHeaValor.add(he);

                    liContador++;

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loHeaValor;
    }

    public BeanCliente fnSelxIdCliente(long piIdCliente) {
        BeanCliente loBeanCliente = new BeanCliente();
        SQLiteDatabase myDB = null;

        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"CLI_PK", "cli_cod", "cli_Nombre",
                "cli_direccion", "tcli_cod",
                "cli_Giro", "cli_deuda", "estado",
                "ped_fecregistro", "usr_codigo", "CLI_CAMPO_ADICIONAL_1", "CLI_CAMPO_ADICIONAL_2",
                "CLI_CAMPO_ADICIONAL_3", "CLI_CAMPO_ADICIONAL_4", "CLI_CAMPO_ADICIONAL_5", "SALDO_CREDITO", "latitud", "longitud",
                "tcli_nombre", "CLI_DEUDADOLARES"
        };

        String lsTablas = "tbl_cliente";
        String lsWhere = "cli_pk = " + String.valueOf(piIdCliente);
        Cursor loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanCliente.setClipk(loCursor.getString(0));
                    loBeanCliente.setClicod(loCursor.getString(1));
                    loBeanCliente.setClinom(loCursor.getString(2));
                    loBeanCliente.setClidireccion(loCursor.getString(3));
                    loBeanCliente.setTclicod(loCursor.getString(4));
                    loBeanCliente.setCligiro(loCursor.getString(5));
                    loBeanCliente.setClideuda(loCursor.getString(6));
                    loBeanCliente.setEstado(loCursor.getString(7));
                    loBeanCliente.setFecUltPedido(loCursor.getString(8));
                    loBeanCliente.setCodUsrUltPedido(loCursor.getString(9));
                    loBeanCliente.setCampoAdicional1(loCursor.getString(10));
                    loBeanCliente.setCampoAdicional2(loCursor.getString(11));
                    loBeanCliente.setCampoAdicional3(loCursor.getString(12));
                    loBeanCliente.setCampoAdicional4(loCursor.getString(13));
                    loBeanCliente.setCampoAdicional5(loCursor.getString(14));
                    loBeanCliente.setSaldoCredito(String.valueOf(loCursor.getFloat(15)));
                    loBeanCliente.setLatitud(loCursor.getString(16));
                    loBeanCliente.setLongitud(loCursor.getString(17));
                    loBeanCliente.setTclinombre(loCursor.getString(18));
                    loBeanCliente.setClideudadolares(loCursor.getString(19));

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return loBeanCliente;
    }

    public boolean fnUpdFecUltPed(String strFechaFin, String strCodCliente,
                                  String strNombreUsuario) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();

        loStbCab.append("UPDATE TBL_CLIENTE SET PED_FECREGISTRO='"
                + strFechaFin + "' , USR_CODIGO = '" + strNombreUsuario
                + "' WHERE CLI_COD = '" + strCodCliente + "'");

        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;

    }

    public BeanCliente fnSelxCodCliente(String piCodCliente) {
        BeanCliente loBeanCliente = new BeanCliente();
        SQLiteDatabase myDB = null;

        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"CLI_PK", "cli_cod", "cli_Nombre",
                "cli_direccion", "tcli_cod",
                "cli_Giro", "cli_deuda", "estado",
                "ped_fecregistro", "usr_codigo", "CLI_CAMPO_ADICIONAL_1", "CLI_CAMPO_ADICIONAL_2",
                "CLI_CAMPO_ADICIONAL_3", "CLI_CAMPO_ADICIONAL_4", "CLI_CAMPO_ADICIONAL_5", "SALDO_CREDITO", "latitud", "longitud",
                "tcli_nombre", "CLI_DEUDADOLARES"
        };

        String lsTablas = "tbl_cliente";
        String lsWhere = "cli_cod = '" + piCodCliente + "'";
        Cursor loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {

                    loBeanCliente.setClipk(loCursor.getString(0));
                    loBeanCliente.setClicod(loCursor.getString(1));
                    loBeanCliente.setClinom(loCursor.getString(2));
                    loBeanCliente.setClidireccion(loCursor.getString(3));
                    loBeanCliente.setTclicod(loCursor.getString(4));
                    loBeanCliente.setCligiro(loCursor.getString(5));
                    loBeanCliente.setClideuda(loCursor.getString(6));
                    loBeanCliente.setEstado(loCursor.getString(7));
                    loBeanCliente.setFecUltPedido(loCursor.getString(8));
                    loBeanCliente.setCodUsrUltPedido(loCursor.getString(9));
                    loBeanCliente.setCampoAdicional1(loCursor.getString(10));
                    loBeanCliente.setCampoAdicional2(loCursor.getString(11));
                    loBeanCliente.setCampoAdicional3(loCursor.getString(12));
                    loBeanCliente.setCampoAdicional4(loCursor.getString(13));
                    loBeanCliente.setCampoAdicional5(loCursor.getString(14));
                    loBeanCliente.setSaldoCredito(String.valueOf(loCursor.getFloat(15)));
                    loBeanCliente.setLatitud(loCursor.getString(16));
                    loBeanCliente.setLongitud(loCursor.getString(17));
                    loBeanCliente.setTclinombre(loCursor.getString(18));
                    loBeanCliente.setClideudadolares(loCursor.getString(19));

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return loBeanCliente;
    }


    public int fnSelNumClienteDireccion(String psCodCLiente, String psTipo) {
        SQLiteDatabase myDB = null;

        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"count(DIR_PK)"};
        String lsTablas = "TBL_DIRECCION";

        String lsWhere = "CLI_CODIGO = ?";
        String[] lsWhereArgs;

        if (!TextUtils.isEmpty(psTipo)) {
            lsWhere += " AND DIR_TIPO = ?";
            lsWhereArgs = new String[]{psCodCLiente, psTipo};
        } else lsWhereArgs = new String[]{psCodCLiente};

        Cursor loCursor = myDB.query(lsTablas, laColumnas, lsWhere, lsWhereArgs, null, null, null);

        try {
            if (loCursor != null) {
                if (loCursor.moveToFirst()) {
                    do {
                        Log.i("INTEGER", loCursor.getString(0));
                        return Integer.parseInt(loCursor.getString(0));
                    } while (loCursor.moveToNext());
                }
            }
            return 0;
        } finally {
            loCursor.deactivate();
            loCursor.close();
            myDB.close();
        }
    }


    public BeanClienteDireccion fnSelDireccionxCodInc(String psCodInc) {
        BeanClienteDireccion loBeanClienteDireccion = null;
        SQLiteDatabase loDB = null;

        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"CLI_CODIGO", "DIR_NOMBRE", "DIR_TIPO",
                "FLGENVIADO", "DIR_LATITUD", "DIR_LONGITUD", "FECCREACION", "DIR_COD"
        };
        String lsTablas = "TBL_DIRECCION";
        String lsWhere = "DIR_INC = " + psCodInc + "";
        Cursor loCursor = loDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanClienteDireccion = new BeanClienteDireccion();
                    loBeanClienteDireccion.setCodCliente(loCursor.getString(0));
                    loBeanClienteDireccion.setNombre(loCursor.getString(1));
                    loBeanClienteDireccion.setTipo(loCursor.getString(2));
                    loBeanClienteDireccion.setFlgEnviado(loCursor.getString(3));
                    loBeanClienteDireccion.setLatitud(loCursor.getString(4));
                    loBeanClienteDireccion.setLongitud(loCursor.getString(5));
                    loBeanClienteDireccion.setFechaCreacion(loCursor.getString(6));
                    loBeanClienteDireccion.setCodDireccion(loCursor.getString(7));
                    loBeanClienteDireccion.setCodInc(psCodInc);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return loBeanClienteDireccion;
    }

    public List<BeanClienteDireccion> fnEnvioDireccionPendientes() {
        List<BeanClienteDireccion> laBeanClienteDirecciones = new ArrayList<BeanClienteDireccion>();
        BeanClienteDireccion loBeanClienteDireccion = null;
        SQLiteDatabase loDB = null;

        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"CLI_CODIGO", "DIR_NOMBRE", "DIR_TIPO",
                "FLGENVIADO", "DIR_LATITUD", "DIR_LONGITUD", "FECCREACION", "DIR_COD"
        };
        String lsTablas = "TBL_DIRECCION";
        String lsWhere = "FLGENVIADO = '" + Configuracion.FLGFALSO + "'";
        Cursor loCursor = loDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanClienteDireccion = new BeanClienteDireccion();
                    loBeanClienteDireccion.setCodCliente(loCursor.getString(0));
                    loBeanClienteDireccion.setNombre(loCursor.getString(1));
                    loBeanClienteDireccion.setTipo(loCursor.getString(2));
                    loBeanClienteDireccion.setFlgEnviado(loCursor.getString(3));
                    loBeanClienteDireccion.setLatitud(loCursor.getString(4));
                    loBeanClienteDireccion.setLongitud(loCursor.getString(5));
                    loBeanClienteDireccion.setFechaCreacion(loCursor.getString(6));
                    loBeanClienteDireccion.setCodDireccion(loCursor.getString(7));
                    laBeanClienteDirecciones.add(loBeanClienteDireccion);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return laBeanClienteDirecciones;
    }


    public String fnGrabarDireccion(BeanClienteDireccion psBean) {
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String lsId = String.valueOf(System.currentTimeMillis());

        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("INSERT INTO TBL_DIRECCION ("
                + "CLI_CODIGO,DIR_NOMBRE,DIR_TIPO,FLGENVIADO, DIR_LATITUD, DIR_LONGITUD, " +
                "FECCREACION) VALUES ('");
        loStbCab.append(psBean.getCodCliente());
        loStbCab.append("','");
        loStbCab.append(psBean.getNombre());
        loStbCab.append("','");
        loStbCab.append(psBean.getTipo());
        loStbCab.append("','");
        loStbCab.append(psBean.getFlgEnviado());
        loStbCab.append("','");
        loStbCab.append(psBean.getLatitud());
        loStbCab.append("','");
        loStbCab.append(psBean.getLongitud());
        loStbCab.append("','");
        loStbCab.append(psBean.getFechaCreacion())
                .append("')");

        try {
            loDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            Log.e("DalCliente", "fnGrabarDireccion", e);
            lsId = "";
        } finally {
            loDB.close();
        }
        return lsId;
    }

    public boolean fnUpdEnviadoDireccion(String poPk, String psCodDireccion, String psCodInc) {
        boolean lbResult = true;
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TBL_DIRECCION SET FLGENVIADO = '"
                + Configuracion.FLGVERDADERO + "', DIR_PK = '" + poPk + "',  DIR_COD = '" + psCodDireccion
                + "' WHERE DIR_INC = " + psCodInc + "");

        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {

            loDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            Log.e("DalCliente", "fnGrabarDireccion", e);
            lbResult = false;
        }
        loDB.close();
        return lbResult;
    }

    public boolean fnUpdEnviadoDireccion(String poPk, String psCodDireccion, String poCodCLiente,
                                         String poFechaCreacion) {
        boolean lbResult = true;
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TBL_DIRECCION SET FLGENVIADO = '"
                + Configuracion.FLGVERDADERO + "', DIR_PK = '" + poPk + "', DIR_COD = '"
                + psCodDireccion + "' WHERE CLI_CODIGO = '" + poCodCLiente + "' AND FECCREACION = '"
                + poFechaCreacion + "'");

        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            loDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            Log.e("DalCliente", "fnGrabarDireccion", e);

            lbResult = false;
        }
        loDB.close();
        return lbResult;
    }

    public LinkedList<BeanClienteDireccion> fnSelDirCliente(String psCodCLiente, String psTipo) {

        SQLiteDatabase myDB = null;
        LinkedList<BeanClienteDireccion> loLista = null;

        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"DIR_PK", "DIR_NOMBRE",
                "CLI_CODIGO", "DIR_TIPO", "FLGENVIADO", "DIR_COD", "DIR_INC"};
        String lsTablas = "TBL_DIRECCION";
        String lsWhere = "CLI_CODIGO = ?";
        String[] lsWhereArgs;

        if (!TextUtils.isEmpty(psTipo)) {
            lsWhere += " AND DIR_TIPO = ?";
            lsWhereArgs = new String[]{psCodCLiente, psTipo};
        } else lsWhereArgs = new String[]{psCodCLiente};


        Cursor loCursor;

        loCursor = myDB.query(lsTablas, laColumnas, lsWhere,
                lsWhereArgs, null, null, "DIR_NOMBRE desc");

        loLista = new LinkedList<BeanClienteDireccion>();

        BeanClienteDireccion bean;
        if (loCursor != null) {

            if (loCursor.moveToFirst()) {
                do {
                    bean = new BeanClienteDireccion();
                    bean.setPk(loCursor.getString(0));
                    bean.setNombre(loCursor.getString(1));
                    bean.setCodCliente(loCursor.getString(2));
                    bean.setTipo(loCursor.getString(3));
                    bean.setFlgEnviado(loCursor.getString(4));
                    bean.setCodDireccion(loCursor.getString(5));
                    bean.setCodInc(loCursor.getString(6));
                    loLista.add(bean);
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loLista;
    }

    /**
     * Actualiza el estado de la visita del cliente
     *
     * @param piIdCliente identificador de cliente
     * @param estado      el nuevo estado
     * @return si se realizo correctamente
     */
    public boolean fnUpdEstadoVisitaCliente(long piIdCliente, String estado) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE tbl_cliente SET estado='").append(estado)
                .append("' WHERE cli_pk = " + piIdCliente);
        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    /**
     * Actualiza la deuda del cliente
     *
     * @param piIdCliente identificador de cliente
     * @param monto       la nueva deuda
     * @return si se realizo correctamente
     */
    public boolean fnUpdDeudaCliente(long piIdCliente, String monto, String tipMoneda) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        if(tipMoneda.equals("1")) {
            loStbCab.append("UPDATE tbl_cliente SET CLI_DEUDA='").append(monto).append("' WHERE cli_pk = " + piIdCliente);
        }
        else{
            loStbCab.append("UPDATE tbl_cliente SET CLI_DEUDADOLARES='").append(monto).append("' WHERE cli_pk = " + piIdCliente);
        }
        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    /**
     * Devuelve el total de clientes de la ruta
     *
     * @return el total de clientes sincronizado
     */
    public int fnSelNumCliente() {
        SQLiteDatabase myDB = null;

        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"count(CLI_PK)"};
        String lsTablas = "tbl_cliente";
        Cursor loCursor = myDB.query(lsTablas, laColumnas, null, null, null,
                null, null);

        try {
            if (loCursor != null) {
                if (loCursor.moveToFirst()) {
                    do {
                        return Integer.parseInt(loCursor.getString(0));
                    } while (loCursor.moveToNext());
                }
            }
            return 0;
        } finally {
            loCursor.deactivate();
            loCursor.close();
            myDB.close();
        }
    }

    public boolean fnUpdSaldoCredito(String cli_codigo, String nuevoSaldo) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();

        loStbCab.append("UPDATE TBL_CLIENTE SET SALDO_CREDITO = '"
                + nuevoSaldo + "' WHERE CLI_COD = '" + cli_codigo + "'");

        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;

    }

    public String fnGrabarProspecto(BeanProspecto prospecto) {
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String lsId = String.valueOf(System.currentTimeMillis());

        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("INSERT INTO TrProspecto ("
                + "Id,Codigo,Nombre,Direccion,TcliCod,Giro,Latitud,Longitud,UserCod,CampoAdic1,CampoAdic2,CampoAdic3,CampoAdic4,CampoAdic5,FechaMovil,Observacion,FlgEnviado) VALUES ('");
        loStbCab.append(lsId);
        loStbCab.append("','");
        loStbCab.append(prospecto.getCodigo());
        loStbCab.append("','");
        loStbCab.append(prospecto.getNombre());
        loStbCab.append("','");
        loStbCab.append(prospecto.getDireccion());
        loStbCab.append("','");
        loStbCab.append(prospecto.getTcli_cod());
        loStbCab.append("','");
        loStbCab.append(prospecto.getGiro());
        loStbCab.append("','");
        loStbCab.append(prospecto.getLatitud());
        loStbCab.append("','");
        loStbCab.append(prospecto.getLongitud());
        loStbCab.append("','");
        loStbCab.append(prospecto.getCodUsuario());
        loStbCab.append("','");
        loStbCab.append(prospecto.getCampoAdicional1());
        loStbCab.append("','");
        loStbCab.append(prospecto.getCampoAdicional2());
        loStbCab.append("','");
        loStbCab.append(prospecto.getCampoAdicional3());
        loStbCab.append("','");
        loStbCab.append(prospecto.getCampoAdicional4());
        loStbCab.append("','");
        loStbCab.append(prospecto.getCampoAdicional5());
        loStbCab.append("','");
        loStbCab.append(prospecto.getFecha());
        loStbCab.append("','");
        loStbCab.append(prospecto.getObservacion());
        loStbCab.append("','");
        loStbCab.append(Configuracion.FLGFALSO)
                .append("')");

        Log.v("XXX", "grabando prospecto..." + loStbCab.toString());
        try {
            loDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            Log.e("DalCliente", "fnGrabarProspecto", e);
            lsId = "";
        } finally {
            loDB.close();
        }
        return lsId;
    }

    public List<BeanProspecto> fnEnvioProspectosPendientes() {
        List<BeanProspecto> prospectos = new ArrayList<BeanProspecto>();
        BeanProspecto prospecto = null;
        SQLiteDatabase loDB = null;

        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"Codigo", "Nombre", "Direccion",
                "TcliCod", "Giro", "Latitud", "Longitud", "UserCod", "CampoAdic1",
                "CampoAdic2", "CampoAdic3", "CampoAdic4", "CampoAdic5", "FechaMovil", "Observacion"
        };
        String lsTablas = "TrProspecto";
        String lsWhere = "FlgEnviado = '" + Configuracion.FLGFALSO + "'";
        Cursor loCursor = loDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    prospecto = new BeanProspecto();
                    prospecto.setCodigo(loCursor.getString(0));
                    prospecto.setNombre(loCursor.getString(1));
                    prospecto.setDireccion(loCursor.getString(2));
                    prospecto.setTcli_cod(loCursor.getString(3));
                    prospecto.setGiro(loCursor.getString(4));
                    prospecto.setLatitud(loCursor.getString(5));
                    prospecto.setLongitud(loCursor.getString(6));
                    prospecto.setCodUsuario(loCursor.getString(7));
                    prospecto.setCampoAdicional1(loCursor.getString(8));
                    prospecto.setCampoAdicional2(loCursor.getString(9));
                    prospecto.setCampoAdicional3(loCursor.getString(10));
                    prospecto.setCampoAdicional4(loCursor.getString(11));
                    prospecto.setCampoAdicional5(loCursor.getString(12));
                    prospecto.setFecha(loCursor.getString(13));
                    prospecto.setObservacion(loCursor.getString(14));
                    prospectos.add(prospecto);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return prospectos;
    }

    public boolean fnUpdEstadoProspectoPendiente(BeanUsuario piUsuario) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrProspecto SET FlgEnviado='"
                + Configuracion.FLGVERDADERO + "'" + "WHERE FlgEnviado='"
                + Configuracion.FLGFALSO + "' and UserCod = '"
                + piUsuario.getCodVendedor() + "'");
        Log.v("XXX", "actualizando Pend prospecto..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());

        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnUpdEstadoProspectoPendienteXId(String IdProspecto) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrProspecto SET FlgEnviado='"
                + Configuracion.FLGVERDADERO + "'" + "WHERE Id = '"
                + IdProspecto + "'");
        Log.v("XXX", "actualizando Pend prospecto..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());

        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

}