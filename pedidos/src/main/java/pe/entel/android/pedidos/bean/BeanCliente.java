package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanCliente {

    @JsonProperty
    private String clipk;
    @JsonProperty
    private String clicod;
    @JsonProperty
    private String clinom;
    @JsonProperty
    private String clidireccion;
    @JsonProperty
    private String tclicod;
    @JsonProperty
    private String tclinombre;
    @JsonProperty
    private String cligiro;
    @JsonProperty
    private String clideuda;
    @JsonProperty
    private String clideudadolares;
    @JsonProperty
    private String estado;
    @JsonProperty
    private String fecUltPedido;
    @JsonProperty
    private String codUsrUltPedido;
    @JsonProperty
    private String online = "0";
    @JsonProperty
    private int idResultado;
    @JsonProperty
    private String resultado;
    @JsonProperty
    private String campoAdicional1;
    @JsonProperty
    private String campoAdicional2;
    @JsonProperty
    private String campoAdicional3;
    @JsonProperty
    private String campoAdicional4;
    @JsonProperty
    private String campoAdicional5;
    @JsonProperty
    private String latitud;
    @JsonProperty
    private String longitud;
    @JsonProperty
    private String saldoCredito;

    @JsonProperty
    private String codUsuario;


    public String getTclinombre() {
        return tclinombre;
    }

    public void setTclinombre(String tclinombre) {
        this.tclinombre = tclinombre;
    }

    public String getCodUsuario() {
        return codUsuario;
    }

    public void setCodUsuario(String codUsuario) {
        this.codUsuario = codUsuario;
    }

    public String getSaldoCredito() {
        return saldoCredito;
    }

    public void setSaldoCredito(String saldoCredito) {
        this.saldoCredito = saldoCredito;
    }

    public String getClipk() {
        return clipk;
    }

    public void setClipk(String clipk) {
        this.clipk = clipk;
    }

    public String getClicod() {
        return clicod;
    }

    public void setClicod(String codigo) {
        clicod = codigo;
    }

    public String getClinom() {
        return clinom;
    }

    public void setClinom(String nombre) {
        clinom = nombre;
    }

    public String getClidireccion() {
        return clidireccion;
    }

    public void setClidireccion(String clidireccion) {
        this.clidireccion = clidireccion;
    }

    public String getTclicod() {
        return tclicod;
    }

    public void setTclicod(String tclicod) {
        this.tclicod = tclicod;
    }

    public String getCligiro() {
        return cligiro;
    }

    public void setCligiro(String giro) {
        cligiro = giro;
    }

    public String getClideuda() {
        return clideuda;
    }

    public String getFecUltPedido() {
        return fecUltPedido;
    }

    public void setFecUltPedido(String fecUltPedido) {
        this.fecUltPedido = fecUltPedido;
    }

    public String getCodUsrUltPedido() {
        return codUsrUltPedido;
    }

    public void setCodUsrUltPedido(String codUsrUltPedido) {
        this.codUsrUltPedido = codUsrUltPedido;
    }

    public void setClideuda(String clideuda) {
        this.clideuda = clideuda;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the online
     */
    public String getOnline() {
        return online;
    }

    /**
     * @param online the online to set
     */
    public void setOnline(String online) {
        this.online = online;
    }

    /**
     * @return the idResultado
     */
    public int getIdResultado() {
        return idResultado;
    }

    /**
     * @param idResultado the idResultado to set
     */
    public void setIdResultado(int idResultado) {
        this.idResultado = idResultado;
    }

    /**
     * @return the resultado
     */
    public String getResultado() {
        return resultado;
    }

    /**
     * @param resultado the resultado to set
     */
    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getCampoAdicional1() {
        return campoAdicional1;
    }

    public void setCampoAdicional1(String campoAdicional1) {
        this.campoAdicional1 = campoAdicional1;
    }

    public String getCampoAdicional2() {
        return campoAdicional2;
    }

    public void setCampoAdicional2(String campoAdicional2) {
        this.campoAdicional2 = campoAdicional2;
    }

    public String getCampoAdicional3() {
        return campoAdicional3;
    }

    public void setCampoAdicional3(String campoAdicional3) {
        this.campoAdicional3 = campoAdicional3;
    }

    public String getCampoAdicional4() {
        return campoAdicional4;
    }

    public void setCampoAdicional4(String campoAdicional4) {
        this.campoAdicional4 = campoAdicional4;
    }

    public String getCampoAdicional5() {
        return campoAdicional5;
    }

    public void setCampoAdicional5(String campoAdicional5) {
        this.campoAdicional5 = campoAdicional5;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public void setClideudadolares(String clideudadolares) {
        this.clideudadolares = clideudadolares;
    }

    public String getClideudadolares() {
        return clideudadolares;
    }
}