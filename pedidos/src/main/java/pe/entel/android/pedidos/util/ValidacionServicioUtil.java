package pe.entel.android.pedidos.util;

import android.content.Context;

import pe.entel.android.verification.util.Service;

/**
 * Created by jvasqueza on 18/06/2015.
 */
public class ValidacionServicioUtil {

    public static void validarServicio(Context poContext) {
        Service.validateActivity(poContext);
    }

    public static void startServiceTransaction(Context poContext) {
        Service.startServiceTransaction(poContext, Configuracion.fnUrl(poContext, Configuracion.EnumUrl.VERIFICAR));
    }

}
