package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.util.Configuracion;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanEnvItemDet {
    public String idArticulo;
    @JsonProperty
    private String codigoArticulo;
    @JsonProperty
    private String cantidad;
    @JsonProperty
    private String codigoMotivo;
    @JsonProperty
    private String nombreMotivo;
    @JsonProperty
    private String fechaVencimiento;
    @JsonProperty
    private String observacion;
    @JsonProperty
    private String codAlmacen;
    @JsonProperty
    private String cantidadPre;
    @JsonProperty
    private String cantidadFrac;
    @JsonProperty
    private String tipoArticulo;
    @JsonIgnore
    private BeanProducto producto;
    @JsonIgnore
    public String nombreArticulo;


    public BeanEnvItemDet() {
        idArticulo = "";
        codigoArticulo = "";
        cantidad = "";
        codigoMotivo = "";
        nombreMotivo = "";
        fechaVencimiento = "";
        observacion = "";
        nombreArticulo = "";
        codAlmacen = "0";
        cantidadPre = "0";
        cantidadFrac = "0";
        tipoArticulo = Configuracion.TipoArticulo.PRODUCTO;
        producto = new BeanProducto();
    }


    public String fnMostrarArticulo(int pdNumDecimales) {
        if (Configuracion.TipoArticulo.PRODUCTO.equals(tipoArticulo)) {
            return "[" + Utilitario.fnRound(this.cantidad, pdNumDecimales) + "] " + this.getNombreArticulo();
        } else
            return this.nombreArticulo + " - " + this.cantidadPre + " " + this.producto.getUnidadDefecto();
    }

    public String fnMostrarArticuloPro() {
        return this.producto.getCodigo() + " - " + this.producto.getNombre();
    }

    public String getIdArticulo() {
        return idArticulo;
    }


    public void setIdArticulo(String idArticulo) {
        this.idArticulo = idArticulo;
    }


    public String getCodigoArticulo() {
        return codigoArticulo;
    }

    public void setCodigoArticulo(String codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }

    public String getCantidad() {
        return cantidad;
    }


    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }


    public String getCodigoMotivo() {
        return codigoMotivo;
    }


    public void setCodigoMotivo(String codigoMotivo) {
        this.codigoMotivo = codigoMotivo;
    }

    public String getNombreMotivo() {
        return nombreMotivo;
    }

    public void setNombreMotivo(String nombreMotivo) {
        this.nombreMotivo = nombreMotivo;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }


    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }


    public String getObservacion() {
        return observacion;
    }


    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }


    public String getNombreArticulo() {
        return nombreArticulo;
    }


    public void setNombreArticulo(String nombreArticulo) {
        this.nombreArticulo = nombreArticulo;
    }

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public String getCantidadPre() {
        return cantidadPre;
    }

    public void setCantidadPre(String cantidadPre) {
        this.cantidadPre = cantidadPre;
    }

    public String getCantidadFrac() {
        return cantidadFrac;
    }

    public void setCantidadFrac(String cantidadFrac) {
        this.cantidadFrac = cantidadFrac;
    }

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }

    public BeanProducto getProducto() {
        return producto;
    }

    public void setProducto(BeanProducto producto) {
        this.producto = producto;
    }
}
