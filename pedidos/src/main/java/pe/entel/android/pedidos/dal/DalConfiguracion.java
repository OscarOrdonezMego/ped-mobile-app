package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.util.Configuracion;

/**
 * Created by Dsb Mobile on 23/03/2015.
 */
public class DalConfiguracion {

    private final String TAG = DalConfiguracion.class.getSimpleName();
    private Context ioContext;

    public DalConfiguracion(Context psClase) {
        ioContext = psClase;
    }

    public String fnValorXCodigo(String psCodigo, String psColumna) {

        SQLiteDatabase loDB = null;
        String lsValor = "";

        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{psColumna};
        String lsTablas = Configuracion.NAME_TABLE_CONFIGURACION;
        String lsWhere = Configuracion.NAME_COLUMN_CODIGO + " = ?";
        String[] lsWhereArgs = new String[]{psCodigo};

        Cursor loCursor;

        loCursor = loDB.query(lsTablas, laColumnas, lsWhere, lsWhereArgs, null, null,
                null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    lsValor = loCursor.getString(0);
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();

        return lsValor;
    }


    public void executeQuery(List<String> listQuery) {
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        try {
            for (String query : listQuery) {

                myDB.execSQL(query);
            }

        } catch (Exception e) {
            // TODO: handle exception
            Log.e(TAG, "executeQuery: ", e);
        } finally {
            myDB.close();
        }
    }

}
