package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pe.com.nextel.android.actividad.NexInvalidOperatorActivity.NexInvalidOperatorException;
import pe.com.nextel.android.actividad.support.NexFragmentActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.Gps;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.util.support.DialogFragmentYesNo;
import pe.com.nextel.android.util.support.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.support.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.support.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanEnvDocumento;
import pe.entel.android.pedidos.bean.BeanEnvPago;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanGeneral;
import pe.entel.android.pedidos.bean.BeanKPI;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.bean.BeanUsuarioOnline;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalCobranza;
import pe.entel.android.pedidos.dal.DalGeneral;
import pe.entel.android.pedidos.dal.DalKPI;
import pe.entel.android.pedidos.dal.DalPago;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.dal.DalTipoCambio;
import pe.entel.android.pedidos.http.HttpConsultaUsuario;
import pe.entel.android.pedidos.http.HttpGrabaCobranza;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.UtilitarioData;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;
import pe.entel.android.pedidos.widget.ListaPopup;

import pe.entel.android.pedidos.R;

@SuppressWarnings("unused")
public class ActCobranzaMonto extends NexFragmentActivity implements NexMenuCallbacks,
        MenuSlidingListener, ListaPopup.OnItemClickPopup {
    private TextView txtdocu, txtmtotot, txtmtopag, txtmpagarsoles, txtmpagardolares,
            txtVoucher, txtFechaDiferida, txtTCSoles, txtTCDolares,
            lblMontoPagarSoles, lblMontoPagarDolares;

    private Button spiFormaPago, spiBanco;

    private Button btnenviar, btnrecibo;

    private ToggleButton tbtnSoles;
    private ToggleButton tbtnDolares;

    TextWatcher textWatcherMontoPagarSoles, textWatcherMontoPagarDolares;

    private LinearLayout ioRowbtnSoles, ioRowSoles ,ioRowbtnDolares, ioRowDolares ;

    long iiIdCliente;

    BeanUsuario loBeanUsuario = null;
    private AdapMenuPrincipalSliding _menu;

    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDASINCRONIZACION = 11;
    private final int ENVIOPENDIENTES = 12;
    private final int VALIDASINCRO = 13;
    private final int NOHAYPENDIENTES = 14;

    private final int POPUP_FORMA_PAGO = 1;
    private final int POPUP_BANCO = 2;

    private LinearLayout lnBanco, lnVaucher, lnFechaDiferida;

    boolean estadoSincronizar = false;
    private CaldroidFragment dialogCaldroidFragment;

    BeanEnvPago loBeanPago = null;
    BeanGeneral ioFormaPago = null;

    private List<BeanGeneral> ioListaFormaPago;
    private List<BeanGeneral> ioListaBanco;

    private DalGeneral loDalGeneral = new DalGeneral(this);

    private Pair<Integer, String> pairTipoCambioSoles;
    private Pair<Integer, String> pairTipoCambioDolar;

    private final int CONSGRABAR = 1;
    private final int CONSVALIDAR = 2;
    private final int CONSDOCU = 3;
    private final int CONSCERO = 4;
    private final int CONSDEUDA = 5;
    private final int ERRORPEDIDO = 7;
    private final int CONSMONTOAMBOS= 8;

    private final int CONSVALIDARVOUCHER = 8;

    public static int ESTADO_POPUP = -1;
    public static int DISMISS_POPUP = -1;
    public static int FORMA_PAGO_POPUP = 0;
    public static int BANCO_POPUP = 1;
    public static int TIPO_PAGO_POPUP = 2;

    public static int ESTADO_ACTIVITY = 0;
    public static int NUEVO_ACTIVITY = 0;
    public static int ACTIGUO_ACTIVITY = 1;

    static BeanGeneral formaPago;
    static BeanGeneral banco;

    private String isLimiteCredito;
    Bundle savedInstanceState;
    // Integracion Ntrack
    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro

    //PARAMETROS DE CONFIGURACION
    private String flgNumDecVista;
    private String flgMoneda;
    private String flgGps;
    private boolean ibHttpObtenerSerie;
    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);
        this.savedInstanceState = savedInstanceState;

        String lsBeanUsuario = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario,
                BeanUsuario.class);

        flgNumDecVista = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(this);
        flgMoneda = Configuracion.EnumConfiguracion.SIMBOLO_MONEDA.getValor(this);
        flgGps = Configuracion.EnumConfiguracion.GPS.getValor(this);

        isLimiteCredito = Configuracion.EnumConfiguracion.LIMITE_CREDITO.getValor(this);

        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);

        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));

        subIniMenu();
    }

    @Override
    public void onBackPressed() {
        Intent loIntent = new Intent(this, ActCobranzaLista.class);
        loIntent.putExtra("IdCliente", iiIdCliente);
        startActivity(loIntent);
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setTitle(
                getString(R.string.actcobranzamonto_bardetalle));
    }

    @Override
    public void subSetControles() {
        setActionBarContentView(R.layout.cobranzamonto2);
        super.subSetControles();
        txtdocu = (TextView) findViewById(R.id.actcobranzamonto_txtdocu);
        txtmtotot = (TextView) findViewById(R.id.actcobranzamonto_txtmtotot);
        txtmtopag = (TextView) findViewById(R.id.actcobranzamonto_txtmtopag);
        spiFormaPago = (Button) findViewById(R.id.actcobranzamonto_cmbTipoDocumento);
        txtmpagarsoles = (TextView) findViewById(R.id.actcobranzamonto_txtMontoPagarSoles);
        txtmpagardolares = (TextView) findViewById(R.id.actcobranzamonto_txtMontoPagarDolares);
        lnBanco = (LinearLayout) findViewById(R.id.lnBanco);
        lnVaucher = (LinearLayout) findViewById(R.id.lnVaucher);
        spiBanco = (Button) findViewById(R.id.actcobranzamonto_cmbBanco);
        txtVoucher = (TextView) findViewById(R.id.actcobranzamonto_txtVoucher);
        txtFechaDiferida = (TextView) findViewById(R.id.actcobranzamonto_txtFechaDiferida);
        lnFechaDiferida = (LinearLayout) findViewById(R.id.lnFechaDiferida);

        btnenviar = (Button) findViewById(R.id.actcobranzamonto_btnenviar);
        btnrecibo = findViewById(R.id.actcobranzamonto_btnnumerorecibo);
        tbtnSoles = findViewById(R.id.actcobranzamonto_tbtnsoles);
        tbtnDolares = findViewById(R.id.actcobranzamonto_tbtndolares);
        txtTCSoles = findViewById(R.id.actcobranzamonto_txtSolesADolares);
        txtTCDolares = findViewById(R.id.actcobranzamonto_txtDolaresASoles);
        lblMontoPagarSoles = findViewById(R.id.actcobranzamonto_lblMontoPagarSoles);
        lblMontoPagarDolares = findViewById(R.id.actcobranzamonto_lblMontoPagarDolares);

        ioRowbtnSoles = findViewById(R.id.actcobranzamonto_linarbtnSoles);
        ioRowSoles = findViewById(R.id.actcobranzamonto_linarPagoSoles);
        ioRowbtnDolares = findViewById(R.id.actcobranzamonto_linarbtnDolares);
        ioRowDolares = findViewById(R.id.actcobranzamonto_linarPagoDolares);
        subCargaDetalle();


        btnenviar.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                grabar();
            }
        });

        btnrecibo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                editarcorrelativo();
            }
        });

        spiFormaPago.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                loDalGeneral = new DalGeneral(ActCobranzaMonto.this);

                ioListaFormaPago = loDalGeneral.fnSelGeneral(String
                        .valueOf(Configuracion.GENERAL4TIPOPAGO));
                ESTADO_POPUP = FORMA_PAGO_POPUP;
                new ListaPopup().dialog(ActCobranzaMonto.this,
                        ioListaFormaPago, POPUP_FORMA_PAGO,
                        ActCobranzaMonto.this,
                        getString(R.string.actcobranzamonto_lblformapago),
                        formaPago, R.attr.PedidosPopupLista);
            }
        });

        spiBanco.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                loDalGeneral = new DalGeneral(ActCobranzaMonto.this);
                ESTADO_POPUP = BANCO_POPUP;
                ioListaBanco = loDalGeneral.fnSelGeneral(String
                        .valueOf(Configuracion.GENERAL3BANCO));
                new ListaPopup().dialog(ActCobranzaMonto.this, ioListaBanco,
                        POPUP_BANCO, ActCobranzaMonto.this,
                        getString(R.string.actcobranzamonto_banco_prompt),
                        banco, R.attr.PedidosPopupLista);
            }
        });

        txtFechaDiferida.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerFechaDiferida();
            }
        });

        textWatcherMontoPagarSoles = new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String saldo = loBeanPago.getSaldosoles();
                String pagar = txtmpagarsoles.getText().toString();
                if(pagar.equals("")) pagar = "0";

                if(tbtnSoles.isChecked()){
                    if(!pagar.equals("")){
                        if(Double.parseDouble(pagar) > Double.parseDouble(saldo)){
                            Toast.makeText(getApplicationContext(),
                                    "El monto a pagar excede al saldo en soles S/." + saldo,
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    Double resultDolar = UtilitarioData.fnRedondeo(Double.parseDouble(saldo) * Double.parseDouble(pairTipoCambioSoles.second),2);
                    if(Double.parseDouble(pagar) > resultDolar){
                        Toast.makeText(getApplicationContext(),
                                "El monto a pagar excede al saldo en dólares: $." + resultDolar,
                                Toast.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
        };

        textWatcherMontoPagarDolares = new TextWatcher() {
            public void afterTextChanged(Editable s) {
                String saldo = loBeanPago.getSaldodolares();
                String pagar = txtmpagardolares.getText().toString();
                if(pagar.equals("")) pagar = "0";

                if(tbtnDolares.isChecked()){
                    if(!pagar.equals("")){
                        if(Double.parseDouble(pagar) > Double.parseDouble(saldo)){
                            Toast.makeText(getApplicationContext(),
                                    "El monto a pagar excede al saldo en soles $." + saldo,
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    Double resultSoles = UtilitarioData.fnRedondeo(Double.parseDouble(saldo) * Double.parseDouble(pairTipoCambioDolar.second),2);
                    if(Double.parseDouble(pagar) > resultSoles){
                        Toast.makeText(getApplicationContext(),
                                "El monto a pagar excede al saldo en dólares: S/" + resultSoles,
                                Toast.LENGTH_LONG).show();
                    }
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

        };

        txtmpagarsoles.addTextChangedListener(textWatcherMontoPagarSoles);
        txtmpagardolares.addTextChangedListener(textWatcherMontoPagarDolares);

        tbtnSoles.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(tbtnSoles.isChecked()){
                    txtmpagarsoles.setText(Utilitario.fnRound(loBeanPago.getSaldosoles(),
                            Integer.valueOf(flgNumDecVista)));
                    lblMontoPagarSoles.setText(R.string.actcobranzamonto_lblmontoapagarsoles);
                } else {
                    String valorSoles = txtmpagarsoles.getText().toString();
                    if(valorSoles.equals("")) {
                        valorSoles = "0";
                        txtmpagarsoles.setText("0");
                    }

                    Double resultDolar = UtilitarioData.fnRedondeo(Double.parseDouble(valorSoles) * Double.parseDouble(pairTipoCambioSoles.second),2);

                    txtmpagarsoles.setText(resultDolar.toString());
                    Toast.makeText(getApplicationContext(),
                            "Se Calculó el Pago Dólares: " + resultDolar + " (Tipo De Cambio " + pairTipoCambioSoles.second + " y Pago Soles " + valorSoles + ").",
                            Toast.LENGTH_LONG).show();
                    lblMontoPagarSoles.setText(R.string.actcobranzamonto_lblmontoapagardolares);
                }
            }
        });

        tbtnDolares.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(tbtnDolares.isChecked()){
                    txtmpagardolares.setText(Utilitario.fnRound(loBeanPago.getSaldodolares(),
                            Integer.valueOf(flgNumDecVista)));
                    lblMontoPagarDolares.setText(R.string.actcobranzamonto_lblmontoapagardolares);
                } else {
                    String valorDolar = txtmpagardolares.getText().toString();
                    if(valorDolar.equals("")) {
                        valorDolar = "0";
                        txtmpagardolares.setText("0");
                    }

                    Double resultSoles = UtilitarioData.fnRedondeo(Double.parseDouble(valorDolar) * Double.parseDouble(pairTipoCambioDolar.second),2);

                    txtmpagardolares.setText(resultSoles.toString());
                    Toast.makeText(getApplicationContext(),
                            "Se Calculó el Pago Soles: " + resultSoles + " (Tipo De Cambio " + pairTipoCambioDolar.second + " y Pago Dólares " + valorDolar + ").",
                            Toast.LENGTH_LONG).show();
                    lblMontoPagarDolares.setText(R.string.actcobranzamonto_lblmontoapagarsoles);
                }
            }
        });
    }

    private void editarcorrelativo(){
        StringBuffer loStb = new StringBuffer();
        loStb.append(getString(R.string.actcobranzamonto_eitarcorrelativo)).append('\n');
        AlertDialog loAlertDialog = null;
        loAlertDialog = new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(getString(R.string.actcobranzamonto_bardetalle))
                .setMessage(loStb.toString())
                .setPositiveButton(getString(R.string.dlg_btnsi),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                try {
                                    Intent intent = new Intent(ActCobranzaMonto.this, ActDocumentoOnline.class);
                                    BeanUsuarioOnline beanRecibo = ((AplicacionEntel) getApplication()).getCurrentRecibo();
                                    BeanEnvDocumento bdocumento = new BeanEnvDocumento();
                                    bdocumento.setIdDocTipo("C");
                                    bdocumento.setSerie(beanRecibo.getSerie());
                                    bdocumento.setCorrelativo(beanRecibo.getCorrelativo());
                                    bdocumento.setIdUsuario(loBeanUsuario.getCodVendedor());
                                    bdocumento.setIdCliente(String.valueOf(((AplicacionEntel) getApplication()).getCurrentCliente().getClicod()));
                                    ((AplicacionEntel) getApplication()).setCurrentDocumento(bdocumento);
                                    startActivityForResult(intent,5);
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }
                        })
                .setNegativeButton(getString(R.string.dlg_btncancelar),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {

                            }
                        }).create();
        loAlertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 5 && resultCode == RESULT_OK){
            fnObtenerRecibo();
        }
    }

    private void subCargaDetalle() {
        Bundle loExtras = getIntent().getExtras();
        iiIdCliente = loExtras.getLong("IdCliente");

        loBeanPago = ((AplicacionEntel) getApplication()).getCurrentPago();
        txtdocu.setText(loBeanPago.getNroDocumento() + " ("
                + loBeanPago.getFechaDocumento() + ")");
        txtmtotot.setText(flgMoneda
                + " "
                + Utilitario.fnRound(loBeanPago.getCob_montototalsoles(),
                Integer.valueOf(flgNumDecVista)));
        txtmtopag.setText(flgMoneda
                + " "
                + Utilitario.fnRound(loBeanPago.getCob_montopagadosoles(),
                Integer.valueOf(flgNumDecVista)));

        if (ESTADO_ACTIVITY == NUEVO_ACTIVITY) {
            activityNuevo();
            txtmpagarsoles.setText(Utilitario.fnRound(loBeanPago.getSaldosoles(), Integer.valueOf(flgNumDecVista)));
            if(loBeanPago.getSaldosoles().equals("") || loBeanPago.getSaldosoles().equals("0") || loBeanPago.getSaldosoles().equals("0.00")){
                ioRowbtnSoles.setVisibility(View.GONE);
                ioRowSoles.setVisibility(View.GONE);
            }
            txtmpagardolares.setText(Utilitario.fnRound(loBeanPago.getSaldodolares(), Integer.valueOf(flgNumDecVista)));
            if(loBeanPago.getSaldodolares().equals("") || loBeanPago.getSaldodolares().equals("0") || loBeanPago.getSaldodolares().equals("0.00")){
                ioRowbtnDolares.setVisibility(View.GONE);
                ioRowDolares.setVisibility(View.GONE);
            }
            // mirar abajo
            loDalGeneral = new DalGeneral(this);

            ioListaFormaPago = loDalGeneral.fnSelGeneral(String.valueOf(Configuracion.GENERAL4TIPOPAGO));

            ioListaBanco = loDalGeneral.fnSelGeneral(String.valueOf(Configuracion.GENERAL3BANCO));

            for (int i = 0; i < ioListaFormaPago.size(); i++) {
                if (i == 0)
                    formaPago = (BeanGeneral) ioListaFormaPago.get(i);
                break;
            }

            if (formaPago != null) {
                spiFormaPago.setText(formaPago.getDescripcion());
                mostrarSegunFormaPago();
            }

            for (int i = 0; i < ioListaBanco.size(); i++) {
                if (i == 0)
                    banco = (BeanGeneral) ioListaBanco.get(i);
                break;
            }
            if (banco != null)
                spiBanco.setText(banco.getDescripcion());

        } else {
            activityAntiguo();
        }
        fnObtenerRecibo();
        fbObtenerTipoCambio();
        if(pairTipoCambioSoles == null && pairTipoCambioDolar == null ){
            Toast toast1 = Toast.makeText(getApplicationContext(),"No existe tipo de cambio vigente.", Toast.LENGTH_SHORT);
            toast1.show();
            return;
        } else if (pairTipoCambioSoles == null){
            Toast toast1 = Toast.makeText(getApplicationContext(),"No existe tipo de cambio Soles vigente.", Toast.LENGTH_SHORT);
            toast1.show();
            return;
        } else if (pairTipoCambioDolar == null){
            Toast toast1 = Toast.makeText(getApplicationContext(),"No existe tipo de cambio Dólares vigente.", Toast.LENGTH_SHORT);
            toast1.show();
            return;
        }else{
            txtTCSoles.setText(String.valueOf(pairTipoCambioSoles.second));
            txtTCDolares.setText(String.valueOf(pairTipoCambioDolar.second));
        }
    }

    private void fbObtenerTipoCambio(){
        DalTipoCambio dalTipoCambio = new DalTipoCambio(this);
        pairTipoCambioSoles = dalTipoCambio.fnObtenerTipoCambio("0");
        pairTipoCambioDolar = dalTipoCambio.fnObtenerTipoCambio("1");
    }

    private void activityNuevo() {
        formaPago = null;
        ESTADO_ACTIVITY = ACTIGUO_ACTIVITY;
        banco = null;
        ESTADO_POPUP = DISMISS_POPUP;
    }

    private void mostrarSegunFormaPago() {
        spiFormaPago.setText(formaPago.getDescripcion());

        if (formaPago.getFlg_banco().equals(Configuracion.FLGVERDADERO)) {
            lnBanco.setVisibility(View.VISIBLE);
        } else {
            lnBanco.setVisibility(View.GONE);
        }

        if (formaPago.getFlg_nrodocumento().equals(Configuracion.FLGVERDADERO)) {
            lnVaucher.setVisibility(View.VISIBLE);
        } else {
            lnVaucher.setVisibility(View.GONE);
        }

        if (formaPago.getFlg_fechadiferida().equals(Configuracion.FLGVERDADERO)) {
            lnFechaDiferida.setVisibility(View.VISIBLE);
        } else {
            lnFechaDiferida.setVisibility(View.GONE);
        }
    }

    private void activityAntiguo() {
        if (formaPago != null) {
            mostrarSegunFormaPago();
        }

        if (banco != null)
            spiBanco.setText(banco.getDescripcion());

        if (ESTADO_POPUP == FORMA_PAGO_POPUP) {
            loDalGeneral = new DalGeneral(ActCobranzaMonto.this);

            ioListaFormaPago = loDalGeneral.fnSelGeneral(String
                    .valueOf(Configuracion.GENERAL4TIPOPAGO));
            ESTADO_POPUP = FORMA_PAGO_POPUP;
            new ListaPopup().dialog(ActCobranzaMonto.this, ioListaFormaPago,
                    POPUP_FORMA_PAGO, ActCobranzaMonto.this,
                    getString(R.string.actcobranzamonto_lblformapago),
                    formaPago, R.attr.PedidosPopupLista);

        } else if (ESTADO_POPUP == BANCO_POPUP) {
            loDalGeneral = new DalGeneral(ActCobranzaMonto.this);
            ESTADO_POPUP = BANCO_POPUP;
            ioListaBanco = loDalGeneral.fnSelGeneral(String
                    .valueOf(Configuracion.GENERAL3BANCO));
            new ListaPopup().dialog(ActCobranzaMonto.this, ioListaBanco,
                    POPUP_BANCO, ActCobranzaMonto.this,
                    getString(R.string.actcobranzamonto_banco_prompt), banco,
                    R.attr.PedidosPopupLista);
        }
    }

    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        DialogFragmentYesNo loDialog;
        switch (id) {
            case CONSMONTOAMBOS:
                loDialog = DialogFragmentYesNo.newInstance(getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSMONTOAMBOS"),
                        getString(R.string.actcobranzamonto_bardetalle),
                        getString(R.string.actcobranzamonto_msnvalidaambosmontos),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case CONSVALIDAR:
                loDialog = DialogFragmentYesNo.newInstance(getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSVALIDAR"),
                        getString(R.string.actcobranzamonto_bardetalle),
                        getString(R.string.actcobranzamonto_msnvalidacion),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case CONSCERO:
                loDialog = DialogFragmentYesNo.newInstance(getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSCERO"),
                        getString(R.string.actcobranzamonto_bardetalle),
                        getString(R.string.actcobranzamonto_msnsinmonto),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case CONSDEUDA:
                loDialog = DialogFragmentYesNo.newInstance(getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDEUDA"),
                        getString(R.string.actcobranzamonto_bardetalle),
                        getString(R.string.actcobranzamonto_msnmasdeuda),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case CONSDOCU:
                loDialog = DialogFragmentYesNo.newInstance(getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDOCU"),
                        getString(R.string.actcobranzamonto_bardetalle),
                        getString(R.string.actcobranzamonto_msnvalidadocu),

                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loDialog = DialogFragmentYesNo.newInstance(getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.actmenu_dlgsincronizar),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    String lsBeanUsuario = getSharedPreferences(
                                            "Actual", MODE_PRIVATE).getString(
                                            "BeanUsuario", "");
                                    BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                            .fromJson(lsBeanUsuario,
                                                    BeanUsuario.class);
                                    estadoSincronizar = true;
                                    new HttpSincronizar(loBeanUsuario
                                            .getCodVendedor(),
                                            ActCobranzaMonto.this,
                                            fnTipactividad()).execute();
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            case VALIDASINCRONIZACION:
                loDialog = DialogFragmentYesNo.newInstance(getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"),
                        getString(R.string.actmenuvalsincro_titulo),
                        getString(R.string.actmenuvalsincro_mensaje),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case VALIDASINCRO:
                loDialog = DialogFragmentYesNo.newInstance(getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                        getString(R.string.msgregpendientesenvinfo),

                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case ENVIOPENDIENTES:
                loDialog = DialogFragmentYesNo.newInstance(getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"),
                        getString(R.string.msgdeseaenviarpendientes),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    Intent loIntentEspera = new Intent(
                                            ActCobranzaMonto.this,
                                            ActGrabaPendiente.class);

                                    startActivity(loIntentEspera);
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });
            case DIALOG_NSERVICES_DOWNLOAD:
                String lsNServices;
                if (ioDALNservices == null)
                    try {
                        ioDALNservices = DALNServices.getInstance(
                                ActCobranzaMonto.this, R.string.db_name);
                    } catch (NexInvalidOperatorException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                try {
                    lsNServices = ioDALNservices.fnSelNServicesLabel();
                } catch (Exception e) {
                    Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                    lsNServices = getString(R.string.nservices_label_default);
                }
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG + DIALOG_NSERVICES_DOWNLOAD,
                        getString(R.string.nservices_consultingdownlad).replace(
                                "{0}", lsNServices),
                        EnumLayoutResource.getDefaultIconHelp(this));
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    subIniNServicesDownload(ioDALNservices
                                            .fnSelNServicesAPK());
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                            }
                        });

            default:
                return super.onCreateNexDialog(id);
        }

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        Log.v("XXX", "ID: " + id);
        switch (id) {
            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSVALIDAR:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actcobranzamonto_bardetalle))
                        .setMessage(
                                getString(R.string.actcobranzamonto_msnvalidacion))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSCERO:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actcobranzamonto_bardetalle))
                        .setMessage(
                                getString(R.string.actcobranzamonto_msnsinmonto))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSDEUDA:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actcobranzamonto_bardetalle))
                        .setMessage(
                                getString(R.string.actcobranzamonto_msnmasdeuda))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSDOCU:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actcobranzamonto_bardetalle))
                        .setMessage(
                                getString(R.string.actcobranzamonto_msnvalidadocu))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences(
                                                    "Actual", MODE_PRIVATE)
                                                    .getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                                    .fromJson(lsBeanUsuario,
                                                            BeanUsuario.class);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActCobranzaMonto.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActCobranzaMonto.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;
            case ERRORPEDIDO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titerror))
                        .setMessage(getString(R.string.actgrabapedido_dlgerrorpedido))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        Intent intent = new Intent(ActCobranzaMonto.this, ActClienteDetalle.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                    }
                                }).create();
                return loAlertDialog;

            // return null;
            default:
                return super.onCreateDialog(id);
        }
    }

    private void grabar() {
        if(pairTipoCambioSoles == null && pairTipoCambioDolar == null ){
            Toast toast1 = Toast.makeText(getApplicationContext(),"No existe tipo de cambio vigente.", Toast.LENGTH_SHORT);
            toast1.show();
            return;
        } else if (pairTipoCambioSoles == null){
            Toast toast1 = Toast.makeText(getApplicationContext(),"No existe tipo de cambio Soles vigente.", Toast.LENGTH_SHORT);
            toast1.show();
            return;
        } else if (pairTipoCambioDolar == null){
            Toast toast1 = Toast.makeText(getApplicationContext(),"No existe tipo de cambio Dólares vigente.", Toast.LENGTH_SHORT);
            toast1.show();
            return;
        }
        BeanGeneral ioFormaPago = formaPago;

        if (ioFormaPago.getFlg_nrodocumento().equals(Configuracion.FLGVERDADERO)) {
            if (txtVoucher.getText().toString().trim().equals("")) {
                showDialog(CONSDOCU);
                return;
            }
        }

        String valorSoles = txtmpagarsoles.getText().toString();
        if(valorSoles.equals("")) valorSoles = "0";

        String valorDolares = txtmpagardolares.getText().toString();
        if(valorDolares.equals("")) valorDolares = "0";

        if(ioRowbtnSoles.getVisibility() == View.VISIBLE && ioRowbtnDolares.getVisibility() == View.VISIBLE) {
            if ((valorSoles.equals(".") || valorSoles.equals("-") || valorSoles.equals("0") || (Double.parseDouble(valorSoles) <= 0.00)) &&
                (valorDolares.equals(".") || valorDolares.equals("-") || valorDolares.equals("0") || (Double.parseDouble(valorDolares) <= 0.00))) {
                showDialog(CONSMONTOAMBOS);
                return;
            }
        }

        if(ioRowbtnSoles.getVisibility() == View.VISIBLE && ioRowbtnDolares.getVisibility() == View.GONE) {
            if (Double.parseDouble(loBeanPago.getSaldosoles()) > 0) {
                if (valorSoles.equals(".") || valorSoles.equals("-") || valorSoles.equals("0")) {
                    txtmpagarsoles.requestFocus();
                    showDialog(CONSVALIDAR);
                    return;
                }

                txtmpagarsoles.setText(Utilitario.fnRound(valorSoles.trim(), Integer.valueOf(flgNumDecVista)));
                if (!(Double.parseDouble(valorSoles) > 0.00)) {
                    txtmpagarsoles.requestFocus();
                    showDialog(CONSCERO);
                    return;
                }

                String _saldosoles = tbtnSoles.isChecked() ? loBeanPago.getSaldosoles() : String.valueOf(Utilitario.fnRedondeo(Double.parseDouble(loBeanPago.getSaldosoles()) * Double.parseDouble(pairTipoCambioSoles.second), 2));
                if ((Double.parseDouble(valorSoles) > Double.parseDouble(_saldosoles))) {
                    txtmpagarsoles.requestFocus();
                    showDialog(CONSDEUDA);
                    return;
                }
            }
        }

        if(ioRowbtnDolares.getVisibility() == View.VISIBLE && ioRowbtnSoles.getVisibility() == View.GONE) {
            if (Double.parseDouble(loBeanPago.getSaldodolares()) > 0) {

                if (valorDolares.equals(".") || valorDolares.equals("-") || valorDolares.equals("0")) {
                    txtmpagardolares.requestFocus();
                    showDialog(CONSVALIDAR);
                    return;
                }

                txtmpagardolares.setText(Utilitario.fnRound(valorDolares.trim(),
                        Integer.valueOf(flgNumDecVista)));
                if (!(Double.parseDouble(valorDolares) > 0.00)) {
                    txtmpagardolares.requestFocus();
                    showDialog(CONSCERO);
                    return;
                }

                String _saldodolares = tbtnDolares.isChecked() ? loBeanPago.getSaldodolares() : String.valueOf(Utilitario.fnRedondeo(Double.parseDouble(loBeanPago.getSaldodolares()) * Double.parseDouble(pairTipoCambioDolar.second), 2));
                if ((Double.parseDouble(valorDolares) > Double.parseDouble(_saldodolares))) {
                    txtmpagardolares.requestFocus();
                    showDialog(CONSDEUDA);
                    return;
                }
            }
        }

        Calendar c = Calendar.getInstance();
        Date loDate = c.getTime();
        final String sFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate)
                .toString();

        StringBuffer loStb = new StringBuffer();
        loStb.append(getString(R.string.actcobranzamonto_dlgfin)).append('\n');
        loStb.append(" Documento : ").append(loBeanPago.getNroDocumento())
                .append(" (").append(loBeanPago.getFechaDocumento())
                .append(")").append('\n');
        loStb.append(" Forma de pago : ").append(ioFormaPago.getDescripcion())
                .append('\n');

        if(!valorSoles.equals("0")){
            if(Double.parseDouble(valorSoles) > 0){
                loStb.append(" Monto pago : ").append(getString(tbtnSoles.isChecked() ? R.string.moneda : R.string.monedaDolares))
                        .append(" ").append(valorSoles).append('\n');
            }
        }
        if(!valorDolares.equals("0")){
            if(Double.parseDouble(valorDolares.toString()) > 0){
                loStb.append(" Monto pago : ").append(getString(tbtnDolares.isChecked() ? R.string.monedaDolares : R.string.moneda))
                        .append(" ").append(valorDolares).append('\n');
            }
        }
        loStb.append(sFecha).append('\n');

        AlertDialog loAlertDialog = null;
        loAlertDialog = new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(getString(R.string.actcobranzamonto_bardetalle))
                .setMessage(loStb.toString())

                .setPositiveButton(getString(R.string.dlg_btnsi),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                try {
                                    subGrabar();
                                    enviarCobranzaServer();
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }
                        })
                .setNegativeButton(getString(R.string.dlg_btncancelar),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {

                            }
                        }).create();
        loAlertDialog.show();
    }

    private void subGrabar() throws Exception {
        // Obtenemos la fecha del sistema
        Calendar c = Calendar.getInstance();
        Date loDate = c.getTime();
        loBeanPago.setFechaMovil(DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate).toString());

        BeanGeneral ioFormaPago = formaPago;

        if (txtmpagarsoles.getText().toString().length() > 0
                && txtmpagarsoles.getText().toString().substring(0, 1)
                .equals(".")) {
            txtmpagarsoles.setText("0"
                    + txtmpagarsoles.getText().toString());
        }

        if (txtmpagardolares.getText().toString().length() > 0
                && txtmpagardolares.getText().toString().substring(0, 1)
                .equals(".")) {
            txtmpagardolares.setText("0"
                    + txtmpagardolares.getText().toString());
        }

        String valorSoles = txtmpagarsoles.getText().toString();
        if(valorSoles.equals("")) valorSoles = "0";

        String valorDolares = txtmpagardolares.getText().toString();
        if(valorDolares.equals("")) valorDolares = "0";

        loBeanPago.setIdtipocambiosoles(pairTipoCambioSoles.first.toString());
        loBeanPago.setIdtipocambiodolares(pairTipoCambioDolar.first.toString());
        loBeanPago.setTipomonedasoles(tbtnSoles.isChecked() ? "1" : "2");
        loBeanPago.setTipomonedadolares(tbtnDolares.isChecked() ? "2" : "1");

        loBeanPago.setPago(valorSoles.trim());
        loBeanPago.setPagosoles(valorSoles.trim());
        loBeanPago.setPagodolares(valorDolares.trim());

        loBeanPago.setIdFormaPago(ioFormaPago.getCodigo());
        loBeanPago.setCodPago(loBeanPago.getNroDocumento());

        if (ioFormaPago.getFlg_nrodocumento().equals(Configuracion.FLGVERDADERO)) {
            loBeanPago.setNroDocumento(txtVoucher.getText().toString().trim());
        } else {
            loBeanPago.setNroDocumento("");
        }

        if (ioFormaPago.getFlg_fechadiferida().equals(Configuracion.FLGVERDADERO)) {
            loBeanPago.setFechaDiferida(txtFechaDiferida.getText().toString());
        } else {
            loBeanPago.setFechaDiferida("");
        }

        if (ioFormaPago.getFlg_banco().equals(Configuracion.FLGVERDADERO)) {
            ioFormaPago = banco;
            loBeanPago.setIdBanco(ioFormaPago.getCodigo());
        } else {
            loBeanPago.setIdBanco("");
        }

        String lsBeanRepresentante = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanRepresentante, BeanUsuario.class);

        // Se guarda GPS si tiene el flag habilitado

        Location location = null;
        try {
            location = Gps.getLocation();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (flgGps.equals(
                Configuracion.FLGVERDADERO)
                && location != null) {
            loBeanPago.setLatitud(String.valueOf(location.getLatitude()));
            loBeanPago.setLongitud(String.valueOf(location.getLongitude()));
            loBeanPago.setCelda(location.getProvider().substring(0, 1)
                    .toUpperCase());
        } else {
            loBeanPago.setLatitud("0");
            loBeanPago.setLongitud("0");
            loBeanPago.setCelda("-");
        }

        loBeanPago.setFlgEnCobertura(Utilitario.fnVerSignal(this) ? "1" : "0");

        String lsBeanVisitaCab = "";
        try {
            lsBeanVisitaCab = BeanMapper.toJson(loBeanPago, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("beanPedidoCab", lsBeanVisitaCab);
        loEditor.commit();

        // Grabamos en la BD del equipo
        DalPago loDalPago = new DalPago(this);
        String codPed = loDalPago.fnGrabarPago(loBeanPago);
        loBeanPago.setCodPago(codPed);

        BeanCliente loBeanCliente = ((AplicacionEntel) getApplication()).getCurrentCliente();

        // Actualizamos deuda, estado y KPI si es un cliente de la ruta
        DalCliente loDalCliente = new DalCliente(this);
        DalCobranza loDalCobranza = new DalCobranza(this);
        BeanKPI loBeanKPI = ((AplicacionEntel) getApplication()).getCurrentKpi();


        // Se actualiza la deuda del cliente
        if((ioRowbtnSoles.getVisibility() == View.VISIBLE) && (!valorSoles.equals("0"))) {
            Double totald = Double.parseDouble(loBeanCliente.getClideuda());
            totald = totald - (tbtnSoles.isChecked() ? Double.parseDouble(loBeanPago.getPagosoles()) : (Double.parseDouble(loBeanPago.getPagosoles()) / Double.parseDouble(pairTipoCambioSoles.second)));

            if(!valorSoles.equals("0")) {
                loBeanCliente.setClideuda(Utilitario.fnRound(totald, Integer.valueOf(flgNumDecVista)));
                loDalCliente.fnUpdDeudaCliente(Long.parseLong(loBeanCliente.getClipk()), Utilitario.fnRound(totald, Configuracion.MAXDECIMALMONTO), "1");
            }
        }
        if((ioRowbtnDolares.getVisibility() == View.VISIBLE)  && (!valorDolares.equals("0"))) {
            Double totald = Double.parseDouble(loBeanCliente.getClideudadolares());
            totald = totald - (tbtnDolares.isChecked() ? Double.parseDouble(loBeanPago.getPagodolares()) : (Double.parseDouble(loBeanPago.getPagodolares()) / Double.parseDouble(pairTipoCambioDolar.second)));
            if(!valorDolares.equals("0")) {
                loBeanCliente.setClideudadolares(Utilitario.fnRound(totald, Integer.valueOf(flgNumDecVista)));
                loDalCliente.fnUpdDeudaCliente(Long.parseLong(loBeanCliente.getClipk()), Utilitario.fnRound(totald, Configuracion.MAXDECIMALMONTO), "2");
            }
        }
        // Se actualiza la deuda de cobranza
        Double pagado = Double.parseDouble(loBeanPago.getCob_montopagadosoles());
        Double pagadosoles = Double.parseDouble(loBeanPago.getCob_montopagadosoles());
        Double pagadodoalres = Double.parseDouble(loBeanPago.getCob_montopagadodolares());

        Double saldo = Double.parseDouble(loBeanPago.getSaldosoles());
        Double saldosoles = Double.parseDouble(loBeanPago.getSaldosoles());
        Double saldodolares = Double.parseDouble(loBeanPago.getSaldodolares());

        pagado = pagado + (tbtnSoles.isChecked() ? Double.parseDouble(loBeanPago.getPagosoles()) : (Double.parseDouble(loBeanPago.getPagosoles()) / Double.parseDouble(pairTipoCambioSoles.second)));
        pagadosoles += (tbtnSoles.isChecked() ? Double.parseDouble(loBeanPago.getPagosoles()) : (Double.parseDouble(loBeanPago.getPagosoles()) / Double.parseDouble(pairTipoCambioSoles.second)));
        pagadodoalres += (tbtnDolares.isChecked() ? Double.parseDouble(loBeanPago.getPagodolares()) : (Double.parseDouble(loBeanPago.getPagodolares()) / Double.parseDouble(pairTipoCambioDolar.second)));

        saldo = saldo - (tbtnSoles.isChecked() ? Double.parseDouble(loBeanPago.getPagosoles()) : (Double.parseDouble(loBeanPago.getPagosoles()) / Double.parseDouble(pairTipoCambioSoles.second)));
        saldosoles = saldosoles - (tbtnSoles.isChecked() ? Double.parseDouble(loBeanPago.getPagosoles()) : (Double.parseDouble(loBeanPago.getPagosoles()) / Double.parseDouble(pairTipoCambioSoles.second)));
        saldodolares = saldodolares - (tbtnDolares.isChecked() ? Double.parseDouble(loBeanPago.getPagodolares()) : (Double.parseDouble(loBeanPago.getPagodolares()) / Double.parseDouble(pairTipoCambioDolar.second)));

        loBeanPago.setCob_montopagado(Utilitario.fnRound(pagado, Integer.valueOf(flgNumDecVista)));
        loBeanPago.setCob_montopagadosoles(Utilitario.fnRound(pagadosoles, Integer.valueOf(flgNumDecVista)));
        loBeanPago.setCob_montopagadodolares(Utilitario.fnRound(pagadodoalres, Integer.valueOf(flgNumDecVista)));

        loBeanPago.setSaldo(Utilitario.fnRound(saldo, Integer.valueOf(flgNumDecVista)));
        loBeanPago.setSaldosoles(Utilitario.fnRound(saldosoles, Integer.valueOf(flgNumDecVista)));
        loBeanPago.setSaldodolares(Utilitario.fnRound(saldodolares, Integer.valueOf(flgNumDecVista)));

        if((ioRowbtnDolares.getVisibility() == View.VISIBLE) && (!valorDolares.equals("0"))) {
            loDalCobranza.fnUpdCobranzaDolares(loBeanPago.getCob_pk(), loBeanPago.getCob_montopagadodolares(), loBeanPago.getSaldodolares());
        }
        if((ioRowbtnSoles.getVisibility() == View.VISIBLE) && (!valorSoles.equals("0"))) {
            String _pagosoles = tbtnSoles.isChecked() ? loBeanPago.getPagosoles() : String.valueOf(Utilitario.fnRedondeo(Double.parseDouble(loBeanPago.getPagosoles()) / Double.parseDouble(pairTipoCambioSoles.second),2));

            loDalCobranza.fnUpdCobranzaSoles(loBeanPago.getCob_pk(), loBeanPago.getCob_montopagadosoles(), loBeanPago.getSaldosoles());

            if (isLimiteCredito.equals(Configuracion.FLGVERDADERO)) {
                Double saldoCre = Double.parseDouble(loBeanCliente.getSaldoCredito());
                Double pago = Double.parseDouble(_pagosoles);

                loBeanCliente.setSaldoCredito(String.valueOf(saldoCre + pago));
                ((AplicacionEntel) getApplication()).setCurrentCliente(loBeanCliente);
                loDalCliente.fnUpdSaldoCredito(loBeanCliente.getClicod(), loBeanCliente.getSaldoCredito());
            }
        }

        // Actualizo estado de cliente en base de datos interna
        if (loBeanCliente.getEstado().equals(
                Configuracion.ESTADO01PORVISITAR)) {

            loDalCliente.fnUpdEstadoVisitaCliente(
                    Long.parseLong(loBeanCliente.getClipk()),
                    Configuracion.ESTADO02VISITADO);
            // Un cliente mas visitado
            loBeanKPI.numCliVis += 1;
        }

        // Se actualiza monto de cobranza acumulado / SOLES
        if((ioRowbtnSoles.getVisibility() == View.VISIBLE) && (!valorSoles.equals("0"))) {
            loBeanKPI.mntCob = String.valueOf(Double.parseDouble(loBeanKPI.mntCob) + Double.parseDouble(loBeanPago.getPagosoles()));

            // Se actualiza bean KPI
            DalKPI lodkpi = new DalKPI(this);
            lodkpi.fnUpdKPI(loBeanKPI);
            ((AplicacionEntel) getApplication()).setCurrentKpi(loBeanKPI);
        }
        // Actualizo el estado de cliente en el currentcliente
        if (loBeanCliente.getEstado().equals(Configuracion.ESTADO01PORVISITAR)) {
            Log.e("OJOOOOOOOOOOOOOOOOO",
                    "Cambio el estado del current a visitado");
            loBeanCliente.setEstado(Configuracion.ESTADO02VISITADO);
        }

        ((AplicacionEntel) getApplication()).setCurrentCliente(loBeanCliente);
    }

    private void direccionarFlujo() {
        new NexToast(this, R.string.actpedidofin_msnpedidograbado,
                EnumType.ACCEPT).show();
        Intent intentF = new Intent(this, ActClienteDetalle.class);
        intentF.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intentF);
    }

    public void enviarCobranzaServer() {
        ((AplicacionEntel) getApplication()).uploadEvent(Configuracion.COBRANZA_EVENT);
        new HttpGrabaCobranza(loBeanPago, this, fnTipactividad()).execute();
    }

    public void fnObtenerRecibo() {
        ibHttpObtenerSerie = true;
        BeanUsuario loBeanUsuarioOnline = new BeanUsuario();
        loBeanUsuarioOnline.setCodVendedor(loBeanUsuario.getCodVendedor());
        new HttpConsultaUsuario(loBeanUsuarioOnline, this, this.fnTipactividad(),  getString(R.string.actcobranzadetalle_obtenerrecibo)).execute();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {
        if (estadoSincronizar) {
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActMenu.class);

                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }
        } else {
            try {
                if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK
                        || getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {
                    if (ibHttpObtenerSerie) {
                        ibHttpObtenerSerie = false;
                        BeanUsuarioOnline beanRecibo = ((AplicacionEntel) getApplication()).getCurrentRecibo();
                        String serie = "";
                        String correlativo = "";
                        Integer newCorr = 0;
                        if (beanRecibo != null) {
                            serie = beanRecibo.getSerie();
                            newCorr = Integer.parseInt(beanRecibo.getCorrelativo());
                            correlativo = String.format("%010d",(newCorr)); // + 1
                            btnrecibo.setText(serie + " - " + correlativo);
                        }
                    } else {
                        Intent intentl = new Intent(this, ActClienteDetalle.class);
                        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                            // como grabo bien en el servidor, hago un update al
                            // registro
                            DalPago loDalPago = new DalPago(this);
                            boolean lbResult = loDalPago.fnDelTrPago(loBeanPago.getCodPago());

                            if (lbResult) {
                                startActivity(intentl);
                            } else {
                                showDialog(ERRORPEDIDO);
                            }
                        } else {
                            showDialog(ERRORPEDIDO);
                        }
                    }
                }
            } catch (Exception e) {
                subShowException(e);
            }
        }
    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent loIntentLogin = new Intent(ActCobranzaMonto.this,
                        ActLogin.class);
                loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(loIntentLogin);
            }
        });

    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        Intent intentl = new Intent(this, ActMenu.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intentl);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @Override
    public void subSincronizarFromSliding() {
        try {

            List<BeanEnvPedidoCab> loList = null;
            DalPedido loDalPedido = new DalPedido(this);
            loList = loDalPedido.fnEnvioPedidosPendientes(loBeanUsuario
                    .getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);

                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();

                showDialog(CONSDIASINCRONIZAR);
            } else {

                showDialog(VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        String lsBeanUsuario = getSharedPreferences(
                "Actual", MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper
                .fromJson(lsBeanUsuario,
                        BeanUsuario.class);
        this.setMsgOk("Eficiencia Online");

        BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
        loBeanListaEficienciaOnline.setIdResultado(0);
        loBeanListaEficienciaOnline.setResultado("");
        loBeanListaEficienciaOnline.setListaEficiencia(null);
        loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

        new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

        Intent loInstent = new Intent(this, ActKpiDetalle.class);
        loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loInstent);
    }

    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        ((AplicacionEntel) getApplication()).setCurrentCliente(null);
        ((AplicacionEntel) getApplication())
                .setCurrentCodFlujo(Configuracion.CONSSTOCK);
        ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
        Intent loIntento = new Intent(ActCobranzaMonto.this,
                ActProductoListaOnline.class);
        loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        BeanExtras loBeanExtras = new BeanExtras();
        loBeanExtras.setFiltro("");
        loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
        String lsFiltro = "";
        try {
            lsFiltro = BeanMapper.toJson(loBeanExtras, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
        startActivity(loIntento);
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            subShowException(new Exception(getString(R.string.ml_sincronizarantes)));
        }

    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK,
                R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */
    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {

                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }


    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

    @Override
    public void setOnItemClickPopup(Object poGeneral, int piTipo) {
        // TODO Auto-generated method stub
        ESTADO_POPUP = DISMISS_POPUP;
        switch (piTipo) {
            case POPUP_FORMA_PAGO:
                formaPago = (BeanGeneral) poGeneral;
                mostrarSegunFormaPago();

                break;
            case POPUP_BANCO:
                banco = (BeanGeneral) poGeneral;
                spiBanco.setText(banco.getDescripcion());
                break;
            default:
                break;
        }
    }

    private void showDatePickerFechaDiferida() {
        dialogCaldroidFragment = new CaldroidFragment();

        TypedArray ta = obtainStyledAttributes(new int[]{R.attr.PagosPopupCalendar});
        dialogCaldroidFragment.setDrawableLogo(ta.getResourceId(0, 0));
        ta.recycle();

        dialogCaldroidFragment.setCancelable(true);
        dialogCaldroidFragment.setCaldroidListener(new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {
                String fecha = DateFormat.format("dd/MM/yyyy", date).toString();
                txtFechaDiferida.setText(fecha);
                dialogCaldroidFragment.dismiss();
            }
        });

        // If activity is recovered from rotation
        final String dialogTag = "CALDROID_DIALOG_FRAGMENT";
        final Bundle statebun = savedInstanceState;
        if (statebun != null) {
            dialogCaldroidFragment.restoreDialogStatesFromKey(
                    getSupportFragmentManager(), statebun,
                    "DIALOG_CALDROID_SAVED_STATE", dialogTag);
            Bundle args = dialogCaldroidFragment.getArguments();
            if (args == null) {
                args = new Bundle();
                dialogCaldroidFragment.setArguments(args);
            }

        } else {
            // Setup arguments
            Bundle bundle = new Bundle();
            // Setup dialogTitle
            dialogCaldroidFragment.setArguments(bundle);
        }

        dialogCaldroidFragment.show(getSupportFragmentManager(), dialogTag);
    }
}