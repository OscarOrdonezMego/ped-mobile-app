package pe.entel.android.pedidos.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Spinner;

import pe.entel.android.pedidos.R;

/**
 * Created by rtamayov on 25/05/2015.
 */
@SuppressLint("AppCompatCustomView")
public class MiSpinner extends Spinner {
    private void inflate() {
        TypedArray ta = getContext().obtainStyledAttributes(new int[]{R.attr.PedidosSpinnerBackground});
        setBackgroundResource(R.drawable.spinner_bg);
    }

    public MiSpinner(Context context) {
        super(context);
        inflate();
    }

    public MiSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate();
    }
}
