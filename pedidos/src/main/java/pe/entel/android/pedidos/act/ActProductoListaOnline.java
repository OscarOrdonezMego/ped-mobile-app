package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;

import pe.com.nextel.android.actividad.NexInvalidOperatorActivity.NexInvalidOperatorException;
import pe.com.nextel.android.actividad.support.NexFragmentActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.Logger;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.util.support.DialogFragmentYesNo;
import pe.com.nextel.android.util.support.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.support.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.support.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexProgressDialog;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.adap.AdapProductoListaOnline;
import pe.entel.android.pedidos.bean.BeanConsulta;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalCobranza;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.http.HttpConsultaPagoOnline;
import pe.entel.android.pedidos.http.HttpConsultaPedidoOnline;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Preferencias;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

public class ActProductoListaOnline extends NexFragmentActivity implements
        OnItemClickListener, NexMenuCallbacks, MenuSlidingListener {

    private ListView lista;

    private LinkedList<String> ioListaString;

    BeanUsuario loBeanUsuario = null;
    private AdapMenuPrincipalSliding _menu;

    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDASINCRONIZACION = 11;
    private final int ENVIOPENDIENTES = 12;
    private final int VALIDASINCRO = 13;
    private final int NOHAYPENDIENTES = 14;
    private final int CONSNOREGISTROPEDIDO = 15;
    private final int CONSNOREGISTROPAGO = 17;
    private final int ERRORAFTERDATE = 16;

    boolean estadoSincronizar = false;
    boolean isConsultaPedido = false;
    boolean isConsultaPago = false;

    Bundle savedInstanceState;

    String fecha = "";

    NexProgressDialog progress;

    private String isPresentacion;

    // Integracion Ntrack
    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        Intent loInstent = new Intent(ActProductoListaOnline.this,
                ActMenu.class);
        loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loInstent);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Logger.d("XXX", "ActProductoListaOnline");
        ValidacionServicioUtil.validarServicio(this);
        this.savedInstanceState = savedInstanceState;

        String lsBeanUsuario = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario,
                BeanUsuario.class);

        isPresentacion = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(this);

        setUsingContextFragment(true);
        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);

        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));

        subIniMenu();

        progress = new NexProgressDialog(ActProductoListaOnline.this);
        if (isUsingDialogFragment())
            progress.setUsesNewLayout(true);
        progress.setMessage("Cargando datos...");

    }

    @Override
    protected void subSetControles() {
        // TODO Auto-generated method stub
        super.subSetControles();

        try {
            setActionBarContentView(R.layout.productolistaonline);
            lista = (ListView) findViewById(R.id.actproductolistaonline_lista);
            lista.setOnItemClickListener(this);

        } catch (Exception e) {
            Log.e("subSetControles", e.toString());
            subShowException(e);
        }

    }

    @SuppressWarnings("unused")
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                            long arg3) {

        if (position == 0)
            fromStockOnline();
        else if (position == 1) {
            if (new DalCliente(this).existeTable()) {
                isConsultaPedido = true;
                isConsultaPago = false;
                showDatePicker();
            } else {
                showDialog(VALIDASINCRONIZACION);
            }
        } else if (position == 2) {
            isConsultaPago = true;
            isConsultaPedido = false;
            showDatePickerConsultaPagos();
        }

    }

    private void fromStockOnline() {
        ((AplicacionEntel) getApplication()).setCurrentCliente(null);
        ((AplicacionEntel) getApplication())
                .setCurrentCodFlujo(Configuracion.CONSSTOCK);
        ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
        Intent loIntento = new Intent(ActProductoListaOnline.this,
                ActProductoBuscar.class);
        BeanExtras loBeanExtras = new BeanExtras();
        loBeanExtras.setFiltro("");
        loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
        String lsFiltro = "";
        try {
            lsFiltro = BeanMapper.toJson(loBeanExtras, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
        startActivity(loIntento);
        finish();
    }

    private void showDatePicker() {
        CaldroidFragment dialogCaldroidFragment = new CaldroidFragment();

        TypedArray ta = obtainStyledAttributes(new int[]{R.attr.PedidosPopupCalendar});
        dialogCaldroidFragment.setDrawableLogo(ta.getResourceId(0, 0));
        ta.recycle();

        dialogCaldroidFragment.setCancelable(true);
        dialogCaldroidFragment.setCaldroidListener(new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {

                BeanConsulta oConsulta = new BeanConsulta();
                oConsulta.setCodVendedor(loBeanUsuario.getCodVendedor());
                oConsulta.setFechaConsulta(fnFecha(date));
                oConsulta.setTipoArticulo(isPresentacion.equals(Configuracion.FLGVERDADERO)
                        ? "PRE" : "PRO");
                fecha = DateFormat.format("dd-MM-yyyy", date).toString();
                try {

                    GregorianCalendar selectDate = new GregorianCalendar();
                    selectDate.setTime(date);
                    GregorianCalendar currentDate = new GregorianCalendar();
                    currentDate.setTime(new Date());

                    if (selectDate.before(currentDate)) {
                        progress.show();
                        new HttpConsultaPedidoOnline(oConsulta,
                                ActProductoListaOnline.this, fnTipactividad())
                                .execute();
                    } else {
                        showDialog(ERRORAFTERDATE);
                    }

                } catch (NexInvalidOperatorException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        });
        // If activity is recovered from rotation
        final String dialogTag = "CALDROID_DIALOG_FRAGMENT";
        final Bundle statebun = savedInstanceState;
        if (statebun != null) {
            dialogCaldroidFragment.restoreDialogStatesFromKey(
                    getSupportFragmentManager(), statebun,
                    "DIALOG_CALDROID_SAVED_STATE", dialogTag);
            Bundle args = dialogCaldroidFragment.getArguments();
            if (args == null) {
                args = new Bundle();
                dialogCaldroidFragment.setArguments(args);
            }

        } else {
            // Setup arguments
            Bundle bundle = new Bundle();
            // Setup dialogTitle

            dialogCaldroidFragment.setArguments(bundle);
        }

        dialogCaldroidFragment.show(getSupportFragmentManager(), dialogTag);
    }

    public void showDatePickerConsultaPagos() {
        CaldroidFragment dialogCaldroidFragment = new CaldroidFragment();

        TypedArray ta = obtainStyledAttributes(new int[]{R.attr.PagosPopupCalendar});
        dialogCaldroidFragment.setDrawableLogo(ta.getResourceId(0, 0));
        ta.recycle();

        dialogCaldroidFragment.setCancelable(true);
        dialogCaldroidFragment.setCaldroidListener(new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {

                BeanConsulta oConsulta = new BeanConsulta();
                oConsulta.setCodVendedor(loBeanUsuario.getCodVendedor());
                oConsulta.setFechaConsulta(fnFecha(date));

                fecha = DateFormat.format("dd-MM-yyyy", date).toString();
                try {

                    GregorianCalendar selectDate = new GregorianCalendar();
                    selectDate.setTime(date);
                    GregorianCalendar currentDate = new GregorianCalendar();
                    currentDate.setTime(new Date());

                    if (selectDate.before(currentDate)) {
                        progress.show();
                        new HttpConsultaPagoOnline(oConsulta,
                                ActProductoListaOnline.this, fnTipactividad())
                                .execute();
                    } else {
                        showDialog(ERRORAFTERDATE);
                    }


                } catch (NexInvalidOperatorException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        });
        // If activity is recovered from rotation
        final String dialogTag = "CALDROID_DIALOG_FRAGMENT";
        final Bundle statebun = savedInstanceState;
        if (statebun != null) {
            dialogCaldroidFragment.restoreDialogStatesFromKey(
                    getSupportFragmentManager(), statebun,
                    "DIALOG_CALDROID_SAVED_STATE", dialogTag);
            Bundle args = dialogCaldroidFragment.getArguments();
            if (args == null) {
                args = new Bundle();
                dialogCaldroidFragment.setArguments(args);
            }

        } else {
            // Setup arguments
            Bundle bundle = new Bundle();
            // Setup dialogTitle

            dialogCaldroidFragment.setArguments(bundle);
        }

        dialogCaldroidFragment.show(getSupportFragmentManager(), dialogTag);
    }

    private String fnFecha(Date date) {

        String lsRetorno = DateFormat.format("yyyyMMdd", date).toString();
        return lsRetorno;

    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setTitle(
                getString(R.string.actproductoonline_bartitulo));

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        subLLenarLista();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {
        progress.dismiss();
        Log.i("subaccdesmensaje", "entrroo");
        if (estadoSincronizar) {
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActMenu.class);

                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }
        } else {
            try {
                if (getIdHttpResultado() != ConfiguracionNextel.CONSRESSERVIDORERROR) {
                    Log.i("isConsultaPago", "-" + String.valueOf(isConsultaPago));
                    Log.i("isConsultaPedido", "-" + String.valueOf(isConsultaPedido));
                    if (isConsultaPedido) {
                        mostrarConsultaPedido();
                    } else if (isConsultaPago) {
                        mostrarConsultaPago();
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();

            }

        }

    }

    public void mostrarConsultaPedido() {
        try {
            if (new DalPedido(getApplicationContext())
                    .fnCountPedidosOnline() > 0) {
                isConsultaPedido = false;
                Intent intent = new Intent(
                        ActProductoListaOnline.this,
                        ActPedidoListaOnline.class);

                Preferencias.fnGuardarCodigo(ActProductoListaOnline.this, Configuracion.CURRENT_FECHA_REPORTE, fecha);
                Preferencias.fnGuardarCodigo(ActProductoListaOnline.this, Configuracion.CURRENT_TIPO_GENERAL, Configuracion.FLGVERDADERO);

                finish();
                startActivity(intent);

            } else {
                showDialog(CONSNOREGISTROPEDIDO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mostrarConsultaPago() {
        try {
            if (new DalCobranza(getApplicationContext()).fnCountPagosOnline() > 0) {
                isConsultaPago = false;

                Intent intent = new Intent(
                        ActProductoListaOnline.this,
                        ActPagoListaOnline.class);

                Bundle bundle = new Bundle();
                bundle.putString("fecha", fecha);

                intent.putExtras(bundle);

                startActivity(intent);

            } else {
                showDialog(CONSNOREGISTROPAGO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void subLLenarLista() {
        try {

            ioListaString = new LinkedList<String>();
            ioListaString.add("Consulta Stock");
            ioListaString.add("Consulta Pedido");
            ioListaString.add("Consulta Pagos");

            AdapProductoListaOnline adapter = new AdapProductoListaOnline(this,
                    R.layout.productolistaonline_item, ioListaString);
            lista.setAdapter(adapter);

        } catch (Exception e) {
            subShowException(e);
        }
    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent loIntentLogin = new Intent(ActProductoListaOnline.this,
                        ActLogin.class);
                loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(loIntentLogin);
            }
        });

    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    EnumMenuPrincipalSlidingItem.STOCK.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        Intent intentl = new Intent(this, ActMenu.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intentl);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub
        DalCliente loDalCliente = new DalCliente(this);

        // Verificamos si existe la tabla.
        boolean lbResult = loDalCliente.existeTable();

        if (lbResult) {
            Intent loInstent = new Intent(ActProductoListaOnline.this,
                    ActClienteBuscar.class);
            loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            BeanExtras loBeanExtras = new BeanExtras();
            loBeanExtras.setFiltro("");
            loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
            String lsFiltro = "";
            try {
                lsFiltro = BeanMapper.toJson(loBeanExtras, false);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            loInstent.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
            finish();
            startActivity(loInstent);
        } else {
            showDialog(VALIDASINCRONIZACION);
        }

    }

    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = null;
            DalPedido loDalPedido = new DalPedido(this);
            loList = loDalPedido.fnEnvioPedidosPendientes(loBeanUsuario
                    .getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);

                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();
                showDialog(CONSDIASINCRONIZAR);
            } else {
                showDialog(VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            showDialog(existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub
        String lsBeanUsuario = getSharedPreferences(
                "Actual", MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper
                .fromJson(lsBeanUsuario,
                        BeanUsuario.class);
        this.setMsgOk("Eficiencia Online");

        BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
        loBeanListaEficienciaOnline.setIdResultado(0);
        loBeanListaEficienciaOnline.setResultado("");
        loBeanListaEficienciaOnline.setListaEficiencia(null);
        loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

        new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

        Intent loInstent = new Intent(this, ActKpiDetalle.class);
        loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loInstent);
    }

    @Override
    public void subStockFromSliding() {

    }

    @Override
    protected Dialog onCreateDialog(int id) {

        AlertDialog loAlertDialog = null;

        switch (id) {
            case ERRORAFTERDATE:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("INFORMACIÓN")
                        .setMessage(getString(R.string.msgdianacualanterior))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSNOREGISTROPEDIDO:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("INFORMACIÓN")
                        .setMessage(getString(R.string.msg_error_pedidoonline))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(ActProductoListaOnline.this, ActProductoListaOnline.class));
                                    }
                                }).create();
                return loAlertDialog;
            case CONSNOREGISTROPAGO:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("INFORMACIÓN")
                        .setMessage(getString(R.string.msg_error_pagoonline))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        startActivity(new Intent(ActProductoListaOnline.this, ActProductoListaOnline.class));
                                    }
                                }).create();
                return loAlertDialog;
            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            estadoSincronizar = true;
                                            Log.i("ALERTDIALOG", "TIPO ACTIVIDAD: "
                                                    + fnTipactividad().ordinal());
                                            progress.show();
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActProductoListaOnline.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })

                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActProductoListaOnline.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            default:

                return super.onCreateDialog(id);
        }
    }

    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        DialogFragmentYesNo loDialog;
        switch (id) {
            case DIALOG_NSERVICES_DOWNLOAD:
                String lsNServices;
                if (ioDALNservices == null)
                    try {
                        ioDALNservices = DALNServices.getInstance(this,
                                R.string.db_name);
                    } catch (NexInvalidOperatorException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                try {
                    lsNServices = ioDALNservices.fnSelNServicesLabel();
                } catch (Exception e) {
                    Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                    lsNServices = getString(R.string.nservices_label_default);
                }
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG + DIALOG_NSERVICES_DOWNLOAD,
                        getString(R.string.nservices_consultingdownlad).replace(
                                "{0}", lsNServices),
                        EnumLayoutResource.getDefaultIconHelp(this));
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    subIniNServicesDownload(ioDALNservices
                                            .fnSelNServicesAPK());
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                            }
                        });
            case ERRORAFTERDATE:
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ERRORAFTERDATE"),
                        getString(R.string.msgdianacualanterior),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case CONSNOREGISTROPEDIDO:
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSNOREGISTRO"),
                        getString(R.string.msg_error_pedidoonline),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {

                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.actmenu_dlgsincronizar),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {

                                    estadoSincronizar = true;
                                    progress.show();
                                    new HttpSincronizar(loBeanUsuario
                                            .getCodVendedor(),
                                            ActProductoListaOnline.this,
                                            fnTipactividad()).execute();

                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            case VALIDASINCRONIZACION:
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"),
                        getString(R.string.actmenuvalsincro_titulo),
                        getString(R.string.actmenuvalsincro_mensaje),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case VALIDASINCRO:
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                        getString(R.string.msgregpendientesenvinfo),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case ENVIOPENDIENTES:
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"),
                        getString(R.string.msgdeseaenviarpendientes),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                Intent loIntentEspera = new Intent(
                                        ActProductoListaOnline.this,
                                        ActGrabaPendiente.class);
                                startActivity(loIntentEspera);
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            default:
                return super.onCreateNexDialog(id);
        }

    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(
                    getString(R.string.ml_sincronizarantes)));
        }

    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */
    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }
}