package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.util.Configuracion;

public class AdapPedidoListaOnline extends ArrayAdapter<BeanEnvPedidoCab> {
    int resource;
    Context context;
    TextView rowTextView;

    LinkedList<BeanEnvPedidoCab> originalList;

    int cont = 0;
    String flgMoneda;
    String flgMonedaDolar;
    String flgNumDecVista;

    public AdapPedidoListaOnline(Context _context, int _resource,
                                 LinkedList<BeanEnvPedidoCab> lista) {
        super(_context, _resource, lista);
        originalList = lista;
        resource = _resource;
        context = _context;

        flgMoneda = Configuracion.EnumConfiguracion.SIMBOLO_MONEDA.getValor(context);
        flgMonedaDolar = "$";
        flgNumDecVista = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(context);

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BeanEnvPedidoCab item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        String nomCliente = "[" + item.getCodigoPedidoServidor() + "] "
                + item.getNombreCliente();


        if (!item.isProcesado()) {
            ((LinearLayout) nuevaVista.findViewById(R.id.actpedidolistaonline_lncontent))
                    .setBackgroundResource(R.drawable.pedidos_productopedido_bonificacion_listview_bg);
        } else {
            ((LinearLayout) nuevaVista.findViewById(R.id.actpedidolistaonline_lncontent))
                    .setBackgroundResource(R.drawable.pedidos_productopedido_listview_bg);
        }


        ((TextView) nuevaVista
                .findViewById(R.id.actproductopedidoitem_txtnombreCliente))
                .setText(nomCliente);
        ((TextView) nuevaVista
                .findViewById(R.id.actproductopedidoitem_txtfecha))
                .setText(item.getFechaRegistro().substring(11));
        ((TextView) nuevaVista
                .findViewById(R.id.actproductopedidoitem_txtmonto))
                .setText(flgMoneda
                        + " "
                        + Utilitario.fnRound(item.getMontoTotalSoles(), Integer //@JBELVY Before getMontoTotal
                        .valueOf(flgNumDecVista)));

        ((TextView) nuevaVista
                .findViewById(R.id.actproductopedidoitem_txtmontoDolar))
                .setText(flgMonedaDolar
                        + " "
                        + Utilitario.fnRound(item.getMontoTotalDolar(), Integer
                        .valueOf(flgNumDecVista))); //@JBELVY

        ((TextView) nuevaVista
                .findViewById(R.id.actproductopedidoitem_txttipopedido))
                .setBackgroundColor(context.getResources()
                        .getColor(item.getFlgTipo()
                                .equals(Configuracion.FLGPEDIDO)
                                ? R.color.pedidos_online_pedido
                                : R.color.pedidos_online_cotizacion));
        ((TextView) nuevaVista
                .findViewById(R.id.actproductopedidoitem_txttipopedido))
                .setText(item.getFlgTipo());


        return nuevaVista;
    }

}
