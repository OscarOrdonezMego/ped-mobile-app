package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Created by rtamayov on 24/09/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanVerificarPedido {

    @JsonProperty("BonificacionTotal")
    private String bonificacionTotal;

    @JsonProperty("CantidadTotal")
    private String cantidadTotal;

    @JsonProperty("Celda")
    private String celda;

    @JsonProperty("CliPk")
    private String cliPk;

    @JsonProperty("CodAlmacen")
    private String codAlmacen;

    @JsonProperty("CodDireccion")
    private String codDireccion;

    @JsonProperty("CodDireccionDespacho")
    private String codDireccionDespacho;

    @JsonProperty("CodigoCliente")
    private String codigoCliente;

    @JsonProperty("CodigoDireccion")
    private String codigoDireccion;

    @JsonProperty("CodigoDireccionDespacho")
    private String codigoDireccionDespacho;

    @JsonProperty("CodigoMotivo")
    private String codigoMotivo;

    @JsonProperty("CodigoPedido")
    private String codigoPedido;

    @JsonProperty("CodigoPedidoServidor")
    private String codigoPedidoServidor;

    @JsonProperty("CodigoUsuario")
    private String codigoUsuario;

    @JsonProperty("CondicionVenta")
    private String condicionVenta;

    @JsonProperty("Empresa")
    private String empresa;

    @JsonProperty("ErrorConexion")
    private String errorConexion;

    @JsonProperty("ErrorPosicion")
    private String errorPosicion;

    @JsonProperty("FechaFin")
    private String fechaFin;

    @JsonProperty("FechaInicio")
    private String fechaInicio;

    @JsonProperty("FleteTotal")
    private String fleteTotal;

    @JsonProperty("FlgEliminar")
    private String flgEliminar;

    @JsonProperty("FlgEnCobertura")
    private String flgEnCobertura;

    @JsonProperty("FlgNuevoDireccion")
    private String flgNuevoDireccion;

    @JsonProperty("FlgTerminado")
    private String flgTerminado;

    @JsonProperty("FlgTipo")
    private String flgTipo;

    @JsonProperty("Latitud")
    private String latitud;

    @JsonProperty("ListaCtrlPedido")
    private List<BeanVerificarPedidoControl> listaCtrlPedido;

    @JsonProperty("ListaDetPedido")
    private List<BeanVerificarPedidoDetalle> listaDetPedido;

    @JsonProperty("Longitud")
    private String longitud;

    @JsonProperty("MontoTotal")
    private String montoTotal;

    @JsonProperty("Observacion")
    private String observacion;

    @JsonProperty("TipoArticulo")
    private String tipoArticulo;

    @JsonProperty("CreditoUtilizado")
    private String creditoUtilizado;

    public String getBonificacionTotal() {
        return bonificacionTotal;
    }

    public void setBonificacionTotal(String bonificacionTotal) {
        this.bonificacionTotal = bonificacionTotal;
    }

    public String getCantidadTotal() {
        return cantidadTotal;
    }

    public void setCantidadTotal(String cantidadTotal) {
        this.cantidadTotal = cantidadTotal;
    }

    public String getCelda() {
        return celda;
    }

    public void setCelda(String celda) {
        this.celda = celda;
    }

    public String getCliPk() {
        return cliPk;
    }

    public void setCliPk(String cliPk) {
        this.cliPk = cliPk;
    }

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public String getCodDireccion() {
        return codDireccion;
    }

    public void setCodDireccion(String codDireccion) {
        this.codDireccion = codDireccion;
    }

    public String getCodDireccionDespacho() {
        return codDireccionDespacho;
    }

    public void setCodDireccionDespacho(String codDireccionDespacho) {
        this.codDireccionDespacho = codDireccionDespacho;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getCodigoDireccion() {
        return codigoDireccion;
    }

    public void setCodigoDireccion(String codigoDireccion) {
        this.codigoDireccion = codigoDireccion;
    }

    public String getCodigoDireccionDespacho() {
        return codigoDireccionDespacho;
    }

    public void setCodigoDireccionDespacho(String codigoDireccionDespacho) {
        this.codigoDireccionDespacho = codigoDireccionDespacho;
    }

    public String getCodigoMotivo() {
        return codigoMotivo;
    }

    public void setCodigoMotivo(String codigoMotivo) {
        this.codigoMotivo = codigoMotivo;
    }

    public String getCodigoPedido() {
        return codigoPedido;
    }

    public void setCodigoPedido(String codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public String getCodigoPedidoServidor() {
        return codigoPedidoServidor;
    }

    public void setCodigoPedidoServidor(String codigoPedidoServidor) {
        this.codigoPedidoServidor = codigoPedidoServidor;
    }

    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getCondicionVenta() {
        return condicionVenta;
    }

    public void setCondicionVenta(String condicionVenta) {
        this.condicionVenta = condicionVenta;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getErrorConexion() {
        return errorConexion;
    }

    public void setErrorConexion(String errorConexion) {
        this.errorConexion = errorConexion;
    }

    public String getErrorPosicion() {
        return errorPosicion;
    }

    public void setErrorPosicion(String errorPosicion) {
        this.errorPosicion = errorPosicion;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFleteTotal() {
        return fleteTotal;
    }

    public void setFleteTotal(String fleteTotal) {
        this.fleteTotal = fleteTotal;
    }

    public String getFlgEliminar() {
        return flgEliminar;
    }

    public void setFlgEliminar(String flgEliminar) {
        this.flgEliminar = flgEliminar;
    }

    public String getFlgEnCobertura() {
        return flgEnCobertura;
    }

    public void setFlgEnCobertura(String flgEnCobertura) {
        this.flgEnCobertura = flgEnCobertura;
    }

    public String getFlgNuevoDireccion() {
        return flgNuevoDireccion;
    }

    public void setFlgNuevoDireccion(String flgNuevoDireccion) {
        this.flgNuevoDireccion = flgNuevoDireccion;
    }

    public String getFlgTerminado() {
        return flgTerminado;
    }

    public void setFlgTerminado(String flgTerminado) {
        this.flgTerminado = flgTerminado;
    }

    public String getFlgTipo() {
        return flgTipo;
    }

    public void setFlgTipo(String flgTipo) {
        this.flgTipo = flgTipo;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public List<BeanVerificarPedidoControl> getListaCtrlPedido() {
        return listaCtrlPedido;
    }

    public void setListaCtrlPedido(List<BeanVerificarPedidoControl> listaCtrlPedido) {
        this.listaCtrlPedido = listaCtrlPedido;
    }

    public String getCreditoUtilizado() {
        return creditoUtilizado;
    }

    public void setCreditoUtilizado(String creditoUtilizado) {
        this.creditoUtilizado = creditoUtilizado;
    }

    public List<BeanVerificarPedidoDetalle> getListaDetPedido() {
        return listaDetPedido;
    }

    public void setListaDetPedido(List<BeanVerificarPedidoDetalle> listaDetPedido) {
        this.listaDetPedido = listaDetPedido;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(String montoTotal) {
        this.montoTotal = montoTotal;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }

    public static BeanVerificarPedido mapperVerificarPedido(BeanEnvPedidoCab pedido) {
        BeanVerificarPedido verificarPedido = new BeanVerificarPedido();

        verificarPedido.setBonificacionTotal(pedido.getBonificacionTotal());
        verificarPedido.setCantidadTotal(pedido.getCantidadTotal());
        verificarPedido.setCelda(pedido.getCelda());
        verificarPedido.setCliPk(pedido.getClipk());
        verificarPedido.setCodAlmacen(pedido.getCodAlmacen());
        verificarPedido.setCodDireccion(pedido.getCodDireccion());
        verificarPedido.setCodDireccionDespacho(pedido.getCodDireccionDespacho());
        verificarPedido.setCodigoCliente(pedido.getCodigoCliente());
        verificarPedido.setCodigoDireccion(pedido.getCodigoDireccion());
        verificarPedido.setCodigoDireccionDespacho(pedido.getCodigoDireccionDespacho());
        verificarPedido.setCodigoMotivo(pedido.getCodigoMotivo());
        verificarPedido.setCodigoPedido(pedido.getCodigoPedido());
        verificarPedido.setCodigoPedidoServidor(pedido.getCodigoPedidoServidor());
        verificarPedido.setCodigoUsuario(pedido.getCodigoUsuario());
        verificarPedido.setCondicionVenta(pedido.getCondicionVenta());
        verificarPedido.setEmpresa(pedido.getEmpresa());
        verificarPedido.setErrorConexion(pedido.getErrorConexion());
        verificarPedido.setErrorPosicion(pedido.getErrorPosicion());
        verificarPedido.setFechaFin(pedido.getFechaFin());
        verificarPedido.setFechaInicio(pedido.getFechaInicio());
        verificarPedido.setFleteTotal(pedido.getFleteTotal());
        verificarPedido.setFlgEliminar(pedido.getFlgEliminar());
        verificarPedido.setFlgEnCobertura(pedido.getFlgEnCobertura());
        verificarPedido.setFlgNuevoDireccion(pedido.getFlgNuevoDireccion());
        verificarPedido.setFlgTerminado(pedido.getFlgTerminado());
        verificarPedido.setFlgTipo(pedido.getFlgTipo());
        verificarPedido.setLatitud(pedido.getLatitud());
        verificarPedido.setLongitud(pedido.getLongitud());
        verificarPedido.setListaCtrlPedido(BeanVerificarPedidoControl.mapperListaVerificarPedidoControl(pedido.getListaCtrlPedido()));
        verificarPedido.setListaDetPedido(BeanVerificarPedidoDetalle.mapperListaVerificarPedidoDetalle(pedido.getListaDetPedido()));
        verificarPedido.setMontoTotal(pedido.getMontoTotal());
        verificarPedido.setObservacion(pedido.getObservacion());
        verificarPedido.setTipoArticulo(pedido.getTipoArticulo());
        verificarPedido.setCreditoUtilizado(pedido.getCreditoUtilizado());

        return verificarPedido;
    }

}
