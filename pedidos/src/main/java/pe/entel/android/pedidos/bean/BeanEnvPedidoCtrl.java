package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by rtamayov on 26/05/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanEnvPedidoCtrl {
    @JsonProperty
    private String etiquetaControl;
    @JsonProperty
    private String idTipoControl;
    @JsonProperty
    private String valorControl;
    @JsonProperty
    private String idControl;
    @JsonProperty
    private String idFormulario;

    public String getEtiquetaControl() {
        return etiquetaControl;
    }

    public void setEtiquetaControl(String etiquetaControl) {
        this.etiquetaControl = etiquetaControl;
    }

    public String getIdTipoControl() {
        return idTipoControl;
    }

    public void setIdTipoControl(String idTipoControl) {
        this.idTipoControl = idTipoControl;
    }

    public String getValorControl() {
        return valorControl;
    }

    public void setValorControl(String valorControl) {
        this.valorControl = valorControl;
    }

    public String getIdControl() {
        return idControl;
    }

    public void setIdControl(String idControl) {
        this.idControl = idControl;
    }

    public String getIdFormulario() {
        return idFormulario;
    }

    public void setIdFormulario(String idFormulario) {
        this.idFormulario = idFormulario;
    }
}
