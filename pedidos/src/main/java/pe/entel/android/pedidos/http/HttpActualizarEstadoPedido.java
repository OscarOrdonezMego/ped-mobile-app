package pe.entel.android.pedidos.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumUrl;
import pe.entel.android.pedidos.util.UtilitarioData;

public class HttpActualizarEstadoPedido extends HttpConexion {
    private BeanEnvPedidoCab ioBeanPedidoCab;
    private Context ioContext;

    public HttpActualizarEstadoPedido(BeanEnvPedidoCab psBeanVisitaCab, Context poContext, EnumTipActividad poEnumTipActividad) {
        super(poContext, poContext.getString(R.string.httpproductopedido_dlgprocesado), poEnumTipActividad);
        ioBeanPedidoCab = psBeanVisitaCab;
        ioContext = poContext;
    }

    @Override
    public boolean OnPreConnect() {
        if (!UtilitarioData.fnObtenerPreferencesBoolean(ioContext, "pedidoPendiente")) {
            SharedPreferences loSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ioContext);
            if (loSharedPreferences.getBoolean(ioContext.getString(R.string.keypre_cobertura), false)) {
                setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_modofueracobertura));
                return false;
            }
        }
        int signal = UtilitarioData.fnSignalLevel(ioContext);
        if (!Utilitario.fnVerSignal(ioContext) || signal < 2) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    public void subConHttp() {
        try {
            //RestTemplate restTemplate = new RestTemplate();
            //restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

            String lsRequest = BeanMapper.toJson(ioBeanPedidoCab, false);
            Log.i("URL: ", Configuracion.fnUrl(ioContext, EnumUrl.ACTUALIZARPROCESADO));
            Log.i("REQUEST: ", lsRequest);

            //String lsResultado = restTemplate.postForObject(Configuracion.fnUrl(ioContext, EnumUrl.ACTUALIZARPROCESADO), ioBeanPedidoCab, String.class);
            String lsResultado = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, EnumUrl.ACTUALIZARPROCESADO), ioBeanPedidoCab);
            setHttpResponseObject(lsResultado);

        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, "Error HTTP: " + e.getMessage());
        }
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            String lsResultado = (String) getHttpResponseObject();
            String[] laRespuesta = lsResultado.split(";");
            int liIdHttpRespuesta = Integer.valueOf(laRespuesta[0]);
            String lsHttpRespuesta = laRespuesta[1];
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.get(liIdHttpRespuesta), lsHttpRespuesta);
        }
    }
}