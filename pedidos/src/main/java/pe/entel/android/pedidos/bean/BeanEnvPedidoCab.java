package pe.entel.android.pedidos.bean;

import android.content.Context;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.dal.DalAlmacen;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalProducto;
import pe.entel.android.pedidos.dal.DalTipoCambio;
import pe.entel.android.pedidos.util.Configuracion;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanEnvPedidoCab {
    private String clipk;
    @JsonProperty
    private String codigoPedido;
    @JsonProperty
    private String empresa;
    @JsonProperty
    private String codigoUsuario;
    @JsonProperty
    private String fechaInicio;
    @JsonProperty
    private String fechaFin;
    @JsonProperty
    private String condicionVenta;
    @JsonProperty
    private String codigoCliente;
    @JsonProperty
    private String codigoMotivo;
    @JsonProperty
    private String montoTotal;
    @JsonProperty
    private String montoTotalSoles; //@JBELVY
    @JsonProperty
    private String montoTotalDolar; //@JBELVY
    @JsonProperty
    private String tipoCambioSoles; //@JBELVY
    @JsonProperty
    private String tipoCambioDolar; //@JBELVY
    @JsonProperty
    private String codDireccion = "0";
    @JsonProperty
    private String codigoDireccion = "";
    @JsonProperty
    private String codAlmacen = "0";
    @JsonProperty
    private String codDireccionDespacho = "0";
    @JsonProperty
    private String codigoDireccionDespacho = "";
    @JsonProperty
    private String latitud;
    @JsonProperty
    private String longitud;
    @JsonProperty
    private String celda;
    @JsonProperty
    private String errorConexion;
    @JsonProperty
    private String errorPosicion;
    @JsonProperty
    private String observacion;
    @JsonProperty
    private String cantidadTotal;
    @JsonProperty
    private String bonificacionTotal;
    @JsonProperty
    private List<BeanEnvPedidoDet> listaDetPedido;
    @JsonProperty
    private String flgEnCobertura;
    @JsonProperty
    private String flgTerminado;
    @JsonProperty
    private String codigoPedidoServidor;
    @JsonProperty
    private String flgEliminar;
    @JsonProperty
    private String flgTipo;
    @JsonProperty
    private String flgNuevoDireccion;
    @JsonProperty
    private String fleteTotal;
    @JsonProperty
    private String tipoArticulo;
    @JsonProperty
    private List<BeanEnvPedidoCtrl> listaCtrlPedido;

    @JsonProperty
    private String creditoUtilizado;

    @JsonProperty
    private boolean enEdicion;
    @JsonProperty
    private boolean procesado;
    private String nombreCliente;
    private String fechaRegistro;

    public BeanEnvPedidoCab() {
        clipk = "";
        codigoPedido = "0";
        empresa = "";
        codigoUsuario = "";
        fechaInicio = "";
        fechaFin = "";
        condicionVenta = "";
        codigoCliente = "";
        codigoMotivo = "";
        montoTotal = "0";
        montoTotalSoles = "0"; //@JBELVY
        montoTotalDolar = "0"; //@JBELVY
        tipoCambioSoles = "0"; //@JBELVY
        tipoCambioDolar = "0"; //@JBELVY
        codDireccion = "";
        codDireccionDespacho = "0";
        latitud = "0";
        longitud = "0";
        celda = "";
        errorConexion = "0";
        errorPosicion = "0";
        observacion = "";
        cantidadTotal = "0";
        bonificacionTotal = "0";
        setListaDetPedido(new ArrayList<BeanEnvPedidoDet>());
        flgEnCobertura = "1";
        flgTerminado = "T";
        codigoPedidoServidor = "0";
        flgEliminar = "F";
        flgTipo = "";
        flgNuevoDireccion = Configuracion.FLGFALSO;
        fleteTotal = "0";
        setListaCtrlPedido(new ArrayList<BeanEnvPedidoCtrl>());
        creditoUtilizado = "0";
        enEdicion = false;
        procesado = true;
        nombreCliente = "";
        fechaRegistro = "";
    }

    public BeanEnvPedidoCab(String psTipoArticulo) {
        clipk = "";
        codigoPedido = "0";
        empresa = "";
        codigoUsuario = "";
        fechaInicio = "";
        fechaFin = "";
        condicionVenta = "";
        codigoCliente = "";
        codigoMotivo = "";
        montoTotal = "0";
        montoTotalSoles = "0"; //@JBELVY
        montoTotalDolar = "0"; //@JBELVY
        tipoCambioSoles = "0"; //@JBELVY
        tipoCambioDolar = "0"; //@JBELVY
        codDireccion = "";
        codDireccionDespacho = "0";
        latitud = "0";
        longitud = "0";
        celda = "";
        errorConexion = "0";
        errorPosicion = "0";
        observacion = "";
        cantidadTotal = "0";
        bonificacionTotal = "0";
        setListaDetPedido(new ArrayList<BeanEnvPedidoDet>());
        flgEnCobertura = "1";
        flgTerminado = "T";
        codigoPedidoServidor = "0";
        flgEliminar = "F";
        flgTipo = "";
        flgNuevoDireccion = Configuracion.FLGFALSO;
        fleteTotal = "0";
        tipoArticulo = psTipoArticulo;
        setListaCtrlPedido(new ArrayList<BeanEnvPedidoCtrl>());
        creditoUtilizado = "0";
        enEdicion = false;
        procesado = true;
        nombreCliente = "";
        fechaRegistro = "";
    }

    public void agregarProductoPedido(BeanEnvPedidoDet dPedido) {
        listaDetPedido.add(dPedido);
    }

    public void actualizarProductoPedido(BeanEnvPedidoDet dPedido) {
        BeanEnvPedidoDet bean = null;

        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);

            if (bean.getIdArticulo().equals(dPedido.getIdArticulo())) {
                listaDetPedido.set(i, dPedido);
                return;
            }
        }
    }

    public String calcularMontoCompleto() {
        return String.valueOf(Double.parseDouble(calcularMontoTotalSoles()) + Double.parseDouble(calcularTotalFlete()));
    }

    public String calcularMontoSolesCompleto() {
        return String.valueOf(Double.parseDouble(calcularMontoTotalSoles()) + Double.parseDouble(calcularTotalFlete()));
    } //@JBELVY

    public String calcularMontoDolarCompleto() {
        return String.valueOf(Double.parseDouble(calcularMontoTotalDolar()) + Double.parseDouble(calcularTotalFlete()));
    } //@JBELVY

    public String calcularMontoCompletoVerificarPedido() {
        return String.valueOf(Double.parseDouble(calcularMontoTotalVerificarPedido()) + Double.parseDouble(calcularTotalFlete()));
    }

    public String calcularMontoSolesCompletoVerificarPedido() {
        return String.valueOf(Double.parseDouble(calcularMontoTotalVerificarPedidoSoles()) + Double.parseDouble(calcularTotalFlete()));
    } //@JBELVY

    public String calcularMontoDolarCompletoVerificarPedido() {
        return String.valueOf(Double.parseDouble(calcularMontoTotalVerificarPedidoDolar()) + Double.parseDouble(calcularTotalFlete()));
    }//@JBELVY

    public String calcularMontoTotal() {
        Double total = 0.00;
        BeanEnvPedidoDet bean = null;

        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);
            total = total + Double.parseDouble(bean.getMonto().equals("") ? "0" : bean.getMonto());
        }

        this.setMontoTotal(total.toString());
        return String.valueOf(total);
    }

    public String calcularMontoTotalSoles() {
        Double totalSoles = 0.00;
        BeanEnvPedidoDet bean = null;

        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);
            totalSoles = totalSoles + Double.parseDouble(bean.getMontoSoles().equals("") ? "0" : bean.getMontoSoles());
        }

        this.setMontoTotalSoles(totalSoles.toString());
        return String.valueOf(totalSoles);
    } //@JBELVY

    public String calcularMontoTotalDolar() {
        Double totalDolar = 0.00;
        BeanEnvPedidoDet bean = null;

        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);
            totalDolar = totalDolar + Double.parseDouble(bean.getMontoDolar().equals("") ? "0" : bean.getMontoDolar());
        }

        this.setMontoTotalDolar(totalDolar.toString());
        return String.valueOf(totalDolar);
    } //@JBELVY

    public String ObtenerIdTipoCambioSoles(Context poContext) {
        DalTipoCambio dalTipoCambio = new DalTipoCambio(poContext);
        String TipoCambioId = dalTipoCambio.fnObtenerIdTipoCambioPorMoneda("0");

        this.setTipoCambioSoles(TipoCambioId);
        tipoCambioSoles = TipoCambioId;
        return tipoCambioSoles;
    } //@JBELVY

    public String ObtenerIdTipoCambioDolares(Context poContext) {
        DalTipoCambio dalTipoCambio = new DalTipoCambio(poContext);
        String TipoCambioId = dalTipoCambio.fnObtenerIdTipoCambioPorMoneda("1");

        this.setTipoCambioDolar(TipoCambioId);
        tipoCambioDolar= TipoCambioId;
        return tipoCambioDolar;
    } //@JBELVY

    public String calcularTotalFlete() {

        double total = 0;
        BeanEnvPedidoDet bean = null;

        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);
            total = total + Double.parseDouble(bean.getFlete());
        }


        return fleteTotal = String.valueOf(total);
    }

    public String calcularMontoTotalVerificarPedido() {
        Double total = 0.00;
        BeanEnvPedidoDet bean = null;
        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);
            total = total + Double.parseDouble(bean.getMonto());
        }
        this.setMontoTotal(total.toString());
        return String.valueOf(total);
    } //@JBELVY

    public String calcularMontoTotalVerificarPedidoSoles() {
        Double totalSoles = 0.00;
        BeanEnvPedidoDet bean = null;
        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);
            totalSoles = totalSoles + Double.parseDouble(bean.getMontoSoles());
        }
        this.setMontoTotalSoles(totalSoles.toString());
        return String.valueOf(totalSoles);
    } //@JBELVY

    public String calcularMontoTotalVerificarPedidoDolar() {
        Double totalDolar = 0.00;
        BeanEnvPedidoDet bean = null;
        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);
            totalDolar = totalDolar + Double.parseDouble(bean.getMontoDolar());
        }
        this.setMontoTotalDolar(totalDolar.toString());
        return String.valueOf(totalDolar);
    }

    public String calcularCantTotalItems() {

        double total = 0;
        BeanEnvPedidoDet bean = null;

        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);
            total = total + Double.parseDouble(bean.getCantidad());
        }

        return String.valueOf(total);
    }

    public String calcularCantFracTotalItems(String isTipoArticulo, Integer numeroDecimales) {

        double total = 0;
        BeanEnvPedidoDet bean = null;

        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);
            total = total + Double.parseDouble(bean.fnMostrarCantidad(isTipoArticulo, numeroDecimales));
        }

        return String.valueOf(total);
    }

    public String calcularCantTotalItemsBonificados() {
        int total = 0;
        BeanEnvPedidoDet bean = null;

        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);

            if (bean.getBonificacion().equals("")) {
                bean.setBonificacion("0");
            }

            if (Double.parseDouble(bean.getBonificacion()) > 0) {
                total++;
            }
        }

        return String.valueOf(total);


    }

    public String calcularBonificacionTotal() {

        double ldBonificacionToal = 0;
        BeanEnvPedidoDet bean = null;

        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);

            if (bean.getBonificacion().equals("")) {
                bean.setBonificacion("0");
            }

            ldBonificacionToal = ldBonificacionToal + Double.parseDouble(bean.getBonificacion().trim());
        }

        return String.valueOf(ldBonificacionToal);
    }

    public void calcularMontos() {


    }

    public boolean existeProducto(String idProducto) {

        boolean flg = false;
        BeanEnvPedidoDet bean = null;

        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);

            if (bean.getIdArticulo().equals(String.valueOf(idProducto)) &&
                    bean.getCodBonificacion() == 0) {
                flg = true;
                break;
            }
        }

        return flg;

    }

    public BeanEnvPedidoDet obtenerDetalle(String idProducto, int piCodBonificacion) {

        BeanEnvPedidoDet bean = null;

        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);

            if (bean.getIdArticulo().equals(String.valueOf(idProducto))
                    && bean.getCodBonificacion() == piCodBonificacion) {
                return bean;
            }
        }
        return null;
    }

    public boolean pedidoValidado() {
        BeanEnvPedidoDet bean = null;
        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);
            if (bean.getAtendible().equals(Configuracion.FLGREGNOHABILITADO)) {
                return false;
            }
        }
        return true;
    }

    public void eliminarProducto(String psProducto, int piCodBonificacion) {

        BeanEnvPedidoDet bean = null;

        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);

            if (bean.getIdArticulo().equals(psProducto)
                    && bean.getCodBonificacion() == piCodBonificacion) {
                listaDetPedido.remove(i);
                break;
            }
        }
    }

    public void eliminarProductoCodigo(String codProducto) {

        BeanEnvPedidoDet bean = null;

        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);

            if (bean.getCodigoArticulo().equals(codProducto)) {
                listaDetPedido.remove(i);
                break;
            }
        }
    }

    public void actualizarStock(String codProducto, String stock, String atendido) {
        BeanEnvPedidoDet bean = null;
        for (int i = listaDetPedido.size() - 1; i >= 0; i--) {
            bean = (BeanEnvPedidoDet) listaDetPedido.get(i);
            if (bean.getCodigoArticulo().equals(codProducto)) {
                bean.setStock(stock);
                bean.setAtendible(atendido);
                listaDetPedido.set(i, bean);
            }
        }
    }

    public void subLimparBonificacion() {
        List<BeanEnvPedidoDet> loListBeanEnvPedidoDet = new ArrayList<BeanEnvPedidoDet>();
        for (BeanEnvPedidoDet loBeanEnvPedidoDet : listaDetPedido) {
            if (loBeanEnvPedidoDet.getCodBonificacion() == 0) {
                loBeanEnvPedidoDet.setCantBonificacion(0);
                loBeanEnvPedidoDet.setMultBonificacion(0);
                loListBeanEnvPedidoDet.add(loBeanEnvPedidoDet);
            }
        }

        listaDetPedido = loListBeanEnvPedidoDet;
    }

    public void subValidarBonificacion(Context poContext, BeanBonificacion poBeanBonificacion, int piNumDecimales, String stockrestrictivo) {

        List<List<Integer>> laListPosition = new ArrayList<List<Integer>>();
        int liMultiplo = -1;

        for (int i = 0; i < poBeanBonificacion.getLstBonificacionDet().size(); i++) {
            BeanBonificacionDet loBeanBonificacionDet = poBeanBonificacion.getLstBonificacionDet().get(i);

            for (int j = 0; j < listaDetPedido.size(); j++) {
                BeanEnvPedidoDet loBeanEnvPedidoDet = listaDetPedido.get(j);

                if (loBeanEnvPedidoDet.fnValidarBonificacion(loBeanBonificacionDet, tipoArticulo)) {
                    List<Integer> liListPosition = new ArrayList<Integer>();
                    liListPosition.add(j);
                    liListPosition.add(i);
                    laListPosition.add(liListPosition);
                    int liMultiploDet = loBeanEnvPedidoDet.fnMultiploBonificacion(loBeanBonificacionDet, tipoArticulo);
                    if (liMultiplo == -1)
                        liMultiplo = liMultiploDet;
                    else if (liMultiploDet < liMultiplo) {
                        liMultiplo = liMultiploDet;
                    }
                }

            }

            if (laListPosition.size() <= i)
                break;
        }

        //AGREGANDO LAS BONIFICACIONES ENCONTRADAS
        if (laListPosition.size() == poBeanBonificacion.getLstBonificacionDet().size()) {

            DalProducto loDalProducto = new DalProducto(poContext);
            double ldCantidadBonificacion = Double.valueOf(poBeanBonificacion.getCantidad()) * liMultiplo;
            boolean lbValidar = false;
            BeanArticulo loBeanArticulo = null;
            double ldStock = 0;
            int liCodAlmacen = 0;


            if (tipoArticulo.equals(Configuracion.TipoArticulo.PRESENTACION)) {

                BeanPresentacion loBeanPresentacion = loDalProducto.fnSelxIdPresentacion(String.valueOf(poBeanBonificacion.getIdPresentacion()));
                loBeanArticulo = loBeanPresentacion;
                if (codAlmacen.equals("0")) {
                    ldStock = Double.valueOf(loBeanPresentacion.getStock());

                    if (ldStock >= ldCantidadBonificacion || stockrestrictivo.equals(Configuracion.FLGFALSO)) {
                        lbValidar = true;
                    }

                } else {
                    DalAlmacen loDalAlmacen = new DalAlmacen(poContext);
                    BeanAlmacen loBeanAlmacen = loDalAlmacen.fnSelectAlmacenPresentacion(loBeanArticulo.getId(),
                            codAlmacen);

                    if (loBeanAlmacen != null) {
                        liCodAlmacen = loBeanAlmacen.getPk();
                        ldStock = Double.valueOf(loBeanAlmacen.getStock());

                        if (ldStock >= ldCantidadBonificacion || stockrestrictivo.equals(Configuracion.FLGFALSO)) {
                            lbValidar = true;
                        }
                    }
                }

            } else {
                BeanProducto loBeanProducto = loDalProducto.fnSelxIdProducto(String.valueOf(poBeanBonificacion.getIdProducto()));
                loBeanArticulo = loBeanProducto;
                if (codAlmacen.equals("0")) {
                    ldStock = Double.valueOf(loBeanProducto.getStock());

                    if (ldStock >= ldCantidadBonificacion || stockrestrictivo.equals(Configuracion.FLGFALSO)) {
                        lbValidar = true;
                    }

                } else {
                    DalAlmacen loDalAlmacen = new DalAlmacen(poContext);
                    BeanAlmacen loBeanAlmacen = loDalAlmacen.fnSelectAlmacenProducto(loBeanProducto.getId(), codAlmacen);
                    if (loBeanAlmacen != null) {
                        liCodAlmacen = loBeanAlmacen.getPk();
                        ldStock = Double.valueOf(loBeanAlmacen.getStock());

                        if (ldStock >= ldCantidadBonificacion || stockrestrictivo.equals(Configuracion.FLGFALSO)) {
                            lbValidar = true;
                        }
                    }
                }
            }

            if (lbValidar) {


                BeanEnvPedidoDet loBeanEnvPedidoDet = new BeanEnvPedidoDet();

                loBeanEnvPedidoDet.setEmpresa(empresa);
                loBeanEnvPedidoDet.setIdArticulo(loBeanArticulo.getId());
                loBeanEnvPedidoDet.setCodigoArticulo(loBeanArticulo.getCodigo());
                loBeanEnvPedidoDet.setBonificacion("0");
                loBeanEnvPedidoDet.setMonto("0.00");
                loBeanEnvPedidoDet.setPrecioBase("0.00");
                loBeanEnvPedidoDet.setStock(Utilitario.fnRound(ldStock, piNumDecimales));
                loBeanEnvPedidoDet.setDescuento("0.00");
                loBeanEnvPedidoDet.setObservacion("");
                loBeanEnvPedidoDet.setCodPedido(codigoPedido);
                loBeanEnvPedidoDet.setNombreArticulo(loBeanArticulo.getNombre());
                loBeanEnvPedidoDet.setCodListaArticulo("");
                loBeanEnvPedidoDet.setFechaUltVenta("");
                loBeanEnvPedidoDet.setUltPrecio("");
                loBeanEnvPedidoDet.setFlete("0.00");
                loBeanEnvPedidoDet.setCodAlmacen(String.valueOf(liCodAlmacen));
                loBeanEnvPedidoDet.setCodBonificacion(poBeanBonificacion.getId());
                loBeanEnvPedidoDet.setTipoDescuento(listaDetPedido.get(laListPosition.get(0).get(0)).getTipoDescuento());
                loBeanEnvPedidoDet.setMultBonificacion(liMultiplo);
                loBeanEnvPedidoDet.setEditBonificacion(poBeanBonificacion.getEditable().equals(Configuracion.FLGVERDADERO));

                if (tipoArticulo.equals(Configuracion.TipoArticulo.PRESENTACION)) {
                    BeanPresentacion loBeanPresentacion = (BeanPresentacion) loBeanArticulo;
                    loBeanEnvPedidoDet.setCantidadFrac("0");
                    loBeanEnvPedidoDet.setPrecioFrac("0.00");
                    loBeanEnvPedidoDet.setCantidadPre(String.valueOf(loBeanPresentacion.getCantidad()));
                    loBeanEnvPedidoDet.setProducto(loBeanPresentacion.getProducto());
                    //TOMANDO EN CUENTA COMO PAQUETES
                    loBeanEnvPedidoDet.setCantidad(String.valueOf((int) ldCantidadBonificacion));
                    loBeanEnvPedidoDet.setCantidadFrac("0");
                    loBeanEnvPedidoDet.setBonificacionFrac("0");

                } else {
                    loBeanEnvPedidoDet.setCantidad(Utilitario.fnRound(ldCantidadBonificacion, piNumDecimales));
                }

                if (lbValidar) {
                    for (List<Integer> liPostion : laListPosition) {
                        listaDetPedido.get(liPostion.get(0)).subModificarCantBonificacion(poBeanBonificacion
                                .getLstBonificacionDet().get(liPostion.get(1)), liMultiplo, tipoArticulo);
                    }
                    listaDetPedido.add(loBeanEnvPedidoDet);
                }

            }
        }
    }

    private double round(double d) {
        return (Math.floor(d * 100.0d + 0.5d) / 100.0d);
    }

    public String getClipk() {
        return clipk;
    }

    public void setClipk(String clipk) {
        this.clipk = clipk;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getCondicionVenta() {
        return condicionVenta;
    }

    public void setCondicionVenta(String condicionVenta) {
        this.condicionVenta = condicionVenta;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getCodigoMotivo() {
        return codigoMotivo;
    }

    public void setCodigoMotivo(String codigoMotivo) {
        this.codigoMotivo = codigoMotivo;
    }

    public String getMontoTotal() {
        return montoTotal;
    }

    public String getMontoTotalSoles() {
        return montoTotalSoles;
    } //@JBELVY

    public String getMontoTotalDolar() {
        return montoTotalDolar;
    } //@JBELVY

    public String getTipoCambioSoles() {
        return tipoCambioSoles;
    } //@JBELVY

    public String getTipoCambioDolar() {
        return tipoCambioDolar;
    } //@JBELVY

    public void setMontoTotal(String montoTotal) {
        this.montoTotal = montoTotal;
    }

    public void setMontoTotalSoles(String montoTotalSoles) {
        this.montoTotalSoles = montoTotalSoles;
    } //@JBELVY

    public void setMontoTotalDolar(String montoTotalDolar) {
        this.montoTotalDolar = montoTotalDolar;
    } //@JBELVY

    public void setTipoCambioSoles(String tipoCambioSoles) {
        this.tipoCambioSoles = tipoCambioSoles;
    } //@JBELVY

    public void setTipoCambioDolar(String tipoCambioDolar) {
        this.tipoCambioDolar = tipoCambioDolar;
    } //@JBELVY

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public String getCodDireccion() {
        return codDireccion;
    }

    public void setCodDireccion(String codDireccion) {
        this.codDireccion = codDireccion;
    }

    public String getCodDireccionDespacho() {
        return codDireccionDespacho;
    }

    public void setCodDireccionDespacho(String codDireccionDespacho) {
        this.codDireccionDespacho = codDireccionDespacho;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getCelda() {
        return celda;
    }

    public void setCelda(String celda) {
        this.celda = celda;
    }

    public String getErrorConexion() {
        return errorConexion;
    }

    public void setErrorConexion(String errorConexion) {
        this.errorConexion = errorConexion;
    }

    public String getErrorPosicion() {
        return errorPosicion;
    }

    public void setErrorPosicion(String errorPosicion) {
        this.errorPosicion = errorPosicion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getCantidadTotal() {
        return cantidadTotal;
    }

    public void setCantidadTotal(String cantidadTotal) {
        this.cantidadTotal = cantidadTotal;
    }

    public List<BeanEnvPedidoDet> getListaDetPedido() {
        return listaDetPedido;
    }

    public void setListaDetPedido(List<BeanEnvPedidoDet> listaDetPedido) {
        this.listaDetPedido = listaDetPedido;
    }

    public String getCodigoPedido() {
        return codigoPedido;
    }

    public void setCodigoPedido(String codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public String getFlgEnCobertura() {
        return flgEnCobertura;
    }

    public void setFlgEnCobertura(String flgEnCobertura) {
        this.flgEnCobertura = flgEnCobertura;
    }

    public String getFlgTerminado() {
        return flgTerminado;
    }

    public void setFlgTerminado(String flgTerminado) {
        this.flgTerminado = flgTerminado;
    }

    public String getCodigoPedidoServidor() {
        return codigoPedidoServidor;
    }

    public void setCodigoPedidoServidor(String codigoPedidoServidor) {
        this.codigoPedidoServidor = codigoPedidoServidor;
    }

    public String getFlgEliminar() {
        return flgEliminar;
    }

    public void setFlgEliminar(String flgEliminar) {
        this.flgEliminar = flgEliminar;
    }

    public String getBonificacionTotal() {
        return bonificacionTotal;
    }

    public void setBonificacionTotal(String bonificacionTotal) {
        this.bonificacionTotal = bonificacionTotal;
    }

    public void EliminarListaDetallePedido() {
        if (listaDetPedido != null) {
            listaDetPedido.clear();
        }
    }

    public String getFlgTipo() {
        return flgTipo;
    }

    public void setFlgTipo(String flgTipo) {
        this.flgTipo = flgTipo;
    }

    public String getFlgNuevoDireccion() {
        return flgNuevoDireccion;
    }

    public void setFlgNuevoDireccion(String flgNuevoDireccion) {
        this.flgNuevoDireccion = flgNuevoDireccion;
    }

    public String getFleteTotal() {
        return fleteTotal;
    }

    public void setFleteTotal(String fleteTotal) {
        this.fleteTotal = fleteTotal;
    }

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }

    public List<BeanEnvPedidoCtrl> getListaCtrlPedido() {
        if (listaCtrlPedido == null)
            return new ArrayList<BeanEnvPedidoCtrl>();
        else
            return listaCtrlPedido;
    }

    public void setListaCtrlPedido(List<BeanEnvPedidoCtrl> listaCtrlPedido) {
        this.listaCtrlPedido = listaCtrlPedido;
    }

    public void setListaControl(List<BeanEnvPedidoCtrl> controles) {
        if (listaCtrlPedido == null || listaCtrlPedido.size() == 0) {
            listaCtrlPedido = controles;
        } else {
            for (int i = 0; i < controles.size(); i++) {
                fnEncontrarControl(controles.get(i));
            }
        }
    }

    private void fnEncontrarControl(BeanEnvPedidoCtrl beanControl) {
        boolean encontrado = true;
        for (int i = 0; i < listaCtrlPedido.size(); i++) {
            BeanEnvPedidoCtrl currentControl = listaCtrlPedido.get(i);
            if (currentControl.getIdControl().equals(beanControl.getIdControl())) {
                listaCtrlPedido.set(i, beanControl);
                encontrado = true;
                break;
            } else
                encontrado = false;
        }
        if (!encontrado) {
            listaCtrlPedido.add(beanControl);
        }
    }

    public String getCodigoDireccion() {
        return codigoDireccion;
    }

    public void setCodigoDireccion(String codigoDireccion) {
        this.codigoDireccion = codigoDireccion;
    }

    public String getCodigoDireccionDespacho() {
        return codigoDireccionDespacho;
    }

    public void setCodigoDireccionDespacho(String codigoDireccionDespacho) {
        this.codigoDireccionDespacho = codigoDireccionDespacho;
    }

    public String getCreditoUtilizado() {
        return creditoUtilizado;
    }

    public void setCreditoUtilizado(String creditoUtilizado) {
        this.creditoUtilizado = creditoUtilizado;
    }

    public String getNombreCliente(Context context) {
        BeanCliente bean = new DalCliente(context).fnSelxCodCliente(this.codigoCliente);
        return bean.getClinom();
    }

    public boolean isEnEdicion() {
        return enEdicion;
    }

    public void setEnEdicion(boolean enEdicion) {
        this.enEdicion = enEdicion;
    }

    public boolean isProcesado() {
        return procesado;
    }

    public void setProcesado(boolean procesado) {
        this.procesado = procesado;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }
}