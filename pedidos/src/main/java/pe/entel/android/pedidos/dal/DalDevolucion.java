package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pe.com.nextel.android.dal.DALGestor;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanAlmacen;
import pe.entel.android.pedidos.bean.BeanEnvDevolucionCab;
import pe.entel.android.pedidos.bean.BeanEnvItemDet;
import pe.entel.android.pedidos.bean.BeanPresentacion;
import pe.entel.android.pedidos.bean.BeanProducto;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.util.Configuracion;

public class DalDevolucion extends DALGestor {
    private Context ioContext;

    public DalDevolucion(Context psClase) throws Exception {
        super(psClase, psClase.getString(R.string.db_name));
        ioContext = psClase;
    }

    public String fnGrabarDevolucion(BeanEnvDevolucionCab psBean) {
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(ioContext.getString(R.string.db_name), Context.MODE_PRIVATE, null);

        String strId = String.valueOf(System.currentTimeMillis());

        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("INSERT INTO TrCabDevolucion (")
                .append("CodigoDevolucion,idCuenta,idCliente,idUsuario,Latitud,Longitud,Celda," +
                        "fechaMovil,ErrorConexion,ErrorPosicion,FlgEnCobertura,FlgEnviado, TipoArticulo")
                .append(") VALUES ('");
        loStbCab.append(strId);
        loStbCab.append("','");
        loStbCab.append(psBean.getIdCuenta());
        loStbCab.append("','");
        loStbCab.append(psBean.getIdCliente());
        loStbCab.append("','");
        loStbCab.append(psBean.getIdUsuario());
        loStbCab.append("','");
        loStbCab.append(psBean.getLatitud());
        loStbCab.append("','");
        loStbCab.append(psBean.getLongitud());
        loStbCab.append("','");
        loStbCab.append(psBean.getCelda());
        loStbCab.append("','");
        loStbCab.append(psBean.getFechaMovil());
        loStbCab.append("','");
        loStbCab.append(psBean.getErrorConexion());
        loStbCab.append("','");
        loStbCab.append(psBean.getErrorPosicion());
        loStbCab.append("','");
        loStbCab.append(psBean.getFlgEnCobertura());
        loStbCab.append("','");
        loStbCab.append(Configuracion.FLGREGNOHABILITADO);
        loStbCab.append("','");
        loStbCab.append(psBean.getTipoArticulo());
        loStbCab.append("')");

        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            return "";
        }

        StringBuilder loStbDet;

        Iterator<BeanEnvItemDet> loIterator = psBean.getLstDevolucionDet().iterator();
        Log.v("XXX", "NUMERO DETALLES:: " + psBean.getLstDevolucionDet().size());

        BeanEnvItemDet loBeanDevolucionDet = null;

        while (loIterator.hasNext()) {
            loBeanDevolucionDet = loIterator.next();
            loStbDet = new StringBuilder();

            loStbDet.append("INSERT INTO TrDetDevolucion (")
                    .append("CodigoDevolucion,CodigoArticulo,Cantidad,CodigoMotivo,FechaVencimiento, " +
                            "Observacion, CodAlmacen, CantidadPre,  CantidadFrac, TipoArticulo")
                    .append(") VALUES ('");

            loStbDet.append(strId);
            loStbDet.append("','");
            loStbDet.append(loBeanDevolucionDet.getCodigoArticulo());
            loStbDet.append("','");
            loStbDet.append(loBeanDevolucionDet.getCantidad());
            loStbDet.append("','");
            loStbDet.append(loBeanDevolucionDet.getCodigoMotivo());
            loStbDet.append("','");
            loStbDet.append(loBeanDevolucionDet.getFechaVencimiento());
            loStbDet.append("','");
            loStbDet.append(loBeanDevolucionDet.getObservacion());
            loStbDet.append("','");
            loStbDet.append(loBeanDevolucionDet.getCodAlmacen());
            loStbDet.append("','");
            loStbDet.append(loBeanDevolucionDet.getCantidadPre());
            loStbDet.append("','");
            loStbDet.append(loBeanDevolucionDet.getCantidadFrac());
            loStbDet.append("','");
            loStbDet.append(loBeanDevolucionDet.getTipoArticulo());
            loStbDet.append("')");

            try {
                myDB.execSQL(loStbDet.toString());
                Log.v("XXX", "INSERTO DETALLE!!!");
            } catch (Exception e) {
                Log.v("XXX", "FALLO EL DETALLE!!!");
                return "";
            }

        }
        myDB.close();
        return strId;
    }

    public boolean fnUpdEstadoDevolucion(String codcan) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(ioContext.getString(R.string.db_name), Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrCabDevolucion SET FlgEnviado='" + Configuracion.FLGREGHABILITADO + "' WHERE CodigoDevolucion = " + codcan);
        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    /**
     * Devuelve la cantidad de devoluciones pendientes de envio
     *
     * @param codVendedor codigo del vendedor
     * @return el numero de pedidos pendientes
     */
    public int fnNumDevolucionesPendientes(String codVendedor) throws Exception {
        StringBuilder loSbSqlCab = new StringBuilder();
        loSbSqlCab.append("SELECT count(*) ")
                .append("FROM TrCabDevolucion ")
                .append("WHERE FlgEnviado= ").append(fnValueQuery(Configuracion.FLGREGNOHABILITADO))
                .append(" AND idUsuario = ").append(fnValueQuery(codVendedor));


        getSQLQuery(loSbSqlCab.toString(), new RowListener() {

            @Override
            public void setRow(long piPosition, Cursor poCursor) {
                setObject(fnInteger(poCursor.getInt(0)));
            }
        });

        return (Integer) getObject();
    }

    public List<BeanEnvDevolucionCab> fnEnvioDevolucionesPendientes(String codVendedor) {
        List<BeanEnvDevolucionCab> loDevolucionesPendientes = new ArrayList<BeanEnvDevolucionCab>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(ioContext.getString(R.string.db_name), Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]
                {"CodigoDevolucion", "idCuenta", "idCliente", "idUsuario", "Latitud", "Longitud", "Celda",
                        "fechaMovil", "ErrorConexion", "ErrorPosicion", "FlgEnCobertura", "TipoArticulo"};
        String lsTablas = "TrCabDevolucion";
        String lsWhere = "FlgEnviado='" + Configuracion.FLGREGNOHABILITADO + "' and idUsuario = '" + codVendedor + "'";

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null, null);

        BeanEnvDevolucionCab loBeanDevolucionCab = null;
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanDevolucionCab = new BeanEnvDevolucionCab();
                    loBeanDevolucionCab.setDevopk(loCursor.getString(0));
                    loBeanDevolucionCab.setIdCuenta(loCursor.getString(1));
                    loBeanDevolucionCab.setIdCliente(loCursor.getString(2));
                    loBeanDevolucionCab.setIdUsuario(loCursor.getString(3));
                    loBeanDevolucionCab.setLatitud(loCursor.getString(4));
                    loBeanDevolucionCab.setLongitud(loCursor.getString(5));
                    loBeanDevolucionCab.setCelda(loCursor.getString(6));
                    loBeanDevolucionCab.setFechaMovil(loCursor.getString(7));
                    loBeanDevolucionCab.setErrorConexion(Integer.parseInt(loCursor.getString(8)));
                    loBeanDevolucionCab.setErrorPosicion(Integer.parseInt(loCursor.getString(9)));
                    loBeanDevolucionCab.setFlgEnCobertura(loCursor.getString(10));
                    loBeanDevolucionCab.setTipoArticulo(loCursor.getString(11));

                    loBeanDevolucionCab.setLstDevolucionDet(subDetallePendientes((loBeanDevolucionCab.getDevopk())));

                    loDevolucionesPendientes.add(loBeanDevolucionCab);
                    loBeanDevolucionCab = null;

                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loDevolucionesPendientes;
    }

    private List<BeanEnvItemDet> subDetallePendientes(String codPed) {
        List<BeanEnvItemDet> loDetallesPendientes = new ArrayList<BeanEnvItemDet>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(ioContext.getString(R.string.db_name), Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"CodigoArticulo", "Cantidad", "CodigoMotivo", "FechaVencimiento",
                "Observacion", "CodAlmacen", "CantidadPre", "CantidadFrac", "TipoArticulo"};
        String lsTablas = "TrDetDevolucion";
        String lsWhere = "CodigoDevolucion=" + codPed;

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null, null);
        BeanEnvItemDet loBeanDevoDet = null;
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanDevoDet = new BeanEnvItemDet();
                    loBeanDevoDet.setCodigoArticulo(loCursor.getString(0));
                    loBeanDevoDet.setCantidad(loCursor.getString(1));
                    loBeanDevoDet.setCodigoMotivo(loCursor.getString(2));
                    loBeanDevoDet.setFechaVencimiento(loCursor.getString(3));
                    loBeanDevoDet.setObservacion(loCursor.getString(4));
                    loBeanDevoDet.setCodAlmacen(loCursor.getString(5));
                    loBeanDevoDet.setCantidadPre(loCursor.getString(6));
                    loBeanDevoDet.setCantidadFrac(loCursor.getString(7));
                    loBeanDevoDet.setTipoArticulo(loCursor.getString(8));
                    loDetallesPendientes.add(loBeanDevoDet);
                    loBeanDevoDet = null;

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return loDetallesPendientes;
    }

    public boolean fnUpdEstadoDevolucionPendiente(BeanUsuario piUsuario) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(ioContext.getString(R.string.db_name), Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrCabDevolucion SET FlgEnviado='" + Configuracion.FLGREGHABILITADO + "'" +
                "WHERE FlgEnviado='" + Configuracion.FLGREGNOHABILITADO + "' and idUsuario = '" + piUsuario.getCodVendedor() + "'");
        Log.v("XXX", "actualizando Pend devolucion..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());

        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnDelTrDetDevo() {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(ioContext.getString(R.string.db_name), Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("DELETE from TrDetDevolucion");
        Log.v("XXX", "eliminando detalle devolucion..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnDelTrCabDevolucion() {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(ioContext.getString(R.string.db_name), Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("DELETE from TrCabDevolucion");
        Log.v("XXX", "eliminando cabecera devolucion..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public void fnValidarStock(List<BeanEnvItemDet> poLstBeanEnvItemDet) {

        for (BeanEnvItemDet loBeanEnvItemDet : poLstBeanEnvItemDet) {

            long llCodAlmacen = Long.valueOf(loBeanEnvItemDet.getCodAlmacen());

            double ldCantidad = Double.parseDouble(loBeanEnvItemDet.getCantidad());
            double ldStock = 0;

            if (llCodAlmacen > 0) {

                DalAlmacen loDalAlmacen = new DalAlmacen(ioContext);

                BeanAlmacen loBeanAlmacen = null;


                if (loBeanEnvItemDet.getTipoArticulo().equals(Configuracion.TipoArticulo.PRESENTACION)) {

                    double ldCantPre = Double.parseDouble(loBeanEnvItemDet.getCantidadPre());
                    double ldCantFrac = Double.parseDouble(loBeanEnvItemDet.getCantidadFrac());

                    ldStock = (ldCantPre * ldCantidad) + ldCantFrac;

                    loBeanAlmacen = loDalAlmacen.fnSelectXCodPresentacionXCodAlmacen(loBeanEnvItemDet.getIdArticulo(), loBeanEnvItemDet.getCodAlmacen());

                    if (loBeanAlmacen != null) {

                        double ldStockAnt = Double.parseDouble(loBeanAlmacen.getStock());
                        ldStock += ldStockAnt;

                        loDalAlmacen.fnUpdPreStock(loBeanEnvItemDet.getCodAlmacen(), loBeanEnvItemDet.getIdArticulo(), String.valueOf(ldStock));
                    } else {
                        loDalAlmacen.fnGrabarAlmacenPresentacion(loBeanEnvItemDet.getCodAlmacen(), loBeanEnvItemDet.getIdArticulo(), String.valueOf(ldStock));
                    }

                } else {

                    ldStock = ldCantidad;
                    loBeanAlmacen = loDalAlmacen.fnSelectXCodProductoXCodAlmacen(loBeanEnvItemDet.getCodAlmacen(), loBeanEnvItemDet.getIdArticulo());

                    if (loBeanAlmacen != null) {
                        double ldStockAnt = Double.parseDouble(loBeanAlmacen.getStock());
                        ldStock += ldStockAnt;
                        loDalAlmacen.fnUpdStock(loBeanEnvItemDet.getCodAlmacen(), loBeanEnvItemDet.getIdArticulo(), String.valueOf(ldStock));
                    } else {
                        loDalAlmacen.fnGrabarAlmacenProducto(loBeanEnvItemDet.getCodAlmacen(), loBeanEnvItemDet.getIdArticulo(), String.valueOf(ldStock));
                    }

                }

            } else {
                DalProducto loDalProducto = new DalProducto(ioContext);
                if (loBeanEnvItemDet.getTipoArticulo().equals(Configuracion.TipoArticulo.PRESENTACION)) {
                    BeanPresentacion loBeanPresentacion = loDalProducto.fnSelxIdPresentacion(loBeanEnvItemDet.getIdArticulo());
                    double ldCantPre = Double.parseDouble(loBeanEnvItemDet.getCantidadPre());
                    double ldCantFrac = Double.parseDouble(loBeanEnvItemDet.getCantidadFrac());
                    double ldStockAnt = Double.parseDouble(loBeanPresentacion.getStock());

                    ldStock = ldStockAnt + (ldCantPre * ldCantidad) + ldCantFrac;

                    loDalProducto.fnUpdStockPresentacion(loBeanEnvItemDet.getIdArticulo(), String.valueOf(ldStock));

                } else {
                    BeanProducto loBeanProducto = loDalProducto.fnSelxIdProducto(loBeanEnvItemDet.getIdArticulo());
                    double ldStockAnt = Double.parseDouble(loBeanProducto.getStock());

                    ldStock = ldStockAnt + ldCantidad;

                    loDalProducto.fnUpdStockProducto(loBeanEnvItemDet.getIdArticulo(), String.valueOf(ldStock));
                }
            }

        }

    }
}