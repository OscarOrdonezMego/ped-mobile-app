package pe.entel.android.pedidos.http;

import android.content.Context;
import android.util.Log;

import org.codehaus.jackson.type.TypeReference;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanConsultaPedido;
import pe.entel.android.pedidos.bean.BeanRespConsultaPedido;
import pe.entel.android.pedidos.bean.BeanRespuesta;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumUrl;

public class HttpConsultaPedido extends HttpConexion {

    private BeanConsultaPedido ioBeanConsultaPedido;
    private Context ioContext;

    public HttpConsultaPedido(BeanConsultaPedido poBeanConsultaPedido, Context poContext, EnumTipActividad poEnumTipActividad) {
        super(poContext, poContext
                .getString(R.string.httpconsultapedido_dlgbuscqueda), poEnumTipActividad);
        ioBeanConsultaPedido = poBeanConsultaPedido;
        ioContext = poContext;
    }

    public HttpConsultaPedido(BeanConsultaPedido poBeanConsultaPedido, Context poContext, EnumTipActividad poEnumTipActividad, String mensaje) {
        super(poContext, mensaje, poEnumTipActividad);
        ioBeanConsultaPedido = poBeanConsultaPedido;
        ioContext = poContext;
    }

    public void subConHttp() {
        try {
            //RestTemplate restTemplate = new RestTemplate();
            //restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            Log.v("URL", Configuracion.fnUrl(ioContext, EnumUrl.CONSULTAPEDIDO));
            Log.v("REQUEST", BeanMapper.toJson(ioBeanConsultaPedido, false));

            //String lsObjeto = restTemplate.postForObject(Configuracion.fnUrl(ioContext, EnumUrl.CONSULTAPEDIDO), ioBeanConsultaPedido, String.class);
            String lsObjeto = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, EnumUrl.CONSULTAPEDIDO), ioBeanConsultaPedido);
            Log.v("lsObjeto", lsObjeto);

            TypeReference type = new TypeReference<BeanRespuesta<BeanRespConsultaPedido>>() {
            };
            BeanRespuesta<BeanRespConsultaPedido> loBeanResponse = (BeanRespuesta<BeanRespConsultaPedido>) BeanMapper.fromJson(lsObjeto, type);

            setHttpResponseObject(loBeanResponse);
        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, "Error HTTP: " + e.getMessage());
        }
    }

    @Override
    public boolean OnPreConnect() {
        if (!Utilitario.fnVerSignal(ioContext)) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            BeanRespuesta<BeanRespConsultaPedido> loBeanResponse = (BeanRespuesta) getHttpResponseObject();
            int liIdHttprespuesta = loBeanResponse.getCodigo();
            String lsHttpRespuesta = loBeanResponse.getMensaje();

            if (ConfiguracionNextel.EnumServerResponse.get(liIdHttprespuesta) == ConfiguracionNextel.EnumServerResponse.OK
                    || ConfiguracionNextel.EnumServerResponse.get(liIdHttprespuesta) == ConfiguracionNextel.EnumServerResponse.OKNOMSG) {
                ((AplicacionEntel) ioContext.getApplicationContext()).setCurrentRespConsultaPedido(loBeanResponse.getGeneral());
            } else {
                ((AplicacionEntel) ioContext.getApplicationContext()).setCurrentRespConsultaPedido(new BeanRespConsultaPedido());
            }
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.get(liIdHttprespuesta), lsHttpRespuesta);
        }
    }
}