package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import pe.com.nextel.android.dal.DALGestor;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanEnvCanjeCab;
import pe.entel.android.pedidos.bean.BeanEnvItemDet;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.util.Configuracion;

public class DalCanje extends DALGestor {
    private Context ioContext;

    public DalCanje(Context psClase) throws Exception {
        super(psClase, psClase.getString(R.string.db_name));
        ioContext = psClase;
    }

    public String fnGrabarCanje(BeanEnvCanjeCab psBean) {
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String strId = String.valueOf(System.currentTimeMillis());

        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("INSERT INTO TrCabCanje (")
                .append("CodigoCanje,idCuenta,idCliente,idUsuario,numeroDocumento,Latitud,Longitud," +
                        "Celda,fechaMovil,ErrorConexion,ErrorPosicion,FlgEnCobertura,FlgEnviado, TipoArticulo")
                .append(") VALUES ('");
        loStbCab.append(strId);
        loStbCab.append("','");
        loStbCab.append(psBean.getIdCuenta());
        loStbCab.append("','");
        loStbCab.append(psBean.getIdCliente());
        loStbCab.append("','");
        loStbCab.append(psBean.getIdUsuario());
        loStbCab.append("','");
        loStbCab.append(psBean.getNumeroDocumento());
        loStbCab.append("','");
        loStbCab.append(psBean.getLatitud());
        loStbCab.append("','");
        loStbCab.append(psBean.getLongitud());
        loStbCab.append("','");
        loStbCab.append(psBean.getCelda());
        loStbCab.append("','");
        loStbCab.append(psBean.getFechaMovil());
        loStbCab.append("','");
        loStbCab.append(psBean.getErrorConexion());
        loStbCab.append("','");
        loStbCab.append(psBean.getErrorPosicion());
        loStbCab.append("','");
        loStbCab.append(psBean.getFlgEnCobertura());
        loStbCab.append("','");
        loStbCab.append(Configuracion.FLGREGNOHABILITADO);
        loStbCab.append("','");
        loStbCab.append(psBean.getTipoArticulo());
        loStbCab.append("')");
        ;

        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            return "";
        }

        StringBuilder loStbDet;

        Iterator<BeanEnvItemDet> loIterator = psBean.getLstCanjeDet()
                .iterator();
        Log.v("XXX", "NUMERO DETALLES:: " + psBean.getLstCanjeDet().size());

        BeanEnvItemDet loBeanCanjeDet = null;

        while (loIterator.hasNext()) {
            loBeanCanjeDet = loIterator.next();
            loStbDet = new StringBuilder();

            loStbDet.append("INSERT INTO TrDetCanje (")
                    .append("CodigoCanje,CodigoArticulo,Cantidad,CodigoMotivo,FechaVencimiento," +
                            "Observacion, CodAlmacen, CantidadPre,  CantidadFrac, TipoArticulo")
                    .append(") VALUES ('");

            loStbDet.append(strId);
            loStbDet.append("','");
            loStbDet.append(loBeanCanjeDet.getCodigoArticulo());
            loStbDet.append("','");
            loStbDet.append(loBeanCanjeDet.getCantidad());
            loStbDet.append("','");
            loStbDet.append(loBeanCanjeDet.getCodigoMotivo());
            loStbDet.append("','");
            loStbDet.append(loBeanCanjeDet.getFechaVencimiento());
            loStbDet.append("','");
            loStbDet.append(loBeanCanjeDet.getObservacion());
            loStbDet.append("','");
            loStbDet.append(loBeanCanjeDet.getCodAlmacen());
            loStbDet.append("','");
            loStbDet.append(loBeanCanjeDet.getCantidadPre());
            loStbDet.append("','");
            loStbDet.append(loBeanCanjeDet.getCantidadFrac());
            loStbDet.append("','");
            loStbDet.append(loBeanCanjeDet.getTipoArticulo());
            loStbDet.append("')");

            try {
                myDB.execSQL(loStbDet.toString());
                Log.v("XXX", "INSERTO DETALLE!!!");
            } catch (Exception e) {
                Log.v("XXX", "FALLO EL DETALLE!!!");
                return "";
            }

        }
        myDB.close();
        return strId;
    }

    public boolean fnUpdEstadoCanje(String codcan) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrCabCanje SET FlgEnviado='"
                + Configuracion.FLGREGHABILITADO + "' WHERE CodigoCanje = "
                + codcan);
        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            Log.e("Error: ", "fnUpdEstadoCanje", e);
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    /**
     * Devuelve la cantidad de canjes pendientes de envio
     *
     * @param codVendedor codigo del vendedor
     * @return el numero de canjes pendientes
     */
    public int fnNumCanjesPendientes(String codVendedor) throws Exception {
        StringBuilder loSbSqlCab = new StringBuilder();
        loSbSqlCab.append("SELECT count(*) ").append("FROM TrCabCanje ")
                .append("WHERE FlgEnviado= ")
                .append(fnValueQuery(Configuracion.FLGREGNOHABILITADO))
                .append(" AND idUsuario = ").append(fnValueQuery(codVendedor));

        getSQLQuery(loSbSqlCab.toString(), new RowListener() {

            @Override
            public void setRow(long piPosition, Cursor poCursor) {
                setObject(fnInteger(poCursor.getInt(0)));
            }
        });

        return (Integer) getObject();
    }

    public List<BeanEnvCanjeCab> fnEnvioCanjesPendientes(String codVendedor) {
        List<BeanEnvCanjeCab> loCanjesPendientes = new ArrayList<BeanEnvCanjeCab>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"CodigoCanje", "idCuenta",
                "idCliente", "idUsuario", "numeroDocumento", "Latitud",
                "Longitud", "Celda", "fechaMovil", "ErrorConexion",
                "ErrorPosicion", "FlgEnCobertura", "TipoArticulo"};
        String lsTablas = "TrCabCanje";
        String lsWhere = "FlgEnviado='" + Configuracion.FLGREGNOHABILITADO
                + "' and idUsuario = '" + codVendedor + "'";

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        BeanEnvCanjeCab loBeanEnvCanjeCab = null;
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanEnvCanjeCab = new BeanEnvCanjeCab();
                    loBeanEnvCanjeCab.setCanpk(loCursor.getString(0));
                    loBeanEnvCanjeCab.setIdCuenta(loCursor.getString(1));
                    loBeanEnvCanjeCab.setIdCliente(loCursor.getString(2));
                    loBeanEnvCanjeCab.setIdUsuario(loCursor.getString(3));
                    loBeanEnvCanjeCab.setNumeroDocumento(loCursor.getString(4));
                    loBeanEnvCanjeCab.setLatitud(loCursor.getString(5));
                    loBeanEnvCanjeCab.setLongitud(loCursor.getString(6));
                    loBeanEnvCanjeCab.setCelda(loCursor.getString(7));
                    loBeanEnvCanjeCab.setFechaMovil(loCursor.getString(8));
                    loBeanEnvCanjeCab.setErrorConexion(Integer.parseInt(loCursor
                            .getString(9)));
                    loBeanEnvCanjeCab.setErrorPosicion(Integer.parseInt(loCursor
                            .getString(10)));

                    loBeanEnvCanjeCab.setFlgEnCobertura(loCursor.getString(11));

                    loBeanEnvCanjeCab.setTipoArticulo(loCursor.getString(12));
                    loBeanEnvCanjeCab
                            .setLstCanjeDet(subDetallePendientes((loBeanEnvCanjeCab
                                    .getCanpk())));

                    loCanjesPendientes.add(loBeanEnvCanjeCab);
                    loBeanEnvCanjeCab = null;

                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loCanjesPendientes;
    }

    private List<BeanEnvItemDet> subDetallePendientes(String codPed) {
        List<BeanEnvItemDet> loDetallesPendientes = new ArrayList<BeanEnvItemDet>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"CodigoArticulo", "Cantidad",
                "CodigoMotivo", "FechaVencimiento", "Observacion", "CodAlmacen",
                "CantidadPre", "CantidadFrac", "TipoArticulo"};
        String lsTablas = "TrDetCanje";
        String lsWhere = "CodigoCanje=" + codPed;

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);
        BeanEnvItemDet loBeanCanjeDet = null;
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanCanjeDet = new BeanEnvItemDet();
                    loBeanCanjeDet.setCodigoArticulo(loCursor.getString(0));
                    loBeanCanjeDet.setCantidad(loCursor.getString(1));
                    loBeanCanjeDet.setCodigoMotivo(loCursor.getString(2));
                    loBeanCanjeDet.setFechaVencimiento(loCursor.getString(3));
                    loBeanCanjeDet.setObservacion(loCursor.getString(4));
                    loBeanCanjeDet.setCodAlmacen(loCursor.getString(5));
                    loBeanCanjeDet.setCantidadPre(loCursor.getString(6));
                    loBeanCanjeDet.setCantidadFrac(loCursor.getString(7));
                    loBeanCanjeDet.setTipoArticulo(loCursor.getString(8));
                    loDetallesPendientes.add(loBeanCanjeDet);
                    loBeanCanjeDet = null;

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return loDetallesPendientes;
    }

    public boolean fnUpdEstadoCanjePendiente(BeanUsuario piUsuario) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrCabCanje SET FlgEnviado='"
                + Configuracion.FLGREGHABILITADO + "'" + "WHERE FlgEnviado='"
                + Configuracion.FLGREGNOHABILITADO + "' and idUsuario = '"
                + piUsuario.getCodVendedor() + "'");
        Log.v("XXX", "actualizando Pend canje..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());

        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnDelTrDetCanje() {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("DELETE from TrDetCanje");
        Log.v("XXX", "eliminando detalle canje..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnDelTrCabCanje() {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("DELETE from TrCabCanje");
        Log.v("XXX", "eliminando cabecera canje..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }
}