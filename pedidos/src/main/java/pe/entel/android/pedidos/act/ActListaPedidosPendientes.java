package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.util.Logger;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapListaPedidoPendiente;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanEnvPedidoDet;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.UtilitarioData;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.MenuLateralAdapter;
import pe.entel.android.pedidos.util.pedidos.MenuLateralPendientesListener;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

/**
 * Created by tkongr on 18/03/2016.
 */
public class ActListaPedidosPendientes extends NexActivity implements AdapterView.OnItemClickListener {

    ListView lista;
    List<BeanEnvPedidoCab> ioLista;
    BeanUsuario loBeanUsuario = null;
    Bundle savedInstanceState;
    private AdapMenuPrincipalSliding menuAdapter;
    private DALNServices ioDALNservices;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Logger.d("XXX", "ActPedidoListaOnline");
        ValidacionServicioUtil.validarServicio(this);
        this.savedInstanceState = savedInstanceState;
        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);

        setUsingNexMenuDrawerLayout(new MenuLateralAdapter(this, menuAdapter, new MenuLateralPendientesListener(this, loBeanUsuario, ioDALNservices)));

        super.onCreate(savedInstanceState);
        UtilitarioPedidos.IndicarMensajeOkError(this);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(ActListaPedidosPendientes.this, ActLogin.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        };

        UtilitarioPedidos.obtenerMenuLateral(this, loBeanUsuario, listener);
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setTitle(getString(R.string.actpedidospendientes_bartitulo));
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, ActListaPendientes.class));
        finish();
    }

    @Override
    protected void subSetControles() {
        super.subSetControles();
        try {
            setActionBarContentView(R.layout.listapedidopendiente);
            lista = (ListView) findViewById(R.id.actlistapedidopendientes_lista);
            lista.setOnItemClickListener(this);
            registerForContextMenu(lista);
            subLLenarLista();
        } catch (Exception e) {
            subShowException(e);
        }
    }

    public void subLLenarLista() {
        try {
            ioLista = new DalPedido(this).fnEnvioPedidosPendientes(loBeanUsuario.getCodVendedor());
            lista.setAdapter(new AdapListaPedidoPendiente(this, R.layout.listapedidopendiente_item, ioLista));
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        for (BeanEnvPedidoDet detalle : ioLista.get(position).getListaDetPedido()) {
            detalle.obtenerProducto(ActListaPedidosPendientes.this, ioLista.get(position).getTipoArticulo());
        }

        String json = "";
        try {
            json = BeanMapper.toJson(ioLista.get(position), false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        UtilitarioData.fnCrearPreferencesPutString(this, "MensajeVerificarPedido", "");//
        UtilitarioData.fnCrearPreferencesPutString(this, "beanPedidoCabVerificarPedido", "");//
        UtilitarioData.fnCrearPreferencesPutBoolean(this, "pedidoPendiente", true);
        UtilitarioData.fnCrearPreferencesPutString(this, "beanPedidoCabPendiente", json);
        UtilitarioData.fnCrearPreferencesPutBoolean(this, "editoPedido", true);
        startActivity(new Intent(ActListaPedidosPendientes.this, ActProductoPedido.class));
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case Constantes.CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            new HttpSincronizar(loBeanUsuario.getCodVendedor(), ActListaPedidosPendientes.this, fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;

            default:
                return super.onCreateDialog(id);
        }

    }

}
