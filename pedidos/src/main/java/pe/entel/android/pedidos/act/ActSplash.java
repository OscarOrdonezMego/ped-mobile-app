package pe.entel.android.pedidos.act;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import pe.com.nextel.android.actividad.NexSplashActivity;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.verification.util.Service;

public class ActSplash extends NexSplashActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());
        //CustomActivityOnCrash.install(this);
        //CustomActivityOnCrash.setErrorActivityClass(ErrorActivity.class);
        if (tienePermisos(this)) {
            //CustomActivityOnCrash.setDefaultErrorActivityDrawable(R.drawable.entel_logo);
            Fabric.with(this, new Crashlytics());
            //Service.startServiceSplash(this, Configuracion.fnUrl(this, Configuracion.EnumUrl.VERIFICAR));
        }
    }

    @Override
    protected Class<?> getNextActivity() {
        return ActLogin.class;
    }

    @Override
    protected String fnGetPackageName() {
        return getPackageName();
    }

    @Override
    protected int fnGetPreferenceResource() {
        // TODO Auto-generated method stub
        return R.xml.preferencias;
    }

    public boolean tienePermisos(Context context) {

        String[] permisos = retrievePermissions(this);

        for (int i = 0; i < permisos.length; i++) {
            //Log.d("XXX", "ActSplash - tienePermisos : " + permisos[i]);
            //Log.d("XXX", "permisos[i].indexOf(\"google\") " + permisos[i].indexOf("google"));
            if (!permisos[i].contains("google")) {
                if (ContextCompat.checkSelfPermission(context, permisos[i])
                        != PackageManager.PERMISSION_GRANTED) {
                    Log.d("XXX", "ActSplash - solicitarPermisos : " + permisos[i]);
                    solicitarPermisos(new String[]{permisos[i]}, i);
                    return false;
                }
            }
        }

        return true;
    }

    public void solicitarPermisos(String[] permiso, int id) {
        ActivityCompat.requestPermissions(ActSplash.this, permiso, id);
        finish();
        System.exit(0);
    }

    public static String[] retrievePermissions(Context context) {
        try {
            return context
                    .getPackageManager()
                    .getPackageInfo(context.getPackageName(), PackageManager.GET_PERMISSIONS)
                    .requestedPermissions;
        } catch (Exception e) {
            throw new RuntimeException("This should have never happened.", e);
        }
    }
}
