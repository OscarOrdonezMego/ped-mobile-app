package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanEnvPedidoDet;
import pe.entel.android.pedidos.util.Configuracion;

public class AdapPedidoDetalleOnline extends ArrayAdapter<BeanEnvPedidoDet> {
    int resource;
    Context context;
    TextView rowTextView;
    String flgNumDecVista;
    String flgMoneda, flgMonedaDolar;
    String mostrarPresentacion;
    String isTipoArticulo;
    String maximoNumeroDecimales;
    int iiMaximoNumeroDecimales;
    String numeroDecimales;

    LinkedList<BeanEnvPedidoDet> originalList;

    public AdapPedidoDetalleOnline(Context _context, int _resource,
                                   LinkedList<BeanEnvPedidoDet> lista) {
        super(_context, _resource, lista);
        originalList = new LinkedList<BeanEnvPedidoDet>();
        originalList.addAll(lista);
        resource = _resource;
        context = _context;

        flgNumDecVista = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(context);
        flgMoneda = Configuracion.EnumConfiguracion.SIMBOLO_MONEDA.getValor(context);
        flgMonedaDolar = "$";
        numeroDecimales = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(context);
        mostrarPresentacion = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(context);
        isTipoArticulo = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(context)
                .equals(Configuracion.FLGVERDADERO) ? Configuracion.TipoArticulo.PRESENTACION
                : Configuracion.TipoArticulo.PRODUCTO;
        maximoNumeroDecimales = Configuracion.EnumConfiguracion.MAXIMO_NUMERODECIMALES.getValor(context);
        setMaximoNumeroDecimales();
    }

    private void setMaximoNumeroDecimales() {
        iiMaximoNumeroDecimales = 0;
        try {
            iiMaximoNumeroDecimales = Integer.parseInt(maximoNumeroDecimales);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ActProductoDetalle", "Error conversion maximo numero de decimales: " + e.getMessage());
        }
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BeanEnvPedidoDet item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        String nombreArticulo = "";
        if (mostrarPresentacion.equals(Configuracion.FLGVERDADERO)) {
            nombreArticulo = item.getNombreArticulo().toUpperCase() + " - " + item.getCantidadPre()
                    + " " + item.getProducto().getUnidadDefecto();
        } else {
            nombreArticulo = item.getNombreArticulo().toUpperCase();
        }

        ((TextView) nuevaVista
                .findViewById(R.id.actpedidodetalleonline_txtnombrearticulo))
                .setText(nombreArticulo);

        ((TextView) nuevaVista
                .findViewById(R.id.actpedidodetalleonlineitem_txtcant))
                .setText(item.fnMostrarCantidad(isTipoArticulo,
                        isTipoArticulo.equals(Configuracion.TipoArticulo.PRESENTACION) ? Integer.valueOf(numeroDecimales) : iiMaximoNumeroDecimales));

        String tipoMeda = item.getTipoMoneda(); //@JBELVY

        ((TextView) nuevaVista
                .findViewById(R.id.actpedidodetalleonlineitem_txtmonto))
                .setText((tipoMeda.equals("1") ? flgMoneda : flgMonedaDolar)
                        + " "
                        + Utilitario.fnRound((tipoMeda.equals("1") ? item.getMontoSoles() : item.getMontoDolar()), Integer //@JBELVY Before getMonto()
                        .valueOf(flgNumDecVista)));

        if (item.getCodBonificacion() == 0) {
            ((LinearLayout) nuevaVista.findViewById(R.id.actproductoitem_lncontent))
                    .setBackgroundResource(R.drawable.pedidos_productopedido_listview_bg);
        } else {
            ((LinearLayout) nuevaVista.findViewById(R.id.actproductoitem_lncontent))
                    .setBackgroundResource(R.drawable.pedidos_productopedido_bonificacion_listview_bg);
        }

        return nuevaVista;
    }

}
