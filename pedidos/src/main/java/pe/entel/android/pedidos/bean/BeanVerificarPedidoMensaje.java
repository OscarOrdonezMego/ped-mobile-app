package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by rtamayov on 24/09/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanVerificarPedidoMensaje {

    @JsonProperty("Codigo")
    private String codigo;
    @JsonProperty("Descripcion")
    private String descripcion;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
