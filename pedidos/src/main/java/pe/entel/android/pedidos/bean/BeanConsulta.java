package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanConsulta {

    @JsonProperty
    private String codVendedor = "";
    @JsonProperty
    private long cantPedidos = 0;
    @JsonProperty
    private long cantNoPedidos = 0;
    @JsonProperty
    private String mntAcumulado = "0.00";
    @JsonProperty
    private long cantVisitasEfectivas = 0;
    @JsonProperty
    private String resultado = "";
    @JsonProperty
    private int idResultado = 0;
    @JsonProperty
    private String fechaConsulta = "";

    @JsonProperty
    private String tipoArticulo = "";

    @JsonProperty
    private String codCliente = "";

    public String getFechaConsulta() {
        return fechaConsulta;
    }

    public void setFechaConsulta(String fechaConsulta) {
        this.fechaConsulta = fechaConsulta;
    }

    public String getCodVendedor() {
        return codVendedor;
    }

    public void setCodVendedor(String codVendedor) {
        this.codVendedor = codVendedor;
    }

    public long getCantPedidos() {
        return cantPedidos;
    }

    public void setCantPedidos(long cantPedidos) {
        this.cantPedidos = cantPedidos;
    }

    public long getCantNoPedidos() {
        return cantNoPedidos;
    }

    public void setCantNoPedidos(long cantNoPedidos) {
        this.cantNoPedidos = cantNoPedidos;
    }

    public String getMntAcumulado() {
        return mntAcumulado;
    }

    public void setMntAcumulado(String mntAcumulado) {
        this.mntAcumulado = mntAcumulado;
    }

    public long getCantVisitasEfectivas() {
        return cantVisitasEfectivas;
    }

    public void setCantVisitasEfectivas(long cantVisitasEfectivas) {
        this.cantVisitasEfectivas = cantVisitasEfectivas;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public int getIdResultado() {
        return idResultado;
    }

    public void setIdResultado(int idResultado) {
        this.idResultado = idResultado;
    }

    public void calcularMontos() {

    }

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }

    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }
}
