package pe.entel.android.pedidos.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanEnvNoCobranza;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.UtilitarioData;

public class HttpGrabaNoCobranza extends HttpConexion {
    private BeanEnvNoCobranza ioBeanNoCobranza;
    private Context ioContext;

    public HttpGrabaNoCobranza(BeanEnvNoCobranza psBeanVisita, Context poContext, ConfiguracionNextel.EnumTipActividad poEnumTipActividad) {
        super(poContext, poContext.getString(R.string.httpgrabarpedido_dlg), poEnumTipActividad);
        ioBeanNoCobranza = psBeanVisita;
        ioContext = poContext;
    }

    @Override
    public boolean OnPreConnect() {
        SharedPreferences loSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ioContext);
        if (loSharedPreferences.getBoolean(ioContext.getString(R.string.keypre_cobertura), false)) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_modofueracobertura));
            return false;
        }
        int signal = UtilitarioData.fnSignalLevel(ioContext);
        if (!Utilitario.fnVerSignal(ioContext) || signal < 2) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            String lsResultado = (String) getHttpResponseObject();
            String[] laRespuesta = lsResultado.split(";");
            int liIdHttpRespuesta = Integer.valueOf(laRespuesta[0]);
            String lsHttpRespuesta = laRespuesta[1];

            setHttpResponseIdMessage(liIdHttpRespuesta, lsHttpRespuesta);
        }
    }

    public void subConHttp() {

        try {
            Log.v("XXX", "URL: " + Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.GRABARNOCOBRANZA));
            Log.v("XXX", "REQUEST: " + BeanMapper.toJson(ioBeanNoCobranza, false));
            String lsResultado = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.GRABARNOCOBRANZA), ioBeanNoCobranza);
            Log.v("XXX", "RESPONSE: " + lsResultado);
            setHttpResponseObject(lsResultado);

        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, "Error HTTP: " + e.getMessage());
        }
    }
}