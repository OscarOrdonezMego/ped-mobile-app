package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Dsb Mobile on 15/05/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanArticuloOnline {

    @JsonProperty
    private String id = "";
    @JsonProperty
    private String codigo = "";
    @JsonProperty
    private String nombre = "";
    @JsonProperty
    private String stock = "";
    @JsonProperty
    private String precioBase = "0";
    @JsonProperty
    private String codAlmacen = "0";
    @JsonProperty
    private String nombreAlmacen = "";
    @JsonProperty
    private String tipoArticulo = "";
    @JsonProperty
    private String tipoBusqueda = "";
    @JsonProperty
    private String codigoProducto = "";
    @JsonProperty
    private String nombreProducto = "";
    @JsonProperty
    private String cantidadPresentacion = "";
    @JsonProperty
    private String unidadDefecto = "";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getPrecioBase() {
        return precioBase;
    }

    public void setPrecioBase(String precioBase) {
        this.precioBase = precioBase;
    }

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }

    public String getNombreAlmacen() {
        return nombreAlmacen;
    }

    public void setNombreAlmacen(String nombreAlmacen) {
        this.nombreAlmacen = nombreAlmacen;
    }

    public String getTipoBusqueda() {
        return tipoBusqueda;
    }

    public void setTipoBusqueda(String tipoBusqueda) {
        this.tipoBusqueda = tipoBusqueda;
    }

    public String getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getCantidadPresentacion() {
        return cantidadPresentacion;
    }

    public void setCantidadPresentacion(String cantidadPresentacion) {
        this.cantidadPresentacion = cantidadPresentacion;
    }

    public String getUnidadDefecto() {
        return unidadDefecto;
    }

    public void setUnidadDefecto(String unidadDefecto) {
        this.unidadDefecto = unidadDefecto;
    }
}
