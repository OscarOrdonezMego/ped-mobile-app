package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import greendroid.widget.ActionBarItem;
import greendroid.widget.item.DrawableItem;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.util.Gps;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.adap.AdapNoPedidoLista;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanKPI;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalGeneral;
import pe.entel.android.pedidos.dal.DalKPI;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.http.HttpGrabaNoPedido;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

public class ActMotNoPedidoLista extends NexActivity implements
        OnItemClickListener, NexMenuCallbacks, MenuSlidingListener {

    @SuppressWarnings("unused")
    private final int CONSICOFIN = 0;
    private final int CONSBACK = 3;
    int mAno, mMes, mDia;
    String codMotNoPedido = "";
    String desMotNoPedido = "";
    BeanEnvPedidoCab loBeanPedidoCab = null;

    ListView lista;

    BeanUsuario loBeanUsuario = null;
    private AdapMenuPrincipalSliding _menu;

    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDASINCRONIZACION = 11;
    private final int ENVIOPENDIENTES = 12;
    private final int VALIDASINCRO = 13;
    private final int NOHAYPENDIENTES = 14;

    private String sharePreferenceBeanPedidoCab;
    boolean estadoSincronizar = false;

    ArrayList<DrawableItem> ioLista;

    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro

    //PARAMETROS DE CONFIGURACION
    private String isGps;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);

        sharePreferenceBeanPedidoCab = "beanPedidoCabVerificarPedido";
        String lsBeanVisitaCab = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                sharePreferenceBeanPedidoCab, "");

        if (lsBeanVisitaCab.equals("")) {
            lsBeanVisitaCab = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                    "beanPedidoCab", "");
            sharePreferenceBeanPedidoCab = "beanPedidoCab";
        }

        loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper.fromJson(
                lsBeanVisitaCab, BeanEnvPedidoCab.class);

        String lsBeanUsuario = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario,
                BeanUsuario.class);

        isGps = Configuracion.EnumConfiguracion.GPS.getValor(this);

        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);

        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));

        subIniMenu();
    }

    @Override
    protected void subSetControles() {
        // TODO Auto-generated method stub
        super.subSetControles();

        setActionBarContentView(R.layout.motnopedido);
        lista = (ListView) findViewById(R.id.actmotnopedidolista_lista);
        lista.setOnItemClickListener(this);

        subLLenarLista();


    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                            long arg3) {
        // TODO Auto-generated method stub

        String lsMotivo = String.valueOf(((DrawableItem) (lista
                .getItemAtPosition(position))).getTag());
        codMotNoPedido = lsMotivo;
        lsMotivo = String.valueOf(((DrawableItem) (lista
                .getItemAtPosition(position))).getText());
        desMotNoPedido = lsMotivo;

        Calendar c = Calendar.getInstance();
        Date loDate = c.getTime();
        final String sFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate)
                .toString();

        String lsBeanPedidoCab = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                sharePreferenceBeanPedidoCab, "");
        loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper.fromJson(
                lsBeanPedidoCab, BeanEnvPedidoCab.class);

        StringBuffer loStb = new StringBuffer();
        loStb.append(getString(R.string.actmotnopedidolista_dlgfin)).append(
                '\n');
        loStb.append("Motivo : ").append(desMotNoPedido).append('\n');
        loStb.append(sFecha).append('\n');

        AlertDialog loAlertDialog = null;
        loAlertDialog = new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setTitle(
                        getString(R.string.actmotnopedidolista_titulonopedido))
                .setMessage(loStb.toString())

                .setPositiveButton(getString(R.string.dlg_btnsi),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                try {
                                    subGrabar();
                                    enviarPedidoServer();
                                } catch (Exception e) {

                                }
                            }
                        })
                .setNegativeButton(getString(R.string.dlg_btncancelar),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                return;
                            }
                        })

                .create();
        loAlertDialog.show();

    }

    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        switch (item.getItemId()) {

            case CONSBACK:
                Intent loIntent = new Intent(this, ActClienteDetalle.class);
                loIntent.putExtra("IdCliente",
                        Long.parseLong(loBeanPedidoCab.getClipk()));
                finish();
                startActivity(loIntent);
                break;

            default:
                return super.onHandleActionBarItemClick(item, position);
        }
        return true;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences(
                                                    "Actual", MODE_PRIVATE)
                                                    .getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                                    .fromJson(lsBeanUsuario,
                                                            BeanUsuario.class);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActMotNoPedidoLista.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActMotNoPedidoLista.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setTitle(
                getString(R.string.actmotnopedidolista_bartitulo));
    }

    public void subLLenarLista() {

        DalGeneral loDalGeneral = new DalGeneral(this);
        ioLista = loDalGeneral.fnSelMotNoPedidoDrawable();

        final AdapNoPedidoLista adapter = new AdapNoPedidoLista(this,
                R.layout.motnopedidolista_item, ioLista);
        lista.setAdapter(adapter);

    }

    @SuppressWarnings("unused")
    private void direccionarFlujo() {
        new NexToast(this, R.string.actmotnopedidolista_nopedidograbado,
                EnumType.ACCEPT).show();
        finish();
        startActivity(new Intent(this, ActClienteDetalle.class));
    }

    private void subGrabar() throws Exception {
        Calendar c = Calendar.getInstance();
        Date loDate = c.getTime();

        final String sFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate)
                .toString();

        String lsBeanRepresentante = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(
                lsBeanRepresentante, BeanUsuario.class);
        // Se guarda GPS si tiene el flag habilitado
        Location location = null;

        try {
            location = Gps.getLocation();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (isGps.equals(
                Configuracion.FLGVERDADERO)
                && location != null) {
            loBeanPedidoCab.setLatitud(String.valueOf(location.getLatitude()));
            loBeanPedidoCab
                    .setLongitud(String.valueOf(location.getLongitude()));
            loBeanPedidoCab.setCelda(location.getProvider().substring(0, 1)
                    .toUpperCase());

        } else {
            loBeanPedidoCab.setLatitud("0");
            loBeanPedidoCab.setLongitud("0");
            loBeanPedidoCab.setCelda("-");

        }

        loBeanPedidoCab.setFechaFin(sFecha);
        loBeanPedidoCab.setCodigoMotivo(codMotNoPedido);

        String lsPedidoCab = "";
        try {
            lsPedidoCab = BeanMapper.toJson(loBeanPedidoCab, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString(sharePreferenceBeanPedidoCab, lsPedidoCab);
        loEditor.commit();

        // Grabamos en la BD del equipo
        DalPedido loDalPedido = new DalPedido(this);
        String codPed = loDalPedido.fnGrabarPedido(loBeanPedidoCab);
        loBeanPedidoCab.setCodigoPedido(codPed);

        BeanCliente loBeanCliente = ((AplicacionEntel) getApplication())
                .getCurrentCliente();

        // Actualizamos estado y KPI si es cliente en la ruta
        if (((AplicacionEntel) getApplication()).getClienteOnline() == 0) {
            DalCliente loDalCliente = new DalCliente(this);
            // Actualizo estado del cliente en base de datos
            BeanKPI loBeanKpi = ((AplicacionEntel) getApplication())
                    .getCurrentKpi();

            if (loBeanCliente.getEstado().equals(
                    Configuracion.ESTADO01PORVISITAR)) {
                loDalCliente.fnUpdEstadoVisitaCliente(
                        Long.parseLong(loBeanCliente.getClipk()),
                        Configuracion.ESTADO02VISITADO);
                // Un cliente mas visitado
                loBeanKpi.numCliVis += 1;
            }

            // Aumenta numero de no visitas
            loBeanKpi.numNoPed += 1;
            // Se graba bean KPI
            DalKPI lodkpi = new DalKPI(this);
            lodkpi.fnUpdKPI(loBeanKpi);
            ((AplicacionEntel) getApplication()).setCurrentKpi(loBeanKpi);

        }
        // Actualizo el estado del currentcliente
        if (loBeanCliente.getEstado().equals(Configuracion.ESTADO01PORVISITAR)) {
            loBeanCliente.setEstado(Configuracion.ESTADO02VISITADO);
        }

        ((AplicacionEntel) getApplication()).setCurrentCliente(loBeanCliente);
    }

    public void enviarPedidoServer() {
        Log.v("XXX", "AHORA GRABO EN SERVIDOR");
        new HttpGrabaNoPedido(loBeanPedidoCab, ActMotNoPedidoLista.this,
                fnTipactividad()).execute();
        Log.v("XXX", "YAAAAA GRABO EN SERVIDOR");
    }

    @SuppressWarnings("deprecation")
    public void subAccDesMensaje() {
        try {
            Intent intentl = new Intent(this, ActClienteDetalle.class);
            intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP); // JUAN

            if (estadoSincronizar) {
                if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                    Intent loIntent = new Intent(this, ActMenu.class);

                    loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loIntent);
                }
            } else if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                // como grabo bien en el servidor, hago un update al registro
                DalPedido loDalVisita = new DalPedido(this);
                boolean lbResult = loDalVisita
                        .fnUpdEstadoPedido(loBeanPedidoCab.getCodigoPedido());

                if (lbResult) {

                    startActivity(intentl);
                } else {
                    new NexToast(this, R.string.actgrabapedido_dlgerrorpedido,
                            EnumType.ERROR).show();
                    // finish();
                    startActivity(intentl);
                }
            } else {
                new NexToast(this, R.string.actgrabapedido_dlgerrorpedido,
                        EnumType.ERROR).show();
                // finish();
                startActivity(intentl);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void onBackPressed() {

        Intent loIntent = new Intent(this, ActClienteDetalle.class);
        loIntent.putExtra("IdCliente", Long
                .parseLong(((AplicacionEntel) getApplication())
                        .getCurrentCliente().getClipk()));
        finish();
        startActivity(loIntent);
    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent loIntentLogin = new Intent(ActMotNoPedidoLista.this,
                        ActLogin.class);
                loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(loIntentLogin);
            }
        });

    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        Intent intentl = new Intent(this, ActMenu.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intentl);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @SuppressWarnings("deprecation")
    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = null;
            DalPedido loDalPedido = new DalPedido(this);
            loList = loDalPedido.fnEnvioPedidosPendientes(loBeanUsuario
                    .getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);

                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();

                showDialog(CONSDIASINCRONIZAR);

            } else {
                showDialog(VALIDASINCRO);

            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        String lsBeanUsuario = getSharedPreferences(
                "Actual", MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper
                .fromJson(lsBeanUsuario,
                        BeanUsuario.class);
        this.setMsgOk("Eficiencia Online");

        BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
        loBeanListaEficienciaOnline.setIdResultado(0);
        loBeanListaEficienciaOnline.setResultado("");
        loBeanListaEficienciaOnline.setListaEficiencia(null);
        loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

        new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

        Intent loInstent = new Intent(this, ActKpiDetalle.class);
        loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loInstent);
    }

    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        ((AplicacionEntel) getApplication()).setCurrentCliente(null);
        ((AplicacionEntel) getApplication())
                .setCurrentCodFlujo(Configuracion.CONSSTOCK);
        ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
        Intent loIntento = new Intent(ActMotNoPedidoLista.this,
                ActProductoListaOnline.class);
        loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        BeanExtras loBeanExtras = new BeanExtras();
        loBeanExtras.setFiltro("");
        loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
        String lsFiltro = "";
        try {
            lsFiltro = BeanMapper.toJson(loBeanExtras, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
        startActivity(loIntento);
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(getString(R.string.ml_sincronizarantes)));
        }

    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }


    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */

    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */
    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        try {
            DialogFragmentYesNo loDialog;
            switch (id) {
                case DIALOG_NSERVICES_DOWNLOAD:
                    String lsNServices;
                    if (ioDALNservices == null)
                        ioDALNservices = DALNServices.getInstance(this,
                                R.string.db_name);
                    try {
                        lsNServices = ioDALNservices.fnSelNServicesLabel();
                    } catch (Exception e) {
                        Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                e);
                        lsNServices = getString(R.string.nservices_label_default);
                    }
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG
                                    + DIALOG_NSERVICES_DOWNLOAD,
                            getString(R.string.nservices_consultingdownlad)
                                    .replace("{0}", lsNServices),
                            EnumLayoutResource.getDefaultIconHelp(this));
                    return new DialogFragmentYesNoPairListener(loDialog,
                            new OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(
                                        DialogFragmentYesNo dialog) {
                                    try {
                                        subIniNServicesDownload(ioDALNservices
                                                .fnSelNServicesAPK());
                                    } catch (Exception e) {
                                        subShowException(e);
                                    }
                                }

                                @Override
                                public void performButtonNo(
                                        DialogFragmentYesNo dialog) {
                                }
                            });

                default:
                    return super.onCreateNexDialog(id);
            }
        } catch (Exception e) {
            subShowException(e);
            return super.onCreateNexDialog(id);
        }
    }

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

}
