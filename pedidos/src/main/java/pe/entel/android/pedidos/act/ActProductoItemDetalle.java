package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.InputType;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pe.com.nextel.android.actividad.NexInvalidOperatorActivity.NexInvalidOperatorException;
import pe.com.nextel.android.actividad.support.NexFragmentActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.DecimalTextWatcher;
import pe.com.nextel.android.util.Logger;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.util.support.DialogFragmentYesNo;
import pe.com.nextel.android.util.support.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.support.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.support.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexProgressDialog;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.bean.BeanAlmacen;
import pe.entel.android.pedidos.bean.BeanArticulo;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanEnvCanjeCab;
import pe.entel.android.pedidos.bean.BeanEnvDevolucionCab;
import pe.entel.android.pedidos.bean.BeanEnvItemDet;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanFraccionamiento;
import pe.entel.android.pedidos.bean.BeanGeneral;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanPresentacion;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalAlmacen;
import pe.entel.android.pedidos.dal.DalGeneral;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.dal.DalProducto;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;
import pe.entel.android.pedidos.widget.ListaPopup;
import pe.entel.android.pedidos.widget.ListaPopup.OnItemClickPopup;

@SuppressWarnings({"unused", "rawtypes"})
public class ActProductoItemDetalle extends NexFragmentActivity implements
        OnClickListener, NexMenuCallbacks, MenuSlidingListener,
        OnItemClickPopup {

    private TextView txtCodigo;
    private TextView txtNombre;
    private EditText txtCantidad;

    private Button btnfinalizar;
    private Button spiMotivo;
    private Button ioSpiFraccionamiento;
    private Button ioSpiAlmacen;

    private TextView mDateDisplay;
    private TextView ioTxtPresentacion;
    private Button btnPickDate;
    private EditText txtObservacion;

    private LinearLayout ioRowPresentacion;
    private LinearLayout ioRowFraccionamiento;
    private LinearLayout ioRowAlmacen;

    private int mYear;
    private int mMonth;
    private int mDay;

    private final int POPUP_MOTIVO = 1;
    private final int POPUP_FRACCIONAMIENTO = 2;
    private final int POPUP_ALMACEN = 3;

    static final int DATE_DIALOG_ID = 4;


    private BeanEnvItemDet loBeanPedidoDet = null;
    private BeanUsuario loBeanUsuario = null;
    private BeanArticulo ioBeanArticulo;

    private final int CONSICOVALIDAR = 0;
    private final int CONSBACK = 3;
    private final int CONSICOVALIDARMOT = 2;
    private final int CONSFINAL = 1;


    private DalGeneral loDalGeneral;
    private List ioLista;

    private boolean editando = false;


    private AdapMenuPrincipalSliding _menu;

    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDASINCRONIZACION = 11;
    private final int ENVIOPENDIENTES = 12;
    private final int VALIDASINCRO = 13;
    private final int NOHAYPENDIENTES = 14;
    private final int CONSVALIDARFRACC = 15;
    boolean estadoSincronizar = false;

    private CaldroidFragment dialogCaldroidFragment;

    private int flujo;

    Bundle savedInstanceState;


    public static int ESTADO_ACTIVITY = 0;
    public static int NUEVO_ACTIVITY = 0;
    public static int ACTIGUO_ACTIVITY = 1;


    public static boolean showPopupFrac = false;
    public static boolean showPopupAlm = false;
    public static boolean showPopupMot = false;
    static BeanGeneral motivo;
    static Date selectDate;

    static BeanAlmacen ioBeanAlmacen = null;
    static BeanFraccionamiento ioBeanFraccionamiento;
    NexProgressDialog progress;

    // Integracion Ntrack
    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro


    private String isPresentacion;
    private String isNumeroDecimales;
    private String isMostrarAlmacen;
    private String isMaximoNumeroDecimales;
    private int iiMaximoNumeroDecimales;

    private int CANTIDAD_ENTEROS;
    private int CANTIDAD_DECIMALES;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logger.d("XXX", "ActProductoItemDetalle");
        ValidacionServicioUtil.validarServicio(this);
        String lsBeanUsuario = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");

        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario,
                BeanUsuario.class);

        isPresentacion = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(this);
        isNumeroDecimales = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(this);
        isMostrarAlmacen = Configuracion.EnumConfiguracion.MOSTRAR_ALMACEN.getValor(this);
        isMaximoNumeroDecimales = Configuracion.EnumConfiguracion.MAXIMO_NUMERODECIMALES.getValor(this);
        setMaximoNumeroDecimales();

        setUsingNexMenuDrawerLayout(this);

        super.onCreate(savedInstanceState);

        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));

        this.savedInstanceState = savedInstanceState;
        subIniMenu();

        progress = new NexProgressDialog(ActProductoItemDetalle.this);
        if (isUsingDialogFragment())
            progress.setUsesNewLayout(true);
        progress.setMessage("Cargando datos...");

    }

    @Override
    public void onBackPressed() {

        Intent loIntent = new Intent(this, ActProductoItemLista.class);
        finish();
        startActivity(loIntent);
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setTitle(
                getString(R.string.actproductodetalle_bartitulo));
    }

    @Override
    public void subSetControles() {
        setActionBarContentView(R.layout.productoitemdetalle2);
        super.subSetControles();

        txtCodigo = (TextView) findViewById(R.id.actproductodetalle_txtCodigo);

        txtNombre = (TextView) findViewById(R.id.actproductodetalle_txtNombre);

        txtCantidad = (EditText) findViewById(R.id.actproductodetalle_txtCantidad);
        ioTxtPresentacion = (TextView) findViewById(R.id.actproductodetalle_txtPresentacion);
        btnfinalizar = (Button) findViewById(R.id.actproductopedido_btnfinalizar);

        spiMotivo = (Button) findViewById(R.id.actproductoitem_cmbmotivo);
        ioSpiFraccionamiento = (Button) findViewById(R.id.actproductodetalle_cmbFraccionamiento);
        ioSpiAlmacen = (Button) findViewById(R.id.actproductodetalle_cmbAlmacen);
        txtObservacion = (EditText) findViewById(R.id.actproductoitemdetalle_txtobservacion);

        mDateDisplay = (TextView) findViewById(R.id.dateDisplay);
        btnPickDate = (Button) findViewById(R.id.pickDate);
        ioRowPresentacion = (LinearLayout) findViewById(R.id.actproductodetalle_tablerow_presentacion);
        ioRowFraccionamiento = (LinearLayout) findViewById(R.id.actproductodetalle_tablerow_fraccionamiento);
        ioRowAlmacen = (LinearLayout) findViewById(R.id.actproductodetalle_tablerow_almacen);

        setInputTypeEditText(txtCantidad);
        txtCantidad.addTextChangedListener(new DecimalTextWatcher(CANTIDAD_ENTEROS, CANTIDAD_DECIMALES));

        btnPickDate.setOnClickListener(this);
        btnfinalizar.setOnClickListener(this);
        spiMotivo.setOnClickListener(this);
        ioSpiFraccionamiento.setOnClickListener(this);
        ioSpiAlmacen.setOnClickListener(this);

        subCargaDetalle();

    }


    private void setInputTypeEditText(EditText editText) {
        if (iiMaximoNumeroDecimales <= 0) {
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else {
            editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        }
    }

    private void setMaximoNumeroDecimales() {
        iiMaximoNumeroDecimales = 0;
        try {
            iiMaximoNumeroDecimales = Integer.parseInt(isMaximoNumeroDecimales);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ActProductoDetalle", "Error conversion maximo numero de decimales: " + e.getMessage());
        }
        if (iiMaximoNumeroDecimales <= 0) {
            CANTIDAD_DECIMALES = iiMaximoNumeroDecimales;
            CANTIDAD_ENTEROS = Configuracion.CANTIDAD_DIGITOS;
        } else {
            CANTIDAD_DECIMALES = iiMaximoNumeroDecimales;
            CANTIDAD_ENTEROS = (Configuracion.CANTIDAD_DIGITOS - iiMaximoNumeroDecimales) - 1;
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {
        progress.dismiss();
        Log.i("subaccdesmensaje", "entrroo");
        if (estadoSincronizar)
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActMenu.class);

                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }

    }

    private void subCargaDetalle() {
        BeanEnvItemDet lobeanitem = ((AplicacionEntel) getApplication())
                .getCurrentItem();
        DalProducto loDalProducto = new DalProducto(this);

        BeanCliente loBeanCliente = ((AplicacionEntel) getApplication())
                .getCurrentCliente();

        boolean lbAntiguo = false;

        if (lobeanitem == null) {
            editando = false;
            Bundle loExtras = getIntent().getExtras();
            String lsIdArticulo = loExtras.getString("IdArticulo");


            if (isPresentacion.equals(Configuracion.FLGVERDADERO))
                ioBeanArticulo = loDalProducto.fnSelxIdPresentacion(lsIdArticulo);
            else
                ioBeanArticulo = loDalProducto.fnSelxIdProducto(lsIdArticulo);

            // get the current date
            if (ESTADO_ACTIVITY == NUEVO_ACTIVITY) {
                activityNuevo();
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                selectDate = c.getTime();
                subActFecha();
            } else {
                lbAntiguo = true;
            }
        } else {
            editando = true;

            if (isPresentacion.equals(Configuracion.FLGVERDADERO))
                ioBeanArticulo = loDalProducto.fnSelxIdPresentacion(lobeanitem.getIdArticulo());
            else
                ioBeanArticulo = loDalProducto.fnSelxIdProducto(lobeanitem.getIdArticulo());


            if (ESTADO_ACTIVITY == NUEVO_ACTIVITY) {

                txtCantidad.setText(lobeanitem.getCantidad());

                if (isPresentacion.equals(Configuracion.FLGVERDADERO) &&
                        iiMaximoNumeroDecimales > 0) {
                    double cantidadTotal = 0;
                    double cantidadpre = ((BeanPresentacion) ioBeanArticulo).getCantidad();
                    try {
                        cantidadTotal = Double.parseDouble(lobeanitem.getCantidad()) + (Double.parseDouble(lobeanitem.getCantidadFrac()) / cantidadpre);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    txtCantidad.setText(Utilitario.fnRound(cantidadTotal, iiMaximoNumeroDecimales));
                }

                txtObservacion.setText(lobeanitem.getObservacion());

                mDay = Integer.parseInt(lobeanitem.getFechaVencimiento()
                        .substring(0, 2));
                mMonth = Integer.parseInt(lobeanitem.getFechaVencimiento()
                        .substring(3, 5)) - 1;
                mYear = Integer.parseInt(lobeanitem.getFechaVencimiento()
                        .substring(6, 10));
                final Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, mYear);
                calendar.set(Calendar.MONTH, mMonth);
                calendar.set(Calendar.DAY_OF_MONTH, mDay);

                selectDate = calendar.getTime();

                subActFecha();
                int seleccionado = -1;

                if (ioLista == null) {
                    loDalGeneral = new DalGeneral(this);
                    flujo = ((AplicacionEntel) getApplication())
                            .getCurrentCodFlujo();
                    if (flujo == Configuracion.CONSCANJE) {
                        ioLista = loDalGeneral.fnSelGeneral(String
                                .valueOf(Configuracion.GENERAL5MOTCANJE));
                    } else {
                        ioLista = loDalGeneral.fnSelGeneral(String
                                .valueOf(Configuracion.GENERAL6MOTDEVO));
                    }
                }

                for (int i = 0; i < ioLista.size(); i++) {
                    if (((BeanGeneral) ioLista.get(i)).getCodigo().equals(
                            lobeanitem.getCodigoMotivo())) {
                        motivo = (BeanGeneral) ioLista.get(i);
                    }
                }
                if (motivo != null)
                    spiMotivo.setText(motivo.getDescripcion());

            } else
                lbAntiguo = true;


        }


        DalAlmacen dalAlmacen = new DalAlmacen(this);
        if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO)) {
            ioRowAlmacen.setVisibility(View.VISIBLE);
            if (!lbAntiguo) {
                if (editando) {
                    ioBeanAlmacen = dalAlmacen.fnSelectXIdAlmacen(lobeanitem.getCodAlmacen());
                } else {
                    ioBeanAlmacen = dalAlmacen.fnSelectAll().get(0);
                }
                ioSpiAlmacen.setText(ioBeanAlmacen.getNombre());
            }

        } else {
            ioRowAlmacen.setVisibility(View.GONE);
        }

        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {


            ioRowPresentacion.setVisibility(View.VISIBLE);
            if (iiMaximoNumeroDecimales > 0) {
                ioRowFraccionamiento.setVisibility(View.GONE);
            } else {
                ioRowFraccionamiento.setVisibility(View.VISIBLE);
            }

            BeanPresentacion loBeanPresentacion = (BeanPresentacion) ioBeanArticulo;

            txtCodigo.setText(loBeanPresentacion.getProducto().getCodigo());
            txtNombre.setText(loBeanPresentacion.getProducto().getNombre());
            ioTxtPresentacion.setText(loBeanPresentacion.getNombre());
            loBeanPresentacion.fnObtenerLstFraccionamiento(Integer.valueOf(isNumeroDecimales));
            if (!lbAntiguo) {
                if (editando) {
                    ioBeanFraccionamiento = loBeanPresentacion.fnObtenerFraccionamientoPorNumerador(lobeanitem.getCantidadFrac());
                } else {
                    ioBeanFraccionamiento = loBeanPresentacion.getLstBeanFraccionamiento().get(0);
                }
                if (ioBeanFraccionamiento != null)
                    ioSpiFraccionamiento.setText(ioBeanFraccionamiento.toString());
            }

        } else {
            ioRowFraccionamiento.setVisibility(View.GONE);
            ioRowPresentacion.setVisibility(View.GONE);
            txtCodigo.setText(ioBeanArticulo.getCodigo());
            txtNombre.setText(ioBeanArticulo.getNombre());
        }
        if (lbAntiguo)
            activityAntiguo();
    }

    private void activityNuevo() {
        motivo = null;
        ESTADO_ACTIVITY = ACTIGUO_ACTIVITY;
        selectDate = null;
        showPopupAlm = false;
        showPopupMot = false;
        showPopupFrac = false;

    }

    @SuppressWarnings("unchecked")
    private void activityAntiguo() {
        if (ioBeanAlmacen != null)
            ioSpiAlmacen.setText(ioBeanAlmacen.getNombre());
        if (ioBeanFraccionamiento != null)
            ioSpiFraccionamiento.setText(ioBeanFraccionamiento.toString());
        if (motivo != null)
            spiMotivo.setText(motivo.getDescripcion());

        if (selectDate != null)
            subActFecha();

        if (showPopupAlm) {
            DalAlmacen loDalAlmacen = new DalAlmacen(ActProductoItemDetalle.this);
            List<BeanAlmacen> loLstBeanAlmacenes = null;

            loLstBeanAlmacenes = loDalAlmacen.fnSelectAll();

            if (loLstBeanAlmacenes.size() > 1)
                new ListaPopup().dialog(ActProductoItemDetalle.this, loLstBeanAlmacenes,
                        POPUP_ALMACEN, ActProductoItemDetalle.this,
                        getString(R.string.listapopup_txtseleccionealmacen), ioBeanAlmacen,
                        R.attr.PedidosPopupLista);
        }

        if (showPopupFrac) {
            BeanPresentacion loBeanPresentacion = (BeanPresentacion) ioBeanArticulo;

            if (loBeanPresentacion.getLstBeanFraccionamiento().size() > 1)
                new ListaPopup().dialog(ActProductoItemDetalle.this, loBeanPresentacion.getLstBeanFraccionamiento(),
                        POPUP_FRACCIONAMIENTO, ActProductoItemDetalle.this,
                        getString(R.string.listapopup_txtseleccionefraccionamiento), ioBeanFraccionamiento,
                        R.attr.PedidosPopupLista);
        }

        if (showPopupMot) {

            if (ioLista == null) {
                loDalGeneral = new DalGeneral(this);
                flujo = ((AplicacionEntel) getApplication())
                        .getCurrentCodFlujo();
                if (flujo == Configuracion.CONSCANJE) {
                    ioLista = loDalGeneral.fnSelGeneral(String
                            .valueOf(Configuracion.GENERAL5MOTCANJE));
                } else {
                    ioLista = loDalGeneral.fnSelGeneral(String
                            .valueOf(Configuracion.GENERAL6MOTDEVO));
                }
            }

            new ListaPopup().dialog(ActProductoItemDetalle.this, ioLista,
                    POPUP_MOTIVO, ActProductoItemDetalle.this,
                    getString(R.string.listapopup_txtseleccionemotivo), motivo,
                    R.attr.PedidosPopupLista);
        }

    }

    public double fnFraccMinimo() {
        BeanPresentacion bean = (BeanPresentacion) ioBeanArticulo;
        if (bean.getCantidad() == 0) return 0.0;
        return (double) bean.getUnidadFraccionamiento() / bean.getCantidad();
    }

    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        DialogFragmentYesNo loDialog;
        switch (id) {

            case DIALOG_NSERVICES_DOWNLOAD:
                String lsNServices;
                if (ioDALNservices == null)
                    try {
                        ioDALNservices = DALNServices.getInstance(this,
                                R.string.db_name);
                    } catch (NexInvalidOperatorException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                try {
                    lsNServices = ioDALNservices.fnSelNServicesLabel();
                } catch (Exception e) {
                    Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                    lsNServices = getString(R.string.nservices_label_default);
                }
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG + DIALOG_NSERVICES_DOWNLOAD,
                        getString(R.string.nservices_consultingdownlad).replace(
                                "{0}", lsNServices),
                        EnumLayoutResource.getDefaultIconHelp(this));
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    subIniNServicesDownload(ioDALNservices
                                            .fnSelNServicesAPK());
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                            }
                        });

            case CONSICOVALIDAR:
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSICOVALIDAR"),
                        getString(R.string.actproductodetalle_dlgingdatoscero),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case CONSICOVALIDARMOT:
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSICOVALIDAR"),
                        getString(R.string.actproductodetalle_dlgmotivo),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case CONSVALIDARFRACC:
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSVALIDARFRACC"),
                        getString(R.string.actproductodetalle_dlgcantidadfracc).
                                replace("?", String.valueOf(fnFraccMinimo())),
                        R.drawable.boton_ayuda, false, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);


            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.actmenu_dlgsincronizar),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    String lsBeanUsuario = getSharedPreferences(
                                            "Actual", MODE_PRIVATE).getString(
                                            "BeanUsuario", "");
                                    BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                            .fromJson(lsBeanUsuario,
                                                    BeanUsuario.class);
                                    estadoSincronizar = true;
                                    progress.show();
                                    new HttpSincronizar(loBeanUsuario
                                            .getCodVendedor(),
                                            ActProductoItemDetalle.this,
                                            fnTipactividad()).execute();
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            case VALIDASINCRONIZACION:
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"),
                        getString(R.string.actmenuvalsincro_titulo),
                        getString(R.string.actmenuvalsincro_mensaje),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case VALIDASINCRO:
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                        getString(R.string.msgregpendientesenvinfo),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case ENVIOPENDIENTES:
                loDialog = DialogFragmentYesNo.newInstance(
                        getSupportFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.msgdeseaenviarpendientes),
                        R.drawable.boton_aceptar, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                Intent loIntentEspera = new Intent(
                                        ActProductoItemDetalle.this,
                                        ActGrabaPendiente.class);
                                startActivity(loIntentEspera);
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            default:
                return super.onCreateNexDialog(id);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSICOVALIDAR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actproductodetalle_dlgingdatoscero))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        txtCantidad.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;
            case CONSICOVALIDARMOT:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.actproductodetalle_dlgmotivo))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        spiMotivo.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;

            case CONSVALIDARFRACC:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductodetalle_dlgcantidadfracc).
                                replace("?", String.valueOf(fnFraccMinimo())))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        txtCantidad.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;

            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, mDateSetListener, mYear, mMonth,
                        mDay);

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences(
                                                    "Actual", MODE_PRIVATE)
                                                    .getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                                    .fromJson(lsBeanUsuario,
                                                            BeanUsuario.class);
                                            estadoSincronizar = true;
                                            progress.show();
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActProductoItemDetalle.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActProductoItemDetalle.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onClick(View arg0) {
        boolean flgPaso = true;

        if (arg0 == btnPickDate) {

            dialogCaldroidFragment = new CaldroidFragment();

            TypedArray ta = obtainStyledAttributes(new int[]{R.attr.PedidosPopupCalendar});
            dialogCaldroidFragment.setDrawableLogo(ta.getResourceId(0, 0));
            ta.recycle();

            dialogCaldroidFragment.setCancelable(true);
            dialogCaldroidFragment.setCaldroidListener(new CaldroidListener() {

                @Override
                public void onSelectDate(Date date, View view) {

                    selectDate = date;
                    subActFecha();
                    dialogCaldroidFragment.dismiss();

                }
            });

            // If activity is recovered from rotation
            final String dialogTag = "CALDROID_DIALOG_FRAGMENT";
            final Bundle statebun = savedInstanceState;
            if (statebun != null) {
                dialogCaldroidFragment.restoreDialogStatesFromKey(
                        getSupportFragmentManager(), statebun,
                        "DIALOG_CALDROID_SAVED_STATE", dialogTag);
                Bundle args = dialogCaldroidFragment.getArguments();
                if (args == null) {
                    args = new Bundle();
                    dialogCaldroidFragment.setArguments(args);
                }

            } else {
                // Setup arguments
                Bundle bundle = new Bundle();
                // Setup dialogTitle

                dialogCaldroidFragment.setArguments(bundle);
            }

            dialogCaldroidFragment.show(getSupportFragmentManager(), dialogTag);
        } else if (arg0 == btnfinalizar)
            finalizar();

        else if (arg0 == spiMotivo) {
            // Llenar el combo

            if (ioLista == null) {
                loDalGeneral = new DalGeneral(this);
                flujo = ((AplicacionEntel) getApplication())
                        .getCurrentCodFlujo();
                if (flujo == Configuracion.CONSCANJE) {
                    ioLista = loDalGeneral.fnSelGeneral(String
                            .valueOf(Configuracion.GENERAL5MOTCANJE));
                } else {
                    ioLista = loDalGeneral.fnSelGeneral(String
                            .valueOf(Configuracion.GENERAL6MOTDEVO));
                }
            }

            showPopupMot = true;

            new ListaPopup().dialog(ActProductoItemDetalle.this, ioLista,
                    POPUP_MOTIVO, ActProductoItemDetalle.this,
                    getString(R.string.listapopup_txtseleccionemotivo), motivo,
                    R.attr.PedidosPopupLista);
            // Creamos el adaptador

        } else if (ioSpiFraccionamiento.equals(arg0)) {
            showPopupFrac = true;
            BeanPresentacion loBeanPresentacion = (BeanPresentacion) ioBeanArticulo;

            if (loBeanPresentacion.getLstBeanFraccionamiento().size() > 1)
                new ListaPopup().dialog(ActProductoItemDetalle.this, loBeanPresentacion.getLstBeanFraccionamiento(),
                        POPUP_FRACCIONAMIENTO, ActProductoItemDetalle.this,
                        getString(R.string.listapopup_txtseleccionefraccionamiento), ioBeanFraccionamiento,
                        R.attr.PedidosPopupLista);
        } else if (ioSpiAlmacen.equals(arg0)) {
            showPopupAlm = true;
            DalAlmacen loDalAlmacen = new DalAlmacen(ActProductoItemDetalle.this);
            List<BeanAlmacen> loLstBeanAlmacenes = null;

            loLstBeanAlmacenes = loDalAlmacen.fnSelectAll();

            if (loLstBeanAlmacenes.size() > 1)
                new ListaPopup().dialog(ActProductoItemDetalle.this, loLstBeanAlmacenes,
                        POPUP_ALMACEN, ActProductoItemDetalle.this,
                        getString(R.string.listapopup_txtseleccionealmacen), ioBeanAlmacen,
                        R.attr.PedidosPopupLista);
        }
    }


    private void finalizar() {
        boolean flgPaso = true;
        long llFraccionamiento = 0;
        double ldcantidad = 0;
        double ldCantidadTotal = 0;

        try {
            ldcantidad = Double.parseDouble(txtCantidad.getText().toString());
        } catch (Exception e) {
            ldcantidad = 0;
        }

        if (ioBeanFraccionamiento != null)
            llFraccionamiento = ioBeanFraccionamiento.getNumerador();

        ldCantidadTotal = ldcantidad + llFraccionamiento;

        if (ldCantidadTotal <= 0) {
            showDialog(CONSICOVALIDAR);
            flgPaso = false;
            return;
        }
        if (TextUtils.isEmpty(spiMotivo.getText().toString())) {
            showDialog(CONSICOVALIDARMOT);
            flgPaso = false;
            return;
        }


        int empaques = (int) ldcantidad;
        double unidades = Double.parseDouble(Utilitario.fnRound((ldcantidad - empaques), iiMaximoNumeroDecimales));
        double cantidad = ldcantidad;
        double cantidadFrac = 0;

        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            if (ioBeanFraccionamiento != null)
                cantidadFrac = ioBeanFraccionamiento.getNumerador();

            if (iiMaximoNumeroDecimales > 0) {
                cantidad = empaques;
                BeanPresentacion bean = (BeanPresentacion) ioBeanArticulo;
                cantidadFrac = unidades * bean.getCantidad();

                if ((cantidadFrac % bean.getUnidadFraccionamiento()) != 0) {
                    showDialog(CONSVALIDARFRACC);
                    flgPaso = false;
                    return;
                }
            }
        }

        if (flgPaso) {

            loBeanPedidoDet = new BeanEnvItemDet();
            loBeanPedidoDet.setIdArticulo(ioBeanArticulo.getId());
            loBeanPedidoDet.setCodigoArticulo(ioBeanArticulo.getCodigo());
            loBeanPedidoDet.setCantidad(Utilitario.fnRound(cantidad,
                    isPresentacion.equals(Configuracion.FLGVERDADERO) ? 0 : iiMaximoNumeroDecimales));
            loBeanPedidoDet.setNombreArticulo(ioBeanArticulo.getNombre());
            loBeanPedidoDet.setFechaVencimiento(btnPickDate.getText()
                    .toString());
            BeanGeneral ioFormaPago = motivo;
            loBeanPedidoDet.setCodigoMotivo(ioFormaPago.getCodigo());
            loBeanPedidoDet.setNombreMotivo(ioFormaPago.getDescripcion());
            loBeanPedidoDet.setObservacion(txtObservacion.getText().toString());

            // Agrego o modifico el producto
            flujo = ((AplicacionEntel) getApplication()).getCurrentCodFlujo();


            if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                BeanPresentacion loBeanPresentacion = (BeanPresentacion) ioBeanArticulo;
                loBeanPedidoDet.setTipoArticulo("PRE");
                loBeanPedidoDet.setCantidadFrac(String.valueOf((int) cantidadFrac));
                loBeanPedidoDet.setCantidadPre(String.valueOf(loBeanPresentacion.getCantidad()));
                loBeanPedidoDet.setProducto(loBeanPresentacion.getProducto());
            }

            if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO)) {
                loBeanPedidoDet.setCodAlmacen(String.valueOf(ioBeanAlmacen.getPk()));
            }

            if (editando) {
                if (flujo == Configuracion.CONSCANJE) {
                    BeanEnvCanjeCab canje = ((AplicacionEntel) getApplication())
                            .getCurrentCanje();
                    canje.actualizarProductoCanje(loBeanPedidoDet);
                    ((AplicacionEntel) getApplication()).setCurrentCanje(canje);
                } else {
                    BeanEnvDevolucionCab devolucion = ((AplicacionEntel) getApplication())
                            .getCurrentDevolucion();
                    devolucion.actualizarProductoDevolucion(loBeanPedidoDet);
                    ((AplicacionEntel) getApplication())
                            .setCurrentDevolucion(devolucion);
                }
            } else {
                if (flujo == Configuracion.CONSCANJE) {
                    BeanEnvCanjeCab canje = ((AplicacionEntel) getApplication())
                            .getCurrentCanje();
                    canje.agregarProductoCanje(loBeanPedidoDet);
                    ((AplicacionEntel) getApplication()).setCurrentCanje(canje);
                } else {
                    BeanEnvDevolucionCab devolucion = ((AplicacionEntel) getApplication())
                            .getCurrentDevolucion();
                    devolucion.agregarProductoDevolucion(loBeanPedidoDet);
                    ((AplicacionEntel) getApplication())
                            .setCurrentDevolucion(devolucion);

                }
            }


            Intent loInstent = new Intent(this, ActProductoItemLista.class);
            finish();
            startActivity(loInstent);
        }
    }

    private double round(double d) {
        return (Math.floor(d * 100.0d + 0.5d) / 100.0d);
    }

    // the callback received when the user "sets" the date in the dialog
    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            subActFecha();
        }
    };

    // updates the date we display in the TextView
    private void subActFecha() {
        btnPickDate.setText(fnFecha(false));
    }

    private String fnFecha(boolean lbAutomatico) {
        String lsRetorno;
        Calendar calendar = Calendar.getInstance();
        Date loDate;
        if (!lbAutomatico) {
            if (selectDate == null) {
                calendar.set(Calendar.YEAR, mYear);
                calendar.set(Calendar.MONTH, mMonth);
                calendar.set(Calendar.DAY_OF_MONTH, mDay);
            } else
                calendar.setTime(selectDate);
        }
        loDate = calendar.getTime();
        lsRetorno = DateFormat.format("dd/MM/yyyy", loDate).toString();
        return lsRetorno;

    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent loIntentLogin = new Intent(ActProductoItemDetalle.this,
                        ActLogin.class);
                loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(loIntentLogin);
            }
        });

    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        Intent intentl = new Intent(this, ActMenu.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intentl);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = null;
            DalPedido loDalPedido = new DalPedido(this);
            loList = loDalPedido.fnEnvioPedidosPendientes(loBeanUsuario
                    .getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);
                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();
                showDialog(CONSDIASINCRONIZAR);
            } else {
                showDialog(VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub

        String lsBeanUsuario = getSharedPreferences(
                "Actual", MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper
                .fromJson(lsBeanUsuario,
                        BeanUsuario.class);
        this.setMsgOk("Eficiencia Online");

        BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
        loBeanListaEficienciaOnline.setIdResultado(0);
        loBeanListaEficienciaOnline.setResultado("");
        loBeanListaEficienciaOnline.setListaEficiencia(null);
        loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

        new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

        Intent loInstent = new Intent(this, ActKpiDetalle.class);
        loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loInstent);
    }

    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        ((AplicacionEntel) getApplication()).setCurrentCliente(null);
        ((AplicacionEntel) getApplication())
                .setCurrentCodFlujo(Configuracion.CONSSTOCK);
        ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
        Intent loIntento = new Intent(ActProductoItemDetalle.this,
                ActProductoListaOnline.class);
        loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        BeanExtras loBeanExtras = new BeanExtras();
        loBeanExtras.setFiltro("");
        loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
        String lsFiltro = "";
        try {
            lsFiltro = BeanMapper.toJson(loBeanExtras, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
        startActivity(loIntento);
    }

    @Override
    public void setOnItemClickPopup(Object poObject, int piTipo) {
        // TODO Auto-generated method stub

        switch (piTipo) {
            case POPUP_MOTIVO:
                motivo = (BeanGeneral) poObject;
                spiMotivo.setText(motivo.getDescripcion());
                showPopupMot = false;
                break;
            case POPUP_ALMACEN:
                ioBeanAlmacen = (BeanAlmacen) poObject;
                ioSpiAlmacen.setText(ioBeanAlmacen.getNombre());
                showPopupAlm = false;
                break;
            case POPUP_FRACCIONAMIENTO:
                showPopupFrac = false;
                ioBeanFraccionamiento = (BeanFraccionamiento) poObject;
                ioSpiFraccionamiento.setText(ioBeanFraccionamiento.toString());
                break;
            default:

                break;
        }

    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(
                    getString(R.string.ml_sincronizarantes)));
        }

    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */
    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        //showNexDialog(DIALOG_NSERVICES_DOWNLOAD);
                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }
}
