package pe.entel.android.pedidos.bean;

/**
 * Created by Dsb Mobile on 12/05/2015.
 */
public class BeanMostrarProducto {

    private String id;
    private String titulo;
    private String presentacion;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(String presentacion) {
        this.presentacion = presentacion;
    }
}
