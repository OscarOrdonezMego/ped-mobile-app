package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.adap.AdapPedidoClienteDireccion;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanClienteDireccion;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

public class ActPedidoClienteDireccionLista extends NexActivity implements
        OnItemClickListener, NexMenuCallbacks, MenuSlidingListener, OnClickListener {

    ListView lista;
    Button btnNuevo;

    private LinkedList<BeanClienteDireccion> ioListaClienteDireccion;
    BeanEnvPedidoCab loBeanPedidoCab;

    BeanUsuario loBeanUsuario = null;
    BeanCliente loBeanCliente = null;

    private final int CONSDIASINCRONIZAR = 1;
    private final int VALIDASINCRONIZACION = 2;
    private final int ENVIOPENDIENTES = 3;
    private final int VALIDASINCRO = 4;
    private final int NOHAYPENDIENTES = 5;

    private final int VALIDAHOME = 11;

    boolean estadoSincronizar = false;
    private AdapMenuPrincipalSliding _menu;

    // Integracion Ntrack
    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro

    private Configuracion.EnumFlujoDireccion ioFlujoDireccion;

    //PARAMETROS DE CONFIGURACION
    private String isMostrarCondVenta;
    private String isCrearDireccionPedido;
    private String isMostrarAlmacen;
    private String isMostrarCabeceraPedido;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();

        if (ioFlujoDireccion.equals(Configuracion.EnumFlujoDireccion.DESPACHO))
            onBackPressedNoVisitaOrDespacho(ActProductoPedido.class);
        else if (ioFlujoDireccion.equals(Configuracion.EnumFlujoDireccion.NO_VISITA)
                || ioFlujoDireccion.equals(Configuracion.EnumFlujoDireccion.PEDIDO))
            onBackPressedNoVisitaOrDespacho(ActClienteDetalle.class);

    }

    private void onBackPressedNoVisitaOrDespacho(Class poClass) {
        Intent intent = new Intent(this, poClass);
        intent.putExtra("IdCliente",
                loBeanCliente.getClipk());
        startActivity(intent);
        finish();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);
        ioFlujoDireccion = Configuracion.
                EnumFlujoDireccion.valueOf(
                getIntent().getStringExtra(
                        Configuracion.EXTRA_NAME_DIRECCION));

        String lsBeanUsuario = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario,
                BeanUsuario.class);


        String lsBeanPedidoCab = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "beanPedidoCab", "");
        loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper.fromJson(
                lsBeanPedidoCab, BeanEnvPedidoCab.class);


        String lsBeanClienteOnline = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanClienteOnline", "");
        loBeanCliente = (BeanCliente) BeanMapper.fromJson(lsBeanClienteOnline,
                BeanCliente.class);
        ((AplicacionEntel) getApplication()).setCurrentCliente(loBeanCliente);

        if (ioFlujoDireccion.equals(Configuracion.EnumFlujoDireccion.PEDIDO)) {
            isMostrarCondVenta = Configuracion.EnumConfiguracion
                    .MOSTRAR_CONDICION_VENTA.getValor(this);
            isCrearDireccionPedido = Configuracion.EnumConfiguracion
                    .CREAR_DIRECCION_PEDIDO.getValor(this);
            isMostrarAlmacen = Configuracion.EnumConfiguracion
                    .MOSTRAR_ALMACEN.getValor(this);
            isMostrarCabeceraPedido = Configuracion.EnumConfiguracion
                    .MOSTRAR_CABECERA_PEDIDO.getValor(this);
        }

        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);

        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));

        subIniMenu();
    }

    @Override
    protected void subIniActionBar() {

        this.getActionBarGD().setTitle(ioFlujoDireccion.getTitulo(this));
    }

    @Override
    protected void subSetControles() {
        // TODO Auto-generated method stub
        super.subSetControles();

        try {
            setActionBarContentView(R.layout.pedidoclientedireccion);
            lista = (ListView) findViewById(R.id.actpedidoclientedireccion_lista);
            btnNuevo = (Button) findViewById(R.id.actpedidoclientedireccion_btNuevo);

            lista.setOnItemClickListener(this);
            btnNuevo.setOnClickListener(this);

            btnNuevo.setVisibility(
                    ioFlujoDireccion.equals(Configuracion.EnumFlujoDireccion.PEDIDO)
                            && isCrearDireccionPedido.equals(Configuracion.FLGVERDADERO)
                            ? View.VISIBLE : View.GONE);

            ioListaClienteDireccion = new DalCliente(this)
                    .fnSelDirCliente(String.valueOf(loBeanCliente
                            .getClicod()), ioFlujoDireccion.getCodigo(this));

            BeanClienteDireccion bean = null;

            bean = new BeanClienteDireccion();
            bean.setPk("-1");
            bean.setNombre(loBeanCliente.getClidireccion() + " (DEFAULT)");
            bean.setFlgEnviado(Configuracion.FLGVERDADERO);

            ioListaClienteDireccion.add(0, bean);

            lista.setAdapter(new AdapPedidoClienteDireccion(this,
                    R.layout.pedidoclientedireccion_item,
                    ioListaClienteDireccion, ioFlujoDireccion));


        } catch (Exception e) {
            Log.e("subSetControles", e.toString());
            subShowException(e);
        }


    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {
        Log.i("subaccdesmensaje", "entrroo");
        if (estadoSincronizar)
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActMenu.class);

                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }

    }


    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub

        BeanClienteDireccion loDireccion = (BeanClienteDireccion) lista
                .getItemAtPosition(arg2);

        String codDireccion = "";
        String codigoDireccion = "";

        if (loDireccion.getFlgEnviado().equals(Configuracion.FLGFALSO)) {
            loBeanPedidoCab.setFlgNuevoDireccion(Configuracion.FLGVERDADERO);
            codDireccion = loDireccion.getCodInc();
            codigoDireccion = loDireccion.getCodInc();
        } else if (loDireccion.getPk().equals("-1")) {
            codDireccion = loBeanCliente.getClidireccion();
            codigoDireccion = loBeanCliente.getClidireccion();
        } else {
            codDireccion = loDireccion.getPk();
            codigoDireccion = loDireccion.getCodDireccion();
        }


        if (ioFlujoDireccion.equals(Configuracion.EnumFlujoDireccion.DESPACHO)) {
            loBeanPedidoCab.setCodDireccionDespacho(codDireccion);
            loBeanPedidoCab.setCodigoDireccionDespacho(codigoDireccion);
            itemClickIntent(ActPedidoFin.class, false);
        } else if (ioFlujoDireccion.equals(Configuracion.EnumFlujoDireccion.NO_VISITA)) {
            loBeanPedidoCab.setCodDireccion(codDireccion);
            loBeanPedidoCab.setCodigoDireccion(codigoDireccion);
            itemClickIntent(ActMotNoPedidoLista.class, false);
        } else if (ioFlujoDireccion.equals(Configuracion.EnumFlujoDireccion.PEDIDO)) {
            loBeanPedidoCab.setCodDireccion(codDireccion);
            loBeanPedidoCab.setCodigoDireccion(codigoDireccion);
            boolean blValidarFormulario = false;

            if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO)) {
                long llCodAlmacen = 0;
                try {
                    llCodAlmacen = Long.parseLong(loBeanPedidoCab.getCodAlmacen());
                } catch (Exception e) {
                    llCodAlmacen = 0;
                }
                if (llCodAlmacen == 0) {
                    itemClickIntent(ActPedidoAlmacen.class, false);
                } else {
                    blValidarFormulario = true;
                }
            } else {
                blValidarFormulario = true;
            }

            if (blValidarFormulario) {
                if (isMostrarCabeceraPedido.equals(Configuracion.FLGVERDADERO)) {
                    itemClickIntent(ActPedidoFormulario.class, true);
                } else if (isMostrarCondVenta.equals(Configuracion.FLGVERDADERO)) {
                    itemClickIntent(ActPedidoCondicionVenta.class, false);
                } else {
                    itemClickIntent(ActProductoPedido.class, false);
                }
            }
        }

    }

    private void itemClickIntent(Class poClass, boolean pbFormulario) {

        String lsBeanVisitaFinCab = "";

        try {
            lsBeanVisitaFinCab = BeanMapper.toJson(loBeanPedidoCab, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("beanPedidoCab", lsBeanVisitaFinCab);
        loEditor.commit();

        Intent loIntent = new Intent(ActPedidoClienteDireccionLista.this,
                poClass);
        if (pbFormulario)
            loIntent.putExtra(Configuracion.EXTRA_FORMULARIO,
                    Configuracion.FORMULARIO_INICIO);
        loIntent.putExtra("IdCliente", loBeanCliente.getClipk());
        finish();
        startActivity(loIntent);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(btnNuevo)) {
            startActivity(new Intent(ActPedidoClienteDireccionLista.this,
                    ActPedidoClienteGrabarDireccion.class));
            finish();
        }
    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent loIntentLogin = new Intent(
                        ActPedidoClienteDireccionLista.this, ActLogin.class);
                loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(loIntentLogin);
            }
        });

    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        showDialog(VALIDAHOME);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @SuppressWarnings("deprecation")
    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = null;
            DalPedido loDalPedido = new DalPedido(this);
            loList = loDalPedido.fnEnvioPedidosPendientes(loBeanUsuario
                    .getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);

                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();

                showDialog(CONSDIASINCRONIZAR);

            } else {
                showDialog(VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {

        String lsBeanUsuario = getSharedPreferences(
                "Actual", MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper
                .fromJson(lsBeanUsuario,
                        BeanUsuario.class);
        this.setMsgOk("Eficiencia Online");

        BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
        loBeanListaEficienciaOnline.setIdResultado(0);
        loBeanListaEficienciaOnline.setResultado("");
        loBeanListaEficienciaOnline.setListaEficiencia(null);
        loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

        new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

        Intent loInstent = new Intent(this, ActKpiDetalle.class);
        loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loInstent);
    }

    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        ((AplicacionEntel) getApplication()).setCurrentCliente(null);
        ((AplicacionEntel) getApplication())
                .setCurrentCodFlujo(Configuracion.CONSSTOCK);
        ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
        Intent loIntento = new Intent(ActPedidoClienteDireccionLista.this,
                ActProductoListaOnline.class);
        loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        BeanExtras loBeanExtras = new BeanExtras();
        loBeanExtras.setFiltro("");
        loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
        String lsFiltro = "";
        try {
            lsFiltro = BeanMapper.toJson(loBeanExtras, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
        startActivity(loIntento);
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        AlertDialog loAlertDialog = null;

        switch (id) {
            case VALIDAHOME:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        Intent intentl = new Intent(
                                                ActPedidoClienteDireccionLista.this,
                                                ActMenu.class);
                                        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                        startActivity(intentl);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;
            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences(
                                                    "Actual", MODE_PRIVATE)
                                                    .getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                                    .fromJson(lsBeanUsuario,
                                                            BeanUsuario.class);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(
                                                    loBeanUsuario.getCodVendedor(),
                                                    ActPedidoClienteDireccionLista.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        // .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(
                                getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActPedidoClienteDireccionLista.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            default:
                return super.onCreateDialog(id);
        }

    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(
                    getString(R.string.ml_sincronizarantes)));
        }

    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */
    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        //showNexDialog(DIALOG_NSERVICES_DOWNLOAD);
                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */
    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        try {
            DialogFragmentYesNo loDialog;
            switch (id) {
                case DIALOG_NSERVICES_DOWNLOAD:
                    String lsNServices;
                    if (ioDALNservices == null)
                        ioDALNservices = DALNServices.getInstance(this,
                                R.string.db_name);
                    try {
                        lsNServices = ioDALNservices.fnSelNServicesLabel();
                    } catch (Exception e) {
                        Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                e);
                        lsNServices = getString(R.string.nservices_label_default);
                    }
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG
                                    + DIALOG_NSERVICES_DOWNLOAD,
                            getString(R.string.nservices_consultingdownlad)
                                    .replace("{0}", lsNServices),
                            EnumLayoutResource.getDefaultIconHelp(this));
                    return new DialogFragmentYesNoPairListener(loDialog,
                            new OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(
                                        DialogFragmentYesNo dialog) {
                                    try {
                                        subIniNServicesDownload(ioDALNservices
                                                .fnSelNServicesAPK());
                                    } catch (Exception e) {
                                        subShowException(e);
                                    }
                                }

                                @Override
                                public void performButtonNo(
                                        DialogFragmentYesNo dialog) {
                                }
                            });

                default:
                    return super.onCreateNexDialog(id);
            }
        } catch (Exception e) {
            subShowException(e);
            return super.onCreateNexDialog(id);
        }
    }

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

}
