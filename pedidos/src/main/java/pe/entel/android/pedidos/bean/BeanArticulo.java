package pe.entel.android.pedidos.bean;

import android.text.TextUtils;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import pe.entel.android.pedidos.util.Configuracion;

/**
 * Created by Dsb Mobile on 13/05/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanArticulo {

    @JsonProperty
    private String id = "";
    @JsonProperty
    private String codigo = "";
    @JsonProperty
    private String nombre = "";
    @JsonProperty
    private String stock = "";
    @JsonProperty
    private String precioBase = "0";
    @JsonProperty
    private String precioBaseSoles = "0"; //@JBELVY
    @JsonProperty
    private String precioBaseDolares = "0"; //@JBELVY
    @JsonProperty
    private String tipoArticulo = "";

    private BeanAlmacen almacen;

    private String descuentoMinimo = "0";
    private String descuentoMaximo = "0";
    private String codAlmacen = "0";


    public String getTitulo() {
        return this.codigo + " - " + this.nombre + " - " + this.stock;
    }

    public BeanArticulo() {

    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return this.id + " - " + this.nombre + " : " + this.stock;
    }

    public BeanArticulo(String psTipoArticulo) {
        this.tipoArticulo = psTipoArticulo;
    }

    public BeanArticuloOnline fnObtenerBeanArticuloOnline() {
        BeanArticuloOnline loBeanArticuloOnline = new BeanArticuloOnline();

        loBeanArticuloOnline.setId(this.id);
        loBeanArticuloOnline.setCodigo(this.codigo);
        if (TextUtils.isEmpty(this.codigo))
            loBeanArticuloOnline.setNombre(this.nombre);
        loBeanArticuloOnline.setCodAlmacen(this.codAlmacen);
        loBeanArticuloOnline.setTipoArticulo(this.tipoArticulo);

        return loBeanArticuloOnline;
    }

    public BeanArticulo fnObtenerBeanArticulo(BeanArticuloOnline loBeanArticuloOnline) {
        BeanArticulo loBeanArticulo = null;
        if (loBeanArticuloOnline.getTipoArticulo().equals(Configuracion.TipoArticulo.PRODUCTO)) {
            loBeanArticulo = new BeanProducto();
        } else {
            loBeanArticulo = new BeanPresentacion();
        }
        loBeanArticulo.setCodigo(loBeanArticuloOnline.getCodigo());
        loBeanArticulo.setNombre(loBeanArticuloOnline.getNombre());
        loBeanArticulo.setStock(loBeanArticuloOnline.getStock());
        loBeanArticulo.setCodAlmacen(loBeanArticuloOnline.getCodAlmacen());


        return loBeanArticulo;
    }


    public BeanPrecio fnBeanPrecio() {
        BeanPrecio beanPrecio = new BeanPrecio();
        beanPrecio.setPrecio(this.precioBase);
        beanPrecio.setPrecioSoles(this.precioBaseSoles); //@JBELVY
        beanPrecio.setPrecioDolares(this.precioBaseDolares); //@JBELVY
        beanPrecio.setDescuentoMinimo(this.descuentoMinimo);
        beanPrecio.setDescuentoMaximo(this.descuentoMaximo);
        return beanPrecio;
    }

    public String fnMostrarStock() {
        return "";
    }


    public String getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getPrecioBase() {
        return precioBase;
    }

    public String getPrecioBaseSoles() {
        return precioBaseSoles;
    } //@JBELVY

    public String getPrecioBaseDolares() {
        return precioBaseDolares;
    } //@JBELVY

    public void setPrecioBase(String precioBase) {
        this.precioBase = precioBase;
    }

    public void setPrecioBaseSoles(String precioBaseSoles) {
        this.precioBaseSoles = precioBaseSoles;
    } //@JBELVY

    public void setPrecioBaseDolares(String precioBaseDolares) {
        this.precioBaseDolares = precioBaseDolares;
    } //@JBELVY

    public String getTipoArticulo() {
        return tipoArticulo;
    }

    public void setTipoArticulo(String tipoArticulo) {
        this.tipoArticulo = tipoArticulo;
    }

    public String getDescuentoMinimo() {
        return descuentoMinimo;
    }

    public void setDescuentoMinimo(String descuentoMinimo) {
        this.descuentoMinimo = descuentoMinimo;
    }

    public String getDescuentoMaximo() {
        return descuentoMaximo;
    }

    public void setDescuentoMaximo(String descuentoMaximo) {
        this.descuentoMaximo = descuentoMaximo;
    }

    public String getCodAlmacen() {
        return codAlmacen;
    }

    public void setCodAlmacen(String codAlmacen) {
        this.codAlmacen = codAlmacen;
    }

    public BeanAlmacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(BeanAlmacen almacen) {
        this.almacen = almacen;
    }
}
