package pe.entel.android.pedidos.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanClienteDireccion;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumUrl;
import pe.entel.android.pedidos.util.UtilitarioData;

public class HttpGrabaDireccion extends HttpConexion {

    private BeanClienteDireccion ioBeanClienteDireccion;
    private Context ioContext;

    public HttpGrabaDireccion(BeanClienteDireccion poBeanClienteDireccion, Context poContext, EnumTipActividad poEnumTipActividad) {
        super(poContext, poContext.getString(R.string.httpgrabarpedido_dlg), poEnumTipActividad);
        ioBeanClienteDireccion = poBeanClienteDireccion;
        ioContext = poContext;
    }

    @Override
    public boolean OnPreConnect() {
        SharedPreferences loSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ioContext);
        if (loSharedPreferences.getBoolean(ioContext.getString(R.string.keypre_cobertura), false)) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_modofueracobertura));
            return false;
        }
        int signal = UtilitarioData.fnSignalLevel(ioContext);
        if (!Utilitario.fnVerSignal(ioContext) || signal < 2) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            String lsResultado = (String) getHttpResponseObject();
            String[] laRespuesta = lsResultado.split(";");
            int liIdHttpRespuesta = Integer.valueOf(laRespuesta[0]);
            String lsHttpRespuesta = laRespuesta[1];
            setHttpResponseIdMessage(liIdHttpRespuesta, lsHttpRespuesta);
        }
    }

    public void subConHttp() {
        try {
            //RestTemplate restTemplate = new RestTemplate();
            //restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            Log.v("XXX", Configuracion.fnUrl(ioContext, EnumUrl.GRABARDIRECCION));
            Log.v("XXX", BeanMapper.toJson(ioBeanClienteDireccion, false));
            //String lsResultado = restTemplate.postForObject(Configuracion.fnUrl(ioContext, EnumUrl.GRABARDIRECCION), ioBeanClienteDireccion, String.class);
            String lsResultado = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, EnumUrl.GRABARDIRECCION), ioBeanClienteDireccion);
            Log.v("XXX", lsResultado);

            BeanClienteDireccion loResponseBeanClienteDireccion = (BeanClienteDireccion) BeanMapper.fromJson(lsResultado, BeanClienteDireccion.class);

            if (loResponseBeanClienteDireccion.getError() == ConfiguracionNextel.CONSRESSERVIDOROK) {

                DalCliente loDalCliente = new DalCliente(ioContext);
                loDalCliente.fnUpdEnviadoDireccion(
                        loResponseBeanClienteDireccion.getPk(),
                        loResponseBeanClienteDireccion.getCodDireccion(),
                        ioBeanClienteDireccion.getCodCliente(),
                        ioBeanClienteDireccion.getFechaCreacion()
                );
            }

            setHttpResponseObject(loResponseBeanClienteDireccion.getHtttpResponseObject());

        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, "Error HTTP: " + e.getMessage());
        }
    }
}