package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.widget.HeadedTextItem;

public class AdapVendedorBuscar extends ArrayAdapter<HeadedTextItem> {
    int resource;
    Context context;
    TextView rowTextView;
    VendedorFilter filter;

    ArrayList<HeadedTextItem> originalList;
    ArrayList<HeadedTextItem> filtroList;
    int cont = 0;

    public Filter getFilter(int tipo) {
        if (filter == null)
            filter = new VendedorFilter(tipo);
        else
            filter.setTipo(tipo);

        return filter;
    }

    public AdapVendedorBuscar(Context _context, int _resource,
                              ArrayList<HeadedTextItem> lista) {
        super(_context, _resource, lista);
        filtroList = new ArrayList<HeadedTextItem>();
        filtroList.addAll(lista);
        originalList = new ArrayList<HeadedTextItem>();
        originalList.addAll(lista);
        resource = _resource;
        context = _context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        HeadedTextItem item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        ((TextView) nuevaVista
                .findViewById(R.id.actvendedorbuscaritem_textTitulo))
                .setText(item.getText());

        return nuevaVista;
    }

    private class VendedorFilter extends Filter {

        int tipo;

        public VendedorFilter(int tipo) {
            this.tipo = tipo;
        }

        public void setTipo(int tipo) {
            this.tipo = tipo;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // TODO Auto-generated method stub

            constraint = constraint.toString().toUpperCase();
            Log.i("constraint", (String) constraint);
            Log.i("tipo", String.valueOf(tipo));
            FilterResults result = new FilterResults();
            if (constraint != null && constraint.toString().length() > 0) {
                ArrayList<HeadedTextItem> filteredItems = new ArrayList<HeadedTextItem>();

                String texto = "";
                for (int i = 0, l = originalList.size(); i < l; i++) {
                    String[] string = originalList.get(i).getText().split("\\|");
                    texto = string[tipo];

                    if (texto.contains(constraint.toString())) {
                        filteredItems.add(originalList.get(i));
                    }
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            } else {
                synchronized (this) {
                    result.values = originalList;
                    result.count = originalList.size();
                }
            }
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            // TODO Auto-generated method stub
            filtroList = (ArrayList<HeadedTextItem>) results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0, l = filtroList.size(); i < l; i++) {
                add(filtroList.get(i));
                notifyDataSetInvalidated();
            }
        }
    }
}
