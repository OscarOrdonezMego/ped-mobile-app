package pe.entel.android.pedidos.act;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;

public class ErrorActivity extends Activity {

    public TextView link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);
        link = (TextView) findViewById(R.id.customactivityoncrash_error_link);
        link.setText(Html.fromHtml("<a href=\"" + Utilitario.readProperties(this).getProperty("PORTAL_AUTOAYUDA") + "\">" + getResources().getString(R.string.customactivityoncrash_error_activity_error_link) + "</a>"));
        link.setClickable(true);
        link.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
