package pe.entel.android.pedidos.util;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import pe.com.nextel.android.util.Logger;

public class OkHttpHelper {
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private OkHttpClient client;

    OkHttpHelper() {
        client = new OkHttpClient();
    }

    public String postJson(Context context, Object object, String url) throws IOException {
        Logger.d("XXX", "OkHttpHelper > postJson");

        final boolean[] envioConforme = {false};
        Component component = new Component();

        if (!new Hardware(context).isConnected()) {
            component.toastCustomHandlerInfo(context, "No hay cobertura de datos");
            return null;
        }

        Gson gson = new Gson();

        Log.d("XXX", "OkHttpHelper > postJson -> url : " + url);
        Log.d("XXX", "OkHttpHelper > postJson -> json : " + gson.toJson(object));

        RequestBody body = RequestBody.create(JSON, gson.toJson(object));
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful())
            throw new IOException("Unexpected code " + response);
        String respuesta = response.body().string();
        Log.d("XXX", "OkHttpHelper > postJson -> respuesta : " + respuesta);
        return respuesta;
    }

    public boolean postJsonOkHttp(Context context, Object object, String url) throws IOException {
        Logger.d("XXX", "OkHttpHelper->postJsonOkHttp");

        final boolean[] envioConforme = {false};
        Component component = new Component();

        if (!new Hardware(context).isConnected()) {
            component.toastCustomHandlerInfo(context, "No hay cobertura de datos");
            return false;
        }

        Gson gson = new Gson();

        Log.d("XXX", "postJsonOkHttp -> url : " + url);
        Log.d("XXX", "postJsonOkHttp -> json : " + gson.toJson(object));

        RequestBody body = RequestBody.create(JSON, gson.toJson(object));
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        if (!response.isSuccessful())
            throw new IOException("Unexpected code " + response);
        String respuesta = response.body().string();
        Log.d("XXX", "postJsonOkHttp -> respuesta : " + respuesta);
        String[] arrayResponse = respuesta.split(";");
        if ("1".equals(arrayResponse[0])) {
            envioConforme[0] = true;
        } else {
            FabricLibrary.CrashlyticsLog(context, respuesta);
            component.toastCustomHandlerInfo(context, "Error del Servidor");
            envioConforme[0] = false;
        }
        return envioConforme[0];
    }

    private String post(String url, String data) throws IOException {
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, data);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
