package pe.entel.android.pedidos.http;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpHelper {

    private final String MediaTypeJSON = "application/json; charset=utf-8";

    private OkHttpClient client;
    private Gson gson;
    private MediaType JSON;

    public HttpHelper() {
        client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
        gson = new Gson();
        JSON = MediaType.parse(MediaTypeJSON);
    }

    public <TJsonObject> String postJson(String url, TJsonObject jsonObject) throws IOException {
        RequestBody body = RequestBody.create(JSON, gson.toJson(jsonObject));
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        String resStr = null;
        if (response.body() != null) {
            resStr = response.body().string();
        }

        return resStr;
    }

    public InputStream getResponseFile(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Response response = client.newCall(request).execute();
        InputStream resStr = null;
        if (response.body() != null)
            resStr = response.body().byteStream();

        return resStr;
    }

    public <TJsonObject, TJsonResponse> TJsonResponse sendPostJson(String url, TJsonObject jsonObject) throws IOException {
        Type typeOfList = new TypeToken<TJsonResponse>() {
        }.getType();
        RequestBody body = RequestBody.create(JSON, gson.toJson(jsonObject));
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        String resStr = null;
        if (response.body() != null) {
            resStr = response.body().string();
        }

        return gson.fromJson(resStr, typeOfList);
    }

    public <TJsonObject> String getJson(String url, TJsonObject jsonObject) throws IOException {
        RequestBody body = RequestBody.create(JSON, gson.toJson(jsonObject));
        Request request = new Request.Builder()
                .url(url)
                .method("GET", body)
                .build();
        Response response = client.newCall(request).execute();
        String resStr = null;
        if (response.body() != null) {
            resStr = response.body().string();
        }

        return resStr;
    }

    public <TJsonObject> Call postJsonAsync(String url, TJsonObject jsonObject, Callback callback) {
        RequestBody body = RequestBody.create(JSON, gson.toJson(jsonObject));
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
        return call;
    }
}
