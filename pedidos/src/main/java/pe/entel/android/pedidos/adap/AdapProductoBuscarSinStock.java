package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanMostrarProducto;
import pe.entel.android.pedidos.util.Configuracion;

public class AdapProductoBuscarSinStock extends ArrayAdapter<BeanMostrarProducto> {
    int resource;
    Context context;
    TextView rowTextView;
    ClienteFilter filter;

    List<BeanMostrarProducto> originalList;
    List<BeanMostrarProducto> filtroList;

    String isPresentacion;

    public Filter getFilter(int tipo) {
        if (filter == null)
            filter = new ClienteFilter(tipo);
        else
            filter.setTipo(tipo);

        return filter;
    }

    public AdapProductoBuscarSinStock(Context _context, int _resource,
                                      List<BeanMostrarProducto> lista, String psPresentacion) {
        super(_context, _resource, lista);
        filtroList = new ArrayList<BeanMostrarProducto>();
        filtroList.addAll(lista);
        originalList = new ArrayList<BeanMostrarProducto>();
        originalList.addAll(lista);
        resource = _resource;
        context = _context;
        isPresentacion = psPresentacion;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BeanMostrarProducto item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        ((TextView) nuevaVista
                .findViewById(R.id.actproductobuscaritem_textTitulo))
                .setText(item.getTitulo());

        TextView txtPresentacion = (TextView) nuevaVista.findViewById(R.id.actproductobuscaritem_textPresentacion);

        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            txtPresentacion.setVisibility(View.VISIBLE);
            txtPresentacion.setText(getContext().getString(R.string.actproductobuscar_pres)
                    + " " + item.getPresentacion());
        } else {
            txtPresentacion.setVisibility(View.GONE);
        }


        return nuevaVista;
    }

    private class ClienteFilter extends Filter {

        int tipo;

        public ClienteFilter(int tipo) {
            this.tipo = tipo;
        }

        public void setTipo(int tipo) {
            this.tipo = tipo;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // TODO Auto-generated method stub

            constraint = constraint.toString().toUpperCase();
            Log.i("constraint", (String) constraint);
            Log.i("tipo", String.valueOf(tipo));
            FilterResults result = new FilterResults();
            String cadenaComparar = constraint.toString().trim();

            if (constraint != null && constraint.toString().length() > 0) {
                List<BeanMostrarProducto> loLstFiltroBeanMostrarProducto = new ArrayList<BeanMostrarProducto>();
                BeanMostrarProducto loBeanMostrarProducto = new BeanMostrarProducto();
                boolean Existe = false;

                for (int i = 0, l = originalList.size(); i < l; i++) {
                    String elemento = originalList.get(i).getTitulo().toUpperCase();
                    StringTokenizer tokens = new StringTokenizer(elemento, "\\-");
                    String codigo = "", titulo = "", precio = "";

                    String tokenBusqueda = "";

                    int cont = 0;
                    while (tokens.hasMoreTokens()) {
                        if (cont == 0) {
                            codigo = tokens.nextToken();
                        } else if (tokens.countTokens() == 1) {
                            titulo = tokens.nextToken();
                        } else {
                            titulo = tokens.nextToken();
                        }
                        cont++;
                    }

                    switch (tipo) {
                        case 0:
                            tokenBusqueda = codigo;
                            break;
                        case 1:
                            tokenBusqueda = titulo;
                            break;
                        case 2:
                            tokenBusqueda = precio;
                            break;
                    }

                    if (cadenaComparar.contains(" ") && !(cadenaComparar.endsWith(" "))) {
                        String[] listaTags = cadenaComparar.split(" ");
                        String expRegBusqueda = "";

                        for (int x = 0; x < listaTags.length; x++) {
                            expRegBusqueda += "(" + listaTags[x].toString() + ")+([\\-\\+\\(\\)\\:/Ñña-zA-Z0-9 .])+";
                        }

                        Log.i("expRegBusqueda", expRegBusqueda);

                        if (tokenBusqueda.trim().matches(expRegBusqueda)) {

                            if (tokenBusqueda.trim().equals(constraint.toString())) {
                                loBeanMostrarProducto = originalList.get(i);
                                Existe = true;
                            } else {
                                loLstFiltroBeanMostrarProducto.add(originalList.get(i));
                            }
                        }
                    } else {
                        if (tokenBusqueda.trim().indexOf(constraint.toString()) != -1) {

                            if (tokenBusqueda.trim().equals(constraint.toString())) {
                                loBeanMostrarProducto = originalList.get(i);
                                Existe = true;
                            } else {
                                loLstFiltroBeanMostrarProducto.add(originalList.get(i));
                            }
                        }
                    }

                }

                final CharSequence finalConstraint = constraint;
                Collections.sort(loLstFiltroBeanMostrarProducto, new Comparator<BeanMostrarProducto>() {
                    @Override
                    public int compare(BeanMostrarProducto poBeanMostrarProducto1, BeanMostrarProducto poBeanMostrarProducto2) {
                        String cadenaComparar1 = poBeanMostrarProducto1.getTitulo().split("-")[tipo].toUpperCase().trim();
                        String cadenaComparar2 = poBeanMostrarProducto2.getTitulo().split("-")[tipo].toUpperCase().trim();
                        return cadenaComparar1.compareTo(cadenaComparar2.toString());
                    }
                });

                if (Existe) {
                    loLstFiltroBeanMostrarProducto.add(0, loBeanMostrarProducto);
                }
                result.count = loLstFiltroBeanMostrarProducto.size();
                result.values = loLstFiltroBeanMostrarProducto;
            } else {
                synchronized (this) {
                    result.values = originalList;
                    result.count = originalList.size();
                }
            }
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            // TODO Auto-generated method stub
            filtroList = (List<BeanMostrarProducto>) results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0, l = filtroList.size(); i < l; i++) {
                add(filtroList.get(i));
                notifyDataSetInvalidated();
            }
        }

    }

}
