package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import greendroid.widget.ActionBarItem;
import greendroid.widget.ActionBarItem.Type;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.bean.BeanEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanKPI;
import pe.entel.android.pedidos.bean.BeanRespListaEfecienciaOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

public class ActKpiDetalleSupervisor extends NexActivity implements NexMenuCallbacks,
        MenuSlidingListener {

    private Button btninfovendedores;
    private AdapMenuPrincipalSliding _menu;

    BeanUsuario loBeanUsuario = null;

    private final int CONSDIASINCRONIZAR = 1;
    private final int VALIDASINCRONIZACION = 2;
    private final int ENVIOPENDIENTES = 3;
    private final int VALIDASINCRO = 4;
    private final int NOHAYPENDIENTES = 5;

    private final int VALIDAR_GRAFICO = 10;

    boolean estadoSincronizar = false;

    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro

    //PARAMETROS DE CONFIGURACION
    private String isNumDecVista;

    private LinearLayout layout;

    private BeanRespListaEfecienciaOnline loListakpi;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        String lsBeanUsuario = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario,
                BeanUsuario.class);

        isNumDecVista = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(this);

        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);
        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));

        subIniMenu();
        //
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        Intent intentl = new Intent(this, ActMenu.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intentl);

    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setTitle(getString(R.string.actkpi_bartitulo));
        addActionBarItem(Type.Nex_Chart, Configuracion.CONSPEDIDO);

    }

    @Override
    public void subSetControles() {
        setActionBarContentView(R.layout.kpidetalle2supervisor);
        super.subSetControles();
        layout = (LinearLayout) findViewById(R.id.kpidetallesupervisor_layout);
        subCargaDetalle();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {
        Log.i("subaccdesmensaje", "entrroo");
        if (estadoSincronizar)
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActMenu.class);

                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }

    }

    private void subCargaDetalle() {

        BeanKPI loBeanKpi = new BeanKPI();
        String lsBeanRespListaEfecienciaOnline = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("BeanRespListaEfecienciaOnlineSupervisor", "");
        loListakpi = (BeanRespListaEfecienciaOnline) BeanMapper.fromJson(lsBeanRespListaEfecienciaOnline, BeanRespListaEfecienciaOnline.class);
        ((AplicacionEntel) getApplication()).setCurrentRespListaEfecienciaOnline(loListakpi);
        List<BeanEficienciaOnline> lista = loListakpi.getListaEficiencia();

        for (int i = 0; i < lista.size(); i++) {
            BeanEficienciaOnline bean = lista.get(i);
            layout.addView(agregarItem(bean.getDescripcion(), bean.getValor()));
        }
    }

    private LinearLayout agregarItem(String pdescripcion, String pvalor) {
        LinearLayout contenedor = new LinearLayout(this);
        contenedor.setPadding(2, 2, 2, 2);
        contenedor.setOrientation(LinearLayout.VERTICAL);
        if (pdescripcion.equals("") && pvalor.equals("")) {
            View v = new View(this);
            v.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, 2));
            v.setBackgroundColor(getResources().getColor(R.color.TextColorGray));
            contenedor.addView(v);
        } else {
            LinearLayout item = new LinearLayout(this);
            item.setPadding(2, 2, 2, 2);
            item.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT, 1f);
            LinearLayout left = new LinearLayout(this);
            left.setLayoutParams(params);
            left.setGravity(Gravity.LEFT);
            TextView descripcion = new TextView(this);
            if (pvalor.equals(("")))
                descripcion.setTextColor(getResources().getColor(R.color.EnThemeBlueLight));
            else descripcion.setTextColor(getResources().getColor(R.color.TextColorOrange));
            descripcion.setText(pdescripcion);
            left.addView(descripcion);

            LinearLayout right = new LinearLayout(this);
            right.setLayoutParams(params);
            right.setGravity(Gravity.RIGHT);
            TextView valor = new TextView(this);
            valor.setTextColor(getResources().getColor(R.color.TextColorGray));
            valor.setText(pvalor);
            right.addView(valor);

            item.addView(left);
            item.addView(right);

            contenedor.addView(item);

            View v = new View(this);
            v.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, 1));
            v.setBackgroundColor(getResources().getColor(R.color.TextColorGray));
            contenedor.addView(v);

        }
        return contenedor;
    }


    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        ((AplicacionEntel) getApplication()).setCurrentCodFlujo(item.getItemId());
        if (item.getContentDescription().toString().equals(getString(R.string.gd_nex_chart))) {

            Intent loInstent = new Intent(ActKpiDetalleSupervisor.this, ActPieChart.class);
            List lovalores = new ArrayList<String>();

            BeanKPI kpi = loListakpi.getKpi();
            int totalTransacciones = Integer.parseInt(kpi.numPedidos) +
                    Integer.parseInt(kpi.numCobranzas) +
                    Integer.parseInt(kpi.numNoPedidos) +
                    Integer.parseInt(kpi.numCanjes) +
                    Integer.parseInt(kpi.numDevoluciones);

            lovalores.add(kpi.numPedidos);
            lovalores.add(kpi.numCobranzas);
            lovalores.add(kpi.numNoPedidos);
            lovalores.add(kpi.numCanjes);
            lovalores.add(kpi.numDevoluciones);

            loInstent.putStringArrayListExtra("valores", (ArrayList<String>) lovalores);
            loInstent.putExtra("numCli", totalTransacciones);

            if (totalTransacciones == 0)
                showDialog(VALIDAR_GRAFICO);
            else
                startActivity(loInstent);
        } else {
            return super.onHandleActionBarItemClick(item, position);
        }
        return true;
    }


    private void subIniMenu() {
        btninfovendedores = (Button) findViewById(R.id.btn_infovendedores);
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent loIntentLogin = new Intent(ActKpiDetalleSupervisor.this,
                        ActLogin.class);
                loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(loIntentLogin);
            }
        });
        btninfovendedores.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ActKpiDetalleSupervisor.this, ActVendedorBuscar.class);
                startActivity(intent);
            }
        });
    }


    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    EnumMenuPrincipalSlidingItem.EFICIENCIA.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        Intent intentl = new Intent(this, ActMenu.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intentl);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub
        DalCliente loDalCliente = new DalCliente(this);

        // Verificamos si existe la tabla.
        boolean lbResult = loDalCliente.existeTable();

        if (lbResult) {
            Intent loInstent = new Intent(ActKpiDetalleSupervisor.this,
                    ActClienteBuscar.class);
            loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            BeanExtras loBeanExtras = new BeanExtras();
            loBeanExtras.setFiltro("");
            loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
            String lsFiltro = "";
            try {
                lsFiltro = BeanMapper.toJson(loBeanExtras, false);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            loInstent.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
            finish();
            startActivity(loInstent);
        } else {

            showDialog(VALIDASINCRONIZACION);

        }
    }

    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = null;
            DalPedido loDalPedido = new DalPedido(this);
            loList = loDalPedido.fnEnvioPedidosPendientes(loBeanUsuario
                    .getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);

                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();


                showDialog(CONSDIASINCRONIZAR);

            } else {

                showDialog(VALIDASINCRO);

            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub

    }

    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub

        Intent loIntento = new Intent(ActKpiDetalleSupervisor.this,
                ActProductoListaOnline.class);
        loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(loIntento);
        finish();
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {

            subShowException(new Exception(getString(R.string.ml_sincronizarantes)));
        }

    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK,
                R.style.Theme_Pedidos_Master);
    }


    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */

    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {

                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */
    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        try {
            DialogFragmentYesNo loDialog;
            switch (id) {
                case DIALOG_NSERVICES_DOWNLOAD:
                    String lsNServices;
                    if (ioDALNservices == null)
                        ioDALNservices = DALNServices.getInstance(this,
                                R.string.db_name);
                    try {
                        lsNServices = ioDALNservices.fnSelNServicesLabel();
                    } catch (Exception e) {
                        Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                e);
                        lsNServices = getString(R.string.nservices_label_default);
                    }
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG
                                    + DIALOG_NSERVICES_DOWNLOAD,
                            getString(R.string.nservices_consultingdownlad)
                                    .replace("{0}", lsNServices),
                            EnumLayoutResource.getDefaultIconHelp(this));
                    return new DialogFragmentYesNoPairListener(loDialog,
                            new OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(
                                        DialogFragmentYesNo dialog) {
                                    try {
                                        subIniNServicesDownload(ioDALNservices
                                                .fnSelNServicesAPK());
                                    } catch (Exception e) {
                                        subShowException(e);
                                    }
                                }

                                @Override
                                public void performButtonNo(
                                        DialogFragmentYesNo dialog) {
                                }
                            });

                default:
                    return super.onCreateNexDialog(id);
            }
        } catch (Exception e) {
            subShowException(e);
            return super.onCreateNexDialog(id);
        }
    }

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        AlertDialog loAlertDialog = null;

        switch (id) {
            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences(
                                                    "Actual", MODE_PRIVATE)
                                                    .getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                                    .fromJson(lsBeanUsuario,
                                                            BeanUsuario.class);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActKpiDetalleSupervisor.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActKpiDetalleSupervisor.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDAR_GRAFICO:

                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actkpi_bartitulo))
                        .setMessage(getString(R.string.actkpidetalle_errorgrafico))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            default:
                return super.onCreateDialog(id);
        }

    }

}