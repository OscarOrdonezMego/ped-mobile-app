package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanEnvDocumento {
    private String idDocTipo;
    @JsonProperty
    private String idDocTipoMotivo;
    @JsonProperty
    private String fechaMovil;
    @JsonProperty
    private String idUsuario;
    @JsonProperty
    private String idCliente;
    @JsonProperty
    private String latitud;
    @JsonProperty
    private String longitud;
    @JsonProperty
    private String serie;
    @JsonProperty
    private String correlativo;
    @JsonProperty
    private String rangocorrelativo;
    @JsonProperty
    private String idPago;
    @JsonProperty
    private String montosoles;
    @JsonProperty
    private String montodolares;
    @JsonProperty
    private String cliente;
    @JsonProperty
    private String fechapago;

    private BeanPagoOnline pagoOnline;

    public BeanEnvDocumento() {
        idDocTipo = "";
        idDocTipoMotivo = "";
        fechaMovil = "";
        idUsuario = "";
        idCliente = "";
        latitud = "0";
        longitud = "0";
        serie = "";
        correlativo = "";
        rangocorrelativo = "0";
        idPago = "";
        montosoles = "";
        montodolares = "";
    }

    public String getIdDocTipo() {
        return idDocTipo;
    }

    public void setIdDocTipo(String idDocTipo) {
        this.idDocTipo = idDocTipo;
    }

    public String getIdDocTipoMotivo() {
        return idDocTipoMotivo;
    }

    public void setIdDocTipoMotivo(String idDocTipoMotivo) {
        this.idDocTipoMotivo = idDocTipoMotivo;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getFechaMovil() {
        return fechaMovil;
    }

    public void setFechaMovil(String fechaMovil) {
        this.fechaMovil = fechaMovil;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getCorrelativo() {
        return correlativo;
    }

    public void setCorrelativo(String correlativo) {
        this.correlativo = correlativo;
    }

    public String getIdPago() {
        return idPago;
    }

    public void setIdPago(String idPago) {
        this.idPago = idPago;
    }

    public String getMontoSoles() {
        return montosoles;
    }

    public void setMontoSoles(String montosoles) {
        this.montosoles = montosoles;
    }

    public String getMontoDolares() {
        return montodolares;
    }

    public void setMontoDolares(String montodolares) {
        this.montodolares = montodolares;
    }

    public String getRangocorrelativo() {
        return rangocorrelativo;
    }

    public void setRangocorrelativo(String rangocorrelativo) {
        this.rangocorrelativo = rangocorrelativo;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getFechaPago() {
        return fechapago;
    }

    public void setFechaPago(String fechapago) {
        this.fechapago = fechapago;
    }

    public BeanPagoOnline getPagoOnline() {
        return pagoOnline;
    }

    public void setPagoOnline(BeanPagoOnline pagoOnline) {
        this.pagoOnline = pagoOnline;
    }

}
