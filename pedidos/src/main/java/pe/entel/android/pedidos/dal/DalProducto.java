package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.util.Pair;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import greendroid.widget.item.DrawableItem;
import greendroid.widget.item.Item;
import greendroid.widget.item.SeparatorItem;
import greendroid.widget.item.SubtitleItem;
import greendroid.widget.item.ThumbnailItem;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanEnvCanjeCab;
import pe.entel.android.pedidos.bean.BeanEnvDevolucionCab;
import pe.entel.android.pedidos.bean.BeanEnvItemDet;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanEnvPedidoDet;
import pe.entel.android.pedidos.bean.BeanMostrarProducto;
import pe.entel.android.pedidos.bean.BeanPrecio;
import pe.entel.android.pedidos.bean.BeanPresentacion;
import pe.entel.android.pedidos.bean.BeanProducto;
import pe.entel.android.pedidos.util.Configuracion;

public class DalProducto {
    private Context ioContext;

    public DalProducto(Context psClase) {

        ioContext = psClase;
    }

    public BeanPresentacion fnSelxIdPresentacion(String psIdPresentacion) {
        BeanPresentacion loBeanPresentacion = new BeanPresentacion();
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"IdPresentacion", "IdProducto", "Nombre", "Stock",
                "Cantidad", "Precio", "DescuentoMinimo", "DescuentoMaximo", "UnidadFraccionamiento", "Codigo",
                "PrecioSoles", "PrecioDolares" }; //@JBELVY
        String lsTablas = "TBL_PRESENTACION";
        String lsWhere = "IdPresentacion= " + psIdPresentacion;

        Cursor loCursor = loDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanPresentacion.setId(loCursor.getString(0));
                    loBeanPresentacion.setProducto(fnSelxIdProducto(loCursor.getString(1)));
                    loBeanPresentacion.setNombre(loCursor.getString(2));
                    loBeanPresentacion.setStock(loCursor.getString(3));
                    loBeanPresentacion.setCantidad(loCursor.getLong(4));
                    loBeanPresentacion.setPrecioBase(loCursor.getString(5));
                    loBeanPresentacion.setDescuentoMinimo(loCursor.getString(6));
                    loBeanPresentacion.setDescuentoMaximo(loCursor.getString(7));
                    loBeanPresentacion.setUnidadFraccionamiento(loCursor.getLong(8));
                    loBeanPresentacion.setCodigo(loCursor.getString(9));
                    loBeanPresentacion.setPrecioBaseSoles(loCursor.getString(10));//@JBELVY
                    loBeanPresentacion.setPrecioBaseDolares(loCursor.getString(11));//@JBELVY

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return loBeanPresentacion;
    }

    public BeanPresentacion fnSelPresentacionxIdProducto(String psIdProducto) {
        BeanPresentacion loBeanPresentacion = new BeanPresentacion();
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"IdPresentacion", "IdProducto", "Nombre", "Stock",
                "Cantidad", "Precio", "DescuentoMinimo", "DescuentoMaximo", "UnidadFraccionamiento", "Codigo",
                "PrecioSoles","PrecioDolares" }; //@JBELVY
        String lsTablas = "TBL_PRESENTACION";
        String lsWhere = "IdProducto= " + psIdProducto;

        Cursor loCursor = loDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanPresentacion.setId(loCursor.getString(0));
                    loBeanPresentacion.setProducto(fnSelxIdProducto(loCursor.getString(1)));
                    loBeanPresentacion.setNombre(loCursor.getString(2));
                    loBeanPresentacion.setStock(loCursor.getString(3));
                    loBeanPresentacion.setCantidad(loCursor.getLong(4));
                    loBeanPresentacion.setPrecioBase(loCursor.getString(5));
                    loBeanPresentacion.setDescuentoMinimo(loCursor.getString(6));
                    loBeanPresentacion.setDescuentoMaximo(loCursor.getString(7));
                    loBeanPresentacion.setUnidadFraccionamiento(loCursor.getLong(8));
                    loBeanPresentacion.setCodigo(loCursor.getString(9));
                    loBeanPresentacion.setPrecioBaseSoles(loCursor.getString(10)); //@JBELVY
                    loBeanPresentacion.setPrecioBaseDolares(loCursor.getString(11)); //@JBELVY

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return loBeanPresentacion;
    }

    public BeanPresentacion fnSelxCodigoPresentacion(String psCodPresentacion) {
        BeanPresentacion loBeanPresentacion = new BeanPresentacion();
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"IdPresentacion", "IdProducto", "Nombre", "Stock",
                "Cantidad", "Precio", "DescuentoMinimo", "DescuentoMaximo", "UnidadFraccionamiento", "Codigo",
                "PrecioSoles", "PrecioDolares" }; //@JBELVY
        String lsTablas = "TBL_PRESENTACION";
        String lsWhere = "Codigo='" + psCodPresentacion + "'";

        Cursor loCursor = loDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanPresentacion.setId(loCursor.getString(0));
                    loBeanPresentacion.setProducto(fnSelxIdProducto(loCursor.getString(1)));
                    loBeanPresentacion.setNombre(loCursor.getString(2));
                    loBeanPresentacion.setStock(loCursor.getString(3));
                    loBeanPresentacion.setCantidad(loCursor.getLong(4));
                    loBeanPresentacion.setPrecioBase(loCursor.getString(5));
                    loBeanPresentacion.setDescuentoMinimo(loCursor.getString(6));
                    loBeanPresentacion.setDescuentoMaximo(loCursor.getString(7));
                    loBeanPresentacion.setUnidadFraccionamiento(loCursor.getLong(8));
                    loBeanPresentacion.setCodigo(loCursor.getString(9));
                    loBeanPresentacion.setPrecioBaseSoles(loCursor.getString(10));//@JBELVY
                    loBeanPresentacion.setPrecioBaseDolares(loCursor.getString(11));//@JBELVY
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return loBeanPresentacion;
    }

    public Boolean fnExistePresentacionSincronizado(String psCodPresentacion) {
        Boolean sincronizado = false;

        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"IdPresentacion", "IdProducto", "Nombre", "Stock",
                "Cantidad", "Precio", "DescuentoMinimo", "DescuentoMaximo", "UnidadFraccionamiento", "Codigo",
                "PrecioSoles", "PrecioDolares" }; //@JBELVY
        String lsTablas = "TBL_PRESENTACION";
        String lsWhere = "Codigo='" + psCodPresentacion + "'";

        Cursor loCursor = loDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    sincronizado = true;
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return sincronizado;
    }

    public BeanProducto fnSelxIdProducto(String psIdProducto) {
        BeanProducto loBeanProducto = new BeanProducto();
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"ID", "Codigo", "Nombre", "Stock",
                "UnidadDefecto", "PrecioBase",
                //@JBELVY -I
                "PrecioBaseSoles", "PrecioBaseDolares",
                //@JBELVY -F
                "DescuentoMinimo", "DescuentoMaximo"};
        String lsTablas = "tbl_producto";
        String lsWhere = "ID=" + psIdProducto;

        Cursor loCursor = loDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanProducto.setId(loCursor.getString(0));
                    loBeanProducto.setCodigo(loCursor.getString(1));
                    loBeanProducto.setNombre(loCursor.getString(2));
                    loBeanProducto.setStock(loCursor.getString(3));
                    loBeanProducto.setUnidadDefecto(loCursor.getString(4));
                    loBeanProducto.setPrecioBase(loCursor.getString(5));
                    //@JBELVY -I
                    loBeanProducto.setPrecioBaseSoles(loCursor.getString(6));
                    loBeanProducto.setPrecioBaseDolares(loCursor.getString(7));
                    //@JBELVY -F
                    loBeanProducto.setDescuentoMinimo(loCursor.getString(8));
                    loBeanProducto.setDescuentoMaximo(loCursor.getString(9));
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return loBeanProducto;
    }

    public boolean fnUpdStockProducto(String id, String stock) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE tbl_producto SET Stock='" + stock
                + "' WHERE ID = " + id);
        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnUpdStockPresentacion(String id, String stock) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TBL_PRESENTACION SET Stock='" + stock
                + "' WHERE IdPresentacion = " + id);
        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public List<Item> fnSelProductos(String psFiltro,
                                     EnumTipBusqueda poTipBusqueda) {
        List<Item> loList;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        boolean lbPriLetra = true;

        String[] laColumnas = new String[]{"ID", "Codigo", "upper(Nombre)",
                "Stock", "UnidadDefecto", "PrecioBase",
                "PrecioBaseSoles", "PrecioBaseDolares" //@JBELVY
        };
        String lsTablas = "tbl_producto";
        String lsWhere = "";

        lsWhere = (poTipBusqueda == EnumTipBusqueda.NOMBRE) ? "Nombre like "
                + "'%" + ((lbPriLetra) ? "" : "%") + psFiltro + "%'"
                : "trim((Codigo)) = " + "'" + psFiltro.toUpperCase() + "'";

        Cursor loCursor;

        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                "upper(Nombre)");

        loList = new ArrayList<Item>();
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    String lsMostrar = loCursor.getString(1) + " - "
                            + loCursor.getString(2);
                    int liKey = Integer.parseInt(loCursor.getString(0));
                    DrawableItem loDrawableItem = new DrawableItem(lsMostrar, 0);
                    loDrawableItem.setTag(liKey);
                    loList.add(loDrawableItem);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return loList;
    }

    public List<BeanMostrarProducto> fnListSelProductosPorAlmacenPorPresentacion(String psFiltro,
                                                                                 EnumTipBusqueda poTipBusqueda,
                                                                                 String psStock,
                                                                                 String psValidarPrecio) {
        List<BeanMostrarProducto> loList;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        boolean lbPriLetra = true;

        psStock = psStock.equals(Configuracion.FLGVERDADERO) ? "1" : "0";
        psValidarPrecio = psValidarPrecio.equals(Configuracion.FLGVERDADERO) ? "1" : "0";

        String lsWhere = " ALMPRE.Stock <> '' AND ((" + psStock + "=1 AND CAST(ALMPRE.Stock AS FLOAT)> 0) " +
                "OR(" + psStock + " != 1)) AND  ((" + psValidarPrecio + " = 1 AND " +
                "CAST(PRE.Precio AS FLOAT)> 0) OR " + psValidarPrecio + " != 1) AND ";

        lsWhere += (poTipBusqueda == EnumTipBusqueda.NOMBRE) ? "PRO.Nombre like "
                + "'%" + ((lbPriLetra) ? "" : "%") + psFiltro + "%'"
                : "trim((PRO.Codigo)) = " + "'" + psFiltro.toUpperCase() + "'";

        lsWhere += " ORDER BY upper(PRO.Nombre)";

        String sql = "SELECT DISTINCT PRE.IdPresentacion,PRO.Codigo, upper(PRO.Nombre), " +
                "upper(PRE.Nombre), PRE.Cantidad,  PRO.UnidadDefecto " +
                "FROM TBL_PRESENTACION PRE " +
                "INNER JOIN TBL_PRODUCTO PRO ON PRO.ID = PRE.IdProducto " +
                "INNER JOIN TBL_ALMACEN_PRESENTACION ALMPRE ON ALMPRE.IdPresentacion = PRE.IdPresentacion " +
                "WHERE " + lsWhere;

        Cursor loCursor;

        loCursor = myDB.rawQuery(sql, null);

        loList = new ArrayList<BeanMostrarProducto>();
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    String lsMostrar = loCursor.getString(1) + " - "
                            + loCursor.getString(2);
                    String lsPresentacion = loCursor.getString(3) + " - " + loCursor.getString(4)
                            + " " + loCursor.getString(5);
                    String lsKey = loCursor.getString(0);


                    BeanMostrarProducto loBeanMostrarProducto = new BeanMostrarProducto();
                    loBeanMostrarProducto.setId(lsKey);
                    loBeanMostrarProducto.setTitulo(lsMostrar);
                    loBeanMostrarProducto.setPresentacion(lsPresentacion);
                    loList.add(loBeanMostrarProducto);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return loList;
    }

    public List<BeanMostrarProducto> fnListSelProductosByAlmacen(String psFiltro,
                                                                 EnumTipBusqueda poTipBusqueda,
                                                                 String psStock,
                                                                 String psValidarPrecio) {
        List<BeanMostrarProducto> loList;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        boolean lbPriLetra = true;

        psStock = psStock.equals(Configuracion.FLGVERDADERO) ? "1" : "0";
        psValidarPrecio = psValidarPrecio.equals(Configuracion.FLGVERDADERO) ? "1" : "0";

        String lsWhere = "ALM_PRO.ALM_PRO_STOCK <> '' AND ((" + psStock + "=1 AND CAST(ALM_PRO.ALM_PRO_STOCK AS FLOAT)> 0) " +
                "OR(" + psStock + " != 1)) AND  ((" + psValidarPrecio + " = 1 AND " +
                "CAST(PrecioBase AS FLOAT)> 0) OR " + psValidarPrecio + " != 1) AND ";

        lsWhere += (poTipBusqueda == EnumTipBusqueda.NOMBRE) ? "Nombre like "
                + "'%" + ((lbPriLetra) ? "" : "%") + psFiltro + "%'"
                : "trim((Codigo)) = " + "'" + psFiltro.toUpperCase() + "'";

        lsWhere += " ORDER BY upper(Nombre)";

        String sql = "SELECT DISTINCT PRO.ID,PRO.Codigo, upper(PRO.Nombre)," +
                "PRO.Stock, PRO.UnidadDefecto, PRO.PrecioBase, PRO.PrecioBaseSoles, PRO.PrecioBaseDolares " +
                "FROM TBL_PRODUCTO PRO " +
                "INNER JOIN TBL_ALMACEN_PRODUCTO ALM_PRO " +
                "ON PRO.ID = ALM_PRO.PRO_PK " +
                "WHERE " + lsWhere;

        Cursor loCursor;

        loCursor = myDB.rawQuery(sql, null);

        loList = new ArrayList<BeanMostrarProducto>();
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    String lsMostrar = loCursor.getString(1) + " - "
                            + loCursor.getString(2);
                    String lsKey = loCursor.getString(0);
                    DrawableItem loDrawableItem = new DrawableItem(lsMostrar, 0);

                    BeanMostrarProducto loBeanMostrarProducto = new BeanMostrarProducto();
                    loBeanMostrarProducto.setId(lsKey);
                    loBeanMostrarProducto.setTitulo(lsMostrar);
                    loList.add(loBeanMostrarProducto);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return loList;
    }

    public List<BeanMostrarProducto> fnListSelPresentacion(String psFiltro,
                                                           EnumTipBusqueda poTipBusqueda,
                                                           String psStock,
                                                           String psValidarPrecio) {
        List<BeanMostrarProducto> loList;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        boolean lbPriLetra = true;

        String lsSql = "SELECT DISTINCT PRE.IdPresentacion,PRO.Codigo, upper(PRO.Nombre), " +
                "upper(PRE.Nombre), PRE.Cantidad,  PRO.UnidadDefecto " +
                "FROM TBL_PRESENTACION PRE " +
                "INNER JOIN TBL_PRODUCTO PRO ON PRO.ID = PRE.IdProducto ";
        String lsWhere = "";

        psStock = psStock.equals(Configuracion.FLGVERDADERO) ? "1" : "0";
        psValidarPrecio = psValidarPrecio.equals(Configuracion.FLGVERDADERO) ? "1" : "0";

        lsWhere = "WHERE PRE.Stock <> '' AND ((" + psStock + "=1 AND CAST(PRE.Stock AS FLOAT)> 0) " +
                "OR(" + psStock + " != 1)) AND  ((" + psValidarPrecio + " = 1 AND " +
                "CAST(PRE.Precio AS FLOAT)> 0) OR " + psValidarPrecio + " != 1) AND ";
        lsWhere += (poTipBusqueda == EnumTipBusqueda.NOMBRE) ? "PRO.Nombre like "
                + "'%" + ((lbPriLetra) ? "" : "%") + psFiltro + "%'"
                : "trim((PRO.Codigo)) = " + "'" + psFiltro.toUpperCase() + "'";

        lsWhere += " ORDER BY upper(PRO.Nombre)";

        lsSql += lsWhere;

        Cursor loCursor;

        loCursor = myDB.rawQuery(lsSql, null);

        loList = new ArrayList<BeanMostrarProducto>();
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    String lsMostrar = loCursor.getString(1) + " - "
                            + loCursor.getString(2);
                    String lsPresentacion = loCursor.getString(3) + " - " + loCursor.getString(4)
                            + " " + loCursor.getString(5);
                    String lsKey = loCursor.getString(0);


                    BeanMostrarProducto loBeanMostrarProducto = new BeanMostrarProducto();
                    loBeanMostrarProducto.setId(lsKey);
                    loBeanMostrarProducto.setTitulo(lsMostrar);
                    loBeanMostrarProducto.setPresentacion(lsPresentacion);
                    loList.add(loBeanMostrarProducto);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return loList;
    }


    public BeanPresentacion fnListSelProductosPresentacionPedidoRapido(String codProd) {
        String isValidarStock = Configuracion.EnumConfiguracion.VALIDAR_STOCK.getValor(ioContext);
        BeanPresentacion beanPresentacion = new BeanPresentacion();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        boolean lbPriLetra = true;

        String[] laColumnas = new String[]{"IdPresentacion", "Codigo", "upper(Nombre)", "Stock", "Precio",
                //@JBELVY -I
                "PrecioSoles", "PrecioDolares",
                //@JBELVY -F
                "Cantidad", "IdProducto"};
        String lsTablas = "tbl_presentacion";
        String lsWhere = "";
        if (isValidarStock.equals(Configuracion.FLGVERDADERO)) {
            lsWhere = "Codigo=" + "'" + codProd + "' AND CAST(Stock AS FLOAT) > 0";
        } else {
            lsWhere = "Codigo=" + "'" + codProd + "'";
        }
        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                "upper(Nombre)");

        if (loCursor.getCount() > 0) {
            if (loCursor.moveToFirst()) {
                do {
                    String idarticulo = loCursor.getString(0);
                    String codProducto = loCursor.getString(1);
                    String nomProd = loCursor.getString(2);
                    String stock = loCursor.getString(3);
                    String precio = loCursor.getString(4);
                    //@JBELVY -I
                    String preciosoles = loCursor.getString(5);
                    String preciodolares = loCursor.getString(6);
                    //@JBELVY -F
                    Long cantidad = loCursor.getLong(7);
                    beanPresentacion.setId(idarticulo);
                    beanPresentacion.setCodigo(codProducto);
                    beanPresentacion.setNombre(nomProd);
                    beanPresentacion.setStock(stock);
                    beanPresentacion.setPrecioBase(precio);
                    //@JBELVY -I
                    beanPresentacion.setPrecioBaseSoles(preciosoles);
                    beanPresentacion.setPrecioBaseDolares(preciodolares);
                    //@JBELVY -F
                    beanPresentacion.setCantidad(cantidad);
                    beanPresentacion.setProducto(fnSelxIdProducto(loCursor.getString(8)));
                } while (loCursor.moveToNext());
            }

        } else {
            beanPresentacion.setNombre("");
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return beanPresentacion;
    }


    public BeanProducto fnListSelProductosPedidoRapido(String codProd) {
        String isValidarStock = Configuracion.EnumConfiguracion.VALIDAR_STOCK.getValor(ioContext);
        BeanProducto beanProducto = new BeanProducto();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        boolean lbPriLetra = true;

        String[] laColumnas = new String[]{"ID", "Codigo", "upper(Nombre)",
                "Stock", "UnidadDefecto", "PrecioBase",
                //@JBELVY -I
                "PrecioBaseSoles", "PrecioBaseDolares"
                //@JBELVY -F
        };
        String lsTablas = "tbl_producto";
        String lsWhere = "";
        if (isValidarStock.equals(Configuracion.FLGVERDADERO)) {
            lsWhere = "Codigo=" + "'" + codProd + "' AND CAST(Stock AS FLOAT) > 0";
        } else {
            lsWhere = "Codigo=" + "'" + codProd + "'";
        }
        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                "upper(Nombre)");

        if (loCursor.getCount() > 0) {
            if (loCursor.moveToFirst()) {
                do {
                    String idarticulo = loCursor.getString(0);
                    String codProducto = loCursor.getString(1);
                    String nomProd = loCursor.getString(2);
                    String stock = loCursor.getString(3);
                    String precioBase = loCursor.getString(5);
                    //@JBELVY -I
                    String precioBaseSoles = loCursor.getString(6);
                    String precioBaseDolares = loCursor.getString(7);
                    //@JBELVY -F
                    beanProducto.setId(idarticulo);
                    beanProducto.setCodigo(codProducto);
                    beanProducto.setNombre(nomProd);
                    beanProducto.setStock(stock);
                    beanProducto.setPrecioBase(precioBase);
                    //@JBELVY -I
                    beanProducto.setPrecioBaseSoles(precioBaseSoles);
                    beanProducto.setPrecioBaseDolares(precioBaseDolares);
                    //@JBELVY -F
                } while (loCursor.moveToNext());
            }

        } else {
            beanProducto.setNombre("");
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return beanProducto;
    }


    public List<BeanMostrarProducto> fnListSelProductos(String psFiltro,
                                                        EnumTipBusqueda poTipBusqueda,
                                                        String psStock,
                                                        String psValidarPrecio) {
        List<BeanMostrarProducto> loList;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        boolean lbPriLetra = true;

        String[] laColumnas = new String[]{"ID", "Codigo", "upper(Nombre)",
                "Stock", "UnidadDefecto", "PrecioBase",
                //@JBELVY -I
                "PrecioBaseSoles", "PrecioBaseDolares"
                //@JBELVY -F
        };
        String lsTablas = "tbl_producto";
        String lsWhere = "";

        psStock = psStock.equals(Configuracion.FLGVERDADERO) ? "1" : "0";
        psValidarPrecio = psValidarPrecio.equals(Configuracion.FLGVERDADERO) ? "1" : "0";

        lsWhere = "Stock <> '' AND ((" + psStock + "=1 AND CAST(Stock AS FLOAT)> 0) " +
                "OR(" + psStock + " != 1)) AND  ((" + psValidarPrecio + " = 1 AND " +
                //@JBELVY -I
                "CAST(PrecioBaseSoles AS FLOAT)> 0) OR " + psValidarPrecio + " != 1) AND ";
                //"CAST(PrecioBase AS FLOAT)> 0) OR " + psValidarPrecio + " != 1) AND ";
                //@JBELVY -F

        lsWhere += (poTipBusqueda == EnumTipBusqueda.NOMBRE) ? "Nombre like "
                + "'%" + ((lbPriLetra) ? "" : "%") + psFiltro + "%'"
                : "trim((Codigo)) = " + "'" + psFiltro.toUpperCase() + "'";

        Cursor loCursor;

        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                "upper(Nombre)");

        loList = new ArrayList<BeanMostrarProducto>();
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    String lsMostrar = loCursor.getString(1) + " - "
                            + loCursor.getString(2);
                    String lsKey = loCursor.getString(0);

                    BeanMostrarProducto loBeanMostrarProducto = new BeanMostrarProducto();
                    loBeanMostrarProducto.setId(lsKey);
                    loBeanMostrarProducto.setTitulo(lsMostrar);
                    loList.add(loBeanMostrarProducto);

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return loList;
    }

    public List<Item> fnSelListaPrecioXCodProductoXCanal(long piIdProducto,
                                                         String canal, String psNumDecimales) {
        List<Item> loLista = new ArrayList<Item>();

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"ID", "Codigo", "Producto",
                "Canal", "Precio",
                //@JBELVY -I
                "PrecioSoles", "PrecioDolares"
                //@JBELVY -F
        };
        String lsTablas = "tbl_listaprecios";
        String lsWhere = "ID = '" + String.valueOf(piIdProducto)
                + "' AND Canal ='" + canal + "'";
        Cursor loCursor;

        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                "Canal");

        ThumbnailItem object = null;

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    String liKey = loCursor.getString(1) + ";"
                            + loCursor.getString(5); //@JBELVY BeforeColumnIndex 4
                    String precioFormat;
                    try {
                        precioFormat = Utilitario.fnRound(
                                loCursor.getString(5), Integer //@JBELVY BeforeColumnIndex 4
                                        .valueOf(psNumDecimales));
                    } catch (Exception e) {
                        precioFormat = loCursor.getString(5); //@JBELVY BeforeColumnIndex 4
                    }

                    object = new ThumbnailItem(loCursor.getString(2) + " ",
                            ioContext.getString(R.string.moneda) + " "
                                    + precioFormat, R.drawable.lista_precio);
                    object.setTag(liKey);
                    loLista.add(object);

                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loLista;
    }

    public List<Item> fnSelListaPrecioXCodProductoXCanalXCondVta(
            long piIdProducto, String canal, String condVta,
            String psNumDecimales) {
        List<Item> loLista = new ArrayList<Item>();

        String descCondVta = "";
        DalGeneral loDalGeneral = new DalGeneral(ioContext);

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"ID", "Codigo", "Producto",
                "Canal", "Precio",
                //@JBELVY -I
                "PrecioSoles","PrecioDolares",
                //@JBELVY -F
                "CondVta"};
        String lsTablas = "tbl_listaprecios";
        String lsWhere = "ID = '" + String.valueOf(piIdProducto) + "' ";

        if (canal.compareTo("") != 0) {
            lsWhere += " AND Canal = '" + canal + "'";
        }

        if (condVta.compareTo("") != 0) {
            lsWhere += " AND CondVta = '" + condVta + "'";
        }

        Cursor loCursor;

        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        ThumbnailItem object = null;

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    String liKey = "";

                    liKey = loCursor.getString(1) + ";"
                            + loCursor.getString(5); //@JBELVY BeforeColumnIndex 4

                    String precioFormat;
                    try {
                        if (loCursor.getString(7).compareTo("") != 0)//@JBELVY BeforeColumnIndex 5
                            descCondVta = loDalGeneral
                                    .fnDescFormaPagoXCod(loCursor.getString(7));//@JBELVY BeforeColumnIndex 5
                        else
                            descCondVta = "";
                        precioFormat = Utilitario.fnRound(
                                loCursor.getString(5), Integer //@JBELVY BeforeColumnIndex 4
                                        .valueOf(psNumDecimales));
                    } catch (Exception e) {
                        precioFormat = loCursor.getString(5); //@JBELVY BeforeColumnIndex 4
                    }

                    if (canal.compareTo("") == 0 && condVta.compareTo("") != 0) {

                        if (loCursor.getString(3).compareTo("") == 0) {
                            object = new ThumbnailItem(loCursor.getString(2),
                                    ioContext.getString(R.string.moneda) + " "
                                            + precioFormat,
                                    R.drawable.lista_precio);
                        } else {
                            object = new ThumbnailItem(loCursor.getString(2)
                                    + " - " + loCursor.getString(3),
                                    ioContext.getString(R.string.moneda) + " "
                                            + precioFormat,
                                    R.drawable.lista_precio);
                        }

                    } else if (condVta.compareTo("") == 0
                            && canal.compareTo("") != 0) {
                        if (loCursor.getString(5).compareTo("") == 0) {
                            object = new ThumbnailItem(loCursor.getString(2),
                                    ioContext.getString(R.string.moneda) + " "
                                            + precioFormat,
                                    R.drawable.lista_precio);
                        } else {
                            object = new ThumbnailItem(loCursor.getString(2)
                                    + " - " + loCursor.getString(7), //@JBELVY BeforeColumnIndex 5
                                    ioContext.getString(R.string.moneda) + " "
                                            + precioFormat,
                                    R.drawable.lista_precio);
                        }

                        object = new ThumbnailItem(loCursor.getString(2)
                                + " - " + descCondVta,
                                ioContext.getString(R.string.moneda) + " "
                                        + precioFormat, R.drawable.lista_precio);
                    } else {
                        object = new ThumbnailItem(loCursor.getString(2) + " ",
                                ioContext.getString(R.string.moneda) + " "
                                        + precioFormat, R.drawable.lista_precio);
                    }

                    object.setTag(liKey);
                    loLista.add(object);

                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loLista;
    }

    public BeanPrecio fnSelPrecioXListaPrecioXCodigo(String piIdPrecio) {
        BeanPrecio loPrecio = new BeanPrecio();
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"Codigo", "Precio", "DescuentoMinimo", "DescuentoMaximo", "PrecioSoles", "PrecioDolares"};
        String lsTablas = "TBL_LISTAPRECIOS";
        String lsWhere = "Codigo = " + String.valueOf(piIdPrecio);

        Cursor loCursor = loDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loPrecio.setId(loCursor.getString(0));
                    loPrecio.setPrecio(loCursor.getString(1));
                    loPrecio.setDescuentoMinimo(loCursor.getString(2));
                    loPrecio.setDescuentoMaximo(loCursor.getString(3));
                    loPrecio.setPrecioSoles(loCursor.getString(4)); //@JBELVY
                    loPrecio.setPrecioDolares(loCursor.getString(5)); //@JBELVY
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return loPrecio;
    }

    public BeanPrecio fnSelPrecioPresentacionXListaPrecioXCodigo(String piIdPrecio) {
        BeanPrecio loPrecio = new BeanPrecio();
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"ID", "Precio", "DescuentoMinimo", "DescuentoMaximo", "PrecioSoles", "PrecioDolares"};
        String lsTablas = "TBL_LISTAPRECIOS_PRESENTACION";
        String lsWhere = "ID = " + String.valueOf(piIdPrecio);

        Cursor loCursor = loDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loPrecio.setId(loCursor.getString(0));
                    loPrecio.setPrecio(loCursor.getString(1));
                    loPrecio.setDescuentoMinimo(loCursor.getString(2));
                    loPrecio.setDescuentoMaximo(loCursor.getString(3));
                    loPrecio.setPrecioSoles(loCursor.getString(4)); //@JBELVY
                    loPrecio.setPrecioDolares(loCursor.getString(5)); //@JBELVY
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return loPrecio;
    }

    public BeanPrecio fnSelPrecioPresentacionXPresentacionXCodigo(String piIdPresentacion) {
        BeanPrecio loPrecio = new BeanPrecio();
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"IdPresentacion", "Precio", "DescuentoMinimo", "DescuentoMaximo", "PrecioSoles", "PrecioDolares"};
        String lsTablas = "TBL_PRESENTACION";
        String lsWhere = "IdPresentacion = " + String.valueOf(piIdPresentacion);

        Cursor loCursor = loDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loPrecio.setId(loCursor.getString(0));
                    loPrecio.setPrecio(loCursor.getString(1));
                    loPrecio.setDescuentoMinimo(loCursor.getString(2));
                    loPrecio.setDescuentoMaximo(loCursor.getString(3));
                    loPrecio.setPrecioSoles(loCursor.getString(4)); //@JBELVY
                    loPrecio.setPrecioDolares(loCursor.getString(5)); //@JBELVY
                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        loDB.close();
        return loPrecio;
    }

    public ArrayList<ThumbnailItem> fnSelListPrecioXCodPresentacionXCanalXCondVta(
            String psIdPresentacion, String canal, String condVta,
            String moneda, String monedaDolar, String numDecimales) {
        ArrayList<ThumbnailItem> loLista = new ArrayList<ThumbnailItem>();

        String descCondVta = "";
        DalGeneral loDalGeneral = new DalGeneral(ioContext);

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"ID",
                "Canal", "Precio", "CondVta", "PrecioSoles", "PrecioDolares"};

        String lsTablas = "tbl_listaprecios_presentacion";
        String lsWhere = "IdPresentacion = " + psIdPresentacion + " ";

        if (canal.compareTo("") != 0) {
            lsWhere += " AND Canal = '" + canal + "'";
        }

        if (condVta.compareTo("") != 0) {
            lsWhere += " AND CondVta = '" + condVta + "'";
        }

        Cursor loCursor;

        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        ThumbnailItem object = null;
        DalGeneral dGeneral = new DalGeneral(ioContext);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    String liKey = "";

                    liKey = loCursor.getString(0);

                    String precioSoles;
                    String precioDolar;//@JBELVY
                    try {
                        if (loCursor.getString(3).compareTo("") != 0)
                            descCondVta = loDalGeneral.fnDescFormaPagoXCod(loCursor.getString(3));
                        else
                            descCondVta = "";

                        precioSoles = Utilitario.fnRound(
                                loCursor.getString(4), Integer //@JBELVY BeforeColumnIndex 2
                                        .valueOf(numDecimales));
                        precioDolar = Utilitario.fnRound(
                                loCursor.getString(5), Integer //@JBELVY
                                        .valueOf(numDecimales));

                    } catch (Exception e) {
                        precioSoles = Utilitario.fnRound(
                                loCursor.getString(4), Integer//@JBELVY BeforeColumnIndex 2
                                        .valueOf(numDecimales));

                        precioDolar = Utilitario.fnRound(
                                loCursor.getString(5), Integer
                                        .valueOf(numDecimales));
                    }

                    String PrecioSolesFormat;
                    String PrecioDolarFormat;
                    String VGuin;

                    if(precioSoles.equals("0") || precioSoles.equals("0.0") || precioSoles.equals("0.00") || precioSoles.equals("")){
                        PrecioSolesFormat = "";

                    }else{
                        PrecioSolesFormat = (moneda + " " + precioSoles);
                    }

                    if(precioDolar.equals("0") || precioDolar.equals("0.0") || precioDolar.equals("0.00") || precioDolar.equals("")){
                        PrecioDolarFormat = "";
                    }else{
                        PrecioDolarFormat = (monedaDolar + " " + precioDolar);
                    }

                    if(PrecioSolesFormat.equals("") && PrecioDolarFormat.length() > 0){
                        VGuin = "";
                    } else if(PrecioDolarFormat.equals("") && PrecioSolesFormat.length() > 0){
                        VGuin = "";
                    } else{
                        VGuin = " - ";
                    }


                    if (canal.compareTo("") == 0 && condVta.compareTo("") != 0) {

                        if (loCursor.getString(1).compareTo("") == 0) {
                            object = new ThumbnailItem(PrecioSolesFormat + VGuin + PrecioDolarFormat,
                                    PrecioSolesFormat + VGuin + PrecioDolarFormat,
                                    R.drawable.lista_precio);
                        } else {
                            object = new ThumbnailItem(PrecioSolesFormat + VGuin + PrecioDolarFormat + " : " + loCursor.getString(1),
                                    PrecioSolesFormat + VGuin + PrecioDolarFormat, R.drawable.lista_precio);
                        }

                    } else if (condVta.compareTo("") == 0
                            && canal.compareTo("") != 0) {
                        if (loCursor.getString(3).compareTo("") == 0) {
                            object = new ThumbnailItem(PrecioSolesFormat + VGuin + PrecioDolarFormat,
                                    PrecioSolesFormat + VGuin + PrecioDolarFormat,
                                    R.drawable.lista_precio);
                        } else {
                            String desCondVenta = dGeneral
                                    .fnDescFormaPagoXCod(loCursor.getString(3));
                            if (desCondVenta.compareTo("") == 0)
                                object = new ThumbnailItem(PrecioSolesFormat + VGuin + PrecioDolarFormat + ": "
                                        + loCursor.getString(3), PrecioSolesFormat + VGuin + PrecioDolarFormat, R.drawable.lista_precio);
                            else
                                object = new ThumbnailItem(PrecioSolesFormat + VGuin + PrecioDolarFormat + " : " + desCondVenta,
                                        PrecioSolesFormat + VGuin + PrecioDolarFormat,
                                        R.drawable.lista_precio);

                        }
                    } else if (canal.compareTo("") == 0
                            && condVta.compareTo("") == 0) {

                        String mensaje = PrecioSolesFormat + VGuin + PrecioDolarFormat;

                        int i = 0;
                        if (loCursor.getString(1).compareTo("") != 0) {
                            mensaje += " : " + loCursor.getString(1);
                            i++;
                        }

                        if (loCursor.getString(3).compareTo("") != 0) {
                            if (i == 0)
                                mensaje += " : ";
                            else
                                mensaje += " - ";

                            String desCondVenta = dGeneral .fnDescFormaPagoXCod(loCursor.getString(3));

                            if (desCondVenta.compareTo("") == 0)
                                mensaje += loCursor.getString(3);
                            else
                                mensaje += descCondVta;

                        }

                        object = new ThumbnailItem(mensaje, PrecioSolesFormat + VGuin + PrecioDolarFormat, R.drawable.lista_precio);

                    } else {
                        object = new ThumbnailItem(PrecioSolesFormat + VGuin + PrecioDolarFormat,
                                PrecioSolesFormat + VGuin + PrecioDolarFormat,
                                R.drawable.lista_precio);
                    }

                    object.setTag(liKey);
                    loLista.add(object);

                } while (loCursor.moveToNext());
            }
        }


        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loLista;
    }

    public ArrayList<ThumbnailItem> fnSelListPrecioXCodProductoXCanalXCondVta(
            String psIdProducto, String canal, String condVta,
            String moneda, String monedaDolar, String numDecimales) {
        ArrayList<ThumbnailItem> loLista = new ArrayList<ThumbnailItem>();

        String descCondVta = "";
        DalGeneral loDalGeneral = new DalGeneral(ioContext);

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"ID", "Codigo", "Producto",
                "Canal", "Precio", "CondVta", "PrecioSoles", "PrecioDolares"};

        String lsTablas = "tbl_listaprecios";
        String lsWhere = "ID = '" + String.valueOf(psIdProducto) + "' ";

        if (canal.compareTo("") != 0) {
            lsWhere += " AND Canal = '" + canal + "'";
        }

        if (condVta.compareTo("") != 0) {
            lsWhere += " AND CondVta = '" + condVta + "'";
        }

        Cursor loCursor;

        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        ThumbnailItem object = null;
        DalGeneral dGeneral = new DalGeneral(ioContext);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    String liKey = "";

                    liKey = loCursor.getString(1);

                    String precioSoles;
                    String precioDolar;//@JBELVY
                    try {
                        if (loCursor.getString(5).compareTo("") != 0)
                            descCondVta = loDalGeneral
                                    .fnDescFormaPagoXCod(loCursor.getString(5));
                        else
                            descCondVta = "";

                        precioSoles = Utilitario.fnRound(
                                loCursor.getString(6), Integer //@JBELVY BeforeColumnIndex 4
                                        .valueOf(numDecimales));

                        precioDolar = Utilitario.fnRound(
                                loCursor.getString(7), Integer //@JBELVY
                                        .valueOf(numDecimales));

                    } catch (Exception e) {
                        precioSoles = Utilitario.fnRound(
                                loCursor.getString(6), Integer //@JBELVY BeforeColumnIndex 4
                                        .valueOf(numDecimales));

                        precioDolar = Utilitario.fnRound(
                                loCursor.getString(7), Integer //@JBELVY
                                        .valueOf(numDecimales));
                    }

                    String PrecioSolesFormat;
                    String PrecioDolarFormat;
                    String VGuin;

                    if(precioSoles.equals("0") || precioSoles.equals("0.0") || precioSoles.equals("0.00") || precioSoles.equals("")){
                        PrecioSolesFormat = "";

                    }else{
                        PrecioSolesFormat = (moneda + " " + precioSoles);
                    }

                    if(precioDolar.equals("0") || precioDolar.equals("0.0") || precioDolar.equals("0.00") || precioDolar.equals("")){
                        PrecioDolarFormat = "";
                    }else{
                        PrecioDolarFormat = (monedaDolar + " " + precioDolar);
                    }

                    if(PrecioSolesFormat.equals("") && PrecioDolarFormat.length() > 0){
                        VGuin = "";
                    } else if(PrecioDolarFormat.equals("") && PrecioSolesFormat.length() > 0){
                        VGuin = "";
                    } else{
                        VGuin = " - ";
                    }

                    if (canal.compareTo("") == 0 && condVta.compareTo("") != 0) {

                        if (loCursor.getString(3).compareTo("") == 0) {
                            object = new ThumbnailItem(PrecioSolesFormat + VGuin + PrecioDolarFormat,
                                    PrecioSolesFormat + VGuin + PrecioDolarFormat,
                                    R.drawable.lista_precio);
                        } else {
                            object = new ThumbnailItem(PrecioSolesFormat + VGuin + PrecioDolarFormat + " : "
                                    + loCursor.getString(3), PrecioSolesFormat + VGuin + PrecioDolarFormat, R.drawable.lista_precio);
                        }

                    } else if (condVta.compareTo("") == 0
                            && canal.compareTo("") != 0) {
                        if (loCursor.getString(5).compareTo("") == 0) {
                            object = new ThumbnailItem(PrecioSolesFormat + VGuin + PrecioDolarFormat,
                                    PrecioSolesFormat + VGuin + PrecioDolarFormat,
                                    R.drawable.lista_precio);
                        } else {
                            String desCondVenta = dGeneral
                                    .fnDescFormaPagoXCod(loCursor.getString(5));
                            if (desCondVenta.compareTo("") == 0)
                                object = new ThumbnailItem(PrecioSolesFormat + VGuin + PrecioDolarFormat + ": "
                                        + loCursor.getString(5), PrecioSolesFormat + VGuin + PrecioDolarFormat, R.drawable.lista_precio);
                            else
                                object = new ThumbnailItem(PrecioSolesFormat + VGuin + PrecioDolarFormat + " : " + desCondVenta,
                                        PrecioSolesFormat + VGuin + PrecioDolarFormat,
                                        R.drawable.lista_precio);

                        }
                    } else if (canal.compareTo("") == 0
                            && condVta.compareTo("") == 0) {

                        String mensaje = PrecioSolesFormat + VGuin + PrecioDolarFormat;

                        int i = 0;
                        if (loCursor.getString(3).compareTo("") != 0) {
                            mensaje += " : " + loCursor.getString(3);
                            i++;
                        }

                        if (loCursor.getString(5).compareTo("") != 0) {
                            if (i == 0)
                                mensaje += " : ";
                            else
                                mensaje += " - ";

                            String desCondVenta = dGeneral
                                    .fnDescFormaPagoXCod(loCursor.getString(5));

                            if (desCondVenta.compareTo("") == 0)
                                mensaje += loCursor.getString(5);
                            else
                                mensaje += descCondVta;

                        }

                        object = new ThumbnailItem(mensaje, PrecioSolesFormat + VGuin + PrecioDolarFormat, R.drawable.lista_precio);

                    } else {
                        object = new ThumbnailItem(PrecioSolesFormat + VGuin + PrecioDolarFormat,
                                PrecioSolesFormat + VGuin + PrecioDolarFormat,
                                R.drawable.lista_precio);
                    }

                    object.setTag(liKey);
                    loLista.add(object);

                } while (loCursor.moveToNext());
            }
        }


        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loLista;
    }

    public ArrayList<BeanEnvPedidoDet> fnListNoDBProducto(BeanEnvPedidoCab poBeanPedidoCab) {

        ArrayList<BeanEnvPedidoDet> LoLista = new ArrayList<BeanEnvPedidoDet>();
        // Calculamos los montos cabecera
        poBeanPedidoCab.calcularMontos();
        Iterator<BeanEnvPedidoDet> loIterator = poBeanPedidoCab.getListaDetPedido().iterator();

        while (loIterator.hasNext()) {
            LoLista.add(loIterator.next());
        }

        return LoLista;
    }

    public List<Item> fnListaNoDBProducto(BeanEnvPedidoCab poBeanPedidoCab, String psMoneda, String psNumDecimales) {
        List<Item> LoLista = new ArrayList<Item>();

        // Calculamos los montos cabecera
        poBeanPedidoCab.calcularMontos();

        Iterator<BeanEnvPedidoDet> loIterator = poBeanPedidoCab.getListaDetPedido().iterator();
        BeanEnvPedidoDet loBeanDetPedido = null;
        boolean lbPinta = true;

        SubtitleItem sti = null;
        String lsTitulo1 = "";
        StringBuffer lsTitulo3 = null;

        while (loIterator.hasNext()) {
            loBeanDetPedido = loIterator.next();

            if (lbPinta) {
                LoLista.add(new SeparatorItem("SSS_Total items: "
                        + poBeanPedidoCab.calcularCantTotalItems()));
                LoLista.add(new SeparatorItem("SSS_Monto total: "
                        + psMoneda
                        + " "
                        + Utilitario.fnRound(poBeanPedidoCab
                        .calcularMontoTotal(), Integer
                        .valueOf(psNumDecimales))));
                lbPinta = false;
            }

            lsTitulo1 = loBeanDetPedido.getNombreArticulo().toLowerCase();
            lsTitulo3 = new StringBuffer();
            lsTitulo3
                    .append("SSS_Precio: ")
                    .append(psMoneda)
                    .append(" ")
                    .append(Utilitario.fnRound(loBeanDetPedido.getPrecioBase(),
                            Integer.valueOf(psNumDecimales)))
                    .append("---Cant: ")
                    .append(loBeanDetPedido.getCantidad())
                    .append("_--SubTotal: ")
                    .append(psMoneda)
                    .append(" ")
                    .append(Utilitario.fnRound(loBeanDetPedido.getMonto(),
                            Integer.valueOf(psNumDecimales)));

            sti = new SubtitleItem(lsTitulo1, lsTitulo3.toString());
            sti.setTag(loBeanDetPedido.getIdArticulo());

            LoLista.add(sti);

        }

        loIterator = poBeanPedidoCab.getListaDetPedido().iterator();

        if (LoLista.size() == 0) {
            LoLista.add(new SeparatorItem("No se ha agregado ningun producto"));
        }
        return LoLista;
    }

    public List<Item> fnListaProductoCanje(BeanEnvCanjeCab poBeanCanjeCab) {
        List<Item> LoLista = new ArrayList<Item>();

        // Calculamos los montos cabecera

        Iterator<BeanEnvItemDet> loIterator = poBeanCanjeCab.getLstCanjeDet()
                .iterator();
        BeanEnvItemDet loBeanDetPedido = null;
        boolean lbPinta = true;

        SubtitleItem sti = null;
        String lsTitulo1 = "";
        String lsTitulo3 = "";

        while (loIterator.hasNext()) {
            loBeanDetPedido = loIterator.next();

            if (lbPinta) {
                LoLista.add(new SeparatorItem("Items: "
                        + poBeanCanjeCab.calcularCantTotalItems()));
                lbPinta = false;
            }

            lsTitulo1 = loBeanDetPedido.getNombreArticulo().toLowerCase();
            lsTitulo3 = "Cant: " + loBeanDetPedido.getCantidad();

            sti = new SubtitleItem(lsTitulo1, lsTitulo3);
            sti.setTag(loBeanDetPedido.getIdArticulo());

            LoLista.add(sti);

        }

        loIterator = poBeanCanjeCab.getLstCanjeDet().iterator();

        if (LoLista.size() == 0) {
            LoLista.add(new SeparatorItem("No se ha agregado ningun producto"));
        }
        return LoLista;
    }

    public List<BeanEnvItemDet> fnListaProductoCanje2(
            BeanEnvCanjeCab poBeanCanjeCab) {
        List<BeanEnvItemDet> LoLista = new ArrayList<BeanEnvItemDet>();

        Iterator<BeanEnvItemDet> loIterator = poBeanCanjeCab.getLstCanjeDet()
                .iterator();

        while (loIterator.hasNext())
            LoLista.add(loIterator.next());

        return LoLista;
    }

    public List<Item> fnListaProductoDevolucion(
            BeanEnvDevolucionCab poBeanDevoCab) {
        List<Item> LoLista = new ArrayList<Item>();

        // Calculamos los montos cabecera

        Iterator<BeanEnvItemDet> loIterator = poBeanDevoCab
                .getLstDevolucionDet().iterator();
        BeanEnvItemDet loBeanDetDevolucion = null;
        boolean lbPinta = true;

        SubtitleItem sti = null;
        String lsTitulo1 = "";
        String lsTitulo3 = "";

        while (loIterator.hasNext()) {
            loBeanDetDevolucion = loIterator.next();

            if (lbPinta) {
                LoLista.add(new SeparatorItem("Items: "
                        + poBeanDevoCab.calcularCantTotalItems()));
                lbPinta = false;
            }

            lsTitulo1 = loBeanDetDevolucion.getNombreArticulo().toLowerCase();
            lsTitulo3 = "Cant: " + loBeanDetDevolucion.getCantidad();

            sti = new SubtitleItem(lsTitulo1, lsTitulo3);
            sti.setTag(loBeanDetDevolucion.getIdArticulo());

            LoLista.add(sti);

        }

        loIterator = poBeanDevoCab.getLstDevolucionDet().iterator();

        if (LoLista.size() == 0) {
            LoLista.add(new SeparatorItem("No se ha agregado ningun producto"));
        }
        return LoLista;
    }

    public List<BeanEnvItemDet> fnListaProductoDevolucion2(
            BeanEnvDevolucionCab poBeanDevoCab) {
        List<BeanEnvItemDet> LoLista = new ArrayList<BeanEnvItemDet>();

        // Calculamos los montos cabecera

        Iterator<BeanEnvItemDet> loIterator = poBeanDevoCab
                .getLstDevolucionDet().iterator();
        BeanEnvItemDet loBeanDetDevolucion = null;
        boolean lbPinta = true;

        SubtitleItem sti = null;
        String lsTitulo1 = "";
        String lsTitulo3 = "";

        while (loIterator.hasNext())
            LoLista.add(loIterator.next());

        return LoLista;
    }


    public void subupStockProductos(List<BeanEnvPedidoDet> lstDetallePedidos) {
        BeanEnvPedidoDet detalle;
        for (int i = 0; i < lstDetallePedidos.size(); i++) {
            detalle = lstDetallePedidos.get(i);
            fnUpdStockProducto(detalle.getIdArticulo(), detalle.getStock());
        }
    }

    public void subupStockPresentacion(List<BeanEnvPedidoDet> poLstDetallePedido) {
        BeanEnvPedidoDet loBeanEnvPedidoDet;
        for (int i = 0; i < poLstDetallePedido.size(); i++) {
            loBeanEnvPedidoDet = poLstDetallePedido.get(i);
            fnUpdStockPresentacion(loBeanEnvPedidoDet.getIdArticulo(), loBeanEnvPedidoDet.getStock());
        }
    }

    /**
     * Busca un registro por codigo de producto
     *
     * @param codigoProducto el codigo de producto
     * @return el bean de producto
     */
    public BeanProducto fnSelxCodProductoxCodigo(String codigoProducto) {
        BeanProducto loBeanProducto = new BeanProducto();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"ID", "Codigo", "Nombre", "Stock",
                "UnidadDefecto", "PrecioBase",
                "PrecioBaseSoles","PrecioBaseDolares" //@JBELVY
        };

        String lsTablas = "tbl_producto";
        String lsWhere = "Codigo='" + codigoProducto + "'";

        Cursor loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);
        try {
            if (loCursor != null) {
                if (loCursor.moveToFirst()) {
                    do {
                        loBeanProducto.setId(loCursor.getString(0));
                        loBeanProducto.setCodigo(loCursor.getString(1));
                        loBeanProducto.setNombre(loCursor.getString(2));
                        loBeanProducto.setStock(loCursor.getString(3));
                        loBeanProducto.setUnidadDefecto(loCursor.getString(4));
                        loBeanProducto.setPrecioBase(loCursor.getString(5));
                        loBeanProducto.setPrecioBaseSoles(loCursor.getString(6)); //@JBELVY
                        loBeanProducto.setPrecioBaseDolares(loCursor.getString(7)); //@JBELVY
                    } while (loCursor.moveToNext());
                } else {
                    return null;
                }
            } else {
                return null;
            }

            return loBeanProducto;


        } finally {
            try {
                loCursor.deactivate();
            } catch (Exception e) {
                // TODO: handle exception
            }

            try {
                loCursor.close();
            } catch (Exception e) {
                // TODO: handle exception
            }

            try {
                myDB.close();
            } catch (Exception e) {
                // TODO: handle exception
            }

        }
    }

    public boolean fnExisteProductoSincronizado(String codigoProducto) {
        Boolean sincronizado = false;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"ID", "Codigo", "Nombre", "Stock",
                "UnidadDefecto", "PrecioBase",
                "PrecioBaseSoles","PrecioBaseDolares" //@JBELVY
        };
        String lsTablas = "tbl_producto";
        String lsWhere = "Codigo='" + codigoProducto + "'";

        Cursor loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);
        try {
            if (loCursor != null) {
                if (loCursor.moveToFirst()) {
                    do {
                        sincronizado = true;
                    } while (loCursor.moveToNext());
                } else {
                    return false;
                }
            } else {
                return false;
            }

            return sincronizado;
        } finally {
            try {
                loCursor.deactivate();
            } catch (Exception e) {
                // TODO: handle exception
            }

            try {
                loCursor.close();
            } catch (Exception e) {
                // TODO: handle exception
            }

            try {
                myDB.close();
            } catch (Exception e) {
                // TODO: handle exception
            }

        }
    }

    /**
     * Selecciona un precio de la lista que cumpla con las condiciones
     *
     * @param piIdProducto identificador de producto
     * @param canal        canal del cliente
     * @param condVta      condicion de venta elegida
     * @return El precio
     */
    public String fnSelListPrecioXIdProductoXCanalXCondVta(
            String piIdProducto, String canal, String condVta, String tpMoneda) {
        ArrayList<ThumbnailItem> loLista = new ArrayList<ThumbnailItem>();

        String descCondVta = "";
        DalGeneral loDalGeneral = new DalGeneral(ioContext);

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"PrecioSoles", "PrecioDolares"}; //@JBELVY Before Precio

        String lsTablas = "tbl_listaprecios";
        String lsWhere = "ID = '" + piIdProducto + "' ";

        if (canal.compareTo("") != 0) {
            lsWhere += " AND Canal = '" + canal + "'";
        }

        if (condVta.compareTo("") != 0) {
            lsWhere += " AND CondVta = '" + condVta + "'";
        }

        Cursor loCursor;

        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        ThumbnailItem object = null;
        DalGeneral dGeneral = new DalGeneral(ioContext);
        String liKey = "";
        try {
            if (loCursor != null) {
                if (loCursor.moveToFirst()) {
                    do {
                        if(tpMoneda.equals("1")) { //@JBELVY
                            liKey = loCursor.getString(0);
                        }else{
                            liKey = loCursor.getString(1);
                        }
                    } while (loCursor.moveToNext());
                }
            }
            return liKey;
        } catch (Exception e) {
            return "";
        } finally {
            try {
                loCursor.deactivate();
            } catch (Exception e2) {
                // TODO: handle exception
            }
            try {
                loCursor.close();
            } catch (Exception e2) {
                // TODO: handle exception
            }
            try {
                myDB.close();
            } catch (Exception e2) {
                // TODO: handle exception
            }

        }

    }


    /**
     * Selecciona un precio de la lista que cumpla con las condiciones
     *
     * @param psIdPresentacion identificador de producto
     * @param psCanal          canal del cliente
     * @param psCondVta        condicion de venta elegida
     * @return El precio
     */
    public String fnSelListPrecioXIdPresentacionXCanalXCondVta(
            String psIdPresentacion, String psCanal, String psCondVta, String tpMoneda) {
        ArrayList<ThumbnailItem> loLista = new ArrayList<ThumbnailItem>();

        String descCondVta = "";
        DalGeneral loDalGeneral = new DalGeneral(ioContext);

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"PrecioSoles", "PrecioDolares"}; //@JBELVY Before Precio

        String lsTablas = "TBL_LISTAPRECIOS_PRESENTACION";
        String lsWhere = "IdPresentacion = " + psIdPresentacion + " ";

        if (psCanal.compareTo("") != 0) {
            lsWhere += " AND Canal = '" + psCanal + "'";
        }

        if (psCondVta.compareTo("") != 0) {
            lsWhere += " AND CondVta = '" + psCondVta + "'";
        }

        Cursor loCursor;

        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        ThumbnailItem object = null;
        DalGeneral dGeneral = new DalGeneral(ioContext);
        String liKey = "";
        try {
            if (loCursor != null) {
                if (loCursor.moveToFirst()) {
                    do {
                        if(tpMoneda.equals("1")) {//@JBELVY
                            liKey = loCursor.getString(0);
                        } else{
                            liKey = loCursor.getString(1);
                        }
                    } while (loCursor.moveToNext());
                }
            }
            return liKey;
        } catch (Exception e) {
            return "";
        } finally {
            try {
                loCursor.deactivate();
            } catch (Exception e2) {
                // TODO: handle exception
            }
            try {
                loCursor.close();
            } catch (Exception e2) {
                // TODO: handle exception
            }
            try {
                myDB.close();
            } catch (Exception e2) {
                // TODO: handle exception
            }

        }

    }

    public boolean fnExisteDescuentoVolumen(String isPresentacion, String idArticulo, String idListaPrecio) {
        boolean resultado = false;
        int contador = 0;

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{" count(idDescuento)"};
        String lsTablas = "GRDescuentoVolumen";
        String lsWhere = "";

        if (isPresentacion.equals("T")) {
            lsWhere = "CodigoPresentacion  = '" + idArticulo + "' ";
        } else {
            lsWhere = "CodigoProducto in " +
                    "(select codigo " +
                    "from tbl_producto " +
                    "where id = '" + idArticulo + "')";
        }
        lsWhere += "AND IdListaPrecio = '" + idListaPrecio + "'";


        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        try {

            if (loCursor != null) {
                if (loCursor.moveToFirst()) {
                    do {
                        contador = loCursor.getInt(0);
                    } while (loCursor.moveToNext());
                }
            }
        } catch (Exception e) {

        } finally {
            try {
                loCursor.deactivate();
            } catch (Exception e) {
                // TODO: handle exception
            }

            try {
                loCursor.close();
            } catch (Exception e) {
                // TODO: handle exception
            }

            try {
                myDB.close();
            } catch (Exception e) {
                // TODO: handle exception
            }
        }

        if (contador > 0) {
            resultado = true;
        }

        return resultado;
    }

    public Pair<String, String> fnSelDescuentosVolumen(String isPresentacion, String idArticulo, String IdListaPrecio, String cantidad) {
        Pair<String, String> descuentos = null;

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"DescuentoMinimo", "DescuentoMaximo"};
        String lsTablas = "GRDescuentoVolumen";
        String lsWhere = "";

        if (isPresentacion.equals("T")) {
            lsWhere = "CodigoPresentacion  = '" + idArticulo + "' ";
        } else {
            lsWhere = "CodigoProducto in " +
                    "(select codigo " +
                    "from tbl_producto " +
                    "where id= '" + idArticulo + "')";
        }
        lsWhere += "AND IdListaPrecio = '" + IdListaPrecio + "'";
        lsWhere += " AND '" + cantidad + "' -  RangoMinimo >= 0";
        lsWhere += " AND RangoMaximo - '" + cantidad + "' >= 0";

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        try {

            if (loCursor != null) {
                if (loCursor.moveToFirst()) {
                    do {
                        descuentos = new Pair<String, String>(loCursor.getString(0), loCursor.getString(1));
                    } while (loCursor.moveToNext());
                }
            }
        } catch (Exception e) {

        } finally {
            try {
                loCursor.deactivate();
            } catch (Exception e) {
                // TODO: handle exception
            }

            try {
                loCursor.close();
            } catch (Exception e) {
                // TODO: handle exception
            }

            try {
                myDB.close();
            } catch (Exception e) {
                // TODO: handle exception
            }
        }

        return descuentos;
    }

}