package pe.entel.android.pedidos.act;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;

import java.util.Properties;

import pe.com.nextel.android.actividad.NexPreferenceActivity;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpActualizar;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumRolPreferencia;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.http.HttpSucursal;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.UtilitarioData;
import pe.entel.android.verification.util.Service;

public class ActPreferencias extends NexPreferenceActivity {

    private EditTextPreference txtUrl;
    private CheckBoxPreference chkGrabadoSinSD, chkFueraDeCobertura;
    private Preference preActualizar;
    private Preference preResUsuario;
    private Preference preConfiguracion;
    private Preference preSucursales;
    private String isIpServer;
    private String isUrlActualizar;
    private final byte CONSHTTPSUCURSALES = 3;
    private byte iiCurrentHttp;
    SharedPreferences ioSharedPreferences;

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {

        if (key.equals(getString(R.string.keypre_sd))) {
            chkGrabadoSinSD.setSummary(ioSharedPreferences.getBoolean(key,
                    false) ? getString(R.string.keypreresumen_sdsi)
                    : getString(R.string.keypreresumen_sdno));
        } else if (key.equals(getString(R.string.keypre_url))) {
            txtUrl.setSummary(ioSharedPreferences.getString(key, isIpServer));
            Service.updatePreferenceRuta(this, Configuracion.fnUrl(this, Configuracion.EnumUrl.VERIFICAR));
        }
    }

    @Override
    protected int getNexPreferenceResourceId() {
        return R.xml.preferencias;
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void setNexPreferenceControls() {
        Properties loProperties = Utilitario.readProperties(this);
        isIpServer = loProperties.getProperty("IP_SERVER");
        isUrlActualizar = loProperties.getProperty("URL_ACTUALIZAR");
        Bundle loExtras = getIntent().getExtras();
        int liEnuOrdinal = loExtras.getInt(getString(R.string.sp_permiso), 1);
        EnumRolPreferencia loEnumRolPreferencia = EnumRolPreferencia.values()[liEnuOrdinal];
        chkGrabadoSinSD = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(getString(R.string.keypre_sd));
        chkFueraDeCobertura = (CheckBoxPreference) getPreferenceScreen()
                .findPreference(getString(R.string.keypre_cobertura));
        preActualizar = (Preference) getPreferenceScreen().findPreference(
                getString(R.string.keypre_actualizar));
        preActualizar
                .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference arg0) {
                        new HttpActualizar(ActPreferencias.this,
                                getString(R.string.msg_actualizandoversion),
                                isUrlActualizar).execute();
                        return true;
                    }
                });

        txtUrl = (EditTextPreference) getPreferenceScreen().findPreference(
                getString(R.string.keypre_url));

        if (txtUrl == null) {
            if (ioSharedPreferences.getString(getString(R.string.keyPrefVerifSucursal), "").equals("S"))
                txtUrl.setText(ioSharedPreferences.getString(getString(R.string.keyPrefIPSucursal), isIpServer));
            else
                txtUrl.setText(isIpServer);
        }

        preResUsuario = (Preference) getPreferenceScreen().findPreference(getString(R.string.keypre_resusuario));
        preResUsuario.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference arg0) {
                try {
                    String lsBeanUsuario = getSharedPreferences(
                            ConfiguracionNextel.CONSPREFERENCIA,
                            MODE_PRIVATE).getString("BeanUsuario", "");

                    if (!lsBeanUsuario.equals("")) {
                        BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                .fromJson(lsBeanUsuario,
                                        BeanUsuario.class);

                        DalPendiente loDalPendiente = new DalPendiente(ActPreferencias.this);

                        if (!loDalPendiente.fnExistePendientes(loBeanUsuario.getCodVendedor())) {
                            UtilitarioData.fnCrearPreferencesPutString(ActPreferencias.this, Configuracion.CONSPRECODUSUARIO, "");
                            UtilitarioData.fnCrearPreferencesPutBoolean(ActPreferencias.this, Configuracion.CONSPREUSURESETEADO, true);

                            Intent loIntent = new Intent(ActPreferencias.this, ActLogin.class);
                            finish();
                            new NexToast(ActPreferencias.this, getString(R.string.actpreferencias_toausureseteado) + loBeanUsuario.getNombre(), EnumType.ACCEPT).show();
                            startActivity(loIntent);
                        } else {
                            new NexToast(ActPreferencias.this, getString(R.string.actpreferencias_toausuenvpendientes) + loBeanUsuario.getNombre(), EnumType.CANCEL).show();
                        }
                    } else {
                        new NexToast(ActPreferencias.this, R.string.actpreferencias_toausunologueado, EnumType.CANCEL).show();
                    }
                    return true;
                } catch (Exception e) {
                    subShowException(e);
                    return false;
                }
            }
        });

        preConfiguracion = (Preference) getPreferenceScreen().findPreference(getString(R.string.keypre_configuracion));

        preConfiguracion.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                ((AplicacionEntel) getApplication()).setPrimeraVez(true);
                finish();
                return false;
            }
        });
        preSucursales = (Preference) getPreferenceScreen().findPreference(getString(R.string.keypre_sucursal));
        preSucursales.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                try {
                    String lsIdEmpresaa = Utilitario.readProperties(ActPreferencias.this).getProperty("ID_EMPRESA");
                    try {
                        iiCurrentHttp = CONSHTTPSUCURSALES;
                        new HttpSucursal(lsIdEmpresaa, ActPreferencias.this).execute();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return false;
            }
        });

        // Deshabilitar los que son de administracion
        ioSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (loEnumRolPreferencia == EnumRolPreferencia.USUARIO) {
            txtUrl.setEnabled(false);
            chkGrabadoSinSD.setEnabled(false);
        }

        String flagCobertura = Configuracion.EnumConfiguracion.MODO_FUERA_DE_COBERTURA.getValor(this);
        if (flagCobertura.equals(Configuracion.FLGFALSO)) {
            chkFueraDeCobertura.setEnabled(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ioSharedPreferences.getString(getString(R.string.keyPrefVerifSucursal), "").equals("S"))
            txtUrl.setSummary(ioSharedPreferences.getString(getString(R.string.keyPrefIPSucursal), isIpServer));
        else
            txtUrl.setSummary(ioSharedPreferences.getString(getString(R.string.keypre_url), isIpServer));

        chkGrabadoSinSD
                .setSummary(ioSharedPreferences.getBoolean(
                        getString(R.string.keypre_sd), false) ? getString(R.string.keypreresumen_sdsi)
                        : getString(R.string.keypreresumen_sdno));
    }

    @Override
    protected String getDatabaseName() {
        return getString(R.string.db_name);
    }

    @Override
    protected boolean OnPreVerifyAdministrator() {
        int liEnuOrdinal = getIntent().getExtras().getInt(getString(R.string.sp_permiso), 1);
        EnumRolPreferencia ioEnumRolPreferencia = EnumRolPreferencia.values()[liEnuOrdinal];
        if (ioEnumRolPreferencia == EnumRolPreferencia.ADMINISTRADOR)
            return false;
        return super.OnPreVerifyAdministrator();
    }

    @Override
    protected boolean OnVerifyAdministrator(String psAdminUser,
                                            String psAdminPass) {
        if (psAdminUser.equals(getString(R.string.keypreadm_usuario))
                && psAdminPass.equals(getString(R.string.keypreadm_password))) {
            getIntent().putExtra(getString(R.string.sp_permiso), 0);
            Bundle loExtras = getIntent().getExtras();
            int liEnuOrdinal = loExtras.getInt(getString(R.string.sp_permiso),
                    1);
            EnumRolPreferencia ioEnumRolPreferencia = EnumRolPreferencia
                    .values()[liEnuOrdinal];
            if (ioEnumRolPreferencia == EnumRolPreferencia.USUARIO) {
                txtUrl.setEnabled(false);
                chkGrabadoSinSD.setEnabled(false);
                return false;
            } else {
                txtUrl.setEnabled(true);
                chkGrabadoSinSD.setEnabled(true);
                return true;
            }
        } else {
            new NexToast(ActPreferencias.this,
                    R.string.actpreferencias_toaadmiusupassincorrecto,
                    EnumType.CANCEL).show();
        }
        return false;
    }

    @Override
    public void subAccDesMensaje() {
        if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {
            finish();
            startActivity(new Intent(ActPreferencias.this, ActListaSucursales.class));
        }
    }
}
