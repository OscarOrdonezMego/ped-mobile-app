package pe.entel.android.pedidos.http;

import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import pe.com.nextel.android.actividad.NexInvalidOperatorActivity.NexInvalidOperatorException;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.BDFramework;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanConsulta;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumUrl;

public class HttpConsultaPedidoOnline extends HttpConexion {

    BeanConsulta iConsulta;
    final BDFramework ioDB;
    private final Context context;

    private String isURL;
    private boolean ibResultado;
    private boolean ibSD;
    private String isDBName;
    private KeyguardLock ioKeyguardLock;
    private int iiDBVersion;

    public HttpConsultaPedidoOnline(BeanConsulta consulta, Context poContext, EnumTipActividad poEnumTipActividad)
            throws NexInvalidOperatorException {
        super(poContext, poContext
                .getString(R.string.hhtppedidorealizado_dlgconexion), poEnumTipActividad);
        iConsulta = consulta;
        ioDB = new BDFramework(poContext);
        context = poContext;
    }

    @Override
    public boolean OnPreConnect() {
        KeyguardManager myKeyGuard = (KeyguardManager) ioContext.getSystemService(Context.KEYGUARD_SERVICE);
        ioKeyguardLock = myKeyGuard.newKeyguardLock("JUAN");
        ioKeyguardLock.disableKeyguard();

        if (!Utilitario.fnVerSignal(ioContext)) {// logueo off Line
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        isURL = Configuracion.fnUrl(ioContext, EnumUrl.CONSPEDIDOONLINE);
        SharedPreferences loSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ioContext);
        ibSD = loSharedPreferences.getBoolean(ioContext.getString(R.string.keypre_sd), true);
        isDBName = ioContext.getString(R.string.db_name);
        iiDBVersion = Integer.parseInt(ioContext.getString(R.string.db_version));
        return true;
    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    private void subConHttp() {
        //HttpParams httpParameters = new BasicHttpParams();
        //HttpConnectionParams.setConnectionTimeout(httpParameters, Configuracion.TIEMPOTIMEOUT);
        //HttpConnectionParams.setSoTimeout(httpParameters, Configuracion.TIEMPOTIMEOUT);
        //HttpClient loHttpClient = new DefaultHttpClient(httpParameters);

        String lsParGet = "?codUsuario=" + iConsulta.getCodVendedor().trim()
                + "&fecha=" + iConsulta.getFechaConsulta().trim()
                + "&codCliente=" + iConsulta.getCodCliente()
                + "&tipoArticulo=" + iConsulta.getTipoArticulo();
        //HttpPost loHttpPost = null;

        //loHttpPost = new HttpPost(isURL + lsParGet);
        Log.v("URL", isURL + lsParGet);
        //loHttpPost.addHeader("Accept-Encoding", "gzip");
        //HttpResponse loHttpResponse;

        BeanMapper loBeanHttpResponse = new BeanMapper();
        try {
            java.net.URL url = new URL(isURL + lsParGet);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.connect();
            InputStream loInputStream = conn.getInputStream();

            //loHttpResponse = loHttpClient.execute(loHttpPost);
            //InputStream loInputStream = loHttpResponse.getEntity().getContent();
            //String lsRespuesta = loHttpResponse.getFirstHeader("Resultado").getValue();
            //int lsIdRespuesta = Integer.parseInt(loHttpResponse.getFirstHeader("IdResultado").getValue());
            String lsRespuesta = conn.getHeaderField("Resultado");
            int lsIdRespuesta = Integer.parseInt(conn.getHeaderField("IdResultado"));

            if (lsIdRespuesta != ConfiguracionNextel.CONSRESSERVIDOROK) {
                loBeanHttpResponse = new BeanMapper();
                loBeanHttpResponse.setIdResultado(ConfiguracionNextel.CONSRESSERVIDORERROR);
                loBeanHttpResponse.setResultado(lsRespuesta);
                setHttpResponseObject(loBeanHttpResponse);
                return;
            }

            //loHttpClient.getConnectionManager().closeExpiredConnections();
            //loInputStream = new GZIPInputStream(loInputStream);

            ibResultado = false;
            Log.v("XXX", "ruta grabado" + String.valueOf(ibSD));
            ibResultado = fnSinSD(loInputStream, isDBName, iiDBVersion, ioDB, ibSD, context, context.getString(R.string.app_name));

            if (ibResultado == true) {
                loBeanHttpResponse = new BeanMapper();
                loBeanHttpResponse.setIdResultado(lsIdRespuesta);
                loBeanHttpResponse.setResultado(lsRespuesta);
                setHttpResponseObject(loBeanHttpResponse);
                return;
            } else {

                loBeanHttpResponse = new BeanMapper();
                loBeanHttpResponse.setIdResultado(ConfiguracionNextel.CONSRESSERVIDORALGUNERROR);
                loBeanHttpResponse.setResultado(ioDB.getDB_LAST_ERROR());
                setHttpResponseObject(loBeanHttpResponse);
                return;
            }
        /*} catch (ClientProtocolException e) {
            loBeanHttpResponse = new BeanMapper();
            loBeanHttpResponse.setIdResultado(ConfiguracionNextel.CONSRESSERVIDORERROR);
            loBeanHttpResponse.setResultado(e.getMessage());
            setHttpResponseObject(loBeanHttpResponse);
            return;*/
        } catch (IOException e) {
            loBeanHttpResponse = new BeanMapper();
            loBeanHttpResponse.setIdResultado(ConfiguracionNextel.CONSRESSERVIDORERROR);
            loBeanHttpResponse.setResultado(e.getMessage());
            setHttpResponseObject(loBeanHttpResponse);
            return;

        } catch (Exception e) {
            loBeanHttpResponse = new BeanMapper();
            loBeanHttpResponse.setIdResultado(ConfiguracionNextel.CONSRESSERVIDORALGUNERROR);
            loBeanHttpResponse.setResultado("*" + e.getMessage());
            setHttpResponseObject(loBeanHttpResponse);
        }
    }

    @Override
    public void OnPostConnect() {
        ioKeyguardLock.reenableKeyguard();
        if (getHttpResponseObject() != null) {
            BeanMapper loBeanHttpResponse = (BeanMapper) getHttpResponseObject();
            if (loBeanHttpResponse.getIdResultado() == ConfiguracionNextel.CONSRESSERVIDORALGUNERROR) {
                loBeanHttpResponse.setIdResultado(ConfiguracionNextel.CONSRESSERVIDORERROR);
                if (loBeanHttpResponse.getResultado().indexOf("*") > -1)
                    loBeanHttpResponse.setResultado(ioContext
                            .getString(R.string.msg_error) + loBeanHttpResponse.getResultado());
                else
                    loBeanHttpResponse.setResultado(ioContext
                            .getString(R.string.msg_scripterror) + loBeanHttpResponse.getResultado());

            } else if (loBeanHttpResponse.getIdResultado() == ConfiguracionNextel.CONSRESSERVIDORERROR) {
                loBeanHttpResponse.setResultado(ioContext
                        .getString(R.string.msg_httperror) + loBeanHttpResponse.getResultado());
            } else {
                loBeanHttpResponse.setIdResultado(2);
                loBeanHttpResponse.setResultado("");
            }

            setHttpResponseIdMessage(loBeanHttpResponse.getIdResultado(), loBeanHttpResponse.getResultado());
        }
    }
}
