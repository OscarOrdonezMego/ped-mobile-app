package pe.entel.android.pedidos.act;

import greendroid.widget.ActionBarGD.Type;
import java.util.List;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanClienteDireccion;
import pe.entel.android.pedidos.bean.BeanEnvCanjeCab;
import pe.entel.android.pedidos.bean.BeanEnvDevolucionCab;
import pe.entel.android.pedidos.bean.BeanEnvPago;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanEnvPendientes;
import pe.entel.android.pedidos.bean.BeanProspecto;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalCanje;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalDevolucion;
import pe.entel.android.pedidos.dal.DalPago;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.http.HttpGrabaPendiente;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

public class ActGrabaPendiente extends NexActivity {

    List<BeanEnvPedidoCab> loListPedido = null;
    List<BeanEnvPago> loListPago = null;
    List<BeanEnvCanjeCab> loListCanje = null;
    List<BeanEnvDevolucionCab> loListDevo = null;
    List<BeanClienteDireccion> loListDireccion = null;
    List<BeanProspecto> loListProspecto = null;

    BeanEnvPendientes loBEnvPend = null;
    private final int ENVIOPENDIENTESMENSAJE = 1;
    BeanUsuario loBeanUsuario = null;
    private String isVerificacionPedidoWS;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.startServiceTransaction(this);
        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);
        isVerificacionPedidoWS = Configuracion.EnumConfiguracion.VERIFICACION_PEDIDO.getValor(this);

        super.onCreate(savedInstanceState);
        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));

        //if (!Utilitario.fnVerOperador(this))
        //    startActivity(new Intent(this, ActOpeInvalido.class));

        try {
            String codigoVendedor = loBeanUsuario.getCodVendedor();
            if(isVerificacionPedidoWS.equals(Configuracion.FLGVERDADERO))
                loListPedido = new DalPedido(this).fnEnvioNoPedidosPendientes(codigoVendedor);
            else
                loListPedido = new DalPedido(this).fnEnvioPedidosPendientes(codigoVendedor);

            loListPago = new DalPago(this).fnEnvioPagosPendientes(codigoVendedor);
            loListCanje = new DalCanje(this).fnEnvioCanjesPendientes(codigoVendedor);
            loListDevo = new DalDevolucion(this).fnEnvioDevolucionesPendientes(codigoVendedor);
            loListDireccion = new DalCliente(this).fnEnvioDireccionPendientes();
            loListProspecto = new DalCliente(this).fnEnvioProspectosPendientes();

            loBEnvPend = new BeanEnvPendientes();
            loBEnvPend.setListPedidos(loListPedido);
            loBEnvPend.setListPagos(loListPago);
            loBEnvPend.setListCanje(loListCanje);
            loBEnvPend.setListDevolucion(loListDevo);
            loBEnvPend.setListDirecciones(loListDireccion);
            loBEnvPend.setListProspectos(loListProspecto);
            if (loListPedido.size() == 0 && loListPago.size() == 0
                    && loListCanje.size() == 0 && loListDevo.size() == 0 &&
                    loListDireccion.size() == 0 && loListProspecto.size() == 0) {
                UtilitarioPedidos.mostrarDialogo(this,ENVIOPENDIENTESMENSAJE);
            } else {
                new HttpGrabaPendiente(loBEnvPend, ActGrabaPendiente.this,
                        fnTipactividad()).execute();
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    public void subIniActionBar() {
        this.getActionBarGD().setType(Type.Empty);
    }

    @Override
    public void subSetControles() {
        setActionBarContentView(R.layout.enviohttp);
        super.subSetControles();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case ENVIOPENDIENTESMENSAJE:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage("No existen registros pendientes de envio")
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        finish();
                                    }
                                }).create();
                return loAlertDialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    @SuppressWarnings({"deprecation", "unused"})
    @Override
    public void subAccDesMensaje() {
        try {
            Intent intentl = new Intent(ActGrabaPendiente.this, ActMenu.class);
            intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP); // JUAN

            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {

                if(isVerificacionPedidoWS.equals(Configuracion.FLGVERDADERO))
                    new DalPedido(this).fnUpdEstadoNoPedidoPendiente(loBeanUsuario);
                else
                    new DalPedido(this).fnUpdEstadoPedidosPendientes(loBeanUsuario);

                new DalPago(this).fnUpdEstadoPagoPendiente(loBeanUsuario);
                new DalCanje(this).fnUpdEstadoCanjePendiente(loBeanUsuario);

                DalDevolucion loDalDevo = new DalDevolucion(this);
                loDalDevo.fnUpdEstadoDevolucionPendiente(loBeanUsuario);

                if (loListDevo.size() > 0)
                    for (BeanEnvDevolucionCab loBeanEnvDevolucionCab : loListDevo)
                        if (loBeanEnvDevolucionCab.getLstDevolucionDet() != null)
                            if (loBeanEnvDevolucionCab.getLstDevolucionDet().size() > 0)
                                loDalDevo.fnValidarStock(loBeanEnvDevolucionCab.getLstDevolucionDet());

                new DalCliente(this).fnUpdEstadoProspectoPendiente(loBeanUsuario);

                finish();
            } else {
                finish();
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }
}