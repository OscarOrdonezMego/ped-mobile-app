package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanRespuesta<T> {

    @JsonProperty
    private int Codigo;
    @JsonProperty
    private String Mensaje;
    @JsonProperty
    private T General;

    public int getCodigo() {
        return Codigo;
    }

    public void setCodigo(int codigo) {
        Codigo = codigo;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public T getGeneral() {
        return General;
    }

    public void setGeneral(T general) {
        General = general;
    }
}
