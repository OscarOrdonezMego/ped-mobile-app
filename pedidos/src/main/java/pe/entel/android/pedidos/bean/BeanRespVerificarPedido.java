package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by rtamayov on 24/09/2015.
 */
public class BeanRespVerificarPedido {

    @JsonProperty("Codigo")
    private int codigo;
    @JsonProperty("General")
    private BeanRespVerificarPedidoGeneral general;
    @JsonProperty("Mensaje")
    private String mensaje;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public BeanRespVerificarPedidoGeneral getGeneral() {
        return general;
    }

    public void setGeneral(BeanRespVerificarPedidoGeneral general) {
        this.general = general;
    }


    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
