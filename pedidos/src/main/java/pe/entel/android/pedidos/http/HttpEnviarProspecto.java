package pe.entel.android.pedidos.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanProspecto;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.UtilitarioData;

/**
 * Created by tkongr on 11/01/2016.
 */
public class HttpEnviarProspecto extends HttpConexion {

    private BeanProspecto ioBeanProspecto;
    private Context ioContext;

    public HttpEnviarProspecto(BeanProspecto beanProspecto, Context poContext, ConfiguracionNextel.EnumTipActividad enumTipActividad) {
        super(poContext, poContext.getString(R.string.httpprospecto_dlg), enumTipActividad);
        ioBeanProspecto = beanProspecto;
        ioContext = poContext;
    }

    @Override
    public boolean OnPreConnect() {
        SharedPreferences loSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ioContext);
        if (loSharedPreferences.getBoolean(ioContext.getString(R.string.keypre_cobertura), false)) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_modofueracobertura));
            return false;
        }

        int signal = UtilitarioData.fnSignalLevel(ioContext);
        if (!Utilitario.fnVerSignal(ioContext) || signal < 2) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        try {
            //RestTemplate restTemplate = new RestTemplate();
            //restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            Log.v("URL", Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.ENVIOPROSPECTO));
            Log.v("REQUEST", BeanMapper.toJson(ioBeanProspecto, false));

            //String lsResultado = restTemplate.postForObject(Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.ENVIOPROSPECTO), ioBeanProspecto, String.class);
            String lsResultado = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.ENVIOPROSPECTO), ioBeanProspecto);
            setHttpResponseObject(lsResultado);

        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, "Error HTTP: " + e.getMessage());
        }
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            String lsResultado = (String) getHttpResponseObject();
            String[] laRespuesta = lsResultado.split(";");
            int liIdHttpRespuesta = Integer.valueOf(laRespuesta[0]);
            String lsHttpRespuesta = laRespuesta[1];
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.get(liIdHttpRespuesta), lsHttpRespuesta);
        }
    }
}
