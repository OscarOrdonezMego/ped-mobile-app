package pe.entel.android.pedidos.http;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.BDFramework;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.util.Configuracion;

/**
 * Created by tkongr on 06/01/2016.
 */
public class HttpSucursal extends HttpConexion {

    private final String TAG = "HttpSucursal";
    final BDFramework ioDB;
    private String isURL;
    private KeyguardManager.KeyguardLock ioKeyguardLock;
    private boolean ibSD;
    private String isDBName;
    private boolean ibResultado;
    private final Context context;
    private int iiDBVersion;

    private final String IdEmpresa;

    public HttpSucursal(String poIdEmpresa, Context poContext) throws Exception {

        super(poContext, poContext.getString(R.string.httpsucursales_dlgbusqueda));

        IdEmpresa = poIdEmpresa;
        ioDB = new BDFramework(poContext);
        context = poContext;
    }

    @Override
    public boolean OnPreConnect() {

        KeyguardManager myKeyGuard = (KeyguardManager) ioContext.getSystemService(Context.KEYGUARD_SERVICE);
        ioKeyguardLock = myKeyGuard.newKeyguardLock("JUAN");
        ioKeyguardLock.disableKeyguard();

        if (!Utilitario.fnVerSignal(ioContext)) {// logueo off Line
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        isURL = Configuracion.fnUrlService(ioContext, Configuracion.EnumUrlService.GETSUCURSALES);
        SharedPreferences loSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ioContext);

        ibSD = loSharedPreferences.getBoolean(ioContext.getString(R.string.keypre_sd), true);
        isDBName = ioContext.getString(R.string.db_name);
        iiDBVersion = Integer.parseInt(ioContext.getString(R.string.db_version));
        return true;

    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    @Override
    public void OnPostConnect() {
        // TODO Auto-generated method stub
        ioKeyguardLock.reenableKeyguard();
        if (getHttpResponseObject() != null) {
            BeanMapper loBeanHttpResponse = (BeanMapper) getHttpResponseObject();
            if (loBeanHttpResponse.getIdResultado() == ConfiguracionNextel.CONSRESSERVIDORALGUNERROR) {
                loBeanHttpResponse.setIdResultado(ConfiguracionNextel.CONSRESSERVIDORERROR);
                if (loBeanHttpResponse.getResultado().indexOf("*") > -1)
                    loBeanHttpResponse.setResultado(ioContext.getString(R.string.msg_error) + loBeanHttpResponse.getResultado());
                else
                    loBeanHttpResponse.setResultado(ioContext.getString(R.string.msg_scripterror) + loBeanHttpResponse.getResultado());
            } else if (loBeanHttpResponse.getIdResultado() == ConfiguracionNextel.CONSRESSERVIDORERROR) {
                loBeanHttpResponse.setResultado(ioContext.getString(R.string.msg_httperror) + loBeanHttpResponse.getResultado());
            } else if (loBeanHttpResponse.getIdResultado() == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {
                loBeanHttpResponse.setIdResultado(ConfiguracionNextel.CONSRESSERVIDOROKNOMSG);
                loBeanHttpResponse.setResultado(loBeanHttpResponse.getResultado());
            }
            setHttpResponseIdMessage(loBeanHttpResponse.getIdResultado(), loBeanHttpResponse.getResultado());
        }
    }

    public void subConHttp() {
        //HttpParams httpParameters = new BasicHttpParams();
        //HttpConnectionParams.setConnectionTimeout(httpParameters, Configuracion.TIEMPOTIMEOUT);
        //HttpConnectionParams.setSoTimeout(httpParameters, Configuracion.TIEMPOTIMEOUT);
        //HttpClient loHttpClient = new DefaultHttpClient(httpParameters);
        //Envio por GET
        String lsParGet = "?IdEmpresa=" + IdEmpresa;
        //HttpPost loHttpPost = null;

        isURL = isURL + lsParGet;

        //loHttpPost = new HttpPost(isURL);
        Log.v(TAG, "subConHttp.isURL: " + isURL);

        //loHttpPost.addHeader("Accept-Encoding", "gzip");
        //HttpResponse loHttpResponse;
        BeanMapper loBeanHttpResponse = new BeanMapper();
        try {
            java.net.URL url = new URL(isURL + lsParGet);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.connect();
            InputStream loInputStream = conn.getInputStream();

            //loHttpResponse = loHttpClient.execute(loHttpPost);
            //InputStream loInputStream = loHttpResponse.getEntity().getContent();
            //String lsRespuesta = loHttpResponse.getFirstHeader("Resultado").getValue();
            //int lsIdRespuesta = Integer.parseInt(loHttpResponse.getFirstHeader("IdResultado").getValue());
            String lsRespuesta = conn.getHeaderField("Resultado");
            int lsIdRespuesta = Integer.parseInt(conn.getHeaderField("IdResultado"));

            Log.i("---------$$$$$$", "lsIdRespuesta: " + lsIdRespuesta);
            if (lsIdRespuesta != ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {
                loBeanHttpResponse = new BeanMapper();
                loBeanHttpResponse.setIdResultado(ConfiguracionNextel.CONSRESSERVIDORERROR);
                loBeanHttpResponse.setResultado(lsRespuesta);
                setHttpResponseObject(loBeanHttpResponse);
                return;
            }

            //loHttpClient.getConnectionManager().closeExpiredConnections();
            //loInputStream = new GZIPInputStream(loInputStream);

            ibResultado = false;
            Log.v("XXX", "ruta grabado" + String.valueOf(ibSD));
            ibResultado = fnSinSD(loInputStream,
                    isDBName,
                    iiDBVersion,
                    ioDB,
                    ibSD,
                    context,
                    context.getString(R.string.app_name));

            Log.i("---------$$$$$$", "ibResultado: " + ibResultado);

            if (ibResultado) {
                loBeanHttpResponse = new BeanMapper();
                loBeanHttpResponse.setIdResultado(lsIdRespuesta);
                loBeanHttpResponse.setResultado(lsRespuesta);
                setHttpResponseObject(loBeanHttpResponse);
                return;
            } else {
                loBeanHttpResponse = new BeanMapper();
                loBeanHttpResponse.setIdResultado(ConfiguracionNextel.CONSRESSERVIDORALGUNERROR);
                loBeanHttpResponse.setResultado(ioDB.getDB_LAST_ERROR());
                setHttpResponseObject(loBeanHttpResponse);
                return;
            }
        /*} catch (ClientProtocolException e) {
            loBeanHttpResponse = new BeanMapper();
            loBeanHttpResponse.setIdResultado(ConfiguracionNextel.CONSRESSERVIDORERROR);
            loBeanHttpResponse.setResultado(e.getMessage());
            setHttpResponseObject(loBeanHttpResponse);
            return;*/
        } catch (IOException e) {
            loBeanHttpResponse = new BeanMapper();
            loBeanHttpResponse.setIdResultado(ConfiguracionNextel.CONSRESSERVIDORERROR);
            loBeanHttpResponse.setResultado(e.getMessage());
            setHttpResponseObject(loBeanHttpResponse);
            return;
        } catch (Exception e) {
            loBeanHttpResponse = new BeanMapper();
            loBeanHttpResponse.setIdResultado(ConfiguracionNextel.CONSRESSERVIDORALGUNERROR);
            loBeanHttpResponse.setResultado("*" + e.getMessage());
            setHttpResponseObject(loBeanHttpResponse);
            return;
        }
    }
}
