package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by tkongr on 14/10/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanEficienciaOnline {

    @JsonProperty("descripcion")
    private String descripcion;

    @JsonProperty("valor")
    private String valor;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

}
