package pe.entel.android.pedidos.bean;

/**
 * Created by Dsb Mobile on 26/03/2015.
 */
public class BeanPrecio {

    private String id;
    private String precio;
    private String precioSoles;//@JBELVY
    private String precioDolares;//@JBELVY
    private String descuentoMinimo;
    private String descuentoMaximo;
    //@JBELVY -F

    public BeanPrecio() {
        id = "";
        precio = "0";
        precioSoles = "0";//@JBELVY
        precioDolares = "0";//@JBELVY
        descuentoMinimo = "0";
        descuentoMaximo = "0";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrecio() {
        return precio;
    }
    public String getPrecioSoles() {
        return precioSoles;
    }//@JBELVY
    public String getPrecioDolares() {
        return precioDolares;
    }//@JBELVY

    public void setPrecio(String precio) {
        this.precio = precio;
    }
    public void setPrecioSoles(String precioSoles) {
        this.precioSoles = precioSoles;
    }//@JBELVY
    public void setPrecioDolares(String precioDolares) {
        this.precioDolares = precioDolares;
    }//@JBELVY

    public String getDescuentoMinimo() {
        return descuentoMinimo;
    }

    public void setDescuentoMinimo(String descuentoMinimo) {
        this.descuentoMinimo = descuentoMinimo;
    }

    public String getDescuentoMaximo() {
        return descuentoMaximo;
    }

    public void setDescuentoMaximo(String descuentoMaximo) {
        this.descuentoMaximo = descuentoMaximo;
    }
}
