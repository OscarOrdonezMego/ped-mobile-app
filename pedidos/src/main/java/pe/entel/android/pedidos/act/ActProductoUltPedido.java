package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import greendroid.widget.ActionBarItem;
import greendroid.widget.ActionBarItem.Type;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.actividad.NexInvalidOperatorActivity.NexInvalidOperatorException;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.adap.AdapProductoPedido;
import pe.entel.android.pedidos.bean.BeanAlmacen;
import pe.entel.android.pedidos.bean.BeanArticulo;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanEnvPedidoDet;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalAlmacen;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalKPI;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.dal.DalProducto;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

@SuppressWarnings("unused")
public class ActProductoUltPedido extends NexActivity implements
        OnItemClickListener, NexMenuCallbacks, MenuSlidingListener {

    private final int EDIT_ID = 1;
    private final int DELETE_ID = 2;
    private final int CANCEL_ID = 3;

    private final int CONSADDPROD = 0;
    private final int CONSFINAL = 1;
    private final int CONSBACK = 3;
    private final int CONSNOITEMS = 4;
    private final int CONSLIMITEEXCEDIDO = 5;
    private final int CONSVALEDIT = 6;

    BeanEnvPedidoCab loBeanPedidoCab = null;
    BeanEnvPedidoDet loBeanPedidoDet = null;
    BeanCliente loBeanCliente = null;

    private ArrayList<BeanEnvPedidoDet> iolista = null;

    private TextView txtcantproductos, txtmontototal,
            txtmontototalDolar, //@JBELVY
            txttotalitems, txtfletetotal;
    private LinearLayout lnContentHeader;
    private LinearLayout lnFlete;
    private ListView lstproductopedido;
    BeanUsuario loBeanUsuario = null;
    private AdapMenuPrincipalSliding _menu;

    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDASINCRONIZACION = 11;
    private final int ENVIOPENDIENTES = 12;
    private final int VALIDASINCRO = 13;
    private final int NOHAYPENDIENTES = 14;

    private final int ACTBARUP = 31;
    private final int ACTBARDOWN = 32;
    private final int ACTBARADD = 33;


    Animation animSlideUp;
    Animation animSideDown;

    boolean estadoSincronizar = false;

    // Integracion Ntrack
    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro

    //PARAMETROS DE CONFIGURACION;
    private String isMoneda;
    private String isMonedaDolar;
    private String isNumDecVista;
    private String isMostrarCondVenta;
    private String isBonificacion;
    private String isPrecioCondVenta;
    private String isPrecioTipoCliente;
    private String isDescuento;
    private String isMostrarDirPedido;
    private String isMostrarAlmacen;
    private String isPresentacion;
    private String isMostrarCabeceraPedido;
    private String isBonificacionAutomatica;
    private String isMaximoNumeroDecimales;
    private String isVerificarPedido;
    private int iiMaximoNumeroDecimales;
    private String isStockRestrictivo;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);
        String lsBeanUsuario = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario,
                BeanUsuario.class);

        isMoneda = Configuracion.EnumConfiguracion.SIMBOLO_MONEDA.getValor(this);
        isMonedaDolar = "$";
        isNumDecVista = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(this);
        isMostrarCondVenta = Configuracion.EnumConfiguracion.MOSTRAR_CONDICION_VENTA.getValor(this);
        isPrecioCondVenta = Configuracion.EnumConfiguracion.PRECIO_CONDICION_VENTA.getValor(this);
        isBonificacion = Configuracion.EnumFuncion.BONIFICACION.getValor(this);
        isPrecioTipoCliente = Configuracion.EnumConfiguracion.PRECIO_TIPO_CLIENTE.getValor(this);
        isDescuento = Configuracion.EnumFuncion.DESCUENTO.getValor(this);
        isMostrarDirPedido = Configuracion.EnumConfiguracion.MOSTRAR_DIRECCION_PEDIDO.getValor(this);
        isMostrarAlmacen = Configuracion.EnumConfiguracion.MOSTRAR_ALMACEN.getValor(this);
        isPresentacion = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(this);
        isMostrarCabeceraPedido = Configuracion.EnumConfiguracion.MOSTRAR_CABECERA_PEDIDO.getValor(this);
        isBonificacionAutomatica = Configuracion.EnumConfiguracion.BONIFICACION_AUTOMATICA.getValor(this);
        isMaximoNumeroDecimales = Configuracion.EnumConfiguracion.MAXIMO_NUMERODECIMALES.getValor(this);
        isVerificarPedido = Configuracion.EnumConfiguracion.VERIFICACION_PEDIDO.getValor(this);
        isStockRestrictivo = Configuracion.EnumConfiguracion.STOCK_RESTRICTIVO.getValor(this);
        setMaximoNumeroDecimales();

        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);

        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));

        subIniMenu();
    }

    @Override
    public void onBackPressed() {
        showDialog(CONSBACK);
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setTitle(
                getString(R.string.actultimopedido_bardetalle));
        addActionBarItem(Type.Nex_Paste, ACTBARADD);
        subIniActionBar(Type.Nex_ArrowDown, ACTBARDOWN);
    }

    @Override
    protected void subSetControles() {
        // TODO Auto-generated method stub
        super.subSetControles();
        Log.i("subSetControles", "ingrese");

        try {
            setActionBarContentView(R.layout.productoultpedido);

            txtcantproductos = (TextView) findViewById(R.id.actproductopedido_txtcantproductos);
            txtmontototal = (TextView) findViewById(R.id.actproductopedido_txtmontototal);
            txtmontototalDolar = (TextView) findViewById(R.id.actproductopedido_txtmontototalDolar);
            txttotalitems = (TextView) findViewById(R.id.actproductopedido_txttotalitems);
            txtfletetotal = (TextView) findViewById(R.id.actproductopedido_txtfletetotal);
            lstproductopedido = (ListView) findViewById(R.id.actproductopedido_lstproductopedido);
            lnContentHeader = (LinearLayout) findViewById(R.id.lnContentHeader);
            lnFlete = (LinearLayout) findViewById(R.id.actproductopedido_lnflete);
            lstproductopedido.setOnItemClickListener(this);

            animations();
        } catch (Exception e) {
            Log.e("subSetControles", e.toString());
            subShowException(e);
        }
        Log.i("subSetControles", "acabee");
    }

    private void setMaximoNumeroDecimales() {
        iiMaximoNumeroDecimales = 0;
        try {
            iiMaximoNumeroDecimales = Integer.parseInt(isMaximoNumeroDecimales);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ActProductoDetalle", "Error conversion maximo numero de decimales: " + e.getMessage());
        }
    }

    private void animations() {
        animSideDown = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);

        animSlideUp = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);

        animSlideUp.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                lnContentHeader.setVisibility(View.GONE);
                subIniActionBar(Type.Nex_ArrowUp, ACTBARUP);
            }
        });
        animSideDown.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                // TODO Auto-generated method stub
                lnContentHeader.setVisibility(View.VISIBLE);
                subIniActionBar(Type.Nex_ArrowDown, ACTBARDOWN);
            }
        });

    }

    public void subIniActionBar(Type type, int itemId) {
        addActionBarItem(type, itemId);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        subLLenarLista();
    }

    public void subLLenarLista() {

        loBeanPedidoCab = ((AplicacionEntel) getApplication())
                .getCurrentPedido();

        DalCliente loDalCliente = new DalCliente(this);
        loBeanCliente = loDalCliente.fnSelxCodCliente(loBeanPedidoCab
                .getCodigoCliente());

        if (((AplicacionEntel) getApplication()).getCurrentConsultaClienteOnline() == null) {
            loBeanCliente = loDalCliente.fnSelxCodCliente(loBeanPedidoCab.getCodigoCliente());
        } else {
            loBeanCliente = ((AplicacionEntel) getApplication()).getCurrentCliente();
        }

        txtcantproductos.setText(String.valueOf(loBeanPedidoCab
                .getListaDetPedido().size()));
        txttotalitems.setText(Utilitario.fnRound(loBeanPedidoCab.calcularCantTotalItems(), iiMaximoNumeroDecimales));

        if (isVerificarPedido.equals(Configuracion.FLGVERDADERO)) {
            txtmontototal.setText(isMoneda
                    + " "
                    + Utilitario.fnRound(loBeanPedidoCab.calcularMontoTotalVerificarPedidoSoles(), //@JBELVY Before calcularMontoTotalVerificarPedido
                    Integer.valueOf(isNumDecVista)));

            txtmontototalDolar.setText(isMonedaDolar
                    + " "
                    + Utilitario.fnRound(loBeanPedidoCab.calcularMontoTotalVerificarPedidoDolar(),
                    Integer.valueOf(isNumDecVista))); //@JBELVY
        } else {
            txtmontototal.setText(isMoneda
                    + " "
                    + Utilitario.fnRound(loBeanPedidoCab.calcularMontoTotalSoles(), //@JBELVY Before calcularMontoTotal
                    Integer.valueOf(isNumDecVista)));

            txtmontototalDolar.setText(isMonedaDolar
                    + " "
                    + Utilitario.fnRound(loBeanPedidoCab.calcularMontoTotalDolar(),
                    Integer.valueOf(isNumDecVista)));
        }


        if (!TextUtils.isEmpty(loBeanPedidoCab.getCodAlmacen())
                && !loBeanPedidoCab.getCodAlmacen().equals("0")) {
            lnFlete.setVisibility(View.VISIBLE);
            txtfletetotal.setText(isMoneda
                    + " "
                    + Utilitario.fnRound(loBeanPedidoCab.calcularTotalFlete(),
                    Integer.valueOf(isNumDecVista)));
        } else
            lnFlete.setVisibility(View.GONE);

        DalProducto loDalProducto = new DalProducto(this);
        iolista = loDalProducto.fnListNoDBProducto(loBeanPedidoCab);

        AdapProductoPedido adap = new AdapProductoPedido(this,
                R.layout.productopedido_item, iolista);
        lstproductopedido.setAdapter(adap);
    }

    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        if (item.getContentDescription().toString()
                .equals(getString(R.string.gd_nex_arrow_down))) {
            getActionBarGD().removeItem(item);
            lnContentHeader.startAnimation(animSlideUp);

        } else if (item.getContentDescription().toString()
                .equals(getString(R.string.gd_nex_arrow_up))) {
            getActionBarGD().removeItem(item);
            lnContentHeader.setVisibility(View.VISIBLE);
            lnContentHeader.startAnimation(animSideDown);

        } else if (item.getContentDescription().toString()
                .equals(getString(R.string.gd_nex_paste))) {

            Intent loIntent = null;
            long iiIdCliente;
            ((AplicacionEntel) getApplication())
                    .setCurrentCodFlujo(Configuracion.CONSPEDIDO);

            subCreatePreference();

            if (isMostrarDirPedido.equals(Configuracion.FLGVERDADERO) &&
                    new DalCliente(ActProductoUltPedido.this)
                            .fnSelNumClienteDireccion(loBeanCliente
                                    .getClicod(), Configuracion
                                    .EnumFlujoDireccion.PEDIDO
                                    .getCodigo(ActProductoUltPedido.this)) > 0) {

                loIntent = new Intent(ActProductoUltPedido.this,
                        ActPedidoClienteDireccionLista.class);
                loIntent.putExtra(Configuracion.EXTRA_NAME_DIRECCION,
                        Configuracion.EnumFlujoDireccion.PEDIDO.name());

            } else if (isMostrarCabeceraPedido.equals(Configuracion.FLGVERDADERO)) {
                loIntent = new Intent(ActProductoUltPedido.this,
                        ActPedidoFormulario.class);
                loIntent.putExtra(Configuracion.EXTRA_FORMULARIO,
                        Configuracion.FORMULARIO_INICIO);
                loIntent.putExtra(Configuracion.EXTRA_FORMULARIO_INICIO_EDITAR, false);

            } else if (isMostrarCondVenta.equals(Configuracion.FLGVERDADERO)) {
                loIntent = new Intent(ActProductoUltPedido.this,
                        ActPedidoCondicionVenta.class);

            } else {
                loIntent = new Intent(ActProductoUltPedido.this,
                        ActProductoPedido.class);
            }

            loIntent.putExtra("IdCliente", loBeanCliente.getClicod());
            finish();
            startActivity(loIntent);
            return true;

        } else {
            return super.onHandleActionBarItemClick(item, position);
        }

        return true;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {
        Log.i("subaccdesmensaje", "entrroo");
        if (estadoSincronizar)
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActMenu.class);

                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }

    }

    public void irclientedetalle() {
        Intent loIntent = new Intent(this, ActClienteDetalle.class);
        loIntent.putExtra("IdCliente", Long.parseLong(loBeanCliente.getClipk()));
        finish();
        startActivity(loIntent);
    }

    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        DialogFragmentYesNo loDialog;
        switch (id) {
            case DIALOG_NSERVICES_DOWNLOAD:
                String lsNServices;
                if (ioDALNservices == null)
                    try {
                        ioDALNservices = DALNServices.getInstance(this,
                                R.string.db_name);
                    } catch (NexInvalidOperatorException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                try {
                    lsNServices = ioDALNservices.fnSelNServicesLabel();
                } catch (Exception e) {
                    Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                    lsNServices = getString(R.string.nservices_label_default);
                }
                loDialog = DialogFragmentYesNo.newInstance(
                        getFragmentManager(),
                        DialogFragmentYesNo.TAG + DIALOG_NSERVICES_DOWNLOAD,
                        getString(R.string.nservices_consultingdownlad).replace(
                                "{0}", lsNServices),
                        EnumLayoutResource.getDefaultIconHelp(this));
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    subIniNServicesDownload(ioDALNservices
                                            .fnSelNServicesAPK());
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {
                            }
                        });
            case CONSVALEDIT:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSVALEDIT"),
                        getString(R.string.actproductopedido_dlgnomodificar),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case CONSLIMITEEXCEDIDO:
                loDialog = DialogFragmentYesNo
                        .newInstance(
                                getFragmentManager(),
                                DialogFragmentYesNo.TAG
                                        .concat("CONSLIMITEEXCEDIDO"),
                                getString(R.string.actproductopedido_dlglimiteexcedido)
                                        + getString(R.string.actproductopedido_cantmaxregistros),
                                R.drawable.boton_informacion, false,
                                EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case CONSNOITEMS:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSNOITEMS"),
                        getString(R.string.actproductopedido_noitems),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case CONSBACK:

                irclientedetalle();


                return null;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.actmenu_dlgsincronizar),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                try {
                                    String lsBeanUsuario = getSharedPreferences(
                                            "Actual", MODE_PRIVATE).getString(
                                            "BeanUsuario", "");
                                    BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                            .fromJson(lsBeanUsuario,
                                                    BeanUsuario.class);
                                    estadoSincronizar = true;
                                    new HttpSincronizar(loBeanUsuario
                                            .getCodVendedor(),
                                            ActProductoUltPedido.this,
                                            fnTipactividad()).execute();
                                } catch (Exception e) {
                                    subShowException(e);
                                }
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            case VALIDASINCRONIZACION:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"),
                        getString(R.string.actmenuvalsincro_titulo),
                        getString(R.string.actmenuvalsincro_mensaje),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case VALIDASINCRO:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                        getString(R.string.msgregpendientesenvinfo),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog, null);

            case ENVIOPENDIENTES:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"),
                        getString(R.string.msgdeseaenviarpendientes),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                return new DialogFragmentYesNoPairListener(loDialog,
                        new OnDialogFragmentYesNoClickListener() {

                            @Override
                            public void performButtonYes(DialogFragmentYesNo dialog) {
                                Intent loIntentEspera = new Intent(
                                        ActProductoUltPedido.this,
                                        ActGrabaPendiente.class);
                                startActivity(loIntentEspera);
                            }

                            @Override
                            public void performButtonNo(DialogFragmentYesNo dialog) {

                            }
                        });

            default:
                return super.onCreateNexDialog(id);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSVALEDIT:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actproductopedido_dlgnomodificar))

                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSLIMITEEXCEDIDO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actproductopedido_dlglimiteexcedido)
                                        + getString(R.string.actproductopedido_cantmaxregistros))

                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSNOITEMS:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.actproductopedido_noitems))

                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSBACK:

                irclientedetalle();


                return loAlertDialog;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences(
                                                    "Actual", MODE_PRIVATE)
                                                    .getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                                    .fromJson(lsBeanUsuario,
                                                            BeanUsuario.class);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActProductoUltPedido.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActProductoUltPedido.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;
            default:
                return super.onCreateDialog(id);
        }

    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                            long arg3) {
        // TODO Auto-generated method stub

        // Si ha presionado elemento superior a la cabecera negra

        StringBuffer mensaje = new StringBuffer();
        // Se le quita uno adicional por la cabecera negra
        BeanEnvPedidoDet bp = (BeanEnvPedidoDet) loBeanPedidoCab
                .getListaDetPedido().get(position);

        String descuento = "";
        if (bp.getTipoDescuento().compareTo("M") == 0) {
            descuento = getString(R.string.moneda) + " " + bp.getDescuento();
        } else if (bp.getTipoDescuento().compareTo("P") == 0) {
            descuento = bp.getDescuento() + " %";
        }
        mensaje.append(" C\u00F3digo : ")
                .append(bp.getCodigoArticulo())
                .append('\n')
                .append(" Nombre : ")
                .append(bp.getNombreArticulo())
                .append('\n');

        if (!TextUtils.isEmpty(loBeanPedidoCab.getCodAlmacen())
                && !loBeanPedidoCab.getCodAlmacen().equals("0")) {
            BeanAlmacen almacen = new DalAlmacen(ActProductoUltPedido.this)
                    .fnSelectXCodProductoXCodAlmacen(bp.getIdArticulo(),
                            bp.getCodAlmacen());
            if (almacen != null)
                mensaje.append(" Almacen : ")
                        .append(almacen.getNombre())
                        .append('\n');
        }
        String tipoMoneda = bp.getTipoMoneda();

        mensaje.append(" Cantidad : ")
                .append(bp.getCantidad())
                .append('\n')
                .append(" Precio Base (")
                .append((tipoMoneda.equals("1") ? isMoneda : isMonedaDolar)) //@JBELVY
                .append("): ")
                .append(Utilitario.fnRound((tipoMoneda.equals("1") ? bp.getPrecioBaseSoles() : bp.getPrecioBaseDolares()),  //@JBELVY
                        Integer.valueOf(isNumDecVista)))
                .append('\n');
        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            mensaje.append(" Frac : ")
                    .append(bp.getCantidadFrac())
                    .append('\n');
        }
        if (isBonificacion.equals(Configuracion.FLGVERDADERO)) {
            mensaje.append(" Bonificaci\u00F3n : ")
                    .append(bp.getBonificacion())
                    .append('\n');
        }

        if (isDescuento.equals(Configuracion.FLGVERDADERO)) {
            mensaje.append(" Descuento : ")
                    .append(descuento)
                    .append('\n');
        }
        mensaje.append(" Subtotal (")
                .append((tipoMoneda.equals("1") ? isMoneda : isMonedaDolar)) //@JBELVY
                .append("): ")
                .append(Utilitario.fnRound((tipoMoneda.equals("1") ? bp.getMontoSoles() : bp.getMontoDolar()), //@JBELVY
                        Integer.valueOf(isNumDecVista)))
                .append('\n');

        if (!TextUtils.isEmpty(loBeanPedidoCab.getCodAlmacen())
                && !loBeanPedidoCab.getCodAlmacen().equals("0")) {
            mensaje.append(" Flete (")
                    .append(isMoneda)
                    .append("): ")
                    .append(Utilitario.fnRound(bp.getFlete(),
                            Integer.valueOf(isNumDecVista)))
                    .append('\n');
        }
        mensaje.append(" Observaci\u00F3n : ")
                .append(bp.getObservacion());

        AlertDialog.Builder ale = new AlertDialog.Builder(this);
        ale.setMessage(mensaje.toString());
        ale.setPositiveButton("Aceptar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        ale.show();

    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent loIntentLogin = new Intent(ActProductoUltPedido.this,
                        ActLogin.class);
                loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(loIntentLogin);
            }
        });

    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        Intent intentl = new Intent(this, ActMenu.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intentl);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = null;
            DalPedido loDalPedido = new DalPedido(this);
            loList = loDalPedido.fnEnvioPedidosPendientes(loBeanUsuario
                    .getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);

                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();

                showDialog(CONSDIASINCRONIZAR);
            } else {
                showDialog(VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub
        String lsBeanUsuario = getSharedPreferences(
                "Actual", MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper
                .fromJson(lsBeanUsuario,
                        BeanUsuario.class);
        this.setMsgOk("Eficiencia Online");

        BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
        loBeanListaEficienciaOnline.setIdResultado(0);
        loBeanListaEficienciaOnline.setResultado("");
        loBeanListaEficienciaOnline.setListaEficiencia(null);
        loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

        new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

        Intent loInstent = new Intent(this, ActKpiDetalle.class);
        loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loInstent);
    }

    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        ((AplicacionEntel) getApplication()).setCurrentCliente(null);
        ((AplicacionEntel) getApplication())
                .setCurrentCodFlujo(Configuracion.CONSSTOCK);
        ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
        Intent loIntento = new Intent(ActProductoUltPedido.this,
                ActProductoListaOnline.class);
        loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        BeanExtras loBeanExtras = new BeanExtras();
        loBeanExtras.setFiltro("");
        loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
        String lsFiltro = "";
        try {
            lsFiltro = BeanMapper.toJson(loBeanExtras, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
        startActivity(loIntento);
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(
                    getString(R.string.ml_sincronizarantes)));
        }

    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */
    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

    private void subCreatePreference() {
        // CREAMOS LA CABECERA

        if (((AplicacionEntel) getApplication()).getCurrentKpi() == null) {
            DalKPI lodkpi = new DalKPI(this);
            ((AplicacionEntel) getApplication()).setCurrentKpi(lodkpi
                    .fnSelCurrentKPI());
        }
        ((AplicacionEntel) getApplication()).getCurrentKpi().tiempoInicioTemporal = System
                .currentTimeMillis();

        Calendar c = Calendar.getInstance();
        Date loDate = c.getTime();
        String sFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate)
                .toString();

        if (!isMostrarCondVenta.equals(Configuracion.FLGVERDADERO)) {
            loBeanPedidoCab.setCondicionVenta("");
        }


        String lsBeanUsuario = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString(
                "BeanUsuario", "");
        loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario,
                BeanUsuario.class);


        loBeanPedidoCab.setEmpresa(loBeanUsuario.getCodCompania());
        loBeanPedidoCab.setCodigoUsuario(loBeanUsuario.getCodVendedor());
        loBeanPedidoCab.setFechaInicio(sFecha);
        loBeanPedidoCab.setFechaFin("");
        loBeanPedidoCab.setCodigoCliente(loBeanCliente.getClicod());
        loBeanPedidoCab.setClipk(loBeanCliente.getClipk());
        loBeanPedidoCab.setCodDireccion("0");
        loBeanPedidoCab.setErrorConexion("0");
        loBeanPedidoCab.setErrorPosicion("0");
        loBeanPedidoCab.setFlgTipo(Configuracion.FLGPEDIDO);

        DalProducto loDalProducto = new DalProducto(this);


        String lsCanal = "";
        String lsCondVenta = "";
        Double ldTotal = 0.00;
        Double ldTotalSoles = 0.00;
        Double ldTotalDolar = 0.00;

        for (int i = loBeanPedidoCab.getListaDetPedido().size() - 1; i >= 0; i--) {
            BeanEnvPedidoDet iobeanpedidodet = (BeanEnvPedidoDet) loBeanPedidoCab.getListaDetPedido().get(i);

            BeanArticulo loBeanArticulo;
            if (isPresentacion.equals(Configuracion.FLGVERDADERO))
                loBeanArticulo = loDalProducto.fnSelxIdPresentacion(iobeanpedidodet.getIdArticulo());
            else
                loBeanArticulo = loDalProducto.fnSelxIdProducto(iobeanpedidodet.getIdArticulo());


            if (loBeanArticulo != null) {

                iobeanpedidodet.setIdArticulo(loBeanArticulo.getId());
                iobeanpedidodet.setPrecioBase(loBeanArticulo.getPrecioBase());
                iobeanpedidodet.setPrecioBaseSoles(loBeanArticulo.getPrecioBaseSoles()); //@JBELVY
                iobeanpedidodet.setPrecioBaseDolares(loBeanArticulo.getPrecioBaseDolares()); //@JBELVY
                if (isPrecioCondVenta.equals(Configuracion.FLGVERDADERO)) {
                    lsCondVenta = loBeanPedidoCab.getCondicionVenta();
                    if (lsCondVenta == null) {
                        lsCondVenta = "";
                    }
                }
                if (isPrecioTipoCliente.equals(Configuracion.FLGVERDADERO)) {
                    lsCanal = loBeanCliente.getTclicod();

                    if (lsCanal == null) {
                        lsCanal = "";
                    }
                }

                String lsPrecio = "";
                String lsPrecioSoles = "";
                String lsPrecioDolar = "";
                if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                    lsPrecio = loDalProducto.fnSelListPrecioXIdPresentacionXCanalXCondVta(loBeanArticulo.getId(), lsCanal, lsCondVenta, "1");
                    lsPrecioSoles = loDalProducto.fnSelListPrecioXIdPresentacionXCanalXCondVta(loBeanArticulo.getId(), lsCanal, lsCondVenta, "1");
                    lsPrecioDolar = loDalProducto.fnSelListPrecioXIdPresentacionXCanalXCondVta(loBeanArticulo.getId(), lsCanal, lsCondVenta, "2");
                } else {
                    lsPrecio = loDalProducto.fnSelListPrecioXIdProductoXCanalXCondVta(loBeanArticulo.getId(), lsCanal, lsCondVenta, "1");
                    lsPrecioSoles = loDalProducto.fnSelListPrecioXIdProductoXCanalXCondVta(loBeanArticulo.getId(), lsCanal, lsCondVenta, "1");
                    lsPrecioDolar = loDalProducto.fnSelListPrecioXIdProductoXCanalXCondVta(loBeanArticulo.getId(), lsCanal, lsCondVenta, "2");
                }
                if (!TextUtils.isEmpty(lsPrecio)) {
                    iobeanpedidodet.setPrecioBase(lsPrecio);
                }
                if (!TextUtils.isEmpty(lsPrecioSoles)) {
                    iobeanpedidodet.setPrecioBaseSoles(lsPrecioSoles);
                }
                if (!TextUtils.isEmpty(lsPrecioDolar)) {
                    iobeanpedidodet.setPrecioBaseDolares(lsPrecioDolar);
                }

                Double monto = Double.parseDouble(iobeanpedidodet.getPrecioBase()) * Double.parseDouble(iobeanpedidodet.getCantidad());
                Double montoSoles = Double.parseDouble(iobeanpedidodet.getPrecioBaseSoles()) * Double.parseDouble(iobeanpedidodet.getCantidad()); //JBELVY
                Double montoDolar = Double.parseDouble(iobeanpedidodet.getPrecioBaseDolares()) * Double.parseDouble(iobeanpedidodet.getCantidad()); //JBELVY
                iobeanpedidodet.setMonto(monto.toString());
                iobeanpedidodet.setMontoSoles(montoSoles.toString()); //JBELVY
                iobeanpedidodet.setMontoDolar(montoDolar.toString()); //JBELVY

                iobeanpedidodet.setDescuento("0.00");
                iobeanpedidodet.setCodPedido("");
                ldTotal = ldTotal + Double.parseDouble(iobeanpedidodet.getMonto());
                ldTotalSoles = ldTotalSoles + Double.parseDouble(iobeanpedidodet.getMontoSoles()); //JBELVY
                ldTotalDolar = ldTotalDolar + Double.parseDouble(iobeanpedidodet.getMontoDolar()); //JBELVY

            } else {
                loBeanPedidoCab.getListaDetPedido().remove(i);
            }
            loBeanPedidoCab.setMontoTotal(ldTotal.toString());
            loBeanPedidoCab.setMontoTotalSoles(ldTotalSoles.toString()); //JBELVY
            loBeanPedidoCab.setMontoTotalDolar(ldTotalDolar.toString()); //JBELVY
        }
        if (isBonificacionAutomatica.equals(Configuracion.FLGVERDADERO)) {
            try {
                DalPedido loDalPedido = new DalPedido(this);
                loBeanPedidoCab = loDalPedido.fnValidarBonificacion(loBeanPedidoCab, iiMaximoNumeroDecimales, isStockRestrictivo);
            } catch (Exception e) {
                Log.e("ActProductoUltPedido", "subCreatePreference", e);
            }
        }

        // creo o edito mi sharedpreference
        String lsBeanVisitaCab = "";
        try {
            lsBeanVisitaCab = BeanMapper.toJson(loBeanPedidoCab, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("beanPedidoCab", lsBeanVisitaCab);
        loEditor.commit();

    }
}