package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Pair;
import pe.entel.android.pedidos.R;

public class DalTipoCambio {

    private Context ioContext;

    public DalTipoCambio(Context psClase) {

        ioContext = psClase;
    }

    public boolean existeTable() {
        boolean lbResult = false;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        String[] laColumnasMed = new String[]{"name"};
        String lsTabla = "sqlite_master";
        String lsWhere = "upper(name)='TBL_TIPO_CAMBIO'";
        Cursor loCursor = myDB.query(true, lsTabla, laColumnasMed, lsWhere,
                null, null, null, null, null);
        if (loCursor.getCount() == 1) {
            lbResult = true;
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return lbResult;
    }

    public Pair<Integer, String> fnObtenerTipoCambio(String TipoMoneda) {
        Pair<Integer, String> tipoCambio = null;

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"TC_PK", "TC_VALOR" };
        String lsTablas = "TBL_TIPO_CAMBIO";
        String lsWhere = "TC_CODIGO='" + TipoMoneda + "'";
        Cursor loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);

        try {

            if (loCursor != null) {
                if (loCursor.moveToFirst()) {
                    do {
                        tipoCambio = new Pair<Integer, String>(loCursor.getInt(0), loCursor.getString(1));
                    } while (loCursor.moveToNext());
                }
            }
        } catch (Exception e) {

        } finally {
            try {
                loCursor.deactivate();
            } catch (Exception e) {
                // TODO: handle exception
                tipoCambio = new Pair<>(0, "");
            }

            try {
                loCursor.close();
            } catch (Exception e) {
                tipoCambio = new Pair<>(0, "");
                // TODO: handle exception
            }

            try {
                myDB.close();
            } catch (Exception e) {
                tipoCambio = new Pair<>(0, "");
                // TODO: handle exception
            }
        }

        return tipoCambio;
    }

    public String fnObtenerIdTipoCambioPorMoneda(String TipoMoneda) {

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"TC_PK" };
        String lsTablas = "TBL_TIPO_CAMBIO";
        String lsWhere = "TC_CODIGO='" + TipoMoneda + "'";
        Cursor loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null,
                null, null);
        String TipoCambioId = "";
        try {
            if (loCursor != null) {
                if (loCursor.moveToFirst()) {
                    do {
                        TipoCambioId = loCursor.getString(0);
                    } while (loCursor.moveToNext());
                }
            }
            return TipoCambioId;
        } catch (Exception e) {
            return "0";
        } finally {
            try {
                loCursor.deactivate();
            } catch (Exception e2) {
                return "0";
                // TODO: handle exception
            }
            try {
                loCursor.close();
            } catch (Exception e2) {
                // TODO: handle exception
                return "0";
            }
            try {
                myDB.close();
            } catch (Exception e2) {
                // TODO: handle exception
                return "0";
            }

        }

    }
}
