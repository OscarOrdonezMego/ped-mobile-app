package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import greendroid.widget.ActionBarGD.Type;
import greendroid.widget.ActionBarItem;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALGestor.DALException;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumServerResponse;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.util.Gps;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalCanje;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalDevolucion;
import pe.entel.android.pedidos.dal.DalPago;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumConfiguracion;
import pe.entel.android.pedidos.util.Configuracion.EnumModulo;
import pe.entel.android.pedidos.util.UtilitarioData;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

public class ActMenu extends NexActivity implements OnItemClickListener {

    public static ArrayList<EnumMenuPrincipalItem> goArrItem = new ArrayList<EnumMenuPrincipalItem>();
    BeanUsuario loBeanUsuario = null;
    List<BeanEnvPedidoCab> loList = null;
    private DALNServices ioDALNservices;
    private String isGps, isEficiencia, isSeguimientoRuta, isPedido;

    @SuppressWarnings("unused")
    private DalPendiente ioDalPendiente = null;
    private GridView ioGridViewMenu;
    private EnumMenuPrincipalItem iiProceso;


    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);
        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);

        isGps = EnumConfiguracion.GPS.getValor(this);
        isEficiencia = EnumConfiguracion.EFICIENCIA.getValor(this);
        isSeguimientoRuta = EnumConfiguracion.SEGUIMIENTO_RUTA.getValor(this);
        isPedido = EnumModulo.PEDIDO.getValor(this);

        super.onCreate(savedInstanceState);

        try {
            UtilitarioPedidos.IndicarMensajeOkError(this);
            ((AplicacionEntel) getApplication()).setCurrentUsuario(loBeanUsuario);

            if (isGps.equals(Configuracion.FLGVERDADERO)) {
                Gps.startListening(this);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        addModule();
        try {
            ioGridViewMenu.setAdapter(new MenuPrincipalAdapter(this));
        } catch (Exception e) {
            Log.e("OnResumen", e.toString());
        }
    }

    private void addModule() {

        goArrItem.clear();
        goArrItem.add(EnumMenuPrincipalItem.INICIAR);
        goArrItem.add(EnumMenuPrincipalItem.SINCRONIZAR);
        goArrItem.add(EnumMenuPrincipalItem.PENDIENTES);

        if (isEficiencia.equals(Configuracion.FLGVERDADERO))
            goArrItem.add(EnumMenuPrincipalItem.EFICIENCIA);

        if (isPedido.equals(Configuracion.FLGVERDADERO))
            goArrItem.add(EnumMenuPrincipalItem.STOCK);

        goArrItem.add(EnumMenuPrincipalItem.SALIR);

        if (isSeguimientoRuta.equals(Configuracion.FLGVERDADERO))
            goArrItem.add(EnumMenuPrincipalItem.NSERVICES);
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setType(Type.Empty);
        this.getActionBarGD().setTitle(getString(R.string.actmenu_bartitulo));

        if (!loBeanUsuario.getFlgEstilo().equals(""))
            addActionBarItem(greendroid.widget.ActionBarItem.Type.Edit, 1);
    }

    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {

        if (item.getContentDescription().toString().equals(getString(R.string.gd_edit))) {
            loBeanUsuario.setFlgEstilo((loBeanUsuario.getFlgEstilo().equals("0")) ? "1" : "0");

            String lsBeanUsuario = "";
            try {
                lsBeanUsuario = BeanMapper.toJson(loBeanUsuario, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            UtilitarioData.fnCrearPreferencesPutString(this, "BeanUsuario", lsBeanUsuario);
            startActivity(new Intent(ActMenu.this, ActMenu.class));
            finish();

        } else {
            return super.onHandleActionBarItemClick(item, position);
        }
        return false;
    }

    @Override
    protected void subSetControles() {
        super.subSetControles();
        try {
            setActionBarContentView(R.layout.menu2);
            ioDalPendiente = new DalPendiente(this);
            ioGridViewMenu = (GridView) findViewById(R.id.menuprincipal_griditems);
            ioGridViewMenu.setOnItemClickListener(this);

        } catch (Exception e) {
            Log.e("subSetControles", e.toString());
            subShowException(e);
        }
    }

    @SuppressWarnings("rawtypes")
    public class MenuPrincipalAdapter extends ArrayAdapter {

        private final TypedArray ioArrImage = obtainStyledAttributes(R.styleable.PedidosMenuItemsDrawables);
        private final String[] ioArrText = getResources().getStringArray(R.array.menuprincipalitems_strings);

        @SuppressWarnings("unchecked")
        public MenuPrincipalAdapter(Context poContext) {
            super(poContext, R.layout.menuprincipal_item, new String[goArrItem.size()]);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.menuprincipal_item, parent, false);
            TextView tv = (TextView) row.findViewById(R.id.menuprincipalitem_text);
            tv.setText(goArrItem.get(position).getText(ioArrText));
            tv.setCompoundDrawablesWithIntrinsicBounds(0, goArrItem.get(position).getImageResource(ioArrImage), 0, 0);
            row.setTag(R.string.keytag_menuprincipalitem, goArrItem.get(position));

            return row;
        }
    }

    @SuppressWarnings("unused")
    private enum EnumMenuPrincipalItem {
        INICIAR, SINCRONIZAR, PENDIENTES, EFICIENCIA, STOCK, SALIR, NSERVICES;

        private String extraInfo = "";

        public int getImageResource(Context context) {
            return context.obtainStyledAttributes(
                    R.styleable.PedidosMenuItemsDrawables).getResourceId(
                    ordinal(), -1);
        }

        public int getImageResource(TypedArray array) {
            return array.getResourceId(ordinal(), -1);
        }

        public String getText(Context context) {
            return context.getResources().getStringArray(
                    R.array.menuprincipalitems_strings)[ordinal()]
                    .concat(extraInfo);
        }

        public String getText(String[] array) {
            return array[ordinal()].concat(extraInfo);
        }

        public void setExtraInfo(String extraInfo) {
            this.extraInfo = extraInfo;
        }
    }

    private void cargarPedido() {
        ((AplicacionEntel) getApplication()).setCurrentCodFlujo(Configuracion.CONSPEDIDO);
        startActivity(new Intent(ActMenu.this, ActProductoPedido.class));
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        // TODO Auto-generated method stub
        try {

            switch ((EnumMenuPrincipalItem) arg1.getTag(R.string.keytag_menuprincipalitem)) {
                case INICIAR:
                    String recuperarPedido = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("flagRecuperarPedido", "");
                    if (recuperarPedido != null && recuperarPedido.equals(Configuracion.FLGVERDADERO)) {
                        showDialog(Constantes.CONSRECUPERARPEDIDO);
                    } else {

                        boolean lbResult = new DalCliente(this).existeTable();

                        if (lbResult) {
                            Intent loInstent = new Intent(this, ActClienteBuscar.class);
                            BeanExtras loBeanExtras = new BeanExtras();
                            loBeanExtras.setFiltro("");
                            loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
                            String lsFiltro = "";
                            try {
                                lsFiltro = BeanMapper.toJson(loBeanExtras, false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            loInstent.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
                            finish();
                            startActivity(loInstent);
                        } else {
                            UtilitarioPedidos.mostrarDialogo(this, Constantes.VALIDASINCRONIZACION);
                        }
                    }
                    break;
                case SINCRONIZAR:
                    loList = new DalPedido(this).fnEnvioPedidosPendientes(loBeanUsuario.getCodVendedor());
                    if (loList.size() <= 0) {
                        DalPedido lDalPedido = new DalPedido(this);
                        lDalPedido.fnDelTrDetPedido();
                        lDalPedido.fnDelTrCabPedido();
                        UtilitarioPedidos.mostrarDialogo(this, Constantes.CONSDIASINCRONIZAR);
                    } else {
                        UtilitarioPedidos.mostrarDialogo(this, Constantes.VALIDASINCRO);
                    }
                    break;
                case PENDIENTES:
                    boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
                    UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
                    break;
                case EFICIENCIA:
                    loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);
                    iiProceso = EnumMenuPrincipalItem.EFICIENCIA;
                    this.setMsgOk("Eficiencia Online");

                    BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
                    loBeanListaEficienciaOnline.setIdResultado(0);
                    loBeanListaEficienciaOnline.setResultado("");
                    loBeanListaEficienciaOnline.setListaEficiencia(null);
                    loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());
                    loBeanListaEficienciaOnline.setUsuariosVendedores(null);
                    loBeanListaEficienciaOnline.setPerfilUsuario(loBeanUsuario.getPerfil());

                    new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();
                    break;
                case STOCK:
                    startActivity(new Intent(ActMenu.this, ActProductoListaOnline.class).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    break;
                case SALIR:
                    UtilitarioPedidos.mostrarDialogo(this, Constantes.CONSSALIR);
                    break;
                case NSERVICES:
                    iiProceso = EnumMenuPrincipalItem.NSERVICES;
                    try {
                        UtilitarioPedidos.subCallNServices(this, ioDALNservices, false, loBeanUsuario.getFlgNServicesObligatorio().equals(Configuracion.FLGVERDADERO));
                    } catch (DALException ex) {
                        UtilitarioPedidos.mostrarDialogo(this, Constantes.VALIDASINCRONIZACION);
                    }

                    break;
                default:
                    break;
            }

        } catch (Exception e) {
            subShowException(e);
        }

    }

    @Override
    protected Dialog onCreateDialog(int id) {

        AlertDialog loAlertDialog = null;

        switch (id) {
            case Constantes.NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            BeanUsuario loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(ActMenu.this);
                                            ((AplicacionEntel) getApplication()).uploadEvent(Configuracion.SINCRONIZACION_EVENT);
                                            new HttpSincronizar(loBeanUsuario.getCodVendedor(), ActMenu.this, fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActMenu.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.CONSRECUPERARPEDIDO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgrecuperarpedido))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        cargarPedido();
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);
                                        startActivity(new Intent(ActMenu.this, ActClienteBuscar.class));
                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.CONSSALIR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsalir))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        Intent loIntentLogin = new Intent(ActMenu.this, ActLogin.class);
                                        loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        finish();
                                        startActivity(loIntentLogin);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;
            default:
                return super.onCreateDialog(id);
        }
    }

    @SuppressWarnings("unused")
    private int fnCalculaTotalPendientes() throws Exception {
        // Se calcula numero de pendientes
        BeanUsuario loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);
        int npedpen;
        try {
            npedpen = new DalPedido(this).fnNumPedidosPendientes(loBeanUsuario.getCodVendedor());
        } catch (Exception e) {
            npedpen = 0;
        }
        try {
            npedpen = npedpen + new DalPago(this).fnNumPagosPendientes(loBeanUsuario.getCodVendedor());
        } catch (Exception e) {
            npedpen = 0;
        }
        try {
            npedpen = npedpen + new DalCanje(this).fnNumCanjesPendientes(loBeanUsuario.getCodVendedor());
        } catch (Exception e) {
            npedpen = 0;
        }
        try {
            npedpen = npedpen + new DalDevolucion(this).fnNumDevolucionesPendientes(loBeanUsuario.getCodVendedor());
        } catch (Exception e) {
            npedpen = 0;
        }

        return npedpen;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {
        try {
            if (getEnumIdHttpResultado() == EnumServerResponse.OK) {
                if (iiProceso == EnumMenuPrincipalItem.NSERVICES && isSeguimientoRuta.equals(Configuracion.FLGVERDADERO)) {
                    UtilitarioPedidos.subCallNServices(this, ioDALNservices, true, loBeanUsuario.getFlgNServicesObligatorio().equals(Configuracion.FLGVERDADERO));

                } else if (iiProceso == EnumMenuPrincipalItem.SINCRONIZAR) {
                    addModule();
                    if (isSeguimientoRuta.equals(Configuracion.FLGVERDADERO)) {
                        UtilitarioPedidos.subCallNServices(this, ioDALNservices, true, loBeanUsuario.getFlgNServicesObligatorio().equals(Configuracion.FLGVERDADERO));
                    }
                }
            } else if (getEnumIdHttpResultado() == EnumServerResponse.OKNOMSG && iiProceso == EnumMenuPrincipalItem.EFICIENCIA && loBeanUsuario.getPerfil().equals(Configuracion.perfilSup)) {
                startActivity(new Intent(ActMenu.this, ActKpiDetalleSupervisor.class));
            } else if (getEnumIdHttpResultado() == EnumServerResponse.OKNOMSG && iiProceso == EnumMenuPrincipalItem.EFICIENCIA && loBeanUsuario.getPerfil().equals(Configuracion.perfilVen)) {
                startActivity(new Intent(ActMenu.this, ActKpiDetalle.class));
            }
        } catch (Exception e) {
            subShowException(e);
        }

    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */
    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        try {
            DialogFragmentYesNo loDialog;
            switch (id) {
                case Constantes.CONSSALIR:
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(),
                            DialogFragmentYesNo.TAG.concat("SALIR"),
                            getString(R.string.dlg_titinformacion),
                            getString(R.string.actmenu_dlgsalir),
                            R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNoPairListener(loDialog, new OnDialogFragmentYesNoClickListener() {
                        @Override
                        public void performButtonYes(DialogFragmentYesNo dialogFragmentYesNo) {
                            finish();
                            startActivity(new Intent(ActMenu.this, ActLogin.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                        }

                        @Override
                        public void performButtonNo(DialogFragmentYesNo dialogFragmentYesNo) {

                        }
                    });
                case Constantes.VALIDASINCRO:
                    loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                            DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                            getString(R.string.msgregpendientesenvinfo),
                            R.drawable.boton_ayuda, false,
                            EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNoPairListener(loDialog, null);
                case Constantes.VALIDASINCRONIZACION:
                    loDialog = DialogFragmentYesNo
                            .newInstance(
                                    getFragmentManager(),
                                    DialogFragmentYesNo.TAG
                                            .concat("VALIDASINCRONIZACION"),
                                    getString(R.string.actmenuvalsincro_titulo),
                                    getString(R.string.actmenuvalsincro_mensaje),
                                    R.drawable.boton_error, false,
                                    EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNoPairListener(loDialog, null);
                case Constantes.CONSDIASINCRONIZAR:
                    loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                            DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                            getString(R.string.actmenu_dlgsincronizar),
                            R.drawable.boton_ayuda,
                            EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNoPairListener(loDialog, new OnDialogFragmentYesNoClickListener() {
                        @Override
                        public void performButtonYes(DialogFragmentYesNo dialogFragmentYesNo) {
                            try {
                                iiProceso = EnumMenuPrincipalItem.SINCRONIZAR;
                                BeanUsuario loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(ActMenu.this);
                                ActMenu.this.setMsgOk(getString(R.string.actmenu_dlghttpok));
                                new HttpSincronizar(loBeanUsuario.getCodVendedor(), ActMenu.this, fnTipactividad()).execute();
                            } catch (Exception e) {
                                subShowException(e);
                            }
                        }

                        @Override
                        public void performButtonNo(DialogFragmentYesNo dialogFragmentYesNo) {

                        }
                    });
                case Constantes.ENVIOPENDIENTES:
                    loDialog = DialogFragmentYesNo
                            .newInstance(
                                    getFragmentManager(),
                                    DialogFragmentYesNo.TAG
                                            .concat("ENVIOPENDIENTES"),
                                    getString(R.string.msgdeseaenviarpendientes),
                                    R.drawable.boton_ayuda,
                                    EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNoPairListener(loDialog, new OnDialogFragmentYesNoClickListener() {
                        @Override
                        public void performButtonYes(DialogFragmentYesNo dialogFragmentYesNo) {
                            UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActMenu.this);
                        }

                        @Override
                        public void performButtonNo(DialogFragmentYesNo dialogFragmentYesNo) {

                        }
                    });
                case Constantes.NOHAYPENDIENTES:
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG.concat("NOHAYPENDIENTES"),
                            getString(R.string.titlenohatpendiente),
                            getString(R.string.toanohaypendientes),
                            R.drawable.boton_informacion, false,
                            EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNoPairListener(loDialog, null);

                case Constantes.DIALOG_NSERVICES_DOWNLOAD:
                    String lsNServices;
                    if (ioDALNservices == null)
                        ioDALNservices = DALNServices.getInstance(this,
                                R.string.db_name);
                    try {
                        lsNServices = ioDALNservices.fnSelNServicesLabel();
                    } catch (Exception e) {
                        Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                        lsNServices = getString(R.string.nservices_label_default);
                    }
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG + Constantes.DIALOG_NSERVICES_DOWNLOAD,
                            getString(R.string.nservices_consultingdownlad).replace("{0}", lsNServices), EnumLayoutResource.getDefaultIconHelp(this));
                    return new DialogFragmentYesNoPairListener(loDialog,
                            new OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(DialogFragmentYesNo dialog) {
                                    try {
                                        UtilitarioPedidos.subIniNServicesDownload(ActMenu.this, ioDALNservices.fnSelNServicesAPK());
                                    } catch (Exception e) {
                                        subShowException(e);
                                    }
                                }

                                @Override
                                public void performButtonNo(DialogFragmentYesNo dialog) {

                                }
                            });

                default:
                    return super.onCreateNexDialog(id);
            }
        } catch (Exception e) {
            subShowException(e);
            return super.onCreateNexDialog(id);
        }
    }


}