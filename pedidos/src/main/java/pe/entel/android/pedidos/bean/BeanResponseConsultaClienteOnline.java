package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

/**
 * Created by tkongr on 30/10/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanResponseConsultaClienteOnline {

    @JsonProperty
    private String resultado = "";
    @JsonProperty
    private int idResultado = 0;
    @JsonProperty
    private BeanCliente clienteOnline;
    @JsonProperty
    private List<BeanCobranzaOnline> listaCobranzasOnline;

    public BeanResponseConsultaClienteOnline() {
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public int getIdResultado() {
        return idResultado;
    }

    public void setIdResultado(int idResultado) {
        this.idResultado = idResultado;
    }

    public BeanCliente getBeanCliente() {
        return clienteOnline;
    }

    public void setBeanCliente(BeanCliente beanCliente) {
        this.clienteOnline = beanCliente;
    }

    public List<BeanCobranzaOnline> getListaCobranzasClienteOnline() {
        return listaCobranzasOnline;
    }

    public void setListaCobranzasClienteOnline(List<BeanCobranzaOnline> listaCobranzasClienteOnline) {
        this.listaCobranzasOnline = listaCobranzasClienteOnline;
    }
}
