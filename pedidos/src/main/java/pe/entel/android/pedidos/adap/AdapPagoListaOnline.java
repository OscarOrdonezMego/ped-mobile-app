package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanPagoOnline;
import pe.entel.android.pedidos.util.Configuracion;

/**
 * Created by tkongr on 15/10/2015.
 */
public class AdapPagoListaOnline extends ArrayAdapter<BeanPagoOnline> {

    int resource;
    Context context;
    TextView rowTextView;

    LinkedList<BeanPagoOnline> originalList;

    int cont = 0;
    String flgMoneda;
    String flgMonedaDolares;
    String flgNumDecVista;

    public AdapPagoListaOnline(Context _context, int _resource,
                               LinkedList<BeanPagoOnline> lista) {
        super(_context, _resource, lista);
        originalList = lista;
        resource = _resource;
        context = _context;

        flgMoneda = Configuracion.EnumConfiguracion.SIMBOLO_MONEDA.getValor(context);
        flgMonedaDolares = "$";
        flgNumDecVista = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(context);

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BeanPagoOnline item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        String nomCliente = "[" + item.getCli_nombre() + "]";

        ((TextView) nuevaVista
                .findViewById(R.id.actpagoonlineitem_txtnombreCliente))
                .setText(nomCliente);
        ((TextView) nuevaVista
                .findViewById(R.id.actpagoonlineitem_txtnumeroDocumento))
                .setText(item.getCob_numdocumento());
        ((TextView) nuevaVista
                .findViewById(R.id.actpagoonlineitem_txtHora))
                .setText(item.getFecha().substring(11));

        //if(Double.parseDouble(item.getMonto_saldo()) > 0) {
            ((TextView) nuevaVista.findViewById(R.id.actpagoonlineitem_txtmonto)).setText(flgMoneda
                            + " "
                            + Utilitario.fnRound(item.getMonto_pagado_fecha(), Integer
                            .valueOf(flgNumDecVista)));

            ((TextView) nuevaVista
                    .findViewById(R.id.actpagoonlineitem_txtsaldoSoles))
                    .setText(flgMoneda
                            + " "
                            + Utilitario.fnRound(item.getMonto_saldo(), Integer
                            .valueOf(flgNumDecVista)));
            ((TextView) nuevaVista
                    .findViewById(R.id.actpagoonlineitem_txtHora))
                    .setText(item.getFecha().substring(11));
        //} else {
        //    (nuevaVista.findViewById(R.id.actpagoonlineitem_lySoles)).setVisibility(View.GONE);
        //}

        //if(Double.parseDouble(item.getMonto_saldodolares()) > 0) {
            ((TextView) nuevaVista
                    .findViewById(R.id.actpagoonlineitem_txtmontodolares))
                    .setText(flgMonedaDolares
                            + " "
                            + Utilitario.fnRound(item.getMonto_pagado_fechadolares(), Integer
                            .valueOf(flgNumDecVista)));

            ((TextView) nuevaVista
                    .findViewById(R.id.actpagoonlineitem_txtsaldoDolares))
                    .setText(flgMonedaDolares
                            + " "
                            + Utilitario.fnRound(item.getMonto_saldodolares(), Integer
                            .valueOf(flgNumDecVista)));
        //} else {
        //    (nuevaVista.findViewById(R.id.actpagoonlineitem_lyDolares)).setVisibility(View.GONE);
        //}

        return nuevaVista;
    }
}
