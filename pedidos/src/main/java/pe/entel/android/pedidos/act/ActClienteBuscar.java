package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import greendroid.widget.ActionBarItem;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapClienteBuscar;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.http.HttpClienteOnline;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.UtilitarioData;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;
import pe.entel.android.pedidos.widget.HeadedTextItem;

public class ActClienteBuscar extends NexActivity implements
        RadioGroup.OnCheckedChangeListener, OnItemClickListener, TextWatcher,
        NexMenuCallbacks, MenuSlidingListener {

    private EditText txtTexto;
    private RadioGroup rbtGruFiltro;
    private RadioButton rbtCodigo, rbtNombre, rbtOnline;
    private ListView _lstClientes;
    private Button btnGrupEconomico;
    private int CONSPROSPECTO = 1;
    private String isCliente;
    AdapClienteBuscar adapter;
    boolean estadoSincronizar = false;
    private AdapMenuPrincipalSliding _menu;

    ArrayList<HeadedTextItem> listCliente = new ArrayList<HeadedTextItem>();
    BeanUsuario loBeanUsuario;

    // Integracion Ntrack
    private DALNServices ioDALNservices;

    private String flgTipoBusqueda;
    private String isBusquedaNumericaCliente;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);
        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);
        isCliente = Configuracion.EnumFuncion.CLIENTE.getValor(this);
        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);
        UtilitarioPedidos.IndicarMensajeOkError(this);
        UtilitarioData.fnExportDatabaseFileTask(this, getString(R.string.db_name));

        View.OnClickListener listener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(ActClienteBuscar.this, ActLogin.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        };
        UtilitarioPedidos.obtenerMenuLateral(this, loBeanUsuario, listener);
    }

    @Override
    protected void subIniActionBar() {
        this.getActionBarGD().setTitle(getString(R.string.actclientebuscar_bartitulo));
        if (isCliente.equals(Configuracion.FLGVERDADERO)) {
            addActionBarItem(ActionBarItem.Type.Add, CONSPROSPECTO);
        }
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        if (item.getContentDescription().toString().equals(getString(R.string.gd_add))) {
            startActivity(new Intent(ActClienteBuscar.this, ActProspecto.class));
        } else {
            return super.onHandleActionBarItemClick(item, position);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, ActMenu.class));
        finish();
    }

    @Override
    protected void subSetControles() {
        setActionBarContentView(R.layout.clientebuscar2);

        _lstClientes = (ListView) findViewById(R.id.actclientebuscar_lista);
        txtTexto = (EditText) findViewById(R.id.actclientebuscar_txtTexto);
        rbtGruFiltro = (RadioGroup) findViewById(R.id.actclientebuscar_rbtGruFiltro);
        rbtCodigo = (RadioButton) findViewById(R.id.actclientebuscar_rbtCodigo);
        rbtNombre = (RadioButton) findViewById(R.id.actclientebuscar_rbtNombre);
        rbtOnline = (RadioButton) findViewById(R.id.actclientebuscar_rbtOnline);
        btnGrupEconomico = findViewById(R.id.actclientebuscar_btnGrupoEconomico);
        rbtGruFiltro.setOnCheckedChangeListener(this);
        txtTexto.addTextChangedListener(this);
        _lstClientes.setOnItemClickListener(this);

        txtTexto.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (rbtOnline.isChecked()) {
                        subBuscarOnline();
                        return true;
                    }
                }
                return false;
            }
        });

        btnGrupEconomico.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent loIntent = new Intent(ActClienteBuscar.this, ActListaGrupoEconomico.class);
                Log.d("XXX", "ActClienteBuscar -> ActListaGrupoEconomico");
                finish();
                startActivity(loIntent);
            }
        });

        rbtNombre.setChecked(true);
        llenarLista();
    }

    public void llenarLista() {

        if (rbtOnline.isChecked())
            listCliente.clear();
        else
            listCliente = new DalCliente(this).fnBuscarListCliente("", (rbtCodigo.isChecked()) ? EnumTipBusqueda.CODIGO : EnumTipBusqueda.NOMBRE, loBeanUsuario);

        adapter = new AdapClienteBuscar(this, R.layout.clientebuscar_item,
                listCliente);

        _lstClientes.setAdapter(adapter);

        isBusquedaNumericaCliente = Configuracion.EnumConfiguracion.BUSQUEDA_NUMERICA_CLIENTES.getValor(this);
        subSetPreferenceBusqueda();
    }

    private void subSetPreferenceBusqueda() {

        String preferenceBusqueda = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("TipoBusquedaCliente", "");

        if (!preferenceBusqueda.equals(Configuracion.CONSTANTE_BUSQUEDA_CODIGO)) {

            if (preferenceBusqueda.equals(Configuracion.CONSTANTE_BUSQUEDA_ONLINE)) {
                rbtOnline.setChecked(true);
            } else {
                //Búsqueda por nombre
                rbtNombre.setChecked(true);
                txtTexto.setInputType(InputType.TYPE_CLASS_TEXT);
            }

        } else {
            //Búsqueda por codigo
            rbtCodigo.setChecked(true);

            if (isBusquedaNumericaCliente.equals(Configuracion.FLGVERDADERO)) {
                txtTexto.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_PHONE);
            } else {
                txtTexto.setInputType(InputType.TYPE_CLASS_TEXT);
            }
        }
    }

    public void onCheckedChanged(RadioGroup group, int checkedId) {

        flgTipoBusqueda = "";
        if (rbtCodigo.isChecked()) {
            flgTipoBusqueda = Configuracion.CONSTANTE_BUSQUEDA_CODIGO;

            if (isBusquedaNumericaCliente.equals(Configuracion.FLGVERDADERO)) {
                txtTexto.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_PHONE);
            } else {
                txtTexto.setInputType(InputType.TYPE_CLASS_TEXT);
            }

        } else if (rbtNombre.isChecked()) {
            flgTipoBusqueda = Configuracion.CONSTANTE_BUSQUEDA_NOMBRE;
            txtTexto.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_PHONE);

        } else if (rbtOnline.isChecked()) {
            flgTipoBusqueda = Configuracion.CONSTANTE_BUSQUEDA_ONLINE;

            if (isBusquedaNumericaCliente.equals(Configuracion.FLGVERDADERO)) {
                txtTexto.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_CLASS_PHONE);
            } else {
                txtTexto.setInputType(InputType.TYPE_CLASS_TEXT);
            }
        }

        SharedPreferences loSharedPreferences = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString("TipoBusquedaCliente", flgTipoBusqueda);
        loEditor.commit();

        llenarLista();
        txtTexto.setText("");
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        switch (id) {
            case Constantes.NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario.getCodVendedor(), ActClienteBuscar.this, fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            estadoSincronizar = false;
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;

            case Constantes.ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActClienteBuscar.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.CONSICOVALIDAR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.actclientebuscar_dlgingdatos))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        txtTexto.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.CONSOTRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.actclientebuscar_dlgnodatos))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        txtTexto.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;

            default:
                return super.onCreateDialog(id);
        }
    }

    private void subConexionHttp() {
        BeanCliente loBeanCliente = new BeanCliente();
        loBeanCliente.setClicod(txtTexto.getText().toString());
        loBeanCliente.setCodUsuario(((AplicacionEntel) getApplication()).getCurrentUsuario().getCodVendedor());
        new HttpClienteOnline(loBeanCliente, this, this.fnTipactividad()).execute();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {

        if (estadoSincronizar) {
            startActivity(new Intent(this, ActMenu.class).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP));
        } else if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
            startActivity(new Intent(this, ActClienteDetalle.class));
        } else {
            UtilitarioPedidos.mostrarDialogo(this, Constantes.CONSOTRO);
        }
    }

    @SuppressWarnings("deprecation")
    private void subBuscarOnline() {

        ((AplicacionEntel) getApplication()).setCurrentCliente(null);

        if (txtTexto.getText().toString().length() < 2) {
            UtilitarioPedidos.mostrarDialogo(this, Constantes.CONSICOVALIDAR);
            return;
        }

        ((AplicacionEntel) getApplication()).setClienteOnline(1);
        subConexionHttp();
    }

    @Override
    public void afterTextChanged(Editable arg0) {
        adapter.getFilter(rbtCodigo.isChecked() ? 0 : 1).filter(txtTexto.getText().toString());
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

        Intent loIntent = new Intent(ActClienteBuscar.this, ActClienteDetalle.class);

        ((AplicacionEntel) getApplication()).setClienteOnline(0);
        ((AplicacionEntel) getApplication()).setCurrentCliente(null);

        HeadedTextItem loHeadedTextItem = (HeadedTextItem) _lstClientes.getItemAtPosition(arg2);
        BeanCliente loBeanCliente = new DalCliente(this).fnSelxIdCliente(Long.valueOf(loHeadedTextItem.getTag().toString()));

        String lsBeanCliente = "";
        try {
            lsBeanCliente = BeanMapper.toJson(loBeanCliente, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        UtilitarioData.fnCrearPreferencesPutString(this, "BeanClienteOnline", lsBeanCliente);
        loIntent.putExtra("IdCliente", Long.parseLong(loHeadedTextItem.getTag().toString()));
        startActivity(loIntent);
    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this, EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        Intent intentl = new Intent(this, ActMenu.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intentl);
    }

    @Override
    public void subInicioFromSliding() {

    }

    @SuppressWarnings("deprecation")
    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = new DalPedido(this).fnEnvioPedidosPendientes(loBeanUsuario.getCodVendedor());
            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);
                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();
                UtilitarioPedidos.mostrarDialogo(this, Constantes.CONSDIASINCRONIZAR);
            } else {
                UtilitarioPedidos.mostrarDialogo(this, Constantes.VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);
        this.setMsgOk("Eficiencia Online");

        BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
        loBeanListaEficienciaOnline.setIdResultado(0);
        loBeanListaEficienciaOnline.setResultado("");
        loBeanListaEficienciaOnline.setListaEficiencia(null);
        loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

        new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

        Intent loInstent = new Intent(this, ActKpiDetalle.class);
        loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loInstent);
    }

    @Override
    public void subStockFromSliding() {
        ((AplicacionEntel) getApplication()).setCurrentCliente(null);
        ((AplicacionEntel) getApplication()).setCurrentCodFlujo(Configuracion.CONSSTOCK);
        ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
        Intent loIntento = new Intent(ActClienteBuscar.this, ActProductoListaOnline.class);
        loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        BeanExtras loBeanExtras = new BeanExtras();
        loBeanExtras.setFiltro("");
        loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
        String lsFiltro = "";
        try {
            lsFiltro = BeanMapper.toJson(loBeanExtras, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
        startActivity(loIntento);
    }

    @Override
    public void subNServicesFromSliding() {
        try {
            UtilitarioPedidos.subCallNServices(this, ioDALNservices, false, loBeanUsuario.getFlgNServicesObligatorio().equals(Configuracion.FLGVERDADERO));
        } catch (Exception e) {
            subShowException(new Exception(getString(R.string.ml_sincronizarantes)));
        }
    }


    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */
    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        try {
            DialogFragmentYesNo loDialog;
            switch (id) {
                case Constantes.NOHAYPENDIENTES:
                    loDialog = DialogFragmentYesNo
                            .newInstance(getFragmentManager(),
                                    DialogFragmentYesNo.TAG.concat("NOHAYPENDIENTES"),
                                    getString(R.string.toanohaypendientes),
                                    R.drawable.boton_informacion, false,
                                    EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNoPairListener(loDialog, null);
                case Constantes.ENVIOPENDIENTES:
                    loDialog = DialogFragmentYesNo
                            .newInstance(
                                    getFragmentManager(),
                                    DialogFragmentYesNo.TAG
                                            .concat("ENVIOPENDIENTES"),
                                    getString(R.string.msgdeseaenviarpendientes),
                                    R.drawable.boton_ayuda,
                                    EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNoPairListener(loDialog, new OnDialogFragmentYesNoClickListener() {
                        @Override
                        public void performButtonYes(DialogFragmentYesNo dialogFragmentYesNo) {
                            UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActClienteBuscar.this);
                        }

                        @Override
                        public void performButtonNo(DialogFragmentYesNo dialogFragmentYesNo) {

                        }
                    });
                case Constantes.VALIDASINCRO:
                    loDialog = DialogFragmentYesNo
                            .newInstance(
                                    getFragmentManager(),
                                    DialogFragmentYesNo.TAG
                                            .concat("VALIDASINCRO"),
                                    getString(R.string.msgregpendientesenvinfo),
                                    R.drawable.boton_informacion, false,
                                    EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNoPairListener(loDialog, null);
                case Constantes.CONSDIASINCRONIZAR:
                    loDialog = DialogFragmentYesNo
                            .newInstance(getFragmentManager(),
                                    DialogFragmentYesNo.TAG
                                            .concat("CONSDIASINCRONIZAR"),
                                    getString(R.string.dlg_titinformacion),
                                    getString(R.string.actmenu_dlgsincronizar),
                                    R.drawable.boton_ayuda,
                                    EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNoPairListener(loDialog, new OnDialogFragmentYesNoClickListener() {
                        @Override
                        public void performButtonYes(DialogFragmentYesNo dialogFragmentYesNo) {
                            try {
                                estadoSincronizar = true;
                                new HttpSincronizar(loBeanUsuario.getCodVendedor(), ActClienteBuscar.this, fnTipactividad()).execute();
                            } catch (Exception e) {
                                estadoSincronizar = false;
                                subShowException(e);
                            }
                        }

                        @Override
                        public void performButtonNo(DialogFragmentYesNo dialogFragmentYesNo) {

                        }
                    });
                case Constantes.CONSICOVALIDAR:
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(),
                            DialogFragmentYesNo.TAG.concat("CONSICOVALIDAR"),
                            getString(R.string.actclientebuscar_dlgingdatos),
                            R.drawable.boton_informacion, false,
                            EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNoPairListener(loDialog, new OnDialogFragmentYesNoClickListener() {
                        @Override
                        public void performButtonYes(DialogFragmentYesNo dialogFragmentYesNo) {
                            txtTexto.requestFocus();
                        }

                        @Override
                        public void performButtonNo(DialogFragmentYesNo dialogFragmentYesNo) {

                        }
                    });
                case Constantes.CONSOTRO:
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(),
                            DialogFragmentYesNo.TAG.concat("CONSOTRO"),
                            getString(R.string.actclientebuscar_dlgnodatos),
                            R.drawable.boton_informacion, false,
                            EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNoPairListener(loDialog, new OnDialogFragmentYesNoClickListener() {
                        @Override
                        public void performButtonYes(DialogFragmentYesNo dialogFragmentYesNo) {
                            txtTexto.requestFocus();
                        }

                        @Override
                        public void performButtonNo(DialogFragmentYesNo dialogFragmentYesNo) {

                        }
                    });
                case Constantes.DIALOG_NSERVICES_DOWNLOAD:
                    String lsNServices;
                    if (ioDALNservices == null)
                        ioDALNservices = DALNServices.getInstance(this, R.string.db_name);
                    try {
                        lsNServices = ioDALNservices.fnSelNServicesLabel();
                    } catch (Exception e) {
                        Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                        lsNServices = getString(R.string.nservices_label_default);
                    }
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG
                                    + Constantes.DIALOG_NSERVICES_DOWNLOAD,
                            getString(R.string.nservices_consultingdownlad)
                                    .replace("{0}", lsNServices),
                            EnumLayoutResource.getDefaultIconHelp(this));
                    return new DialogFragmentYesNoPairListener(loDialog,
                            new OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(
                                        DialogFragmentYesNo dialog) {
                                    try {
                                        UtilitarioPedidos.subIniNServicesDownload(ActClienteBuscar.this, ioDALNservices.fnSelNServicesAPK());
                                    } catch (Exception e) {
                                        subShowException(e);
                                    }
                                }

                                @Override
                                public void performButtonNo(
                                        DialogFragmentYesNo dialog) {
                                }
                            });

                default:
                    return super.onCreateNexDialog(id);
            }
        } catch (Exception e) {
            subShowException(e);
            return super.onCreateNexDialog(id);
        }
    }

}