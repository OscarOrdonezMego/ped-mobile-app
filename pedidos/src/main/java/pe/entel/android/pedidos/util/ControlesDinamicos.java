package pe.entel.android.pedidos.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pe.com.nextel.android.item.BeanSpinner;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanControl;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCtrl;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.widget.MiCheckBox;
import pe.entel.android.pedidos.widget.MiEditText;
import pe.entel.android.pedidos.widget.MiSpinner;
import pe.entel.android.pedidos.widget.MiTextView;

/**
 * Created by rtamayov on 26/05/2015.
 */
public class ControlesDinamicos {

    private static ArrayList<BeanSpinner> loListaSpinner;
    private static MiEditText txtAlphaControl;
    private static MiEditText txtDecimalControl;
    private static MiEditText txtNumericControl;
    private static MiCheckBox chkControl;
    private static MiSpinner spiControl;
    private static DatePicker dateControl;
    private static TimePicker timeControl;
    private static RadioButton radioControl;

    public static void ingresarControles(Context ioContext, BeanEnvPedidoCab pedidoCab, BeanControl loControl, LinearLayout layout) throws Exception {

        String valor = fnBuscarValor(ioContext, pedidoCab, loControl);
        MiTextView label = new MiTextView(ioContext);
        label.setText(loControl.getEtiquetaTexto());
        layout.addView(label);
        if (loControl.getTipo() == Configuracion.TYPE_ALPHANUMERIC) {
            txtAlphaControl = new MiEditText(ioContext);
            int maxLength = loControl.getMaxCaracteres();
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            txtAlphaControl.setFilters(FilterArray);
            if (!valor.equals(""))
                txtAlphaControl.setText(valor);
            layout.addView(txtAlphaControl);
        } else if (loControl.getTipo() == Configuracion.TYPE_DECIMAL) {
            txtDecimalControl = new MiEditText(ioContext);
            txtDecimalControl.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
            int maxLength = loControl.getMaxCaracteres();
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            txtDecimalControl.setFilters(FilterArray);
            if (!valor.equals(""))
                txtDecimalControl.setText(valor);
            layout.addView(txtDecimalControl);
        } else if (loControl.getTipo() == Configuracion.TYPE_NUMERIC) {
            txtNumericControl = new MiEditText(ioContext);
            txtNumericControl.setInputType(InputType.TYPE_CLASS_NUMBER);
            int maxLength = loControl.getMaxCaracteres();
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(maxLength);
            txtNumericControl.setFilters(FilterArray);
            if (!valor.equals(""))
                txtNumericControl.setText(valor);
            layout.addView(txtNumericControl);
        } else if (loControl.getTipo() == Configuracion.TYPE_CHECKBOX) {
            chkControl = new MiCheckBox(ioContext);
            chkControl.setTag("");
            if (!valor.equals("")) {
                boolean flagCheck = valor.equals("T");
                chkControl.setChecked(flagCheck);
            }
            layout.addView(chkControl);
        } else if (loControl.getTipo() == Configuracion.TYPE_COMBOBOX) {
            spiControl = new MiSpinner(ioContext);
            subLlenarSpinner(ioContext, loControl.getGrupo());
            if (!valor.equals("")) {
                for (int i = 0; i < loListaSpinner.size(); i++) {
                    BeanSpinner loSpinner = loListaSpinner.get(i);
                    if (loSpinner.getKey().equals(valor)) {
                        spiControl.setSelection(i);
                        break;
                    }
                }
            }
            layout.addView(spiControl);
        } else if (loControl.getTipo() == Configuracion.TYPE_DATE) {
            dateControl = new DatePicker(ioContext);
            if (!valor.equals("")) {

                int[] fechaArray = fnFechaParticionadaddMMyyyy(valor);
                if (fechaArray != null) {
                    dateControl.updateDate(fechaArray[0], fechaArray[1], fechaArray[2]);
                }

            }
            dateControl.setEnabled(loControl.getIsEditable().equals("T"));
            layout.addView(dateControl);
        } else if (loControl.getTipo() == Configuracion.TYPE_TIME) {
            timeControl = new TimePicker(ioContext);
            String[] parseHora = fnHoraParticionadaHHmm(valor);
            if (!valor.equals("")) {
                timeControl.setCurrentHour(Integer.parseInt(parseHora[0]));
                timeControl.setCurrentMinute(Integer.parseInt(parseHora[1]));
            }
            timeControl.setEnabled(loControl.getIsEditable().equals("T"));
            layout.addView(timeControl);
        } else if (loControl.getTipo() == Configuracion.TYPE_RADIOBUTTON) {
            subLlenarGrupo(ioContext, loControl.getGrupo());
            RadioGroup radioGroup = new RadioGroup(ioContext);
            for (int i = 0 + 1; i < loListaSpinner.size(); i++) {
                radioControl = new RadioButton(ioContext);
                radioControl.setId(i);
                radioControl.setText(loListaSpinner.get(i).getDescription());
                radioControl.setTextColor(ioContext.getResources().getColor(R.color.pedidos_txt));
                if (!valor.equals("") && valor.equals(loListaSpinner.get(i).getDescription())) {
                    radioControl.setChecked(true);
                } else {
                    radioControl.setChecked(false);
                }
                radioGroup.addView(radioControl);
            }


            layout.addView(radioGroup);

        }
    }

    private static String fnBuscarValor(Context ioContext, BeanEnvPedidoCab beanEnvPedidoCab, BeanControl beanControl) {
        String valor = "";
        try {
            List<BeanEnvPedidoCtrl> listaPendiente = beanEnvPedidoCab.getListaCtrlPedido();
            for (BeanEnvPedidoCtrl bean : listaPendiente) {
                if (bean.getIdControl().equals(String.valueOf(beanControl.getCodigo()))) {
                    valor = bean.getValorControl();
                    return valor;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valor;
    }

    private static void subLlenarSpinner(Context ioContext, int grupo) throws Exception {
        // Creamos el adaptador
        subLlenarGrupo(ioContext, grupo);
        ArrayAdapter<BeanSpinner> loAdaSpinner = new ArrayAdapter<BeanSpinner>(ioContext, R.layout.simple_spinner_item, loListaSpinner);
        loAdaSpinner.setDropDownViewResource(R.layout.itemspinnermultiline2);
        spiControl.setAdapter(loAdaSpinner);
        spiControl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

    }

    private static List<BeanSpinner> subLlenarGrupo(Context ioContext, int grupo) throws Exception {
        DalPedido dalPedido = new DalPedido(ioContext);
        loListaSpinner = (ArrayList<BeanSpinner>) dalPedido.fnSelOpcionesSpinerVariable(grupo);
        return loListaSpinner;
    }


    public static String subGuardarControles(Context ioContext, LinearLayout layout, List<BeanControl> ioListaControles) throws Exception {
        String mensaje = "";

        int idx = 0;
        for (int i = 0; i < layout.getChildCount(); i++) {
            View objControl = layout.getChildAt(i);
            if (!(objControl instanceof MiTextView)) {
                ControlesDinamicos.subGuardaValoresVariablesDialog(ioContext, objControl, ioListaControles.get(idx));
                idx = idx + 1;
            }

        }

        for (int i = 0; i < ioListaControles.size(); i++) {
            BeanControl beanControl = ioListaControles.get(i);
            if (beanControl.getValor().equals("") && beanControl.getIsObligatorio().equals("T")) {
                mensaje = "Falta Completar valores obligatorios, Variable: " + beanControl.getEtiquetaTexto();
                return mensaje;
            }
        }

        return mensaje;
    }


    private static void subGuardaValoresVariablesDialog(Context ioContext, View view, BeanControl poControl) throws Exception {
        poControl.setValor("");
        if (view instanceof EditText) {
            EditText editText = (EditText) view;
            poControl.setValor(editText.getText().toString());
        } else if (view instanceof CheckBox) {
            CheckBox checkBox = (CheckBox) view;
            if (checkBox.isChecked()) {
                poControl.setValor("T");
            } else {
                poControl.setValor("F");
            }
        } else if (view instanceof Spinner) {
            Spinner spinner = (Spinner) view;
            BeanSpinner loSpinner = (BeanSpinner) spinner.getSelectedItem();
            if (loSpinner.getId() != -1) {
                poControl.setValor(loSpinner.getKey());
            }
        } else if (view instanceof DatePicker) {
            DatePicker date = (DatePicker) view;
            String fechaFormateada = fnFecha(false, date.getYear(), date.getMonth(), date.getDayOfMonth());
            poControl.setValor(fechaFormateada);
        } else if (view instanceof TimePicker) {
            TimePicker time = (TimePicker) view;
            String fechaFormateada = fnHora(time.getCurrentHour(), time.getCurrentMinute());
            poControl.setValor(fechaFormateada);
        } else if (view instanceof RadioGroup) {
            RadioGroup group = (RadioGroup) view;

            for (int i = 0; i < group.getChildCount(); i++) {
                View objRadio = group.getChildAt(i);
                if (objRadio instanceof RadioButton) {
                    RadioButton boton = (RadioButton) objRadio;
                    if (boton.isChecked()) {
                        poControl.setValor(boton.getText().toString());
                    }
                }
            }
        }
    }

    private static String fnFecha(boolean lbAutomatico, int piYear, int piMes, int piDia) {
        String lsRetorno;
        Calendar calendar = Calendar.getInstance();
        Date loDate;
        if (!lbAutomatico) {
            calendar.set(Calendar.YEAR, piYear);
            calendar.set(Calendar.MONTH, piMes);
            calendar.set(Calendar.DAY_OF_MONTH, piDia);
        }
        loDate = calendar.getTime();
        lsRetorno = DateFormat.format("dd/MM/yyyy", loDate).toString();
        return lsRetorno;
    }

    private static String fnHora(int hour, int minutes) {
        String lsRetorno;

        lsRetorno = Integer.toString(hour) + ":" + Integer.toString(minutes);

        return lsRetorno;
    }

    private static void fnEncontrarControl(List<BeanControl> ioListaControles, BeanControl beanControl) {
        boolean encontrado = false;
        for (int i = 0; i < ioListaControles.size(); i++) {
            BeanControl currentControl = ioListaControles.get(i);
            if (currentControl.getCodigo() == beanControl.getCodigo()) {
                ioListaControles.set(i, beanControl);
                encontrado = true;

            }
        }

        if (!encontrado) {
            ioListaControles.add(beanControl);
        }


    }

    private static int[] fnFechaParticionadaddMMyyyy(String psFecha) {
        SimpleDateFormat loSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date loDate = null;
        int[] fechaPart = new int[3];
        if (!psFecha.trim().equals("")) {
            try {
                loDate = loSimpleDateFormat.parse(psFecha);
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
            Calendar loCalendar = Calendar.getInstance();
            loCalendar.setTime(loDate);
            fechaPart[0] = loCalendar.get(Calendar.YEAR);
            fechaPart[1] = loCalendar.get(Calendar.MONTH);
            fechaPart[2] = loCalendar.get(Calendar.DAY_OF_MONTH);
            return fechaPart;
        }
        return null;
    }

    private static String[] fnHoraParticionadaHHmm(String pshora) {
        String[] horaPart = new String[2];

        horaPart = pshora.split(":");


        return horaPart;
    }


    public static class Tabla {

        private TableLayout tabla;
        private ArrayList<TableRow> filas;
        private Activity actividad;
        private Resources rs;
        private int FILAS, COLUMNAS;

        public Tabla(Activity actividad, TableLayout tabla) {
            this.actividad = actividad;
            this.tabla = tabla;
            rs = this.actividad.getResources();
            FILAS = COLUMNAS = 0;
            filas = new ArrayList<TableRow>();
        }

        public void cargarDatos(List<String> cabecera, List<List<String>> datos) {
            agregarCabecera(cabecera);

            for (int k = 0; k < datos.size(); k++) {
                List<String> elementos = datos.get(k);
                agregarFilaTabla(elementos);
            }
        }

        public void agregarCabecera(List<String> arraycabecera) {
            TableRow.LayoutParams layoutCelda;
            TableRow fila = new TableRow(actividad);
            TableRow.LayoutParams layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
            fila.setLayoutParams(layoutFila);

            COLUMNAS = arraycabecera.size();

            for (int i = 0; i < arraycabecera.size(); i++) {
                TextView texto = new TextView(actividad);
                layoutCelda = new TableRow.LayoutParams(obtenerAnchoPixelesTexto(arraycabecera.get(i)), TableRow.LayoutParams.WRAP_CONTENT);
                texto.setText(arraycabecera.get(i));
                texto.setGravity(Gravity.CENTER_HORIZONTAL);
                texto.setTextAppearance(actividad, R.style.CustomDialogTitle);
                texto.setBackgroundResource(R.drawable.action_bar_background_blue);
                texto.setLayoutParams(layoutCelda);

                fila.addView(texto);
            }
            tabla.addView(fila);
            filas.add(fila);
            FILAS++;
        }

        public void agregarFilaTabla(List<String> elementos) {
            TableRow.LayoutParams layoutCelda;
            TableRow.LayoutParams layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
            TableRow fila = new TableRow(actividad);
            fila.setLayoutParams(layoutFila);
            for (int i = 0; i < elementos.size(); i++) {
                TextView texto = new TextView(actividad);
                texto.setText(String.valueOf(elementos.get(i)));
                texto.setGravity(Gravity.CENTER_HORIZONTAL);
                texto.setTextSize(18);
                texto.setTextColor(actividad.getResources().getColor(R.color.pedidos_txt));
                texto.setBackgroundResource(R.drawable.pedidos_container_producto_bg);
                layoutCelda = new TableRow.LayoutParams(obtenerAnchoPixelesTexto(texto.getText().toString()), TableRow.LayoutParams.WRAP_CONTENT);
                texto.setLayoutParams(layoutCelda);

                fila.addView(texto);
            }
            tabla.addView(fila);
            filas.add(fila);
            FILAS++;
        }

        private int obtenerAnchoPixelesTexto(String texto) {
            Paint p = new Paint();
            Rect bounds = new Rect();
            p.setTextSize(50);
            p.getTextBounds(texto, 0, texto.length(), bounds);
            return bounds.width();
        }
    }


    public static class TextWatcherDecimal implements TextWatcher {

        private EditText editText;
        private int CANTIDAD_ENTEROS;
        private int CANTIDAD_DECIMALES;

        public TextWatcherDecimal(EditText EditText, int cantidadEnteros, int cantidadDecimales) {
            editText = EditText;
            CANTIDAD_ENTEROS = cantidadEnteros;
            CANTIDAD_DECIMALES = cantidadDecimales;

        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            String str = editText.getText().toString();
            if (str.length() == 0) return;
            String str2 = PerfectDecimal(str, CANTIDAD_ENTEROS, CANTIDAD_DECIMALES);

            if (!str2.equals(str)) {
                editText.setText(str2);
                int pos = editText.getText().length();
                editText.setSelection(pos);
            }
        }

        public String PerfectDecimal(String str, int MAX_BEFORE_POINT, int MAX_DECIMAL) {
            if (str.charAt(0) == '.') str = "0" + str;
            int max = str.length();

            String rFinal = "";
            boolean after = false;
            int i = 0, up = 0, decimal = 0;
            char t;
            while (i < max) {
                t = str.charAt(i);
                if (t != '.' && !after) {
                    up++;
                    if (up > MAX_BEFORE_POINT) return rFinal;
                } else if (t == '.') {
                    after = true;
                } else {
                    decimal++;
                    if (decimal > MAX_DECIMAL)
                        return rFinal;
                }
                rFinal = rFinal + t;
                i++;
            }
            return rFinal;
        }
    }
}
