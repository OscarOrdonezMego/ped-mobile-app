package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import pe.com.nextel.android.dal.DALGestor;
import pe.com.nextel.android.item.BeanSpinner;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanBonificacion;
import pe.entel.android.pedidos.bean.BeanControl;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCtrl;
import pe.entel.android.pedidos.bean.BeanEnvPedidoDet;
import pe.entel.android.pedidos.bean.BeanProducto;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.util.Configuracion;

public class DalPedido extends DALGestor {
    private Context ioContext;

    public DalPedido(Context psClase) throws Exception {
        super(psClase, psClase.getString(R.string.db_name));
        ioContext = psClase;
    }

    public String fnGrabarPedido(BeanEnvPedidoCab psBean) {
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String strId = String.valueOf(System.currentTimeMillis());

        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("INSERT INTO TrCabPedido ("
                + "CodigoPedido,CodigoPedidoServidor,Empresa,CodigoUsuario,FechaInicio," +
                "FechaFin,CondicionVenta,CodigoCliente,CodigoMotivo,MontoTotal,CodDireccion," +
                "CodDireccionDespacho, Latitud,Longitud,Celda,ErrorConexion,ErrorPosicion," +
                "Observacion,CantidadTotal,FlgEnCobertura,FlgTerminado,FlgEnviado," +
                "BonificacionTotal, FlgNuevoDireccion,CodAlmacen, FleteTotal, TipoArticulo,FlgTipo, EnEdicion," +
                "MontoTotalSoles, MontoTotalDolar, TipoCambioSoles, TipoCambioDolar) VALUES ('");

        loStbCab.append(strId);
        loStbCab.append("','");
        loStbCab.append(psBean.getCodigoPedidoServidor());
        loStbCab.append("','");
        loStbCab.append(psBean.getEmpresa());
        loStbCab.append("','");
        loStbCab.append(psBean.getCodigoUsuario());
        loStbCab.append("','");
        loStbCab.append(psBean.getFechaInicio());
        loStbCab.append("','");
        loStbCab.append(psBean.getFechaFin());
        loStbCab.append("','");
        loStbCab.append(psBean.getCondicionVenta());
        loStbCab.append("','");
        loStbCab.append(psBean.getCodigoCliente());
        loStbCab.append("','");
        loStbCab.append(psBean.getCodigoMotivo());
        loStbCab.append("','");
        loStbCab.append(psBean.getMontoTotal());
        loStbCab.append("','");
        loStbCab.append(psBean.getCodDireccion());
        loStbCab.append("','");
        loStbCab.append(psBean.getCodDireccionDespacho());
        loStbCab.append("','");
        loStbCab.append(psBean.getLatitud());
        loStbCab.append("','");
        loStbCab.append(psBean.getLongitud());
        loStbCab.append("','");
        loStbCab.append(psBean.getCelda());
        loStbCab.append("','");
        loStbCab.append(psBean.getErrorConexion());
        loStbCab.append("','");
        loStbCab.append(psBean.getErrorPosicion());
        loStbCab.append("','");
        loStbCab.append(psBean.getObservacion());
        loStbCab.append("','");
        loStbCab.append(psBean.getCantidadTotal());
        loStbCab.append("','");
        loStbCab.append(psBean.getFlgEnCobertura());
        loStbCab.append("','");
        loStbCab.append(psBean.getFlgTerminado());
        loStbCab.append("','").append(Configuracion.FLGREGNOHABILITADO);
        loStbCab.append("','");
        loStbCab.append(psBean.getBonificacionTotal());
        loStbCab.append("','");
        loStbCab.append(psBean.getFlgNuevoDireccion());
        loStbCab.append("','");
        loStbCab.append(psBean.getCodAlmacen());
        loStbCab.append("','");
        loStbCab.append(psBean.getFleteTotal());
        loStbCab.append("','");
        loStbCab.append(psBean.getTipoArticulo());
        loStbCab.append("','");
        loStbCab.append(psBean.getFlgTipo());
        loStbCab.append("','");
        loStbCab.append(psBean.isEnEdicion() ? Configuracion.FLGVERDADERO : Configuracion.FLGFALSO);
        loStbCab.append("','");
        loStbCab.append(psBean.getMontoTotalSoles());
        loStbCab.append("','");
        loStbCab.append(psBean.getMontoTotalDolar());
        loStbCab.append("','");
        loStbCab.append(psBean.getTipoCambioSoles());
        loStbCab.append("','");
        loStbCab.append(psBean.getTipoCambioDolar());
        loStbCab.append("')");

        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            return "";
        }

        Log.v("DALPEDIDO", "ACTGRABAPEDIDO " + loStbCab.toString());

        StringBuilder loStbDet;

        Iterator<BeanEnvPedidoDet> loIterator = psBean.getListaDetPedido()
                .iterator();
        Log.v("XXX", "NUMERO DETALLES:: " + psBean.getListaDetPedido().size());

        BeanEnvPedidoDet loBeanPedidoDet = null;

        while (loIterator.hasNext()) {
            loBeanPedidoDet = loIterator.next();
            loStbDet = new StringBuilder();

            loStbDet.append("INSERT INTO TrDetPedido ("
                    + "CodigoPedido,Empresa,CodigoArticulo,Cantidad,Monto,PrecioBase,Stock,Descuento," +
                    "CodPedido,NombreArticulo,Observacion,TipoDescuento,Bonificacion,"
                    + "CodAlmacen, Flete, PrecioFrac, CantidadFrac, Tipo,CodBonificacion, MontoSinDescuento, BonificacionFrac, CantidadPresentacion," +
                    "MontoSoles, MontoDolar, PrecioBaseSoles, PrecioBaseDolar, TipoMoneda) VALUES ('");


            loStbDet.append(strId);
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getEmpresa());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getCodigoArticulo());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getCantidad());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getMonto());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getPrecioBase());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getStock());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getDescuento());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getCodPedido());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getNombreArticulo());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getObservacion());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getTipoDescuento());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getBonificacion());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getCodAlmacen());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getFlete());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getPrecioFrac());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getCantidadFrac());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getTipo());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getCodBonificacion());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getMontoSinDescuento());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getBonificacionFrac());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getCantidadPre());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getMontoSoles());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getMontoDolar());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getPrecioBaseSoles());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getPrecioBaseDolares());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getTipoMoneda());
            loStbDet.append("')");

            try {
                myDB.execSQL(loStbDet.toString());
            } catch (Exception e) {
                Log.e("XXX", "FALLO EL DETALLE!!!", e);
                return "";
            }
        }

        StringBuilder loStbCtrl;
        Iterator<BeanEnvPedidoCtrl> loIteratorCtrl = psBean.getListaCtrlPedido().iterator();
        BeanEnvPedidoCtrl loBeanPedidoCtrl = null;

        while (loIteratorCtrl.hasNext()) {
            loBeanPedidoCtrl = loIteratorCtrl.next();
            loStbCtrl = new StringBuilder();

            loStbCtrl.append("INSERT INTO TrCtrlPedido ("
                    + "CodigoPedido,EtiquetaControl,IdTipoControl,ValorControl,IdControl,IdFormulario) VALUES ('");
            loStbCtrl.append(strId);
            loStbCtrl.append("','");
            loStbCtrl.append(loBeanPedidoCtrl.getEtiquetaControl());
            loStbCtrl.append("','");
            loStbCtrl.append(loBeanPedidoCtrl.getIdTipoControl());
            loStbCtrl.append("','");
            loStbCtrl.append(loBeanPedidoCtrl.getValorControl());
            loStbCtrl.append("','");
            loStbCtrl.append(loBeanPedidoCtrl.getIdControl());
            loStbCtrl.append("','");
            loStbCtrl.append(loBeanPedidoCtrl.getIdFormulario());
            loStbCtrl.append("')");

            try {
                myDB.execSQL(loStbCtrl.toString());
            } catch (Exception e) {
                Log.e("XXX", "FALLO EL CONTROL!!!", e);
                return "";
            }
        }

        myDB.close();
        return strId;
    }

    public boolean fnUpdDireccion(String poPk, String psCodInc) {
        boolean lbResult = true;
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrCabPedido SET FlgNuevoDireccion = '"
                + Configuracion.FLGVERDADERO + "', CodDireccion = '" + poPk + "'"
                + " WHERE CodDireccion = " + psCodInc
                + " AND FlgNuevoDireccion = '" + Configuracion.FLGFALSO + "'");

        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {

            loDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            Log.e("DalPedido", "fnUpdDireccion", e);
            lbResult = false;
        }
        loDB.close();
        return lbResult;
    }

    /**
     * Graba la cabecera del pedido
     *
     * @param psBean pedidoBean a grabar
     */
    public String fnGrabarCabeceraPedido(BeanEnvPedidoCab psBean) {
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String strId = String.valueOf(System.currentTimeMillis());

        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("INSERT INTO TrCabPedido ("
                + "CodigoPedido,CodigoPedidoServidor,Empresa,CodigoUsuario,FechaInicio," +
                "FechaFin,CondicionVenta,CodigoCliente,CodigoMotivo,MontoTotal,CodDireccion," +
                "CodDireccionDespacho,Latitud,Longitud,Celda,ErrorConexion,ErrorPosicion," +
                "Observacion,CantidadTotal,FlgEnCobertura,FlgTerminado,FlgEnviado," +
                "BonificacionTotal,FlgNuevoDireccion, CodAlmacen, FleteTotal, TipoArticulo, FlgTipo, EnEdicion," +
                "MontoTotalSoles, MontoTotalDolar, TipoCambioSoles, TipoCambioDolar) VALUES ('");


        loStbCab.append(strId);
        loStbCab.append("','");
        loStbCab.append(psBean.getCodigoPedidoServidor());
        loStbCab.append("','");
        loStbCab.append(psBean.getEmpresa());
        loStbCab.append("','");
        loStbCab.append(psBean.getCodigoUsuario());
        loStbCab.append("','");
        loStbCab.append(psBean.getFechaInicio());
        loStbCab.append("','");
        loStbCab.append(psBean.getFechaFin());
        loStbCab.append("','");
        loStbCab.append(psBean.getCondicionVenta());
        loStbCab.append("','");
        loStbCab.append(psBean.getCodigoCliente());
        loStbCab.append("','");
        loStbCab.append(psBean.getCodigoMotivo());
        loStbCab.append("','");
        loStbCab.append(psBean.getMontoTotal());
        loStbCab.append("','");
        loStbCab.append(psBean.getCodDireccion());
        loStbCab.append("','");
        loStbCab.append(psBean.getCodDireccionDespacho());
        loStbCab.append("','");
        loStbCab.append(psBean.getLatitud());
        loStbCab.append("','");
        loStbCab.append(psBean.getLongitud());
        loStbCab.append("','");
        loStbCab.append(psBean.getCelda());
        loStbCab.append("','");
        loStbCab.append(psBean.getErrorConexion());
        loStbCab.append("','");
        loStbCab.append(psBean.getErrorPosicion());
        loStbCab.append("','");
        loStbCab.append(psBean.getObservacion());
        loStbCab.append("','");
        loStbCab.append(psBean.getCantidadTotal());
        loStbCab.append("','");
        loStbCab.append(psBean.getFlgEnCobertura());
        loStbCab.append("','");
        loStbCab.append(psBean.getFlgTerminado());
        loStbCab.append("','").append(Configuracion.FLGREGNOHABILITADO);
        loStbCab.append("','");
        loStbCab.append(psBean.getBonificacionTotal());
        loStbCab.append("','");
        loStbCab.append(psBean.getFlgNuevoDireccion());
        loStbCab.append("','");
        loStbCab.append(psBean.getCodAlmacen());
        loStbCab.append("','");
        loStbCab.append(psBean.getFleteTotal());
        loStbCab.append("','");
        loStbCab.append(psBean.getTipoArticulo());
        loStbCab.append("','");
        loStbCab.append(psBean.getFlgTipo());
        loStbCab.append("','");
        loStbCab.append(psBean.isEnEdicion() ? Configuracion.FLGVERDADERO : Configuracion.FLGFALSO);
        loStbCab.append("','");
        loStbCab.append(psBean.getMontoTotalSoles());
        loStbCab.append("','");
        loStbCab.append(psBean.getMontoTotalDolar());
        loStbCab.append("','");
        loStbCab.append(psBean.getTipoCambioSoles());
        loStbCab.append("','");
        loStbCab.append(psBean.getTipoCambioDolar());
        loStbCab.append("')");


        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            return "";
        }

        Log.v("DALPEDIDO", "GRABAPEDIDO " + loStbCab.toString());

        myDB.close();
        return strId;
    }

    public boolean fnUpdEstadoPedido(String codPed) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrCabPedido SET FlgEnviado='"
                + Configuracion.FLGREGHABILITADO + "' WHERE CodigoPedido = "
                + codPed);
        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public List<BeanEnvPedidoCab> fnEnvioSoloPedidosPendientes(String codVendedor) {
        List<BeanEnvPedidoCab> loPedidosPendientes = new ArrayList<BeanEnvPedidoCab>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"CodigoPedido", "Empresa",
                "CodigoUsuario", "FechaInicio", "FechaFin", "CondicionVenta",
                "CodigoCliente", "CodigoMotivo", "MontoTotal", "CodDireccion",
                "Latitud", "Longitud", "Celda", "ErrorConexion",
                "ErrorPosicion", "Observacion", "CantidadTotal", "FlgEnCobertura",
                "CodigoPedidoServidor", "FlgTerminado", "FlgEnviado", "BonificacionTotal",
                "FlgNuevoDireccion", "CodDireccionDespacho", "CodAlmacen", "FleteTotal", "TipoArticulo", "FlgTipo", "EnEdicion",
                "MontoTotalSoles", "MontoTotalDolar", "TipoCambioSoles", "TipoCambioDolar" };

        String lsTablas = "TrCabPedido";
        String lsWhere = "FlgEnviado='" + Configuracion.FLGREGNOHABILITADO
                + "' and CodigoUsuario = '" + codVendedor + "' and CodigoMotivo = ''";

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        BeanEnvPedidoCab loBeanPedidoCab = null;
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanPedidoCab = new BeanEnvPedidoCab();
                    loBeanPedidoCab.setCodigoPedido(loCursor.getString(0));
                    loBeanPedidoCab.setListaDetPedido(subDetallesPendientes(loCursor.getString(0)));
                    loBeanPedidoCab.setListaCtrlPedido(subControlesPendientes(loCursor.getString(0)));
                    loBeanPedidoCab.setEmpresa(loCursor.getString(1));
                    loBeanPedidoCab.setCodigoUsuario(loCursor.getString(2));
                    loBeanPedidoCab.setFechaInicio(loCursor.getString(3));
                    loBeanPedidoCab.setFechaFin(loCursor.getString(4));
                    loBeanPedidoCab.setCondicionVenta(loCursor.getString(5));
                    loBeanPedidoCab.setCodigoCliente(loCursor.getString(6));
                    loBeanPedidoCab.setCodigoMotivo(loCursor.getString(7));
                    loBeanPedidoCab.setMontoTotal(loCursor.getString(8));
                    loBeanPedidoCab.setCodDireccion(loCursor.getString(9));
                    loBeanPedidoCab.setLatitud(loCursor.getString(10));
                    loBeanPedidoCab.setLongitud(loCursor.getString(11));
                    loBeanPedidoCab.setCelda(loCursor.getString(12));
                    loBeanPedidoCab.setErrorConexion(loCursor.getString(13));
                    loBeanPedidoCab.setErrorPosicion(loCursor.getString(14));
                    loBeanPedidoCab.setObservacion(loCursor.getString(15));
                    loBeanPedidoCab.setCantidadTotal(loCursor.getString(16));
                    loBeanPedidoCab.setFlgEnCobertura(loCursor.getString(17));
                    loBeanPedidoCab.setCodigoPedidoServidor(loCursor.getString(18));
                    loBeanPedidoCab.setFlgTerminado(loCursor.getString(19));
                    loBeanPedidoCab.setBonificacionTotal(loCursor.getString(21));
                    loBeanPedidoCab.setFlgNuevoDireccion(loCursor.getString(22));
                    loBeanPedidoCab.setCodDireccionDespacho(loCursor.getString(23));
                    loBeanPedidoCab.setCodAlmacen(loCursor.getString(24));
                    loBeanPedidoCab.setFleteTotal(loCursor.getString(25));
                    loBeanPedidoCab.setTipoArticulo(loCursor.getString(26));
                    loBeanPedidoCab.setFlgTipo(loCursor.getString(27));
                    loBeanPedidoCab.setEnEdicion(loCursor.getString(28).equals(Configuracion.FLGVERDADERO));
                    loBeanPedidoCab.setMontoTotalSoles(loCursor.getString(29));
                    loBeanPedidoCab.setMontoTotalDolar(loCursor.getString(30));
                    loBeanPedidoCab.setTipoCambioSoles(loCursor.getString(31));
                    loBeanPedidoCab.setTipoCambioDolar(loCursor.getString(32));
                    loPedidosPendientes.add(loBeanPedidoCab);

                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loPedidosPendientes;
    }

    public boolean fnDeletePedido(String codPed) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        StringBuffer loStbDet = new StringBuffer();
        StringBuffer loStbContr = new StringBuffer();

        loStbCab.append("DELETE FROM TrCabPedido WHERE CodigoPedido = '" + codPed + "'");
        loStbDet.append("DELETE FROM TrDetPedido WHERE CodigoPedido = '" + codPed + "'");
        loStbContr.append("DELETE FROM TrCtrlPedido WHERE CodigoPedido = '" + codPed + "'");

        Log.v("XXX", "eliminando cabecera: " + loStbCab.toString());
        Log.v("XXX", "eliminando detalle: " + loStbDet.toString());
        Log.v("XXX", "eliminando controles: " + loStbContr.toString());

        try {
            myDB.execSQL(loStbCab.toString());
            myDB.execSQL(loStbDet.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnUpdCodigoPedidoServidor(String codPed, String codPedServidor) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrCabPedido SET CodigoPedidoServidor='"
                + codPedServidor + "' WHERE CodigoPedido = "
                + codPed);
        Log.v("XXX", "actualizando codigo pedido servidor..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    /**
     * Actualiza la cabecera del pedido e inserta detalles de pedido
     *
     * @param psBean pedidoBean a actualizar
     */
    public boolean fnActualizarPedido(BeanEnvPedidoCab psBean) {

        boolean lbResult = true;

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrCabPedido SET FlgTerminado='"
                + psBean.getFlgTerminado() + "' , FechaFin='"
                + psBean.getFechaFin() + "' , MontoTotal='"
                + psBean.getMontoTotal() + "' , MontoTotalSoles='" //@JBELVY
                + psBean.getMontoTotalSoles() + "' , MontoTotalDolar='" //@JBELVY
                + psBean.getMontoTotalDolar() + "' , Observacion='"
                + psBean.getObservacion() + "' , CodDireccion='"
                + psBean.getCodDireccion() + "' , CodDireccionDespacho='"
                + psBean.getCodDireccionDespacho() + "' , CantidadTotal='"
                + psBean.getCantidadTotal() + "' , FlgEnCobertura='"
                + psBean.getFlgEnCobertura() + "' , FlgNuevoDireccion='"
                + psBean.getFlgNuevoDireccion() + "' , CodAlmacen='"
                + psBean.getCodAlmacen() + "' , FleteTotal='"
                + psBean.getFleteTotal() + "' , TipoArticulo='"
                + psBean.getTipoArticulo() + "' , BonificacionTotal='"
                + psBean.getBonificacionTotal() + "' , EnEdicion='"
                + (psBean.isEnEdicion() ? Configuracion.FLGVERDADERO : Configuracion.FLGFALSO)
                + "' WHERE CodigoPedido = "
                + psBean.getCodigoPedido());
        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }

        StringBuilder loStbDet;

        Iterator<BeanEnvPedidoDet> loIterator = psBean.getListaDetPedido()
                .iterator();
        Log.v("XXX", "NUMERO DETALLES:: " + psBean.getListaDetPedido().size());

        BeanEnvPedidoDet loBeanPedidoDet = null;

        while (loIterator.hasNext()) {
            loBeanPedidoDet = loIterator.next();
            loStbDet = new StringBuilder();

            loStbDet.append("INSERT INTO TrDetPedido ("
                    + "CodigoPedido,Empresa,CodigoArticulo,Cantidad,Monto,PrecioBase,Stock," +
                    "Descuento,CodPedido,NombreArticulo,Observacion,TipoDescuento,Bonificacion,"
                    + "CodAlmacen, Flete, PrecioFrac, CantidadFrac, Tipo, CodBonificacion, MontoSinDescuento, BonificacionFrac, cantidadPresentacion," +
                    "MontoSoles, MontoDolar, PrecioBaseSoles, PrecioBaseDolar, TipoMoneda) VALUES ('");


            loStbDet.append(psBean.getCodigoPedido());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getEmpresa());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getCodigoArticulo());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getCantidad());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getMonto());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getPrecioBase());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getStock());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getDescuento());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getCodPedido());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getNombreArticulo());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getObservacion());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getTipoDescuento());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getBonificacion());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getCodAlmacen());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getFlete());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getPrecioFrac());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getCantidadFrac());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getTipo());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getCodBonificacion());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getMontoSinDescuento());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getBonificacionFrac());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getCantidadPre());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getMontoSoles());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getMontoDolar());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getPrecioBaseSoles());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getPrecioBaseDolares());
            loStbDet.append("','");
            loStbDet.append(loBeanPedidoDet.getTipoMoneda());
            loStbDet.append("')");

            Log.v("XXX", "insertando detalles..." + loStbDet.toString());
            try {
                myDB.execSQL(loStbDet.toString());
                Log.v("XXX", "INSERTO DETALLE!!!");
            } catch (Exception e) {
                Log.v("XXX", "FALLO EL DETALLE!!!");
                lbResult = false;

            }

        }

        StringBuilder loStbCtrl;
        Iterator<BeanEnvPedidoCtrl> loIteratorCtrl = psBean.getListaCtrlPedido()
                .iterator();
        BeanEnvPedidoCtrl loBeanPedidoCtrl = null;

        while (loIteratorCtrl.hasNext()) {
            loBeanPedidoCtrl = loIteratorCtrl.next();
            loStbCtrl = new StringBuilder();

            loStbCtrl.append("INSERT INTO TrCtrlPedido ("
                    + "CodigoPedido,EtiquetaControl,IdTipoControl,ValorControl,IdControl,IdFormulario) VALUES ('");
            loStbCtrl.append(psBean.getCodigoPedido());
            loStbCtrl.append("','");
            loStbCtrl.append(loBeanPedidoCtrl.getEtiquetaControl());
            loStbCtrl.append("','");
            loStbCtrl.append(loBeanPedidoCtrl.getIdTipoControl());
            loStbCtrl.append("','");
            loStbCtrl.append(loBeanPedidoCtrl.getValorControl());
            loStbCtrl.append("','");
            loStbCtrl.append(loBeanPedidoCtrl.getIdControl());
            loStbCtrl.append("','");
            loStbCtrl.append(loBeanPedidoCtrl.getIdFormulario());
            loStbCtrl.append("')");

            try {
                myDB.execSQL(loStbCtrl.toString());
            } catch (Exception e) {
                Log.e("XXX", "FALLO EL CONTROL!!!", e);
                lbResult = false;
            }


        }
        myDB.close();
        return lbResult;
    }


    /**
     * Devuelve la cantidad de pedidos pendientes de envio
     *
     * @param codVendedor codigo del vendedor
     * @return el numero de pedidos pendientes
     */
    public int fnNumPedidosPendientes(String codVendedor) throws Exception {
        StringBuilder loSbSqlCab = new StringBuilder();
        loSbSqlCab.append("SELECT count(*) ").append("FROM TrCabPedido ")
                .append("WHERE FlgEnviado= ")
                .append(fnValueQuery(Configuracion.FLGREGNOHABILITADO))
                .append(" AND CodigoUsuario = ")
                .append(fnValueQuery(codVendedor));
        getSQLQuery(loSbSqlCab.toString(), new RowListener() {

            @Override
            public void setRow(long piPosition, Cursor poCursor) {
                setObject(fnInteger(poCursor.getInt(0)));
            }
        });

        return (Integer) getObject();
    }

    public List<BeanEnvPedidoCab> fnEnvioPedidosPendientes(String codVendedor) {
        List<BeanEnvPedidoCab> loPedidosPendientes = new ArrayList<BeanEnvPedidoCab>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"CodigoPedido", "Empresa",
                "CodigoUsuario", "FechaInicio", "FechaFin", "CondicionVenta",
                "CodigoCliente", "CodigoMotivo", "MontoTotal", "CodDireccion",
                "Latitud", "Longitud", "Celda", "ErrorConexion",
                "ErrorPosicion", "Observacion", "CantidadTotal", "FlgEnCobertura",
                "CodigoPedidoServidor", "FlgTerminado", "FlgEnviado", "BonificacionTotal",
                "FlgNuevoDireccion", "CodDireccionDespacho", "CodAlmacen", "FleteTotal", "TipoArticulo", "FlgTipo", "EnEdicion",
                "MontoTotalSoles", "MontoTotalDolar", "TipoCambioSoles", "TipoCambioDolar" };

        String lsTablas = "TrCabPedido";
        String lsWhere = "FlgEnviado='" + Configuracion.FLGREGNOHABILITADO
                + "' and CodigoUsuario = '" + codVendedor + "' and CodigoMotivo = ''";

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        BeanEnvPedidoCab loBeanPedidoCab = null;
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanPedidoCab = new BeanEnvPedidoCab();

                    loBeanPedidoCab.setCodigoPedido(loCursor.getString(0));
                    loBeanPedidoCab.setListaDetPedido(subDetallesPendientes(loCursor.getString(0)));
                    loBeanPedidoCab.setListaCtrlPedido(subControlesPendientes(loCursor.getString(0)));
                    loBeanPedidoCab.setEmpresa(loCursor.getString(1));
                    loBeanPedidoCab.setCodigoUsuario(loCursor.getString(2));
                    loBeanPedidoCab.setFechaInicio(loCursor.getString(3));
                    loBeanPedidoCab.setFechaFin(loCursor.getString(4));
                    loBeanPedidoCab.setCondicionVenta(loCursor.getString(5));
                    loBeanPedidoCab.setCodigoCliente(loCursor.getString(6));
                    loBeanPedidoCab.setCodigoMotivo(loCursor.getString(7));
                    loBeanPedidoCab.setMontoTotal(loCursor.getString(8));
                    loBeanPedidoCab.setCodDireccion(loCursor.getString(9));
                    loBeanPedidoCab.setLatitud(loCursor.getString(10));
                    loBeanPedidoCab.setLongitud(loCursor.getString(11));
                    loBeanPedidoCab.setCelda(loCursor.getString(12));
                    loBeanPedidoCab.setErrorConexion(loCursor.getString(13));
                    loBeanPedidoCab.setErrorPosicion(loCursor.getString(14));
                    loBeanPedidoCab.setObservacion(loCursor.getString(15));
                    loBeanPedidoCab.setCantidadTotal(loCursor.getString(16));
                    loBeanPedidoCab.setFlgEnCobertura(loCursor.getString(17));
                    loBeanPedidoCab.setCodigoPedidoServidor(loCursor.getString(18));
                    loBeanPedidoCab.setFlgTerminado(loCursor.getString(19));
                    loBeanPedidoCab.setBonificacionTotal(loCursor.getString(21));
                    loBeanPedidoCab.setFlgNuevoDireccion(loCursor.getString(22));
                    loBeanPedidoCab.setCodDireccionDespacho(loCursor.getString(23));
                    loBeanPedidoCab.setCodAlmacen(loCursor.getString(24));
                    loBeanPedidoCab.setFleteTotal(loCursor.getString(25));
                    loBeanPedidoCab.setTipoArticulo(loCursor.getString(26));
                    loBeanPedidoCab.setFlgTipo(loCursor.getString(27));
                    loBeanPedidoCab.setEnEdicion(loCursor.getString(28).equals(Configuracion.FLGVERDADERO));
                    loBeanPedidoCab.setMontoTotalSoles(loCursor.getString(29));
                    loBeanPedidoCab.setMontoTotalDolar(loCursor.getString(30));
                    loBeanPedidoCab.setTipoCambioSoles(loCursor.getString(31));
                    loBeanPedidoCab.setTipoCambioDolar(loCursor.getString(32));
                    loPedidosPendientes.add(loBeanPedidoCab);
                    loBeanPedidoCab = null;

                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loPedidosPendientes;
    }

    private List<BeanEnvPedidoDet> subDetallesPendientes(String codPed) {
        List<BeanEnvPedidoDet> loDetallesPendientes = new ArrayList<BeanEnvPedidoDet>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"Empresa", "CodigoArticulo",
                "Cantidad", "Monto", "PrecioBase", "Stock", "Descuento",
                "CodPedido", "NombreArticulo", "Observacion", "TipoDescuento",
                "Bonificacion", "CodAlmacen", "Flete", "PrecioFrac", "CantidadFrac",
                "Tipo", "CodBonificacion", "MontoSinDescuento", "BonificacionFrac", "cantidadPresentacion",
                "MontoSoles","MontoDolar","PrecioBaseSoles","PrecioBaseDolar", "TipoMoneda"};


        String lsTablas = "TrDetPedido";

        String lsWhere = "CodigoPedido=" + codPed;

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);
        BeanEnvPedidoDet loBeanPedidoDet = null;
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanPedidoDet = new BeanEnvPedidoDet();
                    loBeanPedidoDet.setEmpresa(loCursor.getString(0));
                    loBeanPedidoDet.setCodigoArticulo(loCursor.getString(1));
                    loBeanPedidoDet.setCantidad(loCursor.getString(2));
                    loBeanPedidoDet.setMonto(loCursor.getString(3));
                    loBeanPedidoDet.setPrecioBase(loCursor.getString(4));
                    loBeanPedidoDet.setStock(loCursor.getString(5));
                    loBeanPedidoDet.setDescuento(loCursor.getString(6));
                    loBeanPedidoDet.setCodPedido(loCursor.getString(7));
                    loBeanPedidoDet.setNombreArticulo(loCursor.getString(8));
                    loBeanPedidoDet.setObservacion(loCursor.getString(9));
                    loBeanPedidoDet.setTipoDescuento(loCursor.getString(10));
                    loBeanPedidoDet.setBonificacion(loCursor.getString(11));
                    loBeanPedidoDet.setCodAlmacen(loCursor.getString(12));
                    loBeanPedidoDet.setFlete(loCursor.getString(13));
                    loBeanPedidoDet.setPrecioFrac(loCursor.getString(14));
                    loBeanPedidoDet.setCantidadFrac(loCursor.getString(15));
                    loBeanPedidoDet.setTipo(loCursor.getString(16));
                    loBeanPedidoDet.setCodBonificacion(loCursor.getInt(17));
                    loBeanPedidoDet.setMontoSinDescuento(loCursor.getString(18));
                    loBeanPedidoDet.setBonificacionFrac(loCursor.getString(19));
                    loBeanPedidoDet.setCantidadPre(loCursor.getString(20));
                    loBeanPedidoDet.setMontoSoles(loCursor.getString(21));
                    loBeanPedidoDet.setMontoDolar(loCursor.getString(22));
                    loBeanPedidoDet.setPrecioBaseSoles(loCursor.getString(23));
                    loBeanPedidoDet.setPrecioBaseDolares(loCursor.getString(24));
                    loBeanPedidoDet.setTipoMoneda(loCursor.getString(25));
                    loDetallesPendientes.add(loBeanPedidoDet);
                    loBeanPedidoDet = null;

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return loDetallesPendientes;
    }

    public List<BeanControl> subObtenerDatosControlesInicio(int formulario) {
        List<BeanControl> loLista = new ArrayList<BeanControl>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuilder lsQuery = new StringBuilder();

        lsQuery.append(" SELECT IdControl, idFormulario, EtiquetaControl, IdTipoControl, MaxCaracteres, IdGrupo, FlgObligatorio, Orden FROM CFControl ");
        if (formulario != 0) {
            lsQuery.append("WHERE C.idFormulario = '" + formulario + "'");
        }
        lsQuery.append(" ORDER BY C.ORDEN ");
        Log.v("SQL: DalPedido", lsQuery.toString());
        BeanControl loControl = null;
        Cursor loCursor = myDB.rawQuery(lsQuery.toString(), null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loControl = new BeanControl();
                    loControl.setCodigo(loCursor.getInt(0));
                    loControl.setFormulario(loCursor.getInt(1));
                    loControl.setEtiquetaTexto(loCursor.getString(2));
                    loControl.setTipo(loCursor.getInt(3));
                    loControl.setMaxCaracteres(loCursor.getInt(4));
                    loControl.setGrupo(loCursor.getInt(5));
                    loControl.setIsObligatorio(loCursor.getString(6));
                    loControl.setOrden(loCursor.getInt(7));
                    loLista.add(loControl);

                } while (loCursor.moveToNext());
            }
            loCursor.deactivate();
        }
        loCursor.close();

        String[] laColumnas = new String[]{"EtiquetaControl", "IdTipoControl",
                "ValorControl", "IdControl", "IdFormulario"};
        String lsTablas = "TrCtrlPedido";

        Cursor loCursor2;
        loCursor2 = myDB.query(lsTablas, laColumnas, null, null, null, null,
                null);

        BeanEnvPedidoCtrl loBeanPedidoCtrl = null;
        if (loCursor2 != null) {
            if (loCursor2.moveToFirst()) {
                do {
                    loBeanPedidoCtrl = new BeanEnvPedidoCtrl();
                    loBeanPedidoCtrl.setEtiquetaControl(loCursor.getString(0));
                    loBeanPedidoCtrl.setIdTipoControl(loCursor.getString(1));
                    loBeanPedidoCtrl.setValorControl(loCursor.getString(2));
                    loBeanPedidoCtrl.setIdControl(loCursor.getString(3));
                    loBeanPedidoCtrl.setIdFormulario(loCursor.getString(4));
                    loBeanPedidoCtrl = null;

                } while (loCursor.moveToNext());
            }
        }


        myDB.close();
        return loLista;
    }

    private List<BeanEnvPedidoCtrl> subControlesPendientes(String codPed) {
        List<BeanEnvPedidoCtrl> loControlesPendientes = new ArrayList<BeanEnvPedidoCtrl>();

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"EtiquetaControl", "IdTipoControl",
                "ValorControl", "IdControl", "IdFormulario"};
        String lsTablas = "TrCtrlPedido";
        String lsWhere = "CodigoPedido=" + codPed;

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        BeanEnvPedidoCtrl loBeanPedidoCtrl = null;
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanPedidoCtrl = new BeanEnvPedidoCtrl();
                    loBeanPedidoCtrl.setEtiquetaControl(loCursor.getString(0));
                    loBeanPedidoCtrl.setIdTipoControl(loCursor.getString(1));
                    loBeanPedidoCtrl.setValorControl(loCursor.getString(2));
                    loBeanPedidoCtrl.setIdControl(loCursor.getString(3));
                    loBeanPedidoCtrl.setIdFormulario(loCursor.getString(4));
                    loControlesPendientes.add(loBeanPedidoCtrl);
                    loBeanPedidoCtrl = null;

                } while (loCursor.moveToNext());
            }
        }
        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loControlesPendientes;

    }


    public boolean fnUpdEstadoPedidosPendientes(BeanUsuario piUsuario) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrCabPedido SET FlgEnviado='"
                + Configuracion.FLGREGHABILITADO + "'" + "WHERE FlgEnviado='"
                + Configuracion.FLGREGNOHABILITADO + "' and CodigoUsuario = '"
                + piUsuario.getCodVendedor() + "'");
        Log.v("XXX", "actualizando Pend..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());

        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnUpdEstadoPedidoPendiente(String codigoPedido, String codigoVendedor) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("update TrCabPedido set FlgEnviado='"
                + Configuracion.FLGREGHABILITADO + "'" + " where FlgEnviado='"
                + Configuracion.FLGREGNOHABILITADO + "' and CodigoUsuario = '"
                + codigoVendedor + "' and  CodigoPedido = '" + codigoPedido + "'");
        Log.v("XXX", "actualizando Pend..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnUpdEstadoNoPedidoPendiente(BeanUsuario piUsuario) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrCabPedido SET FlgEnviado='"
                + Configuracion.FLGREGHABILITADO + "'" + "WHERE FlgEnviado='"
                + Configuracion.FLGREGNOHABILITADO + "' and CodigoUsuario = '"
                + piUsuario.getCodVendedor() + "' and CodigoMotivo <> ''");
        Log.v("XXX", "actualizando Pend..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());

        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnDelTrDetPedido() {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("delete from TrDetPedido");
        Log.v("XXX", "eliminando detalle..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnDelTrCabPedido() {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("delete from TrCabPedido");
        Log.v("XXX", "eliminando cabecera..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    /**
     * Elimina cabecera de pedido
     *
     * @param psCodPedido codigo del pedido a eliminar
     */
    public boolean fnDelTrCabPedidoCodigo(String psCodPedido) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("delete from TrCabPedido WHERE CodigoPedido = '" + psCodPedido + "'");

        Log.v("XXX", "eliminando cabecera..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public int fnCountPedidosOnline() {

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"codigoPedidoServidor", "fechaInicio", "codigoCliente",
                "codigoUsuario", "montoTotal", "montoTotalSoles", "montoTotalDolar"};
        String lsTablas = "TBL_REP_PEDIDO";

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, null, null, null, null,
                "codigoPedidoServidor");

        int count = loCursor.getCount();
        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return count;
    }

    public LinkedList<BeanEnvPedidoCab> fnListarPedidosOnline() {
        LinkedList<BeanEnvPedidoCab> loLista = new LinkedList<BeanEnvPedidoCab>();

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"codigoPedidoServidor", "empresa", "codigoUsuario",
                "codigoCliente", "pkCliente", "pkAlmacen", "condicionVenta", "montoTotal", "flgTerminado",
                "codigoMotivo", "tipoArticulo", "flgTipo", "fechaInicio", "fechaFin", "observacion", "procesado",
                "nombreCliente", "cantidadTotal", "fechaRegistro", "montoTotalSoles", "montoTotalDolar" };
        String lsTablas = "TBL_REP_PEDIDO";

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, null, null, null, null, "fechaRegistro desc");

        if (loCursor != null) {

            if (loCursor.moveToFirst()) {
                do {

                    BeanEnvPedidoCab pedido = new BeanEnvPedidoCab();
                    pedido.setCodigoPedidoServidor(loCursor.getString(0));
                    pedido.setEmpresa(loCursor.getString(1));
                    pedido.setCodigoUsuario(loCursor.getString(2));
                    pedido.setCodigoCliente(loCursor.getString(3));
                    pedido.setClipk(loCursor.getString(4));
                    pedido.setCodAlmacen(loCursor.getString(5));
                    pedido.setCondicionVenta(loCursor.getString(6));
                    pedido.setMontoTotal(loCursor.getString(7));
                    pedido.setFlgTerminado(loCursor.getString(8));
                    pedido.setCodigoMotivo(loCursor.getString(9));
                    pedido.setTipoArticulo(loCursor.getString(10));
                    pedido.setFlgTipo(loCursor.getString(11));
                    pedido.setFechaInicio(loCursor.getString(12));
                    pedido.setFechaFin(loCursor.getString(13));
                    pedido.setObservacion(loCursor.getString(14));
                    pedido.setProcesado(loCursor.getString(15).equals(Configuracion.FLGVERDADERO));
                    pedido.setNombreCliente(loCursor.getString(16));
                    pedido.setCantidadTotal(loCursor.getString(17));
                    pedido.setFechaRegistro(loCursor.getString(18));
                    pedido.setMontoTotalSoles(loCursor.getString(19)); //@JBELVY
                    pedido.setMontoTotalDolar(loCursor.getString(20)); //@JBELVY
                    pedido.setListaDetPedido(fnListarPedidosDetalle(pedido.getCodigoPedidoServidor()));
                    pedido.setListaCtrlPedido(fnListarPedidosDetalleControles(pedido.getCodigoPedidoServidor()));
                    loLista.add(pedido);
                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loLista;
    }

    public BeanEnvPedidoCab fnListarPedidosOnlineByPedpk(String pedPK) {

        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"pkCliente", "fechaFin", "codigoCliente",
                "montoTotal", "cantidadTotal", "pkAlmacen", "montoTotalSoles", "montoTotalDolar" };
        String lsTablas = "TBL_REP_PEDIDO";
        String lsWhere = "codigoPedidoServidor=" + pedPK;

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                "codigoPedidoServidor");
        BeanEnvPedidoCab pedido = null;
        if (loCursor != null) {

            if (loCursor.moveToFirst()) {
                do {

                    pedido = new BeanEnvPedidoCab();
                    pedido.setClipk(loCursor.getString(0));
                    pedido.setFechaFin(loCursor.getString(1));
                    pedido.setCodigoCliente(loCursor.getString(2));
                    pedido.setMontoTotal(loCursor.getString(3));
                    pedido.setCantidadTotal(loCursor.getString(4));
                    pedido.setCodAlmacen(loCursor.getString(5));
                    pedido.setMontoTotalSoles(loCursor.getString(6));
                    pedido.setMontoTotalDolar(loCursor.getString(7));
                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return pedido;
    }

    public LinkedList<BeanEnvPedidoDet> fnListarPedidosDetalle(String pedPK) {
        LinkedList<BeanEnvPedidoDet> LoLista = new LinkedList<BeanEnvPedidoDet>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] lsColumnas = new String[]{"codigoPedidoDetalleServidor", "codigoPedidoServidor", "empresa",
                "pkArticulo", "codigoArticulo", "cantidad", "bonificacion", "precio", "monto",
                "stock", "descuento", "tipoDescuento", "nombreArticulo", "codigoListaPrecio", "pkAlmacen", "flete",
                "pkBonificacion", "editarBonificacion", "montoSinDescuento", "observacion",
                "cantidadFraccion", "precioFraccion", "cantidadPresentacion", "bonificacionFraccion",
                "productoCodigo", "productoNombre", "productoUnidadDefecto", "productoPrecioBase",
                "precioSoles", "precioDolar", "montoSoles", "montoDolar", "tipoMoneda" }; //@JBELVY
        String lsTablas = "TBL_REP_PEDIDO_DET";
        String lsWhere = "codigoPedidoServidor=" + pedPK;

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, lsColumnas, lsWhere, null, null, null,
                "codigoPedidoDetalleServidor");

        BeanEnvPedidoDet loBeanPedidoDet = null;
        if (loCursor != null) {

            if (loCursor.moveToFirst()) {
                do {
                    loBeanPedidoDet = new BeanEnvPedidoDet();
                    loBeanPedidoDet.setEmpresa(loCursor.getString(2));
                    loBeanPedidoDet.setIdArticulo(loCursor.getString(3));
                    loBeanPedidoDet.setCodigoArticulo(loCursor.getString(4));
                    loBeanPedidoDet.setCantidad(loCursor.getString(5));
                    loBeanPedidoDet.setBonificacion(loCursor.getString(6));
                    loBeanPedidoDet.setPrecioBase(loCursor.getString(7));
                    loBeanPedidoDet.setMonto(loCursor.getString(8));
                    loBeanPedidoDet.setStock(loCursor.getString(9));
                    loBeanPedidoDet.setDescuento(loCursor.getString(10));
                    loBeanPedidoDet.setTipoDescuento(loCursor.getString(11));
                    loBeanPedidoDet.setNombreArticulo(loCursor.getString(12));
                    loBeanPedidoDet.setCodListaArticulo(loCursor.getString(13));
                    loBeanPedidoDet.setIdListaPrecio(loCursor.getString(13));
                    loBeanPedidoDet.setCodAlmacen(loCursor.getString(14));
                    loBeanPedidoDet.setFlete(loCursor.getString(15));
                    loBeanPedidoDet.setCodBonificacion(Integer.parseInt(loCursor.getString(16)));
                    loBeanPedidoDet.setEditBonificacion(loCursor.getString(17).equals(Configuracion.FLGVERDADERO));
                    loBeanPedidoDet.setMontoSinDescuento(loCursor.getString(18));
                    loBeanPedidoDet.setObservacion(loCursor.getString(19));
                    loBeanPedidoDet.setCantidadFrac(loCursor.getString(20));
                    loBeanPedidoDet.setPrecioFrac(loCursor.getString(21));
                    loBeanPedidoDet.setCantidadPre(loCursor.getString(22));
                    loBeanPedidoDet.setBonificacionFrac(loCursor.getString(23));
                    BeanProducto producto = new BeanProducto();
                    producto.setCodigo(loCursor.getString(24));
                    producto.setNombre(loCursor.getString(25));
                    producto.setUnidadDefecto(loCursor.getString(26));
                    producto.setPrecioBase(loCursor.getString(27));
                    producto.setPrecioBaseSoles(loCursor.getString(28));
                    producto.setPrecioBaseDolares(loCursor.getString(29));
                    loBeanPedidoDet.setPrecioBaseSoles(loCursor.getString(28));
                    loBeanPedidoDet.setPrecioBaseDolares(loCursor.getString(29));
                    loBeanPedidoDet.setMontoSoles(loCursor.getString(30)); //@JBELVY
                    loBeanPedidoDet.setMontoDolar(loCursor.getString(31)); //@JBELVY
                    loBeanPedidoDet.setTipoMoneda(loCursor.getString(32)); //@JBELVY
                    loBeanPedidoDet.setProducto(producto);
                    LoLista.add(loBeanPedidoDet);
                    loBeanPedidoDet = null;
                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return LoLista;
    }

    public LinkedList<BeanEnvPedidoCtrl> fnListarPedidosDetalleControles(String pedPK) {
        LinkedList<BeanEnvPedidoCtrl> controles = new LinkedList<BeanEnvPedidoCtrl>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] lsColumnas = new String[]{"idDetalleControl", "etiquetaControl", "idTipoControl", "valorControl", "idControl", "idFormulario"};
        String lsTablas = "TBL_REP_PEDIDO_DET_CONTROLES";
        String lsWhere = "codigoPedidoServidor=" + pedPK;

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, lsColumnas, lsWhere, null, null, null,
                "idDetalleControl");

        BeanEnvPedidoCtrl control = null;
        if (loCursor != null) {

            if (loCursor.moveToFirst()) {
                do {
                    control = new BeanEnvPedidoCtrl();
                    control.setEtiquetaControl(loCursor.getString(1));
                    control.setIdTipoControl(loCursor.getString(2));
                    control.setValorControl(loCursor.getString(3));
                    control.setIdControl(loCursor.getString(4));
                    control.setIdFormulario(loCursor.getString(5));
                    controles.add(control);
                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();
        return controles;
    }

    public List<BeanControl> fnSelControles(int formulario) {
        List<BeanControl> loLista = new ArrayList<BeanControl>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuilder lsQuery = new StringBuilder();
        lsQuery.append(" SELECT IdControl, idFormulario, EtiquetaControl, IdTipoControl, MaxCaracteres, IdGrupo, FlgObligatorio,FlgEditable,Orden FROM CFControl ");

        if (formulario != 0) {
            lsQuery.append("WHERE idFormulario = '" + formulario + "'");
        }

        lsQuery.append(" ORDER BY ORDEN ");

        Log.v("SQL: DalPedido", lsQuery.toString());

        BeanControl loControl = null;
        Cursor loCursor = myDB.rawQuery(lsQuery.toString(), null);

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loControl = new BeanControl();
                    loControl.setCodigo(loCursor.getInt(0));
                    loControl.setFormulario(loCursor.getInt(1));
                    loControl.setEtiquetaTexto(loCursor.getString(2));
                    loControl.setTipo(loCursor.getInt(3));
                    loControl.setMaxCaracteres(loCursor.getInt(4));
                    loControl.setGrupo(loCursor.getInt(5));
                    loControl.setIsObligatorio(loCursor.getString(6));
                    loControl.setIsEditable(loCursor.getString(7));
                    loControl.setOrden(loCursor.getInt(8));
                    loLista.add(loControl);

                } while (loCursor.moveToNext());
            }
            loCursor.deactivate();
        }

        loCursor.close();
        myDB.close();


        return loLista;
    }

    public List<BeanSpinner> fnSelOpcionesSpinerVariable(int grupo) {
        List<BeanSpinner> loLista = new ArrayList<BeanSpinner>();
        SQLiteDatabase myDB = null;

        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuilder lsQuery = new StringBuilder();
        lsQuery.append(" SELECT CodigoDetalle, NombreDetalle FROM CFGrupo");
        lsQuery.append(" WHERE IdGrupo = '" + grupo + "' ");
        lsQuery.append(" ORDER BY CodigoDetalle  ");

        Log.v("SQL: DalPedido", lsQuery.toString());

        BeanSpinner loSpinner = null;
        Cursor loCursor = myDB.rawQuery(lsQuery.toString(), null);

        loLista.add(0, new BeanSpinner((long) -1, "", "Seleccione..."));

        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loSpinner = new BeanSpinner();
                    loSpinner.setKey(loCursor.getString(0));
                    loSpinner.setDescription(loCursor.getString(1));
                    loLista.add(loSpinner);
                } while (loCursor.moveToNext());
            }

            loCursor.deactivate();
        }
        loCursor.close();
        myDB.close();
        return loLista;

    }

    /**
     * Devuelve la lista de BeanEnvPedidoCab validando las bonidicaciones en caso tuviera.
     *
     * @param poEnvPedidoCab BeanEnvPedidoCab
     * @return BeanEnvPedidoCab
     */
    public BeanEnvPedidoCab fnValidarBonificacion(BeanEnvPedidoCab poEnvPedidoCab, int piNumDecimales, String stockrestrictivo) {

        try {

            DalBonificacion loDalBonificacion = new DalBonificacion(ioContext);

            List<BeanEnvPedidoDet> loLstBeanEnvPedidoDet = new ArrayList<BeanEnvPedidoDet>();
            List<BeanBonificacion> loLstBeanBonificacion = loDalBonificacion.
                    fnListBonificacionXTipoArticulo(poEnvPedidoCab.getTipoArticulo());

            poEnvPedidoCab.subLimparBonificacion();

            for (BeanBonificacion loBeanBonificacion : loLstBeanBonificacion) {

                poEnvPedidoCab.subValidarBonificacion(ioContext, loBeanBonificacion, piNumDecimales, stockrestrictivo);

            }

        } catch (Exception e) {
            Log.e("DalPedido", "fnValidarBonificacion", e);
        }


        return poEnvPedidoCab;
    }

    public List<BeanEnvPedidoCab> fnEnvioNoPedidosPendientes(String codVendedor) {
        List<BeanEnvPedidoCab> loPedidosPendientes = new ArrayList<BeanEnvPedidoCab>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"CodigoPedido", "Empresa",
                "CodigoUsuario", "FechaInicio", "FechaFin", "CondicionVenta",
                "CodigoCliente", "CodigoMotivo", "MontoTotal", "CodDireccion",
                "Latitud", "Longitud", "Celda", "ErrorConexion",
                "ErrorPosicion", "Observacion", "CantidadTotal", "FlgEnCobertura",
                "CodigoPedidoServidor", "FlgTerminado", "FlgEnviado", "BonificacionTotal",
                "FlgNuevoDireccion", "CodDireccionDespacho", "CodAlmacen", "FleteTotal", "TipoArticulo", "FlgTipo",
                "MontoTotalSoles", "MontoTotalDolar", "TipoCambioSoles", "TipoCambioDolar" };

        String lsTablas = "TrCabPedido";
        String lsWhere = "FlgEnviado='" + Configuracion.FLGREGNOHABILITADO
                + "' and CodigoUsuario = '" + codVendedor + "' and CodigoMotivo <> ''";

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        BeanEnvPedidoCab loBeanPedidoCab = null;
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanPedidoCab = new BeanEnvPedidoCab();

                    loBeanPedidoCab.setListaDetPedido(subDetallesPendientes(loCursor.getString(0)));
                    loBeanPedidoCab.setListaCtrlPedido(subControlesPendientes(loCursor.getString(0)));
                    loBeanPedidoCab.setEmpresa(loCursor.getString(1));
                    loBeanPedidoCab.setCodigoUsuario(loCursor.getString(2));
                    loBeanPedidoCab.setFechaInicio(loCursor.getString(3));
                    loBeanPedidoCab.setFechaFin(loCursor.getString(4));
                    loBeanPedidoCab.setCondicionVenta(loCursor.getString(5));
                    loBeanPedidoCab.setCodigoCliente(loCursor.getString(6));
                    loBeanPedidoCab.setCodigoMotivo(loCursor.getString(7));
                    loBeanPedidoCab.setMontoTotal(loCursor.getString(8));
                    loBeanPedidoCab.setCodDireccion(loCursor.getString(9));
                    loBeanPedidoCab.setLatitud(loCursor.getString(10));
                    loBeanPedidoCab.setLongitud(loCursor.getString(11));
                    loBeanPedidoCab.setCelda(loCursor.getString(12));
                    loBeanPedidoCab.setErrorConexion(loCursor.getString(13));
                    loBeanPedidoCab.setErrorPosicion(loCursor.getString(14));
                    loBeanPedidoCab.setObservacion(loCursor.getString(15));
                    loBeanPedidoCab.setCantidadTotal(loCursor.getString(16));
                    loBeanPedidoCab.setFlgEnCobertura(loCursor.getString(17));
                    loBeanPedidoCab.setCodigoPedidoServidor(loCursor.getString(18));
                    loBeanPedidoCab.setFlgTerminado(loCursor.getString(19));
                    loBeanPedidoCab.setBonificacionTotal(loCursor.getString(21));
                    loBeanPedidoCab.setFlgNuevoDireccion(loCursor.getString(22));
                    loBeanPedidoCab.setCodDireccionDespacho(loCursor.getString(23));
                    loBeanPedidoCab.setCodAlmacen(loCursor.getString(24));
                    loBeanPedidoCab.setFleteTotal(loCursor.getString(25));
                    loBeanPedidoCab.setTipoArticulo(loCursor.getString(26));
                    loBeanPedidoCab.setFlgTipo(loCursor.getString(27));
                    loBeanPedidoCab.setMontoTotalSoles(loCursor.getString(28));
                    loBeanPedidoCab.setMontoTotalDolar(loCursor.getString(29));
                    loBeanPedidoCab.setTipoCambioSoles(loCursor.getString(30));
                    loBeanPedidoCab.setTipoCambioDolar(loCursor.getString(31));
                    loPedidosPendientes.add(loBeanPedidoCab);

                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loPedidosPendientes;
    }

    public boolean fnDeletePedidoXcodigoServidor(String codigoPedidoServidor) {

        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        StringBuffer loStbDet = new StringBuffer();
        StringBuffer loStbContr = new StringBuffer();

        loStbCab.append("DELETE FROM TrCabPedido WHERE CodigoPedidoServidor = '" + codigoPedidoServidor + "'");
        loStbDet.append("DELETE FROM TrDetPedido WHERE CodigoPedidoServidor = '" + codigoPedidoServidor + "'");
        loStbContr.append("DELETE FROM TrCtrlPedido WHERE CodigoPedidoServidor = '" + codigoPedidoServidor + "'");

        Log.v("XXX", "eliminando cabecera: " + loStbCab.toString());
        Log.v("XXX", "eliminando detalle: " + loStbDet.toString());
        Log.v("XXX", "eliminando controles: " + loStbContr.toString());

        try {
            myDB.execSQL(loStbCab.toString());
            myDB.execSQL(loStbDet.toString());
            myDB.execSQL(loStbContr.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public BeanEnvPedidoCab obtenerPedidoXCodigoServidor(String codigoPedidoServidor) {
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"CodigoPedido", "Empresa",
                "CodigoUsuario", "FechaInicio", "FechaFin", "CondicionVenta",
                "CodigoCliente", "CodigoMotivo", "MontoTotal", "CodDireccion",
                "Latitud", "Longitud", "Celda", "ErrorConexion",
                "ErrorPosicion", "Observacion", "CantidadTotal", "FlgEnCobertura",
                "CodigoPedidoServidor", "FlgTerminado", "FlgEnviado", "BonificacionTotal",
                "FlgNuevoDireccion", "CodDireccionDespacho", "CodAlmacen", "FleteTotal", "TipoArticulo", "FlgTipo", "EnEdicion",
                "MontoTotalSoles", "MontoTotalDolar", "TipoCambioSoles", "TipoCambioDolar"};

        String lsTablas = "TrCabPedido";
        String lsWhere = "CodigoPedidoServidor='" + codigoPedidoServidor + "'";

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        BeanEnvPedidoCab loBeanPedidoCab = new BeanEnvPedidoCab();
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanPedidoCab.setCodigoPedido(loCursor.getString(0));
                    loBeanPedidoCab.setListaDetPedido(subDetallesPendientes(loCursor.getString(0)));
                    loBeanPedidoCab.setListaCtrlPedido(subControlesPendientes(loCursor.getString(0)));
                    loBeanPedidoCab.setEmpresa(loCursor.getString(1));
                    loBeanPedidoCab.setCodigoUsuario(loCursor.getString(2));
                    loBeanPedidoCab.setFechaInicio(loCursor.getString(3));
                    loBeanPedidoCab.setFechaFin(loCursor.getString(4));
                    loBeanPedidoCab.setCondicionVenta(loCursor.getString(5));
                    loBeanPedidoCab.setCodigoCliente(loCursor.getString(6));
                    loBeanPedidoCab.setCodigoMotivo(loCursor.getString(7));
                    loBeanPedidoCab.setMontoTotal(loCursor.getString(8));
                    loBeanPedidoCab.setCodDireccion(loCursor.getString(9));
                    loBeanPedidoCab.setLatitud(loCursor.getString(10));
                    loBeanPedidoCab.setLongitud(loCursor.getString(11));
                    loBeanPedidoCab.setCelda(loCursor.getString(12));
                    loBeanPedidoCab.setErrorConexion(loCursor.getString(13));
                    loBeanPedidoCab.setErrorPosicion(loCursor.getString(14));
                    loBeanPedidoCab.setObservacion(loCursor.getString(15));
                    loBeanPedidoCab.setCantidadTotal(loCursor.getString(16));
                    loBeanPedidoCab.setFlgEnCobertura(loCursor.getString(17));
                    loBeanPedidoCab.setCodigoPedidoServidor(loCursor.getString(18));
                    loBeanPedidoCab.setFlgTerminado(loCursor.getString(19));
                    loBeanPedidoCab.setBonificacionTotal(loCursor.getString(21));
                    loBeanPedidoCab.setFlgNuevoDireccion(loCursor.getString(22));
                    loBeanPedidoCab.setCodDireccionDespacho(loCursor.getString(23));
                    loBeanPedidoCab.setCodAlmacen(loCursor.getString(24));
                    loBeanPedidoCab.setFleteTotal(loCursor.getString(25));
                    loBeanPedidoCab.setTipoArticulo(loCursor.getString(26));
                    loBeanPedidoCab.setFlgTipo(loCursor.getString(27));
                    loBeanPedidoCab.setEnEdicion(loCursor.getString(28).equals(Configuracion.FLGVERDADERO));
                    loBeanPedidoCab.setMontoTotalSoles(loCursor.getString(29));
                    loBeanPedidoCab.setMontoTotalDolar(loCursor.getString(30));
                    loBeanPedidoCab.setTipoCambioSoles(loCursor.getString(31));
                    loBeanPedidoCab.setTipoCambioDolar(loCursor.getString(32));
                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loBeanPedidoCab;
    }

    public boolean fnUpdEstadoPedidoOnline(String idpedido) {
        boolean lbResult = true;
        SQLiteDatabase loDB = null;
        loDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TBL_REP_PEDIDO SET procesado = '"
                + Configuracion.FLGVERDADERO + "'"
                + " WHERE codigoPedidoServidor = '" + idpedido + "'");

        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {

            loDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            Log.e("DalPedido", "fnUpdEstadoPedidoOnline", e);
            lbResult = false;
        }
        loDB.close();
        return lbResult;
    }

}