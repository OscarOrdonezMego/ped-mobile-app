package pe.entel.android.pedidos.http;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import pe.com.nextel.android.dal.BDFramework;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanEnvPedidoDet;
import pe.entel.android.pedidos.bean.BeanPresentacion;
import pe.entel.android.pedidos.bean.BeanRespPedidoOnline;
import pe.entel.android.pedidos.dal.DalAlmacen;
import pe.entel.android.pedidos.dal.DalProducto;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumUrl;

public class HttpUltPedido extends HttpConexion {

    private BeanEnvPedidoCab ioBeanEnvPedidoCab;
    private Context ioContext;
    private String idCliente;
    final BDFramework bd;

    private String isPresentacion;

    // CONSTRUCTOR
    public HttpUltPedido(BeanEnvPedidoCab loBeanPedidoCab, Context poContext, EnumTipActividad poEnumTipActividad) throws Exception {
        super(poContext, poContext
                .getString(R.string.httppedidoonline_dlgbuscqueda), poEnumTipActividad);
        ioBeanEnvPedidoCab = loBeanPedidoCab;
        bd = new BDFramework(poContext);
        ioContext = poContext;
    }

    public void subConHttp() {
        try {
            //RestTemplate restTemplate = new RestTemplate();
            //restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());
            //restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
            Log.v("URL", Configuracion.fnUrl(ioContext, EnumUrl.PEDIDOONLINE));
            Log.v("URL", new Gson().toJson(ioBeanEnvPedidoCab));

            //String lsObjeto = restTemplate.postForObject(Configuracion.fnUrl(ioContext, EnumUrl.PEDIDOONLINE), ioBeanEnvPedidoCab, String.class);
            String lsObjeto = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, EnumUrl.PEDIDOONLINE), ioBeanEnvPedidoCab);

            BeanRespPedidoOnline loBeanPedido = (BeanRespPedidoOnline) new Gson().fromJson(lsObjeto, BeanRespPedidoOnline.class);

            if (loBeanPedido.getIdResultado() == ConfiguracionNextel.CONSRESSERVIDOROK
                    || loBeanPedido.getIdResultado() == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {

                DalProducto loDalProducto = new DalProducto(ioContext);
                DalAlmacen loDalAlmacen = new DalAlmacen(ioContext);
                for (int i = 0; i < loBeanPedido.getPedido().getListaDetPedido().size(); i++) {
                    BeanEnvPedidoDet loBeanEnvPedidoDet = loBeanPedido.getPedido().getListaDetPedido().get(i);
                    long ldCodAlmacen = 0;
                    try {
                        ldCodAlmacen = Long.parseLong(loBeanEnvPedidoDet.getCodAlmacen());
                    } catch (Exception e) {
                        ldCodAlmacen = 0;
                    }

                    if (ioBeanEnvPedidoCab.getTipoArticulo().equals(Configuracion.TipoArticulo.PRESENTACION)) {
                        BeanPresentacion loBeanPresentacion = loDalProducto.fnSelxIdPresentacion(loBeanEnvPedidoDet.getIdArticulo());
                        if (loBeanPresentacion != null)
                            if (loBeanPresentacion.getProducto() != null)
                                loBeanPedido.getPedido().getListaDetPedido().get(i).setProducto(loBeanPresentacion.getProducto());
                    }
                    if (ldCodAlmacen > 0) {
                        if (ioBeanEnvPedidoCab.getTipoArticulo().equals(Configuracion.TipoArticulo.PRESENTACION)) {
                            loDalAlmacen.fnUpdPreStock(
                                    loBeanEnvPedidoDet.getCodAlmacen(),
                                    loBeanEnvPedidoDet.getIdArticulo(),
                                    loBeanEnvPedidoDet.getStock());
                        } else {
                            loDalAlmacen.fnUpdStock(
                                    loBeanEnvPedidoDet.getCodAlmacen(),
                                    loBeanEnvPedidoDet.getIdArticulo(),
                                    loBeanEnvPedidoDet.getStock());
                        }
                    } else {
                        if (ioBeanEnvPedidoCab.getTipoArticulo().equals(Configuracion.TipoArticulo.PRESENTACION)) {
                            loDalProducto.fnUpdStockPresentacion(loBeanEnvPedidoDet.getIdArticulo(), loBeanEnvPedidoDet.getStock());
                        } else {
                            loDalProducto.fnUpdStockProducto(loBeanEnvPedidoDet.getIdArticulo(), loBeanEnvPedidoDet.getStock());
                        }
                    }
                }
            }
            setHttpResponseObject(loBeanPedido);
        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, "Error HTTP: " + e.getMessage());
        }
    }

    @Override
    public boolean OnPreConnect() {
        if (!Utilitario.fnVerSignal(ioContext)) {
            setHttpResponseIdMessage(ConfiguracionNextel.CONSRESSERVIDORERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        subConHttp();
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            BeanRespPedidoOnline loBeanPedido = (BeanRespPedidoOnline) getHttpResponseObject();

            int liIdHttprespuesta = loBeanPedido.getIdResultado();
            String lsHttpRespuesta = loBeanPedido.getResultado();

            if (liIdHttprespuesta == ConfiguracionNextel.CONSRESSERVIDOROK
                    || liIdHttprespuesta == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {
                ((AplicacionEntel) ioContext.getApplicationContext()).setCurrentPedido(loBeanPedido.getPedido());
            } else {
                ((AplicacionEntel) ioContext.getApplicationContext()).setCurrentPedido(new BeanEnvPedidoCab());
            }

            setHttpResponseIdMessage(liIdHttprespuesta, lsHttpRespuesta);
        }
    }
}
