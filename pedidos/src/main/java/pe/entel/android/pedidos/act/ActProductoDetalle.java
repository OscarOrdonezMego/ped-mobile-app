package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.DecimalTextWatcher;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.util.Logger;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.bean.BeanAlmacen;
import pe.entel.android.pedidos.bean.BeanArticulo;
import pe.entel.android.pedidos.bean.BeanArticuloOnline;
import pe.entel.android.pedidos.bean.BeanConsultaProducto;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCtrl;
import pe.entel.android.pedidos.bean.BeanEnvPedidoDet;
import pe.entel.android.pedidos.bean.BeanFraccionamiento;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanPrecio;
import pe.entel.android.pedidos.bean.BeanPresentacion;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalAlmacen;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.dal.DalProducto;
import pe.entel.android.pedidos.dal.DalTipoCambio;
import pe.entel.android.pedidos.http.HttpConsultaProducto;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpProductoOnline;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumConfiguracion;
import pe.entel.android.pedidos.util.UtilitarioData;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;
import pe.entel.android.pedidos.widget.ListaPopup;

public class ActProductoDetalle extends NexActivity implements OnClickListener, NexMenuCallbacks, MenuSlidingListener, ListaPopup.OnItemClickPopup {

    private TextView txtCodigo, txtNombre, lblDescuento,
            txtStock, lblPrecio,
            lblSubtotal, txtSubtotal,
            lblSubtotalDolares, txtSubtotalDolares, //@JBELVY I
            lblSubtotalDesc, txtSubtotalDesc,
            lblSubtotalDolDesc, txtSubtotalDolDesc, //@JBELVY I
            lblBonificacion, lblFlete, txtPresentacion,
            lblPrecioFrac, txtPrecioFrac, lblSubtotalFrac,
            txtSubtotalFrac, ioTxtFechaUltVenta, lblTotalFlete,
            txtTotalFlete, ioTxtUltPrecio;

    private EditText txtPrecio,
            txtPrecioDolar, //@JBELVY I
            txtCantidad, txtBonificacion,
            txtDescuento, txtObservacion, txtFlete,
            txtFraccionamiento;

    private LinearLayout ioRowStock, ioRowDesc, ioRowFechaUltVenta,
            ioRowUltPrecio, ioRowFlete, ioRowAlmacen,
            ioRowPresentacion, ioRowFraccionamiento, ioRowTotalFrac,
            ioRowSubtotalDesc, ioRowFraccionamientoUno, ioRowStockbutton,
            ioRowFinalizar;

    private Button btnStock, btnfinalizar, btnfinalizar2,
            ioBtnVerificar, cmbAlmacen, cmbFraccionamiento;

    private Spinner spTipoMoneda; //@JBELVY I

    private final int POPUP_ALMACEN = 1;
    private final int POPUP_FRACCIONAMIENTO = 2;

    TextWatcher textWatcherPrecio, textWatcherCantidad, textWatcherDescuento,
            textWatcherFrac, textWatcherFlete, textWatcherFraccUno;

    BeanArticulo ioBeanArticulo = null;
    BeanEnvPedidoDet loBeanPedidoDet = null;

    private final int CONSICOVALIDAR = 0;
    private final int CONSRANGODESCPOR = 4;
    private final int CONSRANGODESCMON = 5;
    private final int CONSFLETE = 8;
    private final int CONSBACK = 3;
    private final int CONSICOVALIDARDCTO = 2;
    private final int CONSFINAL = 1;
    private final int CONSVALIDASTOCK = 6;
    private final int CONSVALBONIFICACION = 7;
    private final int CONSRANGODESCVOLPOR = 20;
    private final int CONSVALIDARFRACC = 21;
    private final int CONSVALIDARFRACCBONIF = 22;

    private AdapMenuPrincipalSliding _menu;

    private BeanUsuario loBeanUsuario = null;
    private BeanPrecio ioBeanPrecio = null;
    private BeanAlmacen ioBeanAlmacen = null;
    private BeanFraccionamiento ioBeanFraccionamiento = null;

    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDAHOME = 11;
    private final int VALIDASINCRONIZACION = 12;
    private final int ENVIOPENDIENTES = 13;
    private final int VALIDASINCRO = 14;
    private final int CONSSALIR = 15;
    private final int NOHAYPENDIENTES = 16;

    boolean estadoSincronizar = false;
    boolean editoPedido = false;

    int estadoSalir = 0;

    double montoFin = 0;
    double montoFinSoles = 0;
    double montoFinDolar = 0;
    double montoMinimo = 0;
    double montoMinimoSoles = 0; //@JBELVY
    double montoMinimoDolares = 0;//@JBELVY

    double montoMaximo = 0;
    double montoMaximoSoles = 0;//@JBELVY
    double montoMaximoDolares = 0;//@JBELVY

    String isCodAlmacenArticulo;

    //PARAMETROS DE CONFIGURACION
    private String isSimboloMoneda;
    private String isSimboloMonedaDolares;
    private String isNumeroDecimales;
    private String isDescuentoMinimo;
    private String isDescuentoMaximo;
    private String isDescPorc;
    private String isDescMonto;
    private String isBonificacion;
    private String isStockRestrictivo;
    private String isEditarPrecio;
    private String isConsStockLinea;
    private String isDescuento;
    private String isHabDescProd;
    private String isHabDescGen;
    private String isStock;
    private String isMostrarAlmacen;
    private String isPresentacion;
    private String isConsultarProducto;
    private String isBonificacionAutomatica;
    private String isBonificacionManual;
    private int isMaximoItemsPedido;
    private double isMontoMinimoPedido;
    private double isMontoMaximoPedido;
    private String isMaximoNumeroDecimales;
    private int iiMaximoNumeroDecimales;
    private String isVerificarPedido;

    private int CANTIDAD_ENTEROS;
    private int CANTIDAD_DECIMALES;

    private boolean isDescuentoVolumen;
    private Pair<String, String> pairDescuentos;
    private Pair<Integer, String> pairTipoCambioSoles;  //@JBELVY I
    private Pair<Integer, String> pairTipoCambioDolar;  //@JBELVY I

    private boolean ibHttpConsultarProducto;
    // Integracion Ntrack
    private DALNServices ioDALNservices;
    private BeanPresentacion ioBeanPresentacion;
    private String sharePreferenceBeanPedidoCab;
    Boolean existeProductoAlmacen = false;

    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logger.d("XXX", "ActProductoDetalle");
        ValidacionServicioUtil.validarServicio(this);

        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);

        isSimboloMoneda = Configuracion.EnumConfiguracion.SIMBOLO_MONEDA.getValor(this);
        isSimboloMonedaDolares = "$";
        isNumeroDecimales = Configuracion.EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(this);
        isDescuentoMinimo = Configuracion.EnumConfiguracion.RANGO_DESCUENTO_MINIMO.getValor(this);
        isDescuentoMaximo = Configuracion.EnumConfiguracion.RANGO_DESCUENTO_MAXIMO.getValor(this);
        isDescPorc = Configuracion.EnumConfiguracion.DESCUENTO_PORCENTAJE.getValor(this);
        isDescMonto = Configuracion.EnumConfiguracion.DESCUENTO_MONTO.getValor(this);
        isBonificacion = Configuracion.EnumFuncion.BONIFICACION.getValor(this);
        isStockRestrictivo = Configuracion.EnumConfiguracion.STOCK_RESTRICTIVO.getValor(this);
        isEditarPrecio = Configuracion.EnumConfiguracion.PRECIO_EDITABLE.getValor(this);
        isConsStockLinea = Configuracion.EnumConfiguracion.CONSULTAR_STOCK_LINEA.getValor(this);
        isDescuento = Configuracion.EnumFuncion.DESCUENTO.getValor(this);
        isHabDescProd = EnumConfiguracion.HABILITAR_DESCUENTO_PRODUCTO.getValor(this);
        isHabDescGen = EnumConfiguracion.HABILITAR_DESCUENTO_GENERAL.getValor(this);
        isStock = EnumConfiguracion.STOCK.getValor(this);
        isMostrarAlmacen = EnumConfiguracion.MOSTRAR_ALMACEN.getValor(this);
        isPresentacion = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(this);
        isConsultarProducto = Configuracion.EnumConfiguracion.CONSULTAR_PRODUCTO.getValor(this);
        isBonificacionAutomatica = EnumConfiguracion.BONIFICACION_AUTOMATICA.getValor(this);
        isBonificacionManual = EnumConfiguracion.BONIFICACION_MANUAL.getValor(this);
        isMaximoNumeroDecimales = EnumConfiguracion.MAXIMO_NUMERODECIMALES.getValor(this);
        isMaximoItemsPedido = Integer.parseInt(EnumConfiguracion.MAXIMO_ITEMS_PEDIDO.getValor(this));
        isVerificarPedido = Configuracion.EnumConfiguracion.VERIFICACION_PEDIDO.getValor(this);

        obtenerValorMontoPedido();

        setMaximoNumeroDecimales();
        sharePreferenceBeanPedidoCab = "beanPedidoCabVerificarPedido";

        if (UtilitarioData.fnObtenerPreferencesString(this, sharePreferenceBeanPedidoCab).equals("")) {
            sharePreferenceBeanPedidoCab = (!UtilitarioData.fnObtenerPreferencesBoolean(this, "pedidoPendiente"))
                    ? "beanPedidoCab" : "beanPedidoCabPendiente";
        }

        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);
        subIniMenu();
    }

    private void obtenerValorMontoPedido() {

        if (Configuracion.EnumConfiguracion.MONTO_MAXIMO_PEDIDO.getValor(this).equals(""))
            isMontoMaximoPedido = 0.0;
        else
            isMontoMaximoPedido = Double.parseDouble(Configuracion.EnumConfiguracion.MONTO_MAXIMO_PEDIDO.getValor(this));

        if (Configuracion.EnumConfiguracion.MONTO_MINIMO_PEDIDO.getValor(this).equals(""))
            isMontoMinimoPedido = 0.0;
        else
            isMontoMinimoPedido = Double.parseDouble(Configuracion.EnumConfiguracion.MONTO_MINIMO_PEDIDO.getValor(this));

    }

    @Override
    public void onBackPressed() {
        UtilitarioPedidos.mostrarDialogo(this, CONSBACK);
    }

    @Override
    public void subIniActionBar() {
        this.getActionBarGD().setTitle(getString(R.string.actproductodetalle_bartitulo));
    }

    @Override
    public void subSetControles() {
        setActionBarContentView(R.layout.productodetalle2);
        super.subSetControles();

        txtCodigo = (TextView) findViewById(R.id.actproductodetalle_txtCodigo);
        txtNombre = (TextView) findViewById(R.id.actproductodetalle_txtNombre);
        txtPrecio = (EditText) findViewById(R.id.actproductodetalle_txtPrecio);
        txtPrecioDolar = findViewById(R.id.actproductodetalle_txtPrecioDolares); //@JBELVY
        txtStock = (TextView) findViewById(R.id.actproductodetalle_txtStock);
        txtCantidad = (EditText) findViewById(R.id.actproductodetalle_txtCantidad);
        txtBonificacion = (EditText) findViewById(R.id.actproductodetalle_txtBonificacion);
        lblBonificacion = (TextView) findViewById(R.id.actproductodetalle_lblBonificacion);
        btnfinalizar = (Button) findViewById(R.id.actproductopedido_btnfinalizar);
        btnfinalizar2 = (Button) findViewById(R.id.actproductopedido_btnfinalizar2);
        lblDescuento = (TextView) findViewById(R.id.actproductodetalle_lblDescuento);
        lblPrecio = (TextView) findViewById(R.id.actproductodetalle_lblPrecio);
        lblSubtotal = (TextView) findViewById(R.id.actproductodetalle_lblSubtotal);
        lblSubtotalDolares = findViewById(R.id.actproductodetalle_lblSubtotalDolares);//@JBELVY I
        txtPresentacion = (TextView) findViewById(R.id.actproductodetalle_txtPresentacion);
        lblPrecioFrac = (TextView) findViewById(R.id.actproductodetalle_lblPrecioFrac);
        txtPrecioFrac = (TextView) findViewById(R.id.actproductodetalle_txtPrecioFrac);
        lblSubtotalFrac = (TextView) findViewById(R.id.actproductodetalle_lblSubtotalFrac);
        txtSubtotalFrac = (TextView) findViewById(R.id.actproductodetalle_txtSubtotalFrac);
        ioTxtFechaUltVenta = (TextView) findViewById(R.id.actproductodetalle_txtfechaultventa);
        ioTxtUltPrecio = (TextView) findViewById(R.id.actproductodetalle_txtultprecio);
        txtFraccionamiento = (EditText) findViewById(R.id.actproductodetalle_txtFraccionamiento);
        spTipoMoneda = findViewById(R.id.actproductodetalle_spTipoMoneda);

        txtCantidad.setOnEditorActionListener(new DoneOnEditorActionListener());
        txtFraccionamiento.setOnEditorActionListener(new DoneOnEditorActionListener());

        //@JBELVY I
        spTipoMoneda.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                fbObtenerTipoCambio();
                if(pairTipoCambioSoles == null && pairTipoCambioDolar == null ){
                    Toast toast1 = Toast.makeText(getApplicationContext(),"No existe tipo de cambio vigente.", Toast.LENGTH_SHORT);
                    toast1.show();
                    return;
                } else if (pairTipoCambioSoles == null){
                    Toast toast1 = Toast.makeText(getApplicationContext(),"No existe tipo de cambio Soles vigente.", Toast.LENGTH_SHORT);
                    toast1.show();
                    return;
                } else if (pairTipoCambioDolar == null){
                    Toast toast1 = Toast.makeText(getApplicationContext(),"No existe tipo de cambio Dólares vigente.", Toast.LENGTH_SHORT);
                    toast1.show();
                    return;
                }

                String valorDolar =  ioBeanPrecio.getPrecioDolares();
                String valorSoles =  ioBeanPrecio.getPrecioSoles();

                txtPrecio.setText("0.00");
                txtPrecioDolar.setText("0.00");

               if(position == 0){ //CONVERTIR A SOLES

                   if(valorSoles.equals("0") || valorSoles.equals("0.0") || valorSoles.equals("0.00") || valorSoles.equals("")){

                       Double resultSoles = UtilitarioData.fnRedondeo(Double.parseDouble(valorDolar) * Double.parseDouble(pairTipoCambioDolar.second),2);

                       txtPrecio.setText(resultSoles.toString());

                       Toast tstMsgPreSol = Toast.makeText(getApplicationContext(),
                               "Se Calculó El Precio Soles: " + resultSoles + " (Tipo De Cambio " + pairTipoCambioDolar.second +" Y Precio Dólares " + valorDolar + ").",
                               Toast.LENGTH_LONG);
                       tstMsgPreSol.show();

                   } else{
                       txtPrecio.setText(valorSoles);
                   }

                    lblPrecio.setText("Precio" + "("
                            + isSimboloMoneda + "):");
                    txtPrecio.setVisibility(View.VISIBLE);
                    txtPrecioDolar.setVisibility(View.GONE);


                } else { //CONVERTIR A DOLARES
                   if(valorDolar.equals("0") || valorDolar.equals("0.0") || valorDolar.equals("0.00") || valorDolar.equals("")){

                       Double resultDolar = UtilitarioData.fnRedondeo(Double.parseDouble(valorSoles) * Double.parseDouble(pairTipoCambioSoles.second),2);
                       txtPrecioDolar.setText(resultDolar.toString());

                       Toast tstMsgPreDol = Toast.makeText(getApplicationContext(),
                               "Se Calculó El Precio Dólares: " + resultDolar + " (Tipo De Cambio " + pairTipoCambioSoles.second + " Y Precio Soles " + valorSoles + ").",
                               Toast.LENGTH_LONG);
                       tstMsgPreDol.show();
                   } else{
                       txtPrecioDolar.setText(valorDolar);
                   }
                    lblPrecio.setText("Precio" + "("
                            + isSimboloMonedaDolares + "):");
                    txtPrecio.setVisibility(View.GONE);
                    txtPrecioDolar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //@JBELVY F

        setInputTypeEditText(txtCantidad);
        setInputTypeEditText(txtBonificacion);
        setInputTypeEditText(txtFraccionamiento);

        txtDescuento = (EditText) findViewById(R.id.actproductodetalle_txtDescuento);
        txtDescuento.setOnEditorActionListener(new DoneOnEditorActionListener());

        txtObservacion = (EditText) findViewById(R.id.actproductodetalle_txtObservacion);
        txtObservacion.setOnEditorActionListener(new DoneOnEditorActionListener());

        lblSubtotalDesc = (TextView) findViewById(R.id.actproductodetalle_lblSubtotalDesc);
        lblSubtotalDolDesc = findViewById(R.id.actproductodetalle_lblSubtotalDolaresDesc); //@JBELVY I

        txtSubtotalDesc = (TextView) findViewById(R.id.actproductodetalle_txtSubtotalDesc);
        txtSubtotalDolDesc =  findViewById(R.id.actproductodetalle_txtSubtotalDolaresDesc); //@JBELVY I

        txtSubtotal = (TextView) findViewById(R.id.actproductodetalle_txtSubtotal);
        txtSubtotalDolares = findViewById(R.id.actproductodetalle_txtSubtotalDolares); //@JBELVY I

        lblFlete = (TextView) findViewById(R.id.actproductodetalle_lblFlete);
        txtFlete = (EditText) findViewById(R.id.actproductodetalle_txtFlete);
        lblTotalFlete = (TextView) findViewById(R.id.actproductodetalle_lblTotalFlete);
        txtTotalFlete = (TextView) findViewById(R.id.actproductodetalle_txtTotalFlete);

        ioRowStock = (LinearLayout) findViewById(R.id.actproductodetalle_tablerow_stock);
        ioRowDesc = (LinearLayout) findViewById(R.id.actproductodetalle_tablerow_desc);
        ioRowFlete = (LinearLayout) findViewById(R.id.actproductodetalle_tableRow_flete);
        ioRowAlmacen = (LinearLayout) findViewById(R.id.actproductodetalle_tablerow_almacen);
        ioRowPresentacion = (LinearLayout) findViewById(R.id.actproductodetalle_tablerow_presentacion);
        ioRowFraccionamiento = (LinearLayout) findViewById(R.id.actproductodetalle_tablerow_fraccionamiento);
        ioRowFraccionamientoUno = (LinearLayout) findViewById(R.id.actproductodetalle_tablerow_fraccionamiento_uno);
        ioRowTotalFrac = (LinearLayout) findViewById(R.id.actproductodetalle_tableRow_totalFrac);
        ioRowSubtotalDesc = (LinearLayout) findViewById(R.id.actproductodetalle_tablerow_subTotalDesc);
        ioRowFinalizar = (LinearLayout) findViewById(R.id.actproductodetalle_tableRow_finalizar);
        ioRowStockbutton = (LinearLayout) findViewById(R.id.actproductodetalle_tablerow_stockbutton);
        ioRowUltPrecio = (LinearLayout) findViewById(R.id.actproductodetalle_tablerow_ultprecio);
        ioRowFechaUltVenta = (LinearLayout) findViewById(R.id.actproductodetalle_tablerow_fechaultventa);

        btnStock = (Button) findViewById(R.id.actproductopedido_btnstock);
        ioBtnVerificar = (Button) findViewById(R.id.actproductopedido_btnVerificar);
        cmbAlmacen = (Button) findViewById(R.id.actproductodetalle_cmbAlmacen);
        cmbFraccionamiento = (Button) findViewById(R.id.actproductodetalle_cmbFraccionamiento);

        if (isDescPorc.equals(Configuracion.FLGVERDADERO)
                && isDescMonto.equals(Configuracion.FLGFALSO))
            lblDescuento.setText(lblDescuento.getText().toString() + "(%):");

        else if (isDescPorc.equals(Configuracion.FLGFALSO)
                && isDescMonto.equals(Configuracion.FLGVERDADERO))
            lblDescuento.setText(lblDescuento.getText().toString() + "("
                    + isSimboloMoneda + "):");

        DalAlmacen dalAlmacen = new DalAlmacen(this);

        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            if (dalAlmacen.fnSelectAllPresentacion().size() > 0)
                existeProductoAlmacen = true;
        } else {
            if (dalAlmacen.fnSelectAllProducto().size() > 0)
                existeProductoAlmacen = true;
        }

        subCargaDetalle();
        if (isHabDescGen.equals(Configuracion.FLGVERDADERO)) {
            txtDescuento.setHint(isDescuentoMinimo.toString() + " -  " + isDescuentoMaximo.toString());
        }

        if (isDescPorc.equals(Configuracion.FLGVERDADERO)
                && isDescMonto.equals(Configuracion.FLGFALSO)) {
            txtDescuento.setHint(ioBeanPrecio.getDescuentoMinimo().toString() + " -  " + ioBeanPrecio.getDescuentoMaximo().toString());
        } else {
            if
            (isDescPorc.equals(Configuracion.FLGFALSO)
                    && isDescMonto.equals(Configuracion.FLGVERDADERO))
                txtDescuento.setHint(ioBeanPrecio.getDescuentoMinimo().toString() + " -  " + ioBeanPrecio.getDescuentoMaximo().toString());
        }
        subMostrarVerificar(true);

        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            ioRowPresentacion.setVisibility(View.VISIBLE);

            BeanPresentacion loBeanPresentacion = (BeanPresentacion) ioBeanArticulo;

            if (loBeanPresentacion.getUnidadFraccionamiento() == 1) {

                if (loBeanPresentacion.getCantidad() == loBeanPresentacion.getUnidadFraccionamiento()) {
                    ioRowFraccionamiento.setVisibility(View.VISIBLE);
                    cmbFraccionamiento.setVisibility(View.VISIBLE);
                    ioRowFraccionamientoUno.setVisibility(View.GONE);
                } else {
                    ioRowFraccionamiento.setVisibility(View.GONE);
                    cmbFraccionamiento.setVisibility(View.GONE);
                    ioRowFraccionamientoUno.setVisibility(View.VISIBLE);
                }
            } else {
                ioRowFraccionamiento.setVisibility(View.VISIBLE);
                cmbFraccionamiento.setVisibility(View.VISIBLE);
                ioRowFraccionamientoUno.setVisibility(View.GONE);
            }

        } else {
            ioRowFraccionamiento.setVisibility(View.GONE);
            ioRowPresentacion.setVisibility(View.GONE);
            cmbFraccionamiento.setVisibility(View.GONE);
            ioRowFraccionamientoUno.setVisibility(View.GONE);
        }

        if (isBonificacion.equals(Configuracion.FLGVERDADERO) &&
                isBonificacionManual.equals(Configuracion.FLGVERDADERO)) {
            txtBonificacion.setVisibility(View.VISIBLE);
            lblBonificacion.setVisibility(View.VISIBLE);
        } else {
            txtBonificacion.setVisibility(View.GONE);
            lblBonificacion.setVisibility(View.GONE);
        }

        txtPrecio.setEnabled(isEditarPrecio.equals(
                Configuracion.FLGVERDADERO));

        txtPrecioDolar.setEnabled(isEditarPrecio.equals(
                Configuracion.FLGVERDADERO)); //@JBELVY I

        if (isConsStockLinea.equals(
                Configuracion.FLGVERDADERO)) {
            //muestra boton para actualizar
            if (isStock.equals(Configuracion.FLGFALSO)) {
                ioRowStockbutton.setVisibility(View.GONE);
                ioRowStock.setVisibility(View.GONE);
            } else
                ioRowStock.setVisibility(View.GONE);
        } else {
            //muestra label
            if (isStock.equals(Configuracion.FLGFALSO)) {
                ioRowStock.setVisibility(View.GONE);
                ioRowStockbutton.setVisibility(View.GONE);
            } else
                ioRowStockbutton.setVisibility(View.GONE);
        }

        if (isDescuento.equals(Configuracion.FLGFALSO)) {
            ioRowDesc.setVisibility(View.GONE);
        }

        cmbAlmacen.setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DalAlmacen loDalAlmacen = new DalAlmacen(ActProductoDetalle.this);
                        List<BeanAlmacen> loLstBeanAlmacenes = null;

                        if (isPresentacion.equals(Configuracion.FLGVERDADERO))
                            loLstBeanAlmacenes = loDalAlmacen.fnSelectXCodPresentacion(ioBeanArticulo.getId());
                        else
                            loLstBeanAlmacenes = loDalAlmacen.fnSelectXCodProducto(ioBeanArticulo.getId());

                        if (loLstBeanAlmacenes.size() > 1)
                            new ListaPopup().dialog(ActProductoDetalle.this, loLstBeanAlmacenes,
                                    POPUP_ALMACEN, ActProductoDetalle.this,
                                    getString(R.string.listapopup_txtseleccionealmacen), ioBeanAlmacen,
                                    R.attr.PedidosPopupLista);
                    }
                }
        );

        cmbFraccionamiento.setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BeanPresentacion loBeanPresentacion = (BeanPresentacion) ioBeanArticulo;
                        if (loBeanPresentacion.getLstBeanFraccionamiento().size() > 1)
                            new ListaPopup().dialog(ActProductoDetalle.this, loBeanPresentacion.getLstBeanFraccionamiento(),
                                    POPUP_FRACCIONAMIENTO, ActProductoDetalle.this,
                                    getString(R.string.listapopup_txtseleccionefraccionamiento), ioBeanFraccionamiento,
                                    R.attr.PedidosPopupLista);
                    }
                }
        );

        ioBtnVerificar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (finalizar("3")) {
                    ibHttpConsultarProducto = true;
                    new HttpConsultaProducto(fnBeanConsultaProducto(), ActProductoDetalle.this, fnTipactividad(),
                            getString(R.string.actproductodetalle_consultarproducto)).execute();
                }
            }
        });

        btnfinalizar.setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isVerificarPedido.equals(Configuracion.FLGVERDADERO)) {
                            UtilitarioData.fnCrearPreferencesPutString(ActProductoDetalle.this, "MensajeVerificarPedido", "0");
                        }
                        finalizar("1");
                    }
                }
        );

        btnfinalizar2.setOnClickListener(
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        if (isVerificarPedido.equals(Configuracion.FLGVERDADERO)) {
                            UtilitarioData.fnCrearPreferencesPutString(ActProductoDetalle.this, "MensajeVerificarPedido", "0");
                        }
                        finalizar("2");
                    }
                }
        );

        btnStock.setOnClickListener(
                new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        subConexionHttp();
                    }
                }
        );

        textWatcherFraccUno = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("textWatcherFraccUno", "afterText");
                double precioCant;
                double precioFraccionamiento;
                int cantidadFraccionamiento;
                double montoFracc;
                double valorTotal;

                if (txtFraccionamiento.getText().toString().equals("")) {
                    cantidadFraccionamiento = 0;
                } else {
                    cantidadFraccionamiento = Integer.parseInt(txtFraccionamiento.getText().toString());
                }

                BeanPresentacion loPresentacion = (BeanPresentacion) ioBeanArticulo;

                if (cantidadFraccionamiento > 0) {

                    precioFraccionamiento = Double.parseDouble(TipoMonedaEs() ? txtPrecio.getText().toString() : txtPrecioDolar.getText().toString());

                    montoFracc = (precioFraccionamiento * cantidadFraccionamiento) / loPresentacion.getCantidad();

                    precioCant = Double.parseDouble(TipoMonedaEs() ? txtSubtotal.getText().toString() : txtSubtotalDolares.getText().toString());

                    valorTotal = montoFracc + precioCant;

                    ioRowTotalFrac.setVisibility(View.VISIBLE);
                    txtPrecioFrac.setText(Utilitario.fnRound(montoFracc,
                            Integer.valueOf(isNumeroDecimales)));

                    txtSubtotalFrac.setText(Utilitario.fnRound(valorTotal,
                            Integer.valueOf(isNumeroDecimales)));
                } else {
                    ioRowTotalFrac.setVisibility(View.GONE);
                    txtSubtotalFrac.setText("");
                }
            }
        };

        textWatcherPrecio = new TextWatcher() {

            public void afterTextChanged(Editable s) {
                Bundle loExtras = getIntent().getExtras();
                String strPrecio = "0";
                //@JBELVY I
                String strPrecioSoles = "0";
                String strPrecioDolar = "0";
                //@JBELVY F

               //-----------SOLES
                if(TipoMonedaEs()) {
                    if (TextUtils.isEmpty(txtPrecio.getText().toString())) {
                        strPrecio = ioBeanPrecio.getPrecio();
                        strPrecioSoles = ioBeanPrecio.getPrecioSoles();  //@JBELVY I
                    } else if (txtPrecio.getText().toString().substring(0, 1)
                            .equals(".")) {
                        txtPrecio.setText("0" + txtPrecio.getText().toString());
                        txtPrecio.setSelection(txtPrecio.getText().length());
                        strPrecio = txtPrecio.getText().toString();
                        strPrecioSoles = txtPrecio.getText().toString();
                    } else {
                        strPrecio = txtPrecio.getText().toString();
                        strPrecioSoles = txtPrecio.getText().toString();
                    }
                } else {
                    //-----------DOLARES
                    if (TextUtils.isEmpty(txtPrecioDolar.getText().toString())) {
                        strPrecioDolar = ioBeanPrecio.getPrecioDolares(); //@JBELVY I
                    } else if (txtPrecioDolar.getText().toString().substring(0, 1)
                            .equals(".")) {
                        txtPrecioDolar.setText("0" + txtPrecioDolar.getText().toString());
                        txtPrecioDolar.setSelection(txtPrecioDolar.getText().length());
                        strPrecioDolar = txtPrecioDolar.getText().toString();
                    } else {
                        strPrecioDolar = txtPrecioDolar.getText().toString();
                    }
                }
                //-----------CANTIDAD
                String strCantidad = "";
                String strDescuento = "";

                if (txtCantidad.getText().toString().equals("")) {
                    strCantidad = "0";
                } else if (txtCantidad.getText().toString().substring(0, 1)
                        .equals(".")) {
                    txtCantidad.setText("0" + txtCantidad.getText().toString());
                    txtCantidad.setSelection(txtCantidad.getText().length());
                    strCantidad = txtCantidad.getText().toString();
                } else {
                    strCantidad = txtCantidad.getText().toString();
                }

                if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                    strCantidad = obtenerEmpaques(strCantidad);
                }

                if (txtDescuento.getText().toString().equals("")) {
                    strDescuento = "0";
                } else if (txtDescuento.getText().toString().substring(0, 1)
                        .equals(".")) {
                    txtDescuento.setText("0"
                            + txtDescuento.getText().toString());
                    txtDescuento.setSelection(txtDescuento.getText().length());
                    strDescuento = txtDescuento.getText().toString();
                    if (Double.parseDouble(strDescuento) > 100.0) {
                        txtDescuento.setText("");
                        strDescuento = "0";
                    }
                } else {
                    strDescuento = txtDescuento.getText().toString();
                    if (Double.parseDouble(strDescuento) > 100.0) {
                        txtDescuento.setText("");
                        strDescuento = "0";
                    }
                }
                //--------CALCULO
                double precio;
                double precioSol;//@JBELVY
                double precioDol;//@JBELVY
                double cantidad;
                double montoFrac;
                double flete;
                try {
                    precio = Double.parseDouble(strPrecio);
                    //@JBELVY I
                    precioSol = Double.parseDouble(strPrecioSoles);//@JBELVY
                    precioDol = Double.parseDouble(strPrecioDolar);//@JBELVY
                    //@JBELVY F
                } catch (Exception e) {
                    precio = 0;
                    precioSol = 0;//@JBELVY
                    precioDol = 0;//@JBELVY
                }
                try {
                    cantidad = Double.parseDouble(strCantidad);
                } catch (Exception e) {
                    cantidad = 0;
                }
                try {
                    montoFrac = Double.parseDouble(txtPrecioFrac.getText().toString());
                } catch (Exception e) {
                    montoFrac = 0;
                }

                try {
                    flete = Double.parseDouble(txtFlete.getText().toString());
                } catch (Exception e) {
                    flete = 0;
                }

                double montoBruto = precio * cantidad;
                double montoBrutoSoles = precioSol * cantidad;//@JBELVY
                double montoBrutoDolares = precioDol * cantidad;//@JBELVY


                //txtSubtotal.setText(Utilitario.fnRound(montoBruto, Integer.valueOf(isNumeroDecimales))); //@JBELVY
                txtSubtotal.setText(Utilitario.fnRound(montoBrutoSoles, Integer.valueOf(isNumeroDecimales)));//@JBELVY
                txtSubtotalDolares.setText(Utilitario.fnRound(montoBrutoDolares, Integer.valueOf(isNumeroDecimales)));//@JBELVY

                double montoDesc = (Double.parseDouble(strDescuento) / 100) * (montoBruto);
                double montoDescSoles = (Double.parseDouble(strDescuento) / 100) * (montoBrutoSoles);//@JBELVY
                double montoDescDolares = (Double.parseDouble(strDescuento) / 100) * (montoBrutoDolares);//@JBELVY

                double montoFinal = montoBruto - montoDesc;
                double montoFinalSoles = montoBrutoSoles - montoDescSoles;//@JBELVY
                double montoFinalDolar = montoBrutoDolares - montoDescDolares;//@JBELVY

                double montoFinalFracc = montoFinal + montoFrac;
                double montoFinalFraccSoles = montoFinalSoles + montoFrac;//@JBELVY
                double montoFinalFraccDolar = montoFinalDolar + montoFrac;//@JBELVY

                double montoFinalFlete = (TipoMonedaEs() ? montoFinalFraccSoles : montoFinalFraccDolar) + flete;

                //txtSubtotalDesc.setText(Utilitario.fnRound(montoFinal, Integer.valueOf(isNumeroDecimales)));//@JBELVY
                txtSubtotalDesc.setText(Utilitario.fnRound(montoFinalSoles, Integer.valueOf(isNumeroDecimales)));//@JBELVY
                txtSubtotalDolDesc.setText(Utilitario.fnRound(montoFinalDolar, Integer.valueOf(isNumeroDecimales)));//@JBELVY


                //txtSubtotalFrac.setText(Utilitario.fnRound(montoFinalFracc, Integer.valueOf(isNumeroDecimales)));//@JBELVY
                txtSubtotalFrac.setText(Utilitario.fnRound(TipoMonedaEs() ? montoFinalFraccSoles : montoFinalFraccDolar, Integer.valueOf(isNumeroDecimales)));//@JBELVY

                txtTotalFlete.setText(Utilitario.fnRound(montoFinalFlete,
                        Integer.valueOf(isNumeroDecimales)));
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

        };

        textWatcherCantidad = new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Bundle loExtras = getIntent().getExtras();
                String strPrecio = loExtras.getString("PrcProducto");
                String strPrecioSoles = loExtras.getString("PrcProducto");
                String strPrecioDolar = loExtras.getString("PrcProducto");

                //----------SOLES
                if(TipoMonedaEs()) {
                    if (txtPrecio.getText().toString().equals("")) {
                        if (strPrecio.equals("")) {
                            strPrecio = ioBeanPrecio.getPrecio();//@JBELVY
                            strPrecioSoles = ioBeanPrecio.getPrecioSoles();//@JBELVY
                        }
                    } else if (txtPrecio.getText().toString().substring(0, 1)
                            .equals(".")) {
                        txtPrecio.setText("0" + txtPrecio.getText().toString());
                        txtPrecio.setSelection(txtPrecio.getText().length());
                        strPrecio = txtPrecio.getText().toString();
                        strPrecioSoles = txtPrecio.getText().toString();//@JBELVY
                    } else {
                        strPrecio = txtPrecio.getText().toString();
                        strPrecioSoles = txtPrecio.getText().toString();
                    }
                    strPrecioDolar = "0";
                } else {
                    //----------DOLAR @JBELVY I
                    if (txtPrecioDolar.getText().toString().equals("")) {
                        if (strPrecioDolar.equals("")) {
                            strPrecioDolar = ioBeanPrecio.getPrecioDolares();
                        }
                    } else if (txtPrecioDolar.getText().toString().substring(0, 1)
                            .equals(".")) {
                        txtPrecioDolar.setText("0" + txtPrecioDolar.getText().toString());
                        txtPrecioDolar.setSelection(txtPrecioDolar.getText().length());
                        strPrecioDolar = txtPrecioDolar.getText().toString();
                    } else {
                        strPrecioDolar = txtPrecioDolar.getText().toString();
                    }
                    strPrecio = "0";
                    strPrecioSoles = "0";
                }
                //----------DOLAR @JBELVY F
                String strCantidad = "";
                String strDescuento = "";

                if (txtCantidad.getText().toString().equals("")) {
                    strCantidad = "0";
                } else if (txtCantidad.getText().toString().substring(0, 1)
                        .equals(".")) {
                    txtCantidad.setText("0" + txtCantidad.getText().toString());
                    txtCantidad.setSelection(txtCantidad.getText().length());
                    strCantidad = txtCantidad.getText().toString();
                } else {
                    strCantidad = txtCantidad.getText().toString();
                }

                if (isDescuentoVolumen) {

                    Pair<String, String> pairOriginal = pairDescuentos;
                    fnObtenerDescuentosVolumen();

                    if (pairDescuentos == null || pairOriginal == null || !pairOriginal.first.equals(pairDescuentos.first) || !pairOriginal.equals(pairDescuentos.second)) {
                        strDescuento = "0";
                        txtDescuento.setText("0");
                    }

                } else {
                    if (txtDescuento.getText().toString().equals("")) {
                        strDescuento = "0";
                    } else if (txtDescuento.getText().toString().substring(0, 1)
                            .equals(".")) {
                        txtDescuento.setText("0" + txtDescuento.getText().toString());
                        txtDescuento.setSelection(txtDescuento.getText().length());
                        strDescuento = txtDescuento.getText().toString();
                        if (Double.parseDouble(strDescuento) > 100.0) {
                            txtDescuento.setText("");
                            strDescuento = "0";
                        }
                    } else {
                        strDescuento = txtDescuento.getText().toString();
                        if (Double.parseDouble(strDescuento) > 100.0) {
                            txtDescuento.setText("");
                            strDescuento = "0";
                        }
                    }
                }

                double precio;
                double precioSoles;
                double precioDolares;
                double cantidad;
                double montoFrac;
                double flete;
                try {
                    precio = Double.parseDouble(strPrecio);
                    precioSoles = Double.parseDouble(strPrecioSoles);
                    precioDolares = Double.parseDouble(strPrecioDolar);
                } catch (Exception e) {
                    precio = 0;
                    precioSoles = 0;
                    precioDolares = 0;
                }
                try {
                    cantidad = Double.parseDouble(strCantidad);
                } catch (Exception e) {
                    cantidad = 0;
                }

                /**
                 * nueva configuracion
                 * */
                if (isPresentacion.equals(Configuracion.FLGVERDADERO) &&
                        iiMaximoNumeroDecimales > 0) {
                    int empaques = (int) cantidad;
                    double unidades = Double.parseDouble(Utilitario.fnRound((cantidad - empaques), iiMaximoNumeroDecimales));
                    cantidad = empaques;
                    actualizarBeanFracc(unidades);
                }

                try {
                    montoFrac = Double.parseDouble(txtPrecioFrac.getText() .toString());
                } catch (Exception e) {
                    montoFrac = 0;
                }
                try {
                    flete = Double.parseDouble(txtFlete.getText().toString());
                } catch (Exception e) {
                    flete = 0;
                }

                double montoBruto = precio * cantidad;
                double montoBrutoSoles = precioSoles * cantidad;
                double montoBrutoDolares = precioDolares * cantidad;

                //txtSubtotal.setText(Utilitario.fnRound(montoBruto, Integer.valueOf(isNumeroDecimales))); @JBELVY I
                txtSubtotal.setText(Utilitario.fnRound(montoBrutoSoles, Integer.valueOf(isNumeroDecimales))); // @JBELVY I
                txtSubtotalDolares.setText(Utilitario.fnRound(montoBrutoDolares, Integer.valueOf(isNumeroDecimales))); // @JBELVY I

                double montoDesc = (Double.parseDouble(strDescuento) / 100) * (montoBruto);
                double montoDescSoles = (Double.parseDouble(strDescuento) / 100) * (montoBrutoSoles);// @JBELVY I
                double montoDescDolar = (Double.parseDouble(strDescuento) / 100) * (montoBrutoDolares);// @JBELVY I

                double montoFinal = montoBruto - montoDesc;
                double montoFinalSoles = montoBrutoSoles - montoDescSoles;// @JBELVY I
                double montoFinalDolar = montoBrutoDolares - montoDescDolar;// @JBELVY I

                double montoFinalFracc = montoFinal + montoFrac;
                double montoFinalFraccSoles = montoFinalSoles + montoFrac;// @JBELVY I
                double montoFinalFraccDolar = montoFinal + montoFrac;// @JBELVY I

                double montoFinalFlete = (TipoMonedaEs() ? montoFinalFraccSoles: montoFinalFraccDolar) + flete;

                //txtSubtotalDesc.setText(Utilitario.fnRound(montoFinal, Integer.valueOf(isNumeroDecimales)));// @JBELVY I
                txtSubtotalDesc.setText(Utilitario.fnRound(montoFinalSoles, Integer.valueOf(isNumeroDecimales)));// @JBELVY I
                txtSubtotalDolDesc.setText(Utilitario.fnRound(montoFinalDolar, Integer.valueOf(isNumeroDecimales)));// @JBELVY I

                txtSubtotalFrac.setText(Utilitario.fnRound(TipoMonedaEs() ? montoFinalFraccSoles : montoFinalFraccDolar, Integer.valueOf(isNumeroDecimales)));
                txtTotalFlete.setText(Utilitario.fnRound(montoFinalFlete, Integer.valueOf(isNumeroDecimales)));

                subMostrarVerificar(true);

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            private double round(double d) {
                return (Math.floor(d * 100.0d + 0.5d) / 100.0d);
            }

        };

        textWatcherDescuento = new TextWatcher() {
            public void afterTextChanged(Editable s) {
                Bundle loExtras = getIntent().getExtras();
                String strPrecio = loExtras.getString("PrcProducto");
                String strPrecioSoles = loExtras.getString("PrcProducto");
                String strPrecioDolar = loExtras.getString("PrcProducto");

                if(TipoMonedaEs()) {
                    if (txtPrecio.getText().toString().equals("")) {
                        if (strPrecio.equals("")) {
                            strPrecio = ioBeanPrecio.getPrecio();
                        }
                        //@JBELVY I
                        if (strPrecioSoles.equals("")) {
                            strPrecioSoles = ioBeanPrecio.getPrecioSoles();
                        }
                    } else {
                        if (txtPrecio.getText().toString().substring(0, 1).equals(".")) {
                            txtPrecio.setText("0" + txtPrecio.getText().toString());
                            txtPrecio.setSelection(txtPrecio.getText().length());
                            strPrecio = txtPrecio.getText().toString();
                            strPrecioSoles = txtPrecio.getText().toString();
                        } else {
                            strPrecio = txtPrecio.getText().toString();
                            strPrecioSoles = txtPrecio.getText().toString();
                        }
                    }
                    strPrecioDolar = "0";
                } else{
                    if (txtPrecioDolar.getText().toString().equals("")) {
                        if (strPrecioDolar.equals("")) {
                            strPrecioDolar = ioBeanPrecio.getPrecioDolares();
                        }
                    } else {
                        if (txtPrecioDolar.getText().toString().substring(0, 1).equals(".")) {
                            txtPrecioDolar.setText("0" + txtPrecioDolar.getText().toString());
                            txtPrecioDolar.setSelection(txtPrecioDolar.getText().length());
                            strPrecioDolar = txtPrecioDolar.getText().toString();
                        } else {
                            strPrecioDolar = txtPrecioDolar.getText().toString();
                        }
                    }
                    strPrecio = "0";
                    strPrecioSoles = "0";
                }

                String strCantidad = "";
                String strDescuento = "";

                if (txtCantidad.getText().toString().equals("")) {
                    strCantidad = "0";
                } else if (txtCantidad.getText().toString().substring(0, 1).equals(".")) {
                    txtCantidad.setText("0" + txtCantidad.getText().toString());
                    txtCantidad.setSelection(txtCantidad.getText().length());
                    strCantidad = txtCantidad.getText().toString();
                } else {
                    strCantidad = txtCantidad.getText().toString();
                }

                if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                    strCantidad = obtenerEmpaques(strCantidad);
                }

                if (isDescPorc.equals(Configuracion.FLGVERDADERO)
                        && isDescMonto.equals(Configuracion.FLGFALSO)) {
                    // DESCUENTO POR PORCENTAJE

                    if (txtDescuento.getText().toString().equals("")) {
                        strDescuento = "0";
                    } else if (txtDescuento.getText().toString()
                            .substring(0, 1).equals(".")) {
                        txtDescuento.setText("0" + txtDescuento.getText().toString());
                        txtDescuento.setSelection(txtDescuento.getText()
                                .length());
                        strDescuento = txtDescuento.getText().toString();

                        if (isDescuentoVolumen) {
                            fnObtenerDescuentosVolumen();

                            if (pairDescuentos == null) {
                                strDescuento = "0";
                            } else {
                                if (Double.parseDouble(pairDescuentos.first) > Double
                                        .parseDouble(txtDescuento.getText().toString())) {
                                    strDescuento = "0";
                                }

                                if (Double.parseDouble(pairDescuentos.second) < Double
                                        .parseDouble(txtDescuento.getText().toString())) {
                                    strDescuento = "0";
                                }
                            }
                        } else {
                            if (Double.parseDouble(ioBeanPrecio.getDescuentoMinimo()) > Double
                                    .parseDouble(txtDescuento.getText().toString())) {
                                strDescuento = "0";
                            }

                            if (Double.parseDouble(ioBeanPrecio.getDescuentoMaximo()) < Double
                                    .parseDouble(txtDescuento.getText().toString())) {
                                strDescuento = "0";
                            }
                        }
                    } else {
                        strDescuento = txtDescuento.getText().toString();

                        if (isDescuentoVolumen) {
                            fnObtenerDescuentosVolumen();

                            if (pairDescuentos == null) {
                                strDescuento = "0";

                            } else {
                                if (Double.parseDouble(pairDescuentos.first) > Double
                                        .parseDouble(txtDescuento.getText().toString())) {
                                    strDescuento = "0";
                                }

                                if (Double.parseDouble(pairDescuentos.second) < Double
                                        .parseDouble(txtDescuento.getText().toString())) {
                                    strDescuento = "0";
                                }
                            }
                        } else {
                            if (Double.parseDouble(ioBeanPrecio.getDescuentoMinimo()) > Double
                                    .parseDouble(txtDescuento.getText().toString())) {
                                strDescuento = "0";
                            }

                            if (Double.parseDouble(ioBeanPrecio.getDescuentoMaximo()) < Double
                                    .parseDouble(txtDescuento.getText().toString())) {
                                strDescuento = "0";
                            }
                        }
                    }

                    double precio = Double.parseDouble(strPrecio);
                    double precioSoles = Double.parseDouble(strPrecioSoles); //@JBELVY
                    double precioDolar = Double.parseDouble(strPrecioDolar); //@JBELVY
                    double cantidad = Double.parseDouble(strCantidad);
                    double montoBruto = precio * cantidad;
                    double montoBrutoSoles = precioSoles * cantidad; //@JBELVY
                    double montoBrutoDolar = precioDolar * cantidad; //@JBELVY
                    double montoDesc = (Double.parseDouble(strDescuento) / 100)  * (montoBruto);
                    double montoDescSoles = (Double.parseDouble(strDescuento) / 100)  * (montoBrutoSoles);//@JBELVY
                    double montoDescDolar = (Double.parseDouble(strDescuento) / 100)  * (montoBrutoDolar);//@JBELVY
                    double montoFinal = montoBruto - montoDesc;
                    double montoFinalSoles = montoBrutoSoles - montoDescSoles;//@JBELVY
                    double montoFinalDolar = montoBrutoDolar - montoDescDolar;//@JBELVY
                    montoFin = montoFinal;
                    montoFinSoles = montoFinalSoles;//@JBELVY
                    montoFinDolar = montoFinalDolar;//@JBELVY

                } else if (isDescPorc
                        .equals(Configuracion.FLGFALSO)
                        && isDescMonto
                        .equals(Configuracion.FLGVERDADERO)) {

                    // DESCUENTO POR MONTO
                    if (txtDescuento.getText().toString().equals("")) {
                        strDescuento = "0";
                    } else if (txtDescuento.getText().toString().substring(0, 1).equals(".")) {
                        txtDescuento.setText("0" + txtDescuento.getText().toString());
                        txtDescuento.setSelection(txtDescuento.getText().length());
                        strDescuento = txtDescuento.getText().toString();

                        double precioTmp1 = Double.parseDouble(strPrecio);
                        double precioTmp1Dolar = Double.parseDouble(strPrecioSoles);//@JBELVY
                        double precioTmp1Soles = Double.parseDouble(strPrecioDolar);//@JBELVY
                        double cantidadTmp1 = Double.parseDouble(strCantidad);
                        double montoBrutoTmp1 = precioTmp1 * cantidadTmp1;
                        double montoBrutoTmp1Soles = precioTmp1Soles * cantidadTmp1;//@JBELVY
                        double montoBrutoTmp1Dolar = precioTmp1Dolar * cantidadTmp1;//@JBELVY

                        double strDescuentoTmp1 = (Double.parseDouble(strDescuento) * 100) / (montoBrutoTmp1);
                        double strDescuentoTmp1Soles = (Double.parseDouble(strDescuento) * 100) / (montoBrutoTmp1Soles);//@JBELVY
                        double strDescuentoTmp1Dolar = (Double.parseDouble(strDescuento) * 100) / (montoBrutoTmp1Dolar);//@JBELVY


                        if (isDescuentoVolumen) {
                            fnObtenerDescuentosVolumen();

                            if (pairDescuentos == null) {
                                strDescuento = "0";
                            } else {
                                if (Double.parseDouble(pairDescuentos.first) > strDescuentoTmp1Soles) { //@JBELVY Before strDescuentoTmp1
                                    strDescuento = "0";
                                }

                                if (Double.parseDouble(pairDescuentos.second) < strDescuentoTmp1Soles) { //@JBELVY Before strDescuentoTmp1
                                    strDescuento = "0";
                                }
                            }
                        } else {
                            if (Double.parseDouble(ioBeanPrecio.getDescuentoMinimo()) > strDescuentoTmp1Soles) { //@JBELVY Before strDescuentoTmp1
                                strDescuento = "0";
                            }

                            if (Double.parseDouble(ioBeanPrecio.getDescuentoMaximo()) < strDescuentoTmp1Soles) { //@JBELVY Before strDescuentoTmp1
                                strDescuento = "0";
                            }
                        }
                    } else {
                        strDescuento = txtDescuento.getText().toString();

                        double precioTmp2 = Double.parseDouble(strPrecio);
                        double precioTmp2Soles = Double.parseDouble(strPrecioSoles);//@JBELVY
                        double precioTmp2Dolar = Double.parseDouble(strPrecioDolar);//@JBELVY
                        double cantidadTmp2 = Double.parseDouble(strCantidad);
                        double montoBrutoTmp2 = precioTmp2 * cantidadTmp2;
                        double montoBrutoTmp2Soles = precioTmp2Soles * cantidadTmp2;//@JBELVY
                        double montoBrutoTmp2Dolar = precioTmp2Dolar * cantidadTmp2;//@JBELVY

                        double strDescuentoTmp2 = (Double .parseDouble(strDescuento) * 100) / (montoBrutoTmp2);
                        double strDescuentoTmp2Soles = (Double .parseDouble(strDescuento) * 100) / (montoBrutoTmp2Soles);
                        double strDescuentoTmp2Dolares = (Double .parseDouble(strDescuento) * 100) / (montoBrutoTmp2Dolar);

                        if (isDescuentoVolumen) {
                            fnObtenerDescuentosVolumen();

                            if (pairDescuentos == null) {
                                strDescuento = "0";
                            } else {
                                if (Double.parseDouble(pairDescuentos.first) > strDescuentoTmp2Soles) { //@JBELVY Before strDescuentoTmp2
                                    strDescuento = "0";
                                }

                                if (Double.parseDouble(pairDescuentos.second) < strDescuentoTmp2Soles) { //@JBELVY Before strDescuentoTmp2
                                    strDescuento = "0";
                                }
                            }
                        } else {
                            if (Double.parseDouble(ioBeanPrecio.getDescuentoMinimo()) > strDescuentoTmp2Soles) {//@JBELVY Before strDescuentoTmp2
                                strDescuento = "0";
                            }

                            if (Double.parseDouble(ioBeanPrecio.getDescuentoMaximo()) < strDescuentoTmp2Soles) {//@JBELVY Before strDescuentoTmp2
                                strDescuento = "0";
                            }
                        }
                    }

                    double precio = Double.parseDouble(strPrecio);
                    double precioSoles = Double.parseDouble(strPrecioSoles);//@JBELVY
                    double precioDolar = Double.parseDouble(strPrecioDolar);//@JBELVY
                    double cantidad = Double.parseDouble(strCantidad);
                    double montoBruto = precio * cantidad;
                    double montoBrutoSoles = precioSoles * cantidad;//@JBELVY
                    double montoBrutoDolar = precioDolar * cantidad;//@JBELVY
                    double montoDesc = Double.parseDouble(strDescuento);
                    double montoFinal = montoBruto - montoDesc;
                    double montoFinalSoles = montoBrutoSoles - montoDesc;//@JBELVY
                    double montoFinalDolar = montoBrutoDolar - montoDesc;//@JBELVY
                    montoFin = montoFinal;
                    montoFinSoles = montoFinalSoles;
                    montoFinDolar = montoFinalDolar;

                }
                double montoFrac;
                double flete;
                try {
                    montoFrac = Double.parseDouble(txtPrecioFrac.getText().toString());
                } catch (Exception e) {
                    montoFrac = 0;
                }
                try {
                    flete = Double.parseDouble(txtFlete.getText().toString());
                } catch (Exception e) {
                    flete = 0;
                }
                double montoFinalFracc = montoFin + montoFrac;
                double montoFinalFraccSoles = montoFinSoles + montoFrac;//@JBELVY
                double montoFinalFraccDolar = montoFinDolar + montoFrac;//@JBELVY

                double montoFinalFlete = (TipoMonedaEs() ? montoFinalFraccSoles : montoFinalFraccDolar) + flete;

                //txtSubtotalDesc.setText(Utilitario.fnRound(montoFin,  Integer.valueOf(isNumeroDecimales)));//@JBELVY
                txtSubtotalDesc.setText(Utilitario.fnRound(montoFinSoles,  Integer.valueOf(isNumeroDecimales)));//@JBELVY
                txtSubtotalDolDesc.setText(Utilitario.fnRound(montoFinDolar,  Integer.valueOf(isNumeroDecimales)));//@JBELVY

                double subTotalFrac = (TipoMonedaEs() ? montoFinalFraccSoles : montoFinalFraccDolar);
                //txtSubtotalFrac.setText(Utilitario.fnRound(montoFinalFracc, Integer.valueOf(isNumeroDecimales)));//@JBELVY
                txtSubtotalFrac.setText(Utilitario.fnRound(subTotalFrac, Integer.valueOf(isNumeroDecimales)));//@JBELVY

                txtTotalFlete.setText(Utilitario.fnRound(montoFinalFlete, Integer.valueOf(isNumeroDecimales)));

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }

            private double round(double d) {
                return (Math.floor(d * 100.0d + 0.5d) / 100.0d);
            }
        };

        textWatcherFrac = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void afterTextChanged(Editable s) {
                double valorTotal;
                double valorTotalFrac;
                double montoFrac;
                long cantidadFrac;
                double flete;
                double valorTotalFlete;
                try {
                    montoFrac = Double.parseDouble(txtPrecioFrac.getText()
                            .toString());
                    cantidadFrac = (ioBeanFraccionamiento == null ? Long.valueOf(0) : ioBeanFraccionamiento.getNumerador());//@COMENTAR
                } catch (Exception e) {
                    montoFrac = 0;
                    cantidadFrac = 0;
                }
                try {
                    flete = Double.parseDouble(txtFlete.getText().toString());
                } catch (Exception e) {
                    flete = 0;
                }
                try {
                    valorTotal = Double.parseDouble(txtSubtotalDesc.getText().toString());
                } catch (Exception e) {
                    valorTotal = 0;
                }

                valorTotalFrac = valorTotal + montoFrac;
                valorTotalFlete = valorTotalFrac + flete;

                txtSubtotalFrac.setText(Utilitario.fnRound(valorTotalFrac,
                        Integer.valueOf(isNumeroDecimales)));
                txtTotalFlete.setText(Utilitario.fnRound(valorTotalFlete,
                        Integer.valueOf(isNumeroDecimales)));

            }
        };

        textWatcherFlete = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void afterTextChanged(Editable s) {
                double valorTotal;
                double valorTotalFlete;
                double flete;
                try {
                    flete = Double.parseDouble(txtFlete.getText().toString());
                } catch (Exception e) {
                    flete = 0;
                }
                try {
                    valorTotal = Double.parseDouble(txtSubtotalFrac.getText().toString());
                } catch (Exception e) {
                    valorTotal = 0;
                }
                valorTotalFlete = valorTotal + flete;

                txtTotalFlete.setText(Utilitario.fnRound(valorTotalFlete,
                        Integer.valueOf(isNumeroDecimales)));

            }
        };

        txtPrecio.addTextChangedListener(textWatcherPrecio);
        txtPrecioDolar.addTextChangedListener(textWatcherPrecio);

        txtCantidad.addTextChangedListener(new DecimalTextWatcher(CANTIDAD_ENTEROS, CANTIDAD_DECIMALES));
        txtCantidad.addTextChangedListener(textWatcherCantidad);
        txtBonificacion.addTextChangedListener(new DecimalTextWatcher(CANTIDAD_ENTEROS, CANTIDAD_DECIMALES));
        txtFraccionamiento.addTextChangedListener(textWatcherFraccUno);
        txtDescuento.addTextChangedListener(textWatcherDescuento);
        txtFlete.addTextChangedListener(textWatcherFlete);
        txtPrecioFrac.addTextChangedListener(textWatcherFrac);

        String valorDolar = txtPrecioDolar.getText().toString();
        String valorSoles = txtPrecio.getText().toString();

        if(valorSoles.equals("0.0") || valorSoles.equals("0.00") || valorSoles.equals("")){
            spTipoMoneda.setSelection(1);
            lblPrecio.setText("Precio" + "("
                    + isSimboloMonedaDolares + "):");
            txtPrecio.setVisibility(View.GONE);
            txtPrecioDolar.setVisibility(View.VISIBLE);
        }
        if(valorDolar.equals("0.0") || valorDolar.equals("0.00") || valorDolar.equals("")){
            spTipoMoneda.setSelection(0);
            lblPrecio.setText("Precio" + "("
                    + isSimboloMoneda + "):");
            txtPrecio.setVisibility(View.VISIBLE);
            txtPrecioDolar.setVisibility(View.GONE);
        }
    }

    private void actualizarBeanFracc(double pdFraccion) {
        ioBeanFraccionamiento = ioBeanPresentacion.fnObtenerBeanFraccionamiento(pdFraccion, Integer.valueOf(isNumeroDecimales));
        validarFleteYFracc();
        subMostrarVerificar(true);
    }

    private String obtenerEmpaques(String psCantidad) {
        double cantidad;
        try {
            cantidad = Double.parseDouble(psCantidad);
        } catch (Exception e) {
            e.printStackTrace();
            cantidad = 0;
        }
        return String.valueOf((int) cantidad);
    }

    private void setInputTypeEditText(EditText editText) {
        if (iiMaximoNumeroDecimales <= 0) {
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else {
            editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        }
    }

    private void setMaximoNumeroDecimales() {
        iiMaximoNumeroDecimales = 0;
        try {
            iiMaximoNumeroDecimales = Integer.parseInt(isMaximoNumeroDecimales);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("ActProductoDetalle", "Error conversion maximo numero de decimales: " + e.getMessage());
        }
        if (iiMaximoNumeroDecimales <= 0) {
            CANTIDAD_DECIMALES = iiMaximoNumeroDecimales;
            CANTIDAD_ENTEROS = Configuracion.CANTIDAD_DIGITOS;
        } else {
            CANTIDAD_DECIMALES = iiMaximoNumeroDecimales;
            CANTIDAD_ENTEROS = (Configuracion.CANTIDAD_DIGITOS - iiMaximoNumeroDecimales) - 1;
        }
    }

    private void subConexionHttp() {
        if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO) && existeProductoAlmacen)
            ioBeanArticulo.setCodAlmacen(String.valueOf(ioBeanAlmacen.getPk()));
        else
            ioBeanArticulo.setCodAlmacen("0");

        BeanArticuloOnline loBeanArticuloOnline = ioBeanArticulo.fnObtenerBeanArticuloOnline();
        loBeanArticuloOnline.setTipoBusqueda(Configuracion.TipoBusqueda.PRESENTACION);
        new HttpProductoOnline(loBeanArticuloOnline, this, this.fnTipactividad(),
                getString(R.string.actproductodetalle_actualizarstock)).execute();
    }

    private void actualizarStock(String isIdArticulo) {

        DalProducto loDalProducto = new DalProducto(this);
        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            ioBeanArticulo = loDalProducto.fnSelxIdPresentacion(isIdArticulo);
            ioBeanPresentacion = (BeanPresentacion) ioBeanArticulo;
        } else
            ioBeanArticulo = loDalProducto.fnSelxIdProducto(isIdArticulo);

        DalAlmacen dalAlmacen = new DalAlmacen(this);
        if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO) && existeProductoAlmacen) {

            long llCountAlmacen = 0;

            if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                llCountAlmacen = dalAlmacen.fnCountXCodPresentacion(ioBeanArticulo.getId());
                ioBeanAlmacen = dalAlmacen.fnSelectAlmacenPresentacion(ioBeanArticulo.getId(), String.valueOf(ioBeanAlmacen.getPk()));
            } else {
                llCountAlmacen = dalAlmacen.fnCountXCodProducto(ioBeanArticulo.getId());
                ioBeanAlmacen = dalAlmacen.fnSelectAlmacenProducto(ioBeanArticulo.getId(), String.valueOf(ioBeanAlmacen.getPk()));
            }

            if (llCountAlmacen > 1) {
                cmbAlmacen.setBackgroundResource(R.drawable.spinner_bg);
                cmbAlmacen.setGravity(Gravity.CENTER);
            } else {
                cmbAlmacen.setGravity(Gravity.RIGHT);
                cmbAlmacen.setBackgroundResource(android.R.color.transparent);
            }

        } else {

            ioRowFlete.setVisibility(View.GONE);
            ioRowAlmacen.setVisibility(View.GONE);
        }

    }
    @SuppressWarnings("unused")
    private void subCargaDetalle() {

        Bundle loExtras = getIntent().getExtras();
        String isIdArticulo = loExtras.getString("IdArticulo");
        String isListaPrecio = loExtras.getString("idListaPrecio");

        DalProducto loDalProducto = new DalProducto(this);
        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            ioBeanArticulo = loDalProducto.fnSelxIdPresentacion(isIdArticulo);
            ioBeanPresentacion = (BeanPresentacion) ioBeanArticulo;
        } else
            ioBeanArticulo = loDalProducto.fnSelxIdProducto(isIdArticulo);

        /*VALIDANDO DESCUENTO GENERAL O POR PRODUCTO | INICIO*/

        ioBeanPrecio = new BeanPrecio();
        if (isHabDescProd.equals(Configuracion.FLGVERDADERO) &&
                isHabDescGen.equals(Configuracion.FLGFALSO)) {

            ioBeanPrecio = TextUtils.isEmpty(isListaPrecio) ?
                    ioBeanArticulo.fnBeanPrecio() :
                    isPresentacion.equals(Configuracion.FLGVERDADERO) ?
                            loDalProducto.fnSelPrecioPresentacionXListaPrecioXCodigo(isListaPrecio)
                            : loDalProducto.fnSelPrecioXListaPrecioXCodigo(isListaPrecio);

        } else if (isHabDescProd.equals(Configuracion.FLGFALSO) &&
                isHabDescGen.equals(Configuracion.FLGVERDADERO)) {

            ioBeanPrecio.setPrecio(TextUtils.isEmpty(isListaPrecio) ? ioBeanArticulo.getPrecioBase() :
                    isPresentacion.equals(Configuracion.FLGVERDADERO) ?
                            loDalProducto.fnSelPrecioPresentacionXListaPrecioXCodigo(isListaPrecio).getPrecio()
                            : loDalProducto.fnSelPrecioXListaPrecioXCodigo(isListaPrecio).getPrecio());

            ioBeanPrecio.setPrecioSoles(TextUtils.isEmpty(isListaPrecio) ? ioBeanArticulo.getPrecioBaseSoles() :
                    isPresentacion.equals(Configuracion.FLGVERDADERO) ?
                            loDalProducto.fnSelPrecioPresentacionXListaPrecioXCodigo(isListaPrecio).getPrecioSoles()
                            : loDalProducto.fnSelPrecioXListaPrecioXCodigo(isListaPrecio).getPrecioSoles()); //@JBELVY

            ioBeanPrecio.setPrecioDolares(TextUtils.isEmpty(isListaPrecio) ? ioBeanArticulo.getPrecioBaseDolares() :
                    isPresentacion.equals(Configuracion.FLGVERDADERO) ?
                            loDalProducto.fnSelPrecioPresentacionXListaPrecioXCodigo(isListaPrecio).getPrecioDolares()
                            : loDalProducto.fnSelPrecioXListaPrecioXCodigo(isListaPrecio).getPrecioDolares());//@JBELVY

            ioBeanPrecio.setDescuentoMaximo(isDescuentoMaximo);
            ioBeanPrecio.setDescuentoMinimo(isDescuentoMinimo);

        } else {
            if (TextUtils.isEmpty(isListaPrecio)) {
                ioBeanPrecio = ioBeanArticulo.fnBeanPrecio();
            } else if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                if (!TextUtils.isEmpty(isListaPrecio)) {
                    ioBeanPrecio = loDalProducto.fnSelPrecioPresentacionXListaPrecioXCodigo(isListaPrecio);
                } else {
                    ioBeanPrecio = loDalProducto.fnSelPrecioPresentacionXPresentacionXCodigo(isIdArticulo);
                }
            } else {
                ioBeanPrecio = ioBeanArticulo.fnBeanPrecio();
            }
        }

        String stock = "";
        DalAlmacen dalAlmacen = new DalAlmacen(this);

        if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO) && existeProductoAlmacen) {

            String lsBeanPedidoCab = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE)
                    .getString(sharePreferenceBeanPedidoCab, "");
            BeanEnvPedidoCab loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper
                    .fromJson(lsBeanPedidoCab, BeanEnvPedidoCab.class);

            long llCountAlmacen = 0;

            if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                llCountAlmacen = dalAlmacen.fnCountXCodPresentacion(ioBeanArticulo.getId());
                ioBeanAlmacen = dalAlmacen.fnSelectAlmacenPresentacion(ioBeanArticulo.getId(), loBeanPedidoCab.getCodAlmacen());
            } else {
                llCountAlmacen = dalAlmacen.fnCountXCodProducto(ioBeanArticulo.getId());
                ioBeanAlmacen = dalAlmacen.fnSelectAlmacenProducto(ioBeanArticulo.getId(), loBeanPedidoCab.getCodAlmacen());
            }

            cmbAlmacen.setText(ioBeanAlmacen.getNombre());
            stock = Utilitario.fnRound(ioBeanAlmacen.getStock(), iiMaximoNumeroDecimales);
            isCodAlmacenArticulo = loBeanPedidoCab.getCodAlmacen();

            if (llCountAlmacen > 1) {
                cmbAlmacen.setBackgroundResource(R.drawable.spinner_bg);
                cmbAlmacen.setGravity(Gravity.CENTER);
            } else {
                cmbAlmacen.setGravity(Gravity.RIGHT);
                cmbAlmacen.setBackgroundResource(android.R.color.transparent);
            }

        } else {

            ioRowFlete.setVisibility(View.GONE);
            ioRowAlmacen.setVisibility(View.GONE);
            stock = Utilitario.fnRound(ioBeanArticulo.getStock(), iiMaximoNumeroDecimales);
        }

        if (TextUtils.isEmpty(stock)) {
            stock = "0";
        }

        txtStock.setText(fnObtenerStock(Double.parseDouble(stock)));
        btnStock.setText("(" + fnObtenerStock(Double.parseDouble(stock)) + ") :: " + getString(
                R.string.actproductodetalle_btnactualizar));

        /*VALIDANDO DESCUENTO GENERAL O POR PRODUCTO | FIN*/
        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {

            BeanPresentacion loPresentacion = (BeanPresentacion) ioBeanArticulo;
            txtCodigo.setText(loPresentacion.getProducto().getCodigo());
            txtNombre.setText(loPresentacion.getProducto().getNombre());
            txtPresentacion.setText(loPresentacion.fnMostrarPresentacion());
            loPresentacion.fnObtenerLstFraccionamiento(Integer.valueOf(isNumeroDecimales));
            if(loPresentacion.getLstBeanFraccionamiento().size() > 0) {
                ioBeanFraccionamiento = loPresentacion.getLstBeanFraccionamiento().get(0);
                cmbFraccionamiento.setText(ioBeanFraccionamiento.toString());
            } else {
                cmbFraccionamiento.setText("Sin Fraccionamiento");
            }

            if (loPresentacion.getLstBeanFraccionamiento().size() > 1) {
                cmbFraccionamiento.setGravity(Gravity.CENTER);
                cmbFraccionamiento.setBackgroundResource(R.drawable.spinner_bg);
            } else {
                cmbFraccionamiento.setGravity(Gravity.RIGHT);
                cmbFraccionamiento.setBackgroundResource(android.R.color.transparent);
            }

        } else {
            txtCodigo.setText(ioBeanArticulo.getCodigo());
            txtNombre.setText(ioBeanArticulo.getNombre());
        }

        lblPrecio.setText(lblPrecio.getText().toString() + "("
                + isSimboloMoneda + "):");
        lblPrecioFrac.setText(lblPrecioFrac.getText().toString() + "("
                + isSimboloMoneda + "):");
        lblFlete.setText(lblFlete.getText().toString() + "("
                + isSimboloMoneda + "):");

        validarFleteYFracc();
        txtCantidad.requestFocus();
    }

    private boolean TipoMonedaEs(){
        boolean result;
        String tipoMoneda = spTipoMoneda.getSelectedItem().toString().toUpperCase();

        if(tipoMoneda.equals("SOLES")){
            result = true;
        }else{
            result = false;
        }
        return  result;
    }

    private String fnObtenerStock(double pdStock) {
        String lsStock = "";

        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            BeanPresentacion loBeanPresentacion = (BeanPresentacion) ioBeanArticulo;
            pdStock = Double.parseDouble(Utilitario.fnRound(pdStock, 0));
            long llstock = (long) pdStock / loBeanPresentacion.getCantidad();
            long llUnidades = (long) pdStock % loBeanPresentacion.getCantidad();
            lsStock = Utilitario.fnRound(llstock, 0) + " - " + Utilitario.fnRound(llUnidades, 0) +
                    " " + loBeanPresentacion.getProducto().getUnidadDefecto();
        } else {
            lsStock = Utilitario.fnRound(pdStock, iiMaximoNumeroDecimales);
        }

        return lsStock;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {

        if (estadoSincronizar) {
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActMenu.class);

                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                        | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }
        } else if (ibHttpConsultarProducto) {
            ibHttpConsultarProducto = false;
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK
                    || getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {


                DalAlmacen loDalAlmacen = new DalAlmacen(this);

                BeanConsultaProducto loBeanConsultaProducto = ((AplicacionEntel) getApplication())
                        .getCurrentConsultaProducto();

                if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO) && existeProductoAlmacen) {
                    BeanAlmacen loBeanAlmacen = null;

                    if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                        loBeanAlmacen = loDalAlmacen.fnSelectXCodPresentacionXCodAlmacen(ioBeanArticulo.getId(), String.valueOf(loBeanConsultaProducto.getCodigoAlmacen()));
                        if (loBeanAlmacen != null)
                            loDalAlmacen.fnUpdPreStock(String.valueOf(loBeanAlmacen.getPk()),
                                    ioBeanArticulo.getId(), loBeanConsultaProducto.getStock());
                    } else {
                        loBeanAlmacen = loDalAlmacen.fnSelectXCodProductoXCodAlmacen(ioBeanArticulo.getId(), String.valueOf(loBeanConsultaProducto.getCodigoAlmacen()));
                        if (loBeanAlmacen != null)
                            loDalAlmacen.fnUpdStock(String.valueOf(loBeanAlmacen.getPk()),
                                    ioBeanArticulo.getId(), loBeanConsultaProducto.getStock());
                    }

                    if (loBeanAlmacen != null) {
                        ioBeanAlmacen = loBeanAlmacen;
                        ioBeanAlmacen.setStock(loBeanConsultaProducto.getStock());
                    }

                    cmbAlmacen.setText(ioBeanAlmacen.getNombre());


                } else {
                    DalProducto loDalProducto = new DalProducto(this);
                    ioBeanArticulo.setStock(loBeanConsultaProducto.getStock());
                    if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                        loDalProducto.fnUpdStockPresentacion(ioBeanArticulo.getId(), ioBeanArticulo.getStock());
                    } else {
                        loDalProducto.fnUpdStockProducto(ioBeanArticulo.getId(), ioBeanArticulo.getStock());
                    }
                }

                txtStock.setText(fnObtenerStock(Double.parseDouble(loBeanConsultaProducto.getStock())));
                btnStock.setText("(" + fnObtenerStock(Double.parseDouble(loBeanConsultaProducto.getStock())) + ") :: " + getString(
                        R.string.actproductodetalle_btnactualizar));


                if (!TextUtils.isEmpty(loBeanConsultaProducto.getDescripcion()))
                    txtObservacion.setText(loBeanConsultaProducto.getDescripcion());

                if (!TextUtils.isEmpty(loBeanConsultaProducto.getDescuento()))
                    txtDescuento.setText(loBeanConsultaProducto.getDescuento());

                if (!TextUtils.isEmpty(loBeanConsultaProducto.getFechaUltimaVenta())) {
                    ioTxtFechaUltVenta.setText(loBeanConsultaProducto.getFechaUltimaVenta());
                    ioRowFechaUltVenta.setVisibility(View.VISIBLE);
                } else {
                    ioRowFechaUltVenta.setVisibility(View.GONE);
                }
                if (!TextUtils.isEmpty(loBeanConsultaProducto.getFlete())) {
                    txtFlete.setText(loBeanConsultaProducto.getFlete());
                }
                //if (!TextUtils.isEmpty(loBeanConsultaProducto.getPrecio())) {
                //    txtPrecio.setText(loBeanConsultaProducto.getPrecio());
                //    ioBeanPrecio.setPrecio(loBeanConsultaProducto.getPrecio());
                //} @JBELVY I
                if (!TextUtils.isEmpty(loBeanConsultaProducto.getPrecioSoles())) {
                    txtPrecio.setText(loBeanConsultaProducto.getPrecioSoles());
                    ioBeanPrecio.setPrecioSoles(loBeanConsultaProducto.getPrecioSoles());
                }
                if (!TextUtils.isEmpty(loBeanConsultaProducto.getPrecioDolares())) {
                    txtPrecioDolar.setText(loBeanConsultaProducto.getPrecioDolares());
                    ioBeanPrecio.setPrecioDolares(loBeanConsultaProducto.getPrecioDolares());
                }
                // @JBELVY F
                if (!TextUtils.isEmpty(loBeanConsultaProducto.getPrecioUltimaVenta())) {
                    ioRowUltPrecio.setVisibility(View.VISIBLE);
                    ioTxtUltPrecio.setText(loBeanConsultaProducto.getPrecioUltimaVenta());
                } else {
                    ioRowUltPrecio.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(loBeanConsultaProducto.getTipoDescuento())) {
                    if (loBeanConsultaProducto.getTipoDescuento().equals("M")) {
                        isDescMonto = "T";
                        isDescPorc = "F";
                    } else {
                        isDescMonto = "F";
                        isDescPorc = "T";
                    }
                }


                validarFleteYFracc();

                subMostrarVerificar(false);


            } else if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDORALGUNERROR) {
                subMostrarVerificar(false);
            }

        } else if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK
                || getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROKNOMSG) {

            List<BeanArticulo> listaConStock = ((AplicacionEntel) getApplication())
                    .getCurrentlistaArticulo();
            double stock = 0;
            if (listaConStock != null && listaConStock.size() > 0) {
                stock = Double.valueOf(listaConStock.get(0).getStock());
            }

            if (stock <= 0) {
                new NexToast(this, getString(
                        R.string.actproductodetalle_stocknegativo), EnumType.INFORMATION)
                        .show();
            }


            if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO) && existeProductoAlmacen) {
                ioBeanAlmacen.setStock(String.valueOf(stock));
                DalAlmacen loDalAlmacen = new DalAlmacen(this);
                if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                    loDalAlmacen.fnUpdPreStock(String.valueOf(ioBeanAlmacen.getPk()),
                            ioBeanArticulo.getId(), String.valueOf(stock));
                } else {
                    loDalAlmacen.fnUpdStock(String.valueOf(ioBeanAlmacen.getPk()),
                            ioBeanArticulo.getId(), String.valueOf(stock));
                }
            } else {
                DalProducto loDalProducto = new DalProducto(this);
                if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
                    loDalProducto.fnUpdStockPresentacion(ioBeanArticulo.getId(), String.valueOf(stock));
                } else {
                    loDalProducto.fnUpdStockProducto(ioBeanArticulo.getId(), String.valueOf(stock));
                }


            }

            actualizarStock(ioBeanArticulo.getId());
            btnStock.setText("(" + fnObtenerStock(stock)
                    + ") :: " + getString(R.string.actproductodetalle_btnactualizado));

        }

    }

    private void fnObtenerDescuentosVolumen() {
        String strCantidad = txtCantidad.getText().toString().trim();
        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            strCantidad = obtenerEmpaques(strCantidad);
        }

        Bundle loExtras = getIntent().getExtras();
        String isIdArticulo = loExtras.getString("IdArticulo");
        String isListaPrecio = loExtras.getString("idListaPrecio");

        DalProducto dalProducto = new DalProducto(this);
        pairDescuentos = dalProducto.fnSelDescuentosVolumen(isPresentacion, isIdArticulo, isListaPrecio, strCantidad);
    }

    public boolean finalizar(String tipo) {
        boolean flgPaso = true;
        if(pairTipoCambioSoles == null && pairTipoCambioDolar == null ) {
            Toast toast1 = Toast.makeText(getApplicationContext(), "No existe tipo de cambio vigente.", Toast.LENGTH_SHORT);
            toast1.show();
            return false;
        } else if (pairTipoCambioSoles == null){
            Toast toast1 = Toast.makeText(getApplicationContext(),"No existe tipo de cambio Soles vigente.", Toast.LENGTH_SHORT);
            toast1.show();
            return false;
        } else if (pairTipoCambioDolar == null){
            Toast toast1 = Toast.makeText(getApplicationContext(),"No existe tipo de cambio Dólares vigente.", Toast.LENGTH_SHORT);
            toast1.show();
            return false;
        }

        if(TipoMonedaEs()){
            if(Double.parseDouble(txtPrecio.getText().toString()) <= 0){
                Toast tstMsgPreSol = Toast.makeText(getApplicationContext(),
                        "No se puede agregar al detalle un producto con Precio de valor " + isSimboloMoneda + " 0",
                        Toast.LENGTH_LONG);
                tstMsgPreSol.show();
                return false;
            }
        }else{
            if(Double.parseDouble(txtPrecioDolar.getText().toString()) <= 0){
                Toast tstMsgPreDol = Toast.makeText(getApplicationContext(),
                        "No se puede agregar al detalle un producto con Precio de valor " + isSimboloMonedaDolares + " 0",
                        Toast.LENGTH_LONG);
                tstMsgPreDol.show();
                return false;
            }
        }

        boolean mostrarSuperaMaximoItems = false;
        boolean mostrarSuperoMaximomonto = false;
        String strCantidad = txtCantidad.getText().toString().trim();

        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            strCantidad = obtenerEmpaques(strCantidad);
        }

        double ldStock = Double.parseDouble(isMostrarAlmacen.equals(Configuracion.FLGVERDADERO) && existeProductoAlmacen
                ? ioBeanAlmacen.getStock() : ioBeanArticulo.getStock());

        double ldCantidad = 0;
        double ldBonificacion = 0;
        long ldCantidadFrac = 0;
        long ldCantidadPre = 0;

        try {
            ldCantidad = Double.valueOf(strCantidad);
        } catch (Exception e) {
            ldCantidad = 0;
        }

        try {
            ldBonificacion = Double.parseDouble(txtBonificacion.getText().toString().trim());
        } catch (Exception e) {
            ldBonificacion = 0;
        }

        double ldBonificacionFracc = 0;
        if (isPresentacion.equals(Configuracion.FLGVERDADERO) &&
                iiMaximoNumeroDecimales > 0) {
            int liBonificacion = (int) ldBonificacion;
            ldBonificacionFracc = Double.parseDouble(Utilitario.fnRound((ldBonificacion - liBonificacion), iiMaximoNumeroDecimales));
            ldBonificacion = liBonificacion;
        }

        try {
            if (ioRowFraccionamiento.getVisibility() == View.GONE) {
                ldCantidadFrac = Long.valueOf(txtFraccionamiento.getText().toString());
            } else {
                ldCantidadFrac = (ioBeanFraccionamiento == null ? Long.valueOf(0) : ioBeanFraccionamiento.getNumerador()); //@COMENTAR
            }
        } catch (Exception e) {
            ldCantidadFrac = 0;
        }
        try {
            ldCantidadPre = ((BeanPresentacion) ioBeanArticulo).getCantidad();
        } catch (Exception e) {
            ldCantidadPre = 0;
        }

        String strDescuento = txtDescuento.getText().toString().trim();

        if (TextUtils.isEmpty(strDescuento)) {
            strDescuento = "0";
        }

        double flete;
        try {
            flete = Double.parseDouble(txtFlete.getText().toString().trim());
        } catch (Exception e) {
            flete = 0;
        }

        if (!isConsultarProducto.equals(Configuracion.FLGVERDADERO) || tipo.equals("3")) {

            if (isPresentacion.equals(Configuracion.FLGVERDADERO))
                ldCantidad = (ldCantidad * ldCantidadPre) + (ldBonificacion * ldCantidadPre) + ldCantidadFrac + (ldBonificacionFracc * ldCantidadPre);
            else
                ldCantidad = (ldCantidad) + (ldBonificacion) + ldCantidadFrac;

            if (isBonificacion.equals(Configuracion.FLGFALSO)) {
                if (ldCantidad <= 0) {
                    UtilitarioPedidos.mostrarDialogo(this, CONSICOVALIDAR);
                    flgPaso = false;
                }

                if (flgPaso &&
                        isStockRestrictivo.equals(Configuracion.FLGVERDADERO)) {
                    if (ldStock < ldCantidad) {
                        UtilitarioPedidos.mostrarDialogo(this, CONSVALIDASTOCK);
                        flgPaso = false;
                    }
                }
            } else {

                if (((ldBonificacion + ldBonificacionFracc) <= 0 && ldCantidad <= 0)) {
                    UtilitarioPedidos.mostrarDialogo(this, CONSVALBONIFICACION);
                    flgPaso = false;
                }

                if (flgPaso && isStockRestrictivo
                        .equals(Configuracion.FLGVERDADERO)) {
                    if (ldStock < ldCantidad) {
                        UtilitarioPedidos.mostrarDialogo(this, CONSVALIDASTOCK);
                        flgPaso = false;
                    }
                }
            }
        }

        if (flgPaso && isPresentacion.equals(Configuracion.FLGVERDADERO) &&
                iiMaximoNumeroDecimales > 0) {
            //cantidad
            if (!ioBeanFraccionamiento.fraccionamientoValido(ioBeanPresentacion.getUnidadFraccionamiento())) {
                UtilitarioPedidos.mostrarDialogo(this, CONSVALIDARFRACC);
                flgPaso = false;
            }

            //bonificacion
            if (flgPaso && !(((ldBonificacionFracc * ldCantidadPre) % ioBeanPresentacion.getUnidadFraccionamiento()) == 0)) {
                UtilitarioPedidos.mostrarDialogo(this, CONSVALIDARFRACCBONIF);
                flgPaso = false;
            }
        }

        if (TextUtils.isEmpty(strDescuento)) {
            strDescuento = "0";
        }

        if (Double.parseDouble(strDescuento) != 0) {
            if (isDescuentoVolumen) {

                fnObtenerDescuentosVolumen();

                if (pairDescuentos == null) {
                    strDescuento = "0";
                } else {
                    if (isDescPorc
                            .equals(Configuracion.FLGVERDADERO)) {
                        if (Double.parseDouble(pairDescuentos.first) > Double.parseDouble(strDescuento) ||
                                Double.parseDouble(pairDescuentos.second) < Double.parseDouble(strDescuento)) {
                            UtilitarioPedidos.mostrarDialogo(this, CONSRANGODESCVOLPOR);
                            flgPaso = false;
                        }

                    } else {
                        double montoBruto = Double.parseDouble(txtPrecio.getText().toString()) * Double.parseDouble(strCantidad);
                        double montoBrutoSoles = Double.parseDouble(txtPrecio.getText().toString()) * Double.parseDouble(strCantidad); //@JBELVY
                        double montoBrutoDolar = Double.parseDouble(txtPrecioDolar.getText().toString()) * Double.parseDouble(strCantidad); //@JBELVY

                        montoMinimo = (Double.parseDouble(pairDescuentos.first) / 100)  * (montoBruto);
                        montoMinimoSoles = (Double.parseDouble(pairDescuentos.first) / 100) * (montoBrutoSoles); //@JBELVY
                        montoMinimoSoles = (Double.parseDouble(pairDescuentos.first) / 100)  * (montoBrutoDolar); //@JBELVY

                        montoMaximo = (Double.parseDouble(pairDescuentos.second) / 100) * (montoBruto);
                        montoMaximoSoles = (Double.parseDouble(pairDescuentos.second) / 100) * (montoBrutoSoles); //@JBELVY
                        montoMaximoDolares = (Double.parseDouble(pairDescuentos.second) / 100)  * (montoBrutoDolar); //@JBELVY

                        double strDescuentoTmp1 = (Double.parseDouble(strDescuento) * 100) / (montoBruto);
                        double strDescuentoTmp1Soles = (Double.parseDouble(strDescuento) * 100)  / (montoBrutoSoles);
                        double strDescuentoTmp1Dolar = (Double.parseDouble(strDescuento) * 100) / (montoBrutoDolar);

                        //@JBELVY I
                        //if (Double.parseDouble(pairDescuentos.first) > strDescuentoTmp1 ||
                        //        Double.parseDouble(pairDescuentos.second) < strDescuentoTmp1) {
                        //    UtilitarioPedidos.mostrarDialogo(this, CONSRANGODESCMON);
                        //    flgPaso = false;
                        if (Double.parseDouble(pairDescuentos.first) > strDescuentoTmp1Soles ||
                                Double.parseDouble(pairDescuentos.second) < strDescuentoTmp1Soles) {
                            UtilitarioPedidos.mostrarDialogo(this, CONSRANGODESCMON);
                            flgPaso = false;
                        }
                        //@JBELVY F
                    }
                }
            } else {

                if (isDescPorc.equals(Configuracion.FLGVERDADERO)) {
                    //@JBELVY I
                    if (Double.parseDouble(ioBeanPrecio.getDescuentoMinimo()) > Double.parseDouble(strDescuento)
                            || Double.parseDouble(ioBeanPrecio.getDescuentoMaximo()) < Double.parseDouble(strDescuento)) {
                        UtilitarioPedidos.mostrarDialogo(this, CONSRANGODESCPOR);
                        flgPaso = false;
                    }
                    //@JBELVY F
                } else {

                    double montoBruto = Double.parseDouble(txtPrecio.getText()
                            .toString()) * Double.parseDouble(strCantidad);
                    double montoBrutoSoles = Double.parseDouble(txtPrecio.getText()
                            .toString()) * Double.parseDouble(strCantidad); //@JBELVY
                    double montoBrutoDolares = Double.parseDouble(txtPrecioDolar.getText()
                            .toString()) * Double.parseDouble(strCantidad); //@JBELVY

                    montoMinimo = (Double.parseDouble(ioBeanPrecio.getDescuentoMinimo()) / 100) * (montoBruto);
                    montoMinimoSoles = (Double.parseDouble(ioBeanPrecio.getDescuentoMinimo()) / 100) * (montoBrutoSoles); //@JBELVY
                    montoMinimoDolares = (Double.parseDouble(ioBeanPrecio.getDescuentoMinimo()) / 100) * (montoBrutoDolares); //@JBELVY

                    montoMaximo = (Double.parseDouble(ioBeanPrecio.getDescuentoMaximo()) / 100) * (montoBruto);
                    montoMaximoSoles = (Double.parseDouble(ioBeanPrecio.getDescuentoMaximo()) / 100) * (montoBrutoSoles); //@JBELVY
                    montoMaximoDolares = (Double.parseDouble(ioBeanPrecio.getDescuentoMaximo()) / 100) * (montoBrutoDolares); //@JBELVY

                    double strDescuentoTmp1 = (Double.parseDouble(strDescuento) * 100) / (montoBruto);
                    double strDescuentoTmp1Soles = (Double.parseDouble(strDescuento) * 100) / (montoBrutoSoles); //@JBELVY
                    double strDescuentoTmp1Dolares = (Double.parseDouble(strDescuento) * 100) / (montoBrutoDolares); //@JBELVY

                    if (Double.parseDouble(ioBeanPrecio.getDescuentoMinimo()) > strDescuentoTmp1Soles) { //@JBELVY Before strDescuentoTmp1
                        UtilitarioPedidos.mostrarDialogo(this, CONSRANGODESCMON);
                        flgPaso = false;
                    }

                    if (Double.parseDouble(ioBeanPrecio.getDescuentoMaximo()) < strDescuentoTmp1Soles) { //@JBELVY Before strDescuentoTmp1
                        UtilitarioPedidos.mostrarDialogo(this, CONSRANGODESCMON);
                        flgPaso = false;
                    }
                }
            }
        }

        if (flgPaso && !tipo.equals("3")) {

            //actualizar el valor del flag de edicion de pedido
            UtilitarioData.fnCrearPreferencesPutBoolean(this, "editoPedido", true);

            String lsBeanPedidoCab = UtilitarioData.fnObtenerPreferencesString(this, sharePreferenceBeanPedidoCab);
            BeanEnvPedidoCab loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper.fromJson(lsBeanPedidoCab, BeanEnvPedidoCab.class);

            Bundle loExtras = getIntent().getExtras();
            String isListaPrecio = loExtras.getString("idListaPrecio");
            String strPrecio = txtPrecio.getText().toString();
            String strPrecioSoles = txtPrecio.getText().toString(); //@JBELVY
            String strPrecioDolar = txtPrecioDolar.getText().toString(); //@JBELVY

            String strObserva = txtObservacion.getText().toString();

            if (strPrecio.equals("")) {
                strPrecio = ioBeanPrecio.getPrecio();
            }
            //@JBELVY I
            if (strPrecioSoles.equals("")) {
                strPrecioSoles = ioBeanPrecio.getPrecioSoles();
            }
            if (strPrecioDolar.equals("")) {
                strPrecioDolar = ioBeanPrecio.getPrecioDolares();
            }
            //@JBELVY F
            if (strDescuento.equals("")) {
                strDescuento = "0";
            }

            double precio;
            double precioSoles; //@JBELVY
            double precioDolar; //@JBELVY
            try {
                precio = Double.parseDouble(strPrecio);
                precioSoles = Double.parseDouble(strPrecioSoles); //@JBELVY
                precioDolar = Double.parseDouble(strPrecioDolar); //@JBELVY
            } catch (Exception e) {
                precio = 0;
                precioSoles = 0;
                precioDolar = 0;
            }
            double cantidad;
            try {
                cantidad = Double.parseDouble(strCantidad);
            } catch (Exception e) {
                cantidad = 0;
            }
            double montoFrac;
            try {
                montoFrac = Double.parseDouble(txtPrecioFrac.getText().toString());
            } catch (Exception e) {
                montoFrac = 0;
            }
            double montoBruto = precio * cantidad;
            double montoBrutoSoles = precioSoles * cantidad; //@JBELVY
            double montoBrutoDolar = precioDolar * cantidad; //@JBELVY

            double montoDesc = 0.0;
            double montoDescSoles = 0.0; //@JBELVY
            double montoDescDolar = 0.0; //@JBELVY

            loBeanPedidoDet = new BeanEnvPedidoDet();

            if (isDescPorc.equals(Configuracion.FLGVERDADERO)
                    && isDescMonto.equals(Configuracion.FLGFALSO)) {

                if (isDescuentoVolumen) {

                    if (pairDescuentos == null) {
                        strDescuento = "0";
                    } else {
                        if (Double.parseDouble(pairDescuentos.first) > Double
                                .parseDouble(strDescuento) ||
                                Double.parseDouble(pairDescuentos.second) < Double
                                        .parseDouble(strDescuento)) {
                            strDescuento = "0";
                        }
                    }
                }
                montoDesc = (Double.parseDouble(strDescuento) / 100) * (montoBruto);
                montoDescSoles = (Double.parseDouble(strDescuento) / 100) * (montoBrutoSoles); //@JBELVY
                montoDescDolar = (Double.parseDouble(strDescuento) / 100) * (montoBrutoDolar); //@JBELVY

                loBeanPedidoDet.setTipoDescuento(Configuracion.DESCUENTO_PORCENTAJE);
            } else if (isDescPorc.equals(Configuracion.FLGFALSO)
                    && isDescMonto
                    .equals(Configuracion.FLGVERDADERO)) {

                double strDescuentoTmp1 = (Double.parseDouble(strDescuento) * 100) / (montoBruto);
                double strDescuentoTmp1Soles = (Double.parseDouble(strDescuento) * 100) / (montoBrutoSoles); //@JBELVY
                double strDescuentoTmp1Dolar = (Double.parseDouble(strDescuento) * 100) / (montoBrutoDolar); //@JBELVY

                if (isDescuentoVolumen) {

                    if (pairDescuentos == null) {
                        strDescuento = "0";
                    } else {
                        if (Double.parseDouble(pairDescuentos.first) > strDescuentoTmp1Soles) { //@JBELVY Before strDescuentoTmp1
                            strDescuento = "0";
                        }

                        if (Double.parseDouble(pairDescuentos.second) < strDescuentoTmp1Soles) { //@JBELVY Before strDescuentoTmp1
                            strDescuento = "0";
                        }
                    }
                }

                montoDesc = Double.parseDouble(strDescuento);
                montoDescSoles = Double.parseDouble(strDescuento);//@JBELVY
                montoDescDolar = Double.parseDouble(strDescuento);//@JBELVY
                loBeanPedidoDet.setTipoDescuento(Configuracion.DESCUENTO_MONTO);
            }

            double montoFinal = montoBruto - montoDesc + montoFrac;
            double montoFinalSoles = montoBrutoSoles - montoDescSoles + montoFrac;//@JBELVY
            double montoFinalDolar = montoBrutoDolar - montoDescDolar + montoFrac;//@JBELVY

            loBeanPedidoDet.setEmpresa(loBeanPedidoCab.getEmpresa());
            loBeanPedidoDet.setIdArticulo(ioBeanArticulo.getId());
            loBeanPedidoDet.setCodigoArticulo(ioBeanArticulo.getCodigo());
            loBeanPedidoDet.setCantidad(Utilitario.fnRound(cantidad,
                    isPresentacion.equals(Configuracion.FLGVERDADERO) ? 0 : iiMaximoNumeroDecimales));
            loBeanPedidoDet.setBonificacion(Utilitario.fnRound(ldBonificacion,
                    isPresentacion.equals(Configuracion.FLGVERDADERO) ? 0 : iiMaximoNumeroDecimales));
            loBeanPedidoDet.setMonto(Utilitario.fnRound(montoFinal,Integer.valueOf(isNumeroDecimales)));
            loBeanPedidoDet.setMontoSoles(Utilitario.fnRound(montoFinalSoles,Integer.valueOf(isNumeroDecimales)));//@JBELVY
            loBeanPedidoDet.setMontoDolar(Utilitario.fnRound(montoFinalDolar,Integer.valueOf(isNumeroDecimales)));//@JBELVY
            loBeanPedidoDet.setPrecioBase(strPrecio);
            loBeanPedidoDet.setPrecioBaseSoles(strPrecioSoles); //@JBELVY
            loBeanPedidoDet.setPrecioBaseDolares(strPrecioDolar); //@JBELVY
            loBeanPedidoDet.setTipoMoneda(TipoMonedaEs() ? "1" : "2"); //@JBELVY
            loBeanPedidoDet.setStock(Utilitario.fnRound(ldStock,
                    isPresentacion.equals(Configuracion.FLGVERDADERO) ? 0 : iiMaximoNumeroDecimales));
            loBeanPedidoDet.setDescuento(strDescuento);
            loBeanPedidoDet.setObservacion(strObserva);
            loBeanPedidoDet.setCodPedido("");
            loBeanPedidoDet.setNombreArticulo(ioBeanArticulo.getNombre());
            loBeanPedidoDet.setCodListaArticulo(ioBeanPrecio.getId());
            loBeanPedidoDet.setIdListaPrecio(isListaPrecio);
            loBeanPedidoDet.setFechaUltVenta(ioTxtFechaUltVenta.getText().toString());
            loBeanPedidoDet.setUltPrecio(ioTxtUltPrecio.getText().toString());

            if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO) && existeProductoAlmacen) {

                loBeanPedidoDet.setCodAlmacen(String.valueOf(ioBeanAlmacen.getPk()));

                if (!isCodAlmacenArticulo.equals(String.valueOf(ioBeanAlmacen.getPk())))
                    loBeanPedidoDet.setFlete(Utilitario.fnRound(flete, Integer.valueOf(isNumeroDecimales)));
                else
                    loBeanPedidoDet.setFlete(Utilitario.fnRound(0, Integer.valueOf(isNumeroDecimales)));
            }

            if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {

                BeanPresentacion loBeanPresentacion = (BeanPresentacion) ioBeanArticulo;
                loBeanPedidoDet.setCantidadFrac(String.valueOf((ioBeanFraccionamiento == null ? Long.valueOf(0) : ioBeanFraccionamiento.getNumerador()))); //@COMENTAR

                if (loBeanPresentacion.getUnidadFraccionamiento() == 1) {
                    loBeanPedidoDet.setCantidadFrac("".equals(txtFraccionamiento.getText().toString().trim()) ? "0" :
                            txtFraccionamiento.getText().toString().trim());
                }

                loBeanPedidoDet.setPrecioFrac(Utilitario.fnRound(txtPrecioFrac.getText().toString(), Integer.valueOf(isNumeroDecimales)));
                loBeanPedidoDet.setCantidadPre(String.valueOf(loBeanPresentacion.getCantidad()));
                loBeanPedidoDet.setBonificacionFrac(String.valueOf((long) (ldBonificacionFracc * ldCantidadPre)));
                loBeanPedidoDet.setProducto(loBeanPresentacion.getProducto());
            }

            // Agrego o modifico el producto


            loBeanPedidoCab.agregarProductoPedido(loBeanPedidoDet);

            if (isBonificacionAutomatica.equals(Configuracion.FLGVERDADERO)) {
                try {
                    loBeanPedidoCab = new DalPedido(this).fnValidarBonificacion(loBeanPedidoCab, iiMaximoNumeroDecimales, isStockRestrictivo);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // creo o edito mi sharedpreference
            try {
                lsBeanPedidoCab = BeanMapper.toJson(loBeanPedidoCab, false);
            } catch (Exception e) {
                e.printStackTrace();
            }
            SharedPreferences loSharedPreferences = getSharedPreferences(
                    ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
            SharedPreferences.Editor loEditor = loSharedPreferences.edit();
            loEditor.putString(sharePreferenceBeanPedidoCab, lsBeanPedidoCab);
            loEditor.commit();

            // por cambiar
            Intent loInstent = null;

            if (isMontoMaximoPedido > 0.0 && Double.parseDouble(loBeanPedidoCab.getMontoTotal()) > isMontoMaximoPedido) {
                mostrarSuperoMaximomonto = true;

            }
            if (tipo.equals("1")) {
                loInstent = new Intent(this, ActProductoPedido.class);

            } else {
                int tamDetalle = 0;
                String lsFiltro = "";
                ArrayList<BeanEnvPedidoDet> iolista = new DalProducto(this).fnListNoDBProducto(loBeanPedidoCab);
                for (int d = 0; d < iolista.size(); d++) {
                    if (iolista.get(d) instanceof BeanEnvPedidoDet) {
                        tamDetalle++;
                    }
                }

                if (isMaximoItemsPedido > 0 && iolista.size() > isMaximoItemsPedido) {
                    mostrarSuperaMaximoItems = true;
                    //Eliminar el ultimo item agregado
                    loBeanPedidoCab.getListaDetPedido().remove(isMaximoItemsPedido);

                    try {
                        lsBeanPedidoCab = BeanMapper.toJson(loBeanPedidoCab, false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    loSharedPreferences = getSharedPreferences(
                            ConfiguracionNextel.CONSPREFERENCIA, Context.MODE_PRIVATE);
                    loEditor = loSharedPreferences.edit();
                    loEditor.putString(sharePreferenceBeanPedidoCab, lsBeanPedidoCab);
                    loEditor.commit();

                } else if (tamDetalle <= Integer.parseInt(getString(R.string.actproductopedido_cantmaxregistros)) - 1) {

                    ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
                    loInstent = new Intent(this, ActProductoBuscar.class);
                    BeanExtras loBeanExtras = new BeanExtras();
                    loBeanExtras.setFiltro("");
                    loBeanExtras.setFiltro2("");
                    loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
                    try {
                        lsFiltro = BeanMapper.toJson(loBeanExtras, false);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    loInstent.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO,
                            lsFiltro);
                } else {
                    loInstent = new Intent(this, ActProductoPedido.class);
                }
            }

            if (mostrarSuperoMaximomonto) {
                UtilitarioPedidos.mostrarDialogo(this, Constantes.SUPERAMONTOMAXIMO);
            } else if (mostrarSuperaMaximoItems) {
                UtilitarioPedidos.mostrarDialogo(this, Constantes.SUPEROMAXIMOITEMS);

            } else {
                finish();
                startActivity(loInstent);
            }
        }
        return flgPaso;
    }

    @Override
    public void setOnItemClickPopup(Object poObject, int piTipo) {
        switch (piTipo) {
            case POPUP_ALMACEN:

                ioBeanAlmacen = (BeanAlmacen) poObject;
                cmbAlmacen.setText(ioBeanAlmacen.getNombre());
                txtStock.setText(fnObtenerStock(Double.parseDouble(ioBeanAlmacen.getStock())));
                btnStock.setText("(" + fnObtenerStock(Double.parseDouble(ioBeanAlmacen.getStock())) + ")" +
                        " :: " + getString(R.string.actproductodetalle_btnactualizar));
                validarFleteYFracc();
                subMostrarVerificar(true);
                break;
            case POPUP_FRACCIONAMIENTO:

                ioBeanFraccionamiento = (BeanFraccionamiento) poObject;
                cmbFraccionamiento.setText(ioBeanFraccionamiento.toString());
                validarFleteYFracc();
                subMostrarVerificar(true);
                break;
        }
    }

    //@JBELVY I
    private void fbObtenerTipoCambio(){
        DalTipoCambio dalTipoCambio = new DalTipoCambio(this);
        pairTipoCambioSoles = dalTipoCambio.fnObtenerTipoCambio("0");
        pairTipoCambioDolar = dalTipoCambio.fnObtenerTipoCambio("1");
    }
    //@JBELVY F

    private BeanConsultaProducto fnBeanConsultaProducto() {

        Calendar loCalendar = Calendar.getInstance();
        Date loDate = loCalendar.getTime();
        String lsFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate)
                .toString();

        String lsBeanPedidoCab = getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE)
                .getString(sharePreferenceBeanPedidoCab, "");
        BeanEnvPedidoCab loBeanPedidoCab = (BeanEnvPedidoCab) BeanMapper
                .fromJson(lsBeanPedidoCab, BeanEnvPedidoCab.class);

        List<BeanEnvPedidoCtrl> loLstControles = loBeanPedidoCab.getListaCtrlPedido();

        String lsDstoFormaPago = "0";
        double ldCantidad = 0;
        long llCantidadFraccionamiento = 0;
        String lsCantidadTotal = "0";
        long llCantidadPresentacion = 0;
        String lsCodigoAlmacen = "";

        try {
            ldCantidad = Double.parseDouble(txtCantidad.getText().toString());
        } catch (Exception e) {
            ldCantidad = 0;
        }

        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            int liCantidad = (int) ldCantidad;
            ldCantidad = liCantidad;
        }

        try {
            if (ioRowFraccionamiento.getVisibility() == View.GONE) {
                llCantidadFraccionamiento = Long.valueOf(txtFraccionamiento.getText().toString());
            } else {
                llCantidadFraccionamiento = (ioBeanFraccionamiento == null ? Long.valueOf(0) : ioBeanFraccionamiento.getNumerador()); //@COMENTAR
            }
        } catch (Exception e) {
            llCantidadFraccionamiento = 0;
        }
        try {
            lsCodigoAlmacen = ioBeanAlmacen.getCodigo();
        } catch (Exception e) {
            lsCodigoAlmacen = "";
        }
        if (ioBeanArticulo instanceof BeanPresentacion) {
            llCantidadPresentacion = ((BeanPresentacion) ioBeanArticulo).getCantidad();
        }
        try {
            for (BeanEnvPedidoCtrl loBeanControl : loLstControles) {
                if (loBeanControl.getEtiquetaControl().equals(Configuracion.ETIQUETA_DSTO_FORMA_PAGO)) {
                    lsDstoFormaPago = loBeanControl.getValorControl().equals(Configuracion.FLGVERDADERO) ? "1" : "0";
                    break;
                }

            }
        } catch (Exception e) {
            lsDstoFormaPago = "0";
        }

        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            lsCantidadTotal = String.valueOf((long) (ldCantidad * llCantidadPresentacion + llCantidadFraccionamiento));
        } else {
            lsCantidadTotal = Utilitario.fnRound(ldCantidad, iiMaximoNumeroDecimales);
        }

        BeanConsultaProducto loBeanConsultaProducto = new BeanConsultaProducto();
        loBeanConsultaProducto.setCodigoUsuario(loBeanPedidoCab.getCodigoUsuario());
        loBeanConsultaProducto.setFecha(lsFecha);
        loBeanConsultaProducto.setCodigoCliente(loBeanPedidoCab.getCodigoCliente());
        loBeanConsultaProducto.setCodigoArticulo(ioBeanArticulo.getCodigo());
        loBeanConsultaProducto.setCondicionVenta(loBeanPedidoCab.getCondicionVenta());
        loBeanConsultaProducto.setDireccion(loBeanPedidoCab.getCodigoDireccion());
        loBeanConsultaProducto.setDireccionDespacho("");
        loBeanConsultaProducto.setCodigoAlmacen(lsCodigoAlmacen);
        loBeanConsultaProducto.setCantidad(lsCantidadTotal);
        loBeanConsultaProducto.setCantidadPresentacion(llCantidadPresentacion);
        loBeanConsultaProducto.setDescuento(txtDescuento.getText().toString());
        loBeanConsultaProducto.setDescuentoFormaPago(lsDstoFormaPago);
        loBeanConsultaProducto.setFlete(txtFlete.getText().toString());


        return loBeanConsultaProducto;
    }

    private void subMostrarVerificar(boolean pbConsultarProducto) {

        if (isConsultarProducto.equals(Configuracion.FLGVERDADERO) && pbConsultarProducto) {
            ioBtnVerificar.setVisibility(View.VISIBLE);
            ioRowFinalizar.setVisibility(View.GONE);
        } else {
            ioBtnVerificar.setVisibility(View.GONE);
            ioRowFinalizar.setVisibility(View.VISIBLE);
        }
    }

    private void validarFleteYFracc() {

        Long numerador;
        Double montoFraccionamiento;
        ioRowSubtotalDesc.setVisibility(isDescuento.equals(Configuracion.FLGVERDADERO)
                ? View.VISIBLE : View.GONE);

        txtSubtotal.setText(Utilitario.fnRound(0,
                Integer.valueOf(isNumeroDecimales)));
        txtSubtotalDolares.setText(Utilitario.fnRound(0,
                Integer.valueOf(isNumeroDecimales))); //@JBELVY
        txtSubtotalDesc.setText(Utilitario.fnRound(0,
                Integer.valueOf(isNumeroDecimales)));
        txtSubtotalDolDesc.setText(Utilitario.fnRound(0,
                Integer.valueOf(isNumeroDecimales)));//@JBELVY
        txtTotalFlete.setText(Utilitario.fnRound(0,
                Integer.valueOf(isNumeroDecimales)));
        txtSubtotalFrac.setText(Utilitario.fnRound(0,
                Integer.valueOf(isNumeroDecimales)));

        txtPrecio.setText(Utilitario.fnRound(
                ioBeanPrecio.getPrecioSoles(), //@JBELVY Before getPrecio
                Integer.valueOf(isNumeroDecimales)));

        txtPrecioDolar.setText(Utilitario.fnRound(
                ioBeanPrecio.getPrecioDolares(),
                Integer.valueOf(isNumeroDecimales))); //@JBELVY


        if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO) && existeProductoAlmacen) {

            if (isCodAlmacenArticulo
                    .equals(String.valueOf(ioBeanAlmacen.getPk()))) {

                ioRowFlete.setVisibility(View.GONE);

                if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {

                    if (ioRowFraccionamiento.getVisibility() == View.GONE) {
                        if (txtFraccionamiento.getText().toString().equals("")) {
                            numerador = Long.valueOf(0);
                        } else {
                            numerador = Long.valueOf(txtFraccionamiento.getText().toString());
                        }

                    } else {
                         numerador = (ioBeanFraccionamiento == null ? Long.valueOf(0) : ioBeanFraccionamiento.getNumerador()); //@COMENTAR
                    }

                    if (numerador > 0) {
                        ioRowTotalFrac.setVisibility(View.VISIBLE);

                        lblSubtotalFrac.setText(getString(R.string.actproductodetalle_total) + "("
                                + isSimboloMoneda + "):");

                        lblSubtotalDesc.setText(getString(R.string.actproductodetalle_subtotaldesc) + "("
                                + isSimboloMoneda + "):");

                        lblSubtotalDolDesc.setText(getString(R.string.actproductodetalle_subtotaldesc) + "("
                                + isSimboloMonedaDolares + "):"); // @JBELVY
                        txtPrecioFrac.setText(Utilitario.fnRound(
                                ioBeanFraccionamiento.fnObtenerMontoFracc(Double
                                        .parseDouble(ioBeanPrecio.getPrecioSoles())), // @JBELVY
                                Integer.valueOf(isNumeroDecimales)));
                    } else {
                        ioRowTotalFrac.setVisibility(View.GONE);
                        ioRowSubtotalDesc.setVisibility(View.VISIBLE);

                        lblSubtotalDesc.setText(getString(R.string.actproductodetalle_total) + "("
                                + isSimboloMoneda + "):");

                        lblSubtotalDolDesc.setText(getString(R.string.actproductodetalle_total) + "("
                                + isSimboloMonedaDolares + "):"); // @JBELVY
                        txtPrecioFrac.setText(Utilitario.fnRound(
                                0, Integer.valueOf(isNumeroDecimales)));
                    }


                } else {
                    ioRowTotalFrac.setVisibility(View.GONE);
                    ioRowSubtotalDesc.setVisibility(View.VISIBLE);
                    lblSubtotalDesc.setText(getString(R.string.actproductodetalle_total) + "("
                            + isSimboloMoneda + "):");
                    txtPrecioFrac.setText(Utilitario.fnRound(
                            0, Integer.valueOf(isNumeroDecimales)));

                    lblSubtotalDolDesc.setText(getString(R.string.actproductodetalle_total) + "("
                            + isSimboloMonedaDolares + "):"); // @JBELVY
                }

            } else {
                ioRowFlete.setVisibility(View.VISIBLE);

                lblTotalFlete.setText(getString(R.string.actproductodetalle_total) + "("
                        + isSimboloMoneda + "):");
                lblSubtotalFrac.setText(getString(R.string.actproductodetalle_subtotalfrac) + "("
                        + isSimboloMoneda + "):");
                lblSubtotalDesc.setText(getString(R.string.actproductodetalle_subtotaldesc) + "("
                        + isSimboloMoneda + "):");
                lblSubtotalDolDesc.setText(getString(R.string.actproductodetalle_subtotaldesc) + "("
                        + isSimboloMonedaDolares + "):"); //@JBELVY

                if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {

                    if (ioRowFraccionamiento.getVisibility() == View.GONE) {
                        if (txtFraccionamiento.getText().toString().equals("")) {
                            numerador = Long.valueOf(0);
                        } else {
                            numerador = Long.valueOf(txtFraccionamiento.getText().toString());
                        }
                    } else {
                        numerador = (ioBeanFraccionamiento == null ? Long.valueOf(0) : ioBeanFraccionamiento.getNumerador());//@COMENTAR
                    }

                    if (numerador > 0) {
                        ioRowTotalFrac.setVisibility(View.VISIBLE);
                        txtPrecioFrac.setText(Utilitario.fnRound(
                                ioBeanFraccionamiento.fnObtenerMontoFracc(Double
                                        .parseDouble(ioBeanPrecio.getPrecio())),
                                Integer.valueOf(isNumeroDecimales)));
                    } else {
                        ioRowTotalFrac.setVisibility(View.GONE);
                        txtPrecioFrac.setText(Utilitario.fnRound(
                                0, Integer.valueOf(isNumeroDecimales)));
                    }


                } else {
                    ioRowTotalFrac.setVisibility(View.GONE);
                    txtPrecioFrac.setText(Utilitario.fnRound(
                            0, Integer.valueOf(isNumeroDecimales)));
                }
            }

        } else {
            if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {

                if (ioRowFraccionamiento.getVisibility() == View.GONE) {
                    if (txtFraccionamiento.getText().toString().equals("")) {
                        numerador = Long.valueOf(0);
                    } else {
                        numerador = Long.valueOf(Integer.parseInt(txtFraccionamiento.getText().toString()));
                    }

                } else {
                    numerador = (ioBeanFraccionamiento == null ? Long.valueOf(0) : ioBeanFraccionamiento.getNumerador()); //@COMENTAR
                }

                if (numerador > 0) {
                    ioRowTotalFrac.setVisibility(View.VISIBLE);

                    lblSubtotalFrac.setText(getString(R.string.actproductodetalle_total) + "("
                            + isSimboloMoneda + "):");

                    lblSubtotalDesc.setText(getString(R.string.actproductodetalle_subtotaldesc) + "("
                            + isSimboloMoneda + "):");

                    lblSubtotalDolDesc.setText(getString(R.string.actproductodetalle_subtotaldesc) + "("
                            + isSimboloMonedaDolares + "):"); // @JBELVY

                    txtPrecioFrac.setText(Utilitario.fnRound(
                            ioBeanFraccionamiento.fnObtenerMontoFracc(Double
                                    .parseDouble(ioBeanPrecio.getPrecio())),
                            Integer.valueOf(isNumeroDecimales)));
                } else {
                    ioRowTotalFrac.setVisibility(View.GONE);
                    ioRowSubtotalDesc.setVisibility(View.VISIBLE);
                    lblSubtotalDesc.setText(getString(R.string.actproductodetalle_total) + "("
                            + isSimboloMoneda + "):");

                    lblSubtotalDolDesc.setText(getString(R.string.actproductodetalle_total) + "("
                            + isSimboloMonedaDolares + "):"); // @JBELVY
                    txtPrecioFrac.setText(Utilitario.fnRound(
                            0, Integer.valueOf(isNumeroDecimales)));
                }


            } else {
                ioRowTotalFrac.setVisibility(View.GONE);
                ioRowSubtotalDesc.setVisibility(View.VISIBLE);
                lblSubtotalDesc.setText(getString(R.string.actproductodetalle_total) + "("
                        + isSimboloMoneda + "):");

                lblSubtotalDolDesc.setText(getString(R.string.actproductodetalle_total) + "("
                        + isSimboloMonedaDolares + "):"); // @JBELVY
                txtPrecioFrac.setText(Utilitario.fnRound(
                        0, Integer.valueOf(isNumeroDecimales)));
            }


        }
    }

    protected void NexShowDialog(int id) {
        DialogFragmentYesNo loDialog;
        String strTipoMoneda = String.valueOf(spTipoMoneda.getSelectedItem());
        switch (id) {
            case CONSICOVALIDAR:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSICOVALIDAR"),
                        getString(R.string.actproductodetalle_dlgingdatoscero),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSICOVALIDAR"))
                        == DialogFragmentYesNo.YES) {
                    txtCantidad.setText("");
                    txtCantidad.requestFocus();
                }
                break;

            case CONSVALIDARFRACC:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSVALIDARFRACC"),
                        getString(R.string.actproductodetalle_dlgcantidadfracc).
                                replace("?", String.valueOf(ioBeanPresentacion.fnFraccMinimo())),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSVALIDARFRACC"))
                        == DialogFragmentYesNo.YES) {
                    txtCantidad.requestFocus();
                }
                break;

            case CONSVALIDARFRACCBONIF:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSVALIDARFRACCBONIF"),
                        getString(R.string.actproductodetalle_dlgbonificacionfracc).
                                replace("?", String.valueOf(ioBeanPresentacion.fnFraccMinimo())),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSVALIDARFRACCBONIF"))
                        == DialogFragmentYesNo.YES) {
                    txtBonificacion.requestFocus();
                }
                break;

            case CONSVALBONIFICACION:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSVALBONIFICACION"),
                        getString(R.string.actproductodetalle_dlgvalidabonificacion),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSVALBONIFICACION"))
                        == DialogFragmentYesNo.YES) {
                    txtCantidad.requestFocus();
                }
                break;

            case CONSFLETE:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSFLETE"),
                        getString(R.string.actproductodetalle_dlgvalidaflete),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSFLETE"))
                        == DialogFragmentYesNo.YES) {
                    txtFlete.requestFocus();
                }
                break;

            case CONSVALIDASTOCK:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSVALIDASTOCK"),
                        getString(R.string.actproductodetalle_dlgvalidastock),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSVALIDASTOCK"))
                        == DialogFragmentYesNo.YES) {
                    txtCantidad.requestFocus();
                }
                break;

            case CONSICOVALIDARDCTO:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSICOVALIDARDCTO"),
                        getString(R.string.actproductodetalle_dlgmontonegativo),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSICOVALIDARDCTO"))
                        == DialogFragmentYesNo.YES) {
                    txtDescuento.requestFocus();
                }
                break;

            case CONSRANGODESCPOR:
                loDialog = DialogFragmentYesNo.newInstance(
                        getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSRANGODESCPOR"),
                        getString(R.string.actproductodetalle_dlgfueraRangoDesc)
                                + "\nIngresar un porcentaje entre "
                                + ioBeanPrecio.getDescuentoMinimo() + "% y "
                                + ioBeanPrecio.getDescuentoMaximo() + "%",
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSRANGODESCPOR"))
                        == DialogFragmentYesNo.YES) {
                    txtDescuento.requestFocus();
                }
                break;

            case CONSRANGODESCMON:
                loDialog = DialogFragmentYesNo.newInstance(
                        getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSRANGODESCMON"),
                        getString(R.string.actproductodetalle_dlgfueraRangoDesc)
                                + "\nIngresar un monto entre "
                                + (strTipoMoneda == Configuracion.MONEDASOLES ? (R.string.moneda) : (R.string.monedaDolares)) //@JBELVY
                                + " "
                                + Utilitario.fnRound(montoMinimo, Integer
                                .valueOf(isNumeroDecimales))
                                + " y "
                                + (strTipoMoneda == Configuracion.MONEDASOLES ? (R.string.moneda) : (R.string.monedaDolares)) //@JBELVY
                                + " "
                                + Utilitario.fnRound(montoMaximo, Integer
                                .valueOf(isNumeroDecimales)),
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSRANGODESCMON"))
                        == DialogFragmentYesNo.YES) {
                    txtDescuento.requestFocus();
                }
                break;
            case VALIDAHOME:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDAHOME"),
                        getString(R.string.actproductopedido_dlgback),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDAHOME"))
                        == DialogFragmentYesNo.YES) {

                    Intent intentl = new Intent(ActProductoDetalle.this,
                            ActMenu.class);
                    intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    startActivity(intentl);
                }
                break;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"),
                        getString(R.string.actmenu_dlgsincronizar),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSDIASINCRONIZAR"))
                        == DialogFragmentYesNo.YES) {
                    try {
                        String lsBeanUsuario = getSharedPreferences("Actual",
                                MODE_PRIVATE).getString("BeanUsuario", "");
                        BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                .fromJson(lsBeanUsuario, BeanUsuario.class);
                        estadoSincronizar = true;
                        new HttpSincronizar(loBeanUsuario.getCodVendedor(),
                                ActProductoDetalle.this, fnTipactividad())
                                .execute();
                    } catch (Exception e) {
                        subShowException(e);
                    }
                }
                break;

            case VALIDASINCRONIZACION:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"),
                        getString(R.string.actmenuvalsincro_titulo),
                        getString(R.string.actmenuvalsincro_mensaje),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRONIZACION"));
                break;

            case VALIDASINCRO:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"),
                        getString(R.string.msgregpendientesenvinfo),
                        R.drawable.boton_error, false, EnumLayoutResource.DESIGN04);
                loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("VALIDASINCRO"));
                break;

            case ENVIOPENDIENTES:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"),
                        getString(R.string.msgdeseaenviarpendientes),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("ENVIOPENDIENTES"))
                        == DialogFragmentYesNo.YES) {
                    UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActProductoDetalle.this);
                }
                break;

            case CONSSALIR:
                loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSSALIR"),
                        getString(R.string.actproductopedido_dlgsalir),
                        R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSSALIR"))
                        == DialogFragmentYesNo.YES) {
                    salir();
                }
                break;

            case CONSRANGODESCVOLPOR:
                loDialog = DialogFragmentYesNo.newInstance(
                        getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSRANGODESCPOR"),
                        getString(R.string.actproductodetalle_dlgfueraRangoDesc)
                                + "\nIngresar un porcentaje entre "
                                + pairDescuentos.first + "% y "
                                + pairDescuentos.second + "%",
                        R.drawable.boton_informacion, false,
                        EnumLayoutResource.DESIGN04);
                if (loDialog.showDialog(getFragmentManager(),
                        DialogFragmentYesNo.TAG.concat("CONSRANGODESCPOR"))
                        == DialogFragmentYesNo.YES) {
                    txtDescuento.requestFocus();
                }
                break;


        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog loAlertDialog = null;
        String strTipoMoneda = String.valueOf(spTipoMoneda.getSelectedItem());
        switch (id) {
            case Constantes.SUPERAMONTOMAXIMO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductodetalle_montoMaximoSuperado1))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case Constantes.SUPEROMAXIMOITEMS:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.actproductopedido_itemsSuperado))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        Intent loInstent = new Intent(ActProductoDetalle.this, ActProductoPedido.class);
                                        finish();
                                        startActivity(loInstent);

                                    }
                                }).create();
                return loAlertDialog;
            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSICOVALIDAR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actproductodetalle_dlgingdatoscero))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        txtCantidad.setText("");
                                        txtCantidad.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;

            case CONSVALIDARFRACC:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actproductodetalle_dlgcantidadfracc).
                                        replace("?", String.valueOf(ioBeanPresentacion.fnFraccMinimo())))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        txtCantidad.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;

            case CONSVALIDARFRACCBONIF:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actproductodetalle_dlgbonificacionfracc).
                                        replace("?", String.valueOf(ioBeanPresentacion.fnFraccMinimo())))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        txtBonificacion.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;

            case CONSVALIDASTOCK:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actproductodetalle_dlgvalidastock))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        txtCantidad.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;
            case CONSFLETE:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actproductodetalle_dlgvalidaflete))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        txtFlete.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;

            case CONSVALBONIFICACION:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actproductodetalle_dlgvalidabonificacion))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        txtCantidad.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;

            case CONSICOVALIDARDCTO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actproductodetalle_dlgmontonegativo))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        txtDescuento.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;
            case CONSRANGODESCPOR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.actproductodetalle_dlgfueraRangoDesc)
                                        + "\nIngresar un porcentaje entre "
                                        + ioBeanPrecio.getDescuentoMinimo()
                                        + "% y "
                                        + ioBeanPrecio.getDescuentoMaximo()
                                        + "%")
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        txtDescuento.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;
            case CONSRANGODESCMON:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.actproductodetalle_dlgfueraRangoDesc)
                                        + "\nIngresar un monto entre "
                                        + (strTipoMoneda == Configuracion.MONEDASOLES ? (R.string.moneda) : (R.string.monedaDolares)) //@JBELVY
                                        + " "
                                        + Utilitario.fnRound(montoMinimo, Integer
                                        .valueOf(isNumeroDecimales))
                                        + " y "
                                        + (strTipoMoneda == Configuracion.MONEDASOLES ? (R.string.moneda) : (R.string.monedaDolares)) //@JBELVY
                                        + " "
                                        + Utilitario.fnRound(montoMaximo, Integer
                                        .valueOf(isNumeroDecimales)))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        txtDescuento.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;

            case VALIDAHOME:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        Intent intentl = new Intent(
                                                ActProductoDetalle.this,
                                                ActMenu.class);
                                        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP);

                                        startActivity(intentl);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            String lsBeanUsuario = getSharedPreferences(
                                                    "Actual", MODE_PRIVATE)
                                                    .getString("BeanUsuario", "");
                                            BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper
                                                    .fromJson(lsBeanUsuario,
                                                            BeanUsuario.class);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActProductoDetalle.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActProductoDetalle.this);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case CONSBACK:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgback))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        Intent loIntent = new Intent(ActProductoDetalle.this, ActProductoPedido.class);
                                        finish();
                                        startActivity(loIntent);
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case CONSSALIR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actproductopedido_dlgsalir))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        salir();
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                    }
                                }).create();
                return loAlertDialog;

            case CONSRANGODESCVOLPOR:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(
                                getString(R.string.actproductodetalle_dlgfueraRangoDesc)
                                        + "\nIngresar un porcentaje entre "
                                        + pairDescuentos.first
                                        + "% y "
                                        + pairDescuentos.second
                                        + "%")
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        txtDescuento.requestFocus();
                                    }
                                }).create();
                return loAlertDialog;

            default:
                return super.onCreateDialog(id);
        }

    }

    private class DoneOnEditorActionListener implements OnEditorActionListener {
        @Override
        public boolean onEditorAction(TextView poTextView, int actionId,
                                      KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                if (poTextView == txtPrecio || poTextView == txtPrecioDolar) {
                    txtCantidad.requestFocus();
                } else if (poTextView == txtCantidad) {
                    txtDescuento.requestFocus();
                } else if (poTextView == txtDescuento) {
                    txtObservacion.requestFocus();
                } else if (poTextView == txtObservacion) {
                    if (isEditarPrecio
                            .equals(Configuracion.FLGFALSO)) {
                        txtCantidad.requestFocus();
                    } else {
                        txtPrecio.requestFocus();
                    }
                }
                return true;
            }
            return false;
        }
    }

    @Override
    public void onClick(View arg0) {

    }

    private double round(double d) {
        return (Math.floor(d * 100.0d + 0.5d) / 100.0d);
    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(
                R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir,
                null);

        Button btnSalir = (Button) _salir
                .findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario))
                .setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(
                R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                estadoSalir = 3;
                showDialog(CONSSALIR);
            }
        });
    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this,
                    EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        showDialog(VALIDAHOME);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @SuppressWarnings("deprecation")
    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = null;
            DalPedido loDalPedido = new DalPedido(this);
            loList = loDalPedido.fnEnvioPedidosPendientes(loBeanUsuario
                    .getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);

                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();
                showDialog(CONSDIASINCRONIZAR);
            } else {
                showDialog(VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? Constantes.ENVIOPENDIENTES : Constantes.NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub
        estadoSalir = 1;
        showDialog(CONSSALIR);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        estadoSalir = 2;
        showDialog(CONSSALIR);
    }

    private void salir() {

        if (estadoSalir == 2) {
            ((AplicacionEntel) getApplication()).setCurrentCliente(null);
            ((AplicacionEntel) getApplication())
                    .setCurrentCodFlujo(Configuracion.CONSSTOCK);
            ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
            Intent loIntento = new Intent(ActProductoDetalle.this,
                    ActProductoListaOnline.class);
            loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            BeanExtras loBeanExtras = new BeanExtras();
            loBeanExtras.setFiltro("");
            loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
            String lsFiltro = "";
            try {
                lsFiltro = BeanMapper.toJson(loBeanExtras, false);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
            startActivity(loIntento);
        } else if (estadoSalir == 1) {

            String lsBeanUsuario = getSharedPreferences(
                    "Actual", MODE_PRIVATE).getString(
                    "BeanUsuario", "");
            loBeanUsuario = (BeanUsuario) BeanMapper
                    .fromJson(lsBeanUsuario,
                            BeanUsuario.class);
            this.setMsgOk("Eficiencia Online");

            BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
            loBeanListaEficienciaOnline.setIdResultado(0);
            loBeanListaEficienciaOnline.setResultado("");
            loBeanListaEficienciaOnline.setListaEficiencia(null);
            loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

            new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

            Intent loInstent = new Intent(this, ActKpiDetalle.class);
            loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(loInstent);
        } else if (estadoSalir == 3) {
            Intent loIntentLogin = new Intent(ActProductoDetalle.this,
                    ActLogin.class);
            loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(loIntentLogin);
        }
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio()
                    .equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(
                    getString(R.string.ml_sincronizarantes)));
        }
    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */
    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this,
                        R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX",
                                    "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD",
                                    e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), EnumType.INFORMATION)
                                .show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        showDialog(Constantes.DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(
                            getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */
    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        try {
            DialogFragmentYesNo loDialog;
            switch (id) {
                case Constantes.DIALOG_NSERVICES_DOWNLOAD:
                    String lsNServices;
                    if (ioDALNservices == null)
                        ioDALNservices = DALNServices.getInstance(this, R.string.db_name);
                    try {
                        lsNServices = ioDALNservices.fnSelNServicesLabel();
                    } catch (Exception e) {
                        Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                        lsNServices = getString(R.string.nservices_label_default);
                    }
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG
                                    + Constantes.DIALOG_NSERVICES_DOWNLOAD,
                            getString(R.string.nservices_consultingdownlad)
                                    .replace("{0}", lsNServices),
                            EnumLayoutResource.getDefaultIconHelp(this));
                    return new DialogFragmentYesNoPairListener(loDialog,
                            new OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(
                                        DialogFragmentYesNo dialog) {
                                    try {
                                        subIniNServicesDownload(ioDALNservices.fnSelNServicesAPK());
                                    } catch (Exception e) {
                                        subShowException(e);
                                    }
                                }

                                @Override
                                public void performButtonNo(
                                        DialogFragmentYesNo dialog) {
                                }
                            });
                case CONSBACK:
                    loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                            DialogFragmentYesNo.TAG.concat("CONSBACK"),
                            getString(R.string.actproductopedido_dlgback),
                            R.drawable.boton_ayuda, EnumLayoutResource.DESIGN04);
                    return new DialogFragmentYesNoPairListener(loDialog,
                            new OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(DialogFragmentYesNo dialog) {
                                    finish();
                                    startActivity(new Intent(ActProductoDetalle.this, ActProductoPedido.class));
                                }

                                @Override
                                public void performButtonNo(DialogFragmentYesNo dialog) {

                                }
                            });
                default:
                    return super.onCreateNexDialog(id);
            }
        } catch (Exception e) {
            subShowException(e);
            return super.onCreateNexDialog(id);
        }
    }

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                Constantes.REQUEST_NSERVICES);
    }

}
