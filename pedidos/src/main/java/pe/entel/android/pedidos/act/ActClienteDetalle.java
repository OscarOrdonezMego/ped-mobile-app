package pe.entel.android.pedidos.act;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import greendroid.widget.ActionBarItem;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.DialogFragmentYesNo.OnDialogFragmentYesNoClickListener;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.IconContextMenu;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.AplicacionEntel;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding.MenuSlidingListener;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanCobranzaOnline;
import pe.entel.android.pedidos.bean.BeanEnvCanjeCab;
import pe.entel.android.pedidos.bean.BeanEnvDevolucionCab;
import pe.entel.android.pedidos.bean.BeanEnvNoCobranza;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanListaEficienciaOnline;
import pe.entel.android.pedidos.bean.BeanResponseConsultaClienteOnline;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.dal.DalAlmacen;
import pe.entel.android.pedidos.dal.DalCliente;
import pe.entel.android.pedidos.dal.DalCobranza;
import pe.entel.android.pedidos.dal.DalKPI;
import pe.entel.android.pedidos.dal.DalPedido;
import pe.entel.android.pedidos.dal.DalPendiente;
import pe.entel.android.pedidos.http.HttpObtenerEficiencia;
import pe.entel.android.pedidos.http.HttpSincronizar;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.Configuracion.EnumConfiguracion;
import pe.entel.android.pedidos.util.Configuracion.EnumModulo;
import pe.entel.android.pedidos.util.UtilitarioData;
import pe.entel.android.pedidos.util.UtilitarioFirebase;
import pe.entel.android.pedidos.util.ValidacionServicioUtil;
import pe.entel.android.pedidos.util.pedidos.Constantes;
import pe.entel.android.pedidos.util.pedidos.UtilitarioPedidos;

public class ActClienteDetalle extends NexActivity implements
        OnItemClickListener, OnClickListener, NexMenuCallbacks,
        MenuSlidingListener {
    // private Spinner spiDireccion;
    private TextView txtCodigo, txtNombre, txtDireccion, txtTcli, txtGiro,
            txtDeuda, txtDeudaDolares, txtEstado, txtFechaUltPedReg, txtUsrUltPedReg,
            lblColAdic5, txtColAdic5, lblColAdic4, txtColAdic4, lblColAdic3, lblLimiteCredito, lblCreditoUtilizado,
            txtColAdic3, lblColAdic2, txtColAdic2, lblColAdic1, txtColAdic1, txtLimiteCredito, txtCreditoUtilizado;

    private final int CONSCOBRA = 1;
    private final int CONSOTRO = -1;
    BeanCliente loBeanCliente;
    private long iiIdCliente;


    // Datos del Menu
    private IconContextMenu iconContextMenu = null;
    private final int CONTEXT_MENU_ID = 99;
    @SuppressWarnings("unused")
    private final int CONSCANCEL = 6;
    private final int CONSBACK = 7;
    private final int NOHAYPENDIENTES = 14;

    // Datos del dialog box CONDICION DE VENTA
    private final int CONSREGCONDVTA = 2;
    private LinearLayout ioLayout, lnCodUsrUltPedReg, lnFechaUltPedReg,
            lnlyColAdic1, lnlyColAdic2, lnlyColAdic3,
            lnlyColAdic4, lnlyColAdic5, lnlyLimiteCredito, lnlyCreditoUtilizado;

    private GridView ioGridViewCliente;
    BeanUsuario loBeanUsuario = null;
    private AdapMenuPrincipalSliding _menu;

    private final int CONSDIASINCRONIZAR = 10;
    private final int VALIDASINCRONIZACION = 11;
    private final int ENVIOPENDIENTES = 12;
    private final int VALIDASINCRO = 13;
    private final int DEUDAVENCIDA = 16;
    private final int CREDITOSUPERADO = 15;
    private final int CONSDIAGMAPS = 17;


    boolean estadoSincronizar = false;

    // Integracion Ntrack
    private DALNServices ioDALNservices;
    public static final int DIALOG_NSERVICES_DOWNLOAD = 104;// pueden asignar
    // otro
    public static final int REQUEST_NSERVICES = 204;// pueden asignar otro
    public static ArrayList<EnumMenuPrincipalItem> goArrItem = new ArrayList<EnumMenuPrincipalItem>();

    //PARAMETROS DE CONFIGURACION
    private String isSimboloMoneda;
    private String isNumDecimalesVista;
    private String isMostrarCondVenta;
    private String isPedido;
    private String isDevolucion;
    private String isCanje;
    private String isCobranza;
    private String isCotizacion;
    private String isDetalleCliente;
    private String isDesColumAdic1;
    private String isDesColumAdic2;
    private String isDesColumAdic3;
    private String isDesColumAdic4;
    private String isDesColumAdic5;
    private String isValColumAdic1;
    private String isValColumAdic2;
    private String isValColumAdic3;
    private String isValColumAdic4;
    private String isValColumAdic5;
    private String isMostrarDirPedido;
    private String isMostrarDirDespacho;
    private String isMostrarDirNoVisita;
    private String isMostrarAlmacen;
    private String isPresentacion;
    private String isMostrarCaberceraPedido;
    private String isCobranzaVencida;
    private String isLimiteCredito;
    private String isCobranzaClienteFueraDeRuta;
    private int cobranzaVencida;
    private String isNoCobranza;
    private BeanResponseConsultaClienteOnline loBeanConsultaClienteOnline;


    @Override
    protected int getThemeResource() {
        if (loBeanUsuario != null)
            if (loBeanUsuario.getFlgEstilo().equals("0"))
                return R.style.Theme_Pedidos_Orange;
            else if (loBeanUsuario.getFlgEstilo().equals("1"))
                return R.style.Theme_Pedidos_Blue;
            else
                return super.getThemeResource();
        else
            return super.getThemeResource();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ValidacionServicioUtil.validarServicio(this);

        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);
        isSimboloMoneda = EnumConfiguracion.SIMBOLO_MONEDA.getValor(this);
        isNumDecimalesVista = EnumConfiguracion.NUM_DECIMALES_VISTA.getValor(this);
        isMostrarCondVenta = EnumConfiguracion.MOSTRAR_CONDICION_VENTA.getValor(this);
        isPedido = EnumModulo.PEDIDO.getValor(this);
        isDevolucion = EnumModulo.DEVOLUCION.getValor(this);
        isCanje = EnumModulo.CANJE.getValor(this);
        isCobranza = EnumModulo.COBRANZA.getValor(this);
        isCotizacion = EnumConfiguracion.COTIZACION.getValor(this);
        isDetalleCliente = "Editar";
        isValColumAdic1 = EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_1.getValor(this);
        isValColumAdic2 = EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_2.getValor(this);
        isValColumAdic3 = EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_3.getValor(this);
        isValColumAdic4 = EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_4.getValor(this);
        isValColumAdic5 = EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_5.getValor(this);
        isDesColumAdic1 = EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_1.getDescripcion(this);
        isDesColumAdic2 = EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_2.getDescripcion(this);
        isDesColumAdic3 = EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_3.getDescripcion(this);
        isDesColumAdic4 = EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_4.getDescripcion(this);
        isDesColumAdic5 = EnumConfiguracion.CLIENTE_COLUMNA_ADICIONAL_5.getDescripcion(this);
        isMostrarCaberceraPedido = EnumConfiguracion.MOSTRAR_CABECERA_PEDIDO.getValor(this);
        isNoCobranza = "";
        isMostrarDirPedido = EnumConfiguracion.MOSTRAR_DIRECCION_PEDIDO.getValor(this);
        isMostrarDirDespacho = EnumConfiguracion.MOSTRAR_DIRECCION_DESPACHO.getValor(this);
        isMostrarDirNoVisita = EnumConfiguracion.MOSTRAR_DIRECCION_NO_VISITA.getValor(this);
        isMostrarAlmacen = EnumConfiguracion.MOSTRAR_ALMACEN.getValor(this);
        isPresentacion = Configuracion.EnumFuncion.FRACCIONAMIENTO.getValor(this);
        isCobranzaVencida = EnumConfiguracion.COBRANZA_VENCIDA.getValor(this);
        isLimiteCredito = EnumConfiguracion.LIMITE_CREDITO.getValor(this);
        isCobranzaClienteFueraDeRuta = EnumConfiguracion.COBRANZA_CLIENTE_FUERA_RUTA.getValor(this);
        Log.e("isCobranzaCFR", isCobranzaClienteFueraDeRuta);


        UtilitarioData.fnCrearPreferencesPutString(this, "MensajeVerificarPedido", "");
        UtilitarioData.fnCrearPreferencesPutString(this, "isHttpVerificarPedido", "");
        UtilitarioData.fnCrearPreferencesPutString(this, "beanPedidoCabVerificarPedido", "");
        UtilitarioData.fnCrearPreferencesPutBoolean(this, "editarFormularioInicio", false);


        DalCobranza dalCob = new DalCobranza(this);

        if (((AplicacionEntel) getApplication()).getClienteOnline() == 0) {
            loBeanCliente = ((AplicacionEntel) getApplication()).getCurrentCliente();
        } else {

            if (((AplicacionEntel) getApplication()).getCurrentConsultaClienteOnline() == null) {
                String lsBeanClienteOnline = getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, MODE_PRIVATE).getString("BeanClienteOnline", "");
                loBeanConsultaClienteOnline = (BeanResponseConsultaClienteOnline) BeanMapper.fromJson(lsBeanClienteOnline, BeanResponseConsultaClienteOnline.class);
            } else {
                loBeanConsultaClienteOnline = ((AplicacionEntel) getApplication()).getCurrentConsultaClienteOnline();
            }

            loBeanCliente = loBeanConsultaClienteOnline.getBeanCliente();

            if (isCobranzaClienteFueraDeRuta.equals(Configuracion.FLGVERDADERO)) {
                List<BeanCobranzaOnline> listaCobranzasOnline = loBeanConsultaClienteOnline.getListaCobranzasClienteOnline();

                if (listaCobranzasOnline.size() > 0) {
                    isCobranza = "T";

                    for (BeanCobranzaOnline cob : listaCobranzasOnline) {
                        dalCob.fnInsertarCobranzaOnline(cob);
                    }
                }
            } else {
                isCobranza = "F";
            }

            ((AplicacionEntel) getApplication()).setCurrentCliente(loBeanCliente);
            ((AplicacionEntel) getApplication()).setCurrentConsultaClienteOnline(loBeanConsultaClienteOnline);
        }

        if (isCobranzaVencida.equals(Configuracion.FLGVERDADERO)) {
            cobranzaVencida = dalCob.fnBuscarCobranzaVencida(loBeanCliente.getClicod());
        }

        setUsingNexMenuDrawerLayout(this);
        super.onCreate(savedInstanceState);

        this.setMsgOk(getString(R.string.actmenu_dlghttpok));
        this.setMsgError(getString(R.string.actmenu_dlghttperror));

        subIniMenu();
    }


    @Override
    public void onBackPressed() {

        ((AplicacionEntel) getApplication()).setCurrentConsultaClienteOnline(null);
        Intent loInstento = new Intent(this, ActClienteBuscar.class);
        BeanExtras loBeanExtra = new BeanExtras();
        loBeanExtra.setFiltro("");
        loBeanExtra.setTipo(EnumTipBusqueda.NOMBRE);
        loBeanExtra.setPosLista(0);
        String lsFiltro = "";
        try {
            lsFiltro = BeanMapper.toJson(loBeanExtra, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        loInstento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
        startActivity(loInstento);
        finish();
        return;
    }


    public void subIniActionBar() {

        this.getActionBarGD().setTitle(
                getString(R.string.actclientedetalle_bardetalle));
    }

    private void crearMenu() {

        goArrItem.clear();
        if (isPedido.equals(Configuracion.FLGVERDADERO)) {
            goArrItem.add(EnumMenuPrincipalItem.PEDIDO);
            goArrItem.add(EnumMenuPrincipalItem.NOVISITA);
            lnCodUsrUltPedReg.setVisibility(View.VISIBLE);
            lnFechaUltPedReg.setVisibility(View.VISIBLE);
        }
        if (isDevolucion.equals(Configuracion.FLGVERDADERO)) {
            goArrItem.add(EnumMenuPrincipalItem.DEVOLUCION);
        }
        if (isCanje.equals(Configuracion.FLGVERDADERO)) {
            goArrItem.add(EnumMenuPrincipalItem.CANJE);
        }
        if (isCobranza.equals(Configuracion.FLGVERDADERO)) {
            if ((Double.parseDouble(loBeanCliente.getClideuda()) > 0) || (Double.parseDouble(loBeanCliente.getClideudadolares()) > 0))
                goArrItem.add(EnumMenuPrincipalItem.COBRANZA);
        }

        if (isPedido.equals(Configuracion.FLGVERDADERO)) {
            goArrItem.add(EnumMenuPrincipalItem.ULTIMOPEDIDO);
        }

        if (isCotizacion.equals(Configuracion.FLGVERDADERO)) {
            goArrItem.add(EnumMenuPrincipalItem.COTIZACION);
        }
        goArrItem.add(EnumMenuPrincipalItem.POSICIONGPS);
        if (isCobranza.equals(Configuracion.FLGVERDADERO)) {
            if ((Double.parseDouble(loBeanCliente.getClideuda()) > 0) || (Double.parseDouble(loBeanCliente.getClideudadolares()) > 0)) {
                goArrItem.add(EnumMenuPrincipalItem.NOCOBRANZA);
            }
        }
        goArrItem.add(EnumMenuPrincipalItem.EDITCLIENTE);
    }

    @Override
    public void subSetControles() {

        setActionBarContentView(R.layout.clientedetalle2);
        super.subSetControles();

        txtCodigo = (TextView) findViewById(R.id.actclientedetalle_txtCodigo);
        txtNombre = (TextView) findViewById(R.id.actclientedetalle_txtNombre);
        txtDireccion = (TextView) findViewById(R.id.actclientedetalle_txtDireccion);
        txtTcli = (TextView) findViewById(R.id.actclientedetalle_txtTcli);
        txtGiro = (TextView) findViewById(R.id.actclientedetalle_txtGiro);
        txtDeuda = (TextView) findViewById(R.id.actclientedetalle_txtDeuda);
        txtDeudaDolares = findViewById(R.id.actclientedetalle_txtDeudaDolares);
        txtEstado = (TextView) findViewById(R.id.actclientedetalle_txtEstado);

        lblColAdic1 = (TextView) findViewById(R.id.actclientedetalle_lblColAdic1);
        lblColAdic2 = (TextView) findViewById(R.id.actclientedetalle_lblColAdic2);
        lblColAdic3 = (TextView) findViewById(R.id.actclientedetalle_lblColAdic3);
        lblColAdic4 = (TextView) findViewById(R.id.actclientedetalle_lblColAdic4);
        lblColAdic5 = (TextView) findViewById(R.id.actclientedetalle_lblColAdic5);
        lblLimiteCredito = (TextView) findViewById(R.id.actclientedetalle_lblLimiteCredito);
        lblCreditoUtilizado = (TextView) findViewById(R.id.actclientedetalle_lblCreditoUtilizado);

        txtColAdic1 = (TextView) findViewById(R.id.actclientedetalle_txtColAdic1);
        txtColAdic2 = (TextView) findViewById(R.id.actclientedetalle_txtColAdic2);
        txtColAdic3 = (TextView) findViewById(R.id.actclientedetalle_txtColAdic3);
        txtColAdic4 = (TextView) findViewById(R.id.actclientedetalle_txtColAdic4);
        txtColAdic5 = (TextView) findViewById(R.id.actclientedetalle_txtColAdic5);
        txtLimiteCredito = (TextView) findViewById(R.id.actclientedetalle_txtLimiteCredito);
        txtCreditoUtilizado = (TextView) findViewById(R.id.actclientedetalle_txtCreditoUtilizado);

        lnFechaUltPedReg = (LinearLayout) findViewById(R.id.actclientedetalle_lnFechaUltPedReg);
        lnCodUsrUltPedReg = (LinearLayout) findViewById(R.id.actclientedetalle_lnCodUsrUltPedReg);

        lnlyColAdic1 = (LinearLayout) findViewById(R.id.actclientedetalle_lnlyColAdic1);
        lnlyColAdic2 = (LinearLayout) findViewById(R.id.actclientedetalle_lnlyColAdic2);
        lnlyColAdic3 = (LinearLayout) findViewById(R.id.actclientedetalle_lnlyColAdic3);
        lnlyColAdic4 = (LinearLayout) findViewById(R.id.actclientedetalle_lnlyColAdic4);
        lnlyColAdic5 = (LinearLayout) findViewById(R.id.actclientedetalle_lnlyColAdic5);
        lnlyLimiteCredito = (LinearLayout) findViewById(R.id.actclientedetalle_lnlyLimiteCredito);
        lnlyCreditoUtilizado = (LinearLayout) findViewById(R.id.actclientedetalle_lnlyCreditoUtilizado);

        txtFechaUltPedReg = (TextView) findViewById(R.id.actclientedetalle_txtFechaUltPedReg);
        txtUsrUltPedReg = (TextView) findViewById(R.id.actclientedetalle_txtCodUsrUltPedReg);

        ioGridViewCliente = (GridView) findViewById(R.id.clientedetalle_griditems);
        ioGridViewCliente.setOnItemClickListener(this);

        lnFechaUltPedReg.setVisibility(View.GONE);
        lnCodUsrUltPedReg.setVisibility(View.GONE);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subAccDesMensaje() {

        if (estadoSincronizar) {
            if (getIdHttpResultado() == ConfiguracionNextel.CONSRESSERVIDOROK) {
                Intent loIntent = new Intent(this, ActMenu.class);

                loIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(loIntent);
            }
        }
    }

    @Override
    public void onClick(View arg0) {

    }

    @Override
    public void onResume() {
        super.onResume();
        subCargaDetalle();
        crearMenu();

        try {
            ioGridViewCliente.setAdapter(new MenuPrincipalAdapter(this));
        } catch (Exception e) {
            // TODO: handle exception
            Log.e("OnResumen", e.toString());
        }
    }

    private void subCargaDetalle() {
        Log.d("XXX", "ActClienteDetalle -> subCargaDetalle ");
        if (((AplicacionEntel) getApplication()).getCurrentCliente() != null) {
            if (((AplicacionEntel) getApplication()).getClienteOnline() == 0) {
                loBeanCliente = ((AplicacionEntel) getApplication()).getCurrentCliente();
            }

        } else {

            if (((AplicacionEntel) getApplication()).getClienteOnline() == 0) {
                Bundle loExtras = getIntent().getExtras();
                iiIdCliente = loExtras.getLong("IdCliente");
                loBeanCliente = new DalCliente(this).fnSelxIdCliente(iiIdCliente);
                ((AplicacionEntel) getApplication()).setCurrentCliente(loBeanCliente);
            } else {
                loBeanCliente = UtilitarioPedidos.obtenerBeanClienteOnlineDePreferences(this);
                ((AplicacionEntel) getApplication()).setCurrentCliente(loBeanCliente);
            }
        }

        txtCodigo.setText(loBeanCliente.getClicod());
        txtNombre.setText(loBeanCliente.getClinom());
        txtDireccion.setText(loBeanCliente.getClidireccion());
        txtTcli.setText(loBeanCliente.getTclinombre());
        txtGiro.setText(loBeanCliente.getCligiro());
        txtDeuda.setText(Utilitario.fnRound(loBeanCliente.getClideuda(), Integer.valueOf(isNumDecimalesVista)));
        txtDeudaDolares.setText(Utilitario.fnRound(loBeanCliente.getClideudadolares(), Integer.valueOf(isNumDecimalesVista)));
        txtEstado.setText(loBeanCliente.getEstado());
        txtFechaUltPedReg.setText(loBeanCliente.getFecUltPedido());
        txtUsrUltPedReg.setText(loBeanCliente.getCodUsrUltPedido());

        if (loBeanCliente.getFecUltPedido() == null)
            loBeanCliente.setFecUltPedido("");

        if (loBeanCliente.getCodUsrUltPedido() == null)
            loBeanCliente.setCodUsrUltPedido("");

        if (loBeanCliente.getFecUltPedido().compareTo("") == 0 || loBeanCliente.getCodUsrUltPedido().compareTo("") == 0) {
            txtFechaUltPedReg.setText(" - ");
            txtUsrUltPedReg.setText(" - ");
        }

        if (isValColumAdic1.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic1.setVisibility(View.VISIBLE);
            txtColAdic1.setText(loBeanCliente.getCampoAdicional1());
            lblColAdic1.setText(isDesColumAdic1);
        } else
            lnlyColAdic1.setVisibility(View.GONE);

        if (isValColumAdic2.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic2.setVisibility(View.VISIBLE);
            txtColAdic2.setText(loBeanCliente.getCampoAdicional2());
            lblColAdic2.setText(isDesColumAdic2);
        } else
            lnlyColAdic2.setVisibility(View.GONE);

        if (isValColumAdic3.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic3.setVisibility(View.VISIBLE);
            txtColAdic3.setText(loBeanCliente.getCampoAdicional3());
            lblColAdic3.setText(isDesColumAdic3);
        } else
            lnlyColAdic3.setVisibility(View.GONE);

        if (isValColumAdic4.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic4.setVisibility(View.VISIBLE);
            txtColAdic4.setText(loBeanCliente.getCampoAdicional4());
            lblColAdic4.setText(isDesColumAdic4);
        } else
            lnlyColAdic4.setVisibility(View.GONE);

        if (isValColumAdic5.equals(Configuracion.FLGVERDADERO)) {
            lnlyColAdic5.setVisibility(View.VISIBLE);
            txtColAdic5.setText(loBeanCliente.getCampoAdicional5());
            lblColAdic5.setText(isDesColumAdic5);
        } else
            lnlyColAdic5.setVisibility(View.GONE);

        if (isLimiteCredito.equals(Configuracion.FLGVERDADERO)) {
            lnlyLimiteCredito.setVisibility(View.VISIBLE);
            lblLimiteCredito.setText("Saldo Credito:");
            txtLimiteCredito.setText(isSimboloMoneda
                    + " "
                    + Utilitario.fnRound(loBeanCliente.getSaldoCredito(),
                    Integer.valueOf(isNumDecimalesVista)));
        } else
            lnlyLimiteCredito.setVisibility(View.GONE);
    }

    private void subCreatePreference(String psTipoPedido) {
        // CREAMOS LA CABECERA

        Calendar loCalendar = Calendar.getInstance();
        Date loDate = loCalendar.getTime();
        String lsFecha = DateFormat.format("dd/MM/yyyy kk:mm:ss", loDate).toString();

        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);

        BeanEnvPedidoCab loBeanPedidoCab = new BeanEnvPedidoCab(isPresentacion.equals(Configuracion.FLGVERDADERO)
                ? Configuracion.TipoArticulo.PRESENTACION
                : Configuracion.TipoArticulo.PRODUCTO);
        loBeanPedidoCab.setEmpresa(loBeanUsuario.getCodCompania());
        loBeanPedidoCab.setCodigoUsuario(loBeanUsuario.getCodVendedor());
        loBeanPedidoCab.setFechaInicio(lsFecha);
        loBeanPedidoCab.setCodigoCliente(loBeanCliente.getClicod());
        loBeanPedidoCab.setClipk(loBeanCliente.getClipk());
        loBeanPedidoCab.setFlgTipo(psTipoPedido);

        loBeanPedidoCab.setCodDireccion("0");

        // creo o edito mi sharedpreference
        String lsBeanVisitaCab = "";
        try {
            lsBeanVisitaCab = BeanMapper.toJson(loBeanPedidoCab, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        UtilitarioData.fnCrearPreferencesPutString(this, "beanPedidoCab", lsBeanVisitaCab);
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        switch (item.getItemId()) {
            case CONTEXT_MENU_ID:
                showDialog(CONTEXT_MENU_ID);
                break;

            case CONSBACK:
                Intent loInstento = new Intent(this, ActClienteBuscar.class);
                BeanExtras loBeanExtra = new BeanExtras();
                loBeanExtra.setFiltro("");
                loBeanExtra.setTipo(EnumTipBusqueda.NOMBRE);
                loBeanExtra.setPosLista(0);
                String loFiltro = "";
                try {
                    loFiltro = BeanMapper.toJson(loBeanExtra, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                loInstento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, loFiltro);
                startActivity(loInstento);
                break;

            default:
                return super.onHandleActionBarItemClick(item, position);
        }
        return true;
    }

    @SuppressWarnings("rawtypes")
    public class MenuPrincipalAdapter extends ArrayAdapter {

        private final TypedArray ioArrImage = obtainStyledAttributes(R.styleable.PedidosClienteDetalleItemsDrawables);
        private final String[] ioArrText = getResources().getStringArray(R.array.clientedetalleitems_strings);

        @SuppressWarnings("unchecked")
        public MenuPrincipalAdapter(Context poContext) {
            super(poContext, R.layout.clientedetalle_item, new String[goArrItem.size()]);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.clientedetalle_item, parent, false);

            TextView tv = (TextView) row.findViewById(R.id.clientedetalleitem_text);
            tv.setText(goArrItem.get(position).getText(ioArrText));

            tv.setCompoundDrawablesWithIntrinsicBounds(0, (goArrItem.get(position).getImageResource(ioArrImage)), 0, 0);

            row.setTag(R.string.keytag_menuprincipalitem, goArrItem.get(position));

            if (goArrItem.get(position).equals(EnumMenuPrincipalItem.VACIO)) {
                row.setEnabled(false);
                tv.setEnabled(false);
            }

            return row;
        }
    }

    @SuppressWarnings("unused")
    private enum EnumMenuPrincipalItem {
        PEDIDO, NOVISITA, DEVOLUCION, CANJE, COBRANZA, ULTIMOPEDIDO, COTIZACION, POSICIONGPS, NOCOBRANZA, EDITCLIENTE, VACIO;

        private String extraInfo = "";

        public int getImageResource(Context context) {
            return context.obtainStyledAttributes(R.styleable.PedidosClienteDetalleItemsDrawables).getResourceId(ordinal(), -1);
        }

        public int getImageResource(TypedArray array) {
            return array.getResourceId(ordinal(), -1);
        }

        public String getText(Context context) {
            return context.getResources().getStringArray(R.array.clientedetalleitems_strings)[ordinal()].concat(extraInfo);
        }

        public String getText(String[] array) {
            return array[ordinal()].concat(extraInfo);
        }

        public void setExtraInfo(String extraInfo) {
            this.extraInfo = extraInfo;
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        Log.d("XXX", "ActClienteDetalle -> onItemClick arg1 : " + arg1.getTag(R.string.keytag_menuprincipalitem));
        try {
            Intent loIntent = null;

            switch ((EnumMenuPrincipalItem) arg1.getTag(R.string.keytag_menuprincipalitem)) {
                case PEDIDO:
                    UtilitarioFirebase.seleccionarAccion(this, "PEDIDO", "PED");

                    subCreatePreference(Configuracion.FLGPEDIDO);

                    if (isCobranzaVencida.equals(Configuracion.FLGVERDADERO) && cobranzaVencida > 0) {
                        UtilitarioPedidos.mostrarDialogo(ActClienteDetalle.this, DEUDAVENCIDA);
                    } else {
                        onItemClickPedidoOrCotizacion();
                    }

                    break;
                case NOVISITA:

                    UtilitarioFirebase.seleccionarAccion(this, "NO PEDIDO", "NOPED");

                    ((AplicacionEntel) getApplication()).setCurrentCodFlujo(Configuracion.CONSNOPEDIDO);
                    subCreatePreference(Configuracion.FLGPEDIDO);

                    if (isMostrarDirNoVisita.equals(Configuracion.FLGVERDADERO) &&
                            new DalCliente(ActClienteDetalle.this).fnSelNumClienteDireccion(loBeanCliente
                                    .getClicod(), Configuracion
                                    .EnumFlujoDireccion.NO_VISITA
                                    .getCodigo(ActClienteDetalle.this)) > 0) {
                        loIntent = new Intent(ActClienteDetalle.this, ActPedidoClienteDireccionLista.class);
                        loIntent.putExtra(Configuracion.EXTRA_NAME_DIRECCION, Configuracion.EnumFlujoDireccion.NO_VISITA.name());

                    } else
                        loIntent = new Intent(this, ActMotNoPedidoLista.class);


                    loIntent.putExtra("IdCliente", iiIdCliente);
                    finish();
                    startActivity(loIntent);
                    break;
                case DEVOLUCION:

                    UtilitarioFirebase.seleccionarAccion(this, "DEVOLUCION", "DEV");

                    BeanEnvDevolucionCab loBeanEnvDevolucionCab = new BeanEnvDevolucionCab();
                    if (isPresentacion.equals(Configuracion.FLGVERDADERO))
                        loBeanEnvDevolucionCab.setTipoArticulo(Configuracion.TipoArticulo.PRESENTACION);
                    else
                        loBeanEnvDevolucionCab.setTipoArticulo(Configuracion.TipoArticulo.PRODUCTO);

                    ((AplicacionEntel) getApplication()).setCurrentCodFlujo(Configuracion.CONSDEVOLUC);
                    ((AplicacionEntel) getApplication()).setCurrentDevolucion(loBeanEnvDevolucionCab);
                    loIntent = new Intent(ActClienteDetalle.this, ActProductoItemLista.class);
                    finish();
                    startActivity(loIntent);
                    break;
                case CANJE:

                    UtilitarioFirebase.seleccionarAccion(this, "CANJE", "CANJE");

                    BeanEnvCanjeCab loBeanEnvCanjeCab = new BeanEnvCanjeCab();
                    if (isPresentacion.equals(Configuracion.FLGVERDADERO))
                        loBeanEnvCanjeCab.setTipoArticulo(Configuracion.TipoArticulo.PRESENTACION);
                    else
                        loBeanEnvCanjeCab.setTipoArticulo(Configuracion.TipoArticulo.PRODUCTO);
                    ((AplicacionEntel) getApplication()).setCurrentCodFlujo(Configuracion.CONSCANJE);
                    ((AplicacionEntel) getApplication()).setCurrentCanje(loBeanEnvCanjeCab);
                    loIntent = new Intent(ActClienteDetalle.this, ActProductoItemLista.class);
                    finish();
                    startActivity(loIntent);
                    break;
                case COBRANZA:
                    UtilitarioFirebase.seleccionarAccion(this, "COBRANZA", "COB");

                    ((AplicacionEntel) getApplication())
                            .setCurrentCodFlujo(Configuracion.CONSCOBRANZA);
                    if ((Double.parseDouble(loBeanCliente.getClideuda()) <= 0) && (Double.parseDouble(loBeanCliente.getClideudadolares()) <= 0)) {
                        if (!isUsingDialogFragment()) {
                            showDialog(CONSCOBRA);
                        } else {
                            DialogFragmentYesNo loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                                    DialogFragmentYesNo.TAG.concat("CONSCOBRA"), getString(R.string.actclientedetalle_dlgnodatos),
                                    R.drawable.boton_informacion, false, EnumLayoutResource.DESIGN04);
                            loDialog.showDialog(getFragmentManager(), DialogFragmentYesNo.TAG.concat("CONSCOBRA"));
                        }
                        return;
                    }

                    loIntent = new Intent(ActClienteDetalle.this, ActCobranzaLista.class);
                    loIntent.putExtra("IdCliente", iiIdCliente);

                    finish();
                    startActivity(loIntent);
                    break;
                case ULTIMOPEDIDO:

                    UtilitarioFirebase.seleccionarAccion(this, "ULTIMO PEDIDO", "UTLPED");

                    Intent loInstent = new Intent(this, ActMenuConsultaPedido.class);
                    loInstent.putExtra("CodCliente", loBeanCliente.getClicod());
                    startActivity(loInstent);
                    break;

                case COTIZACION:
                    UtilitarioFirebase.seleccionarAccion(this, "COTIZACION", "COTI");

                    subCreatePreference(Configuracion.FLGCOTIZACION);
                    onItemClickPedidoOrCotizacion();

                    break;

                case POSICIONGPS:

                    UtilitarioFirebase.seleccionarAccion(this, "GPS", "GPS");

                    if (!UtilitarioData.isGoogleMapsInstalled(this)) {
                        showDialog(CONSDIAGMAPS);
                    } else {
                        String latitudLongitud = loBeanCliente.getLatitud() + "," + loBeanCliente.getLongitud();
                        if (latitudLongitud.equals("0,0") || latitudLongitud.equals(",")) {
                            Toast.makeText(this, "No se encontro ubicacion", Toast.LENGTH_SHORT).show();
                        } else {
                            Uri gmmIntentUri = Uri.parse("geo:" + latitudLongitud + "?q=" + latitudLongitud + "(" + loBeanCliente.getClidireccion() + ")");
                            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                            mapIntent.setPackage("com.google.android.apps.maps");
                            if (mapIntent.resolveActivity(getPackageManager()) != null) {
                                startActivity(mapIntent);
                            }
                        }
                    }
                    break;
                case NOCOBRANZA:
                    UtilitarioFirebase.seleccionarAccion(this, "NOCOBRANZA", "COB");

                    ((AplicacionEntel) getApplication())
                            .setCurrentCodFlujo(Configuracion.CONSCOBRANZA);
                    if ((Double.parseDouble(loBeanCliente.getClideuda()) <= 0) && (Double.parseDouble(loBeanCliente.getClideudadolares()) <= 0)) {
                        if (!isUsingDialogFragment()) {
                            showDialog(CONSCOBRA);
                        } else {
                            DialogFragmentYesNo loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                                    DialogFragmentYesNo.TAG.concat("CONSCOBRA"), getString(R.string.actclientedetalle_dlgnodatos),
                                    R.drawable.boton_informacion, false, EnumLayoutResource.DESIGN04);
                            loDialog.showDialog(getFragmentManager(), DialogFragmentYesNo.TAG.concat("CONSCOBRA"));
                        }
                        return;
                    }

                    loIntent = new Intent(ActClienteDetalle.this, ActNoCobranza.class);
                    BeanEnvNoCobranza loBeanNoCobranza = new BeanEnvNoCobranza();
                    loBeanNoCobranza.setIdUsuario(loBeanUsuario.getCodVendedor());
                    loBeanNoCobranza.setIdCliente(String.valueOf(((AplicacionEntel) getApplication()).getCurrentCliente().getClicod()));
                    ((AplicacionEntel) getApplication()).setCurrentNoCobranza(loBeanNoCobranza);
                    finish();
                    startActivity(loIntent);

                    break;
                case EDITCLIENTE:

                    UtilitarioFirebase.seleccionarAccion(this, "EDITAR CLIENTE", "EDICLI");

                    Intent leditInstent = new Intent(this, ActClienteEditar.class);
                    leditInstent.putExtra("IdCliente", loBeanCliente.getClicod());
                    startActivity(leditInstent);

                    break;
                default:
                    break;
            }
        } catch (
                Exception e
        ) {
            subShowException(e);
        }
    }

    private void onItemClickPedidoOrCotizacion() {
        Log.d("XXX", "ActClienteDetalle -> onItemClickPedidoOrCotizacion ");
        ((AplicacionEntel) getApplication()).setCurrentCodFlujo(Configuracion.CONSPEDIDO);

        //reiniciar pedido
        ((AplicacionEntel) getApplicationContext()).activarRecuperarPedido(false);
        UtilitarioData.fnCrearPreferencesPutBoolean(this, "pedidoPendiente", false);

        // Seteo la hora de inicio de visita
        if (((AplicacionEntel) getApplication()).getCurrentKpi() == null) {
            DalKPI lodkpi = new DalKPI(this);
            ((AplicacionEntel) getApplication()).setCurrentKpi(lodkpi.fnSelCurrentKPI());
        }

        ((AplicacionEntel) getApplication()).getCurrentKpi().tiempoInicioTemporal = System
                .currentTimeMillis();

        //iniciar flag de edicion en false
        UtilitarioData.fnCrearPreferencesPutBoolean(this, "editoPedido", false);

        DalAlmacen dalAlmacen = new DalAlmacen(this);
        boolean existeProductoAlmacen = false;

        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            if (dalAlmacen.fnSelectAllPresentacion().size() > 0)
                existeProductoAlmacen = true;
        } else {
            if (dalAlmacen.fnSelectAllProducto().size() > 0)
                existeProductoAlmacen = true;
        }

        Intent loIntent;

        if (isMostrarDirPedido.equals(Configuracion.FLGVERDADERO) &&
                new DalCliente(ActClienteDetalle.this).fnSelNumClienteDireccion(loBeanCliente
                        .getClicod(), Configuracion
                        .EnumFlujoDireccion.PEDIDO
                        .getCodigo(ActClienteDetalle.this)) > 0) {

            loIntent = new Intent(ActClienteDetalle.this, ActPedidoClienteDireccionLista.class);
            loIntent.putExtra(Configuracion.EXTRA_NAME_DIRECCION, Configuracion.EnumFlujoDireccion.PEDIDO.name());

        } else if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO) && existeProductoAlmacen) {
            loIntent = new Intent(this, ActPedidoAlmacen.class);
        } else if (isMostrarCaberceraPedido.equals(Configuracion.FLGVERDADERO)) {
            loIntent = new Intent(ActClienteDetalle.this, ActPedidoFormulario.class);
            loIntent.putExtra(Configuracion.EXTRA_FORMULARIO, Configuracion.FORMULARIO_INICIO);
            loIntent.putExtra(Configuracion.EXTRA_FORMULARIO_INICIO_EDITAR, false);
        } else if (isMostrarCondVenta.equals(Configuracion.FLGVERDADERO)) {
            loIntent = new Intent(ActClienteDetalle.this, ActPedidoCondicionVenta.class);
        } else {
            loIntent = new Intent(ActClienteDetalle.this, ActProductoPedido.class);
        }
        Log.d("XXX", "ActClienteDetalle -> onItemClickPedidoOrCotizacion - loBeanCliente.getClipk() : " + loBeanCliente.getClipk());
        Log.d("XXX", "ActClienteDetalle -> onItemClickPedidoOrCotizacion - iiIdCliente : " + iiIdCliente);
        loIntent.putExtra("IdCliente", loBeanCliente.getClipk());
        finish();
        startActivity(loIntent);
    }

    private void subIniMenu() {
        View _titulo = getLayoutInflater().inflate(R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario)).setText(loBeanUsuario.getNombre());

        View _salir = getLayoutInflater().inflate(R.layout.menulateralsalir, null);

        Button btnSalir = (Button) _salir.findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario)).setText(loBeanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(getResources().getColor(R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);

        _layoutMenu.addView(btnSalir);

        btnSalir.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent loIntentLogin = new Intent(ActClienteDetalle.this, ActLogin.class);
                loIntentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(loIntentLogin);
            }
        });

    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        // TODO Auto-generated method stub
        if (_menu == null) {
            _menu = new AdapMenuPrincipalSliding(this, this, EnumMenuPrincipalSlidingItem.INICIO.getPosition());
        }
        return _menu;
    }

    @Override
    public void OnNexMenuItemClick(int arg0) {
        // TODO Auto-generated method stub
        _menu.OnItemClick(arg0);
    }

    @Override
    public void subHomeFromSliding() {
        // TODO Auto-generated method stub
        Intent intentl = new Intent(this, ActMenu.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(intentl);
    }

    @Override
    public void subInicioFromSliding() {
        // TODO Auto-generated method stub

    }

    @Override
    public void subSincronizarFromSliding() {
        try {
            List<BeanEnvPedidoCab> loList = new DalPedido(this).fnEnvioPedidosPendientes(loBeanUsuario.getCodVendedor());

            if (loList.size() <= 0) {
                DalPedido lDalPedido = new DalPedido(this);
                lDalPedido.fnDelTrDetPedido();
                lDalPedido.fnDelTrCabPedido();
                UtilitarioPedidos.mostrarDialogo(this, Constantes.CONSDIASINCRONIZAR);
            } else {
                UtilitarioPedidos.mostrarDialogo(this, Constantes.VALIDASINCRO);
            }
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void subPendientesFromSliding() {
        try {
            boolean existePendientes = new DalPendiente(this).fnExistePendientes(loBeanUsuario.getCodVendedor());
            UtilitarioPedidos.mostrarDialogo(this, existePendientes ? ENVIOPENDIENTES : NOHAYPENDIENTES);
        } catch (Exception e) {
            subShowException(e);
        }
    }

    @Override
    public void subEficienciaFromSliding() {
        // TODO Auto-generated method stub

        loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(this);
        this.setMsgOk("Eficiencia Online");

        BeanListaEficienciaOnline loBeanListaEficienciaOnline = new BeanListaEficienciaOnline();
        loBeanListaEficienciaOnline.setIdResultado(0);
        loBeanListaEficienciaOnline.setResultado("");
        loBeanListaEficienciaOnline.setListaEficiencia(null);
        loBeanListaEficienciaOnline.setUsuario(loBeanUsuario.getCodVendedor());

        new HttpObtenerEficiencia(loBeanListaEficienciaOnline, this, this.fnTipactividad()).execute();

        Intent loInstent = new Intent(this, ActKpiDetalle.class);
        loInstent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loInstent);
    }

    @Override
    public void subStockFromSliding() {
        // TODO Auto-generated method stub
        ((AplicacionEntel) getApplication()).setCurrentCliente(null);
        ((AplicacionEntel) getApplication())
                .setCurrentCodFlujo(Configuracion.CONSSTOCK);
        ((AplicacionEntel) getApplication()).setCurrentlistaArticulo(null);
        Intent loIntento = new Intent(ActClienteDetalle.this, ActProductoListaOnline.class);
        loIntento.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        BeanExtras loBeanExtras = new BeanExtras();
        loBeanExtras.setFiltro("");
        loBeanExtras.setTipo(EnumTipBusqueda.NOMBRE);
        String lsFiltro = "";
        try {
            lsFiltro = BeanMapper.toJson(loBeanExtras, false);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        loIntento.putExtra(ConfiguracionNextel.CONBUNDLEFILTRO, lsFiltro);
        startActivity(loIntento);
    }

    @Override
    public void subNServicesFromSliding() {
        // TODO Auto-generated method stub
        try {
            subCallNServices(false, loBeanUsuario.getFlgNServicesObligatorio().equals("T"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            subShowException(new Exception(getString(R.string.ml_sincronizarantes)));
        }

    }

    // Integracion NTrack
    /*
     * Inicia el servicio de Descarga de APK
     */
    private void subIniNServicesDownload(String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(this);
        NServicesDownloadService.startService(this, psAPK, /* Asignan el estilo  maestro de la app         */
                R.style.Theme_Pedidos_Master);
    }

    /*
     * Procedimiento para llamar al NServices. Crea Toasts o Diálogos de
     * interacción con el usuario para la descarga
     */

    private void subCallNServices(boolean fromSincro, boolean obligatorio)
            throws Exception {
        if (!Utilitario.verifyPackageInstalled(this,
                NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(this, R.string.db_name);
            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                            lsNServices = getString(R.string.nservices_label_default);
                        }
                        new NexToast(this, getString(
                                R.string.nservices_startingdownload).replace(
                                "{0}", lsNServices), EnumType.INFORMATION).show();
                        subIniNServicesDownload(lsAPK);
                    } else {
                        // showNexDialog(DIALOG_NSERVICES_DOWNLOAD);
                        showDialog(DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    subShowException(new Exception(getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(this);
        } else {
            subIniNServices();
        }
    }

    /*
     * Este método es utilizado en lugar del onCreateDialog cuando se está
     * utilizando el DialogFragmentos. Si ya existe el método implementado,
     * agregar el cuerpo del método en la implemetación existente. Si no se está
     * usando DialogFragmentos, agregar el cuerpo al onCreateDialog
     */
    @Override
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        try {
            DialogFragmentYesNo loDialog;
            switch (id) {
                case DIALOG_NSERVICES_DOWNLOAD:
                    String lsNServices;
                    if (ioDALNservices == null)
                        ioDALNservices = DALNServices.getInstance(this, R.string.db_name);
                    try {
                        lsNServices = ioDALNservices.fnSelNServicesLabel();
                    } catch (Exception e) {
                        Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                        lsNServices = getString(R.string.nservices_label_default);
                    }
                    loDialog = DialogFragmentYesNo.newInstance(
                            getFragmentManager(), DialogFragmentYesNo.TAG + DIALOG_NSERVICES_DOWNLOAD,
                            getString(R.string.nservices_consultingdownlad)
                                    .replace("{0}", lsNServices),
                            EnumLayoutResource.getDefaultIconHelp(this));
                    return new DialogFragmentYesNoPairListener(loDialog,
                            new OnDialogFragmentYesNoClickListener() {

                                @Override
                                public void performButtonYes(
                                        DialogFragmentYesNo dialog) {
                                    try {
                                        subIniNServicesDownload(ioDALNservices.fnSelNServicesAPK());
                                    } catch (Exception e) {
                                        subShowException(e);
                                    }
                                }

                                @Override
                                public void performButtonNo(
                                        DialogFragmentYesNo dialog) {
                                }
                            });

                default:
                    return super.onCreateNexDialog(id);
            }
        } catch (Exception e) {
            subShowException(e);
            return super.onCreateNexDialog(id);
        }
    }

    /*
     * Inicia la actividad del NServices
     */
    private void subIniNServices() {
        startActivityForResult(
                NServicesConfiguration.createIntentForIniNServicesActivity(),
                REQUEST_NSERVICES);
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        AlertDialog loAlertDialog = null;

        switch (id) {
            case NOHAYPENDIENTES:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage(getString(R.string.toanohaypendientes))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case CONSCOBRA:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.actclientedetalle_dlgnodatos))

                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                break;
            case CONSOTRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(
                                getString(R.string.actclientedetalle_dlgfueraruta))

                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                break;

            case CONTEXT_MENU_ID:
                Log.v("XXX", "AQUI-2");
                return iconContextMenu
                        .createMenu(getString(R.string.actclientedetalle_menutitulo));
            // break;

            case CONSREGCONDVTA:// REGISTRAR CONDICION DE VENTA
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle("Condición de Venta")
                        .setPositiveButton("Aceptar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        BeanEnvPedidoCab loBeanPedidoCab = UtilitarioPedidos.obtenerBeanPedidoCabDePreferences(ActClienteDetalle.this);
                                        String lsBeanVisitaFinCab = "";
                                        try {
                                            lsBeanVisitaFinCab = BeanMapper.toJson(loBeanPedidoCab, false);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        UtilitarioData.fnCrearPreferencesPutString(ActClienteDetalle.this, "beanPedidoCab", lsBeanVisitaFinCab);
                                        Intent loInstent = new Intent(ActClienteDetalle.this, ActProductoPedido.class);
                                        loInstent.putExtra("IdCliente", iiIdCliente);
                                        finish();
                                        startActivity(loInstent);
                                    }
                                })
                        .setNegativeButton("Cancelar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }

                                }).setView(ioLayout).create();
                break;

            case CONSDIASINCRONIZAR:// SINCRONIZAR
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.dlg_titinformacion))
                        .setMessage(getString(R.string.actmenu_dlgsincronizar))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        try {
                                            BeanUsuario loBeanUsuario = UtilitarioPedidos.obtenerBeanUsuarioDePreferences(ActClienteDetalle.this);
                                            estadoSincronizar = true;
                                            new HttpSincronizar(loBeanUsuario
                                                    .getCodVendedor(),
                                                    ActClienteDetalle.this,
                                                    fnTipactividad()).execute();
                                        } catch (Exception e) {
                                            estadoSincronizar = false;
                                            subShowException(e);
                                        }
                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;

            case VALIDASINCRONIZACION:
                loAlertDialog = null;
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.actmenuvalsincro_titulo))
                        .setMessage(getString(R.string.actmenuvalsincro_mensaje))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                    }
                                }).create();
                return loAlertDialog;
            case VALIDASINCRO:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setMessage(getString(R.string.ml_registrosenvio))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;
            case ENVIOPENDIENTES:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.ml_registrospendientes))
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                        UtilitarioPedidos.mostrarPendientesSegunConfiguracion(ActClienteDetalle.this);

                                    }
                                })
                        .setNegativeButton(getString(R.string.dlg_btnno),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                    }
                                }).create();
                return loAlertDialog;
            case DEUDAVENCIDA:
                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage((getString(R.string.actclientedetalle_cobranzavencida)))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;

            case CREDITOSUPERADO:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage((getString(R.string.actclientedetalle_creditoSuperado)))
                        .setPositiveButton(getString(R.string.dlg_btnok),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {
                                    }
                                }).create();
                return loAlertDialog;


            case CONSDIAGMAPS:

                loAlertDialog = new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setTitle(getString(R.string.titlenohatpendiente))
                        .setMessage("Esta aplicación necesita de Google Maps. ¿Desea instalarla?")
                        .setPositiveButton(getString(R.string.dlg_btnsi),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int whichButton) {

                                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.apps.maps"));
                                        startActivity(intent);
                                        finish();


                                    }
                                }).setNegativeButton(getString(R.string.dlg_btnno), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).create();


                return loAlertDialog;

            default:
                return super.onCreateDialog(id);

        }
        return super.onCreateDialog(id);
    }

}