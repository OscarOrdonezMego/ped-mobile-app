package pe.entel.android.pedidos.util;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.text.SimpleDateFormat;
import java.util.Date;

import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanEnvPedidoDet;

public class UtilitarioFirebase {


    public static void registrarProducto(Context context, BeanEnvPedidoCab cabecera) {

        try {

            String currency = "PEN";
            SimpleDateFormat dateFormatInicio = new SimpleDateFormat(
                    "dd/MM/yyyy kk:mm:ss");
            SimpleDateFormat dateFormatFin = new SimpleDateFormat(
                    " yyyy-MM-dd");

            Date dInicio = dateFormatInicio.parse(cabecera.getFechaInicio());
            Date dFin = dateFormatInicio.parse(cabecera.getFechaFin());

            if (Configuracion.EnumConfiguracion.SIMBOLO_MONEDA.getValor(context).contains("$")) {
                currency = "USD";
            }

            for (BeanEnvPedidoDet loBeanPedidoDet : cabecera.getListaDetPedido()
            ) {
                Bundle params = new Bundle();
                params.putString(FirebaseAnalytics.Param.ITEM_ID, loBeanPedidoDet.getCodigoArticulo());
                params.putString(FirebaseAnalytics.Param.ITEM_NAME, loBeanPedidoDet.getNombreArticulo());
                params.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, loBeanPedidoDet.getTipo());
                params.putLong(FirebaseAnalytics.Param.QUANTITY, (long) Double.parseDouble(loBeanPedidoDet.getCantidad()));
                params.putDouble(FirebaseAnalytics.Param.PRICE, Double.parseDouble(loBeanPedidoDet.getPrecioBase()));
                params.putDouble(FirebaseAnalytics.Param.VALUE, Double.parseDouble(loBeanPedidoDet.getMonto()));
                params.putString(FirebaseAnalytics.Param.CURRENCY, currency);
                params.putString(FirebaseAnalytics.Param.START_DATE, dateFormatFin.format(dInicio));
                params.putString(FirebaseAnalytics.Param.END_DATE, dateFormatFin.format(dFin));
                FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(context);
                analytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART, params);
            }

        } catch (Exception ex) {
            Log.e("FireBase", "agregarProducto er:" + ex.getMessage());
        }
    }

    public static void registrarPedido(Context context, BeanEnvPedidoCab cabecera) {
        try {
            String currency = "PEN";
            SimpleDateFormat dateFormatInicio = new SimpleDateFormat(
                    "dd/MM/yyyy kk:mm:ss");
            SimpleDateFormat dateFormatFin = new SimpleDateFormat(
                    " yyyy-MM-dd");

            Date dInicio = dateFormatInicio.parse(cabecera.getFechaInicio());
            Date dFin = dateFormatInicio.parse(cabecera.getFechaFin());

            if (Configuracion.EnumConfiguracion.SIMBOLO_MONEDA.getValor(context).contains("$")) {
                currency = "USD";
            }

            Bundle params = new Bundle();
            params.putString(FirebaseAnalytics.Param.TRANSACTION_ID, cabecera.getCodigoPedido());
            params.putDouble(FirebaseAnalytics.Param.VALUE, Double.parseDouble(cabecera.getMontoTotal()));
            params.putString(FirebaseAnalytics.Param.CURRENCY, currency);
            params.putString(FirebaseAnalytics.Param.START_DATE, dateFormatFin.format(dInicio));
            params.putString(FirebaseAnalytics.Param.END_DATE, dateFormatFin.format(dFin));

            FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(context);
            analytics.logEvent(FirebaseAnalytics.Event.ECOMMERCE_PURCHASE, params);

            registrarProducto(context, cabecera);

        } catch (Exception ex) {
            Log.e("FireBase", "registrarPedido er:" + ex.getMessage());
        }
    }

    public static void logueo(Context context, String metodo) {
        try {

            Bundle params = new Bundle();
            params.putString(FirebaseAnalytics.Param.SIGN_UP_METHOD, metodo);
            FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(context);
            analytics.logEvent(FirebaseAnalytics.Event.LOGIN, params);
        } catch (Exception ex) {
            Log.e("FireBase", "logueo er:" + ex.getMessage());
        }
    }

    public static void seleccionarAccion(Context context, String tipo, String id) {
        try {

            Bundle params = new Bundle();
            params.putString(FirebaseAnalytics.Param.CONTENT_TYPE, tipo);
            params.putString(FirebaseAnalytics.Param.ITEM_ID, id);
            FirebaseAnalytics analytics = FirebaseAnalytics.getInstance(context);
            analytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, params);
        } catch (Exception ex) {
            Log.e("FireBase", "seleccionarAccion er:" + ex.getMessage());
        }
    }
}
