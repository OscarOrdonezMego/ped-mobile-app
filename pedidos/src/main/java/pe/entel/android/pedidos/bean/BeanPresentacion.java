package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.util.Configuracion;

/**
 * Created by Dsb Mobile on 13/05/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanPresentacion extends BeanArticulo {

    @JsonProperty
    private long cantidad;
    @JsonProperty
    private long unidadFraccionamiento;

    BeanProducto producto;

    List<BeanFraccionamiento> lstBeanFraccionamiento;

    public BeanPresentacion() {
        super(Configuracion.TipoArticulo.PRESENTACION);
    }

    public double fnCalcularMontoFraccionamiento(long plCantFrac) {

        return plCantFrac * Double.parseDouble(getProducto().getPrecioBase());


    }


    public void fnObtenerLstFraccionamiento(int psNumeroDecimales) {
        lstBeanFraccionamiento = new ArrayList<BeanFraccionamiento>();

        long liCantMultiplos = cantidad / unidadFraccionamiento;

        for (int i = 0; i < liCantMultiplos; i++) {
            BeanFraccionamiento loBeanFraccionamiento = new BeanFraccionamiento();
            loBeanFraccionamiento.setNumerador(i * unidadFraccionamiento);
            loBeanFraccionamiento.setDenomindador(cantidad);
            loBeanFraccionamiento.fnActualizarDivision();
            loBeanFraccionamiento.setMostrarDivision(Utilitario
                    .fnRound(loBeanFraccionamiento.getDivision(), psNumeroDecimales));
            loBeanFraccionamiento.setUnidadDeferecto(producto.getUnidadDefecto());
            lstBeanFraccionamiento.add(loBeanFraccionamiento);

        }
    }

    public BeanFraccionamiento fnObtenerBeanFraccionamiento(double pdFraccion, int psNumeroDecimales) {
        BeanFraccionamiento loBeanFraccionamiento = new BeanFraccionamiento();
        loBeanFraccionamiento.setNumerador((long) (pdFraccion * cantidad));
        loBeanFraccionamiento.setDenomindador(cantidad);
        loBeanFraccionamiento.setDivision(pdFraccion);
        loBeanFraccionamiento.setMostrarDivision(Utilitario
                .fnRound(loBeanFraccionamiento.getDivision(), psNumeroDecimales));
        loBeanFraccionamiento.setUnidadDeferecto(producto.getUnidadDefecto());

        return loBeanFraccionamiento;
    }

    public double fnFraccMinimo() {
        if (cantidad == 0) return 0.0;
        return (double) unidadFraccionamiento / cantidad;
    }

    public BeanFraccionamiento fnObtenerFraccionamientoPorNumerador(String psNumerador) {
        BeanFraccionamiento loBeanFraccionamiento = null;
        for (BeanFraccionamiento loBeanFraccionamientoAux : lstBeanFraccionamiento)
            if (String.valueOf(loBeanFraccionamientoAux.getNumerador()).equals(psNumerador)) {
                loBeanFraccionamiento = loBeanFraccionamientoAux;
                break;
            }
        return loBeanFraccionamiento;

    }


    public String fnMostrarPresentacion() {
        return this.getNombre() + " - " + this.cantidad + " " + this.producto.getUnidadDefecto();
    }

    @Override
    public String fnMostrarStock() {
        String lsStock = "";
        double ldStock = 0;
        try {
            ldStock = Double.parseDouble(this.getStock());
        } catch (Exception e) {
            ldStock = 0;
        }
        long llStock = (long) ldStock / this.getCantidad();
        long llUnidades = (long) ldStock % this.getCantidad();
        lsStock = Utilitario.fnRound(llStock, 0) + " - " + Utilitario.fnRound(llUnidades, 0) +
                " " + this.getProducto().getUnidadDefecto();

        return lsStock;
    }


    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }

    public long getUnidadFraccionamiento() {
        return unidadFraccionamiento;
    }

    public void setUnidadFraccionamiento(long unidadFraccionamiento) {
        this.unidadFraccionamiento = unidadFraccionamiento;
    }

    public BeanProducto getProducto() {
        return producto;
    }

    public void setProducto(BeanProducto producto) {
        this.producto = producto;
    }

    public List<BeanFraccionamiento> getLstBeanFraccionamiento() {
        return lstBeanFraccionamiento;
    }

    public void setLstBeanFraccionamiento(List<BeanFraccionamiento> lstBeanFraccionamiento) {
        this.lstBeanFraccionamiento = lstBeanFraccionamiento;
    }
}
