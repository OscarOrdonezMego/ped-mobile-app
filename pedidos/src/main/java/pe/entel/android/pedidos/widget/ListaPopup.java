package pe.entel.android.pedidos.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.adap.AdapPopupBeanAlmacen;
import pe.entel.android.pedidos.adap.AdapPopupBeanFraccionamiento;
import pe.entel.android.pedidos.adap.AdapPopupBeanGeneral;
import pe.entel.android.pedidos.bean.BeanAlmacen;
import pe.entel.android.pedidos.bean.BeanFraccionamiento;
import pe.entel.android.pedidos.bean.BeanGeneral;

public class ListaPopup {

    private Dialog ioDialog;
    private Context ioContext;
    private ImageView ioImgLogo;
    private TextView ioTitulo;
    private ListView loList;

    OnItemClickPopup ioListener;

    public void dialog(Context poContext, List<BeanGeneral> paLista, final int piTipo,
                       OnItemClickPopup poListener, String psTitulo, BeanGeneral poGeneral,
                       int piIcono) {

        ioContext = poContext;
        ioListener = poListener;

        ioDialog = new Dialog(ioContext, R.style.NextelThemeDialog);
        ioDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ioDialog.setCancelable(true);
        ioDialog.setContentView(R.layout.lista_popup);

        loList = (ListView) ioDialog.findViewById(R.id.listapopup_lstLista);
        ioTitulo = (TextView) ioDialog.findViewById(R.id.listapopup_txtTitulo);
        ioImgLogo = (ImageView) ioDialog.findViewById(R.id.listapopup_imgLogo);

        ioTitulo.setText(psTitulo);

        TypedArray ta = ioContext.obtainStyledAttributes(new int[]{piIcono});
        ioImgLogo.setImageResource(ta.getResourceId(0, 0));
        ta.recycle();

        AdapPopupBeanGeneral adapter = new AdapPopupBeanGeneral(ioContext, R.layout.lista_popup_item,
                paLista, poGeneral);

        loList.setAdapter(adapter);

        loList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                if (ioListener != null)
                    ioListener.setOnItemClickPopup(
                            (BeanGeneral) loList.getItemAtPosition(arg2),
                            piTipo);

                ioDialog.dismiss();

            }
        });

        ioDialog.show();
    }

    public void dialog(Context poContext, List<BeanAlmacen> paLista, final int piTipo,
                       OnItemClickPopup poListener, String psTitulo, BeanAlmacen poAlmacen,
                       int piIcono) {

        ioContext = poContext;
        ioListener = poListener;

        ioDialog = new Dialog(ioContext, R.style.NextelThemeDialog);
        ioDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ioDialog.setCancelable(true);
        ioDialog.setContentView(R.layout.lista_popup);

        loList = (ListView) ioDialog.findViewById(R.id.listapopup_lstLista);
        ioTitulo = (TextView) ioDialog.findViewById(R.id.listapopup_txtTitulo);
        ioImgLogo = (ImageView) ioDialog.findViewById(R.id.listapopup_imgLogo);

        ioTitulo.setText(psTitulo);

        TypedArray ta = ioContext.obtainStyledAttributes(new int[]{piIcono});
        ioImgLogo.setImageResource(ta.getResourceId(0, 0));
        ta.recycle();

        AdapPopupBeanAlmacen adapter = new AdapPopupBeanAlmacen(ioContext,
                R.layout.lista_popup_item, paLista, poAlmacen);

        loList.setAdapter(adapter);

        loList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                if (ioListener != null)
                    ioListener.setOnItemClickPopup(
                            loList.getItemAtPosition(arg2),
                            piTipo);

                ioDialog.dismiss();

            }
        });

        ioDialog.show();
    }

    public void dialog(Context poContext, List<BeanFraccionamiento> poLstBeanFraccionamiento, final int piTipo,
                       OnItemClickPopup poListener, String psTitulo, BeanFraccionamiento poBeanFraccionamiento,
                       int piIcono) {

        ioContext = poContext;
        ioListener = poListener;

        ioDialog = new Dialog(ioContext, R.style.NextelThemeDialog);
        ioDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ioDialog.setCancelable(true);
        ioDialog.setContentView(R.layout.lista_popup);

        loList = (ListView) ioDialog.findViewById(R.id.listapopup_lstLista);
        ioTitulo = (TextView) ioDialog.findViewById(R.id.listapopup_txtTitulo);
        ioImgLogo = (ImageView) ioDialog.findViewById(R.id.listapopup_imgLogo);

        ioTitulo.setText(psTitulo);

        TypedArray ta = ioContext.obtainStyledAttributes(new int[]{piIcono});
        ioImgLogo.setImageResource(ta.getResourceId(0, 0));
        ta.recycle();

        AdapPopupBeanFraccionamiento adapter = new AdapPopupBeanFraccionamiento(ioContext,
                R.layout.lista_popup_item, poLstBeanFraccionamiento, poBeanFraccionamiento);

        loList.setAdapter(adapter);

        loList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                if (ioListener != null)
                    ioListener.setOnItemClickPopup(
                            loList.getItemAtPosition(arg2),
                            piTipo);

                ioDialog.dismiss();

            }
        });

        ioDialog.show();
    }


    public interface OnItemClickPopup {
        public void setOnItemClickPopup(Object poObject, int piTipo);

    }
}
