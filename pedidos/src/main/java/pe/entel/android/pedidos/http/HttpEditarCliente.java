package pe.entel.android.pedidos.http;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.Utilitario;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanProspecto;
import pe.entel.android.pedidos.util.Configuracion;
import pe.entel.android.pedidos.util.UtilitarioData;

public class HttpEditarCliente extends HttpConexion {

    private BeanProspecto ioBeanProspecto;
    private Context ioContext;

    public HttpEditarCliente(BeanProspecto beanProspecto, Context poContext, ConfiguracionNextel.EnumTipActividad enumTipActividad) {
        super(poContext, poContext.getString(R.string.httpeditarcliente_dlg), enumTipActividad);
        ioBeanProspecto = beanProspecto;
        ioContext = poContext;
    }

    @Override
    public boolean OnPreConnect() {
        SharedPreferences loSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ioContext);
        if (loSharedPreferences.getBoolean(ioContext.getString(R.string.keypre_cobertura), false)) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_modofueracobertura));
            return false;
        }

        int signal = UtilitarioData.fnSignalLevel(ioContext);
        if (!Utilitario.fnVerSignal(ioContext) || signal < 2) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, ioContext.getString(R.string.msg_fueracobertura));
            return false;
        }
        return true;
    }

    @Override
    public void OnConnect() {
        try {
            Log.v("URL", Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.EDITARDATOSCLIENTE));
            Log.v("REQUEST", BeanMapper.toJson(ioBeanProspecto, false));

            //String lsResultado = restTemplate.postForObject(Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.EDITARDATOSCLIENTE), ioBeanProspecto, String.class);
            String lsResultado = new HttpHelper().postJson(Configuracion.fnUrl(ioContext, Configuracion.EnumUrl.EDITARDATOSCLIENTE), ioBeanProspecto);
            setHttpResponseObject(lsResultado);

        } catch (Exception e) {
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, "Error HTTP: " + e.getMessage());
        }
    }

    @Override
    public void OnPostConnect() {
        if (getHttpResponseObject() != null) {
            String lsResultado = (String) getHttpResponseObject();
            String[] laRespuesta = lsResultado.split(";");
            int liIdHttpRespuesta = Integer.valueOf(laRespuesta[0]);
            String lsHttpRespuesta = laRespuesta[1];
            setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.get(liIdHttpRespuesta), lsHttpRespuesta);
        }
    }
}