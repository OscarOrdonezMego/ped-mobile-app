package pe.entel.android.pedidos.message;

import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import pe.entel.android.pedidos.R;

public class PedidosFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d("FBM", "token " + FirebaseInstanceId.getInstance().getToken());

        String from = remoteMessage.getFrom();
        Log.d("FBM", "Mensaje recibido de : " + from);

        if (remoteMessage.getNotification() != null) {
            Log.d("FBM", "Notificación : " + remoteMessage.getNotification().getBody());
            mostrarNotificacion(remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
        }

        if (remoteMessage.getData().size() > 0) {
            Log.d("FBM", "Data : " + remoteMessage.getData());
        }
    }

    private void mostrarNotificacion(String title, String body) {
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        long[] liVibrador = new long[50];
        for (int i = 0; i < liVibrador.length; i++) {
            if (i % 2 > 0) {
                liVibrador[i] = 1500;
            } else {
                liVibrador[i] = 800;
            }
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.pedidosicon)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setVibrate(liVibrador);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.d("NEW_TOKEN", token);
    }
}
