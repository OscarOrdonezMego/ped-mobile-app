package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanSucursal;

/**
 * Created by tkongr on 07/01/2016.
 */
public class AdapListaSucursales extends ArrayAdapter<BeanSucursal> {

    int resource;
    Context context;
    TextView rowTextView;

    ArrayList<BeanSucursal> originalList;

    int cont = 0;

    public AdapListaSucursales(Context _context, int _resource,
                               List<BeanSucursal> lista) {
        super(_context, _resource, lista);
        originalList = new ArrayList<BeanSucursal>();
        originalList.addAll(lista);
        resource = _resource;
        context = _context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BeanSucursal item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        ((TextView) nuevaVista
                .findViewById(R.id.actlistasucursaleslistaitem_textTitulo))
                .setText(item.getCodigo() + " - " + item.getNombre());
        ((TextView) nuevaVista
                .findViewById(R.id.actlistasucursaleslistaitem_textConexion))
                .setText(item.getUrl());

        return nuevaVista;
    }
}
