package pe.entel.android.pedidos.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pe.com.nextel.android.dal.DALGestor;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanEnvPago;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.util.Configuracion;

public class DalPago extends DALGestor {

    private Context ioContext;

    public DalPago(Context psClase) throws Exception {
        super(psClase, psClase.getString(R.string.db_name));
        ioContext = psClase;
    }

    public String fnGrabarPago(BeanEnvPago psBean) {
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String strId = String.valueOf(System.currentTimeMillis());

        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("INSERT INTO TrPago ("
                + "PAG_PK,COB_CODIGO,USR_CODIGO,PAG_MONTOPAGO,PAG_TIPOPAGO,PAG_CODIGOBANCO,PAG_VOUCHER,PAG_FECHADIFERIDA,Latitud,Longitud,Celda,FechaMovil,ErrorConexion,ErrorPosicion,FlgEnCobertura,FlgEnviado,"
                + "PAG_MONTOPAGOSOLES,PAG_MONTOPAGODOLARES,TM_SOLES,TM_DOLARES,TC_SOLES,TC_DOLARES) VALUES ('");
        loStbCab.append(strId);
        loStbCab.append("','");
        loStbCab.append(psBean.getIdDocCobranza());
        loStbCab.append("','");
        loStbCab.append(psBean.getIdUsuario());
        loStbCab.append("','");
        loStbCab.append(psBean.getPago());
        loStbCab.append("','");
        loStbCab.append(psBean.getIdFormaPago());
        loStbCab.append("','");
        loStbCab.append(psBean.getIdBanco());
        loStbCab.append("','");
        loStbCab.append(psBean.getNroDocumento());
        loStbCab.append("','");
        loStbCab.append(psBean.getFechaDiferida());
        loStbCab.append("','");
        loStbCab.append(psBean.getLatitud());
        loStbCab.append("','");
        loStbCab.append(psBean.getLongitud());
        loStbCab.append("','");
        loStbCab.append(psBean.getCelda());
        loStbCab.append("','");
        loStbCab.append(psBean.getFechaMovil());
        loStbCab.append("','");
        loStbCab.append(psBean.getErrorConexion());
        loStbCab.append("','");
        loStbCab.append(psBean.getErrorPosicion());
        loStbCab.append("','");
        loStbCab.append(psBean.getFlgEnCobertura());
        loStbCab.append("','");
        loStbCab.append(Configuracion.FLGREGNOHABILITADO);
        loStbCab.append("','");
        loStbCab.append(psBean.getPagosoles());
        loStbCab.append("','");
        loStbCab.append(psBean.getPagodolares());
        loStbCab.append("','");
        loStbCab.append(psBean.getTipomonedasoles());
        loStbCab.append("','");
        loStbCab.append(psBean.getTipomonedadolares());
        loStbCab.append("','");
        loStbCab.append(psBean.getIdtipocambiosoles());
        loStbCab.append("','");
        loStbCab.append(psBean.getIdtipocambiodolares());
        loStbCab.append("')");

        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            return "";
        }
        myDB.close();
        return strId;
    }

    public boolean fnUpdEstadoPago(String codPed) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrPago SET FlgEnviado='"
                + Configuracion.FLGREGHABILITADO + "' WHERE PAG_PK = " + codPed);
        Log.v("XXX", "actualizando..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    /**
     * Devuelve la cantidad de pagos pendientes de envio
     *
     * @param codVendedor codigo del vendedor
     * @return el numero de pagos pendientes
     */
    public int fnNumPagosPendientes(String codVendedor) throws Exception {
        StringBuilder loSbSqlCab = new StringBuilder();
        loSbSqlCab.append("SELECT count(*) ").append("FROM TrPago ")
                .append("WHERE FlgEnviado= ")
                .append(fnValueQuery(Configuracion.FLGREGNOHABILITADO))
                .append(" AND USR_CODIGO = ").append(fnValueQuery(codVendedor));

        getSQLQuery(loSbSqlCab.toString(), new RowListener() {

            @Override
            public void setRow(long piPosition, Cursor poCursor) {
                setObject(fnInteger(poCursor.getInt(0)));
            }
        });

        return (Integer) getObject();
    }

    public List<BeanEnvPago> fnEnvioPagosPendientes(String codVendedor) {
        List<BeanEnvPago> loPagosPendientes = new ArrayList<BeanEnvPago>();
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);

        String[] laColumnas = new String[]{"PAG_PK", "COB_CODIGO",
                "USR_CODIGO", "PAG_MONTOPAGO", "PAG_TIPOPAGO",
                "PAG_CODIGOBANCO", "PAG_VOUCHER", "Latitud", "Longitud",
                "Celda", "FechaMovil", "ErrorConexion", "ErrorPosicion",
                "FlgEnCobertura", "FlgEnviado", "PAG_FECHADIFERIDA",
                "PAG_MONTOPAGOSOLES", "PAG_MONTOPAGODOLARES",
                "TM_SOLES", "TM_DOLARES", "TC_SOLES", "TC_DOLARES"};
        String lsTablas = "TrPago";
        String lsWhere = "FlgEnviado='" + Configuracion.FLGREGNOHABILITADO
                + "' and USR_CODIGO = '" + codVendedor + "'";

        Cursor loCursor;
        loCursor = myDB.query(lsTablas, laColumnas, lsWhere, null, null, null,
                null);

        BeanEnvPago loBeanEnvPago = null;
        if (loCursor != null) {
            if (loCursor.moveToFirst()) {
                do {
                    loBeanEnvPago = new BeanEnvPago();
                    loBeanEnvPago.setCodPago(loCursor.getString(0));
                    loBeanEnvPago.setIdDocCobranza(loCursor.getString(1));
                    loBeanEnvPago.setIdUsuario(loCursor.getString(2));
                    loBeanEnvPago.setPago(loCursor.getString(3));
                    loBeanEnvPago.setIdFormaPago(loCursor.getString(4));
                    loBeanEnvPago.setIdBanco(loCursor.getString(5));
                    loBeanEnvPago.setNroDocumento(loCursor.getString(6));
                    loBeanEnvPago.setLatitud(loCursor.getString(7));
                    loBeanEnvPago.setLongitud(loCursor.getString(8));
                    loBeanEnvPago.setCelda(loCursor.getString(9));
                    loBeanEnvPago.setFechaMovil(loCursor.getString(10));
                    loBeanEnvPago.setErrorConexion(loCursor.getString(11));
                    loBeanEnvPago.setErrorPosicion(loCursor.getString(12));
                    loBeanEnvPago.setFlgEnCobertura(loCursor.getString(13));
                    loBeanEnvPago.setFechaDiferida(loCursor.getString(15));
                    loBeanEnvPago.setPagosoles(loCursor.getString(16));
                    loBeanEnvPago.setPagodolares(loCursor.getString(17));
                    loBeanEnvPago.setTipomonedasoles(loCursor.getString(18));
                    loBeanEnvPago.setTipomonedadolares(loCursor.getString(19));
                    loBeanEnvPago.setIdtipocambiosoles(loCursor.getString(20));
                    loBeanEnvPago.setIdtipocambiodolares(loCursor.getString(21));
                    loPagosPendientes.add(loBeanEnvPago);
                    loBeanEnvPago = null;

                } while (loCursor.moveToNext());
            }
        }

        loCursor.deactivate();
        loCursor.close();
        myDB.close();

        return loPagosPendientes;
    }

    public boolean fnUpdEstadoPagoPendiente(BeanUsuario piUsuario) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("UPDATE TrPago SET FlgEnviado='"
                + Configuracion.FLGREGHABILITADO + "'" + " WHERE FlgEnviado='"
                + Configuracion.FLGREGNOHABILITADO + "' and USR_CODIGO = '"
                + piUsuario.getCodVendedor() + "'");
        Log.v("XXX", "actualizando Pend..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());

        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnDelTrCabPedido() {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("delete from TrPago");
        Log.v("XXX", "eliminando cabecera..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }

    public boolean fnDelTrPago(String codPed) {
        boolean lbResult = true;
        SQLiteDatabase myDB = null;
        myDB = ioContext.openOrCreateDatabase(
                ioContext.getString(R.string.db_name),
                Context.MODE_PRIVATE, null);
        StringBuffer loStbCab = new StringBuffer();
        loStbCab.append("DELETE FROM TrPago WHERE PAG_PK = " + codPed);
        Log.v("XXX", "eliminando pago..." + loStbCab.toString());
        try {
            myDB.execSQL(loStbCab.toString());
        } catch (Exception e) {
            lbResult = false;
        }
        myDB.close();
        return lbResult;
    }
}