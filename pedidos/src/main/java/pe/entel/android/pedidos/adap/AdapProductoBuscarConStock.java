package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanArticulo;
import pe.entel.android.pedidos.bean.BeanPresentacion;
import pe.entel.android.pedidos.dal.DalAlmacen;
import pe.entel.android.pedidos.util.Configuracion;

public class AdapProductoBuscarConStock extends ArrayAdapter<BeanArticulo> {
    int resource;
    Context context;
    TextView rowTextView;
    ClienteFilter filter;

    List<BeanArticulo> originalList;
    List<BeanArticulo> filtroList;
    int cont = 0;

    String isPresentacion;
    String isMostrarAlmacen;

    public Filter getFilter(int tipo) {
        if (filter == null)
            filter = new ClienteFilter(tipo);
        else
            filter.setTipo(tipo);

        return filter;
    }

    public AdapProductoBuscarConStock(Context _context, int _resource,
                                      List<BeanArticulo> lista, String psPresentacion, String psMostrarAlmacen) {
        super(_context, _resource, lista);
        filtroList = new ArrayList<BeanArticulo>();
        filtroList.addAll(lista);
        originalList = new ArrayList<BeanArticulo>();
        originalList.addAll(lista);
        resource = _resource;
        context = _context;
        isPresentacion = psPresentacion;
        isMostrarAlmacen = psMostrarAlmacen;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        BeanArticulo item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        TextView txtTitulo = (TextView) nuevaVista
                .findViewById(R.id.actproductobuscaritem_textTitulo);

        TextView txtPresentacion = (TextView) nuevaVista.findViewById(R.id.actproductobuscaritem_textPresentacion);
        TextView txtProducto = (TextView) nuevaVista.findViewById(R.id.actproductobuscaritem_textProducto);
        DalAlmacen dalAlmacen = new DalAlmacen(context);
        boolean existeproductoalmacen = false;

        if (isPresentacion.equals(Configuracion.FLGVERDADERO)) {
            if (dalAlmacen.fnSelectAllPresentacion().size() > 0) {
                existeproductoalmacen = true;
            }
        } else if (dalAlmacen.fnSelectAllProducto().size() > 0) {
            existeproductoalmacen = true;
        }

        if (isPresentacion.equals(Configuracion.FLGVERDADERO) && item instanceof BeanPresentacion) {
            BeanPresentacion loBeanPresentacion = (BeanPresentacion) item;
            txtTitulo.setText(loBeanPresentacion.fnMostrarPresentacion());
            txtProducto.setText(loBeanPresentacion.getProducto().getTitulo());
            txtProducto.setVisibility(View.VISIBLE);
        } else {
            txtProducto.setVisibility(View.GONE);
            txtTitulo.setText(item.getTitulo());
        }

        if (isMostrarAlmacen.equals(Configuracion.FLGVERDADERO) && item.getAlmacen() != null && existeproductoalmacen) {
            txtPresentacion.setVisibility(View.VISIBLE);
            txtPresentacion.setText(getContext().getString(R.string.actproductobuscar_almacen)
                    + " " + item.getAlmacen().getNombre() + " Stock: " + item.fnMostrarStock());
        } else {
            txtPresentacion.setVisibility(View.GONE);
            txtTitulo.setText(txtTitulo.getText().toString() + " - Stock: " + item.fnMostrarStock());

        }

        return nuevaVista;
    }

    private class ClienteFilter extends Filter {

        int tipo;

        public ClienteFilter(int tipo) {
            this.tipo = tipo;
        }

        public void setTipo(int tipo) {
            this.tipo = tipo;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            // TODO Auto-generated method stub

            constraint = constraint.toString().toUpperCase();
            Log.i("constraint", (String) constraint);
            Log.i("tipo", String.valueOf(tipo));
            FilterResults result = new FilterResults();
            String cadenaComparar = constraint.toString();

            if (constraint != null && constraint.toString().length() > 0) {
                List<BeanArticulo> loLstFiltroBeanMostrarArticulo = new ArrayList<BeanArticulo>();

                String cadenaCompleta = "";

                for (BeanArticulo loBeanArticulo : originalList) {
                    String[] string = null;
                    if (isPresentacion.equals(Configuracion.FLGVERDADERO) && loBeanArticulo instanceof BeanPresentacion) {
                        cadenaCompleta = ((BeanPresentacion) loBeanArticulo).getProducto().getTitulo().toUpperCase();
                    } else {
                        cadenaCompleta = loBeanArticulo.getTitulo().toUpperCase();
                    }

                    StringTokenizer tokens = new StringTokenizer(cadenaCompleta, "\\-");
                    String codigo = "", titulo = "", precio = "";

                    String tokenBusqueda = "";

                    int cont = 0;
                    while (tokens.hasMoreTokens()) {
                        if (cont == 0) {
                            codigo = tokens.nextToken();
                        } else if (tokens.countTokens() == 1) {
                            precio = tokens.nextToken();
                        } else {
                            if (!titulo.equals(""))
                                titulo += "-";
                            titulo += tokens.nextToken();
                        }
                        cont++;
                    }

                    switch (tipo) {
                        case 0:
                            tokenBusqueda = codigo;
                            break;
                        case 1:
                            tokenBusqueda = titulo;
                            break;
                        case 2:
                            tokenBusqueda = precio;
                            break;
                    }

                    if (cadenaComparar.contains(" ") && !(cadenaComparar.endsWith(" "))) {

                        String[] listaTags = cadenaComparar.split(" ");
                        String expRegBusqueda = "";

                        for (int x = 0; x < listaTags.length; x++) {

                            expRegBusqueda += "(" + listaTags[x].toString() + ")+([\\-\\+\\(\\)\\:/Ñña-zA-Z0-9 .])+";
                        }

                        if (tokenBusqueda.trim().matches(expRegBusqueda)) {
                            loLstFiltroBeanMostrarArticulo.add(loBeanArticulo);
                        }
                    } else if (tokenBusqueda.trim().indexOf(constraint.toString()) != -1)
                        loLstFiltroBeanMostrarArticulo.add(loBeanArticulo);

                }

                final CharSequence finalConstraint = constraint;
                Collections.sort(loLstFiltroBeanMostrarArticulo, new Comparator<BeanArticulo>() {
                    @Override
                    public int compare(BeanArticulo poBeanArticulo1, BeanArticulo poBeanArticulo2) {
                        String lsArticulo1 = "";
                        String lsArticulo2 = "";
                        if (isPresentacion.equals(Configuracion.FLGVERDADERO) && poBeanArticulo1 instanceof BeanPresentacion) {
                            lsArticulo1 = ((BeanPresentacion) poBeanArticulo1).getProducto().getTitulo()
                                    .split("-")[tipo].toUpperCase().trim();
                            lsArticulo2 = ((BeanPresentacion) poBeanArticulo2).getProducto().getTitulo()
                                    .split("-")[tipo].toUpperCase().trim();
                        } else {
                            lsArticulo1 = poBeanArticulo1.getTitulo().split("-")[tipo].toUpperCase().trim();
                            lsArticulo2 = poBeanArticulo2.getTitulo().split("-")[tipo].toUpperCase().trim();
                        }

                        int liConstraint = finalConstraint.toString().length();

                        String lsFilterArticulo1 = "";
                        String lsFilterArticulo2 = "";
                        try {
                            lsFilterArticulo1 = lsArticulo1.substring(0, liConstraint);
                        } catch (Exception e) {
                            lsFilterArticulo1 = "";
                        }
                        try {
                            lsFilterArticulo2 = lsArticulo2.substring(0, liConstraint);
                        } catch (Exception e) {
                            lsFilterArticulo2 = "";
                        }
                        if (lsFilterArticulo1.equals(finalConstraint.toString())
                                && (lsFilterArticulo2.equals(finalConstraint.toString()))) {
                            return 0;
                        } else if (lsFilterArticulo1.equals(finalConstraint.toString())) {
                            return -2;
                        } else if (lsFilterArticulo2.equals(finalConstraint.toString())) {
                            return 2;
                        } else if (lsArticulo1.indexOf(finalConstraint.toString()) != -1
                                && lsArticulo2.indexOf(finalConstraint.toString()) != -1) {
                            return 0;
                        } else if (lsArticulo1.indexOf(finalConstraint.toString()) != -1) {
                            return -1;
                        } else if (lsArticulo2.indexOf(finalConstraint.toString()) != -1) {
                            return 1;
                        } else
                            return 0;
                    }
                });

                result.count = loLstFiltroBeanMostrarArticulo.size();
                result.values = loLstFiltroBeanMostrarArticulo;
            } else {
                synchronized (this) {
                    result.values = originalList;
                    result.count = originalList.size();
                }
            }
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            // TODO Auto-generated method stub
            filtroList = (List<BeanArticulo>) results.values;
            notifyDataSetChanged();
            clear();
            for (int i = 0, l = filtroList.size(); i < l; i++) {
                add(filtroList.get(i));
                notifyDataSetInvalidated();
            }
        }

    }

}
