package pe.entel.android.pedidos.util.pedidos;

import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesDownloaderReceiver;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.service.NServicesDownloadService;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexToast;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.act.ActGrabaPendiente;
import pe.entel.android.pedidos.act.ActListaPendientes;
import pe.entel.android.pedidos.bean.BeanCliente;
import pe.entel.android.pedidos.bean.BeanEnvPedidoCab;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.util.Configuracion;

/**
 * Created by tkongr on 17/03/2016.
 */
public class UtilitarioPedidos {

    public static void IndicarMensajeOkError(NexActivity activity) {
        activity.setMsgOk(activity.getString(R.string.actmenu_dlghttpok));
        activity.setMsgError(activity.getString(R.string.actmenu_dlghttperror));
    }

    public static void mostrarPendientesSegunConfiguracion(Context context) {
        if (Configuracion.EnumConfiguracion.VERIFICACION_PEDIDO.getValor(context).equals(Configuracion.FLGVERDADERO))
            context.startActivity(new Intent(context, ActListaPendientes.class));
        else
            context.startActivity(new Intent(context, ActGrabaPendiente.class));
    }

    public static void mostrarDialogo(Context context, int tagDialog) {
        NexActivity activity = (NexActivity) context;
        if (!activity.isUsingDialogFragment()) {
            activity.showDialog(tagDialog);
        } else {
            activity.showNexDialog(tagDialog);
        }
    }

    //Procedimiento para llamar al NServices. Crea Toasts o Diálogos de interacción con el usuario para la descarga
    public static void subCallNServices(Context context, DALNServices ioDALNservices, boolean fromSincro, boolean obligatorio) throws Exception {

        NexActivity activity = (NexActivity) context;

        Log.v("OK", "OK");
        if (!Utilitario.verifyPackageInstalled(context, NServicesConfiguration.NSERVICES_PACKAGE)) {
            if (ioDALNservices == null)
                ioDALNservices = DALNServices.getInstance(context, R.string.db_name);

            String lsError = ioDALNservices.fnSelNServicesError();
            if (lsError == null) {
                String lsAPK = ioDALNservices.fnSelNServicesAPK();
                if (lsAPK != null) {
                    if (obligatorio) {
                        String lsNServices;
                        try {
                            lsNServices = ioDALNservices.fnSelNServicesLabel();
                        } catch (Exception e) {
                            Log.e("XXX", "onCreateNexDialog.DIALOG_NSERVICES_DOWNLOAD", e);
                            lsNServices = activity.getString(R.string.nservices_label_default);
                        }
                        new NexToast(context, activity.getString(R.string.nservices_startingdownload).replace("{0}", lsNServices), NexToast.EnumType.INFORMATION).show();
                        subIniNServicesDownload(context, lsAPK);
                    } else {
                        UtilitarioPedidos.mostrarDialogo(context, Constantes.DIALOG_NSERVICES_DOWNLOAD);
                    }
                } else {
                    activity.subShowException(new Exception(activity.getString(R.string.actmenuvalsincro_mensaje)));
                }
            } else {
                activity.subShowException(new Exception(lsError));
            }
        } else if (fromSincro) {
            NServicesInstallerReceiver.sendBroadcast(context);
        } else {
            UtilitarioPedidos.subIniNServices((NexActivity) context);
        }
    }

    //Inicia el servicio de Descarga de APK
    public static void subIniNServicesDownload(Context context, String psAPK) throws Exception {
        new NServicesDownloaderReceiver().register(context);
        NServicesDownloadService.startService(context, psAPK, /*
                 * Asignan el estilo
                 * maestro de la app
                 */
                R.style.Theme_Pedidos_Master);
    }

    //Inicia la actividad del NServices
    public static void subIniNServices(NexActivity activity) {
        activity.startActivityForResult(NServicesConfiguration.createIntentForIniNServicesActivity(), Constantes.REQUEST_NSERVICES);
    }

    public static BeanUsuario obtenerBeanUsuarioDePreferences(Context context) {
        String lsBeanUsuario = context.getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, context.MODE_PRIVATE).getString("BeanUsuario", "");
        return (BeanUsuario) BeanMapper.fromJson(lsBeanUsuario, BeanUsuario.class);
    }

    public static BeanEnvPedidoCab obtenerBeanPedidoCabDePreferences(Context context) {
        String lsBean = context.getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, context.MODE_PRIVATE).getString("beanPedidoCab", "");
        return (BeanEnvPedidoCab) BeanMapper.fromJson(lsBean, BeanEnvPedidoCab.class);
    }

    public static BeanCliente obtenerBeanClienteOnlineDePreferences(Context context) {
        String lsBean = context.getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, context.MODE_PRIVATE).getString("BeanClienteOnline", "");
        return (BeanCliente) BeanMapper.fromJson(lsBean, BeanCliente.class);
    }

    public static void obtenerMenuLateral(final NexActivity activity, BeanUsuario beanUsuario, View.OnClickListener listener) {
        View _titulo = activity.getLayoutInflater().inflate(R.layout.menulateralslidetitulo, null);
        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario)).setText(beanUsuario.getNombre());

        View _salir = activity.getLayoutInflater().inflate(R.layout.menulateralsalir, null);
        Button btnSalir = (Button) _salir.findViewById(R.id.menulateral_btnsalir);

        ((TextView) _titulo.findViewById(R.id.menulateralslide_usuario)).setText(beanUsuario.getNombre());

        LinearLayout _layoutMenu = (LinearLayout) activity.findViewById(R.id.nexmenudrawerlayout_menulayout);
        _layoutMenu.addView(_titulo, 1);

        _layoutMenu.setBackgroundColor(activity.getResources().getColor(R.color.pedidos_menulateral_bg));
        DrawerLayout.LayoutParams _params = new DrawerLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        _params.gravity = Gravity.START;
        _layoutMenu.setLayoutParams(_params);
        _layoutMenu.addView(btnSalir);
        btnSalir.setOnClickListener(listener);
    }


    public static BeanEnvPedidoCab obtenerBeanPedidoCabPendienteDePreferences(Context context) {
        String lsBean = context.getSharedPreferences(ConfiguracionNextel.CONSPREFERENCIA, context.MODE_PRIVATE).getString("beanPedidoCabPendiente", "");
        return (BeanEnvPedidoCab) BeanMapper.fromJson(lsBean, BeanEnvPedidoCab.class);
    }
}
