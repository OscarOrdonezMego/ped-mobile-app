package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.bean.BeanUsuario;
import pe.entel.android.pedidos.util.Configuracion;

public class AdapMenuPrincipalSliding extends BaseAdapter {
    private Context _context;
    private TypedArray _drawables;
    private String[] _strings;
    private Boolean[] _enabled;
    private View[] _convertView;
    private MenuSlidingListener _listener;

    ArrayList<EnumMenuPrincipalSlidingItem> goArrItem = new ArrayList<AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem>();

    public AdapMenuPrincipalSliding(Context context,
                                    MenuSlidingListener listener, int enable) {
        this._context = context;
        this._drawables = context
                .obtainStyledAttributes(R.styleable.PedidosMenuLateralItemsDrawables);
        this._strings = context.getResources().getStringArray(
                R.array.menulateralitems_strings);
        int cant = EnumMenuPrincipalSlidingItem.values().length;
        this._enabled = new Boolean[cant];

        String lsBeanUsuario = context.getSharedPreferences(
                ConfiguracionNextel.CONSPREFERENCIA, context.MODE_PRIVATE)
                .getString("BeanUsuario", "");
        BeanUsuario loBeanUsuario = (BeanUsuario) BeanMapper.fromJson(
                lsBeanUsuario, BeanUsuario.class);

        goArrItem.add(EnumMenuPrincipalSlidingItem.HOME);
        goArrItem.add(EnumMenuPrincipalSlidingItem.INICIO);
        goArrItem.add(EnumMenuPrincipalSlidingItem.SINCRONIZAR);
        goArrItem.add(EnumMenuPrincipalSlidingItem.PENDIENTES);
        if (Configuracion.EnumConfiguracion.EFICIENCIA.getValor(context).equals(
                Configuracion.FLGVERDADERO))
            goArrItem.add(EnumMenuPrincipalSlidingItem.EFICIENCIA);

        if (Configuracion.EnumModulo.PEDIDO.getValor(context).equals(
                Configuracion.FLGVERDADERO))
            goArrItem.add(EnumMenuPrincipalSlidingItem.STOCK);

        if (Configuracion.EnumConfiguracion.SEGUIMIENTO_RUTA.getValor(context).equals(
                Configuracion.FLGVERDADERO))
            goArrItem.add(EnumMenuPrincipalSlidingItem.NSERVICES);

        goArrItem.add(EnumMenuPrincipalSlidingItem.PORTALAUTOAYUDA);
        for (int i = 0; i < cant; i++)
            this._enabled[i] = i != enable;

        this._convertView = new View[goArrItem.size()];
        this._listener = listener;

    }

    @Override
    public int getCount() {
        return goArrItem.size();
    }

    @Override
    public Object getItem(int position) {
        return goArrItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return goArrItem.get(position).ordinal();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = _convertView[position];
        if (convertView == null) {
            convertView = ((LayoutInflater) _context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.menulaterallista_item, null);
            _convertView[position] = convertView;
        }
        convertView
                .setTag(R.string.keytag_menuprincipalitem, getItem(position));

        TextView text = (TextView) convertView
                .findViewById(R.id.actmenulaterallistaitem_txtnombre);

        text.setCompoundDrawablesWithIntrinsicBounds(
                ((EnumMenuPrincipalSlidingItem) getItem(position))
                        .getImageResource(_drawables), 0, 0, 0);

        text.setText(((EnumMenuPrincipalSlidingItem) getItem(position))
                .getText(_strings));

        text.setEnabled(((EnumMenuPrincipalSlidingItem) getItem(position))
                .getEnabled(_enabled));

        convertView
                .setEnabled(((EnumMenuPrincipalSlidingItem) getItem(position))
                        .getEnabled(_enabled));

        return convertView;

    }

    private EnumMenuPrincipalSlidingItem _process = null;

    public void commitProcess() {
        _process = null;
    }

    public EnumMenuPrincipalSlidingItem getCurrentProcess() {
        return _process;
    }

    public void OnItemClick(int position) {
        if (_listener == null)
            new NexToast(
                    _context,
                    _context.getString(R.string.menuprincipalitems_faltalistener_slide),
                    EnumType.CANCEL).show();
        switch ((EnumMenuPrincipalSlidingItem) getItem(position)) {
            case INICIO:
                if (_listener != null)
                    _listener.subInicioFromSliding();
                break;
            case SINCRONIZAR:
                if (_listener != null)
                    _listener.subSincronizarFromSliding();
                break;
            case PENDIENTES:
                if (_listener != null)
                    _listener.subPendientesFromSliding();
                break;
            case EFICIENCIA:
                if (_listener != null)
                    _listener.subEficienciaFromSliding();
                break;
            case STOCK:
                if (_listener != null)
                    _listener.subStockFromSliding();
                break;
            case HOME:
                if (_listener != null)
                    _listener.subHomeFromSliding();
                break;
            case NSERVICES:
                if (_listener != null)
                    _listener.subNServicesFromSliding();
                break;
            case PORTALAUTOAYUDA:
                String url = Utilitario.readProperties(_context).getProperty("PORTAL_AUTOAYUDA");
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                _context.startActivity(i);
                break;
            default:

                break;
        }
        _process = (EnumMenuPrincipalSlidingItem) getItem(position);
    }

    public enum EnumMenuPrincipalSlidingItem {
        HOME, INICIO, SINCRONIZAR, PENDIENTES, EFICIENCIA, STOCK, NSERVICES, PORTALAUTOAYUDA;

        private String extraInfo = "";

        public int getImageResource(Context context) {
            return context.obtainStyledAttributes(
                    R.styleable.PedidosMenuLateralItemsDrawables)
                    .getResourceId(ordinal(), -1);
        }

        public int getImageResource(TypedArray array) {
            return array.getResourceId(ordinal(), -1);
        }

        public int getPosition() {
            return ordinal();
        }

        public String getText(Context context) {
            return context.getResources().getStringArray(
                    R.array.menulateralitems_strings)[ordinal()]
                    .concat(extraInfo);
        }

        public String getText(String[] array) {
            return array[ordinal()].concat(extraInfo);
        }

        public Boolean getEnabled(Boolean[] array) {
            return array[ordinal()];
        }

        public Boolean getVisibility(Boolean[] array) {
            return array[ordinal()];
        }

        public void setExtraInfo(String extraInfo) {
            this.extraInfo = extraInfo;
        }

    }

    public static interface MenuSlidingListener {

        public void subHomeFromSliding();

        public void subInicioFromSliding();

        public void subSincronizarFromSliding();

        public void subPendientesFromSliding();

        public void subEficienciaFromSliding();

        public void subStockFromSliding();

        public void subNServicesFromSliding();

    }

}
