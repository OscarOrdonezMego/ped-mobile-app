package pe.entel.android.pedidos.widget;

import android.content.Context;
import android.util.AttributeSet;

import pe.com.nextel.android.widget.NexKeyboardView;

/**
 * Created by rtamayov on 21/09/2015.
 */
public class NexKeyboardViewPedidos extends NexKeyboardView {
    public NexKeyboardViewPedidos(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
