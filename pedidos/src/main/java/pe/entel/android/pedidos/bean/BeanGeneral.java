package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanGeneral {

    @JsonProperty
    private String codigo = "";
    @JsonProperty
    private String descripcion = "";
    @JsonProperty
    private String extra = "";
    @JsonProperty
    private String flg_banco = "";
    @JsonProperty
    private String flg_nrodocumento = "";
    @JsonProperty
    private String flg_fechadiferida = "";

    public String getFlg_fechadiferida() {
        return flg_fechadiferida;
    }

    public void setFlg_fechadiferida(String flg_fechadiferida) {
        this.flg_fechadiferida = flg_fechadiferida;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFlg_banco() {
        return flg_banco;
    }

    public void setFlg_banco(String flg_banco) {
        this.flg_banco = flg_banco;
    }

    public String getFlg_nrodocumento() {
        return flg_nrodocumento;
    }

    public void setFlg_nrodocumento(String flg_nrodocumento) {
        this.flg_nrodocumento = flg_nrodocumento;
    }

    public BeanGeneral() {

    }

    public BeanGeneral(String codigo, String descripcion) {
        setCodigo(codigo);
        setDescripcion(descripcion);
    }

    public BeanGeneral(String codigo, String descripcion, String extra) {
        setCodigo(codigo);
        setDescripcion(descripcion);
        setExtra(extra);
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
