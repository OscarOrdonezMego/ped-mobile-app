package pe.entel.android.pedidos.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Dsb Mobile on 10/06/2015.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanConsultaProducto {

    @JsonProperty("CodigoUsuario")
    private String codigoUsuario = "";
    @JsonProperty("Fecha")
    private String fecha = "";
    @JsonProperty("CodigoCliente")
    private String codigoCliente = "";
    @JsonProperty("CodigoArticulo")
    private String codigoArticulo = "";
    @JsonProperty("CondicionVenta")
    private String condicionVenta = "";
    @JsonProperty("Direccion")
    private String direccion = "";
    @JsonProperty("DireccionDespacho")
    private String direccionDespacho = "";
    @JsonProperty("CodigoAlmacen")
    private String codigoAlmacen = "";
    @JsonProperty("Cantidad")
    private String cantidad = "";
    @JsonProperty("CantidadPresentacion")
    private long cantidadPresentacion = 0;
    @JsonProperty("Descripcion")
    private String descripcion = "";
    @JsonProperty("Descuento")
    private String descuento = "";
    @JsonProperty("DescuentoFormaPago")
    private String descuentoFormaPago = "";
    @JsonProperty("FechaUltimaVenta")
    private String fechaUltimaVenta = "";
    @JsonProperty("Flete")
    private String flete = "";
    @JsonProperty("Precio")
    private String precio = "";
    //@JBELVY I
    @JsonProperty("PrecioSoles")
    private String precioSoles = "";
    @JsonProperty("PrecioDolares")
    private String precioDolares = "";
    //@JBELVY F
    @JsonProperty("PrecioUltimaVenta")
    private String precioUltimaVenta = "";
    //@JBELVY I
    @JsonProperty("PrecioUltimaVentaSoles")
    private String precioUltimaVentaSoles = "";
    @JsonProperty("PrecioUltimaVentaDolares")
    private String precioUltimaVentaDolares = "";
    //@JBELVY F
    @JsonProperty("Stock")
    private String stock = "";
    @JsonProperty("TipoDescuento")
    private String tipoDescuento = "";

    public String getCodigoUsuario() {
        return codigoUsuario;
    }

    public void setCodigoUsuario(String codigoUsuario) {
        this.codigoUsuario = codigoUsuario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(String codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getCodigoArticulo() {
        return codigoArticulo;
    }

    public void setCodigoArticulo(String codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }

    public String getCondicionVenta() {
        return condicionVenta;
    }

    public void setCondicionVenta(String condicionVenta) {
        this.condicionVenta = condicionVenta;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDireccionDespacho() {
        return direccionDespacho;
    }

    public void setDireccionDespacho(String direccionDespacho) {
        this.direccionDespacho = direccionDespacho;
    }

    public String getCodigoAlmacen() {
        return codigoAlmacen;
    }

    public void setCodigoAlmacen(String codigoAlmacen) {
        this.codigoAlmacen = codigoAlmacen;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public long getCantidadPresentacion() {
        return cantidadPresentacion;
    }

    public void setCantidadPresentacion(long cantidadPresentacion) {
        this.cantidadPresentacion = cantidadPresentacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getDescuentoFormaPago() {
        return descuentoFormaPago;
    }

    public void setDescuentoFormaPago(String descuentoFormaPago) {
        this.descuentoFormaPago = descuentoFormaPago;
    }

    public String getFechaUltimaVenta() {
        return fechaUltimaVenta;
    }

    public void setFechaUltimaVenta(String fechaUltimaVenta) {
        this.fechaUltimaVenta = fechaUltimaVenta;
    }

    public String getFlete() {
        return flete;
    }

    public void setFlete(String flete) {
        this.flete = flete;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }
    //@JBELVY I
    public String getPrecioSoles() {
        return precioSoles;
    }

    public void setPrecioSoles(String precioSoles) {
        this.precioSoles = precioSoles;
    }

    public String getPrecioDolares() {
        return precioDolares;
    }

    public void setPrecioDolares(String precioDolares) {
        this.precioDolares = precioDolares;
    }
    //@JBELVY F
    public String getPrecioUltimaVenta() {
        return precioUltimaVenta;
    }

    public void setPrecioUltimaVenta(String precioUltimaVenta) {
        this.precioUltimaVenta = precioUltimaVenta;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getTipoDescuento() {
        return tipoDescuento;
    }

    public void setTipoDescuento(String tipoDescuento) {
        this.tipoDescuento = tipoDescuento;
    }
}
