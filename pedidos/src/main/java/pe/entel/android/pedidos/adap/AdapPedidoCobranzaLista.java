package pe.entel.android.pedidos.adap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import pe.entel.android.pedidos.R;
import pe.entel.android.pedidos.widget.HeadedTextItem;

public class AdapPedidoCobranzaLista extends ArrayAdapter<HeadedTextItem> {
    int resource;
    Context context;
    TextView rowTextView;

    ArrayList<HeadedTextItem> originalList;

    int cont = 0;

    public AdapPedidoCobranzaLista(Context _context, int _resource,
                                   ArrayList<HeadedTextItem> lista) {
        super(_context, _resource, lista);
        originalList = new ArrayList<HeadedTextItem>();
        originalList.addAll(lista);
        resource = _resource;
        context = _context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        HeadedTextItem item = getItem(position);
        LinearLayout nuevaVista;

        if (convertView == null) {
            nuevaVista = new LinearLayout(getContext());
            inflater.inflate(resource, nuevaVista, true);

        } else {
            nuevaVista = (LinearLayout) convertView;
        }

        ((TextView) nuevaVista
                .findViewById(R.id.actpedidocobranzalistaitem_textTitulo))
                .setText(item.getText());

        return nuevaVista;
    }

}
