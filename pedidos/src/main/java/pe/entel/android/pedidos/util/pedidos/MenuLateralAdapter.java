package pe.entel.android.pedidos.util.pedidos;

import android.content.Context;
import android.widget.ListAdapter;

import pe.com.nextel.android.widget.NexMenuDrawerLayout;
import pe.entel.android.pedidos.adap.AdapMenuPrincipalSliding;

/**
 * Created by tkongr on 21/03/2016.
 */
public class MenuLateralAdapter implements NexMenuDrawerLayout.NexMenuCallbacks {

    AdapMenuPrincipalSliding adapter;
    Context context;
    AdapMenuPrincipalSliding.MenuSlidingListener listener;

    public MenuLateralAdapter(Context context, AdapMenuPrincipalSliding adapter, AdapMenuPrincipalSliding.MenuSlidingListener listener) {
        this.adapter = adapter;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public ListAdapter IniNexMenuAdapter() {
        if (adapter == null) {
            adapter = new AdapMenuPrincipalSliding(context, listener, AdapMenuPrincipalSliding.EnumMenuPrincipalSlidingItem.PENDIENTES.getPosition());
        }
        return adapter;
    }

    @Override
    public void OnNexMenuItemClick(int i) {
        adapter.OnItemClick(i);
    }
}
