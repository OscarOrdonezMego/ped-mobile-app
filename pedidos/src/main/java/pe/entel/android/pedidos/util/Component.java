package pe.entel.android.pedidos.util;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import pe.entel.android.pedidos.R;

public class Component {
    public static final String DEFAULT_CHANNEL_ID = "DEFAULT";
    public static final String FOTO_CHANNEL_ID = "FOTO";

    public void notificationCompatCustom(Context context, String mensaje, String canal) {
        Integer idNotificacion = 0;
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context, canal)
                        .setSmallIcon(R.drawable.pedidosicon)
                        .setContentTitle("Actividades de Campo")
                        .setContentText(mensaje);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(idNotificacion++, mBuilder.build());
    }

    public void toastCustomError(Context context, String mensaje) {
        toastCustom(context, mensaje, Toast.LENGTH_LONG);
    }

    public void toastCustomInfo(Context context, String mensaje) {
        toastCustom(context, mensaje, Toast.LENGTH_SHORT);
    }

    public void toastCustomHandlerError(final Context context, final String mensaje) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                toastCustomError(context, mensaje);
            }
        });
    }

    public void toastCustomHandlerInfo(final Context context, final String mensaje) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                toastCustomInfo(context, mensaje);
            }
        });
    }

    private void toastCustom(Context context, String mensaje, int toastLength) {
        Toast.makeText(context, mensaje, toastLength).show();
    }

    /*public void snackbarCustom(View view, String mensaje) {
        Snackbar.make(view, mensaje, Snackbar.LENGTH_LONG).show();
    }*/
}
