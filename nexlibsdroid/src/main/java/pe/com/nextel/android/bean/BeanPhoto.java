package pe.com.nextel.android.bean;


import android.content.Context;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import pe.com.nextel.android.util.NexDataOutputStream;

/**
 * Clase que hereda de {@link BeanMapper}</br>
 * Contiene la informaci&#243;n de la toma de foto.
 *
 * @author jroeder
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BeanPhoto extends BeanMapper {
    @JsonIgnore
    private NexPhotoListener NEXPHOTOLISTENER = null;
    @JsonIgnore
    private String URL = null;

    /**
     * Instancia el listener de fotos
     *
     * @param listener - Listener de fotos
     */
    @JsonIgnore
    public void setNexPhotoListener(NexPhotoListener listener) {
        this.NEXPHOTOLISTENER = listener;
    }

    /**
     * Retorna el listener de fotos
     *
     * @return NexPhotoListener
     */
    @JsonIgnore
    public NexPhotoListener getNexPhotoListener() {
        return this.NEXPHOTOLISTENER;
    }

    /**
     * Instancia la URL de env&#237;o de fotos
     *
     * @param url - URL de env&#237;o de fotos
     */
    @JsonIgnore
    public void setURL(String url) {
        this.URL = url;
    }

    /**
     * Retorna la URL de env&#237;o de fotos
     *
     * @return String
     */
    @JsonIgnore
    public String getURL() {
        return this.URL;
    }

    /**
     * Inicializa el listener de fotos
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     */
    @JsonIgnore
    public abstract NexPhotoListener initializePhotoListener(final Context poContext);

    /**
     * Inicializa la URL de env&#237;o de fotos
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @return String
     */
    @JsonIgnore
    public abstract String initializeURL(final Context poContext);

    @JsonProperty
    private long idDate;
    @JsonProperty
    private short order;

    /**
     * Retorna el Id de fecha de la toma de foto
     *
     * @return long
     */
    public long getIdDate() {
        return idDate;
    }

    /**
     * Instancia el Id de fecha de la toma de foto
     *
     * @param idDate
     */
    public void setIdDate(long idDate) {
        this.idDate = idDate;
    }

    /**
     * Retorna el n&#250;mero de orden de la foto
     *
     * @return short
     */
    public short getOrder() {
        return order;
    }

    /**
     * Instancia el n&#250;mero de orden de la foto
     *
     * @param order - N&#250;mero de orden de foto
     */
    public void setOrder(short order) {
        this.order = order;
    }

    /**
     * Clase interfaz que implementa la escritura y la obtenci&#243;n de la ruta y nombre de la foto
     *
     * @author jroeder
     */
    public static interface NexPhotoListener {
        /**
         * Escribe los datos del {@link BeanPhoto} en un {@link NexDataOutputStream}
         *
         * @param poOutput    - {@link NexDataOutputStream}
         * @param poBeanPhoto - Bean de la toma foto
         */
        public void subWriteHttp(NexDataOutputStream poOutput, BeanPhoto poBeanPhoto) throws Exception;

        /**
         * Retorna el la ruta y nombre de la toma de foto
         *
         * @param poBeanPhoto Objeto bean de la toma de foto
         * @return String
         */
        public String fnPhotoName(BeanPhoto poBeanPhoto) throws Exception;
    }
}