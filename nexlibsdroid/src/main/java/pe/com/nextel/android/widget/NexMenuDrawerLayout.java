package pe.com.nextel.android.widget;

import android.content.Context;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import pe.com.nextel.android.R;

/**
 * Clase que contiene un {@link FrameLayout} como contenido de la actividad y un {@link ListView}
 * como men&#250; deslizable ubicado en la parte izquieda de la pantalla de la actividad.
 *
 * @author jroeder
 */
public class NexMenuDrawerLayout extends DrawerLayout {
    private NexMenuCallbacks callbacks;

    /**
     * Constructor de la clase
     *
     * @param context - Contexto de la aplicaci&#243;n
     */
    public NexMenuDrawerLayout(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }

    /**
     * Constructor de la clase
     *
     * @param context - Contexto de la aplicaci&#243;n
     * @param attrs   - Configuraci&#243;n de atributos.
     */
    public NexMenuDrawerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    /**
     * Constructor de la clase
     *
     * @param context  - Contexto de la aplicaci&#243;n
     * @param attrs    - Configuraci&#243;n de atributos
     * @param defStyle - Estilo
     */
    public NexMenuDrawerLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
    }

    /**
     * Instancia la interfaz para inicializar el Men&#250;
     *
     * @param callbacks - Interfaz {@link NexMenuCallbacks}
     */
    public void setNexMenuCallbacks(NexMenuCallbacks callbacks) throws Exception {
        this.callbacks = callbacks;
//		this.callbacks.subIniNexMenu(getMenuList());
        getMenuList().setAdapter(this.callbacks.IniNexMenuAdapter());
        getMenuList().setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View convertView, int position,
                                    long id) {
                NexMenuDrawerLayout.this.callbacks.OnNexMenuItemClick(position);
                if (Build.VERSION.SDK_INT >= 14) {
                    if (isDrawerVisible(Gravity.START))
                        closeDrawer(Gravity.START);
                } else {
                    if (isDrawerVisible(GravityCompat.START))
                        closeDrawer(GravityCompat.START);
                }
            }

        });
    }

    /**
     * Instancia la interfaz para inicializar el men&#250;.
     *
     * @param resourceId - Id de recurso Layout que se adjunta al {@link FrameLayout} de contenido
     * @param callbacks  - Interfaz NexMenuCallbacks
     */
    public void setNexMenuCallbacks(int resourceId, NexMenuCallbacks callbacks) throws Exception {
        setLayout(resourceId);
        subCrearDrawerMenu(callbacks);
    }

    /**
     * Instancia la interfaz para inicializar el men&#250;.
     *
     * @param poView      - View que se adjunta al {@link FrameLayout} de contenido
     * @param poCallbacks - Interfaz NexMenuCallbacks
     */
    public void setNexMenuCallbacks(View poView, NexMenuCallbacks poCallbacks) throws Exception {
        setLayout(poView);
        subCrearDrawerMenu(poCallbacks);
    }

    private void subCrearDrawerMenu(NexMenuCallbacks poCallbacks) {
        this.callbacks = poCallbacks;
//		this.callbacks.subIniNexMenu(getMenuList());
        getMenuList().setAdapter(this.callbacks.IniNexMenuAdapter());
        getMenuList().setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapterView, View convertView, int position,
                                    long id) {
                NexMenuDrawerLayout.this.callbacks.OnNexMenuItemClick(position);
                if (Build.VERSION.SDK_INT >= 14) {
                    if (isDrawerVisible(Gravity.START))
                        closeDrawer(Gravity.START);
                } else {
                    if (isDrawerVisible(GravityCompat.START))
                        closeDrawer(GravityCompat.START);
                }
            }

        });
    }

    @Override
    protected void onFinishInflate() {
        DrawerLayout.LayoutParams loParams = new DrawerLayout.LayoutParams(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
        if (Build.VERSION.SDK_INT >= 14)
            loParams.gravity = Gravity.START;
        else
            loParams.gravity = GravityCompat.START;
        findViewById(R.id.nexmenudrawerlayout_menulayout).setLayoutParams(loParams);
        super.onFinishInflate();
    }

    /**
     * Retorna el {@link ListView} de Elementos del men&#250; deslizable
     *
     * @return {@link ListView}
     */
    public ListView getMenuList() {
        return (ListView) findViewById(R.id.nexmenudrawerlayout_menulist);
    }

    /**
     * Instancia un logo para el men&#250; deslizable
     *
     * @param resourceId - Id de recurso del logo </br> En caso fuese de valor NULL, no se mostrar&#225; logo alguno
     */
    public void setMenuLogo(Integer resourceId) {
        ImageView loLogo = findViewById(R.id.nexmenudrawerlayout_menulogo);
        if (resourceId == null) {
            loLogo.setVisibility(View.GONE);
        } else {
            loLogo.setVisibility(View.VISIBLE);
            loLogo.setImageResource(resourceId.intValue());
        }
    }

    /**
     * Instancia un fragment en el {@link FrameLayout} de contenido
     *
     * @param fragment - {@link Fragment}
     */
    public void setNexFragment(Fragment fragment) throws Exception {
        FragmentTransaction loTransaction = ((FragmentActivity) getContext()).getSupportFragmentManager().beginTransaction();
        loTransaction.replace(R.id.nexmenudrawerlayout_framelayout, fragment);
        loTransaction.commit();
    }

    /**
     * Instancia un layout en el {@link FrameLayout} de contenido
     *
     * @param resourceId - Id de recurso del Layout
     */
    public void setLayout(int resourceId) throws Exception {
        FrameLayout loLayout = findViewById(R.id.nexmenudrawerlayout_framelayout);
//		LinearLayout loLayout = (LinearLayout)findViewById(R.id.nexmenudrawerlayout_framelayout);
        loLayout.removeAllViews();
        ((LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(resourceId, loLayout);
    }

    /**
     * Instancia un layout en el {@link FrameLayout} de contenido
     *
     * @param poView - Layout
     */
    public void setLayout(View poView) throws Exception {
        FrameLayout loLayout = findViewById(R.id.nexmenudrawerlayout_framelayout);
//		LinearLayout loLayout = (LinearLayout)findViewById(R.id.nexmenudrawerlayout_framelayout);
        loLayout.removeAllViews();
        loLayout.addView(poView);
    }

    /**
     * Retorna el {@link FrameLayout} de contenido
     *
     * @return {@link FrameLayout}
     */
    public FrameLayout getLayout() {
        return (FrameLayout) findViewById(R.id.nexmenudrawerlayout_framelayout);
    }

//	public LinearLayout getLayout(){
//		return (LinearLayout)findViewById(R.id.nexmenudrawerlayout_framelayout);
//	}

    /**
     * Interfaz utilizada para implementar la inicializaci&#243;n del {@link NexMenuDrawerLayout}
     *
     * @author jroeder
     */
    public static interface NexMenuCallbacks {
//		public void subIniNexMenu(ListView menuList);

        /**
         * Implementa un adapter con los elementos del ListView del men&#250;.
         *
         * @return {@link ListAdapter} - Adapter del {@link ListView}
         */
        public ListAdapter IniNexMenuAdapter();

        /**
         * Implementa la acci&#243;n cuando se realiza el evento Click en un elemento del {@link ListView} del men&#250;.
         *
         * @param position - Posici&#243;n del elemento
         */
        public void OnNexMenuItemClick(int position);
    }
}
