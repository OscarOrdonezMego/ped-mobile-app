package pe.com.nextel.android.bean;

import android.util.Log;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.StringWriter;

import pe.com.nextel.android.util.ConfiguracionNextel.EnumServerResponse;

/**
 * Clase que contiene m&#233;todos para serializar y desarializar objetos en JSON o clases especificadas respecticamente.
 *
 * @author jroeder
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanMapper {
    private static ObjectMapper m = new ObjectMapper();
    private static JsonFactory jf = new JsonFactory();

    /**
     * Convierte una cadena JSON a una clase BEAN
     *
     * @param jsonAsString Cadena JSON
     * @param BeanClass    Clase BEAN
     * @return Objeto del tipo Clase BEAN
     */
    public static <T> Object fromJson(String jsonAsString, Class<T> BeanClass) {
        try {
            return m.readValue(jsonAsString, BeanClass);
        } catch (Exception e) {
            Log.e("PRO", "BeanMapper.fromJson", e);
            return null;
        }
    }

    /**
     * Convierte una cadena JSON a un Tipo de Referencia
     *
     * @param jsonAsString Cadena JSON
     * @param typeRef      Referencia de Tipo
     * @return Objeto del tipo de referencia
     */
    public static <T> Object fromJson(String jsonAsString, TypeReference<T> typeRef) {
        try {
            return m.readValue(jsonAsString, typeRef);
        } catch (Exception e) {
            Log.e("PRO", "BeanMapper.fromJson", e);
            return null;
        }
    }

    /**
     * Convierte un objecto en cadena JSON
     *
     * @param bean Objeto a convertir
     * @return Cadena en JSON
     */
    public static String toJson(Object bean) throws Exception {
        return toJson(bean, false);
    }

    /**
     * Convierte un objecto en cadena JSON
     *
     * @param Bean        Objeto a convertir
     * @param prettyPrint Valor booleano para realizar impresi&#243;n del {@link #toString} del Objeto (Por defecto usarlo en <b>false</b>)
     * @return Cadena en JSON
     */
    public static String toJson(Object Bean, boolean prettyPrint) throws Exception {
        StringWriter sw = new StringWriter();
//        try 
//        {
        JsonGenerator jg = jf.createJsonGenerator(sw);
        if (prettyPrint) {
            jg.useDefaultPrettyPrinter();
        }
        m.writeValue(jg, Bean);
//		} 
//        catch (Exception e) 
//		{
//        	Log.e("PRO", "BeanMapper.toJson", e);
//        	sw.append(e.getMessage());
//		}

        return sw.toString();
    }

    /**
     * Resultado de la conexi&#243;n
     */
    @JsonProperty
    protected String resultado = "";
    /**
     * Id del resultado de la conexi&#243;n.</br>Para los valores pre-definidos ver {@link EnumServerResponse}
     */
    @JsonProperty
    protected int idResultado = EnumServerResponse.ALGUNERROR.getValue();
//    protected int idResultado = 0;

    /**
     * Retorna el resultado de la conexi&#243;n
     *
     * @return String
     */
    public String getResultado() {
        return resultado;
    }

    /**
     * Instancia el resultado de la conexi&#243;n
     *
     * @param resultado - Resultado de la conexi&#243;n
     */
    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    /**
     * Retorna el Id de resultado de la conexi&#243;n</br>
     *
     * @return int
     * @deprecated Utilizar m&#233;todo {@link #getEnumIdResultado}
     */
    public int getIdResultado() {
        return idResultado;
    }

    /**
     * Retorna el Id de resultado
     *
     * @return EnumServerResponse
     */
    public EnumServerResponse getEnumIdResultado() {
        return EnumServerResponse.get(getIdResultado());
    }

    /**
     * Instancia el Id de resultado de la conexi&#243;n
     *
     * @param idResultado - Id de resultado
     * @deprecated Utilizar el m&#233;todo {@link #setEnumIdResultado}
     */
    public void setIdResultado(int idResultado) {
        this.idResultado = idResultado;
    }

    /**
     * Instacia el Id de resultado de la conexi&#243;n.
     *
     * @param idResultado - Id de resultado
     */
    public void setEnumIdResultado(EnumServerResponse idResultado) {
        this.idResultado = idResultado.getValue();
    }
}
