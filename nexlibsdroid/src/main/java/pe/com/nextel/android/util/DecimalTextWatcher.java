package pe.com.nextel.android.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Clase que implementa un {@link TextWatcher} y verifica si la cadena de entrada
 * es un n&#250;mero decimal
 * @author jroeder
 *
 */
public class DecimalTextWatcher implements TextWatcher {
	private Pattern ioPattern;
	private int iiStart;
	private int iiIntegerDigits;
	@SuppressWarnings("unused")
	private int iiDecimalDigits;
	/**
	 * Contructor de la clase
	 * @param piIntegerDigits - N&#250;mero de d&#237;gitos enteros
	 * @param piDecimalDigits - N&#250;mero de d&#237;gitos decimales
	 */
	public DecimalTextWatcher(int piIntegerDigits,int piDecimalDigits) {
		iiIntegerDigits = piIntegerDigits;
		iiDecimalDigits = piDecimalDigits;
		ioPattern = Pattern.compile("^\\d{1," + (piIntegerDigits) + "}(\\.\\d{0," + (piDecimalDigits) + "})?$");
	}
	
	@Override
	public void afterTextChanged(Editable s) {
		try{
			Matcher loMatcher = ioPattern.matcher(s.toString());       
	        if(!loMatcher.matches()){
	        	s.delete(iiStart, iiStart+1);
	        }
	        if(!(s.toString().indexOf(".") > 0)){
	        	if(s.length()==iiIntegerDigits){
	        		s.append(".");
	        	}
	        }
		}catch (IndexOutOfBoundsException e) {
			//EMPTY STRING
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		iiStart = start;
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		//DO NOTHING
	}

}
