package pe.com.nextel.android.util;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import pe.com.nextel.android.R;

/**
 * {@link DialogFragment} que muestra un mensaje con una entrada condicional
 *
 * @author jroeder
 */
public class DialogFragmentYesNo extends DialogFragment {

    /**
     * Enumerable que contiene una lista de recursos layout predefinidos.
     *
     * @author jroeder
     */
    public enum EnumLayoutResource {
        /**
         * Recurso del layout <b>R.layout.dialogfragmentyesno_design01</b>
         */
        DESIGN01(R.layout.dialogfragmentyesno_design01,
                R.attr.DialogFragmentYesNo_design01_icon_accept,
                R.attr.DialogFragmentYesNo_design01_icon_cancel,
                R.attr.DialogFragmentYesNo_design01_icon_information,
                R.attr.DialogFragmentYesNo_design01_icon_help,
                R.attr.DialogFragmentYesNo_design01_icon_error),
        /**
         * Recurso del layout <b>R.layout.dialogfragmentyesno_design02</b>
         */
        DESIGN02(R.layout.dialogfragmentyesno_design02,
                R.attr.DialogFragmentYesNo_design02_icon_accept,
                R.attr.DialogFragmentYesNo_design02_icon_cancel,
                R.attr.DialogFragmentYesNo_design02_icon_information,
                R.attr.DialogFragmentYesNo_design02_icon_help,
                R.attr.DialogFragmentYesNo_design02_icon_error),
        /**
         * Recurso del layout <b>R.layout.dialogfragmentyesno_design03</b>
         */
        DESIGN03(R.layout.dialogfragmentyesno_design03,
                R.attr.DialogFragmentYesNo_design03_icon_accept,
                R.attr.DialogFragmentYesNo_design03_icon_cancel,
                R.attr.DialogFragmentYesNo_design03_icon_information,
                R.attr.DialogFragmentYesNo_design03_icon_help,
                R.attr.DialogFragmentYesNo_design03_icon_error),

        /**
         * Recurso del layout <b>R.layout.dialogfragmentyesno_design04</b>
         */
        DESIGN04(R.layout.dialogfragmentyesno_design04,
                R.attr.DialogFragmentYesNo_design04_icon_accept,
                R.attr.DialogFragmentYesNo_design04_icon_cancel,
                R.attr.DialogFragmentYesNo_design04_icon_information,
                R.attr.DialogFragmentYesNo_design04_icon_help,
                R.attr.DialogFragmentYesNo_design04_icon_error),

        /**
         * Recurso del layout <b>R.layout.dialogfragmentyesno_design04</b>
         */
        DESIGN05(R.layout.dialogfragmentyesno_design05,
                R.attr.DialogFragmentYesNo_design05_icon_accept,
                R.attr.DialogFragmentYesNo_design05_icon_cancel,
                R.attr.DialogFragmentYesNo_design05_icon_information,
                R.attr.DialogFragmentYesNo_design05_icon_help,
                R.attr.DialogFragmentYesNo_design05_icon_error);;

        private int resource;

        private int iconHelp;
        private int iconCancel;
        private int iconInformation;
        private int iconError;
        private int iconAccept;

        private EnumLayoutResource(int resource, int iconAccept, int iconCancel, int iconInformation, int iconHelp, int iconError) {
            this.resource = resource;

            this.iconAccept = iconAccept;
            this.iconCancel = iconCancel;
            this.iconInformation = iconInformation;
            this.iconHelp = iconHelp;
            this.iconError = iconError;
        }

        /**
         * Retorna el id de recurso layout
         *
         * @return int
         */
        public int getResource() {
            return this.resource;
        }

        private static final Map<Integer, EnumLayoutResource> lookup = new HashMap<Integer, EnumLayoutResource>();

        static {
            for (EnumLayoutResource loEnum : EnumSet.allOf(EnumLayoutResource.class)) {
                lookup.put(loEnum.getResource(), loEnum);
            }
        }

        /**
         * Retorna el enumerable de recurso layout
         *
         * @param resource - Id de Layout de recurso
         * @return {@link EnumLayoutResource}
         */
        public static EnumLayoutResource getEnum(int resource) {
            return lookup.get(resource);
        }

        /**
         * Retorna el id de recurso del &#237;cono de aceptar del dise&#241;o por defecto del tema
         *
         * @return int
         */
        public static int getDefaultIconAccept(Context context) {
            return EnumLayoutResource.obtainLayoutResource(context).getIconAccept(context);
        }

        /**
         * Retorna el id de recurso del &#237;cono de error del dise&#241;o por defecto del tema
         *
         * @return int
         */
        public static int getDefaultIconError(Context context) {
            return EnumLayoutResource.obtainLayoutResource(context).getIconError(context);
        }

        /**
         * Retorna el id de recurso del &#237;cono de ayuda del dise&#241;o por defecto del tema
         *
         * @return int
         */
        public static int getDefaultIconHelp(Context context) {
            return EnumLayoutResource.obtainLayoutResource(context).getIconHelp(context);
        }

        /**
         * Retorna el id de recurso del &#237;cono de informaci&#243;n del dise&#241;o por defecto del tema
         *
         * @return int
         */
        public static int getDefaultIconInformation(Context context) {
            return EnumLayoutResource.obtainLayoutResource(context).getIconInformation(context);
        }

        /**
         * Retorna el id de recurso del &#237;cono de cancelar del dise&#241;o por defecto del tema
         *
         * @return int
         */
        public static int getDefaultIconCancel(Context context) {
            return EnumLayoutResource.obtainLayoutResource(context).getIconCancel(context);
        }

        /**
         * Retorna el id de recurso del &#237;cono de aceptar
         *
         * @param context {@link Context} de la aplicaci&#243;n
         *                </br>Si el contexto es nulo, retorna el id de recurso de atributo <b>R.attr.*</b>
         * @return <b>int</b>
         */
        public int getIconAccept(Context context) {
            if (context == null)
                return iconAccept;
            return Utilitario.obtainResourceId(context, iconAccept);
        }

        /**
         * Retorna el id de recurso del &#237;cono de error
         *
         * @param context {@link Context} de la aplicaci&#243;n
         *                </br>Si el contexto es nulo, retorna el id de recurso de atributo <b>R.attr.*</b>
         * @return <b>int</b>
         */
        public int getIconError(Context context) {
            if (context == null)
                return iconError;
            return Utilitario.obtainResourceId(context, iconError);
        }

        /**
         * Retorna el id de recurso del &#237;cono de informaci&#243;n
         *
         * @param context {@link Context} de la aplicaci&#243;n
         *                </br>Si el contexto es nulo, retorna el id de recurso de atributo <b>R.attr.*</b>
         * @return <b>int</b>
         */
        public int getIconInformation(Context context) {
            if (context == null)
                return iconInformation;
            return Utilitario.obtainResourceId(context, iconInformation);
        }

        /**
         * Retorna el id de recurso del &#237;cono de ayuda
         *
         * @param context {@link Context} de la aplicaci&#243;n
         *                </br>Si el contexto es nulo, retorna el id de recurso de atributo <b>R.attr.*</b>
         * @return <b>int</b>
         */
        public int getIconHelp(Context context) {
            if (context == null)
                return iconHelp;
            return Utilitario.obtainResourceId(context, iconHelp);
        }

        /**
         * Retorna el id de recurso del &#237;cono de cancelar
         *
         * @param context {@link Context} de la aplicaci&#243;n
         *                </br>Si el contexto es nulo, retorna el id de recurso de atributo <b>R.attr.*</b>
         * @return <b>int</b>
         */
        public int getIconCancel(Context context) {
            if (context == null)
                return iconCancel;
            return Utilitario.obtainResourceId(context, iconCancel);
        }

        /**
         * Retorna el {@link EnumLayoutResource} definido en el atributo <b>R.attr.DialogFragmentYesNo_design</b>
         *
         * @param context {@link Context} de aplicaci&#243;n
         * @return {@link EnumLayoutResource}
         */
        public static EnumLayoutResource obtainLayoutResource(Context context) {
            TypedArray loTypedArray = context.obtainStyledAttributes(new int[]{R.attr.DialogFragmentYesNo_design});
            TypedValue loTypedValue = new TypedValue();
            try {
                if (loTypedArray.getValue(0, loTypedValue))
                    return EnumLayoutResource.values()[loTypedValue.data];
                return EnumLayoutResource.DESIGN01;
            } catch (Exception e) {
                Log.e("PRO", "DialogFragmentYesNo.EnumLayoutResource.obtainLayoutResource", e);
                return EnumLayoutResource.DESIGN01;
            } finally {
                try {
                    loTypedArray.recycle();
                    loTypedArray = null;
                    loTypedValue = null;
                } catch (Exception e) {
                    Log.e("PRO", "DialogFragmentYesNo.EnumLayoutResource.obtainLayoutResource.finally", e);
                }
            }
        }
    }

    ;

    /**
     * Variable Global de la respuesta en caso sea positiva (YES)
     */
    public static final short YES = 1;
    /**
     * Variable Global de la respuesta en caso sea negativa (NO)
     */
    public static final short NO = 0;

    /**
     * Instancia del Resultado
     */
    private static int iiDialogResult;
    /**
     * Instancia del hilo o {@link Handler}
     */
    private Handler ioHandler;

    private final static String KEY_TITLE = "title";
    private final static String KEY_MESSAGE = "message";
    private final static String KEY_YESNO = "yesno";
    private final static String KEY_ICON = "icon";
    private final static String KEY_ICONATTR = "iconattr";
    private final static String KEY_LAYOUT = "layout";
    private final static String KEY_TAGNAME = "tagname";

    /**
     * Variable global utilizada para adjuntar como nombre adjunto al {@link Fragment} del {@link FragmentManager} y {@link FragmentTransaction}
     */
    public static final String TAG = "loFragmentYesNo";

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}, con una entrada condicional.
     *
     * @param poManager - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag     - Nombre adjunto al {@link Fragment}
     * @param psMessage - Mensaje a mostrar
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psMessage) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, null);
        loBundle.putString(KEY_MESSAGE, psMessage);
//		loBundle.putInt(KEY_ICON, android.R.drawable.ic_dialog_info);
//		loBundle.putInt(KEY_ICON, EnumLayoutResource.DESIGN01.getIconInformation());
//		loBundle.putInt(KEY_ICONATTR, EnumLayoutResource.DESIGN01.getIconInformation(null));
        loBundle.putBoolean(KEY_YESNO, true);
//		loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}.
     *
     * @param poManager - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag     - Nombre adjunto al {@link Fragment}
     * @param psMessage - Mensaje a mostrar
     * @param pbYesNo   - Inidicador L&#243;gico.
     *                  <ul>
     *                  <li><b>true</b> si es con 2 botones</li>
     *                  <li><b>false</b> si es con 1 bot&#243;n</li>
     *                  </ul>
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psMessage, boolean pbYesNo) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, null);
        loBundle.putString(KEY_MESSAGE, psMessage);
//		loBundle.putInt(KEY_ICON, android.R.drawable.ic_dialog_info);
//		loBundle.putInt(KEY_ICON, R.drawable.ic_nex_information);
//		loBundle.putInt(KEY_ICON, EnumLayoutResource.DESIGN01.getIconInformation());
//		loBundle.putInt(KEY_ICONATTR, EnumLayoutResource.DESIGN01.getIconInformation(null));
        loBundle.putBoolean(KEY_YESNO, pbYesNo);
//		loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}
     *
     * @param poManager - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag     - Nombre adjunto al {@link Fragment}
     * @param psTitle   T&#237;tulo del di&#225;logo
     * @param psMessage Mensaje a mostrar
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psTitle, String psMessage) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, psTitle);
        loBundle.putString(KEY_MESSAGE, psMessage);
//		loBundle.putInt(KEY_ICON, android.R.drawable.ic_dialog_info);
//		loBundle.putInt(KEY_ICON, R.drawable.ic_nex_information);
//		loBundle.putInt(KEY_ICON, EnumLayoutResource.DESIGN01.getIconInformation());
//		loBundle.putInt(KEY_ICONATTR, EnumLayoutResource.DESIGN01.getIconInformation(null));
        loBundle.putBoolean(KEY_YESNO, true);
//		loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}
     *
     * @param poManager - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag     - Nombre adjunto al {@link Fragment}
     * @param psTitle   T&#237;tulo del di&#225;logo
     * @param psMessage Mensaje a mostrar
     * @param pbYesNo   Inidicador L&#243;gico.
     *                  <ul>
     *                  <li><b>true</b> si es con 2 botones</li>
     *                  <li><b>false</b> si es con 1 bot&#243;n</li>
     *                  </ul>
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psTitle, String psMessage, boolean pbYesNo) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, psTitle);
        loBundle.putString(KEY_MESSAGE, psMessage);
//		loBundle.putInt(KEY_ICON, android.R.drawable.ic_dialog_info);
//		loBundle.putInt(KEY_ICON, R.drawable.ic_nex_information);
//		loBundle.putInt(KEY_ICON, EnumLayoutResource.DESIGN01.getIconInformation());
//		loBundle.putInt(KEY_ICONATTR, EnumLayoutResource.DESIGN01.getIconInformation(null));
        loBundle.putBoolean(KEY_YESNO, pbYesNo);
//		loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}
     *
     * @param poManager - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag     - Nombre adjunto al {@link Fragment}
     * @param psMessage Mensaje a mostrar
     * @param piIcon    Id de Recurso del &#205;cono del Di&#243;logo
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psMessage, int piIcon) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, null);
        loBundle.putString(KEY_MESSAGE, psMessage);
        loBundle.putInt(KEY_ICON, piIcon);
        loBundle.putBoolean(KEY_YESNO, true);
//		loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}
     *
     * @param poManager - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag     - Nombre adjunto al {@link Fragment}
     * @param psMessage Mensaje a mostrar
     * @param piIcon    Id de Recurso del &#205;cono del Di&#225;logo
     * @param pbYesNo   Inidicador L&#243;gico.
     *                  <ul>
     *                  <li><b>true</b> si es con 2 botones</li>
     *                  <li><b>false</b> si es con 1 bot&#243;n</li>
     *                  </ul>
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psMessage, int piIcon, boolean pbYesNo) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, null);
        loBundle.putString(KEY_MESSAGE, psMessage);
        loBundle.putInt(KEY_ICON, piIcon);
        loBundle.putBoolean(KEY_YESNO, pbYesNo);
//		loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}
     *
     * @param poManager - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag     - Nombre adjunto al {@link Fragment}
     * @param psTitle   T&#237;tulo del di&#225;logo
     * @param psMessage Mensaje a mostrar
     * @param piIcon    Id de Recurso del &#205;cono del Di&#225;logo
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psTitle, String psMessage, int piIcon) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, psTitle);
        loBundle.putString(KEY_MESSAGE, psMessage);
        loBundle.putInt(KEY_ICON, piIcon);
        loBundle.putBoolean(KEY_YESNO, true);
//		loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}
     *
     * @param poManager - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag     - Nombre adjunto al {@link Fragment}
     * @param psTitle   T&#237;tulo del di&#225;logo
     * @param psMessage Mensaje a mostrar
     * @param piIcon    Id de Recurso del &#205;cono del Di&#225;logo
     * @param pbYesNo   Inidicador L&#243;gico.
     *                  <ul>
     *                  <li><b>true</b> si es con 2 botones</li>
     *                  <li><b>false</b> si es con 1 bot&#243;n</li>
     *                  </ul>
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psTitle, String psMessage, int piIcon, boolean pbYesNo) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, psTitle);
        loBundle.putString(KEY_MESSAGE, psMessage);
        loBundle.putInt(KEY_ICON, piIcon);
        loBundle.putBoolean(KEY_YESNO, pbYesNo);
//		loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}, con una entrada condicional.
     *
     * @param poManager        - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag            - Nombre adjunto al {@link Fragment}
     * @param psMessage        - Mensaje a mostrar
     * @param poLayoutResource - Enumerable de recurso layout
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psMessage, EnumLayoutResource poLayoutResource) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, null);
        loBundle.putString(KEY_MESSAGE, psMessage);
//		loBundle.putInt(KEY_ICON, android.R.drawable.ic_dialog_info);
        loBundle.putBoolean(KEY_YESNO, true);
        try {

//			loBundle.putInt(KEY_ICON, poLayoutResource.getIconInformation());
            loBundle.putInt(KEY_ICONATTR, poLayoutResource.getIconInformation(null));
            loBundle.putInt(KEY_LAYOUT, poLayoutResource.getResource());
        } catch (NullPointerException e) {
//			loBundle.putInt(KEY_ICON, EnumLayoutResource.DESIGN01.getIconInformation());
//			loBundle.putInt(KEY_ICONATTR, EnumLayoutResource.DESIGN01.getIconInformation(null));
//			loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        }
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}.
     *
     * @param poManager        - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag            - Nombre adjunto al {@link Fragment}
     * @param psMessage        - Mensaje a mostrar
     * @param pbYesNo          - Inidicador L&#243;gico.
     *                         <ul>
     *                         <li><b>true</b> si es con 2 botones</li>
     *                         <li><b>false</b> si es con 1 bot&#243;n</li>
     *                         </ul>
     * @param poLayoutResource - Enumerable de recurso layout
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psMessage, boolean pbYesNo, EnumLayoutResource poLayoutResource) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, null);
        loBundle.putString(KEY_MESSAGE, psMessage);
//		loBundle.putInt(KEY_ICON, android.R.drawable.ic_dialog_info);
//		loBundle.putInt(KEY_ICON, R.drawable.ic_nex_information);
        loBundle.putBoolean(KEY_YESNO, pbYesNo);
        try {
//			loBundle.putInt(KEY_ICON, poLayoutResource.getIconInformation());
            loBundle.putInt(KEY_ICONATTR, poLayoutResource.getIconInformation(null));
            loBundle.putInt(KEY_LAYOUT, poLayoutResource.getResource());
        } catch (NullPointerException e) {
//			loBundle.putInt(KEY_ICON, EnumLayoutResource.DESIGN01.getIconInformation());
//			loBundle.putInt(KEY_ICONATTR, EnumLayoutResource.DESIGN01.getIconInformation(null));
//			loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        }
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}
     *
     * @param poManager        - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag            - Nombre adjunto al {@link Fragment}
     * @param psTitle          T&#237;tulo del di&#225;logo
     * @param psMessage        Mensaje a mostrar
     * @param poLayoutResource - Enumerable de recurso layout
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psTitle, String psMessage, EnumLayoutResource poLayoutResource) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, psTitle);
        loBundle.putString(KEY_MESSAGE, psMessage);
//		loBundle.putInt(KEY_ICON, android.R.drawable.ic_dialog_info);
//		loBundle.putInt(KEY_ICON, R.drawable.ic_nex_information);
        loBundle.putBoolean(KEY_YESNO, true);
        try {
//			loBundle.putInt(KEY_ICON, poLayoutResource.getIconInformation());
            loBundle.putInt(KEY_ICONATTR, poLayoutResource.getIconInformation(null));
            loBundle.putInt(KEY_LAYOUT, poLayoutResource.getResource());
        } catch (NullPointerException e) {
//			loBundle.putInt(KEY_ICON, EnumLayoutResource.DESIGN01.getIconInformation());
//			loBundle.putInt(KEY_ICONATTR, EnumLayoutResource.DESIGN01.getIconInformation(null));
//			loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        }
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}
     *
     * @param poManager        - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag            - Nombre adjunto al {@link Fragment}
     * @param psTitle          T&#237;tulo del di&#225;logo
     * @param psMessage        Mensaje a mostrar
     * @param pbYesNo          Inidicador L&#243;gico.
     *                         <ul>
     *                         <li><b>true</b> si es con 2 botones</li>
     *                         <li><b>false</b> si es con 1 bot&#243;n</li>
     *                         </ul>
     * @param poLayoutResource - Enumerable de recurso layout
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psTitle, String psMessage, boolean pbYesNo, EnumLayoutResource poLayoutResource) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, psTitle);
        loBundle.putString(KEY_MESSAGE, psMessage);
//		loBundle.putInt(KEY_ICON, android.R.drawable.ic_dialog_info);
//		loBundle.putInt(KEY_ICON, R.drawable.ic_nex_information);
        loBundle.putBoolean(KEY_YESNO, pbYesNo);
        try {
//			loBundle.putInt(KEY_ICON, poLayoutResource.getIconInformation());
            loBundle.putInt(KEY_ICONATTR, poLayoutResource.getIconInformation(null));
            loBundle.putInt(KEY_LAYOUT, poLayoutResource.getResource());
        } catch (NullPointerException e) {
//			loBundle.putInt(KEY_ICON, EnumLayoutResource.DESIGN01.getIconInformation());
//			loBundle.putInt(KEY_ICONATTR, EnumLayoutResource.DESIGN01.getIconInformation(null));
//			loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        }
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}
     *
     * @param poManager        - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag            - Nombre adjunto al {@link Fragment}
     * @param psMessage        Mensaje a mostrar
     * @param piIcon           Id de Recurso del &#205;cono del Di&#243;logo
     * @param poLayoutResource - Enumerable de recurso layout
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psMessage, int piIcon, EnumLayoutResource poLayoutResource) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, null);
        loBundle.putString(KEY_MESSAGE, psMessage);
        loBundle.putInt(KEY_ICON, piIcon);
        loBundle.putBoolean(KEY_YESNO, true);
        try {
            loBundle.putInt(KEY_LAYOUT, poLayoutResource.getResource());
        } catch (NullPointerException e) {
//			loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        }
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}
     *
     * @param poManager        - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag            - Nombre adjunto al {@link Fragment}
     * @param psMessage        Mensaje a mostrar
     * @param piIcon           Id de Recurso del &#205;cono del Di&#225;logo
     * @param pbYesNo          Inidicador L&#243;gico.
     *                         <ul>
     *                         <li><b>true</b> si es con 2 botones</li>
     *                         <li><b>false</b> si es con 1 bot&#243;n</li>
     *                         </ul>
     * @param poLayoutResource - Enumerable de recurso layout
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psMessage, int piIcon, boolean pbYesNo, EnumLayoutResource poLayoutResource) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, null);
        loBundle.putString(KEY_MESSAGE, psMessage);
        loBundle.putInt(KEY_ICON, piIcon);
        loBundle.putBoolean(KEY_YESNO, pbYesNo);
        try {
            loBundle.putInt(KEY_LAYOUT, poLayoutResource.getResource());
        } catch (NullPointerException e) {
//			loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        }
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}
     *
     * @param poManager        - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag            - Nombre adjunto al {@link Fragment}
     * @param psTitle          T&#237;tulo del di&#225;logo
     * @param psMessage        Mensaje a mostrar
     * @param piIcon           Id de Recurso del &#205;cono del Di&#225;logo
     * @param poLayoutResource - Enumerable de recurso layout
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psTitle, String psMessage, int piIcon, EnumLayoutResource poLayoutResource) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, psTitle);
        loBundle.putString(KEY_MESSAGE, psMessage);
        loBundle.putInt(KEY_ICON, piIcon);
        loBundle.putBoolean(KEY_YESNO, true);
        try {
            loBundle.putInt(KEY_LAYOUT, poLayoutResource.getResource());
        } catch (NullPointerException e) {
//			loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        }
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    /**
     * Genera nueva instancia de la clase {@link DialogFragmentYesNo}
     *
     * @param poManager        - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag            - Nombre adjunto al {@link Fragment}
     * @param psTitle          T&#237;tulo del di&#225;logo
     * @param psMessage        Mensaje a mostrar
     * @param piIcon           Id de Recurso del &#205;cono del Di&#225;logo
     * @param pbYesNo          Inidicador L&#243;gico.
     *                         <ul>
     *                         <li><b>true</b> si es con 2 botones</li>
     *                         <li><b>false</b> si es con 1 bot&#243;n</li>
     *                         </ul>
     * @param poLayoutResource - Enumerable de recurso layout
     */
    public static DialogFragmentYesNo newInstance(FragmentManager poManager, String psTag, String psTitle, String psMessage, int piIcon, boolean pbYesNo, EnumLayoutResource poLayoutResource) {
        DialogFragmentYesNo.subRemovePrevDialog(poManager, psTag);
        DialogFragmentYesNo loFrag = new DialogFragmentYesNo();
        Bundle loBundle = new Bundle();
        loBundle.putString(KEY_TAGNAME, psTag);
        loBundle.putString(KEY_TITLE, psTitle);
        loBundle.putString(KEY_MESSAGE, psMessage);
        loBundle.putInt(KEY_ICON, piIcon);
        loBundle.putBoolean(KEY_YESNO, pbYesNo);
        try {
            loBundle.putInt(KEY_LAYOUT, poLayoutResource.getResource());
        } catch (NullPointerException e) {
//			loBundle.putInt(KEY_LAYOUT, EnumLayoutResource.DESIGN01.getResource());
        }
        loFrag.setArguments(loBundle);
        return loFrag;
    }

    @Override
    public void onResume() {
        getDialog().getWindow().setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setStyle(DialogFragmentYesNo.STYLE_NO_FRAME, android.R.style.Theme);
        super.onResume();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            ioHandler = new Handler() {
                @Override
                public void handleMessage(Message mesg) {
                    // process incoming messages here
                    //super.handleMessage(msg);
                    Looper.getMainLooper().quit();
                    throw new RuntimeException();
                }
            };
        }
        Dialog loDialog = super.onCreateDialog(savedInstanceState);
        loDialog.setCancelable(false);
        return loDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(false);
        int liLayout = getArguments().getInt(KEY_LAYOUT);
        if (liLayout == 0) {
            EnumLayoutResource loLayoutResource = EnumLayoutResource.obtainLayoutResource(getActivity());
            liLayout = loLayoutResource.getResource();
            getArguments().putInt(KEY_LAYOUT, liLayout);
            getArguments().putInt(KEY_ICONATTR, loLayoutResource.getIconInformation(null));
        }
        View v = inflater.inflate(liLayout, container, true);
        TextView loTitle = v.findViewById(R.id.dialogfragmentyesno_title);
        loTitle.setText(getArguments().getString(KEY_TITLE) != null ? getArguments().getString(KEY_TITLE) : getActivity().getString(R.string.dlg_titinformacion));
        ImageView loIcon = v.findViewById(R.id.dialogfragmentyesno_icon);
        int resIdIcon = getArguments().getInt(KEY_ICON);
        if (resIdIcon == 0)
            resIdIcon = Utilitario.obtainResourceId(getActivity(), getArguments().getInt(KEY_ICONATTR));
        loIcon.setImageResource(resIdIcon);
        TextView loMessage = v.findViewById(R.id.dialogfragmentyesno_message);
        loMessage.setText(getArguments().getString(KEY_MESSAGE));
        if (EnumLayoutResource.getEnum(liLayout) != EnumLayoutResource.DESIGN03) {
            if (EnumLayoutResource.getEnum(liLayout) == EnumLayoutResource.DESIGN05) {
                loMessage.setMovementMethod(new ScrollingMovementMethod());
            }
            Button loYes = v.findViewById(R.id.dialogfragmentyesno_yes);
            loYes.setText(getArguments().getBoolean(KEY_YESNO) ? getActivity().getString(R.string.dlg_btnsi) : getActivity().getString(R.string.dlg_btnok));
            loYes.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    endDialog(DialogFragmentYesNo.YES);
                }
            });
            View loDivider = v.findViewById(R.id.dialogfragmentyesno_divider);
            Button loNo = v.findViewById(R.id.dialogfragmentyesno_no);
            if (getArguments().getBoolean(KEY_YESNO)) {
                loDivider.setVisibility(View.VISIBLE);
                loNo.setVisibility(View.VISIBLE);
                loNo.setText(getActivity().getString(R.string.dlg_btnno));
                loNo.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        endDialog(DialogFragmentYesNo.NO);
                    }
                });
            } else {
                loDivider.setVisibility(View.GONE);
                loNo.setVisibility(View.GONE);
                if (EnumLayoutResource.getEnum(liLayout) == EnumLayoutResource.DESIGN04)
                    loYes.setBackgroundResource(R.drawable.dialogfragmentyesnobutton04);
                else if (EnumLayoutResource.getEnum(liLayout) == EnumLayoutResource.DESIGN05)
                    loYes.setBackgroundResource(R.drawable.dialogfragmentyesnobutton05);
            }
        } else {
            loMessage.setMovementMethod(new ScrollingMovementMethod());
            ImageButton loYes = v.findViewById(R.id.dialogfragmentyesno_yes);
            loYes.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    endDialog(DialogFragmentYesNo.YES);
                }
            });
            View loDivider = v.findViewById(R.id.dialogfragmentyesno_divider);
            ImageButton loNo = v.findViewById(R.id.dialogfragmentyesno_no);
            if (getArguments().getBoolean(KEY_YESNO)) {
                loDivider.setVisibility(View.VISIBLE);
                loNo.setVisibility(View.VISIBLE);
                loNo.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        endDialog(DialogFragmentYesNo.NO);
                    }
                });
            } else {
                loDivider.setVisibility(View.GONE);
                loNo.setVisibility(View.GONE);
            }
        }
        return v;
    }

    /**
     * Finaliza el Alert Dialog
     *
     * @param piResult Resultado del di&#225;logo
     */
    public void endDialog(int piResult) {
        Log.v("DialogFragmentYesNo", "<endDialog(" + piResult + ")>");
        dismiss();
        iiDialogResult = piResult;
        Message m = ioHandler.obtainMessage();
        ioHandler.sendMessage(m);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            super.show(manager, tag);
        } catch (IllegalStateException e) {
            Log.e("PRO", "DialogFragmentYesNo.show", e);
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        endDialog(DialogFragmentYesNo.NO);
    }

    /**
     * Remueve el {@link Fragment} con el nombre de tag indicado del {@link FragmentTransaction}
     *
     * @param poManager - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag     - Nombre adjunto del {@link Fragment}
     */
    private static void subRemovePrevDialog(FragmentManager poManager, String psTag) {
        FragmentTransaction loTransaction = poManager.beginTransaction();
        Fragment loPrevFragment = poManager.findFragmentByTag(psTag);
        if (loPrevFragment != null) {
            loTransaction.remove(loPrevFragment);
        }
        loTransaction.addToBackStack(null);
    }

    /**
     * Muestra el di&#225;logo, retornando un valor de respuesta.
     *
     * @param poManager - {@link FragmentManager} del contexto de la aplicaci&#243;n
     * @param psTag     - Nombre adjunto al {@link Fragment}
     * @return <b>int</b> - {@link #YES YES}, {@link #NO NO}
     */
    @SuppressWarnings("static-access")
    public int showDialog(FragmentManager poManager, String psTag) {
        Log.v("DialogFragmentYesNo", "<showDialog>");
        ioHandler = new Handler() {
            @Override
            public void handleMessage(Message mesg) {
                // process incoming messages here
                //super.handleMessage(msg);
                Looper.getMainLooper().quit();
                System.gc();
                throw new RuntimeException();
            }
        };
        show(poManager, psTag);
        try {
            Looper.getMainLooper().loop();
        } catch (RuntimeException e2) {
        }
        return iiDialogResult;
    }

    /**
     * Retorna el nombre del TAG asignado en el momento de crear la instancia
     *
     * @return {String}
     */
    public String getTagName() {
        return getArguments().getString(KEY_TAGNAME);
    }

    /**
     * Implementa los m&#233todos que de acci&#243;n de los botones de afirmaci&#243;n o negaci&#243;n en el di&#225;logo de confirmaci&#243;n.
     * En caso el mensaje no sea de confirmaci&#243;n s&#243;lo implementar el m&#233;todo {@link OnDialogFragmentYesNoClickListener#performButtonYes performButtonYes}
     *
     * @author jroeder
     */
    public static interface OnDialogFragmentYesNoClickListener {
        /**
         * Invocado cuando la confirmaci&#243;n ha sido afirmativa
         *
         * @param dialog {@link DialogFragmentYesNo} de confirmaci&#243;n
         */
        public void performButtonYes(DialogFragmentYesNo dialog);

        /**
         * Invocado cuando la confirmaci&#243;n ha sido negativa
         *
         * @param dialog {@link DialogFragmentYesNo} de confirmaci&#243;n
         */
        public void performButtonNo(DialogFragmentYesNo dialog);
    }

    /**
     * Agrupa un {@link DialogFragmentYesNo} con un {@link OnDialogFragmentYesNoClickListener}
     *
     * @author jroeder
     */
    public static class DialogFragmentYesNoPairListener extends Pair<DialogFragmentYesNo, OnDialogFragmentYesNoClickListener> {

        /**
         * Constructor del {@link DialogFragmentYesNoPairListener}
         *
         * @param dialog   {@link DialogFragmentYesNo}
         * @param listener {@link OnDialogFragmentYesNoClickListener}
         */
        public DialogFragmentYesNoPairListener(DialogFragmentYesNo dialog,
                                               OnDialogFragmentYesNoClickListener listener) {
            super(dialog, listener);
        }

        /**
         * @return {@link DialogFragmentYesNo}
         */
        public DialogFragmentYesNo getDialogFragment() {
            return first;
        }

        /**
         * @return {@link OnDialogFragmentYesNoClickListener}
         */
        public OnDialogFragmentYesNoClickListener getOnClickListener() {
            return second;
        }
    }
}
