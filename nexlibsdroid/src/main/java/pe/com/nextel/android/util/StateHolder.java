package pe.com.nextel.android.util;

import android.content.Context;

/**
 * Clase utilizada para contener el estado de instancia de una actividad.</br>
 * Se puede implementar para contener diversos objetos.
 *
 * @author jroeder
 */
public abstract class StateHolder {
    private Context ioContext;

    /**
     * Constructor de la clase
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     */
    public StateHolder(Context poContext) {
        this.ioContext = poContext;
    }

    /**
     * Adjunta un nuevo contexto al objeto
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     */
    public void attach(Context poContext) {
        this.ioContext = poContext;
        attach();
    }

    /**
     * Remueve el contexto de la aplicaci&#243;n
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     */
    public void dettach(Context poContext) {
        this.ioContext = null;
        dettach();
    }

    /**
     * Implementa una acci&#243;n al momento de adjuntar el contexto de la aplicaci&#243;n
     */
    protected abstract void attach();

    /**
     * Implementa una acci&#243;n al momento de remover el contexto de la aplicaci&#243;n
     */
    protected abstract void dettach();

    /**
     * Retorna el actual contexto de la aplicaci&#243;n
     *
     * @return Context - Contexto de la aplicaci&#243;n
     */
    public Context getContext() {
        return ioContext;
    }
}
