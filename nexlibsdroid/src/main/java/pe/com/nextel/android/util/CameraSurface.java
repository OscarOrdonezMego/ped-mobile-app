package pe.com.nextel.android.util;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

/**
 * Clase que implementa un {@link SurfaceView} para el display de la c&#225;mara
 *
 * @author jroeder
 */
public class CameraSurface extends SurfaceView implements SurfaceHolder.Callback {

    private Camera mCamera;
    private SurfaceHolder mHolder;
    private Context mContext;

    /**
     * Contructor de la clase
     *
     * @param context - Contexto de la aplicaci&#243;n
     * @param camera  - Instancia de la c&#225;mara
     */
    @SuppressWarnings("deprecation")
    public CameraSurface(Context context, Camera camera) {
        super(context);
        mContext = context;
        mCamera = camera;
        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        Log.e("CAMARA", "---- SURFACE CHANGED ----");
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }

        subHandleRotationChange((Activity) mContext);

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e) {
            Log.d("CAMARA", "Error starting camera preview: " + e.getMessage());
        }
    }

    private void subHandleRotationChange(Activity act) {
        int rotation = act.getWindowManager().getDefaultDisplay().getRotation();
        Log.v("CAMARA", "cambio rotacion --> " + rotation);
        Parameters parameters = mCamera.getParameters();

        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 90;
                break;
            case Surface.ROTATION_90:
                degrees = 0;
                break;
            case Surface.ROTATION_180:
                degrees = 270;
                break;
            case Surface.ROTATION_270:
                degrees = 180;
                break;
        }
        try {
            parameters.setRotation(degrees);
            mCamera.setDisplayOrientation(degrees);
            mCamera.setParameters(parameters);
        } catch (Exception e) {

        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.e("CAMARA", "---- SURFACE CREATED ----");
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            subHandleRotationChange((Activity) mContext);
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d("CAMARA", "Error setting camera preview: " + e.getMessage());
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.e("CAMARA", "---- SURFACE DESTROYED ----");
    }

}