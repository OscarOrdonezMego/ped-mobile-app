package pe.com.nextel.android.util;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.Stack;

/**
 * Clase que realiza del gesti&#243; del {@link Stack} de actividades de la aplicaci&#243;n
 *
 * @author jroeder
 */
public class NexActivityGestor {
    /**
     * {@link Stack} de actividades de la aplicaci&#243;n android
     */
    private static Stack<Activity> ioStkActivity;
    /**
     * Instancia del {@link NexActivityGestor} de la aplicaci&#243;n android
     */
    private static NexActivityGestor ioInstance;

    /**
     * Retorna la instancia del {@link NexActivityGestor}
     *
     * @return NexActivityGestor - Instancia de la clase
     */
    public static NexActivityGestor getInstance() {
        if (ioInstance == null) {
            ioInstance = new NexActivityGestor();
        }
        return ioInstance;
    }

    /**
     * A&#241;ade la actividad al {@link Stack} del {@link NexActivityGestor}
     *
     * @param poActivity Actividad de la aplicaci&#243;n android
     */
    public void addActivity(Activity poActivity) {
        if (ioStkActivity == null) {
            ioStkActivity = new Stack<Activity>();
        }
        ioStkActivity.add(poActivity);
    }

    /**
     * Finaliza la &#250;ltima actividad agregada al {@link Stack} del {@link NexActivityGestor}
     */
    public void finishActivity() {
        Activity loActivity = ioStkActivity.lastElement();
        finishActivity(loActivity);
    }

    /**
     * Finaliza una actividad agregada al {@link Stack} del {@link NexActivityGestor}
     *
     * @param poActivity Actividad de la aplicaci&#243;n android
     */
    public void finishActivity(Activity poActivity) {
        if (poActivity != null) {
            ioStkActivity.remove(poActivity);
//			poActivity.finish();
            poActivity = null;
        }
    }

    private void killActivity(Activity poActivity) {
        if (poActivity != null) {
            ioStkActivity.remove(poActivity);
            poActivity.finish();
            poActivity = null;
        }
    }

    /**
     * Destruye todas las actividades de la pila excepto por la actividad quien invoca el m&#233;todo
     *
     * @param poActivity Actividad de la aplicaci&#243;n android
     */
    public void killActivityAllButThis(Activity poActivity) {
        int _counter = ioStkActivity.size() - 1;
        while (!ioStkActivity.isEmpty() && ioStkActivity.size() > 1 && _counter >= 0) {
            Activity act = ioStkActivity.get(_counter--);
            if (act != null && act != poActivity) {
                killActivity(act);
            }
        }
    }

    /**
     * Reinicia la aplicaci&#243;n m&#243;vil
     *
     * @param poContext {@link Context} de la aplici&#243;n
     */
    public void restartApplication(Context poContext) {
        if (poContext instanceof Activity)
            killActivityAllButThis((Activity) poContext);
        Intent _intent = poContext.getPackageManager().getLaunchIntentForPackage(poContext.getPackageName());
        _intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        poContext.startActivity(_intent);
    }

    /**
     * Finaliza las actividades del {@link Stack} del {@link NexActivityGestor} y reinicia la aplicaci&#243;n y paquete
     *
     * @param poContext Contexto de la aplicaci&#243;n
     */
    @SuppressWarnings("deprecation")
    public void exitApplication(Context poContext) {
        try {
            String lsPkgName = poContext.getPackageName();
            ActivityManager loActMgr = (ActivityManager) poContext.getSystemService(Context.ACTIVITY_SERVICE);
//			List<ComponentName> _lstServices = Utilitario.obtainRunningService(poContext);
//			if(_lstServices != null){
//				for (ComponentName component : _lstServices) {
//					Intent intent = new Intent();
//					intent.setClassName(component.getPackageName(), component.getClassName());
//					poContext.stopService(intent);
//				}
//			}
            while (!ioStkActivity.isEmpty()) {
                Activity act = ioStkActivity.lastElement();
                if (act != null) {
                    killActivity(act);
                }
            }
            loActMgr.restartPackage(lsPkgName);
            System.exit(0);
        } catch (Exception e) {
            Log.e("ActivityGestor", "NexActivityGestor.exitApplication", e);
        }
    }
}
