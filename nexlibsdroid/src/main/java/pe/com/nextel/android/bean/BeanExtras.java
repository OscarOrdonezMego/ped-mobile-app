package pe.com.nextel.android.bean;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import pe.com.nextel.android.util.ConfiguracionNextel.EnumFlujo;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipBusqueda;

/**
 * Clase para contener datos extras, utilizada como valor serializado en el {@link Bundle} Extras de un {@link Intent}
 *
 * @author jroeder
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BeanExtras {

    @JsonProperty
    private String filtro;
    @JsonProperty
    private String filtro2;
    @JsonProperty
    private String filtro3;
    @JsonProperty
    private String filtro4;
    @JsonProperty
    private String filtro5 = "";
    @JsonProperty
    private String filtro6;
    @JsonProperty
    private String filtro7;
    @JsonProperty
    private String filtro8;
    @JsonProperty
    private String filtro9;
    @JsonProperty
    private String filtro10;
    @JsonProperty
    private String filtro11;
    @JsonProperty
    private int posLista = 0;
    @JsonProperty
    private EnumTipBusqueda tipo;
    @JsonProperty
    private EnumFlujo flujo;
    @JsonProperty
    private boolean unicoResultado;

    /**
     * Instancia el valor de filtro
     *
     * @param filtro Valor de filtro, utilizado para b&#250;squedas
     */
    public void setFiltro(String filtro) {
        this.filtro = filtro;
    }

    /**
     * Retorna el valor de filtro
     *
     * @return {String}
     */
    public String getFiltro() {
        return filtro;
    }

    /**
     * Constructor de la clase
     *
     * @param psFiltro Valor de filtro
     */
    public BeanExtras(String psFiltro) {
        this.filtro = psFiltro;
    }

    /***
     * Constructor de la clase
     */
    public BeanExtras() {

    }

    /**
     * Instancia el tipo de b&#250;squeda
     *
     * @param tipo Tipo de b&#243;squeda {@link EnumTipBusqueda}
     */
    public void setTipo(EnumTipBusqueda tipo) {
        this.tipo = tipo;
    }

    /**
     * Retorna el tipo de b&#250;squeda
     *
     * @return {@link EnumTipBusqueda}
     */
    public EnumTipBusqueda getTipo() {
        return tipo;
    }

    /**
     * Instancia el tipo de flujo
     *
     * @param flujo Tipo de flujo {@link EnumFlujo}
     */
    public void setFlujo(EnumFlujo flujo) {
        this.flujo = flujo;
    }

    /**
     * Retorna el tipo de flujo
     *
     * @return {@link EnumFlujo}
     */
    public EnumFlujo getFlujo() {
        return flujo;
    }

    /**
     * Instancia el valor de filtro 2
     *
     * @param filtro2 Valor de filtro 2
     */
    public void setFiltro2(String filtro2) {
        this.filtro2 = filtro2;
    }

    /**
     * Retorna el valor de filtro 2
     *
     * @return {String}
     */
    public String getFiltro2() {
        return filtro2;
    }

    /**
     * Instancia el valor de filtro 3
     *
     * @param filtro3 Valor de filtro 3
     */
    public void setFiltro3(String filtro3) {
        this.filtro3 = filtro3;
    }

    /**
     * Retorna el valor de filtro 3
     *
     * @return {String}
     */
    public String getFiltro3() {
        return filtro3;
    }

    /**
     * Instancia el valor de filtro 4
     *
     * @param filtro4 Valor de filtro 4
     */
    public void setFiltro4(String filtro4) {
        this.filtro4 = filtro4;
    }

    /**
     * Retorna el valor de filtro 4
     *
     * @return {String}
     */
    public String getFiltro4() {
        return filtro4;
    }

    /**
     * Retorna el valor de filtro 5
     *
     * @return {String}
     */
    public String getFiltro5() {
        return filtro5;
    }

    /**
     * Instancia el valor de filtro 5
     *
     * @param filtro5 Valor de filtro 5
     */
    public void setFiltro5(String filtro5) {
        this.filtro5 = filtro5;
    }

    /**
     * Retorna el valor de filtro 8
     *
     * @return {String}
     */
    public String getFiltro8() {
        return filtro8;
    }

    /**
     * Instancia el valor de filtro 8
     *
     * @param filtro8 Valor de filtro 8
     */
    public void setFiltro8(String filtro8) {
        this.filtro8 = filtro8;
    }

    /**
     * Retorna el valor de filtro 9
     *
     * @return {String}
     */
    public String getFiltro9() {
        return filtro9;
    }

    /**
     * Instancia el valor de filtro 9
     *
     * @param filtro9 Valor de filtro 9
     */
    public void setFiltro9(String filtro9) {
        this.filtro9 = filtro9;
    }

    /**
     * Retorna el valor de filtro 10
     *
     * @return {String}
     */
    public String getFiltro10() {
        return filtro10;
    }

    /**
     * Instancia el valor de filtro 10
     *
     * @param filtro10 Valor de filtro 10
     */
    public void setFiltro10(String filtro10) {
        this.filtro10 = filtro10;
    }

    /**
     * Realiza la impresi&#243;n de los valores del {@link BeanExtras} en el Log a modo de debug
     *
     * @param poContext  Contexto de la aplicaci&#243;n
     * @param psF1       Nombre de filtro
     * @param psF2       Nombre de filtro 2
     * @param psF3       Nombre de filtro 3
     * @param psF4       Nombre de filtro 4
     * @param psF5       Nombre de filtro 5
     * @param psF6       Nombre de filtro 6
     * @param psNomFlujo Nombre del Flujo
     */
    public void subLog(Context poContext, String psF1, String psF2, String psF3, String psF4, String psF5, String psF6, String psNomFlujo) {
        Log.v("BE", "=============" + poContext.getClass().getName() + "=================");
        Log.v("BE", "filtro===>" + filtro + "(" + psF1 + ")");
        Log.v("BE", "filtro2===>" + filtro2 + "(" + psF2 + ")");
        Log.v("BE", "filtro3===>" + filtro3 + "(" + psF3 + ")");
        Log.v("BE", "filtro4===>" + filtro4 + "(" + psF4 + ")");
        Log.v("BE", "filtro5===>" + filtro5 + "(" + psF5 + ")");
        Log.v("BE", "filtro6===>" + filtro6 + "(" + psF6 + ")");
        if (tipo == null) {
            Log.v("BE", "tipo===>Null");
        } else {
            if (tipo == EnumTipBusqueda.CODIGO) {
                Log.v("BE", "tipo===>busqueda por Codigo");
            } else if (tipo == EnumTipBusqueda.NOMBRE) {
                Log.v("BE", "tipo===>Busqueda poNombre");
            } else if (tipo == EnumTipBusqueda.OTRO) {
                Log.v("BE", "tipo===>Busqueda por Otro");
            } else if (tipo == EnumTipBusqueda.OTRO2) {
                Log.v("BE", "tipo===>Busqueda por Otro2");
            } else if (tipo == EnumTipBusqueda.OTRO3) {
                Log.v("BE", "tipo===>Busqueda por Otro3");
            }
        }
        if (flujo == null) {
            Log.v("BE", "flujo===>Null");
        } else {
            if (flujo == EnumFlujo.FLUJO1) {
                Log.v("BE", "flujo===>Flujo1" + "(" + psNomFlujo + ")");
            } else if (flujo == EnumFlujo.FLUJO2) {
                Log.v("BE", "flujo===>Flujo2" + "(" + psNomFlujo + ")");
            } else if (flujo == EnumFlujo.FLUJO3) {
                Log.v("BE", "flujo===>Flujo3" + "(" + psNomFlujo + ")");
            } else if (flujo == EnumFlujo.FLUJO4) {
                Log.v("BE", "flujo===>Flujo4" + "(" + psNomFlujo + ")");
            }
        }
    }

    /**
     * Realiza la impresi&#243;n de los valores del {@link BeanExtras} en el Log a modo de debug
     *
     * @param poContext Contexto de la aplicaci&#243;n
     */
    public void subLog(Context poContext) {
        Log.v("BE", "=============" + poContext.getClass().getName() + "=================");
        Log.v("BE", "filtro===>" + filtro);
        Log.v("BE", "filtro2===>" + filtro2);
        Log.v("BE", "filtro3===>" + filtro3);
        Log.v("BE", "filtro4===>" + filtro4);
        Log.v("BE", "filtro5===>" + filtro5);
        Log.v("BE", "filtro5===>" + filtro6);
        if (tipo == null) {
            Log.v("BE", "tipo===>Null");
        } else {
            if (tipo == EnumTipBusqueda.CODIGO) {
                Log.v("BE", "tipo===>busqueda por Codigo");
            } else if (tipo == EnumTipBusqueda.NOMBRE) {
                Log.v("BE", "tipo===>Busqueda poNombre");
            } else if (tipo == EnumTipBusqueda.OTRO) {
                Log.v("BE", "tipo===>Busqueda por Otro");
            } else if (tipo == EnumTipBusqueda.OTRO2) {
                Log.v("BE", "tipo===>Busqueda por Otro2");
            } else if (tipo == EnumTipBusqueda.OTRO3) {
                Log.v("BE", "tipo===>Busqueda por Otro3");
            }
        }
        if (flujo == null) {
            Log.v("BE", "flujo===>Null");
        } else {
            if (flujo == EnumFlujo.FLUJO1) {
                Log.v("BE", "flujo===>Flujo1");
            } else if (flujo == EnumFlujo.FLUJO2) {
                Log.v("BE", "flujo===>Flujo2");
            } else if (flujo == EnumFlujo.FLUJO3) {
                Log.v("BE", "flujo===>Flujo3");
            } else if (flujo == EnumFlujo.FLUJO4) {
                Log.v("BE", "flujo===>Flujo4");
            }
        }
    }

    /**
     * Retorna el valor de filtro 6
     *
     * @return {String}
     */
    public String getFiltro6() {
        return filtro6;
    }

    /**
     * Instancia el valor de filtro 6
     *
     * @param filtro6 Valor de filtro 6
     */
    public void setFiltro6(String filtro6) {
        this.filtro6 = filtro6;
    }

    /**
     * Retorna el indicador de si es resultado &#250;nico o no
     *
     * @return boolean
     */
    public boolean isUnicoResultado() {
        return unicoResultado;
    }

    /**
     * Instancia el indicador de si es resultado &#250;nico o no
     *
     * @param unicoResultado
     */
    public void setUnicoResultado(boolean unicoResultado) {
        this.unicoResultado = unicoResultado;
    }

    /**
     * Retorna la posici&#243;n de la lista
     *
     * @return int
     */
    public int getPosLista() {
        return posLista;
    }

    /**
     * Instancia la posici&#243;n de la lista
     *
     * @param posLista
     */
    public void setPosLista(int posLista) {
        this.posLista = posLista;
    }

    /**
     * Retorna el valor de filtro 7
     *
     * @return {String}
     */
    public String getFiltro7() {
        return filtro7;
    }

    /**
     * Instancia el valor de filtro 7
     *
     * @param filtro7 Valor de filtro 7
     */
    public void setFiltro7(String filtro7) {
        this.filtro7 = filtro7;
    }

    /**
     * Retorna el valor de filtro 11
     *
     * @return {String}
     */
    public String getFiltro11() {
        return filtro11;
    }

    /**
     * Instancia el valor de filtro 11
     *
     * @param filtro11 Valor de filtro 11
     */
    public void setFiltro11(String filtro11) {
        this.filtro11 = filtro11;
    }
}
