package pe.com.nextel.android.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import pe.com.nextel.android.R;

/**
 * Clase que realiza la gesti&#243;n del GPS y Localizaci&#243;n del m&#243;vil
 *
 * @author jroeder
 */
public class Gps {
    /**
     * Mejor localizaci&#243;n obtenida
     */
    public static Location currentBestLocation;
    private static LocationManager lm;
    private static boolean gps_enabled = false;
    private static boolean network_enabled = false;
    /**
     * Constante con valor equivalente a 10 segundos en milisegundos
     */
    public static int TEN_SECONDS = 10000;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    /**
     * Constante de mensaje
     */
    public static String mensaje;
    private static final int TWO_MINUTES = 1000 * 60 * 2;
    // flag for GPS status
    private static boolean canGetLocation = false;
    private static LocationResult locationResult;


    public static boolean isCanGetLocation() {
        return canGetLocation;
    }


    public static void setCanGetLocation(boolean canGetLocation) {
        Gps.canGetLocation = canGetLocation;
    }

    public static void setLocationResult(LocationResult poResult) {
        locationResult = poResult;
    }

    /**
     * Inicia el {@link LocationListener} y obtiene la mejor ubicaci&#243;n que no pase de los 2 minutos de antig&#252;edad
     *
     * @param context - Contexto de la aplicaci&#243;n
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si se logra registrar por lo menos un proveedor</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    @SuppressWarnings("MissingPermission")
    public static boolean startListening(Context context) throws Exception {

        //if (!Utilitario.fnVerOperador(context))
        //    throw new NexInvalidOperatorException(context);

        // I use LocationResult callback class to pass location value from
        // MyLocation to user code.
        if (lm == null)
            lm = (LocationManager) context
                    .getSystemService(Context.LOCATION_SERVICE);
        System.out.println(lm.getAllProviders().toString());

        // exceptions will be thrown if provider is not permitted.
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = lm
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        // don't start listeners if no provider is enabled
        if (!gps_enabled && !network_enabled) {
            setCanGetLocation(false);
            return false;
        }
        if (gps_enabled) {
            setCanGetLocation(true);
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, TEN_SECONDS,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
            System.out.println("GPS Registered");
            mensaje = "GPS Registered";
        }

        if (network_enabled) {
            setCanGetLocation(true);
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    TEN_SECONDS, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
            System.out.println("Network Registered");
            mensaje = "Network Registered";
        }

        return true;
    }

    /**
     * Inicia el {@link LocationListener} y obtiene la mejor ubicaci&#243;n que no pase de los 2 minutos de antig&#252;edad
     *
     * @param context          - Contexto de la aplicaci&#243;n
     * @param poLocationResult - Callback para cuando se ha encontrado una coordenada v&#225;lida.
     * @return retorna true si se logra registrar por lo menos un proveedor, de lo contrario, falso
     */
    public static boolean startListening(Context context, LocationResult poLocationResult) throws Exception {

        setLocationResult(poLocationResult);

        return Gps.startListening(context);
    }

    /**
     * retorna la mejor ubicaci&#243;n obtenida dentro de los 2 minutos o la &#250;ltima ubicaci&#243;n conocida.
     *
     * @return {@link Location}
     */
    @SuppressWarnings("MissingPermission")
    public static Location getLocation() throws Exception {
        Location gps= null, net;

        if (currentBestLocation == null) {
            if (lm.getLastKnownLocation(LocationManager.GPS_PROVIDER)!=null)
                gps = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            net = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (gps == null)
                gps_enabled = false;
            if (gps_enabled & network_enabled) {
                if (isBetterLocation(gps, net)) {
                    currentBestLocation = gps;
                    mensaje = "GPS Registered";
                } else {
                    currentBestLocation = net;
                    mensaje = "Network Registered";
                }
            } else if (gps_enabled) {
                mensaje = "GPS Registered";
                currentBestLocation = gps;
            } else if (network_enabled) {
                mensaje = "Network Registered";
                currentBestLocation = net;
            }
        }
        return currentBestLocation;
    }

    public static String getMensaje() {
        return mensaje;
    }

    /**
     * Detiene los listeners
     */
    @SuppressWarnings("MissingPermission")
    public static void stopListening() {
        try {
            lm.removeUpdates(locationListener);
            locationResult = null;
        } catch (NullPointerException e) {

        }
    }

    /**
     * Retorna un string que indica el estado del GPS y red
     *
     * @return {String}
     */
    public static String getLocationState() {
        StringBuilder s = new StringBuilder("network: ");
        if (network_enabled) {
            s.append("ON");
        } else {
            s.append("OFF");
        }
        s.append("\ngps: ");
        if (gps_enabled) {
            s.append("ON");
        } else {
            s.append("OFF");
        }
        return s.toString();
    }

    /**
     * Interfaz que implementa la captura de localizaciones
     */
    static LocationListener locationListener = new LocationListener() {

        @Override
        public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onProviderEnabled(String arg0) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onProviderDisabled(String arg0) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.d("Location", "Location Received: " + location.getLatitude() + "; " + location.getLongitude());
            try {
                if (isBetterLocation(location, currentBestLocation)) {
                    currentBestLocation = location;
                }
                if (locationResult != null) {
                    locationResult.gotLocation(getLocation());
                }
            } catch (Exception e) {
                Log.e("Location", "onLocationChanged", e);
            }
        }
    };

    /**
     * Muestra una alerta para configurar el GPS
     * en caso &#233;ste est&#225; desactivado.
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     */
    public static void showSettingsAlert(final Context poContext) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(poContext);

        // Setting Dialog Title
        alertDialog.setTitle(poContext.getString(R.string.dlg_titinformacion));

        // Setting Dialog Message
        alertDialog
                .setMessage(poContext.getString(R.string.msg_gpsdeshabilitado));

        // On pressing Settings button
        alertDialog.setPositiveButton(poContext.getString(R.string.act_btnconfiguracion),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        poContext.startActivity(intent);
                    }
                });

        // on pressing cancel button
        alertDialog.setNegativeButton(poContext.getString(R.string.act_btncancelar),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        // Showing Alert Message
        alertDialog.show();
    }


    /**
     * Verifica si un {@link Location} obtenido es mejor que el actual.
     *
     * @param location            La nueva localizaci&#243;n obtenida que se quiere evaluar
     * @param currentBestLocation La actual localizaci&#243;n obtenida.
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si la verificaci&#243;n es correcta</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    protected static boolean isBetterLocation(Location location,
                                              Location currentBestLocation) throws Exception {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use
        // the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be
            // worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation
                .getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and
        // accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate
                && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Verifica si 2 proveedores son iguales
     *
     * @param provider1 - Proveedor 1
     * @param provider2 - Proveedor 2
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si la verificaci&#243;n es correcta.</li>
     * <li><b>false</b> de lo contrario.</li>
     * </ul>
     */
    private static boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    /**
     * Clase que implementa el resultado de una localizaci&#243;n
     *
     * @author jroeder
     */
    public static abstract class LocationResult {
        /**
         * Invocado para realizar una acci&#243;n cuando se realiza la localizaci&#243;n desp&#250;es de calcular la mejor localizaci&#243;n.
         *
         * @param location - La mejor localizaci&#243;n obtenida.
         */
        public abstract void gotLocation(Location location);
    }
}
