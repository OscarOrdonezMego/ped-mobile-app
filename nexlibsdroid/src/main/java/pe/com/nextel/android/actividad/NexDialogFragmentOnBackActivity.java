package pe.com.nextel.android.actividad;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import pe.com.nextel.android.util.DialogFragmentYesNo;

/**
 * Actividad invocada por el {@link #showNexDialog} con el m&#233;todo {@link #startActivityForResult} de las actividades del framework NexLibsDroid.
 * </br>Es utilizada como <b>work around</b> del <b>bug</b> del {@link DialogFragmentYesNo} cuando es invocado desde el {@link #onBackPressed}.
 * </br>Esta actividad al momento que termina el m&#233;todo {@link #onCreate}, se finaliza, retonando al {@link #onActivityResult} de la actividad
 * quien la invoc&#243;.
 * </br>Se debe declarar en el <b>AndroidManifest.xml</b> la siguiente l&#237;nea:</br>
 * </br>&#60;activity android:name="pe.com.nextel.android.actividad.NexDialogFragmentOnBackActivity"/&#62;
 *
 * @author jroeder
 */
public final class NexDialogFragmentOnBackActivity extends NexActivity {

    @Override
    public final void onBackPressed() {

    }

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(RESULT_OK, new Intent(getIntent()));
        finish();
    }

    @Override
    protected final void onResume() {
        super.onResume();
    }

    @Override
    protected final void onStart() {
        super.onStart();
    }

    @Override
    protected final void onPause() {
        super.onPause();
    }

    @Override
    protected final void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected final void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected final void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected final void subIniActionBar() {
        getActionBarGD().setVisibility(View.GONE);
    }

    @Override
    protected final void subSetControles() {

    }
}
