package pe.com.nextel.android.dal;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;

import pe.com.nextel.android.actividad.NexInvalidOperatorActivity.NexInvalidOperatorException;

/**
 * Esta clase gestiona el acceso a base de datos SQLite
 *
 * @author jroeder
 * @version 1.4
 */
public class DALGestor {
    /**
     * Variable global para valores cadena nulas
     */
    public static final String STRING_NULL = "null";
    /**
     * Variable global para valores num&#233;ricos nulos
     */
    public static final int NUMERIC_NULL = -999999;
    /**
     * Instancia del Contexto de la aplicaci&#243;n
     */
    private Context ioContext;
    /**
     * Instancia del nombre de la base de datos
     */
    private String isDataBaseName;
    /**
     * Instancia de Flag de Impresi&#243;n de Log
     */
    private boolean ibLogSQL;
    /**
     * Instancia de base de datos SQLite
     */
    private SQLiteDatabase myDB;
    /**
     * Instancia de Objeto de entidad
     */
    private Object ioObject;
    /**
     * Instancia de Error en query
     */
    private String isQueryError = "";

    /**
     * Devuelve el contexto de la aplicaci&#243;n
     *
     * @return Context - Contexto de la aplicaci&#243;n
     */
    public Context getContext() {
        return ioContext;
    }

    /**
     * Coloca el Contexto de la aplicaci&#243;n
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     */
    public void setContext(Context poContext) {
        this.ioContext = poContext;
    }

    /**
     * Devuelve el nombre de la base de datos
     *
     * @return String - Nombre de la base de datos
     */
    public String getDataBaseName() {
        return isDataBaseName;
    }

    /**
     * Coloca el nombre de la base de datos
     *
     * @param psDataBaseName - Nombre de la base de datos
     */
    public void setDataBaseName(String psDataBaseName) {
        this.isDataBaseName = psDataBaseName;
    }

    /**
     * Constructor de la clase
     *
     * @param poContext      - Contexto de la aplicaci&#243;n
     * @param psDataBaseName - Nombre de la base de datos
     */
    public DALGestor(Context poContext, String psDataBaseName) throws NexInvalidOperatorException {
        //if(!Utilitario.fnVerOperador(poContext))
        //throw new NexInvalidOperatorException(poContext);
        ioContext = poContext;
        isDataBaseName = psDataBaseName;
        setLogSQL(true);
    }

    /**
     * Constructor de la clase
     *
     * @param poContext      - Contexto de la aplicaci&#243;n
     * @param psDataBaseName - Nombre de la base de datos
     * @param pbLogSQL       - Flag de Log de SQL
     */
    public DALGestor(Context poContext, String psDataBaseName, boolean pbLogSQL) throws NexInvalidOperatorException {
        //if(!Utilitario.fnVerOperador(poContext))
        //	throw new NexInvalidOperatorException(poContext);
        ioContext = poContext;
        isDataBaseName = psDataBaseName;
        setLogSQL(pbLogSQL);
    }

    /**
     * Permite ejecutar un query o sentencia sql, retornando un valor booleano
     * "True" si se ha ejecutado, "False" de lo contrario.</br>
     * En caso si se ha generado una excepci&#243;n en la ejecuci&#243;n, llamar al m&#233;todo
     * {@link #getQueryError} para obtener la descripci&#243;n del mismo.
     *
     * @param psQuery - Sentencia SQL
     * @return boolean - "True" si es correcto, "False" si hay problemas
     * @throws DALException
     */
    protected boolean executeSQLQuery(String psQuery) throws DALException {
        if (isLogSQL()) Log.v("DALGestor", "executeSQLQuery");
        boolean lbExecute = true;
        setQueryError("");
        myDB = null;
        myDB = ioContext.openOrCreateDatabase(isDataBaseName, Context.MODE_PRIVATE, null);
        try {
            if (isLogSQL()) Log.v("SQL", psQuery);
            myDB.execSQL(psQuery);
        } catch (Exception e) {
            if (isLogSQL()) Log.e("DALGestor", "" + e);
            lbExecute = false;
            setQueryError("" + e);
            throw new DALException(e);
        } finally {
            closeDB();
        }
        return lbExecute;
    }

    /**
     * Devuelve un Cursor con el resultado de la ejecuci&#243;n del query o sentencia en el
     * argumento
     *
     * @param psQuery - Sentencia SQL
     * @return Cursor - {@link Cursor} o dataset de resultado
     * @throws DALException - Arroja una excepci&#243;n en caso existan problemas en la ejecuci&#243;n
     */
    @SuppressWarnings("deprecation")
    protected Cursor getSQLQuery(String psQuery) throws DALException {
        if (isLogSQL()) Log.v("DALGestor", "getSQLQuery");
        try {
            setQueryError("");
            myDB = null;
            myDB = ioContext.openOrCreateDatabase(isDataBaseName, Context.MODE_PRIVATE, null);
            Cursor loCursor;
            if (isLogSQL()) Log.v("SQL", psQuery);
            loCursor = myDB.rawQuery(psQuery, null);
            //myDB.close();
            return loCursor;
        } catch (Exception e) {
            setQueryError("" + e);
            throw new DALException(e);
        }
    }

    /**
     * Permite ejecutar una sentencia SQL que retorna un dataset como resultado,
     * asignando lo valores seg&#250;n la implementaci&#243;n del m&#233;todo {@link RowListener#setRow setRow} del
     * {@link RowListener}
     *
     * @param psQuery       - Sentencia SQL
     * @param poRowListener - Objeto que implementa un {@link RowListener}
     * @throws DALException - Arroja una excepci&#243;n en caso de problemas en la ejecuci&#243;n
     */
    @SuppressWarnings("deprecation")
    protected void getSQLQuery(String psQuery, RowListener poRowListener) throws DALException {
        if (isLogSQL()) Log.v("DALGestor", "getSQLQuery");
        try {
            setQueryError("");
            setObject(null);
            myDB = null;
            myDB = ioContext.openOrCreateDatabase(isDataBaseName, Context.MODE_PRIVATE, null);
            Cursor loCursor;
            if (isLogSQL()) Log.v("SQL", psQuery);
            loCursor = myDB.rawQuery(psQuery, null);

            if (loCursor != null) {
                if (loCursor.moveToFirst()) {
                    long liPosition = 0;
                    do {
                        poRowListener.setRow(liPosition, loCursor);
                        liPosition++;
                    } while (loCursor.moveToNext());
                }
            }
            loCursor.deactivate();
            loCursor.close();
        } catch (Exception e) {
            setQueryError("" + e);
            throw new DALException(e);
        } finally {
            closeDB();
        }
    }

    /**
     * Permite ejecutar una sentencia SQL que retorna un dataset como resultado,
     * asignando los valores seg&#250;n la implementaci&#243;n del m&#233;todo {@link RowListener#setRow setRow} del
     * {@link RowListener} que retorna el m&#233;todo {@link RowCallBack#getRowListener getRowListener} del {@link RowCallBack}</br>
     * Este m&#233;todo puede ser utilizado para retornar una lista de filas del {@link Cursor}
     *
     * @param psQuery       - Sentencia SQL
     * @param poLstObject   - Lista de Objectos que implementan un {@link RowListener}
     * @param poRowCallBack - Objeto que implementa un {@link RowCallBack}
     * @throws DALException - Arroja una excepci&#243;n en caso de problemas en la ejecuci&#243;n
     */
    @SuppressWarnings({"deprecation", "unchecked"})
    protected void getSQLQuery(String psQuery, List<?> poLstObject, RowCallBack poRowCallBack) throws DALException {
        if (isLogSQL()) Log.v("DALGestor", "getSQLQuery");
        try {
            setQueryError("");
            setObject(null);
            myDB = null;
            myDB = ioContext.openOrCreateDatabase(isDataBaseName, Context.MODE_PRIVATE, null);
            Cursor loCursor;
            if (isLogSQL()) Log.v("SQL", psQuery);
            loCursor = myDB.rawQuery(psQuery, null);

            if (loCursor != null) {
                if (loCursor.moveToFirst()) {
                    long liPosition = 0;
                    do {
                        RowListener loRowListener = poRowCallBack.getRowListener();
                        loRowListener.setRow(liPosition, loCursor);
                        ((List<RowListener>) poLstObject).add(loRowListener);
                        liPosition++;
                    } while (loCursor.moveToNext());
                }
            }
            loCursor.deactivate();
            loCursor.close();
        } catch (Exception e) {
            setQueryError("" + e);
            throw new DALException(e);
        } finally {
            closeDB();
        }
    }

    /**
     * Permite ejecutar una sentencia SQL que retorna un dataset como resultado,
     * asignando lo valores seg&#250;n la implementaci&#243;n del m&#233;todo {@link RowListener#setRow setRow} del
     * {@link RowListener} que retorna el m&#233;todo {@link RowCallBack#getRowListener getRowListener} del {@link RowCallBack}</br>
     * Este m&#233;todo puede ser utilizado para retornar una fila del {@link Cursor}
     *
     * @param psQuery       - Sentencia SQL
     * @param poRowCallBack - Objeto que implementa un {@link RowCallBack}
     * @throws DALException - Arroja una excepci&#243;n en caso de problemas en la ejecuci&#243;n
     */
    @SuppressWarnings("deprecation")
    protected void getSQLQuery(String psQuery, RowCallBack poRowCallBack) throws DALException {
        if (isLogSQL()) Log.v("DALGestor", "getSQLQuery");
        try {
            setQueryError("");
            setObject(null);
            myDB = null;
            myDB = ioContext.openOrCreateDatabase(isDataBaseName, Context.MODE_PRIVATE, null);
            Cursor loCursor;
            if (isLogSQL()) Log.v("SQL", psQuery);
            loCursor = myDB.rawQuery(psQuery, null);

            if (loCursor != null) {
                if (loCursor.moveToFirst()) {
                    long liPosition = 0;
                    do {
                        RowListener loRowListener = poRowCallBack.getRowListener();
                        loRowListener.setRow(liPosition, loCursor);
                        setObject(loRowListener);
                        liPosition++;
                    } while (loCursor.moveToNext());
                }
            }
            loCursor.deactivate();
            loCursor.close();
        } catch (Exception e) {
            setQueryError("" + e);
            throw new DALException(e);
        } finally {
            closeDB();
        }
    }

//	public SQLiteDatabase getMyDB(){
//		return myDB;
//	}

    /**
     * Cierra la base de datos abierta
     */
    protected void closeDB() {
        if (isLogSQL()) Log.v("DALGestor", "closeDB");
        try {
            myDB.close();
        } catch (Exception e) {
            if (isLogSQL()) Log.e("DALGestor", "" + e);
        }
    }

    /**
     * Genera un script del SQL de conversi&#243;n
     * de fecha de formato <b>dd%MM%yyyy</b> a formato militar <b>yyyyMMdd</b>
     *
     * @param psColumnDate - Nombre de la columna en la tabla de la base de datos
     * @return String - Script de conversi&#243;n de columna fecha en String
     */
    public static String toStringDateYYYYMMDD(String psColumnDate) {
        return "(substr(" + psColumnDate + ",7,4)||substr(" + psColumnDate + ",4,2)||substr(" + psColumnDate + ",1,2))";
    }

    /**
     * Genera un script del SQL de conversi&#243;n
     * de fecha de formato <b>dd%MM%yyyy kk%mm%ss</b> a formato militar <b>yyyyMMddkkmmss</b>
     *
     * @param psColumnDate - Nombre de la columna en la tabla de la base de datos
     * @return String - Script de conversi&#243;n de columna fecha en String
     */
    public static String toStringDateYYYYMMDDKKmmSS(String psColumnDate) {
        String lsYYYYMMDD = toStringDateYYYYMMDD(psColumnDate);
        String lsKKmmSS = "(substr(" + psColumnDate + ",12,2)||substr(" + psColumnDate + ",15,2)||substr(" + psColumnDate + ",18,2))";
        return "(" + lsYYYYMMDD + "||" + lsKKmmSS + ")";
    }

    /**
     * Devuelve el objeto de entidad de base de datos. Puede ser un bean, entero,
     * booleano, cadena, etc.
     *
     * @return Object - Objeto de entidad de base de datos.
     */
    protected Object getObject() {
        return ioObject;
    }

    /**
     * Devuelve el objeto de entidad de base de datos. Puede ser un bean, entero,
     * booleano, cadena, etc.
     *
     * @param defValue - Valor por defecto
     * @return Object - Objeto de entidad de base de datos.
     */
    protected Object getObject(Object defValue) {
        return getObject() != null ? getObject() : defValue;
    }

    /**
     * Coloca el Objeto de entidad de base datos. Puede ser un bean, entero,
     * booleano, cadena, etc.
     *
     * @param poObject - Objeto de entidad de base de datos.
     */
    protected void setObject(Object poObject) {
        this.ioObject = poObject;
    }

    /**
     * Devuelve el &#250;ltimo error o excepci&#243;n de query que se ha generado
     *
     * @return String - Descripci&#243;n de la excepci&#243;n
     */
    public String getQueryError() {
        return isQueryError;
    }

    /**
     * Coloca la descripci&#243;n del error o excepci&#243;n del query o sentencia que se
     * ha generado al ejecutarse
     */
    protected void setQueryError(String psQueryError) {
        this.isQueryError = psQueryError;
    }

    /**
     * Verifica si una tabla existe
     *
     * @param psTableName - Cadena con el nombre de la tabla a verificar
     * @return boolean - TRUE si existe, FALSE de lo contrario
     * @throws DALException - Devuelve una excepci&#243;n si se ha generado un error en la consulta
     */
    protected boolean isExistsTable(String psTableName) throws DALException {
        if (isLogSQL()) Log.v("DALGestor", "isExistsTable " + psTableName);
        String loSbSql = " select distinct name " +
                " from sqlite_master " +
                " where upper(name) = '" + psTableName.toUpperCase() + "' ";

        getSQLQuery(loSbSql, new RowListener() {

            @Override
            public void setRow(long piPosition, Cursor poCursor) {
                setObject(!poCursor.getString(0).equals("") ? true : false);
            }
        });

        if (getObject() == null) {
            setObject(false);
        }

        return (Boolean) getObject();
    }

    /**
     * Convierte el valor de un objeto en su representaci&#243;n de cadena
     * para la creaci&#243;n del raw query. Si el objeto tiene valor null, retornar&#243; la cadena <b>"null"</b></br>
     * Este m&#233;todo realiza el reemplazo de un caracter comilla simple (<b>'</b>) por 2 comillas simples (<b>''</b>)
     *
     * @param poValue - Objeto a convertir en cadena
     * @return String - Representaci&#243;n en cadena para raw query del objeto
     */
    public static String fnValueQuery(Object poValue) {
        if (poValue instanceof String) {
            return poValue == null ? DALGestor.STRING_NULL : "'" + ((String) poValue).replace("'", "''") + "'";
        } else if (poValue instanceof Boolean) {
            return poValue == null ? DALGestor.STRING_NULL : "'" + ((Boolean) poValue ? "1" : "0") + "'";
        } else {
            return String.valueOf(poValue);
        }
    }

    /**
     * Retorna un objeto Boolean.
     *
     * @param psValue - Valor obtenido del query
     * @return Boolean - Objeto Boolean
     */
    public static Boolean fnBoolean(String psValue) {
        if (psValue.equals(DALGestor.STRING_NULL)) {
            return null;
        } else if (psValue.equals("1")) {
            return true;
        }
        return false;
    }

    /**
     * Retorna un objeto Boolean.
     *
     * @param poCursor      - Cursor del SQLite
     * @param piColumnIndex - &#205;ndice de la Columna
     * @return Boolean - Objeto Boolean
     */
    public static Boolean fnBoolean(Cursor poCursor, int piColumnIndex) {
        if (poCursor.isNull(piColumnIndex)) {
            return null;
        } else if (poCursor.getString(piColumnIndex).equals("1")) {
            return true;
        }
        return false;
    }

    /**
     * Retorna un objeto Boolean.
     *
     * @param poCursor      - Cursor del SQLite
     * @param piColumnIndex - &#205;ndice de la Columna
     * @param defValue      - Valor por defecto si la columna es de valor null
     * @return Boolean - Objeto Boolean
     */
    public static boolean fnBoolean(Cursor poCursor, int piColumnIndex, boolean defValue) {
        Boolean _value = fnBoolean(poCursor, piColumnIndex);
        return _value != null ? _value : defValue;
    }

    /**
     * Retorna un objeto Double
     *
     * @param pdValue - Valor obtenido del query
     * @return Double - Objeto Double
     */
    public static Double fnDouble(double pdValue) {
        if (pdValue == DALGestor.NUMERIC_NULL) {
            return null;
        }
        return pdValue;
    }

    /**
     * Retorna un objeto Double
     *
     * @param poCursor      - Cursor del SQLite
     * @param piColumnIndex - &#205;ndice de la Columna
     * @return Double - Objeto Double
     */
    public static Double fnDouble(Cursor poCursor, int piColumnIndex) {
        if (poCursor.isNull(piColumnIndex)) {
            return null;
        }
        return poCursor.getDouble(piColumnIndex);
    }

    /**
     * Retorna un objeto Double
     *
     * @param poCursor      - Cursor del SQLite
     * @param piColumnIndex - &#205;ndice de la Columna
     * @param defValue      - Valor por defecto si la columna es de valor null
     * @return Double - Objeto Double
     */
    public static double fnDouble(Cursor poCursor, int piColumnIndex, double defValue) {
        Double _value = fnDouble(poCursor, piColumnIndex);
        return _value != null ? _value : defValue;
    }

    /**
     * Retorna un objeto Long
     *
     * @param piValue - Valor obtenido del query
     * @return Long - Objeto Long
     */
    public static Long fnLong(long piValue) {
        if (piValue == DALGestor.NUMERIC_NULL) {
            return null;
        }
        return piValue;
    }

    /**
     * Retorna un objeto Long
     *
     * @param poCursor      - Cursor del SQLite
     * @param piColumnIndex - &#205;ndice de la Columna
     * @return Long - Objeto Long
     */
    public static Long fnLong(Cursor poCursor, int piColumnIndex) {
        if (poCursor.isNull(piColumnIndex)) {
            return null;
        }
        return poCursor.getLong(piColumnIndex);
    }

    /**
     * Retorna un objeto Long
     *
     * @param poCursor      - Cursor del SQLite
     * @param piColumnIndex - &#205;ndice de la Columna
     * @param defValue      - Valor por defecto si la columna es de valor null
     * @return Long - Objeto Long
     */
    public static long fnLong(Cursor poCursor, int piColumnIndex, long defValue) {
        Long _value = fnLong(poCursor, piColumnIndex);
        return _value != null ? _value : defValue;
    }

    /**
     * Retorna un objeto Integer
     *
     * @param piValue - Valor obtenido del query
     * @return Integer - Objeto Integer
     */
    public static Integer fnInteger(int piValue) {
        if (piValue == DALGestor.NUMERIC_NULL) {
            return null;
        }
        return piValue;
    }

    /**
     * Retorna un objeto Integer
     *
     * @param poCursor      - Cursor del SQLite
     * @param piColumnIndex - &#205;ndice de la Columna
     * @return Integer - Objeto Integer
     */
    public static Integer fnInteger(Cursor poCursor, int piColumnIndex) {
        if (poCursor.isNull(piColumnIndex)) {
            return null;
        }
        return poCursor.getInt(piColumnIndex);
    }

    /**
     * Retorna un objeto Integer
     *
     * @param poCursor      - Cursor del SQLite
     * @param piColumnIndex - &#205;ndice de la Columna
     * @param defValue      - Valor por defecto si la columna es de valor null
     * @return Integer - Objeto Integer
     */
    public static int fnInteger(Cursor poCursor, int piColumnIndex, int defValue) {
        Integer _value = fnInteger(poCursor, piColumnIndex);
        return _value != null ? _value : defValue;
    }

    /**
     * Retorna un objeto Float
     *
     * @param pdValue - Valor obtenido del query
     * @return Float - Objeto Float
     */
    public static Float fnFloat(float pdValue) {
        if (pdValue == DALGestor.NUMERIC_NULL) {
            return null;
        }
        return pdValue;
    }

    /**
     * Retorna un objeto Float
     *
     * @param poCursor      - Cursor del SQLite
     * @param piColumnIndex - &#205;ndice de la Columna
     * @return Float - Objeto Float
     */
    public static Float fnFloat(Cursor poCursor, int piColumnIndex) {
        if (poCursor.isNull(piColumnIndex)) {
            return null;
        }
        return poCursor.getFloat(piColumnIndex);
    }

    /**
     * Retorna un objeto Float
     *
     * @param poCursor      - Cursor del SQLite
     * @param piColumnIndex - &#205;ndice de la Columna
     * @param defValue      - Valor por defecto si la columna es de valor null
     * @return Float - Objeto Float
     */
    public static float fnFloat(Cursor poCursor, int piColumnIndex, float defValue) {
        Float _value = fnFloat(poCursor, piColumnIndex);
        return _value != null ? _value : defValue;
    }

    /**
     * Retorna un objeto String
     *
     * @param psValue - Valor obtenido del query
     * @return String - Objeto String
     */
    public static String fnString(String psValue) {
        if (psValue.equals(DALGestor.STRING_NULL)) {
            return null;
        }
        return psValue;
    }

    /**
     * Retorna un objeto String
     *
     * @param poCursor      - Cursor del SQLite
     * @param piColumnIndex - &#205;ndice de la Columna
     * @return String - Objeto String
     */
    public static String fnString(Cursor poCursor, int piColumnIndex) {
        if (poCursor.isNull(piColumnIndex)) {
            return null;
        }
        return poCursor.getString(piColumnIndex);
    }

    /**
     * Retorna un objeto String
     *
     * @param poCursor      - Cursor del SQLite
     * @param piColumnIndex - &#205;ndice de la Columna
     * @param defValue      - Valor por defecto si la columna es de valor null
     * @return String - Objeto String
     */
    public static String fnString(Cursor poCursor, int piColumnIndex, String defValue) {
        String _value = fnString(poCursor, piColumnIndex);
        return _value != null ? _value : defValue;
    }

    /**
     * Retorna la sentencia <b>ifnull</b> del SQLite
     * para columnas alfanum&#233;ricas, utilizando por defecto la variable <b>STRING_NULL</b>
     *
     * @param psColumn - Nombre de la columna
     * @return String - Sentencia ifnull
     */
    public static String isStringNull(String psColumn) {
        return "ifnull(" + psColumn + ", '" + DALGestor.STRING_NULL + "')";
    }

    /**
     * Retorna la sentencia <b>ifnull</b> del SQLite
     * para columnas num&#233;ricas, utilizando por defecto la variable <b>NUMERIC_NULL</b>
     *
     * @param psColumn - Nombre de la columna
     * @return String - Sentencia ifnull
     */
    public static String isNumericNull(String psColumn) {
        return "ifnull(" + psColumn + ", " + DALGestor.NUMERIC_NULL + ")";
    }

    /**
     * Retorna la sentencia <b>ifnull</b> del SQLite
     * para columnas alfanum&#233;ricas
     *
     * @param psColumn       - Nombre de la columna
     * @param psDefaultValue - Valor cadena por defecto
     * @return String - Sentencia ifnull
     */
    public static String isStringNull(String psColumn, String psDefaultValue) {
        return "ifnull(" + psColumn + ", '" + psDefaultValue + "')";
    }

    /**
     * Retorna la sentencia <b>ifnull</b> del SQLite
     * para columnas num&#233;ricas
     *
     * @param psColumn       - Nombre de la columna
     * @param piDefaultValue - Valor entero por defecto
     * @return String - Sentencia ifnull
     */
    public static String isNumericNull(String psColumn, int piDefaultValue) {
        return "ifnull(" + psColumn + ", " + String.valueOf(piDefaultValue) + ")";
    }

    /**
     * Retorna la sentencia <b>ifnull</b> del SQLite
     * para columnas num&#233;ricas
     *
     * @param psColumn       - Nombre de la columna
     * @param piDefaultValue - Valor long por defecto
     * @return String - Sentencia ifnull
     */
    public static String isNumericNull(String psColumn, long piDefaultValue) {
        return "ifnull(" + psColumn + ", " + String.valueOf(piDefaultValue) + ")";
    }

    /**
     * Retorna la sentencia <b>ifnull</b> del SQLite
     * para columnas num&#233;ricas
     *
     * @param psColumn       - Nombre de la columna
     * @param pdDefaultValue - Valor double por defecto
     * @return String - Sentencia ifnull
     */
    public static String isNumericNull(String psColumn, double pdDefaultValue) {
        return "ifnull(" + psColumn + ", " + String.valueOf(pdDefaultValue) + ")";
    }

    /**
     * Retorna la sentencia <b>ifnull</b> del SQLite
     * para columnas num&#233;ricas
     *
     * @param psColumn       - Nombre de la columna
     * @param pdDefaultValue - Valore float por defecto
     * @return String - Sentencia ifnull
     */
    public static String isNumericNull(String psColumn, float pdDefaultValue) {
        return "ifnull(" + psColumn + ", " + String.valueOf(pdDefaultValue) + ")";
    }

    /**
     * Retorna el indicador de impresi&#243;n de Log de SQL
     *
     * @return boolean
     */
    public boolean isLogSQL() {
        return ibLogSQL;
    }

    /**
     * Instancia el indicador de impresi&#243;n de Log de SQL
     *
     * @param ibLogSQL - Indicador de impresi&#243;n
     */
    public void setLogSQL(boolean ibLogSQL) {
        this.ibLogSQL = ibLogSQL;
    }

    /**
     * Clase Interfaz que implementa la asignaci&#243;n de datos del dataset o resultado
     * de una sentencia en un arreglo de objetos definido por el programador.
     *
     * @author jroeder
     * @version 1.0
     */
    public static interface RowListener {
        /**
         * Asigna los valores del dataset de un {@link Cursor} en un arreglo de objetos, definido en la
         * implementaci&#243;n del programador de este m&#233;todo.
         *
         * @param piPosition - Posici&#243;n de la fila de Cursor
         * @param poCursor   - Cursor del dataset de base de datos
         */
        public void setRow(long piPosition, Cursor poCursor) throws Exception;
    }

    /**
     * Clase Interfaz que implementa el retorno de un {@link RowListener}.</br>
     * Esta clase puede ser implementada en los objetos Bean o &#205;tem donde se asignan los valores.
     *
     * @author jroeder
     * @version 1.0
     */
    public static interface RowCallBack {
        /**
         * Retorna un {@link RowListener}
         *
         * @return RowCallBack
         */
        public RowListener getRowListener();
    }

    /**
     * Instancia un nuevo contexto de la aplicaci&#243;n
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     */
    public void attach(Context poContext) {
        ioContext = poContext;
    }

    /**
     * Desajunta el contexto de la aplicaci&#243;n
     */
    public void dettach() {
        ioContext = null;
    }

    /**
     * Clase Exception propia del DALGestor, para el control interno de excepciones
     * al ejecutar las consultas
     *
     * @author jroeder
     * @version 1.0
     */
    @SuppressWarnings("serial")
    public static class DALException extends Exception {
        public DALException(Exception poException) {
            this(poException.getMessage(), poException.getCause());
        }

        public DALException() {
            super();
        }

        public DALException(String psMessageError) {
            super(psMessageError);
        }

        public DALException(Throwable poThrowable) {
            super(poThrowable);
        }

        public DALException(String psMessageError, Throwable poThrowable) {
            super(psMessageError, poThrowable);
        }
    }

    /**
     * Retorna el &#250;ltimo &#237;ndice identity de una tabla.</br>
     * Por defecto el nombre del identity es _id
     *
     * @param psTableName - Nombre de la tabla
     * @return Long - &#205;ndice identity. Retorna null en caso no existiese registros
     * @throws DALException
     */
    public Long getLastIndentity(String psTableName) throws DALException {
        getSQLQuery("SELECT _id FROM " + psTableName + " ORDER BY _id DESC LIMIT 1", new RowListener() {

            @Override
            public void setRow(long piPosition, Cursor poCursor) throws Exception {
                setObject(fnLong(poCursor.getLong(0)));
            }
        });
        return (Long) getObject();
    }

    /**
     * Retorna el &#250;ltimo &#237;ndice identity de una tabla.
     *
     * @param psTableName - Nombre de la tabla
     * @param psIdentity  - Nombre del identity
     * @return Long - &#205;ndice identity. Retorna null en caso no existiese registros
     * @throws DALException
     */
    public Long getLastIndentity(String psTableName, String psIdentity) throws DALException {
        getSQLQuery("SELECT " + psIdentity + " FROM " + psTableName + " ORDER BY " + psIdentity + " DESC LIMIT 1", new RowListener() {

            @Override
            public void setRow(long piPosition, Cursor poCursor) throws Exception {
                setObject(fnLong(poCursor.getLong(0)));
            }
        });
        return (Long) getObject();
    }
}
