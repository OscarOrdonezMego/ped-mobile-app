package pe.com.nextel.android.actividad;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import greendroid.widget.ActionBarGD;
import greendroid.widget.ActionBarGD.Type;
import greendroid.widget.ActionBarItem;
import pe.com.nextel.android.R;
import pe.com.nextel.android.util.NexActivityGestor;
import pe.com.nextel.android.util.Utilitario;

/**
 * Actividad b&#225;sica que muestra un mensaje de operador telef&#243;nico no correspondiente al <b>NEXTEL DEL PERU S.A.</b> o <b>ENTEL DEL PERU S.A.</b>
 * </br>Se debe declarar en el <b>AndroidManifest.xml</b> la siguiente l&#237;nea:
 * </br></br>&#60;activity android:name="pe.com.nextel.android.actividad.NexInvalidOperatorActivity"/&#62;
 *
 * @author jroeder
 */
public final class NexInvalidOperatorActivity extends NexActivity {
    private final int CONSICOSALIR = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Inicializa el {@link ActionBarGD} de la actividad. Este m&#233;todo no puede ser sobrecargado.
     */
    @Override
    public final void subIniActionBar() {
//		this.getActionBar().setType(Type.Empty);
        this.getActionBarGD().setType(Type.Empty);
        this.setTitle(getString(R.string.actopeinvalido_bartitulo));
        addActionBarItem(greendroid.widget.ActionBarItem.Type.Export, CONSICOSALIR);
    }

    /**
     * Inicializa los controles de la actividad. Este m&#233;todo no puede ser sobrecargado.
     */
    @Override
    protected final void subSetControles() {
        setActionBarContentView(R.layout.opeinvalido);

        Button mSalir = findViewById(R.id.actopeinvalido_button);
        mSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                NexActivityGestor.getInstance().exitApplication(getApplicationContext());
            }
        });


    }

    @Override
    public final void onBackPressed() {
        NexActivityGestor.getInstance().exitApplication(this);
    }

    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        switch (item.getItemId()) {
            case CONSICOSALIR:
//                	android.os.Process.killProcess(android.os.Process.myPid());
                NexActivityGestor.getInstance().exitApplication(this);
                break;
            default:
                return super.onHandleActionBarItemClick(item, position);
        }
        return true;
    }

    public static class NexInvalidOperatorException extends Exception {

        /**
         *
         */
        private static final long serialVersionUID = 4730083273994027437L;

        private String _message;

        @Override
        public String getMessage() {
            return _message;
        }

        @Override
        public String toString() {
            return _message;
        }

        public NexInvalidOperatorException(Context context) {
            super();
            _message = context.getString(Utilitario.obtainResourceId(context, R.attr.InvalidOperatorMessage));
        }

        public NexInvalidOperatorException(Context context, String detailMessage,
                                           Throwable throwable) {
            super(detailMessage, throwable);
            _message = context.getString(Utilitario.obtainResourceId(context, R.attr.InvalidOperatorMessage));
        }

        public NexInvalidOperatorException(Context context, String detailMessage) {
            super(detailMessage);
            _message = context.getString(Utilitario.obtainResourceId(context, R.attr.InvalidOperatorMessage));
        }

        public NexInvalidOperatorException(Context context, Throwable throwable) {
            super(throwable);
            _message = context.getString(Utilitario.obtainResourceId(context, R.attr.InvalidOperatorMessage));
        }
    }
}

