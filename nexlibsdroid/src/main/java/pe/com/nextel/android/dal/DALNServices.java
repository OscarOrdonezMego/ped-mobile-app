package pe.com.nextel.android.dal;

import android.content.Context;
import android.database.Cursor;

import pe.com.nextel.android.R;
import pe.com.nextel.android.actividad.NexInvalidOperatorActivity.NexInvalidOperatorException;

/**
 * Clase que gestiona la conexi&#243;n a datos de la integraci&#243;n de la aplicaci&#243;n con el NServices
 *
 * @author jroeder
 */
public final class DALNServices extends DALGestor {
    private static String NSERVICES_ETQ = "ETQ";
    private static String NSERVICES_APK = "APK";
    private static String NSERVICES_ERR = "ERR";
    public final static String NSERVICES_TABLE_NAME = "CFNServices";
    private static DALNServices instance;

    /**
     * Retorna una instancia de {@link DALNServices}
     *
     * @param poContext {@link Context} de la aplicaci&#243;n
     * @param resDBName Id de recurso de nombre de la base de datos
     * @return {@link DALNServices}
     * @throws NexInvalidOperatorException
     */
    public static DALNServices getInstance(Context poContext, int resDBName) throws NexInvalidOperatorException {
        if (instance == null)
            instance = new DALNServices(poContext, resDBName);
        else if (instance.getContext() != poContext)
            instance = new DALNServices(poContext, resDBName);
        return instance;
    }

    /**
     * Retorna una instancia de {@link DALNServices}
     *
     * @param poContext {@link Context} de la aplicaci&#243;n
     * @param dbName    Nombre de la base de datos
     * @return {@link DALNServices}
     * @throws NexInvalidOperatorException
     */
    public static DALNServices getInstance(Context poContext, String dbName) throws NexInvalidOperatorException {
        if (instance == null)
            instance = new DALNServices(poContext, dbName);
        else if (instance.getContext() != poContext)
            instance = new DALNServices(poContext, dbName);
        return instance;
    }

    private DALNServices(Context poContext, int resDBName)
            throws NexInvalidOperatorException {
        super(poContext, poContext.getString(resDBName));
    }

    private DALNServices(Context poContext, String dbName)
            throws NexInvalidOperatorException {
        super(poContext, dbName);
    }

    /**
     * Retorna si existe la etiqueta en la configuraci&#243;n del NServices
     *
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> Si existe la etiqueta</li>
     * <li><b>false</b> Si no existe la etiqueta</li>
     * </ul>
     * @throws DALException
     */
    public boolean fnSelExistsNServicesLabel() throws DALException {
        if (!isExistsTable(NSERVICES_TABLE_NAME))
            return false;
        getSQLQuery(("SELECT COUNT(*) FROM " + NSERVICES_TABLE_NAME + " WHERE CodModulo = " + fnValueQuery(NSERVICES_ETQ) + " AND Descripcion <> ''"), new RowListener() {

            @Override
            public void setRow(long piPosition, Cursor poCursor) throws Exception {
                setObject(fnInteger(poCursor, 0) > 0 ? true : false);
            }
        });
        return (Boolean) getObject(false);
    }

    /**
     * Retorna la etiqueta de la configuraci&#243;n del NServices
     *
     * @return {String}
     * @throws DALException
     */
    public String fnSelNServicesLabel() throws DALException {
        if (!isExistsTable(NSERVICES_TABLE_NAME))
            return null;
        getSQLQuery(("SELECT Descripcion FROM " + NSERVICES_TABLE_NAME + " WHERE CodModulo = " + fnValueQuery(NSERVICES_ETQ) + " AND Descripcion <> ''"), new RowListener() {

            @Override
            public void setRow(long piPosition, Cursor poCursor) throws Exception {
                setObject(fnString(poCursor, 0));
            }
        });
        return (String) getObject(getContext().getString(R.string.nservices_label_default));
    }

    /**
     * Retorna la descripci&#243;n de un error si se ha generado al obtener la configuraci&#243;n del NServices
     *
     * @return {String}
     * @throws DALException
     */
    public String fnSelNServicesError() throws DALException {
        if (!isExistsTable(NSERVICES_TABLE_NAME))
            return null;
        getSQLQuery(("SELECT Descripcion FROM " + NSERVICES_TABLE_NAME + " WHERE CodModulo = " + fnValueQuery(NSERVICES_ERR) + " AND Descripcion <> ''"), new RowListener() {

            @Override
            public void setRow(long piPosition, Cursor poCursor) throws Exception {
                setObject(fnString(poCursor, 0));
            }
        });
        return (String) getObject();
    }

    /**
     * Retorna la ruta o URL del APK instalador del NServices
     *
     * @return {String}
     * @throws DALException
     */
    public String fnSelNServicesAPK() throws DALException {
        if (!isExistsTable(NSERVICES_TABLE_NAME))
            return null;
        getSQLQuery(("SELECT Descripcion FROM " + NSERVICES_TABLE_NAME + " WHERE CodModulo = " + fnValueQuery(NSERVICES_APK) + " AND Descripcion <> ''"), new RowListener() {

            @Override
            public void setRow(long piPosition, Cursor poCursor) throws Exception {
                setObject(fnString(poCursor, 0));
            }
        });
        return (String) getObject();
    }

}
