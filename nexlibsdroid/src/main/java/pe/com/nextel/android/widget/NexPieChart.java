package pe.com.nextel.android.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import pe.com.nextel.android.R;
import pe.com.nextel.android.widget.NexToast.EnumType;

public class NexPieChart extends LinearLayout implements OnClickListener {

    private LinearLayout layoutChart;
    private CategorySeries mSeries;
    private DefaultRenderer mRenderer;
    private String mDateFormat;
    private GraphicalView mChartView;
    private OnNexPieChartClickListener clickListener;
    private OnNexPieChartCustomize customizeListener;
    private SimpleSeriesRenderer[] ioArrSeriesRender;
//	private boolean isPrimeraVez = true;

    private String[] _LabelArray;
    private int[] _ColorArray;
    private String _Title;
    private double[] _Values;
    private Integer _LabelColor;

    public NexPieChart(Context context) {
        super(context);
        ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.nexpiechart, this);
    }

    public NexPieChart(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    public NexPieChart(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
    }

    public NexPieChart(Context context, Bundle savedState) {
        super(context);
        ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.nexpiechart, this);
        onRestoreInstanceState(savedState);
        refreshView();
    }

    @Override
    public void onClick(View arg0) {
        if (mChartView.getCurrentSeriesAndPoint() != null) {
            setHighLight(mChartView.getCurrentSeriesAndPoint().getPointIndex());
            if (clickListener != null) {
                clickListener.onClickPieChart(mChartView.getCurrentSeriesAndPoint().getPointIndex(),
                        mChartView.getCurrentSeriesAndPoint().getValue());
                return;
            }
        } else {
            setHighLight(null);
            if (clickListener != null) {
                clickListener.onClickPieChartOut();
                return;
            }
        }
    }

    private void setHighLight(Integer id) {
        for (int i = 0; i < ioArrSeriesRender.length; i++) {
            ioArrSeriesRender[i].setHighlighted((id == null ? false : (id.intValue() == i)));
        }
        mChartView.repaint();
    }

    public void onRestoreInstanceState(Bundle savedState) {
        mSeries = (CategorySeries) savedState.getSerializable("current_series");
        mRenderer = (DefaultRenderer) savedState.getSerializable("current_renderer");
        mDateFormat = savedState.getString("date_format");
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("current_series", mSeries);
        outState.putSerializable("current_renderer", mRenderer);
        outState.putString("date_format", mDateFormat);
    }

    private int getRendererLabelsTextSize() {
        TypedValue _TypedValue = new TypedValue();
        getContext().getResources().getValue(Build.VERSION.SDK_INT < 11 ? R.dimen.nexpiechart_labelsTextSize_apilow11 : R.dimen.nexpiechart_labelsTextSize, _TypedValue, true);
        return _TypedValue.data;
    }

    private int getRendererLegendsTextSize() {
        TypedValue _TypedValue = new TypedValue();
        getContext().getResources().getValue(Build.VERSION.SDK_INT < 11 ? R.dimen.nexpiechart_legendTextSize_apilowl11 : R.dimen.nexpiechart_legendTextSize, _TypedValue, true);
        return _TypedValue.data;
    }

    private int[] getRendererMargin() {
        TypedArray _TypedArray = getContext().getResources().obtainTypedArray(R.array.nexpiechart_margin);
        int[] _margin = new int[_TypedArray.length()];
        for (int i = 0; i < _margin.length; i++) {
            _margin[i] = _TypedArray.getInt(i, -1);
        }
        return _margin;
    }

    private void iniObjectView() {
        if (_LabelColor == null)
            _LabelColor = getContext().getResources().getColor(R.color.nexgraylow);
        if (mSeries == null) {
            mSeries = new CategorySeries("Grafico");
        }
        if (mRenderer == null) {
            mRenderer = new DefaultRenderer();
        }
        mRenderer.setApplyBackgroundColor(true);
//		mRenderer.setBackgroundColor(Color.argb(100, 50, 50, 50));
        mRenderer.setBackgroundColor(Color.TRANSPARENT);
        mRenderer.setChartTitleTextSize(30);
        mRenderer.setLabelsTextSize(getRendererLabelsTextSize());
        mRenderer.setShowLabels(false);
        mRenderer.setLegendTextSize(getRendererLegendsTextSize());
        mRenderer.setLabelsColor(_LabelColor.intValue());
        mRenderer.setMargins(getRendererMargin());
        mRenderer.setZoomButtonsVisible(false);
        mRenderer.setStartAngle(135);
        mRenderer.setClickEnabled(true);
        mRenderer.setSelectableBuffer(10);
        mRenderer.setPanEnabled(false);
        mRenderer.setInScroll(true);
        mRenderer.setDisplayValues(true);
        mRenderer.setFitLegend(true);

        if (customizeListener != null)
            customizeListener.customizeRenderer(mRenderer);

        if (mChartView == null) {
            layoutChart = findViewById(R.id.nexpiechart);
            layoutChart.removeAllViews();
            mChartView = ChartFactory.getPieChartView(getContext(), mSeries, mRenderer);
            mChartView.setOnClickListener(this);
            layoutChart.addView(mChartView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            if (customizeListener != null)
                customizeListener.customizeChartView(mChartView);
        }
    }

    private String getLabel(int id) {
        if (customizeListener != null && customizeListener.customizeSeriesLabel(id, getLabelArray()[id], getValues()[id]) != null) {
            return customizeListener.customizeSeriesLabel(id, getLabelArray()[id], getValues()[id]);
        }
        return getLabelArray()[id];
    }

    public void refreshView() {
        iniObjectView();

        if (!verifyObjects())
            return;

        if (ioArrSeriesRender == null)
            ioArrSeriesRender = new SimpleSeriesRenderer[_LabelArray.length];

        if (ioArrSeriesRender.length != getLabelArray().length)
            ioArrSeriesRender = new SimpleSeriesRenderer[_LabelArray.length];

        mRenderer.setChartTitle(getTitle() != null ? getTitle() : "");

        if (getLabelArray() != null && getColorArray() != null && getValues() != null) {

            for (int i = 0; i < getLabelArray().length; i++) {

                try {
                    mSeries.set(i, getLabel(i), getValues()[i]);
                } catch (Exception e) {
                    mSeries.add(getLabel(i), getValues()[i]);
                }

                if (ioArrSeriesRender[i] == null) {
                    ioArrSeriesRender[i] = new SimpleSeriesRenderer();
                    ioArrSeriesRender[i].setColor(getColorArray()[i]);
                }

                if (customizeListener != null)
                    customizeListener.customizeSeriesRenderer(i, ioArrSeriesRender[i]);

                try {
                    mRenderer.removeSeriesRenderer(ioArrSeriesRender[i]);
                    mRenderer.addSeriesRenderer(ioArrSeriesRender[i]);
                } catch (Exception e) {
                    mRenderer.addSeriesRenderer(ioArrSeriesRender[i]);
                }
            }
//			if(isPrimeraVez){
//				mChartView.zoomOut();
//				isPrimeraVez = false;
//			}

            if (customizeListener != null)
                customizeListener.OnRefreshing(mChartView, mRenderer);

            mChartView.repaint();
        }
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        try {
            new NexToast(getContext(), getContext().getString(R.string.nexpiechart_useonnexpiechartclicklistener), EnumType.INFORMATION).show();
        } catch (Exception e) {
            Log.e("PRO", "NexPieChart.setOnClickListener", e);
        }
    }

    private boolean verifyObjects() {
        boolean flag = true;
        if (getLabelArray() == null) {
            new NexToast(getContext(), R.string.nexpiechart_errorinilabelarray, EnumType.ERROR).show();
            flag = flag & false;
        }
        if (getColorArray() == null) {
            new NexToast(getContext(), R.string.nexpiechart_errorinicolorarray, EnumType.ERROR).show();
            flag = false;
        }
        if (getValues() == null) {
            new NexToast(getContext(), R.string.nexpiechart_errorinivalores, EnumType.ERROR).show();
            flag = flag & false;
        }
        if (!((getLabelArray().length == getValues().length) && (getValues().length == getColorArray().length))
                && flag) {
            new NexToast(getContext(), R.string.nexpiechart_errorcantidadelementos, EnumType.ERROR).show();
            flag = flag & false;
        }
        if (getTitle() == null) {
            new NexToast(getContext(), R.string.nexpiechart_errorinititulo, EnumType.ERROR).show();
            flag = flag & false;
        }
        return flag;
    }

    public void setOnNexPieChartClickListener(OnNexPieChartClickListener listener) {
        this.clickListener = listener;
    }

    public void setOnNexPieChartCustomize(OnNexPieChartCustomize listener) {
        this.customizeListener = listener;
    }

    public String[] getLabelArray() {
        return _LabelArray;
    }

    public int[] getColorArray() {
        return _ColorArray;
    }

    public String getTitle() {
        return _Title;
    }

    public double[] getValues() {
        return this._Values;
    }

    public void setLabelArray(String[] labelArray, boolean refresh) {
        this._LabelArray = labelArray;
        if (refresh)
            refreshView();
    }

    public void setColorArray(int[] colorArray, boolean refresh) {
        this._ColorArray = colorArray;
        if (refresh)
            refreshView();
    }

    public void setLabelArray(int labelArrayResource, boolean refresh) {
        try {
            _LabelArray = getContext().getResources().getStringArray(labelArrayResource);
        } catch (Exception e) {
            _LabelArray = null;
        }
        if (refresh)
            refreshView();
    }

    public void setColorArray(int colorArrayResource, boolean refresh) {
        try {
            TypedArray loTypedArray = getContext().getResources().obtainTypedArray(colorArrayResource);
            _ColorArray = new int[loTypedArray.length()];
            for (int i = 0; i < _ColorArray.length; i++) {
                _ColorArray[i] = getContext().getResources().getColor(loTypedArray.getResourceId(i, -1));
            }
        } catch (Exception e) {
            _ColorArray = null;
        }
        if (refresh)
            refreshView();
    }

    public void setTitle(String title, boolean refresh) {
        this._Title = title;
        if (refresh)
            refreshView();
//		if(mRenderer != null)
//			mRenderer.setChartTitle(getTitle() != null? getTitle() : "");
//		if(mChartView != null)
//			mChartView.repaint();
    }

    public void setTitle(int titleResource, boolean refresh) {
        try {
            this._Title = getContext().getString(titleResource);
        } catch (Exception e) {
            _Title = null;
        }
        if (refresh)
            refreshView();
//		if(mRenderer != null)
//			mRenderer.setChartTitle(getTitle() != null? getTitle() : "");
//		if(mChartView != null)
//			mChartView.repaint();
    }

    public void setValues(double[] values, boolean refresh) {
        this._Values = values;
        if (refresh)
            refreshView();
    }

    public void setLabelArray(String[] labelArray) {
        setLabelArray(labelArray, true);
    }

    public void setColorArray(int[] colorArray) {
        setColorArray(colorArray, true);
    }

    public void setLabelArray(int labelArrayResource) {
        setLabelArray(labelArrayResource, true);
    }

    public void setColorArray(int colorArrayResource) {
        setColorArray(colorArrayResource, true);
    }

    public void setTitle(String title) {
        setTitle(title, true);
    }

    public void setTitle(int titleResource) {
        setTitle(titleResource, true);
    }

    public void setValues(double[] values) {
        setValues(values, true);
    }

    public void setLabelColor(Integer color) {
        setLabelColor(color, true);
    }

    public void setLabelColor(Integer color, boolean refresh) {
        _LabelColor = color;
        if (refresh) {
            refreshView();
        }
//		if(mRenderer != null)
//			mRenderer.setLabelsColor(_LabelColor.intValue());
//		if(mChartView != null)
//			mChartView.repaint();
    }

    public static interface OnNexPieChartClickListener {
        public void onClickPieChart(int id, double value);

        public void onClickPieChartOut();
    }

    public static interface OnNexPieChartCustomize {
        public void customizeSeriesRenderer(int id, SimpleSeriesRenderer seriesRenderer);

        public void customizeRenderer(DefaultRenderer renderer);

        public void customizeChartView(GraphicalView chart);

        public String customizeSeriesLabel(int id, String label, double value);

        public void OnRefreshing(GraphicalView chart, DefaultRenderer renderer);
    }
}
