package pe.com.nextel.android.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import pe.com.nextel.android.R;

/**
 * Clase que muestra un {@link AlertDialog} con 2 botones Positivos y Negativos
 * y que devuelve la respuesta seg&#250;n la elecci&#243;n del usuario
 *
 * @author jroeder
 * @version 1.1
 */
public class AlertDialogYesNo extends AlertDialog {
    /**
     * Variable Global de la respuesta en caso sea positiva (YES)
     */
    public static final short YES = 1;
    /**
     * Variable Global de la respuesta en caso sea negativa (NO)
     */
    public static final short NO = 0;

    /**
     * Instancia del Resultado
     */
    private int iiDialogResult;
    /**
     * Instancia del hilo o {@link Handler}
     */
    private Handler ioHandler;
    /**
     * Instancia del Mensaje a mostrar
     */
    private CharSequence ioMessage;
    /**
     * Instancia de Indicador L&#243;gico de Botones YES NO
     */
    private boolean ibYesNo = true;
    /**
     * Instancia del T&#237;tulo del di&#225;logo
     */
    private CharSequence ioTitle;
    /**
     * Instancia del &#205;cono del Di&#225;logo
     */
    private Drawable ioIcon;
//    /**
//     * Contexto de la aplicaci&#243;n
//     * @author jroeder
//     */
//    private Context ioContext;

    /**
     * Constructor de la clase {@link AlertDialogYesNo}
     *
     * @param poContext Contexto de la aplicaci&#243;n
     * @param poMessage Mensaje a mostrar
     */
    public AlertDialogYesNo(Context poContext, CharSequence poMessage) {
        super(poContext);
//		this.ioContext = poContext;
        this.ibYesNo = true;
        this.ioIcon = getContext().getResources().getDrawable(android.R.drawable.ic_dialog_info);
        this.ioTitle = getContext().getString(R.string.dlg_titinformacion);
        this.ioMessage = poMessage;
        setOwnerActivity((Activity) poContext);
        onCreate();
    }

    /**
     * Constructor de la clase {@link AlertDialogYesNo}
     *
     * @param poContext Contexto de la aplicaci&#243;n
     * @param poMessage Mensaje a mostrar
     * @param pbYesNo   Inidicador L&#243;gico.
     *                  <ul>
     *                  <li><b>true</b> si es con 2 botones</li>
     *                  <li><b>false</b> si es con 1 bot&#243;n</li>
     *                  </ul>
     */
    public AlertDialogYesNo(Context poContext, CharSequence poMessage, boolean pbYesNo) {
        super(poContext);
//		this.ioContext = poContext;
        this.ibYesNo = pbYesNo;
        this.ioIcon = getContext().getResources().getDrawable(android.R.drawable.ic_dialog_info);
        this.ioTitle = getContext().getString(R.string.dlg_titinformacion);
        this.ioMessage = poMessage;
        setOwnerActivity((Activity) poContext);
        onCreate();
    }

    /**
     * Constructor de la clase {@link AlertDialogYesNo}
     *
     * @param poContext Contexto de la aplicaci&#243;n
     * @param poTitle   T&#237;tulo del di&#225;logo
     * @param poMessage Mensaje a mostrar
     */
    public AlertDialogYesNo(Context poContext, CharSequence poTitle, CharSequence poMessage) {
        super(poContext);
//		this.ioContext = poContext;
        this.ibYesNo = true;
        this.ioIcon = getContext().getResources().getDrawable(android.R.drawable.ic_dialog_info);
        this.ioTitle = poTitle;
        this.ioMessage = poMessage;
        setOwnerActivity((Activity) poContext);
        onCreate();
    }

    /**
     * Constructor de la clase {@link AlertDialogYesNo}
     *
     * @param poContext Contexto de la aplicaci&#243;n
     * @param poTitle   T&#237;tulo del di&#225;logo
     * @param poMessage Mensaje a mostrar
     * @param pbYesNo   Inidicador L&#243;gico.
     *                  <ul>
     *                  <li><b>true</b> si es con 2 botones</li>
     *                  <li><b>false</b> si es con 1 bot&#243;n</li>
     *                  </ul>
     */
    public AlertDialogYesNo(Context poContext, CharSequence poTitle, CharSequence poMessage, boolean pbYesNo) {
        super(poContext);
//		this.ioContext = poContext;
        this.ibYesNo = pbYesNo;
        this.ioIcon = getContext().getResources().getDrawable(android.R.drawable.ic_dialog_info);
        this.ioTitle = poTitle;
        this.ioMessage = poMessage;
        setOwnerActivity((Activity) poContext);
        onCreate();
    }

    /**
     * Constructor de la clase {@link AlertDialogYesNo}
     *
     * @param poContext Contexto de la aplicaci&#243;n
     * @param poMessage Mensaje a mostrar
     * @param poIcon    &#205;cono del Di&#225;logo
     */
    public AlertDialogYesNo(Context poContext, CharSequence poMessage, Drawable poIcon) {
        super(poContext);
//		this.ioContext = poContext;
        this.ibYesNo = true;
        this.ioIcon = poIcon;
        this.ioTitle = getContext().getString(R.string.dlg_titinformacion);
        this.ioMessage = poMessage;
        setOwnerActivity((Activity) poContext);
        onCreate();
    }

    /**
     * Constructor de la clase {@link AlertDialogYesNo}
     *
     * @param poContext Contexto de la aplicaci&#243;n
     * @param poMessage Mensaje a mostrar
     * @param poIcon    &#205;cono del Di&#225;logo
     * @param pbYesNo   Inidicador L&#243;gico.
     *                  <ul>
     *                  <li><b>true</b> si es con 2 botones</li>
     *                  <li><b>false</b> si es con 1 bot&#243;n</li>
     *                  </ul>
     */
    public AlertDialogYesNo(Context poContext, CharSequence poMessage, Drawable poIcon, boolean pbYesNo) {
        super(poContext);
//		this.ioContext = poContext;
        this.ibYesNo = pbYesNo;
        this.ioIcon = poIcon;
        this.ioTitle = getContext().getString(R.string.dlg_titinformacion);
        this.ioMessage = poMessage;
        setOwnerActivity((Activity) poContext);
        onCreate();
    }

    /**
     * Constructor de la clase {@link AlertDialogYesNo}
     *
     * @param poContext Contexto de la aplicaci&#243;n (Actividad)
     * @param poTitle   T&#237;tulo del di&#225;logo
     * @param poMessage Mensaje a mostrar
     * @param poIcon    &#237;cono del Di&#225;logo
     */
    public AlertDialogYesNo(Context poContext, CharSequence poTitle, CharSequence poMessage, Drawable poIcon) {
        super(poContext);
//		this.ioContext = poContext;
        this.ibYesNo = true;
        this.ioIcon = poIcon;
        this.ioTitle = poTitle;
        this.ioMessage = poMessage;
        setOwnerActivity((Activity) poContext);
        onCreate();
    }

    /**
     * Constructor de la clase {@link AlertDialogYesNo}
     *
     * @param poContext Contexto de la aplicaci&#243;n
     * @param poTitle   T&#237;tulo del di&#225;logo
     * @param poMessage Mensaje a mostrar
     * @param poIcon    &#205;cono del Di&#225;logo
     * @param pbYesNo   Inidicador L&#243;gico.
     *                  <ul>
     *                  <li><b>true</b> si es con 2 botones</li>
     *                  <li><b>false</b> si es con 1 bot&#243;n</li>
     *                  </ul>
     */
    public AlertDialogYesNo(Context poContext, CharSequence poTitle, CharSequence poMessage, Drawable poIcon, boolean pbYesNo) {
        super(poContext);
//		this.ioContext = poContext;
        this.ibYesNo = pbYesNo;
        this.ioIcon = poIcon;
        this.ioTitle = poTitle;
        this.ioMessage = poMessage;
        setOwnerActivity((Activity) poContext);
        onCreate();
    }

    /**
     * Crea los objetos en el {@link AlertDialog}
     *
     * @author jroeder
     */
    private void onCreate() {
        Log.v("AlertDialogYesNo", "<onCreate>");
        setIcon(this.ioIcon);
        setTitle(this.ioTitle);
        setMessage(this.ioMessage);
        setButton(BUTTON_POSITIVE, ibYesNo ? getContext().getString(R.string.dlg_btnsi) : getContext().getString(R.string.dlg_btnok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                endDialog(AlertDialogYesNo.YES);
            }
        });
//		setPositiveButton(ibYesNo? getContext().getString(R.string.dlg_btnsi) : getContext().getString(R.string.dlg_btnok), new OnClickListener() {			
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				endDialog(AlertDialogYesNo.YES);
//			}
//		});
        if (ibYesNo) {
            setButton(BUTTON_NEGATIVE, getContext().getString(R.string.dlg_btnno), new OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    endDialog(AlertDialogYesNo.NO);
                }
            });
//        	setNegativeButton(getContext().getString(R.string.dlg_btnno), new OnClickListener() {
//				
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					endDialog(AlertDialogYesNo.NO);
//				}
//			});
        }
    }

    /**
     * Finaliza el {@link AlertDialog}
     *
     * @param piResult Resultado del di&#225;logo
     */
    public void endDialog(int piResult) {
        Log.v("AlertDialogYesNo", "<endDialog(" + piResult + ")>");
        dismiss();
//		ioAlert.dismiss();
        this.iiDialogResult = piResult;
        Message m = ioHandler.obtainMessage();
        ioHandler.sendMessage(m);
    }

//	AlertDialog ioAlert = null;

    /**
     * Muestra el {@link AlertDialog}
     *
     * @return int - Resultado del di&#225;logo
     */
    @SuppressWarnings("static-access")
    public int showDialog() {
        Log.v("AlertDialogYesNo", "<showDialog>");
        ioHandler = new Handler() {
            @Override
            public void handleMessage(Message mesg) {
                // process incoming messages here
                //super.handleMessage(msg);
                Looper.getMainLooper().quit();
                throw new RuntimeException();
            }
        };
        super.show();
//        ioAlert = this.create();
//        ioAlert.show();
        try {
            Looper.getMainLooper().loop();
        } catch (RuntimeException e2) {
        }
        return iiDialogResult;
    }

    /**
     * Instancia la actividad due&#241;a del di&#225;logo
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     */
    public void setOwner(Context poContext) {
        setOwnerActivity((Activity) poContext);
    }
}
