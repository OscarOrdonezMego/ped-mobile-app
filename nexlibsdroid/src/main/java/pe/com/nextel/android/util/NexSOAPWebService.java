package pe.com.nextel.android.util;

import android.app.ProgressDialog;
import android.content.Context;

import pe.com.nextel.android.R;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumServerResponse;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;

/**
 * Clase para el manejo de Web Services.</br>
 * Esta clase puede ser usada como clase padre y heredarla para su respectivo uso
 *
 * @author jroeder
 */
public class NexSOAPWebService {
    //Constantes para la invocacion del web service
    /**
     * Nombre constante del Nombre de espacio por defecto
     * SPACENAME_DEFAULT: http://tempuri.org/
     */
    protected static final String SPACENAME_DEFAULT = "http://tempuri.org/";

    /**
     * Contexto de la aplicaci&#243;n
     */
    protected Context ioContext;
    /**
     * URL del Servicio Web
     */
    protected String URL = null;
    /**
     * Nombre del Servicio Web
     */
    protected String SERVICE_NAME = null;
    /**
     * Nombre de espacio
     */
    protected String SPACE_NAME = null;
    /**
     * Nombre del m&#233;todo
     */
    protected String METHOD_NAME = null;
//	private HashMap<String, NexSOAPWebServiceSOAPAction> HASH_ACTION = null;

    private Throwable LASTERROR = null;
    private boolean DOTNET = false;
    private HttpConexion ioHTTPConexion = null;

    /**
     * Constructor de la clase
     *
     * @param psNameService - Nombre del Servicio
     * @param psURLService  - URL del servicio
     * @throws NexSOAPWebServiceException
     */
    public NexSOAPWebService(String psNameService, String psURLService, Context poContext) throws NexSOAPWebServiceException {
        ioContext = null;
        SERVICE_NAME = psNameService;
        URL = psURLService;
        SPACE_NAME = SPACENAME_DEFAULT;
        if (SERVICE_NAME == null) {
            NullPointerException loNULLException = new NullPointerException("SERVICE_NAME");
            setLastError(loNULLException);
            throw new NexSOAPWebServiceException(loNULLException);
        }
        if (SERVICE_NAME.equals("")) {
            NullPointerException loNULLException = new NullPointerException("SERVICE_NAME");
            setLastError(loNULLException);
            throw new NexSOAPWebServiceException(loNULLException);
        }
        if (URL == null) {
            NullPointerException loNULLException = new NullPointerException("URL");
            setLastError(loNULLException);
            throw new NexSOAPWebServiceException(loNULLException);
        }
        if (URL.equals("")) {
            NullPointerException loNULLException = new NullPointerException("URL");
            setLastError(loNULLException);
            throw new NexSOAPWebServiceException(loNULLException);
        }
        if (URL.contains(".asmx")) {
            DOTNET = true;
        }
    }

    /**
     * Instancia la &#250;ltima excepci&#243;n generada
     *
     * @param poLASTERROR - &#218;ltima excepci&#243;n generada
     */
    public void setLastError(Throwable poLASTERROR) {
        LASTERROR = poLASTERROR;
    }

    /**
     * Inicializa una conexi&#243;n HTTP utilizando la clase {@link HttpConexion}
     *
     * @param poContext      - Contexto de la aplicaci&#243;n
     * @param psMensaje      - Mensaje a mostrar en el {@link ProgressDialog}
     * @param poHttpConexion - Interfaz del tipo {@link NexSOAPWebServiceHTTPConexion}
     * @throws NexSOAPWebServiceException
     */
    protected void subInitializeHttpConexion(Context poContext, String psMensaje,
                                             final NexSOAPWebServiceHTTPConexion poHttpConexion) throws NexSOAPWebServiceException {
        ioHTTPConexion = new HttpConexion(poContext, psMensaje) {
            @Override
            public boolean OnPreConnect() {
                if (!Utilitario.fnVerSignal(ioContext)) {
                    setHttpResponseIdMessage(EnumServerResponse.ERROR, ioContext.getString(R.string.msg_fueracobertura));
                    return false;
                }
                poHttpConexion.subPreConnect(this);
                return true;
            }

            @Override
            public void OnConnect() {
                poHttpConexion.subConnect(this);
            }

            @Override
            public void OnPostConnect() {
                poHttpConexion.subPostConnect(this);
            }

            @Override
            protected void subCallHttpResponse(boolean showProgress) {
                poHttpConexion.subCallHttpResponse(this);
            }
        };
        ioHTTPConexion.execute();
    }

    /**
     * Adjunta el {@link NexSOAPWebService} a un nuevo {@link Context}
     *
     * @param context {@link Context} de la aplicaci&#243;n
     */
    public void attach(Context context) {
        this.ioContext = context;
    }

    /**
     * Elimina el {@link Context} del {@link NexSOAPWebService}
     */
    public void detach() {
        this.ioContext = null;
    }

    /**
     * Excepci&#243;n que arroja la clase cuando existe alg&#250;n error al inicializar el servicio web o al momento de consumir un m&#233;todo del servicio web
     *
     * @author jroeder
     */
    @SuppressWarnings("serial")
    public static class NexSOAPWebServiceException extends Exception {

        public NexSOAPWebServiceException(Exception poException) {
            this(poException.getMessage(), poException.getCause());
        }

        public NexSOAPWebServiceException() {
            super();
        }

        public NexSOAPWebServiceException(String detailMessage,
                                          Throwable throwable) {
            super(detailMessage, throwable);
        }

        public NexSOAPWebServiceException(String detailMessage) {
            super(detailMessage);
        }

        public NexSOAPWebServiceException(Throwable throwable) {
            super(throwable);
        }

    }

    /**
     * Interfaz utilizada para implementar los m&#233;todos de conexi&#243;n y consumo de m&#233;todos.</br>
     * Esta debe de ser implementada cuando se realiza un consumo de un m&#233;todo del servicio web.
     *
     * @author jroeder
     */
    public interface NexSOAPWebServiceHTTPConexion {
        /**
         * Invocado en el OnPreConnect del {@link HttpConexion} que realiza la conexi&#243;n.</br>
         * Este m&#233;todo se va a ejecutar si existe se&#241;al para realizar una conexi&#243;n.
         *
         * @param poHttpConexion - Objeto {@link HttpConexion} que realiza la conexi&#243;n
         */
        public void subPreConnect(HttpConexion poHttpConexion);

        /**
         * Invocado en el OnConnect del {@link HttpConexion} que realiza la conexi&#243;n.</br>
         * En este m&#233;todo se realiza el consumo del servicio web.
         *
         * @param poHttpConexion - Objeto {@link HttpConexion} que realiza la conexi&#243;n
         */
        public void subConnect(HttpConexion poHttpConexion);

        /**
         * Invocado en el OnPostConnect del {@link HttpConexion} que realiza la conexi&#243;n.</br>
         * En este m&#233;todo se realiza el manejo de las respuestas.
         *
         * @param poHttpConexion - Objeto {@link HttpConexion} que realiza la conexi&#243;n
         */
        public void subPostConnect(HttpConexion poHttpConexion);

        /**
         * Invocado en el subCallHttpResponse del {@link HttpConexion} que realiza la conexi&#243;n.</br>
         * Este m&#233;todo se invoca cuando el Contexto de la aplicaci&#243;n no pertenece a las actividades pre-definidas en {@link EnumTipActividad}
         *
         * @param poHttpConexion - Objeto {@link HttpConexion} que realiza la conexi&#243;n
         */
        public void subCallHttpResponse(HttpConexion poHttpConexion);
    }
}
