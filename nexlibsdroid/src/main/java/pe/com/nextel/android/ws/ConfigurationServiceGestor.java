package pe.com.nextel.android.ws;

import android.content.Context;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.type.TypeReference;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pe.com.nextel.android.R;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumServerResponse;
import pe.com.nextel.android.util.NexSOAPWebService;

/**
 * Clase que gestiona la conexi&#243;n con el Servicio Web que lista la configuraci&#243;n actual de las aplicaciones
 *
 * @author jroeder
 */
public final class ConfigurationServiceGestor extends NexSOAPWebService {
    /**
     * Nombre del Servicio Web de configraci&#243;n
     */
    public static final String CONFIGURATION_SERVICE_NAME = "ConfigurationService";
    private static ConfigurationServiceGestor _instance = null;

    private ConfigurationServiceGestor(Context poContext) throws NexSOAPWebServiceException {
        super(CONFIGURATION_SERVICE_NAME, poContext.getString(R.string.ConfigurationServiceUrl), poContext);
    }

    /**
     * Retorna la instancia del gestor de servicio web de configuraci&#243;n
     *
     * @param context {@link Context} de la aplicaci&#243;n
     * @return {@link ConfigurationServiceGestor}
     * @throws NexSOAPWebServiceException En caso existiese un error al inicializar el {@link ConfigurationServiceGestor}
     */
    public static ConfigurationServiceGestor getInstance(Context context) throws NexSOAPWebServiceException {
        if (_instance == null)
            _instance = new ConfigurationServiceGestor(context);
        _instance.attach(context);
        return _instance;
    }

    /**
     * Retorna la lista de todas las configuraciones actuales en la configuraci&#243;n de aplicaciones
     * </br>No utiliza un {@link HttpConexion}
     *
     * @return {@link List}&#60;{@link BeanConfiguration}&#62;
     * @throws NexSOAPWebServiceException En caso existiese un error al realizar el consumo del m&#233;todo del servicio web
     */
    @SuppressWarnings("unchecked")
    public List<BeanConfiguration> obtainCurrentConfiguration() throws NexSOAPWebServiceException {
        //String _confiration = fnConsumeHTTPSOAPPrimitive("obtenerConfiguracionActual", null).toString();
        String _confiration = "[{\"clave\":\"fecha\",\"valor\":\"20251013\"},{\"clave\":\"operador\",\"valor\":\"71617\"},{\"clave\":\"tema\",\"valor\":\"0\"}]";
        try {
            return (List<BeanConfiguration>) BeanMapper.fromJson(_confiration, new TypeReference<List<BeanConfiguration>>() {
            });
        } catch (Exception e) {
            throw new NexSOAPWebServiceException(e);
        }
    }

    /**
     * Obtiene la lista de todas las configuraciones actuales en la configuraci&#243;n de aplicaciones
     * </br>Utiliza un {@link HttpConexion}
     *
     * @param listener {@link ConnectionListener} que implementa el grabado de informaci&#243;n obtenido del servicio web
     * @throws NexSOAPWebServiceException En caso existiese un error al realizar el consumo del m&#233;todo del servicio web
     */
    public void obtainCurrentConfiguration(final ConnectionListener listener) throws NexSOAPWebServiceException {
        if (listener == null)
            return;
        subInitializeHttpConexion(ioContext, ioContext.getString(R.string.ConfigurationServiceMessageConnection), new NexSOAPWebServiceHTTPConexion() {

            @Override
            public void subPreConnect(HttpConexion poHttpConexion) {

            }

            @Override
            public void subPostConnect(HttpConexion poHttpConexion) {
                try {
                    if (poHttpConexion.getHttpResponseIdEnum() == EnumServerResponse.OKNOMSG) {
                        listener.saveInformation(poHttpConexion.getHttpResponseObject());
                    }
                } catch (Exception e) {
                    poHttpConexion.setHttpResponseIdMessageObject(EnumServerResponse.ERROR, e + "", null);
                }
            }

            @Override
            public void subConnect(HttpConexion poHttpConexion) {
                try {
                    poHttpConexion.setHttpResponseIdMessageObject(EnumServerResponse.OKNOMSG, "", obtainCurrentConfiguration());
                } catch (Exception e) {
                    poHttpConexion.setHttpResponseIdMessage(EnumServerResponse.ERROR, e + "");
                }
            }

            @Override
            public void subCallHttpResponse(HttpConexion poHttpConexion) {

            }
        });
    }

    /**
     * Implementa el m&#233;todo que guarda o almacena la informaci&#243;n obtenida del servicio web
     *
     * @author jroeder
     */
    public static interface ConnectionListener {
        /**
         * Es invocado cuando se culmina la conexi&#243;n del {@link HttpConexion}
         *
         * @param data Informaci&#243;n obtenida del servicio web
         */
        public void saveInformation(Object data) throws Exception;
    }

    /**
     * Clase que contiene una configuraci&#243;n de aplicaci&#243;n obtenido del servicio web
     *
     * @author jroeder
     */
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class BeanConfiguration extends BeanMapper {
        /**
         * Enumerable con la lista de claves de configuraci&#243;n
         *
         * @author jroeder
         */
        public static enum EnumConfigurationKey {
            /**
             * Enumerable de clave de Tema: tema
             */
            THEME("tema"),
            /**
             * Enumerable de clave de Operador: operador
             */
            OPERATOR("operador"),
            /**
             * Enumerable de clave de fecha de vigencia: fecha
             */
            EFFECTIVE_DATE("fecha"),
            /**
             * Enumerable de clave de Error: error
             */
            ERROR("error");
            private String _key;

            private EnumConfigurationKey(String key) {
                _key = key;
            }

            private static final Map<String, EnumConfigurationKey> _lookUp = new HashMap<String, EnumConfigurationKey>();

            static {
                for (EnumConfigurationKey _enum : EnumSet.allOf(EnumConfigurationKey.class)) {
                    _lookUp.put(_enum.getKey(), _enum);
                }
            }

            /**
             * Retorna la clave del enumerable
             *
             * @return {String}
             */
            public String getKey() {
                return _key;
            }

            /**
             * Retorna un {@link EnumConfigurationKey} seg&#250;n la clave de configuraci&#243;n
             *
             * @param key Clave de configuraci&#243;n
             * @return {@link EnumConfigurationKey}
             */
            public static EnumConfigurationKey get(String key) {
                return _lookUp.get(key);
            }
        }

        @JsonProperty
        private String clave;
        @JsonProperty
        private String valor;

        /**
         * Retorna la clave de configuraci&#243;n
         *
         * @return {String}
         */
        public String getClave() {
            return clave;
        }

        /**
         * Instancia la clave de configuraci&#243;n
         *
         * @param clave
         */
        public void setClave(String clave) {
            this.clave = clave;
        }

        /**
         * Retorna el valor de configuraci&#243;n
         *
         * @return {String}
         */
        public String getValor() {
            return valor;
        }

        /**
         * Instancia el valor de configuraci&#243;n
         *
         * @param valor
         */
        public void setValor(String valor) {
            this.valor = valor;
        }

    }
}
