package pe.com.nextel.android.actividad;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;

import pe.com.nextel.android.R;
import pe.com.nextel.android.dal.DalConfiguracion;
import pe.com.nextel.android.util.AlertDialogYesNo;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTheme;
import pe.com.nextel.android.util.NexActivityGestor;

/**
 * Clase Actividad Base para mostrar un Splash de inicio de aplicaci&#243;n
 *
 * @author jroeder
 * @version 1.0
 */
public abstract class NexSplashActivity extends Activity {
    /**
     * Variable Global para la verificaci&#243;n del proceso de inicio de base de datos SQLite.
     */
    private final static short PROCESS_DATAFRAMEWORK = 0;
    /**
     * Variable Global para la verificaci&#243;n del proceso de inicio de preferencias.
     */
    private final static short PROCESS_PREFERENCES = 1;
    /**
     * Variable Global para la verificaci&#243;n del proceso de obtenci&#243;n de actividad siguiente.
     */
    private final static short PROCESS_NEXTACTIVITY = 2;
    /**
     * Variable Global para la verificaci&#243;n del proceso de obtenci&#243;n de actividad de operador inv&#225;lido.
     */
    private final static short PROCESS_INVALIDOPERATOR = 3;
    private String isPackageName = null;
    private Integer iiPreferenceResource = null;
    /**
     * Bandera l&#243;gica que indica si el hilo del Splash se encuentra activo
     */
    protected boolean ibActivo = true;
    /**
     * Tiempo de duraci&#243;n del Splash en milisegundos. Por defecto son 3000 milisegundos
     */
    protected int iiTiempo = 3000;
    /**
     * Bandera l&#243;gica que indica si el operador telef&#243;nico es v&#225;lido o no
     */
    protected boolean ibOpeValido;
    private Thread ioThreadSplash;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NexActivityGestor.getInstance().finishActivity(this);
    }

    /**
     * Retorna el id de recurso del estilo del tema a utilizar en la actividad
     *
     * @return <b>int</b>
     */
    protected int getThemeResource() {
        return EnumTheme.BLUE.getResourceStyle(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NexActivityGestor.getInstance().addActivity(this);

        setTheme(getThemeResource());

        setContentView(R.layout.splash);
        //ver operador
        ibOpeValido = true; //Utilitario.fnVerOperador(this);

        subIniAppDataPreferences();

        // thread for displaying the SplashScreen
        ioThreadSplash = new Thread() {
            @Override
            public void run() {
                try {
                    int waited = 0;
                    while (ibActivo && (waited < iiTiempo)) {
                        sleep(100);
                        if (ibActivo) {
                            waited += 100;
                        }
                    }
                } catch (InterruptedException e) {

                } finally {
                    if (ibOpeValido) {
                        startActivity(new Intent(NexSplashActivity.this, getNextActivity()));
                        interrupt();
                        finish();
                    } else {
                        startActivity(new Intent(NexSplashActivity.this, getInvalidOperatorActivity()));
                        interrupt();
                        finish();
                    }
                }
            }
        };

        AlertDialogYesNo loAlert;
        if (!fnVerify(PROCESS_DATAFRAMEWORK)) {
            Log.v("PRO", getString(R.string.actsplash_dlgmeninidataframework));
            loAlert = new AlertDialogYesNo(this, getString(R.string.actsplash_dlgmeninidataframework), false);
            if (loAlert.showDialog() == AlertDialogYesNo.YES) {
//	    		android.os.Process.killProcess(android.os.Process.myPid());
                NexActivityGestor.getInstance().exitApplication(this);
            }
            return;
        }
        if (!fnVerify(PROCESS_PREFERENCES)) {
            Log.v("PRO", getString(R.string.actsplash_dlgmeninipreferences));
            loAlert = new AlertDialogYesNo(this, getString(R.string.actsplash_dlgmeninipreferences), false);
            if (loAlert.showDialog() == AlertDialogYesNo.YES) {
//	    		android.os.Process.killProcess(android.os.Process.myPid());
                NexActivityGestor.getInstance().exitApplication(this);
            }
            return;
        }
        if (!fnVerify(PROCESS_NEXTACTIVITY)) {
            Log.v("PRO", getString(R.string.actsplash_dlgmenininextactivity));
            loAlert = new AlertDialogYesNo(this, getString(R.string.actsplash_dlgmenininextactivity), false);
            if (loAlert.showDialog() == AlertDialogYesNo.YES) {
                NexActivityGestor.getInstance().exitApplication(this);
            }
            return;
        }
        if (!fnVerify(PROCESS_INVALIDOPERATOR)) {
            Log.v("PRO", getString(R.string.actsplash_dlgmeniniinvalidperator));
            loAlert = new AlertDialogYesNo(this, getString(R.string.actsplash_dlgmeniniinvalidperator), false);
            if (loAlert.showDialog() == AlertDialogYesNo.YES) {
                NexActivityGestor.getInstance().exitApplication(this);
            }
            return;
        }
        ioThreadSplash.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            ibActivo = false;
        }
        return true;
    }

    /**
     * M&#233;todo que inicia la base de datos y preferencias de la aplicaci&#243;n.<br/>
     * Este m&#233;todo debe de ser implementado en la clase hija invocando los m&#233;todos:<br/>
     * <ul>
     * <li>{@link #subIniDataFramework}</li>
     * <li>{@link #subIniPreferences}</li>
     * </ul>
     */
    protected void subIniAppDataPreferences() {
        subIniPreferences(fnGetPreferenceResource());
        subIniDataFramework(fnGetPackageName());
    }

    /**
     * Retorna la Actividad que se inicia al t&#233;rmino del hilo del Splash.<br/>
     * Este m&#233;todo debe de ser implementado en la clase hija
     *
     * @return {Class}&#60;&#63;&#62; Clase de la Actividad siguiente.
     */
    protected abstract Class<?> getNextActivity();

    /**
     * Retorna la Actividad de Operador Inv&#243;lido.<br/>
     *
     * @return {Class}&#60;&#63;&#62; Clase de la actividad de operador inv&#225;lido
     */
    protected Class<?> getInvalidOperatorActivity() {
        return NexInvalidOperatorActivity.class;
    }

    /**
     * Retorna el Nombre de Paquete de la aplicaci&#243;n
     *
     * @return {String}
     */
    protected abstract String fnGetPackageName();

    /**
     * Inicializa la base de datos SQLite de la aplicaci&#243;n
     *
     * @param psPackageName Nombre del paquete de la aplicaci&#243;n
     */
    protected void subIniDataFramework(String psPackageName) {
        isPackageName = psPackageName;
        DalConfiguracion loDalConfiguracion = new DalConfiguracion(this);
        loDalConfiguracion.subDataFramework(psPackageName);
    }

    /**
     * Retorna el Id de Recurso de las preferencias
     *
     * @return <b>int</b>
     */
    protected abstract int fnGetPreferenceResource();

    /**
     * Inicializa las preferencias en el {@link PreferenceManager}
     *
     * @param piPreferenceResource Id de Recurso de preferencias
     */
    protected void subIniPreferences(int piPreferenceResource) {
        iiPreferenceResource = piPreferenceResource;
        PreferenceManager.setDefaultValues(this, piPreferenceResource, false);
    }

    /**
     * Verifica si se han implementado los m&#233;todos de inicializaci&#243;n
     *
     * @param piProcess Proceso de verificaci&#243;n
     */
    private boolean fnVerify(short piProcess) {
        if (piProcess == PROCESS_DATAFRAMEWORK) {
            if (isPackageName == null) {
                return false;
            } else if (isPackageName.equals("")) {
                return false;
            }
            return true;
        } else if (piProcess == PROCESS_PREFERENCES) {
            if (iiPreferenceResource == null) {
                return false;
            }
            return true;
        } else if (piProcess == PROCESS_NEXTACTIVITY) {
            if (getNextActivity() == null) {
                return false;
            }
            return true;
        } else if (piProcess == PROCESS_INVALIDOPERATOR) {
            if (getInvalidOperatorActivity() == null) {
                return false;
            }
            return true;
        }
        return true;
    }
}
