package pe.com.nextel.android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.HashMap;

import pe.com.nextel.android.R;

public class NexNumericKeyboard extends LinearLayout {

//	private THEME_COLOR_DIGIT _themeColorDigit = THEME_COLOR_DIGIT.WHITE;

    private ImageView _keydel;
    private ImageView _keyret;

    private LinearLayout _key1;
    private LinearLayout _key2;
    private LinearLayout _key3;
    private LinearLayout _key4;
    private LinearLayout _key5;
    private LinearLayout _key6;
    private LinearLayout _key7;
    private LinearLayout _key8;
    private LinearLayout _key9;
    private LinearLayout _key0;

    private NexNumericKeyboardCallback _callback;
    private HashMap<String, NexEditText> _maptext;
    private NexEditText _currentEditText;

    public NexNumericKeyboard(Context context) {
        super(context);
        ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.nexnumerickeyboard, this);
    }

    public NexNumericKeyboard(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    public NexNumericKeyboard(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
    }

    private OnClickListener _KeyClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            if (_callback != null) {
                if (v instanceof LinearLayout) {
                    if (v == _key1)
                        addText("1");
                    if (v == _key2)
                        addText("2");
                    if (v == _key3)
                        addText("3");
                    if (v == _key4)
                        addText("4");
                    if (v == _key5)
                        addText("5");
                    if (v == _key6)
                        addText("6");
                    if (v == _key7)
                        addText("7");
                    if (v == _key8)
                        addText("8");
                    if (v == _key9)
                        addText("9");
                    if (v == _key0)
                        addText("0");
                } else if (v instanceof ImageView) {
                    if (v == _keydel)
                        delText();
                    if (v == _keyret)
                        _callback.performReturn();
                }
            }
        }
    };

    private void OnCreateView() {
        if (_key1 == null) {
            _key1 = findViewById(R.id.nexnumerickeyboard_one);
            _key1.setOnClickListener(_KeyClickListener);
        }
        if (_key2 == null) {
            _key2 = findViewById(R.id.nexnumerickeyboard_two);
            _key2.setOnClickListener(_KeyClickListener);
        }
        if (_key3 == null) {
            _key3 = findViewById(R.id.nexnumerickeyboard_three);
            _key3.setOnClickListener(_KeyClickListener);
        }
        if (_key4 == null) {
            _key4 = findViewById(R.id.nexnumerickeyboard_four);
            _key4.setOnClickListener(_KeyClickListener);
        }
        if (_key5 == null) {
            _key5 = findViewById(R.id.nexnumerickeyboard_five);
            _key5.setOnClickListener(_KeyClickListener);
        }
        if (_key6 == null) {
            _key6 = findViewById(R.id.nexnumerickeyboard_six);
            _key6.setOnClickListener(_KeyClickListener);
        }
        if (_key7 == null) {
            _key7 = findViewById(R.id.nexnumerickeyboard_seven);
            _key7.setOnClickListener(_KeyClickListener);
        }
        if (_key8 == null) {
            _key8 = findViewById(R.id.nexnumerickeyboard_eight);
            _key8.setOnClickListener(_KeyClickListener);
        }
        if (_key9 == null) {
            _key9 = findViewById(R.id.nexnumerickeyboard_nine);
            _key9.setOnClickListener(_KeyClickListener);
        }
        if (_key0 == null) {
            _key0 = findViewById(R.id.nexnumerickeyboard_zero);
            _key0.setOnClickListener(_KeyClickListener);
        }
        if (_keydel == null) {
            _keydel = findViewById(R.id.nexnumerickeyboard_delete);
            _keydel.setOnClickListener(_KeyClickListener);
        }
//		_keydel.setImageResource(_themeColorDigit.getDrwDeleteResource());
        if (_keyret == null) {
            _keyret = findViewById(R.id.nexnumerickeyboard_return);
            _keyret.setOnClickListener(_KeyClickListener);
        }
//		_keyret.setImageResource(_themeColorDigit.getDrwReturnResource());
    }

    private void addText(String digit) {
        if (_currentEditText != null) {
            int start = Math.max(_currentEditText.getSelectionStart(), 0);
            int end = Math.max(_currentEditText.getSelectionEnd(), 0);
            _currentEditText.getText().replace(Math.min(start, end), Math.max(start, end),
                    digit, 0, digit.length());
        }
    }

    private void delText() {
        if (_currentEditText != null) {
            try {
                int start = Math.max(_currentEditText.getSelectionStart(), 0);
                int end = Math.max(_currentEditText.getSelectionEnd(), 0);
                _currentEditText.getText().replace(start - 1, end, "");
            } catch (IndexOutOfBoundsException e) {
                Log.i("NexNumericKeyboard", "Inicio del texto");
            }
        }
    }

//	@Override
//	protected void onFinishInflate() {
//		super.onFinishInflate();
//		OnCreateView();
//	}

    public void setCallback(NexNumericKeyboardCallback callback) {
        OnCreateView();
        this._callback = callback;
    }

//	private void setFocus(EditText text, boolean focus){
//		_currentEditText = text;
//		text.setFocusable(focus);
//		if(focus)
//			text.requestFocus();
//		if(!focus)
//			text.setClickable(!focus);
//	}

    public void registerEditText(final NexEditText text) {
        OnCreateView();

        if (_maptext == null) {
            _maptext = new HashMap<String, NexEditText>();
        }
        for (NexEditText texttemp : _maptext.values()) {
            if (texttemp == text) {
                return;
            }
        }
        text.setTag(String.valueOf(_maptext.size()));
        text.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    InputMethodManager imm = (InputMethodManager) text.getContext().getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(text.getWindowToken(), 0);
                    _currentEditText = _maptext.get(v.getTag());
//					setFocus(_maptext.get((String)v.getTag()), true);
                } catch (NullPointerException e) {
                    Log.e("NexNumericKeyboard", "onFocusChange", e);
                }
            }
        });
        text.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                try {
                    if (hasFocus) {
                        InputMethodManager imm = (InputMethodManager) text.getContext().getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(text.getWindowToken(), 0);
                        _currentEditText = _maptext.get(v.getTag());
                    }
//					if(!hasFocus){
//						setFocus((EditText)v, hasFocus);
//					}
                } catch (NullPointerException e) {
                    Log.e("NexNumericKeyboard", "onFocusChange", e);
                }
            }
        });
        _maptext.put(String.valueOf(_maptext.size()), text);
        _currentEditText = text;
//		setFocus(text, true);
    }

    public void registerEditText(final NexEditText text, final OnClickListener onClickListener, final OnFocusChangeListener onFocusChangeListener) {
        OnCreateView();
        if (_maptext == null) {
            _maptext = new HashMap<String, NexEditText>();
        }
        for (NexEditText texttemp : _maptext.values()) {
            if (texttemp == text) {
                return;
            }
        }
        text.setTag(String.valueOf(_maptext.size()));
        text.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    InputMethodManager imm = (InputMethodManager) text.getContext().getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(text.getWindowToken(), 0);
                    _currentEditText = _maptext.get(v.getTag());
//					setFocus(_maptext.get((String)v.getTag()), true);
                } catch (NullPointerException e) {
                    Log.e("NexNumericKeyboard", "onFocusChange", e);
                }
                if (onClickListener != null)
                    onClickListener.onClick(v);
            }
        });
        text.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                try {
                    if (hasFocus) {
                        InputMethodManager imm = (InputMethodManager) text.getContext().getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(text.getWindowToken(), 0);
                        _currentEditText = _maptext.get(v.getTag());
                    }
//					if(!hasFocus){
//						setFocus((EditText)v, hasFocus);
//					}
                } catch (NullPointerException e) {
                    Log.e("NexNumericKeyboard", "onFocusChange", e);
                }
                if (onFocusChangeListener != null)
                    onFocusChangeListener.onFocusChange(v, hasFocus);
            }
        });
        _maptext.put(String.valueOf(_maptext.size()), text);
        _currentEditText = text;
//		setFocus(text, true);
    }

    public NexEditText getCurrentEditText() {
        return _currentEditText;
    }

    public static interface NexNumericKeyboardCallback {
        public void performReturn();
    }
}
