package pe.com.nextel.android.util;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;

public class NServicesConfiguration {

    /**
     * Nombre de acci&#243;n al realizar un {@link Intent} a la actividad del NServices
     */
    public static final String NSERVICES_ACTION_REMOTE = "pe.entel.android.action.remoto";
    /**
     * Nombre de acci&#243;n al realizar un {@link Intent} para iniciar el proceso del NServices
     */
    public static final String NSERVICES_ACTION_INITIAL = "pe.entel.android.servicios.action.iniciar";

    /**
     * Nombre del paquete del NServices
     */
    public static final String NSERVICES_PACKAGE = "pe.entel.android.servicios";
    /**
     * Nombre de la actividad del NServices
     */
    public static final String NSERVICES_ACTIVITY_CLASS = NSERVICES_PACKAGE + ".actividad.ActEstadosGridView";

    /**
     * Nombre del broadcast o petici&#243;n al t&#233;rmino de la descarga
     */
    public static final String NSERVICES_BROADCAST_DOWNLOADED = ".action.descargado";
    /**
     * C&#243;digo &#250;nico de notificaci&#243;n del instalador
     */
    public static final int NSERVICES_NOTICATION_INSTALLER_CODE = 07734;
    /**
     * Nombre de la carpeta donde se almacena el instalador del NServices descargado
     */
    public final static String NSERVICES_DOWNLOAD_FOLDER = "/download/";
    /**
     * Nombre del APK instalador del NServices
     */
    public final static String NSERVICES_DOWNLOAD_APK = "app.apk";
    /**
     * Nombre de la etiqueta extra del {@link Bundle} del {@link Intent} del servicio de descarga del instalador de NServices para la URL del APK instalador del NServices
     */
    public static final String NSERVICES_BUNLDE_EXTRA_DOWNLOAD_URL = "download_url";
    /**
     * Nombre de la etiqueta extra del {@link Bundle} del {@link Intent} del servicio de descarga del instalador de NServices para el recurso del estilo Maestro de la aplicaci&#243;n
     */
    public static final String NSERVICES_BUNDLE_EXTRA_RESSTYLEMASTER = "resStyleMaster";

    /**
     * Crea un {@link Intent} para invocar la actividad del paquete del NServices
     *
     * @return {@link Intent}
     */
    public static Intent createIntentForIniNServicesActivity() {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(NServicesConfiguration.NSERVICES_PACKAGE,
                        NServicesConfiguration.NSERVICES_ACTIVITY_CLASS
                )
        );
        intent.setAction(NServicesConfiguration.NSERVICES_ACTION_REMOTE);
        return intent;
    }
}
