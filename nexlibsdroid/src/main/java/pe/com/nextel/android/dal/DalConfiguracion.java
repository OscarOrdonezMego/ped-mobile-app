package pe.com.nextel.android.dal;

import android.content.Context;

import com.android.dataframework.DataFramework;

/**
 * Clase que realiza la gesti&#243;n de creaci&#243;n y actualizaci&#243;n de la base de datos SQLite definida en el recurso XML R.xml.tables,
 * utilizando la librer&#237;a DataFramework
 */
public class DalConfiguracion {

    private Context ioContext;

    /**
     * Constructor de la clase
     *
     * @param poContext Contexto de la aplicaci&#243;n
     */
    public DalConfiguracion(Context poContext) {
        ioContext = poContext;
    }

    /**
     * Inicializa la base de datos
     *
     * @param psPaquete Nombre del Paquete la aplicaci&#243;n
     */
    public void subDataFramework(String psPaquete) {
        try {
            //if(!Utilitario.fnVerOperador(ioContext))
            //	throw new NexInvalidOperatorActivity.NexInvalidOperatorException(ioContext);
            DataFramework.getInstance().open(ioContext, psPaquete);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
