package pe.com.nextel.android.receiver;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileFilter;

import pe.com.nextel.android.util.Logger;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;

/**
 * Implementa un {@link BroadcastReceiver} que est&#225; a la escucha del t&#233;rmino de la instanlaci&#243;n del paquete de NServices.</br>
 * A diferencia del {@link NServicesDownloaderReceiver}, no invoca al {@link Activity#unregisterReceiver unregisterReceiver} recibir el <b>broadcast</b>
 * </br>Se debe declarar en el <b>AndroidManifest.xml</b> la siguiente l&#237;nea:</br>
 * </br>
 * &#60;receiver android:name="pe.com.nextel.android.receiver.NServicesInstallerReceiver"&#62;</br>
 * &#160;&#160;&#60;intent-filter android:priority="100"&#62;</br>
 * &#160;&#160;&#160;&#160;&#60;action android:name="android.intent.action.PACKAGE_ADDED"/&#62;</br>
 * &#160;&#160;&#160;&#160;&#60;data android:scheme="package"/&#62;</br>
 * &#160;&#160;&#160;&#60;/intent-filter&#62;</br>
 * &#60;/receiver&#62;
 *
 * @author jroeder
 */
public final class NServicesInstallerReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context poContext, Intent poIntent) {
        try {
            Logger.initialize(poContext);
            //Uri loUri = Uri.parse(poIntent.getDataString());
            String lsDataString = poIntent.getDataString();
            if (lsDataString.equals("package:" + NServicesConfiguration.NSERVICES_PACKAGE)) {
                if (poIntent.getAction().equals(Intent.ACTION_PACKAGE_ADDED)) {
                    Logger.v("Se instalo el localizador");
                    NotificationManager loManager = (NotificationManager) poContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    loManager.cancel(NServicesConfiguration.NSERVICES_NOTICATION_INSTALLER_CODE);
                    subDeleteInstaller(poContext);
                    subStartService(poContext);
                } else if (poIntent.getAction().equals(Intent.ACTION_PACKAGE_REMOVED)) {
                    Logger.v("Se desinstalo el localizador");
                } else {
                    Logger.w("Se recibio un action desconocido");
                }
            }
        } catch (Exception e) {
            Log.e("PRO", "NServicesInstallerReceiver.onReceive", e);
        }
    }

    public static void subDeleteInstaller(Context poContext) {
        try {
            FileFilter loFileFilter = new FileFilter() {
                public boolean accept(File poFile) {
                    return poFile.isFile()
                            && poFile.getName().startsWith(NServicesConfiguration.NSERVICES_DOWNLOAD_APK);
                }
            };
            String lsAppName = Utilitario.getStringResourceByName(poContext, "app_name");
            File loFileDirectorio =
                    new File(Environment.getExternalStorageDirectory() +
                            Utilitario.fnNomCarpeta(lsAppName,
                                    NServicesConfiguration.NSERVICES_DOWNLOAD_FOLDER)
                    );

            File[] loLstFiles = loFileDirectorio.listFiles(loFileFilter);
            if (loLstFiles.length > 0) {
                for (File loLstFile : loLstFiles) {
                    Logger.v("Borrando apk");
                    loLstFile.delete();
                }
            } else {
                Logger.v("No se encontro el apk");
            }
        } catch (Exception ex) {
            Logger.e("No se borro el apk.");
            Logger.e(ex);
        }
    }

    /**
     * Crea un {@link Intent} con la acci&#243;n de inicio del paquete del NServices
     *
     * @return {@link Intent}
     */
    public static Intent createIntentForIniNServices() {
        Intent loIntent = new Intent();
        loIntent.setAction(NServicesConfiguration.NSERVICES_ACTION_INITIAL);
        if ((Build.VERSION.SDK_INT >= 11)) {
            loIntent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        }
        return loIntent;
    }

    private void subStartService(Context poContext) {
        Intent loIntent = createIntentForIniNServices();
        Logger.v("enviando broadcast: " + loIntent);
        poContext.sendBroadcast(loIntent);
    }

    /**
     * Env&#237;a un <b>Broadcast</b> o petici&#243;n
     *
     * @param context {@link Context} de la aplicaci&#243;n
     */
    public static void sendBroadcast(Context context) {
        context.sendBroadcast(createIntentForIniNServices());
    }
}