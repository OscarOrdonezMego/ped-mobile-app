package pe.com.nextel.android.util;

import android.text.format.DateFormat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Clase utilitaria para el manejo de fechas
 *
 * @author jroeder
 */
public class NexDate {

    /**
     * Formato de fechas pre-definidas
     *
     * @author jroeder
     */
    public static enum FORMAT {
        /**
         * DEFAULT_FORMAT_24H: dd/MM/yyyy HH:mm:ss (0-23)
         */
        DEFAULT_FORMAT_24H("dd/MM/yyyy HH:mm:ss", 103),
        /**
         * MILITAR_DATE_HOUR: yyyyMMddHHmmss (0-23)
         */
        MILITAR_DATE_HOUR("yyyyMMddHHmmss", null),
        /**
         * MILITAR_DATE: yyyyMMdd
         */
        MILITAR_DATE("yyyyMMdd", 112),
        /**
         * MILITAR_HOUR: HHmmss (0-23)
         */
        MILITAR_HOUR("HHmmss", null),
        /**
         * DEFAULT_FORMAT_AMPM: dd/MM/yyyy hh:mm:ss aa
         */
        DEFAULT_FORMAT_AMPM("dd/MM/yyyy hh:mm:ss aa", 103),
        /**
         * DATE: dd/MM/yyyy
         */
        DATE("dd/MM/yyyy", 103),
        /**
         * HOUR_24H: HH:mm:ss (0-23)
         */
        HOUR_24H("HH:mm:ss", 108),
        /**
         * HOUR_AMPM: hh:mm:ss aa
         */
        HOUR_AMPM("hh:mm:ss aa", 108),
        /**
         * DEFAULT_FORMAT_24K: dd/MM/yyyy kk:mm:ss (1-24)
         */
        DEFAULT_FORMAT_24K("dd/MM/yyyy kk:mm:ss (1-24)", 103),
        /**
         * MILITAR_DATE_KOUR: yyyyMMddkkmmss (1-24)
         */
        MILITAR_DATE_KOUR("yyyyMMddkkmmss", null),
        /**
         * MILITAR_KOUR: kkmmss (1-24)
         */
        MILITAR_KOUR("kkmmss", null),
        /**
         * HOUR_24K: kk:mm:ss (1-24)
         */
        HOUR_24K("kk:mm:ss", 108);
        private String format;
        private Integer sqlFormat;

        private FORMAT(String format, Integer sqlformat) {
            this.format = format;
            this.sqlFormat = sqlformat;
        }

        /**
         * Retorna el formato
         *
         * @return String
         */
        public String getFormat() {
            return this.format;
        }

        /**
         * Retorna el formato SQL.</br>
         * Si el format es igual a <b>null</b>, entonces es un formato no soportado por el SQL SERVER
         *
         * @return {Integer}
         */
        public Integer getSqlFormat() {
            return this.sqlFormat;
        }
    }

    /**
     * Convierte una cadena al tipo {@link Date}, usando el formato por defecto de 24 Hrs.
     *
     * @param date - Fecha en cadena
     * @return Date
     */
    public static Date convertToDate(String date) throws Exception {
        return (new SimpleDateFormat(FORMAT.DEFAULT_FORMAT_24H.getFormat())).parse(date);
    }

    /**
     * Convierte una cadena al tipo {@link Date}, usando uno de los formatos pre-definidios
     *
     * @param date   - Fecha en cadena
     * @param format - Formato de fecha pre-definida
     * @return Date
     */
    public static Date convertToDate(String date, FORMAT format) throws Exception {
        return (new SimpleDateFormat(format.getFormat())).parse(date);
    }

    /**
     * Convierte una cadena al tipo {@link Date}, usando un formato especificado
     *
     * @param date   - Fecha en cadena
     * @param format - Formato de fecha especificado
     * @return Date
     */
    public static Date convertToDate(String date, String format) throws Exception {
        return (new SimpleDateFormat(format)).parse(date);
    }

    /**
     * Convierte un objecto del tipo {@link Date} a {String}, usando el formato por defecto de 24 Hrs.
     *
     * @param date - Fecha del tipo {@link Date}
     * @return String
     */
    public static String convertToString(Date date) {
        String lsDate = DateFormat.format(FORMAT.DEFAULT_FORMAT_24H.getFormat(), date).toString();
        if (lsDate.contains("H")) {
            boolean midnight = false;
            String lsHour = DateFormat.format("kk", date).toString();
            if (lsHour.equals("24"))
                midnight = true;
            lsDate = DateFormat.format(FORMAT.DEFAULT_FORMAT_24H.getFormat().replace("H", midnight ? "0" : "k"), date).toString();
        }
        return lsDate;
    }

    /**
     * Convierte un objecto del tipo {@link Date} a {String}, usando uno de los formatos pre-definidos.
     *
     * @param date   - Fecha del tipo {@link Date}
     * @param format - Formato de fecha pre-definida
     * @return String
     */
    public static String convertToString(Date date, FORMAT format) {
        String lsDate = DateFormat.format(format.getFormat(), date).toString();
        if (lsDate.contains("H")) {
            boolean midnight = false;
            String lsHour = DateFormat.format("kk", date).toString();
            if (lsHour.equals("24"))
                midnight = true;
            lsDate = DateFormat.format(format.getFormat().replace("H", midnight ? "0" : "k"), date).toString();
        }
        return lsDate;
    }

    /**
     * Convierte un objecto del tipo {@link Date} a {String}, usando un formato especificado
     *
     * @param date   - Fecha del tipo {@link Date}
     * @param format - Formato de fecha especificado
     * @return String
     */
    public static String convertToString(Date date, String format) {
        String lsDate = DateFormat.format(format, date).toString();
        if (lsDate.contains("H")) {
            boolean midnight = false;
            String lsHour = DateFormat.format("kk", date).toString();
            if (lsHour.equals("24"))
                midnight = true;
            lsDate = DateFormat.format(format.replace("H", midnight ? "0" : "k"), date).toString();
        }
        return lsDate;
    }

    /**
     * Retorna la fecha y hora actual en un objeto {@link Date}
     *
     * @return Date
     */
    public static Date getCurrentDate() {
        Calendar loCal = Calendar.getInstance();
        return loCal.getTime();
    }

    /**
     * Retorna una nueva instancia de un Calendar
     *
     * @return Calendar
     */
    public static Calendar getNewCalendar() {
        return Calendar.getInstance();
    }

    /**
     * Retorna la fecha actual en el formato por defecto de 24 Hrs.
     *
     * @return String
     */
    public static String getCurrentStringDate() {
        Calendar loCal = Calendar.getInstance();
        return convertToString(loCal.getTime());
    }

    /**
     * Retorna la fecha actual en el formato especificado
     *
     * @param format - Formato de fecha
     * @return String
     */
    public static String getCurrentStringDate(String format) {
        Calendar loCal = Calendar.getInstance();
        return convertToString(loCal.getTime(), format);
    }

    /**
     * Retorna la fecha actual en el formato especificado
     *
     * @param format - Formato de fecha
     * @return String
     */
    public static String getCurrentStringDate(FORMAT format) {
        Calendar loCal = Calendar.getInstance();
        return convertToString(loCal.getTime(), format);
    }

    /**
     * Retorna el formato de fecha de uno de los elementos pre-definidos
     *
     * @param format - Elemento pre-definido del enumerable
     * @return String
     */
    public static String getFormat(FORMAT format) {
        return format.getFormat();
    }

    /**
     * Verifica 2 fechas.
     *
     * @param date1   - Fecha 1
     * @param format1 - Formato de Fecha 1
     * @param date2   - Fecha 2
     * @param format2 - Formato de Fecha 2
     * @return int
     * <ul>
     * <li>Si Fecha 1 es menor a Fecha 2, retorna < 0</li>
     * <li>Si las fechas son iguales, retorna 0</li>
     * <li>Si Fecha 1 es mayor a Fecha 2, retorna > 0</li>
     * </ul>
     */
    public static int Compare(String date1, FORMAT format1, String date2, FORMAT format2) throws Exception {
        Date loDate1 = convertToDate(date1, format1);
        Date loDate2 = convertToDate(date2, format2);
        return loDate1.compareTo(loDate2);
    }

    /**
     * Verifica 2 fechas.
     *
     * @param date1   - Fecha 1
     * @param format1 - Formato de Fecha 1
     * @param date2   - Fecha 2
     * @param format2 - Formato de Fecha 2
     * @return int
     * <ul>
     * <li>Si es Fecha 1 es menor a Fecha 2, retorna < 0</li>
     * <li>Si las fechas son iguales, retorna 0</li>
     * <li>Si Fecha 1 es mayor a Fecha 2, retorna > 0</li>
     * </ul>
     */
    public static int Compare(String date1, String format1, String date2, String format2) throws Exception {
        Date loDate1 = convertToDate(date1, format1);
        Date loDate2 = convertToDate(date2, format2);
        return loDate1.compareTo(loDate2);
    }
}
