package pe.com.nextel.android.util;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Spinner;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pe.com.nextel.android.R;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.actividad.NexListActivityBasic;
import pe.com.nextel.android.actividad.NexListActivityTweaked;
import pe.com.nextel.android.actividad.NexPreferenceActivity;
import pe.com.nextel.android.actividad.support.NexFragmentActivity;
import pe.com.nextel.android.actividad.support.NexFragmentListActivityBasic;
import pe.com.nextel.android.actividad.support.NexFragmentListActivityTweaked;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.util.NexDate.FORMAT;
import pe.com.nextel.android.ws.ConfigurationServiceGestor;
import pe.com.nextel.android.ws.ConfigurationServiceGestor.BeanConfiguration;
import pe.com.nextel.android.ws.ConfigurationServiceGestor.BeanConfiguration.EnumConfigurationKey;

/**
 * Clase que contiene valores constantes de configuraci&#243;n
 *
 * @author jroeder
 */
public class ConfiguracionNextel {
    /*-----------------------------------------------------
     MANEJO DE CONSTANTES
	 -----------------------------------------------------*/
    /**
     * Constante de respuesta de servidor OKNOMSG</br>
     * CONSRESSERVIDOROKNOMSG: 2
     *
     * @deprecated
     */
    public final static int CONSRESSERVIDOROKNOMSG = 2;
    /**
     * Constante de respuesta de servidor OK</br>
     * CONSRESSERVIDOROK: 1
     *
     * @deprecated
     */
    public final static int CONSRESSERVIDOROK = 1;
    /**
     * Constante de respuesta de servidor ERROR</br>
     * CONSRESSERVIDORERROR: -1
     *
     * @deprecated
     */
    public final static int CONSRESSERVIDORERROR = -1;
    /**
     * Constante de respuesta de servidor ALGUNERROR</br>
     * CONSRESSERVIDORALGUNERROR: 0
     *
     * @deprecated
     */
    public final static int CONSRESSERVIDORALGUNERROR = 0;

    /**
     * Constante de texto a mostrar en un {@link Spinner}
     */
    public final static String CONSSPINNERSELECCIONAR = "...";
    /**
     * Constante de nombre clave de {@link Bundle} de Filtro de {@link BeanExtras}</br>
     * CONBUNDLEFILTRO: filtro
     */
    public final static String CONBUNDLEFILTRO = "filtro";
    /**
     * Constante de nombre clave de {@link Bundle} de Foto</br>
     * CONBUNDLEFOTO: foto
     */
    public final static String CONBUNDLEFOTO = "foto";

    /**
     * Constante <b>TAG_COMM</b>
     *
     * @deprecated
     */
    public final static String TAG_COMM = "COMM";
    /**
     * Constante de tipo de env&#237;o <b>POST</b> en conexi&#243;n BlueTooth
     *
     * @deprecated
     */
    public final static String CONSTIPENVBLUETOOTHPOST = "POST";
    /**
     * Constante del tipo de env&#237;o <b>GET</b> en conexi&#243;n Bluetooth
     *
     * @deprecated
     */
    public final static String CONSTIPENVBLUETOOTHGET = "GET";
    /**
     * Constante de respuesta de servidor FTP OK</br>
     * CONSERVIDORFTPOK: 1;
     *
     * @deprecated
     */
    public final static int CONSERVIDORFTPOK = 1;
    /**
     * Constante de respuesta de servidor FTP ERROR</br>
     * CONSERVIDORFTPERROR: 0
     *
     * @deprecated
     */
    public final static int CONSERVIDORFTPERROR = 0;
	
	/*-----------------------------------------------------
	 CONSTANTES PREFERENCAS (GENERAL)
	-----------------------------------------------------*/
    /**
     * Constante Clave de Preferencia</br>
     * CONSPREFERENCIA: Actual
     *
     * @deprecated Utilizar el enumerable {@link EnumPreferenceKey} con el valor {@link EnumPreferenceKey#PREFERENCE PREFERENCE}
     */
    public final static String CONSPREFERENCIA = "Actual";
    /**
     * Constate de Clave de Preferencia de Operador</br>
     * CONSPREOPERADOR: operador
     *
     * @deprecated Utilizar el enumerable {@link EnumPreferenceKey} con el valor {@link EnumPreferenceKey#OPERATOR OPERATOR}
     */
    public final static String CONSPREOPERADOR = "operador";
	
	/*-----------------------------------------------------
	 CONSTANTES SHARE PREFERENCAS (Nombe de las variables a guardar)
	-----------------------------------------------------*/
    /**
     * Constante de preferencia de Bluetooth</br>
     * CONSPRENOMBLUETOOTH: BluetoothDevice
     */
    public final static String CONSPRENOMBLUETOOTH = "BluetoothDevice";
	
	/*-----------------------------------------------------
	 SINCRONIZACION
	 -----------------------------------------------------*/
    /**
     * Constante de nombre de archivo plano de sincronizaci&#243;n</br>
     * CONSSINNOMBRETXT: sincro.txt
     */
    public final static String CONSSINNOMBRETXT = "sincro.txt";
    /**
     * Constante de nombre de carpeta de archivo plano de sincronizaci&#243;n</br>
     * CONSSINNOMBRECARPETA: /txt/
     */
    public final static String CONSSINNOMBRECARPETA = "/txt/";
    /**
     * Constante de nombre de carpeta de im&#225;genes sincronizadas</br>
     * CONSIMGNOMBRECARPETA: /Img/
     */
    public final static String CONSIMGNOMBRECARPETA = "/Img/";
    /**
     * Constante de nombre de carpeta de toma de fotos</br>
     * CONSFOTNOMBRECARPETA: /Foto/
     */
    public final static String CONSFOTNOMBRECARPETA = "/Foto/";
    /**
     * Constante de nombre de zip de im&#225;genes sincronizadas</br>
     * CONSIMGNOMBREZIP: imagenes.zip
     */
    public final static String CONSIMGNOMBREZIP = "imagenes.zip";
	
	/*-----------------------------------------------------
	 PARAMETROS ASSETS
	 -----------------------------------------------------*/
    /**
     * Nombre del par&#225;metro en el <b>param.properties</b> del <b>Assets</b> que contiene la ruta de conexi&#243;n a servidor
     */
    public final static String IP_SERVER = "IP_SERVER";
    /**
     * Nombre del par&#225;metro en el <b>param.properties</b> del <b>Assets</b> que contiene la ruta de descarga del APK de la aplicaci&#243;n para actualizar
     */
    public final static String URL_ACTUALIZAR = "URL_ACTUALIZAR";
	
	/*-----------------------------------------------------
	 ROLES DE PREFERNCIAS
	 -----------------------------------------------------*/

    /**
     * Enumerable de Rol de Preferencia
     *
     * @author jroeder
     */
    public static enum EnumRolPreferencia {
        /**
         * Rol de Administrador. Puede modificar las preferencias
         */
        ADMINISTRADOR,
        /**
         * Rol de Usuario. Puede visualizar las preferencias
         */
        USUARIO
    }

    ;
	
	
	/*-----------------------------------------------------
	 TIPO DE BUSQUEDA
	 -----------------------------------------------------*/

    /**
     * Enumerable del tipo de b&#250;squeda
     *
     * @author jroeder
     */
    public static enum EnumTipBusqueda {
        NOMBRE, CODIGO, OTRO, OTRO2, OTRO3
    }

    ;
	
	/*-----------------------------------------------------
	 FLUJO, cuando se reutiliza varias actividades pero en diferentes flujos
	 -----------------------------------------------------*/

    /**
     * Enumerable de tipo de flujo en actividad.</br>
     * Es usado para reutilizar una actividad con diferentes flujos
     *
     * @author jroeder
     */
    public static enum EnumFlujo {
        FLUJO1, FLUJO2, FLUJO3, FLUJO4
    }

    ;
	
	/*-----------------------------------------------------
	 Estado de la transaccion
	 -----------------------------------------------------*/

    /**
     * Enumerable para el estado de objeto bean
     *
     * @author jroeder
     */
    public static enum EnumEstBean {
        NUEVO, NOENVIADO, ENVIADO
    }

    ;
	
	
	/*-----------------------------------------------------
	 TIPO DE ACTIVIDADES
	 -----------------------------------------------------*/

    /**
     * Enumerable del tipo de actividad
     *
     * @author jroeder
     */
    public static enum EnumTipActividad {
        /**
         * Tipo de actividad correspondiente a {@link NexActivity}
         */
        NEXACTIVITY,
        /**
         * Tipo de actividad correspondiente a {@link NexListActivityBasic}
         */
        NEXLISTACTIVITYBASIC,
        /**
         * Tipo de actividad correspondiente a {@link NexListActivityTweaked}
         */
        NEXLISTACTIVITYTWEAKED,
        /**
         * Tipo de actividad correspondiente a una actividad no pre-definida
         */
        NEXCUSTOMACTIVITY,
        /**
         * Tipo de actividad correspondiente a {@link NexFragmentActivity}
         */
        NEXFRAGMENTACTIVITY,
        /**
         * Tipo de actividad correspondiente a {@link NexPreferenceActivity}
         */
        NEXPREFERENCEACTIVITY,
        /**
         * Tipo de actividad correspondiente a {@link NexFragmentListActivityBasic}
         */
        NEXFRAGMENTLISTACTIVITYBASIC,
        /**
         * Tipo de actividad correspondiente a {@link NexFragmentListActivityTweaked}
         */
        NEXFRAGMENTLISTACTIVITYTWEAKED;

        /**
         * Retorna el tipo de actividad correspondiente al contexto de la aplicaci&#243;n
         *
         * @param poContext - Contexto de la aplicaci&#243;n
         * @return EnumTipActividad
         */
        public static EnumTipActividad get(Context poContext) {
            if (poContext instanceof NexActivity) {
                return NEXACTIVITY;
            } else if (poContext instanceof NexListActivityBasic) {
                return NEXLISTACTIVITYBASIC;
            } else if (poContext instanceof NexListActivityTweaked) {
                return NEXLISTACTIVITYTWEAKED;
            } else if (poContext instanceof NexFragmentActivity) {
                return NEXFRAGMENTACTIVITY;
            } else if (poContext instanceof NexFragmentListActivityBasic) {
                return NEXFRAGMENTLISTACTIVITYBASIC;
            } else if (poContext instanceof NexFragmentListActivityTweaked) {
                return NEXFRAGMENTLISTACTIVITYTWEAKED;
            } else if (poContext instanceof NexPreferenceActivity) {
                return NEXPREFERENCEACTIVITY;
            } else {
                return NEXCUSTOMACTIVITY;
            }
        }

    }

    ;
	
	/*-----------------------------------------------------
	 TIPO DE CONECCIONES
	 -----------------------------------------------------*/

    /**
     * Enumerable del tipo de conexi&#243;n
     *
     * @author jroeder
     */
    public static enum EnumTipConexion {
        CONHTTP,
        CONBLUETOOTH;

        /**
         * Retorna el tipo de conexi&#243;n GET
         *
         * @return {String}
         */
        public String getRequestGET() {
            return "GET";
        }

        /**
         * Retorna el tipo de conexi&#243;n POST
         *
         * @return {String}
         */
        public String getRequestPOST() {
            return "POST";
        }

        /**
         * Retorna el {@link EnumTipConexion} correspondiente al ordinal del par&#225;metro.</br>
         * En caso el ordinal no exista, por defecto retorna {@link EnumTipConexion#CONHTTP CONHTTP}
         *
         * @param ordinal - Ordinal del enumerable
         * @return {@link EnumTipConexion}
         */
        public static EnumTipConexion Get(int ordinal) {
            try {
                return values()[ordinal];
            } catch (IndexOutOfBoundsException e) {
                return CONHTTP;
            }
        }
    }

    ;

    /*------------------------------------------------------
     * ASSETS\PROPERTIES
     *-----------------------------------------------------*/
    /**
     * Nombre constante del archivos de propiedades en la carpeta <b>Assets</b></br>
     * CONSPROPERTYFILE: params.properties
     */
    public static String CONSPROPERTYFILE = "params.properties";


    /**
     * Clase enumerable con los valores de di&#225;logos en las actividades
     * <ul>
     * <li>EXCEPTION: -1</li>
     * <li>PROGRESS: -2</li>
     * <li>BACKUP: -3</li>
     * <li>ADMIN: -4</li>
     * </ul>
     *
     * @author jroeder
     */
    public static enum EnumDialog {
        /**
         * EXCEPTION: -1
         */
        EXCEPTION(-1),
        /**
         * PROGRESS: -2
         */
        PROGRESS(-2),
        /**
         * BACKUP: -3
         */
        BACKUP(-3),
        /**
         * ADMIN: -4
         */
        ADMIN(-4);

        private static final Map<Integer, EnumDialog> lookup = new HashMap<Integer, EnumDialog>();

        static {
            for (EnumDialog loEnum : EnumSet.allOf(EnumDialog.class)) {
                lookup.put(loEnum.getValue(), loEnum);
            }
        }

        private int value;

        private EnumDialog(int value) {
            this.value = value;
        }

        /**
         * Obtiene el valor del enumerable
         *
         * @return int - valor del enumerable
         */
        public int getValue() {
            return value;
        }

        /**
         * Obtiene el enumerable dado un valor. Si el valor no coincide, retorna null
         *
         * @param value - valor del enumerable
         * @return EnumDialog - Enumerable
         */
        public static EnumDialog get(int value) {
            return lookup.get(value);
        }

        /**
         * Obtiene el enumerable dado el nombre del enumerable. Si el nombre no coincide, retorna null
         *
         * @param name - nombre del enumerable
         * @return EnumDialog - Enumerable
         */
        public static EnumDialog get(String name) {
            for (EnumDialog loEnum : lookup.values()) {
                if (loEnum.toString().equals(name))
                    return loEnum;
            }
            return null;
        }
    }

    /**
     * Clase enumerable con los valores de respuesta del servidor http
     * <ul>
     * <li>ERROR: -1</li>
     * <li>ALGUNERROR: 0</li>
     * <li>OK: 1</li>
     * <li>OKNOMSG: 2</li>
     * </ul>
     *
     * @author jroeder
     */
    public static enum EnumServerResponse {
        /**
         * ERROR: -1
         */
        ERROR(-1),
        /**
         * ALGUNERROR: 0
         */
        ALGUNERROR(0),
        /**
         * OK: 1
         */
        OK(1),
        /**
         * OKNOMSG: 2
         */
        OKNOMSG(2);

        private static final Map<Integer, EnumServerResponse> lookup = new HashMap<Integer, EnumServerResponse>();

        static {
            for (EnumServerResponse loEnum : EnumSet.allOf(EnumServerResponse.class)) {
                lookup.put(loEnum.getValue(), loEnum);
            }
        }

        private int value;

        private EnumServerResponse(int value) {
            this.value = value;
        }

        /**
         * Obtiene el valor del enumerable
         *
         * @return int - valor del enumerable
         */
        public int getValue() {
            return value;
        }

        /**
         * Obtiene el enumerable dado un valor. Si el valor no coincide, retorna null
         *
         * @param value - valor del enumerable
         * @return EnumServerResponse - Enumerable
         */
        public static EnumServerResponse get(int value) {
            return lookup.get(value);
        }

        /**
         * Obtiene el enumerable dado el nombre del enumerable. Si el nombre no coincide, retorna null
         *
         * @param name - nombre del enumerable
         * @return EnumServerResponse - Enumerable
         */
        public static EnumServerResponse get(String name) {
            for (EnumServerResponse loEnum : lookup.values()) {
                if (loEnum.toString().equals(name))
                    return loEnum;
            }
            return null;
        }
    }

    /**
     * Clase enumerable con los valores de respuesta del servidor ftp
     * <ul>
     * <li>ERROR: -1</li>
     * <li>ALGUNERROR: 0</li>
     * <li>OK: 1</li>
     * <li>OKNOMSG: 2</li>
     * </ul>
     *
     * @author jroeder
     */
    public static enum EnumServerFTPResponse {
        /**
         * ERROR: 0
         */
        ERROR(0),
        /**
         * OK: 1
         */
        OK(1),
        /**
         * OKNOMSG: 2
         */
        OKNOMSG(2);

        private static final Map<Integer, EnumServerFTPResponse> lookup = new HashMap<Integer, EnumServerFTPResponse>();

        static {
            for (EnumServerFTPResponse loEnum : EnumSet.allOf(EnumServerFTPResponse.class)) {
                lookup.put(loEnum.getValue(), loEnum);
            }
        }

        private int value;

        private EnumServerFTPResponse(int value) {
            this.value = value;
        }

        /**
         * Obtiene el valor del enumerable
         *
         * @return int - valor del enumerable
         */
        public int getValue() {
            return value;
        }

        /**
         * Obtiene el enumerable dado un valor. Si el valor no coincide, retorna null
         *
         * @param value - valor del enumerable
         * @return EnumServerFTPResponse - Enumerable
         */
        public static EnumServerFTPResponse get(int value) {
            return lookup.get(value);
        }

        /**
         * Obtiene el enumerable dado el nombre del enumerable. Si el nombre no coincide, retorna null
         *
         * @param name - nombre del enumerable
         * @return EnumServerFTPResponse - Enumerable
         */
        public static EnumServerFTPResponse get(String name) {
            for (EnumServerFTPResponse loEnum : lookup.values()) {
                if (loEnum.toString().equals(name))
                    return loEnum;
            }
            return null;
        }
    }

    /**
     * Constate de Clave de Preferencia de Tema de Aplicaci&#243;n</br>
     * CONSPRETEMA: tema
     *
     * @deprecated Utilizar el enumerable {@link EnumPreferenceKey} con el valor {@link EnumPreferenceKey#THEME THEME}
     */
    public final static String CONSPRETEMA = "tema";

    /**
     * Enumerable con la lista de claves de preferencias</br>
     * <ul>
     * <li>{@link EnumPreferenceKey#PREFERENCE PREFERENCE}</li>
     * <li>{@link EnumPreferenceKey#OPERATOR OPERATOR}</li>
     * <li>{@link EnumPreferenceKey#THEME THEME}</li>
     * </ul>
     *
     * @author jroeder
     */
    public static enum EnumPreferenceKey {
        /**
         * PREFERENCIA: {@link ConfiguracionNextel#CONSPREFERENCIA CONSPREFERENCIA}
         */
        PREFERENCE(CONSPREFERENCIA),
        /**
         * OPERATOR: {@link ConfiguracionNextel#CONSPREOPERADOR CONSPREOPERADOR}
         */
        OPERATOR(CONSPREOPERADOR),
        /**
         * THEME: {@link ConfiguracionNextel#CONSPRETEMA CONSPRETEMA}
         */
        THEME(CONSPRETEMA);

        private String preferenceKey;

        private EnumPreferenceKey(String preferenceKey) {
            this.preferenceKey = preferenceKey;
        }

        /**
         * Retorna el valor constante del enumerable
         */
        @Override
        public String toString() {
            return preferenceKey;
        }

        private static final Map<String, EnumPreferenceKey> _lookUp = new HashMap<String, EnumPreferenceKey>();

        static {
            for (EnumPreferenceKey _preferenceKey : EnumSet.allOf(EnumPreferenceKey.class)) {
                _lookUp.put(_preferenceKey.toString(), _preferenceKey);
            }
        }

        /**
         * Retorna el {@link EnumPreferenceKey} seg&#250;n el valor del nombre de preferencia.
         * </br>Si el nombre no pertenece al grupo del enumerable, retorna <b>null</b>
         *
         * @param preferenceKey Nombre de la preferencia
         * @return {@link EnumPreferenceKey}
         */
        public static EnumPreferenceKey get(String preferenceKey) {
            return _lookUp.get(preferenceKey);
        }
    }

    /**
     * Enumerable con la colecci&#243;n de temas permitidos en la aplicaci&#243;n.
     * </br>Los temas est&#225;n instanciados en el estilo <b>R.style.Theme_Master</b>, el cual debe ser utilizado
     * como el tema principal en el <b>AndroidManifest.xml</b>
     * </br>En el caso sea necesario utilizar un tema personalizado, referenciar el nuevo tema
     * heredando el estilo <b>R.style.Theme_Master</b> e instanciando los atributos del tema a
     * reemplazar con el nuevo tema personalizado.
     * <ul>
     * <li>{@link EnumTheme #ORANGE ORANGE}</li>
     * <li>{@link EnumTheme#BLUE BLUE}</li>
     * </ul>
     *
     * @author jroeder
     */
    public static enum EnumTheme {
        /**
         * <b>BLUE</b>
         * <ul>
         * <li>
         * <b>R.attr.ThemeBlueStyle:</b> R.style.Theme_Blue
         * </br> Id de recurso de estilo con el tema azul
         * </li>
         * <li>
         * <b>R.attr.ThemeBlueTweakedItemsStyle:</b> R.style.Theme_Blue_TweakedItems
         * </br> Id de recurso de estilo con el tema azul para actividades del tipo {@link EnumTipActividad#NEXLISTACTIVITYTWEAKED NEXLISTACTIVITYTWEAKED} y {@link EnumTipActividad#NEXFRAGMENTLISTACTIVITYTWEAKED NEXFRAGMENTLISTACTIVITYTWEAKED}
         * </li>
         * <li>
         * <b>R.attr.ThemeHoloLightBlueStyle:</b> R.style.Theme_Holo_Light_Blue
         * </br> Id de recurso de estilo Holo Light con el tema azul
         * </li>
         * <li>
         * <b>R.attr.ThemeHoloLightBlueTweakedItemsStyle:</b> R.style.Theme_Holo_Light_Blue_TweakedItems
         * </br> Id de recurso de estilo Holo Light con el tema azul para actividades del tipo {@link EnumTipActividad#NEXLISTACTIVITYTWEAKED NEXLISTACTIVITYTWEAKED} y {@link EnumTipActividad#NEXFRAGMENTLISTACTIVITYTWEAKED NEXFRAGMENTLISTACTIVITYTWEAKED}
         * </li>
         * </ul>
         */
        BLUE(0,
                R.attr.ThemeBlueStyle,
                R.attr.ThemeBlueTweakedItemsStyle,
                R.attr.ThemeHoloLightBlueStyle,
                R.attr.ThemeHoloLightBlueTweakedItemsStyle,
                R.attr.ThemeHoloBlueStyle,
                R.attr.ThemeHoloBlueTweakedItemsStyle);

        private int _id;
        private int _attrRes;
        private int _attrResTweakedItems;
        private int _attrResHoloLight;
        private int _attrResHoloLightTweakedItems;
        private int _attrResHolo;
        private int _attrResHoloTweakedItems;

        private EnumTheme(int id,
                          int attrRes,
                          int attrResTweakedItem,
                          int attrResHoloLight,
                          int attrResHoloLightTweakedItems,
                          int attrResHolo,
                          int attrResHoloTweakedItems) {
            _id = id;
            _attrRes = attrRes;
            _attrResTweakedItems = attrResTweakedItem;
            _attrResHoloLight = attrResHoloLight;
            _attrResHoloLightTweakedItems = attrResHoloLightTweakedItems;
            _attrResHolo = attrResHolo;
            _attrResHoloTweakedItems = attrResHoloTweakedItems;
        }

        /**
         * Retorna el id de atributo R.attr.* correspondiente al tema
         *
         * @return int
         */
        public int getAttrRes() {
            return _attrRes;
        }

        /**
         * Retorna el id de atributo R.attr.* correspondiente al tema TweakedItems
         *
         * @return int
         */
        public int getAttrResTweakedItems() {
            return _attrResTweakedItems;
        }

        /**
         * Retorna el id de atributo R.attr.* correspondiente al tema Holo Light
         *
         * @return int
         */
        public int getAttrResHoloLight() {
            return _attrResHoloLight;
        }

        /**
         * Retorna el id de atributo R.attr.* correspondiente al tema Holo Light TweakedItems
         *
         * @return int
         */
        public int getAttrResHoloLightTweakedItems() {
            return _attrResHoloLightTweakedItems;
        }


        /**
         * Retorna el id de atributo R.attr.* correspondiente al tema Holo
         *
         * @return int
         */
        public int getAttrResHolo() {
            return _attrResHolo;
        }

        /**
         * Retorna el id de atributo R.attr.* correspondiente al tema Holo TweakedItems
         *
         * @return int
         */
        public int getAttrResHoloTweakedItems() {
            return _attrResHoloTweakedItems;
        }

        /**
         * Retorna el Id del enumerable
         *
         * @return <b>int</b>
         */
        public int getId() {
            return _id;
        }

        /**
         * Retorna el id de recurso del estilo referenciado en el atributo del tema
         *
         * @param context {@link Context} de la aplicaci&#243;n
         * @return <b>int</b>
         */
        public int getResourceStyle(Context context) {
            return Utilitario.obtainResourceId(context, _attrRes);
        }

        /**
         * Retorna el id de recurso del estilo referenciado en el atributo del tema TweakedItems
         *
         * @param context {@link Context} de la aplicaci&#243;n
         * @return <b>int</b>
         */
        public int getResourceTweakedItemsStyle(Context context) {
            return Utilitario.obtainResourceId(context, _attrResTweakedItems);
        }

        /**
         * Retorna el id de recurso del estilo referenciado en el atributo del tema Holo Light
         *
         * @param context {@link Context} de la aplicaci&#243;n
         * @return <b>int</b>
         */
        public int getResourceHoloLightStyle(Context context) {
            return Utilitario.obtainResourceId(context, _attrResHoloLight);
        }

        /**
         * Retorna el id de recurso del estilo referenciado en el atributo del tema Holo Light TweakedItems
         *
         * @param context {@link Context} de la aplicaci&#243;n
         * @return <b>int</b>
         */
        public int getResourceHoloLightTweakedItemsStyle(Context context) {
            return Utilitario.obtainResourceId(context, _attrResHoloLightTweakedItems);
        }

        /**
         * Retorna el id de recurso del estilo referenciado en el atributo del tema Holo
         *
         * @param context {@link Context} de la aplicaci&#243;n
         * @return <b>int</b>
         */
        public int getResourceHoloStyle(Context context) {
            return Utilitario.obtainResourceId(context, _attrResHolo);
        }

        /**
         * Retorna el id de recurso del estilo referenciado en el atributo del tema Holo TweakedItems
         *
         * @param context {@link Context} de la aplicaci&#243;n
         * @return <b>int</b>
         */
        public int getResourceHoloTweakedItemsStyle(Context context) {
            return Utilitario.obtainResourceId(context, _attrResHoloTweakedItems);
        }

        /**
         * Retorna el nombre del operador de red de la configuraci&#243;n de tema actual
         * </br>Si no existe la configuraci&#243;n de nombre de operador, retorna valor del atriburo <b>R.string.ThemeNetworkName</b>
         *
         * @param context {@link Context} de la aplicaci&#243;n
         * @return {}
         */
        public static String getNetworkOperatorName(Context context) {
            try {
//				return (String)SharedPreferenceGestor.fnSharedPrefer(context, EnumPreferenceKey.THEME, _netOpeName, context.getString(R.string.ThemeNetworkName), String.class);
                return context.getString(R.string.ThemeNetworkName);
            } catch (Exception e) {
                Log.e("PRO", "ConfiguracionNextel.EnumTheme.getNetworkOperatorName", e);
                return "71617";
            }
        }

        /**
         * Retorna el nombre del operador de red de la configuraci&#243;n de tema actual
         * </br>Si no existe la configuraci&#243;n de nombre de operador, retorna valor del atriburo <b>R.string.ThemeNetworkName</b>
         *
         * @param context {@link Context} de la aplicaci&#243;n
         * @return {String}
         */
        public static String getSimOperatorName(Context context) {
            try {
                return context.getString(R.string.SimOperatorName);
            } catch (Exception e) {
                Log.e("PRO", "ConfiguracionNextel.EnumTheme.getSimOperatorName", e);
                return "ENTEL";
            }
        }

        /**
         * Retorna la fecha de inicio de vigencia del tema en formato militar del enumerable {@link FORMAT}
         * </br>Si no existe la configuraci&#243;n de la fecha de inicio de vigencia, retorna el valor del atriburo <b>R.string.ThemeEffectiveDate</b>
         *
         * @param context {@link Context} de la aplicaci&#243;n
         * @return {String}
         */
        public static String getThemeEffectiveDate(Context context) {
            return getThemeEffectiveDate(context, FORMAT.MILITAR_DATE);
        }

        /**
         * Retorna la fecha de inicio de vigencia del tema en un formato del enumerable {@link FORMAT} espec&#237;fico
         * </br>Si no existe la configuraci&#243;n de la fecha de inicio de vigencia, retorna el valor del atriburo R.string.ThemeEffectiveDate
         *
         * @param context {@link Context} de la aplicaci&#243;n
         * @param format  Formato de la fecha a retornar
         * @return {String}
         */
        public static String getThemeEffectiveDate(Context context, FORMAT format) {
            return getThemeEffectiveDate(context, format.getFormat());
        }

        /**
         * Retorna la fecha de inicio de vigencia del tema en un formato espec&#237;fico
         * </br>Si no existe la configuraci&#243;n de la fecha de inicio de vigencia, retorna el valor del atriburo R.string.ThemeEffectiveDate
         *
         * @param context {@link Context} de la aplicaci&#243;n
         * @param format  Formato de la fecha a retornar
         * @return {String}
         */
        public static String getThemeEffectiveDate(Context context, String format) {
            try {
                String effectiveDate = (String) SharedPreferenceGestor.fnSharedPrefer(context, EnumPreferenceKey.THEME, _effecDate, context.getString(R.string.ThemeEffectiveDate), String.class);
                Log.i("PRO", "EnumTheme.getThemeEffectiveDate: " + effectiveDate);
                return NexDate.convertToString(NexDate.convertToDate(effectiveDate, FORMAT.MILITAR_DATE), format);
            } catch (Exception e) {
                Log.e("PRO", "ConfiguracionNextel.EnumTheme.getThemeEffectiveDate", e);
                return "20141001";
            }
        }

        private static final Map<Integer, EnumTheme> _lookUp = new HashMap<Integer, EnumTheme>();

        private static final String _themeKey = EnumConfigurationKey.THEME.getKey();
        @SuppressWarnings("unused")
        private static final String _netOpeName = EnumConfigurationKey.OPERATOR.getKey();
        private static final String _effecDate = EnumConfigurationKey.EFFECTIVE_DATE.getKey();

        static {
            for (EnumTheme _theme : EnumSet.allOf(EnumTheme.class)) {
                _lookUp.put(_theme.getId(), _theme);
            }
        }

        /**
         * Retorna un {@link EnumTheme} seg&#250;n un Id.
         * </br>Si el Id no existe retorna el {@link EnumTheme} por defecto
         *
         * @param id Id del enumerable
         * @return {@link EnumTheme}
         */
        public static EnumTheme get(int id) {
            EnumTheme _theme = _lookUp.get(id);
            return _theme != null ? _theme : EnumTheme.getDefault(null);
        }

        /**
         * Retorna el {@link EnumTheme} por defecto.
         * </br>Retorna {@link EnumTheme #BLUE BLUE}
         *
         * @return {@link EnumTheme}
         */
        public static EnumTheme getDefault(Context context) {
            return EnumTheme.BLUE;
        }

        /**
         * Retorna el Tema actual configurado en el m&#243;vil
         *
         * @param context {@link Context} de la aplicaci&#243;n
         * @return {@link EnumTheme}
         */
        public static EnumTheme getCurrentTheme(Context context) {
            return EnumTheme.BLUE;
			/*try{
				if(Utilitario.fnVerSignal(context)
						&& NexDate.Compare(NexDate.getCurrentStringDate(FORMAT.MILITAR_DATE), FORMAT.MILITAR_DATE, 
							getThemeEffectiveDate(context), FORMAT.MILITAR_DATE) >= 0){
//					if(!SharedPreferenceGestor.fnExiSharedPrefer(context, EnumPreferenceKey.THEME, _themeKey)){
//						return obtainThemeFromServer(context, true);
//					}else{
//						return EnumTheme.get(Integer.parseInt((String)SharedPreferenceGestor.fnSharedPrefer(context, EnumPreferenceKey.THEME, _themeKey, String.class)));
//					}
					return obtainThemeFromServer(context, true);
				}else if(!SharedPreferenceGestor.fnExiSharedPrefer(context, EnumPreferenceKey.THEME, _themeKey)){
					return getDefault(context);
				}else{
					return EnumTheme.get(Integer.parseInt((String)SharedPreferenceGestor.fnSharedPrefer(context, EnumPreferenceKey.THEME, _themeKey, String.class)));
				}
			}catch(Exception e){
				Log.e("PRO", "ConfiguracionNextel.EnumTheme.getCurrentTheme", e);
				return getDefault(context);
			}*/
        }

        /**
         * Realiza una conexi&#243;n a servidor para retornar el tema configurado desde el servidor
         *
         * @param context    {@link Context} de la aplicaci&#243;n
         * @param background Indicador de conexi&#243;n en background
         * @return {@link EnumTheme}
         */
        public static EnumTheme obtainThemeFromServer(final Context context, boolean background) throws Exception {
            //if (background) {
            List<BeanConfiguration> _lstConfiguration = ConfigurationServiceGestor.getInstance(context).obtainCurrentConfiguration();
            for (BeanConfiguration bean : _lstConfiguration) {
                if (EnumConfigurationKey.get(bean.getClave()) == EnumConfigurationKey.ERROR) {
                    throw new Exception(bean.getValor());
                }
                SharedPreferenceGestor.subIniSharedPrefer(context, EnumPreferenceKey.THEME, bean.getClave(), bean.getValor());
            }
            /*} else {
                ConfigurationServiceGestor.getInstance(context).obtainCurrentConfiguration(new ConnectionListener() {
                    @SuppressWarnings("unchecked")
                    @Override
                    public void saveInformation(Object data) throws Exception {
                        for (BeanConfiguration bean : (List<BeanConfiguration>) data) {
                            if (EnumConfigurationKey.get(bean.getClave()) == EnumConfigurationKey.ERROR) {
                                throw new Exception(bean.getValor());
                            }
                            SharedPreferenceGestor.subIniSharedPrefer(context, EnumPreferenceKey.THEME, bean.getClave(), bean.getValor());
                        }
                    }
                });
            }*/

            return EnumTheme.get(Integer.parseInt((String) SharedPreferenceGestor.fnSharedPrefer(context, EnumPreferenceKey.THEME, _themeKey, String.class)));
        }
    }
}
