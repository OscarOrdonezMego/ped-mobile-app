package pe.com.nextel.android.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewDebug.ExportedProperty;
import android.widget.EditText;

@SuppressLint("AppCompatCustomView")
public class NexEditText extends EditText {
    private boolean _istexteditor = false;

    public NexEditText(Context context) {
        super(context);
    }

    public NexEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NexEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onCheckIsTextEditor() {
        // TODO Auto-generated method stub
        return _istexteditor;
    }

    public void setCheckIsTextEditor(boolean check) {
        this._istexteditor = check;
    }

    @Override
    @ExportedProperty(category = "text")
    public int getSelectionEnd() {
        if (!onCheckIsTextEditor())
            return getText().length();
        return super.getSelectionEnd();
    }

    @Override
    @ExportedProperty(category = "text")
    public int getSelectionStart() {
        if (!onCheckIsTextEditor())
            return getText().length();
        return super.getSelectionStart();
    }
}
