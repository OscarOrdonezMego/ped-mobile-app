package pe.com.nextel.android.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumPreferenceKey;

/**
 * Clase est&#225;tica que permite la gesti&#243;n de variables con datos en el Shared Preference.
 *
 * @author jroeder
 * @version 1.0
 */
public class SharedPreferenceGestor {
    /**
     * Inicializa un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext            - Contexto de la aplicaci&#243;n
     * @param psKeyObject          - Llave de identificaci&#243;n del objeto
     * @param poSerializableObject - Objeto serializable a convertir en JSON
     */
    public static void subIniSharedPreferJSON(Context poContext, String psKeyObject, Object poSerializableObject) throws Exception {
        subIniSharedPreferJSON(poContext, EnumPreferenceKey.PREFERENCE, psKeyObject, poSerializableObject);
    }

    /**
     * Inicializa un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext            - Contexto de la aplicaci&#243;n
     * @param psPreferenceName     - Nombre de la preferencia
     * @param psKeyObject          - Llave de identificaci&#243;n del objeto
     * @param poSerializableObject - Objeto serializable a convertir en JSON
     */
    public static void subIniSharedPreferJSON(Context poContext, String psPreferenceName, String psKeyObject, Object poSerializableObject) throws Exception {
        String lsSerializableObject = BeanMapper.toJson(poSerializableObject, false);
        SharedPreferences loSharedPreferences = poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString(psKeyObject, lsSerializableObject);
        loEditor.commit();
    }

    /**
     * Inicializa un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext            - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey      - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param psKeyObject          - Llave de identificaci&#243;n del objeto
     * @param poSerializableObject - Objeto serializable a convertir en JSON
     */
    public static void subIniSharedPreferJSON(Context poContext, EnumPreferenceKey poPreferenceKey, String psKeyObject, Object poSerializableObject) throws Exception {
        subIniSharedPreferJSON(poContext, poPreferenceKey.toString(), psKeyObject, poSerializableObject);
    }

    /**
     * Inicializa un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext            - Contexto de la aplicaci&#243;n
     * @param poKeyObject          - Llave de identificaci&#243;n del objeto
     * @param poSerializableObject - Objeto serializable a convertir en JSON
     */
    public static void subIniSharedPreferJSON(Context poContext, Object poKeyObject, Object poSerializableObject) throws Exception {
        subIniSharedPreferJSON(poContext, EnumPreferenceKey.PREFERENCE, poKeyObject, poSerializableObject);
    }

    /**
     * Inicializa un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext            - Contexto de la aplicaci&#243;n
     * @param psPreferenceName     - Nombre de la preferencia
     * @param poKeyObject          - Llave de identificaci&#243;n del objeto
     * @param poSerializableObject - Objeto serializable a convertir en JSON
     */
    public static void subIniSharedPreferJSON(Context poContext, String psPreferenceName, Object poKeyObject, Object poSerializableObject) throws Exception {
        String lsSerializableObject = BeanMapper.toJson(poSerializableObject, false);
        SharedPreferences loSharedPreferences = poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString(poKeyObject.toString(), lsSerializableObject);
        loEditor.commit();
    }

    /**
     * Inicializa un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext            - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey      - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param poKeyObject          - Llave de identificaci&#243;n del objeto
     * @param poSerializableObject - Objeto serializable a convertir en JSON
     */
    public static void subIniSharedPreferJSON(Context poContext, EnumPreferenceKey poPreferenceKey, Object poKeyObject, Object poSerializableObject) throws Exception {
        subIniSharedPreferJSON(poContext, poPreferenceKey.toString(), poKeyObject, poSerializableObject);
    }

    /**
     * Cierra o coloca el valor en blanco un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext   - Contexto de la aplicaci&#243;n
     * @param psKeyObject - Llave de identificaci&#243;n del objeto
     */
    public static void subCloseSharedPreferJSON(Context poContext, String psKeyObject) {
        subCloseSharedPreferJSON(poContext, EnumPreferenceKey.PREFERENCE, psKeyObject);
    }

    /**
     * Cierra o coloca el valor en blanco un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param psKeyObject      - Llave de identificaci&#243;n del objeto
     */
    public static void subCloseSharedPreferJSON(Context poContext, String psPreferenceName, String psKeyObject) {
        SharedPreferences loSharedPreferences = poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString(psKeyObject, "");
        loEditor.commit();
    }

    /**
     * Cierra o coloca el valor en blanco un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param psKeyObject     - Llave de identificaci&#243;n del objeto
     */
    public static void subCloseSharedPreferJSON(Context poContext, EnumPreferenceKey poPreferenceKey, String psKeyObject) {
        subCloseSharedPreferJSON(poContext, poPreferenceKey.toString(), psKeyObject);
    }

    /**
     * Cierra o coloca el valor en blanco un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext   - Contexto de la aplicaci&#243;n
     * @param poKeyObject - Llave de identificaci&#243;n del objeto
     */
    public static void subCloseSharedPreferJSON(Context poContext, Object poKeyObject) {
        subCloseSharedPreferJSON(poContext, EnumPreferenceKey.PREFERENCE, poKeyObject);
    }

    /**
     * Cierra o coloca el valor en blanco un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param poKeyObject      - Llave de identificaci&#243;n del objeto
     */
    public static void subCloseSharedPreferJSON(Context poContext, String psPreferenceName, Object poKeyObject) {
        SharedPreferences loSharedPreferences = poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.putString(poKeyObject.toString(), "");
        loEditor.commit();
    }

    /**
     * Cierra o coloca el valor en blanco un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param poKeyObject     - Llave de identificaci&#243;n del objeto
     */
    public static void subCloseSharedPreferJSON(Context poContext, EnumPreferenceKey poPreferenceKey, Object poKeyObject) {
        subCloseSharedPreferJSON(poContext, poPreferenceKey.toString(), poKeyObject);
    }

    /**
     * Verifica si existe un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext   - Contexto de la aplicaci&#243;n
     * @param psKeyObject - Llave de identificaci&#243;n del objeto
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si la verificaci&#243;n es correcta</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnExiSharedPreferJSON(Context poContext, String psKeyObject) {
        return fnExiSharedPreferJSON(poContext, EnumPreferenceKey.PREFERENCE, psKeyObject);
    }

    /**
     * Verifica si existe un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param psKeyObject      - Llave de identificaci&#243;n del objeto
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si la verificaci&#243;n es correcta</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnExiSharedPreferJSON(Context poContext, String psPreferenceName, String psKeyObject) {
        if (!fnSharedPreferJSON(poContext, psPreferenceName, psKeyObject).equals("")) {
            return true;
        }
        return false;
    }

    /**
     * Verifica si existe un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param psKeyObject     - Llave de identificaci&#243;n del objeto
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si la verificaci&#243;n es correcta</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnExiSharedPreferJSON(Context poContext, EnumPreferenceKey poPreferenceKey, String psKeyObject) {
        return fnExiSharedPreferJSON(poContext, poPreferenceKey.toString(), psKeyObject);
    }

    /**
     * Verifica si existe un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext   - Contexto de la aplicaci&#243;n
     * @param poKeyObject - Llave de identificaci&#243;n
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si la verificaci&#243;n es correcta</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnExiSharedPreferJSON(Context poContext, Object poKeyObject) {
        return fnExiSharedPreferJSON(poContext, EnumPreferenceKey.PREFERENCE, poKeyObject);
    }

    /**
     * Verifica si existe un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param poKeyObject      - Llave de identificaci&#243;n
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si verficaci&#243;n es correcta</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnExiSharedPreferJSON(Context poContext, String psPreferenceName, Object poKeyObject) {
        if (!fnSharedPreferJSON(poContext, psPreferenceName, poKeyObject.toString()).equals("")) {
            return true;
        }
        return false;
    }

    /**
     * Verifica si existe un objeto en formato JSON seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param poKeyObject     - Llave de identificaci&#243;n
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si verficaci&#243;n es correcta</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnExiSharedPreferJSON(Context poContext, EnumPreferenceKey poPreferenceKey, Object poKeyObject) {
        return fnExiSharedPreferJSON(poContext, poPreferenceKey.toString(), poKeyObject);
    }

    /**
     * Recupera el objeto en formato JSON seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext   - Contexto de la aplicaci&#243;n
     * @param psKeyObject - Llave de identificaci&#243;n del objeto
     * @return String - Formato JSON del objeto
     */
    public static String fnSharedPreferJSON(Context poContext, String psKeyObject) {
        return fnSharedPreferJSON(poContext, EnumPreferenceKey.PREFERENCE, psKeyObject);
    }

    /**
     * Recupera el objeto en formato JSON seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param psKeyObject      - Llave de identificaci&#243;n del objeto
     * @return String - Formato JSON del objeto
     */
    public static String fnSharedPreferJSON(Context poContext, String psPreferenceName, String psKeyObject) {
        return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getString(psKeyObject, "");
    }

    /**
     * Recupera el objeto en formato JSON seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param psKeyObject     - Llave de identificaci&#243;n del objeto
     * @return String - Formato JSON del objeto
     */
    public static String fnSharedPreferJSON(Context poContext, EnumPreferenceKey poPreferenceKey, String psKeyObject) {
        return fnSharedPreferJSON(poContext, poPreferenceKey.toString(), psKeyObject);
    }

    /**
     * Recupera el objeto en formato JSON seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext   - Contexto de la aplicaci&#243;n
     * @param poKeyObject - Llave de identificaci&#243;n del objeto
     * @return String - Formato JSON del objeto
     */
    public static String fnSharedPreferJSON(Context poContext, Object poKeyObject) {
        return fnSharedPreferJSON(poContext, EnumPreferenceKey.PREFERENCE, poKeyObject);
    }

    /**
     * Recupera el objeto en formato JSON seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param poKeyObject      - Llave de identificaci&#243;n del objeto
     * @return String - Formato JSON del objeto
     */
    public static String fnSharedPreferJSON(Context poContext, String psPreferenceName, Object poKeyObject) {
        return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getString(poKeyObject.toString(), "");
    }

    /**
     * Recupera el objeto en formato JSON seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param poKeyObject     - Llave de identificaci&#243;n del objeto
     * @return String - Formato JSON del objeto
     */
    public static String fnSharedPreferJSON(Context poContext, EnumPreferenceKey poPreferenceKey, Object poKeyObject) {
        return fnSharedPreferJSON(poContext, poPreferenceKey.toString(), poKeyObject);
    }

    /**
     * Recupera el objeto en formato JSON seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext     - Contexto de la aplicaci&#243;n
     * @param psKeyObject   - Llave de identificaci&#243;n del objeto
     * @param poObjectClass - Clase del objeto
     * @return Object - Objeto en formato JSON
     */
    public static Object fnSharedPreferJSON(Context poContext, String psKeyObject, Class<?> poObjectClass) throws Exception {
        return fnSharedPreferJSON(poContext, EnumPreferenceKey.PREFERENCE, psKeyObject, poObjectClass);
    }

    /**
     * Recupera el objeto en formato JSON seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param psKeyObject      - Llave de identificaci&#243;n del objeto
     * @param poObjectClass    - Clase del objeto
     * @return Object - Objeto en formato JSON
     */
    public static Object fnSharedPreferJSON(Context poContext, String psPreferenceName, String psKeyObject, Class<?> poObjectClass) throws Exception {
        return BeanMapper.fromJson(SharedPreferenceGestor.fnSharedPreferJSON(poContext, psPreferenceName, psKeyObject), poObjectClass);
    }

    /**
     * Recupera el objeto en formato JSON seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param psKeyObject     - Llave de identificaci&#243;n del objeto
     * @param poObjectClass   - Clase del objeto
     * @return Object - Objeto en formato JSON
     */
    public static Object fnSharedPreferJSON(Context poContext, EnumPreferenceKey poPreferenceKey, String psKeyObject, Class<?> poObjectClass) throws Exception {
        return fnSharedPreferJSON(poContext, poPreferenceKey.toString(), psKeyObject, poObjectClass);
    }

    /**
     * Recupera el objeto en formato JSON seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext     - Contexto de la aplicaci&#243;n
     * @param poKeyObject   - Llave de identificaci&#243;n del objeto
     * @param poObjectClass - Clase del objeto
     * @return Object - Objeto en formato JSON
     */
    public static Object fnSharedPreferJSON(Context poContext, Object poKeyObject, Class<?> poObjectClass) throws Exception {
        return fnSharedPreferJSON(poContext, EnumPreferenceKey.PREFERENCE, poKeyObject, poObjectClass);
    }

    /**
     * Recupera el objeto en formato JSON seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param poKeyObject      - Llave de identificaci&#243;n del objeto
     * @param poObjectClass    - Clase del objeto
     * @return Object - Objeto en formato JSON
     */
    public static Object fnSharedPreferJSON(Context poContext, String psPreferenceName, Object poKeyObject, Class<?> poObjectClass) throws Exception {
        return BeanMapper.fromJson(SharedPreferenceGestor.fnSharedPreferJSON(poContext, psPreferenceName, poKeyObject.toString()), poObjectClass);
    }

    /**
     * Recupera el objeto en formato JSON seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param poKeyObject     - Llave de identificaci&#243;n del objeto
     * @param poObjectClass   - Clase del objeto
     * @return Object - Objeto en formato JSON
     */
    public static Object fnSharedPreferJSON(Context poContext, EnumPreferenceKey poPreferenceKey, Object poKeyObject, Class<?> poObjectClass) throws Exception {
        return fnSharedPreferJSON(poContext, poPreferenceKey.toString(), poKeyObject, poObjectClass);
    }

    /**
     * Manipula objetos usando el {@link Editor} del {@link SharedPreferences}
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @param listener  - Interfaz del {@link Editor} del {@link SharedPreferences}
     */
    public static void subSharedPrefer(Context poContext, OnEditorListener listener) {
        subSharedPrefer(poContext, EnumPreferenceKey.PREFERENCE, listener);
    }

    /**
     * Manipula objetos usando el {@link Editor} en el {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param listener         - Interfaz del {@link Editor} del {@link SharedPreferences}
     */
    public static void subSharedPrefer(Context poContext, String psPreferenceName, OnEditorListener listener) {
        SharedPreferences loSharedPreferences = poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        if (listener != null)
            listener.onEditor(loEditor);
        loEditor.commit();
    }

    /**
     * Manipula objetos usando el {@link Editor} en el {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param listener        - Interfaz del {@link Editor} del {@link SharedPreferences}
     */
    public static void subSharedPrefer(Context poContext, EnumPreferenceKey poPreferenceKey, OnEditorListener listener) {
        subSharedPrefer(poContext, poPreferenceKey.toString(), listener);
    }

    /**
     * Inicializa un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext   - Contexto de la aplicaci&#243;n
     * @param psKeyObject - Llave de identificaci&#243;n del objeto
     * @param poObject    - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static void subIniSharedPrefer(Context poContext, String psKeyObject, Object poObject) throws ClassNotFoundException {
        subIniSharedPrefer(poContext, EnumPreferenceKey.PREFERENCE, psKeyObject, poObject);
    }

    /**
     * Inicializa un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param psKeyObject      - Llave de identificaci&#243;n del objeto
     * @param poObject         - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static void subIniSharedPrefer(Context poContext, String psPreferenceName, String psKeyObject, Object poObject) throws ClassNotFoundException {
        SharedPreferences loSharedPreferences = poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        if (poObject instanceof String) {
            loEditor.putString(psKeyObject, (String) poObject);
        } else if (poObject instanceof Integer) {
            loEditor.putInt(psKeyObject, (Integer) poObject);
        } else if (poObject instanceof Long) {
            loEditor.putLong(psKeyObject, (Long) poObject);
        } else if (poObject instanceof Float) {
            loEditor.putFloat(psKeyObject, (Float) poObject);
        } else if (poObject instanceof Boolean) {
            loEditor.putBoolean(psKeyObject, (Boolean) poObject);
        } else {
            throw new ClassNotFoundException();
        }
        loEditor.commit();
    }

    /**
     * Inicializa un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param psKeyObject     - Llave de identificaci&#243;n del objeto
     * @param poObject        - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static void subIniSharedPrefer(Context poContext, EnumPreferenceKey poPreferenceKey, String psKeyObject, Object poObject) throws ClassNotFoundException {
        subIniSharedPrefer(poContext, poPreferenceKey.toString(), psKeyObject, poObject);
    }

    /**
     * Inicializa un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext   - Contexto de la aplicaci&#243;n
     * @param poKeyObject - Llave de identificaci&#243;n del objeto
     * @param poObject    - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static void subIniSharedPrefer(Context poContext, Object poKeyObject, Object poObject) throws ClassNotFoundException {
        subIniSharedPrefer(poContext, EnumPreferenceKey.PREFERENCE, poKeyObject, poObject);
    }

    /**
     * Inicializa un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param poKeyObject      - Llave de identificaci&#243;n del objeto
     * @param poObject         - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static void subIniSharedPrefer(Context poContext, String psPreferenceName, Object poKeyObject, Object poObject) throws ClassNotFoundException {
        SharedPreferences loSharedPreferences = poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        if (poObject instanceof String) {
            loEditor.putString(poKeyObject.toString(), (String) poObject);
        } else if (poObject instanceof Integer) {
            loEditor.putInt(poKeyObject.toString(), (Integer) poObject);
        } else if (poObject instanceof Long) {
            loEditor.putLong(poKeyObject.toString(), (Long) poObject);
        } else if (poObject instanceof Float) {
            loEditor.putFloat(poKeyObject.toString(), (Float) poObject);
        } else if (poObject instanceof Boolean) {
            loEditor.putBoolean(poKeyObject.toString(), (Boolean) poObject);
        } else {
            throw new ClassNotFoundException();
        }
        loEditor.commit();
    }

    /**
     * Inicializa un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param poKeyObject     - Llave de identificaci&#243;n del objeto
     * @param poObject        - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static void subIniSharedPrefer(Context poContext, EnumPreferenceKey poPreferenceKey, Object poKeyObject, Object poObject) throws ClassNotFoundException {
        subIniSharedPrefer(poContext, poPreferenceKey.toString(), poKeyObject, poObject);
    }

    /**
     * Cierra o remueve un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext   - Contexto de la aplicaci&#243;n
     * @param psKeyObject - Llave de identificaci&#243;n del objeto
     */
    public static void subCloseSharedPrefer(Context poContext, String psKeyObject) {
        subCloseSharedPrefer(poContext, EnumPreferenceKey.PREFERENCE, psKeyObject);
    }

    /**
     * Cierra o remueve un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param psKeyObject      - Llave de identificaci&#243;n del objeto
     */
    public static void subCloseSharedPrefer(Context poContext, String psPreferenceName, String psKeyObject) {
        SharedPreferences loSharedPreferences = poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.remove(psKeyObject);
        loEditor.commit();
    }

    /**
     * Cierra o remueve un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param psKeyObject     - Llave de identificaci&#243;n del objeto
     */
    public static void subCloseSharedPrefer(Context poContext, EnumPreferenceKey poPreferenceKey, String psKeyObject) {
        subCloseSharedPrefer(poContext, poPreferenceKey.toString(), psKeyObject);
    }

    /**
     * Cierra o remueve un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext   - Contexto de la aplicaci&#243;n
     * @param poKeyObject - Llave de identificaci&#243;n del objeto
     */
    public static void subCloseSharedPrefer(Context poContext, Object poKeyObject) {
        subCloseSharedPrefer(poContext, EnumPreferenceKey.PREFERENCE, poKeyObject);
    }

    /**
     * Cierra o remueve un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param poKeyObject      - Llave de identificaci&#243;n del objeto
     */
    public static void subCloseSharedPrefer(Context poContext, String psPreferenceName, Object poKeyObject) {
        SharedPreferences loSharedPreferences = poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE);
        SharedPreferences.Editor loEditor = loSharedPreferences.edit();
        loEditor.remove(poKeyObject.toString());
        loEditor.commit();
    }

    /**
     * Cierra o remueve un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param poKeyObject     - Llave de identificaci&#243;n del objeto
     */
    public static void subCloseSharedPrefer(Context poContext, EnumPreferenceKey poPreferenceKey, Object poKeyObject) {
        subCloseSharedPrefer(poContext, poPreferenceKey.toString(), poKeyObject);
    }

    /**
     * Verifica si existe un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext   - Contexto de la aplicaci&#243;n
     * @param psKeyObject - Llave de identificaci&#243;n del objeto
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si la verificaci&#243;n es correcta</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnExiSharedPrefer(Context poContext, String psKeyObject) {
        return fnExiSharedPrefer(poContext, EnumPreferenceKey.PREFERENCE, psKeyObject);
    }

    /**
     * Verifica si existe un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param psKeyObject      - Llave de identificaci&#243;n del objeto
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si la verficaci&#243;n es correcta</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnExiSharedPrefer(Context poContext, String psPreferenceName, String psKeyObject) {
        return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).contains(psKeyObject);
    }

    /**
     * Verifica si existe un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param psKeyObject     - Llave de identificaci&#243;n del objeto
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si la verficaci&#243;n es correcta</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnExiSharedPrefer(Context poContext, EnumPreferenceKey poPreferenceKey, String psKeyObject) {
        return fnExiSharedPrefer(poContext, poPreferenceKey.toString(), psKeyObject);
    }

    /**
     * Verifica si existe un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext   - Contexto de la aplicaci&#243;n
     * @param poKeyObject - Llave de identificaci&#243;n del objeto
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si la verificaci&#243;n es correcta</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnExiSharedPrefer(Context poContext, Object poKeyObject) {
        return fnExiSharedPrefer(poContext, EnumPreferenceKey.PREFERENCE, poKeyObject);
    }

    /**
     * Verifica si existe un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param poKeyObject      - Llave de identificaci&#243;n del objeto
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si la verficaci&#243;n es correcta</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnExiSharedPrefer(Context poContext, String psPreferenceName, Object poKeyObject) {
        return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).contains(poKeyObject.toString());
    }

    /**
     * Verifica si existe un objeto seg&#250;n una llave en el {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param poKeyObject     - Llave de identificaci&#243;n del objeto
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si la verficaci&#243;n es correcta</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnExiSharedPrefer(Context poContext, EnumPreferenceKey poPreferenceKey, Object poKeyObject) {
        return fnExiSharedPrefer(poContext, poPreferenceKey.toString(), poKeyObject);
    }

    /**
     * Recupera el objeto seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext     - Contexto de la aplicaci&#243;n
     * @param psKeyObject   - Llave de identificaci&#243;n del objeto
     * @param poObjectClass - Clase del objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     * @return Object - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static Object fnSharedPrefer(Context poContext, String psKeyObject, Class<?> poObjectClass) throws ClassNotFoundException {
        return fnSharedPrefer(poContext, EnumPreferenceKey.PREFERENCE, psKeyObject, poObjectClass);
    }

    /**
     * Recupera el objeto seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - nombre de la preferencia
     * @param psKeyObject      - Llave de identificaci&#243;n del objeto
     * @param poObjectClass    - Clase del objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     * @return Object - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static Object fnSharedPrefer(Context poContext, String psPreferenceName, String psKeyObject, Class<?> poObjectClass) throws ClassNotFoundException {
        if (poObjectClass == String.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getString(psKeyObject, "");
        } else if (poObjectClass == Integer.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getInt(psKeyObject, 0);
        } else if (poObjectClass == Long.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getLong(psKeyObject, (long) 0);
        } else if (poObjectClass == Float.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getFloat(psKeyObject, (float) 0);
        } else if (poObjectClass == Boolean.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getBoolean(psKeyObject, false);
        } else {
            throw new ClassNotFoundException();
        }
    }

    /**
     * Recupera el objeto seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param psKeyObject     - Llave de identificaci&#243;n del objeto
     * @param poObjectClass   - Clase del objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     * @return Object - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static Object fnSharedPrefer(Context poContext, EnumPreferenceKey poPreferenceKey, String psKeyObject, Class<?> poObjectClass) throws ClassNotFoundException {
        return fnSharedPrefer(poContext, poPreferenceKey.toString(), psKeyObject, poObjectClass);
    }

    /**
     * Recupera el objeto seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext     - Contexto de la aplicaci&#243;n
     * @param poKeyObject   - Llave de identificaci&#243;n del objeto.
     * @param poObjectClass - Clase del objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     * @return Object - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static Object fnSharedPrefer(Context poContext, Object poKeyObject, Class<?> poObjectClass) throws ClassNotFoundException {
        return fnSharedPrefer(poContext, poKeyObject, EnumPreferenceKey.PREFERENCE, poObjectClass);
    }

    /**
     * Recupera el objeto seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param poKeyObject      - Llave de identificaci&#243;n del objeto
     * @param psPreferenceName - Nombre de la preferencia
     * @param poObjectClass    - Clase del objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     * @return Object - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static Object fnSharedPrefer(Context poContext, Object poKeyObject, String psPreferenceName, Class<?> poObjectClass) throws ClassNotFoundException {
        if (poObjectClass == String.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getString(poKeyObject.toString(), "");
        } else if (poObjectClass == Integer.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getInt(poKeyObject.toString(), 0);
        } else if (poObjectClass == Long.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getLong(poKeyObject.toString(), (long) 0);
        } else if (poObjectClass == Float.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getFloat(poKeyObject.toString(), (float) 0);
        } else if (poObjectClass == Boolean.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getBoolean(poKeyObject.toString(), false);
        } else {
            throw new ClassNotFoundException();
        }
    }

    /**
     * Recupera el objeto seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poKeyObject     - Llave de identificaci&#243;n del objeto
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param poObjectClass   - Clase del objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     * @return Object - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static Object fnSharedPrefer(Context poContext, Object poKeyObject, EnumPreferenceKey poPreferenceKey, Class<?> poObjectClass) throws ClassNotFoundException {
        return fnSharedPrefer(poContext, poKeyObject, poPreferenceKey.toString(), poObjectClass);
    }

    /**
     * Recupera el objeto seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext      - Contexto de la aplicaci&#243;n
     * @param psKeyObject    - Llave de identificaci&#243;n del objeto
     * @param poDefaultValue - Valor por defecto
     * @param poObjectClass  - Clase del objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     * @return Object - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static Object fnSharedPrefer(Context poContext, String psKeyObject, Object poDefaultValue, Class<?> poObjectClass) throws ClassNotFoundException {
        return fnSharedPrefer(poContext, EnumPreferenceKey.PREFERENCE, psKeyObject, poDefaultValue, poObjectClass);
    }

    /**
     * Recupera el objeto seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param psKeyObject      - Llave de identificaci&#243;n del objeto
     * @param poDefaultValue   - Valor por defecto
     * @param poObjectClass    - Clase del objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     * @return Object - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static Object fnSharedPrefer(Context poContext, String psPreferenceName, String psKeyObject, Object poDefaultValue, Class<?> poObjectClass) throws ClassNotFoundException {
        if (poObjectClass == String.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getString(psKeyObject, (String) poDefaultValue);
        } else if (poObjectClass == Integer.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getInt(psKeyObject, (Integer) poDefaultValue);
        } else if (poObjectClass == Long.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getLong(psKeyObject, (Long) poDefaultValue);
        } else if (poObjectClass == Float.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getFloat(psKeyObject, (Float) poDefaultValue);
        } else if (poObjectClass == Boolean.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getBoolean(psKeyObject, (Boolean) poDefaultValue);
        } else {
            throw new ClassNotFoundException();
        }
    }

    /**
     * Recupera el objeto seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param psKeyObject     - Llave de identificaci&#243;n del objeto
     * @param poDefaultValue  - Valor por defecto
     * @param poObjectClass   - Clase del objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     * @return Object - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static Object fnSharedPrefer(Context poContext, EnumPreferenceKey poPreferenceKey, String psKeyObject, Object poDefaultValue, Class<?> poObjectClass) throws ClassNotFoundException {
        return fnSharedPrefer(poContext, poPreferenceKey.toString(), psKeyObject, poDefaultValue, poObjectClass);
    }

    /**
     * Recupera el objeto seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext      - Contexto de la aplicaci&#243;n
     * @param poKeyObject    - Llave de identificaci&#243;n del objeto
     * @param poDefaultValue - Valor por defecto
     * @param poObjectClass  - Clase del objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     * @return Object - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static Object fnSharedPrefer(Context poContext, Object poKeyObject, Object poDefaultValue, Class<?> poObjectClass) throws ClassNotFoundException {
        return fnSharedPrefer(poContext, EnumPreferenceKey.PREFERENCE, poKeyObject, poDefaultValue, poObjectClass);
    }

    /**
     * Recupera el objeto seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext        - Contexto de la aplicaci&#243;n
     * @param psPreferenceName - Nombre de la preferencia
     * @param poKeyObject      - Llave de identificaci&#243;n del objeto
     * @param poDefaultValue   - Valor por defecto
     * @param poObjectClass    - Clase del objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     * @return Object - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static Object fnSharedPrefer(Context poContext, String psPreferenceName, Object poKeyObject, Object poDefaultValue, Class<?> poObjectClass) throws ClassNotFoundException {
        if (poObjectClass == String.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getString(poKeyObject.toString(), (String) poDefaultValue);
        } else if (poObjectClass == Integer.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getInt(poKeyObject.toString(), (Integer) poDefaultValue);
        } else if (poObjectClass == Long.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getLong(poKeyObject.toString(), (Long) poDefaultValue);
        } else if (poObjectClass == Float.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getFloat(poKeyObject.toString(), (Float) poDefaultValue);
        } else if (poObjectClass == Boolean.class) {
            return poContext.getSharedPreferences(psPreferenceName, Context.MODE_PRIVATE).getBoolean(poKeyObject.toString(), (Boolean) poDefaultValue);
        } else {
            throw new ClassNotFoundException();
        }
    }

    /**
     * Recupera el objeto seg&#250;n una llave del {@link SharedPreferences}
     *
     * @param poContext       - Contexto de la aplicaci&#243;n
     * @param poPreferenceKey - {@link EnumPreferenceKey} con el nombre de la preferencia
     * @param poKeyObject     - Llave de identificaci&#243;n del objeto
     * @param poDefaultValue  - Valor por defecto
     * @param poObjectClass   - Clase del objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     * @return Object - Objeto {String}, {Integer}, { Long}, {Float}, { Boolean}
     */
    public static Object fnSharedPrefer(Context poContext, EnumPreferenceKey poPreferenceKey, Object poKeyObject, Object poDefaultValue, Class<?> poObjectClass) throws ClassNotFoundException {
        return fnSharedPrefer(poContext, poPreferenceKey.toString(), poKeyObject, poDefaultValue, poObjectClass);
    }

    /**
     * Interfaz que implementa la manipulaci&#243;n del {@link Editor} del {@link SharedPreferences}
     *
     * @author jroeder
     */
    public static interface OnEditorListener {
        /**
         * Realiza una acci&#243;n al recuperar el {@link Editor} del {@link SharedPreferences}
         *
         * @param loEditor {@link Editor} del {@link SharedPreferences}
         */
        public void onEditor(SharedPreferences.Editor loEditor);
    }
}
