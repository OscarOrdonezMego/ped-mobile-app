package pe.com.nextel.android.dal;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;

import pe.com.nextel.android.actividad.NexInvalidOperatorActivity.NexInvalidOperatorException;

/**
 * Clase que realiza la gesti&#243;n de manipulaci&#243;n de datos del SQLite
 *
 * @deprecated Utilizar clase {@link DALGestor}
 */
public class BDFramework {

    private String DB_NOMBRE = "DB.FRAMEWORK";
    private int DB_VERSION = 1;
    private String DB_LAST_ERROR = "";

    private SQLiteDatabase db;
    private Cursor cursor;

    private Context context;
    private DatabaseHelper DBHelper;

    /**
     * Indicador de impresi&#243;n de log
     */
    public boolean log = false;

    /**
     * Retorna la instancia de la base de datos del SQLite
     *
     * @return {@link SQLiteDatabase}
     */
    public SQLiteDatabase getInstance() {
        if (db == null) {
            open();
        }
        return db;
    }

    /**
     * Desajunta el contexto de la aplicaci&#243;n
     */
    public void dettach() {
//	    	try{
//	    		if(db.inTransaction()){
//	    			db.endTransaction();
//	    		}
//	    		db.close();
//	    	}catch(Exception e){
//	    		Log.e("BDFramework","dettach Exception",e);
//	    	}
        context = null;
    }

    /**
     * Adjunta la clase a un nuevo contexto de la aplicaci&#243;n
     *
     * @param poContext Contexto de la aplicaci&#243;n
     */
    public void attach(Context poContext) {
        context = poContext;
//	    	try{
//	    		open();
//	    	}catch(Exception e){
//	    		Log.e("BDFramework","attach Exception",e);
//	    	}
    }

    /**
     * Constructor de la clase
     *
     * @param ctx Contexto de la aplicaci&#243;n
     */
    public BDFramework(Context ctx) throws NexInvalidOperatorException {
        //if(!Utilitario.fnVerOperador(ctx))
        //	throw new NexInvalidOperatorException(ctx);
        this.context = ctx;
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context, String nombre, int version) {
            super(context, nombre, null, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
                /*db.execSQL(TABLA_PRUEBA);*/
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion,
                              int newVersion) {
	            /*Log.w("LOG_VERSION", "Verificando version vieja" + oldVersion 
	                  + " nueva " + newVersion );
	            db.execSQL(DROP_TABLA_PRUEBA);
	            onCreate(db);*/
        }
    }

    /**
     * Apertura la base de datos SQLite
     *
     * @return {@link BDFramework}
     * @throws SQLException
     */
    public BDFramework open() throws SQLException {
        if (DBHelper == null) {
            DBHelper = new DatabaseHelper(context, DB_NOMBRE, DB_VERSION);
        }
        db = DBHelper.getWritableDatabase();
        return this;
    }

    /**
     * Clausura la base de datos SQLite
     */
    public void close() {
        DBHelper.close();
    }

    /**
     * Verifica la existencia de la base de datos SQLite. Si no existe, la apertura.
     *
     * @return boolean
     */
    public boolean verificaExistencia() {
        boolean resultado = false;
        if (db == null) {
            open();
            resultado = true;
        } else {
            resultado = true;
        }
        return resultado;
    }

    //..........................................................

    /**
     * Realiza una ejecuci&#243;n de diversas sentencias SQL en un archivo plano
     *
     * @param file Archivo plano
     * @return boolean
     */
    public boolean insertBatch(File file) {
        boolean resultado = false;
        try {
            db.beginTransaction();
            String sqlComand;
            Log.i("Ok", "Metiendo en DB");
            BufferedReader br = new BufferedReader(new FileReader(file));
            while ((sqlComand = br.readLine()) != null) {
                //System.out.println(sqlComand);
                db.execSQL(sqlComand);
            }
            br.close();
            Log.i("Ok", "Termino DB");
            db.setTransactionSuccessful();
            resultado = true;
        } catch (IOException e) {

        } catch (Exception e) {
            setDB_LAST_ERROR(e.getMessage());
        } finally {
            db.endTransaction();
            db.close();
        }

        return resultado;
    }

    private void setDB_LAST_ERROR(String dB_LAST_ERROR) {
        DB_LAST_ERROR = dB_LAST_ERROR;
    }

    /**
     * Retorna el &#250;ltimo error generado
     *
     * @return {String}
     */
    public String getDB_LAST_ERROR() {
        return DB_LAST_ERROR;
    }

    /**
     * Retorna el nombre de la base de datos
     *
     * @return {String}
     */
    public String getDB_NOMBRE() {
        return DB_NOMBRE;
    }

    /**
     * Instancia el nombre de la base de datos
     *
     * @param dB_NOMBRE
     */
    public void setDB_NOMBRE(String dB_NOMBRE) {
        DB_NOMBRE = dB_NOMBRE;
    }

    /**
     * Retorna la versi&#243;n de la base de datos
     *
     * @return int
     */
    public int getDB_VERSION() {
        return DB_VERSION;
    }

    /**
     * Instancia la versi&#243;n de la base de datos
     *
     * @param dB_VERSION
     */
    public void setDB_VERSION(int dB_VERSION) {
        DB_VERSION = dB_VERSION;
    }

}

