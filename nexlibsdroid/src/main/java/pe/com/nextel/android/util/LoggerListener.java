package pe.com.nextel.android.util;

/**
 * Listener para cuando se ha escrito un mensaje en el log (en caso se quiera mostrar en pantalla).
 *
 * @author azamora
 * @version 1.0.0
 * @since 1.6.4
 */
public interface LoggerListener {

    /**
     * Notifica la escritura de un mensaje.
     *
     * @param message Mensaje escrito.
     */
    public void notifyMessage(String message);

}
