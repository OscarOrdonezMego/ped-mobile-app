package pe.com.nextel.android.service;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import pe.com.nextel.android.R;
import pe.com.nextel.android.dal.DALNServices;
import pe.com.nextel.android.receiver.NServicesInstallerReceiver;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTheme;
import pe.com.nextel.android.util.Logger;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;

/**
 * Implementa un {@link Service} que realiza la descarga del APK instalador del NServices.</br>
 * Utilizar el m&#233;todo {@link #createIntentForIniDownload} para crear el {@link Intent} que invoca a este servicio.</b>
 * La descarga se almacenar&#225; en la ruta <b>/NextelApp/&#60;R.string.app_name&#62;/download/</b>
 * </br>Se debe declarar en el <b>AndroidManifest.xml</b> la siguiente l&#237;nea:
 * </br></br>&#60;service android:name="pe.com.nextel.android.service.NServicesDownloadService"/&#62;
 *
 * @author jroeder
 */
public class NServicesDownloadService extends Service implements Runnable {
    private String isDownloadURL;
    private int iiResStyleMaster;
    private int iiResAttr;
    private NotificationManager ioNotifyManager;

    private enum EnumTipoNotificacion {OK, ERROR, PROGRESO}

    private int iiPorcentaje = 0;
    private WakeLock ioWakeLock = null;
    //	private static EnumTipoNotificacion ioCurrentNotification;
    private static String NSERVICES_DOWNLOADER_SERVICE_STOP = ".action.stop";
    private Intent _intent = null;
    private boolean _threadAlive = false;

    /**
     * Constructor del {@link NServicesDownloadService}
     */
    public NServicesDownloadService() {
//		super("NServicesDownloadService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ioNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }


    @Override
    @Deprecated
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        try {
            _intent = intent;
            isDownloadURL = intent.getStringExtra(NServicesConfiguration.NSERVICES_BUNLDE_EXTRA_DOWNLOAD_URL);
            iiResStyleMaster = intent.getIntExtra(NServicesConfiguration.NSERVICES_BUNDLE_EXTRA_RESSTYLEMASTER, R.style.Theme_Master);
            iiResAttr = EnumTheme.getCurrentTheme(this).getAttrRes();
            if (intent.getAction() == null) {
//				subIniciarDescarga();
                new Thread(this).start();
            } else if (intent.getAction().equals(getPackageName() + NSERVICES_DOWNLOADER_SERVICE_STOP)) {
                stopSelf();
            }
        } catch (Exception e) {
            Logger.e(e);
            subError(e);
        }
    }

    @SuppressWarnings("finally")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            _intent = intent;
            isDownloadURL = intent.getStringExtra(NServicesConfiguration.NSERVICES_BUNLDE_EXTRA_DOWNLOAD_URL);
            iiResStyleMaster = intent.getIntExtra(NServicesConfiguration.NSERVICES_BUNDLE_EXTRA_RESSTYLEMASTER, R.style.Theme_Master);
            iiResAttr = EnumTheme.getCurrentTheme(this).getAttrRes();
            if (intent.getAction() == null) {
//				subIniciarDescarga();
                new Thread(this).start();
            } else if (intent.getAction().equals(getPackageName() + NSERVICES_DOWNLOADER_SERVICE_STOP)) {
                stopSelf();
            }
        } catch (Exception e) {
            Logger.e(e);
            subError(e);
        } finally {
            return START_STICKY;
        }
    }

    private void subIniciarDescarga() throws Exception {
        subAbrirWakeLock();

        iiPorcentaje = 0;
        subNotificar(getString(R.string.nservices_download_progress), EnumTipoNotificacion.PROGRESO);

        URL loUrls = new URL(isDownloadURL);
        HttpURLConnection loHttp = (HttpURLConnection) loUrls.openConnection();
        loHttp.setRequestMethod("GET");
        if (Build.VERSION.SDK_INT < 14)
            loHttp.setDoOutput(true);
        loHttp.connect();

        int liLongitudTotal = loHttp.getContentLength();

        String lsRuta = Environment.getExternalStorageDirectory()
                + Utilitario.fnNomCarpeta(Utilitario.getStringResourceByName(this, "app_name")
                , NServicesConfiguration.NSERVICES_DOWNLOAD_FOLDER);
        File loFile = new File(lsRuta);
        loFile.mkdirs();
        File loOutput = new File(loFile, NServicesConfiguration.NSERVICES_DOWNLOAD_APK);
        FileOutputStream loFileOutput = new FileOutputStream(loOutput);

        InputStream loInput = loHttp.getInputStream();

        byte[] liBuffer = new byte[1024];
        int liContador = 0;
        int liTotal = 0;
        int liPasos = 0;
        while ((liContador = loInput.read(liBuffer)) != -1) {
            liTotal += liContador;
            loFileOutput.write(liBuffer, 0, liContador);
            if (liLongitudTotal > 0 && (liPasos % 100 == 0)) {
                iiPorcentaje = liTotal * 100 / liLongitudTotal;
                subNotificar(getString(R.string.nservices_download_progress), EnumTipoNotificacion.PROGRESO);
            }
            ++liPasos;
        }
        loFileOutput.close();
        loInput.close();
        loFileOutput.close();
        loInput.close();

        iiPorcentaje = 100;
        String lsPackge = getPackageName() + NServicesConfiguration.NSERVICES_BROADCAST_DOWNLOADED;
        sendBroadcast(new Intent(lsPackge).putExtra(getString(R.string.nservices_keyPreExitoDescarga), true));
        subNotificar(getString(R.string.nservices_download_ok), EnumTipoNotificacion.OK);

        subCerrarWakeLock();
        stopSelf();
    }

    private void subNotificar(String psMensaje, EnumTipoNotificacion poTipo) {
        Logger.d("PRO", "NServicesDownloadService.subNotificar.psMensaje: " + psMensaje);
        Logger.d("PRO", "NServicesDownloadService.subNotificar.poTipo: " + poTipo);
        String NOTIFICATION_CHANNEL_ID = "NServicesDownloadServiceChannel";
        NotificationCompat.Builder loBuilder =
                new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                        .setTicker(psMensaje)
                        .setAutoCancel(true)
                        .setContentText(psMensaje)
                        .setContentTitle(Utilitario.getStringResourceByName(this, "app_name"))
                        .setWhen(System.currentTimeMillis())
                        .setStyle(new NotificationCompat.BigTextStyle());

        switch (poTipo) {
            case PROGRESO:
                if (!_threadAlive)
                    return;
                loBuilder.setSmallIcon(Utilitario.obtainResourceId(this,
                        iiResStyleMaster,
                        iiResAttr,
                        R.attr.NServicesNotificationIconDescarga)
                );
                loBuilder.setOngoing(true);
                loBuilder.setProgress(100, iiPorcentaje, /*(iiPorcentaje == 0)*/false);
                break;
            case OK:
                loBuilder.setVibrate(new long[]{0, 800});
                loBuilder.setSmallIcon(Utilitario.obtainResourceId(this,
                        iiResStyleMaster,
                        iiResAttr,
                        R.attr.NServicesNotificationIconOk)
                );
                _threadAlive = false;
                break;
            case ERROR:
                loBuilder.setContentText(getString(R.string.nservices_download_error));
                loBuilder.setSubText(psMensaje);
                loBuilder.setVibrate(new long[]{0, 300, 200, 300});
                loBuilder.setSmallIcon(Utilitario.obtainResourceId(this,
                        iiResStyleMaster,
                        iiResAttr,
                        R.attr.NServicesNotificationIconAdvertencia)
                );
                _threadAlive = false;
                break;
        }

        ioNotifyManager.notify(NServicesConfiguration.NSERVICES_NOTICATION_INSTALLER_CODE, loBuilder.build());
    }

    private void subAbrirWakeLock() {
        try {
            if (ioWakeLock == null) {
                Logger.d("Inicio acquire wakelock");
                PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                ioWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, Utilitario.getStringResourceByName(this, "app_name"));
            } else {
                Logger.d("No se creo WakeLock: ya existia.");
            }

            if (!ioWakeLock.isHeld()) {
                ioWakeLock.acquire();
                Logger.d("Fin acquire wakelock");
            } else {
                Logger.d("Wake lock no estaba liberado.");
            }
        } catch (Exception e) {
            Logger.e("Error al abrir WakeLock");
            Logger.e(e);
        }
    }

    private void subCerrarWakeLock() {
        try {
            if (ioWakeLock != null) {
                if (ioWakeLock.isHeld()) {
                    Logger.d("Inicio release wakelock");
                    try {
                        ioWakeLock.release();
                    } catch (Throwable t) {
                        Logger.e(t);
                    }
                    Logger.d("Fin release wakelock");
                } else {
                    Logger.d("Wakelock ya estaba liberado.");
                }
            }
        } catch (Exception e) {
            Logger.e("Error al liberar WakeLock");
            Logger.e(e);
        }
    }

    /**
     * Crea un {@link Intent} con los datos necesarios para ejecutar un {@link NServicesDownloadService}
     *
     * @param context        {@link Context} de la aplicaci&#243;n
     * @param pathAPK        URL de descarga del APK instalador
     * @param resStyleMaster Id de recurso de estilo maestro de la aplicaci&#243;n
     * @return {@link Intent}
     */
    public static Intent createIntentForIniDownload(Context context, String pathAPK, int resStyleMaster) {
//		if(_intent == null)
        Intent _intent = new Intent(context.getApplicationContext(), NServicesDownloadService.class);
        _intent.putExtra(NServicesConfiguration.NSERVICES_BUNLDE_EXTRA_DOWNLOAD_URL, pathAPK);
        _intent.putExtra(NServicesConfiguration.NSERVICES_BUNDLE_EXTRA_RESSTYLEMASTER, resStyleMaster);
        return _intent;
    }

    /**
     * Inicia el Servicio {@link NServicesDownloadService}
     *
     * @param context        {@link Context} de la aplicaci&#243;n
     * @param pathAPK        URL de descarga del instalador del NServices
     * @param resStyleMaster Id de recurso del estilo maestro
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si se ha ejecutado el servicio</li>
     * <li><b>false</b> si ya existe un servicio iniciado</li>
     * </ul>
     */
    public static boolean startService(Context context, String pathAPK, int resStyleMaster) {
        if (Utilitario.verifyRunningService(context, NServicesDownloadService.class))
            return false;
        context.startService(createIntentForIniDownload(context, pathAPK, resStyleMaster));
        return true;
    }

    private void subError(Exception e) {
        subError(e + "");
    }

    private void subError(String error) {
        NServicesInstallerReceiver.subDeleteInstaller(this);
        subNotificar(error, EnumTipoNotificacion.ERROR);
        sendBroadcast(new Intent(getPackageName() +
                NServicesConfiguration.NSERVICES_BROADCAST_DOWNLOADED)
                .putExtra(getString(R.string.nservices_keyPreExitoDescarga), false));
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.d("PRO", "NServicesDownloadService.onTaskRemoved.isDownloadURL: " + isDownloadURL);
        Log.d("PRO", "NServicesDownloadService.onTaskRemoved.iiResStyleMaster: " + iiResStyleMaster);
        Log.d("PRO", "NServicesDownloadService.onTaskRemoved.iiResAttr: " + iiResAttr);
        subError(getString(R.string.nservices_download_error_descargadetenida));
        stopSelf();
        super.onTaskRemoved(rootIntent);
    }

    @Override
    public void onDestroy() {
        Log.d("PRO", "NServicesDownloadService.onDestroy._intent: " + _intent);
        if (_intent != null && _intent.getAction() != null && _intent.getAction().equals(getPackageName() + NSERVICES_DOWNLOADER_SERVICE_STOP))
            subError(getString(R.string.nservices_download_error_descargadetenida));
        super.onDestroy();
    }

    /**
     * Detiene el servicio en ejecuci&#243;n.</br>
     * Utiliza como URL de APK de descarga el que se encuentra almacenado en la tabla <b>CFNServices</b>
     * de la base del SQLite con el nombre que contiene el recurso <b>R.string.db_name</b></br>
     * Utiliza por defecto el estilo maestro <b>R.style.Theme_Master</b>
     *
     * @param context {@link Context} de la aplicaci&#243;n
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> Si se ha ejecutado el detenido del servicio iniciado</li>
     * <li><b>false</b> si no existe un servicio iniciado</li>
     * </ul>
     */
    public static boolean stopService(Context context) throws Exception {
        return stopService(context, R.style.Theme_Master);
    }

    /**
     * Detiene el servicio en ejecuci&#243;n.</br>
     * Utiliza como URL de APK de descarga el que se encuentra almacenado en la tabla <b>CFNServices</b>
     * de la base del SQLite con el nombre que contiene el recurso <b>R.string.db_name</b>
     *
     * @param context        {@link Context} de la aplicaci&#243;n
     * @param resStyleMaster Id de recurso del estilo maestro de la aplicaci&#243;n
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> Si se ha ejecutado el detenido del servicio iniciado</li>
     * <li><b>false</b> si no existe un servicio iniciado</li>
     * </ul>
     */
    public static boolean stopService(Context context, int resStyleMaster) throws Exception {
        return stopService(Utilitario.getStringResourceByName(context, "db_name"), context, resStyleMaster);
    }

    /**
     * Detiene el servicio en ejecuci&#243;n.</br>
     * Utiliza como URL del APK de descarga el que se encuentra almacenado en la tabla <b>CFNServices</b>
     * de la base del SQLite con el nombre que contiene el par&#225;metro <b>dbName</b>
     *
     * @param dbName         Nombre de la base de datos SQLite
     * @param context        {@link Context} de la aplicaci&#243;n
     * @param resStyleMaster Id de recurso del estilo maestro de la aplicaci&#243;n
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> Si se ha ejecutado el detenido del servicio iniciado</li>
     * <li><b>false</b> si no existe un servicio iniciado</li>
     * </ul>
     */
    public static boolean stopService(String dbName, Context context, int resStyleMaster) throws Exception {
        return stopService(context, DALNServices.getInstance(context, dbName).fnSelNServicesAPK(), resStyleMaster);
    }

    /**
     * Detiene el servicio en ejecuci&#243;n
     *
     * @param context        {@link Context} de la aplicaci&#243;n
     * @param pathAPK        URL del APK instalador del NServices
     * @param resStyleMaster Id de recurso del estilo maestro de la aplicaci&#243;n
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> Si se ha ejecutado el detenido del servicio iniciado</li>
     * <li><b>false</b> si no existe un servicio iniciado</li>
     * </ul>
     */
    public static boolean stopService(Context context, String pathAPK, int resStyleMaster) {
        if (!Utilitario.verifyRunningService(context, NServicesDownloadService.class))
            return false;
        context.startService(createIntentForIniDownload(context, pathAPK, resStyleMaster).setAction(context.getPackageName() + NSERVICES_DOWNLOADER_SERVICE_STOP));
        return true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void run() {
        try {
            _threadAlive = true;
            subIniciarDescarga();
        } catch (Exception e) {
            Logger.e(e);
            subError(e);
        }
    }
}