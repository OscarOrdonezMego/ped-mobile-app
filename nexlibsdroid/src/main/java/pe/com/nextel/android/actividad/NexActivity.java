package pe.com.nextel.android.actividad;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentIntegratorRegister;
import com.google.zxing.integration.android.IntentResultCallback;

import java.util.ArrayList;
import java.util.List;

import greendroid.app.GDActivity;
import greendroid.widget.ActionBarGD;
import greendroid.widget.ActionBarGD.Type;
import greendroid.widget.ActionBarItem;
import greendroid.widget.NormalActionBarItem;
import pe.com.nextel.android.R;
import pe.com.nextel.android.bean.BeanExtras;
import pe.com.nextel.android.bean.BeanMapper;
import pe.com.nextel.android.bean.BeanPhoto;
import pe.com.nextel.android.http.HttpActualizar;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumDialog;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumServerResponse;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTheme;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.ContextMenuFragment;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo.DialogFragmentYesNoPairListener;
import pe.com.nextel.android.util.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.util.NexActivityGestor;
import pe.com.nextel.android.util.OptionsMenuFragment;
import pe.com.nextel.android.util.OptionsMenuFragment.OptionsMenuFragmentListener;
import pe.com.nextel.android.util.StateHolder;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.util.menu.ContextMenuBuilder;
import pe.com.nextel.android.util.menu.MenuBuilder;
import pe.com.nextel.android.util.menu.NexMenu;
import pe.com.nextel.android.widget.NexMenuDrawerLayout;
import pe.com.nextel.android.widget.NexMenuDrawerLayout.NexMenuCallbacks;
import pe.com.nextel.android.widget.NexProgressDialog;

/**
 * Actividad base de la estructura de trabajo del proyecto.
 *
 * @author jroeder
 */
public class NexActivity extends GDActivity implements IntentIntegratorRegister {
    //---------------------------------------
    //propiedades para la conexion Http
    //---------------------------------------
    private String httpResultado;
    private int idHttpResultado;

    /**
     * Retorna el mensaje de resultado de la conexi&#243;n
     *
     * @return {String}
     */
    public String getHttpResultado() {
        return httpResultado;
    }

    /**
     * Instancia el mensaje de resultado de la conexi&#243;n
     *
     * @param psHttpResultado Mensaje de resultado
     */
    public void setHttpResultado(String psHttpResultado) {
        this.httpResultado = psHttpResultado;
    }

    /**
     * Instancia el Id de resultado de la conexi&#243;n
     *
     * @param idHttpResultado Id de resultado
     * @deprecated Usar el m&#233;todo {@link #setEnumIdHttpResultado}
     */
    public void setIdHttpResultado(int idHttpResultado) {
        this.idHttpResultado = idHttpResultado;
    }

    /**
     * Instancia el Id de resultado de la conexi&#243;n
     *
     * @param idHttpResultado Id de resultado
     */
    public void setEnumIdHttpResultado(EnumServerResponse idHttpResultado) {
        this.idHttpResultado = idHttpResultado.getValue();
    }

    /**
     * Retorna el Id de resultado de la conexi&#243;n
     *
     * @return int
     * @deprecated Usar el m&#243;todo {@link #getEnumIdHttpResultado}
     */
    public int getIdHttpResultado() {
        return idHttpResultado;
    }

    /**
     * Retorna el Id de resultado de la conexi&#243;n
     *
     * @return {@link EnumServerResponse}
     */
    public EnumServerResponse getEnumIdHttpResultado() {
        return EnumServerResponse.get(idHttpResultado);
    }

    //---------------------------------------
    //propiedades para el mensaje luego de la conexion
    //---------------------------------------
    private String msgOk = "";
    private String msgError = "";

    /**
     * Instancia el t&#237tulo de di&#225;logo de conexi&#243;n satisfactoria
     *
     * @param msgOk T&#237;tulo de di&#225;logo
     */
    public void setMsgOk(String msgOk) {
        this.msgOk = msgOk;
    }

    /**
     * Retorna el t&#237;tulo de di&#225;logo de conexi&#243;n satisfactoria
     *
     * @return {String}
     */
    public String getMsgOk() {
        return msgOk;
    }

    /**
     * Instancia el t&#237;tulo de di&#225;logo de conexi&#243;n con errores
     *
     * @param msgError T&#237;tulo de di&#225;logo
     */
    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    /**
     * Instancia el t&#237;tulo de di&#225;logo de conexi&#243;n con errores
     *
     * @return {String}
     */
    public String getMsgError() {
        return msgError;
    }

    private AlertDialog ioAlertDialog;

    /**
     * Retorna el tipo de actividad {@link EnumTipActividad#NEXACTIVITY NEXACTIVITY}
     *
     * @return {@link EnumTipActividad}
     */
    public EnumTipActividad fnTipactividad() {
        return EnumTipActividad.NEXACTIVITY;
    }

    /**
     * Objeto del tipo {@link BeanExtras} obtenido del {@link Intent} de la actividad
     */
    protected BeanExtras ioBeanExtras;

    /**
     * Restaura valores de la instancia salvada
     *
     * @param poBundle {@link Bundle} con datos de instancia salvada
     */
    protected void subRestoreNexInstanceState(Bundle poBundle) {
        Log.v("PRO", this.getClass().getName() + "=>Sobreescribir metodo subRestoreNexInstanceState()");
    }

    /**
     * Salva valores de instancia
     *
     * @param poBundle {@link Bundle} con datos de instancia
     */
    protected void subSaveNexInstanceState(Bundle poBundle) {
        Log.v("PRO", this.getClass().getName() + "=>Sobreescribir metodo subSaveNexInstanceState()");
    }

    /**
     * Restaura valores de instancia salvada
     *
     * @param poBundle {@link Bundle} con datos de instancia salvada
     */
    private void restoreInstanceState(Bundle poBundle) {
        if (poBundle != null) {
            subRestoreNexInstanceState(poBundle);
        }
    }

    /**
     * Retorna el id de recurso del estilo del tema a utilizar en la actividad
     *
     * @return <b>int</b>
     */
    protected int getThemeResource() {
        return EnumTheme.BLUE.getResourceStyle(this);
    }

    /**
     * Retorna el dise&#241;o por defecto del tema de {@link DialogFragment} a utilizar
     *
     * @return {@link EnumLayoutResource}
     */
    protected EnumLayoutResource obtainResourceLayout() {
        return EnumLayoutResource.obtainLayoutResource(this);
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        restoreInstanceState(savedInstanceState);
        super.onCreate(savedInstanceState);
        NexActivityGestor.getInstance().addActivity(this);//Método invocado por jroeder 15/01/2013
        setTheme(getThemeResource());
        //if(!Utilitario.fnVerOperador(this)
        //		&& !(this instanceof NexInvalidOperatorActivity))
        //	startActivity(new Intent(this, NexInvalidOperatorActivity.class));
        setUsingDialogFragment(isUsingDialogFragment(), obtainResourceLayout());
        Bundle loBundle = getIntent().getExtras();
        if (loBundle != null) {
            String lsFiltro = loBundle.getString(ConfiguracionNextel.CONBUNDLEFILTRO);
            try {
                Log.e("Framework", "lsFiltro=" + lsFiltro);
                if (lsFiltro != null && !lsFiltro.equals("")) {
                    ioBeanExtras = (BeanExtras) BeanMapper.fromJson(lsFiltro, BeanExtras.class);
                }
            } catch (Exception e) {
                Log.e("PRO", "NexActivity.onCreate", e);
            }
            //subLLenarLista();
        }
        subIniActionBar();
        subVerificar(1);
        subSetControles();
        Object loObject = getLastNonConfigurationInstance();
        if (loObject != null) {
            if (loObject instanceof HttpActualizar) {
                setHttpActualizar((HttpActualizar) loObject);
                getHttpActualizar().attach(this);
            } else if (loObject instanceof HttpConexion) {
                setHttpConexion((HttpConexion) loObject);
                getHttpConexion().attach(NexActivity.this);
            } else if (loObject instanceof IntentResultCallback) {
                _resultScanCallback = (IntentResultCallback) loObject;
            } else if (loObject instanceof StateHolder) {
                subSetLastNexStateHolderNonConfigInstance((StateHolder) loObject);
            } else {
                subSetLastNexNonConfigurationInstance(loObject);
            }
        }
        if (getActionBarGD().getVisibility() == View.VISIBLE && isUsingOptionMenu()) {
            NexMenu _menu = new MenuBuilder(this);
            if (onCreateOptionsMenu(_menu))
                subCreateMenuOptionsActionBarItem(_menu);
        }
        try {
            findViewById(R.id.gd_action_bar_content_view).setBackgroundResource(Utilitario.obtainResourceId(this, R.attr.FrameLayoutBackgroundDrawable));
        } catch (NullPointerException e) {
            Log.e("PRO", "NexActivity.onCreate", e);
        } catch (Exception e) {
            Log.e("PRO", "NexActivity.onCreate", e);
            findViewById(R.id.gd_action_bar_content_view).setBackgroundColor(Color.WHITE);
        }
    }

    /**
     * Inicializa valores del &#250;ltimo {@link StateHolder} configurado
     *
     * @param poStateHolder {@link StateHolder} obtenido del {@link #getLastNonConfigurationInstance}
     */
    protected void subSetLastNexStateHolderNonConfigInstance(StateHolder poStateHolder) {
        Log.v("PRO", this.getClass().getName() + "=>Sobreescribir metodo subSetLastNexStateHolderNonConfigInstance()");
    }

    /**
     * Inicializa valores del objeto obtenido del {@link #getLastNonConfigurationInstance}
     *
     * @param poObject Objeto obtenido del {@link #getLastNonConfigurationInstance}
     */
    protected void subSetLastNexNonConfigurationInstance(Object poObject) {
        Log.v("PRO", this.getClass().getName() + "=>Sobreescribir metodo subSetLastNonConfigurationInstance()");
    }

    //Método sobre-escrito por jroeder 15/01/2013
    @Override
    protected void onDestroy() {
        super.onDestroy();
        NexActivityGestor.getInstance().finishActivity(this);
    }

    /**
     * Inicializa los controles de la vista de contenido de la actividad.</br>
     * En este m&#233;todo se puede llamar al m&#233;todo {@link #setActionBarContentView} que inicializa la vista de contenido.
     */
    protected void subSetControles() {
        Log.v("PRO", this.getClass().getName() + "=>Sobreescribir metodo subSetControles()");
    }

    /**
     * Inicializa el {@link ActionBarGD}</br>
     * En este m&#233;todo se puede llamar a los m&#233;todos {@link #setTitle}, {@link #getActionBarGD} y {@link #addActionBarItem}
     * para inicializar el t&#237;tulo, manipular y crear opciones del {@link ActionBarGD}</br>
     * En el caso se creen opciones con el {@link #addActionBarItem}, es necesario sobrecargar el m&#233;todo {@link #onHandleActionBarItemClick},
     * para realizar las acciones de las opciones en sus respectivos eventos.
     */
    protected void subIniActionBar() {
        Log.v("PRO", this.getClass().getName() + "=>Sobreescribir metodo subIniActionBar()");
    }

    /**
     * Invoca el m&#233;todo {@link #subAccDesMensaje()} que se ejecuta luego de mostrar un di&#225;logo con el mensaje de respuesta de la conexi&#243;n
     */
    @SuppressWarnings({"deprecation", "incomplete-switch"})
    public void subHttpResultado() {
        subVerificar(2);
        if (Build.VERSION.SDK_INT >= 11 && isUsingDialogFragment()) {
            DialogFragmentYesNo loFragmentYesNo;
            switch (getEnumIdHttpResultado()) {
                case OK:
                    loFragmentYesNo = DialogFragmentYesNo.newInstance(getFragmentManager(),
                            DialogFragmentYesNo.TAG.concat("HTTP"),
                            getMsgOk(),
                            getHttpResultado(),
                            ioDialogFragmentDesign.getIconAccept(this),
                            false,
                            ioDialogFragmentDesign);
                    break;
                case ERROR:
                    loFragmentYesNo = DialogFragmentYesNo.newInstance(getFragmentManager(),
                            DialogFragmentYesNo.TAG.concat("HTTP"),
                            getMsgError(),
                            getHttpResultado(),
                            ioDialogFragmentDesign.getIconError(this),
                            false,
                            ioDialogFragmentDesign);
                    break;
                case OKNOMSG:
//				case ConfiguracionNextel.CONSRESSERVIDOROKNOMSG:
                    subAccDesMensaje();
                    return;
                default:
                    loFragmentYesNo = DialogFragmentYesNo.newInstance(getFragmentManager(),
                            DialogFragmentYesNo.TAG.concat("HTTP"),
                            null,
                            getHttpResultado(),
                            android.R.drawable.ic_dialog_alert,
                            false);
                    loFragmentYesNo = DialogFragmentYesNo.newInstance(getFragmentManager(),
                            DialogFragmentYesNo.TAG.concat("HTTP"),
                            null,
                            getHttpResultado(),
                            ioDialogFragmentDesign.getIconError(this),
                            false,
                            ioDialogFragmentDesign);
                    break;
            }
            loFragmentYesNo.setCancelable(false);
            if (loFragmentYesNo.showDialog(getFragmentManager(), DialogFragmentYesNo.TAG.concat("HTTP")) == DialogFragmentYesNo.YES) {
                subAccDesMensaje();
            }
        } else {
            ioAlertDialog = new AlertDialog.Builder(this).create();
            ioAlertDialog.setCancelable(false);
            ioAlertDialog.setButton(getString(R.string.dlg_btnok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                    subAccDesMensaje();
                }
            });
            switch (getEnumIdHttpResultado()) {
                case OK:
                    ioAlertDialog.setTitle(getMsgOk());
                    ioAlertDialog.setMessage(getHttpResultado());
                    ioAlertDialog.setIcon(android.R.drawable.ic_dialog_info);
                    break;
                case ERROR:
                    ioAlertDialog.setTitle(getMsgError());
                    ioAlertDialog.setMessage(getHttpResultado());
                    ioAlertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    break;
                case OKNOMSG:
                    subAccDesMensaje();
                    return;
            }
            ioAlertDialog.show();
        }
    }

    /**
     * Realiza la acci&#243;n posterior al mensaje de respuesta de conexi&#243;n
     */
    protected void subAccDesMensaje() {
        Log.v("PRO", this.getClass().getName() + "=>Sobreescribir metodo subAccDesMensaje()");
    }

    private void subVerificar(int piIndice) {
        switch (piIndice) {
            case 1:
                if (this.getTitle().equals(""))
                    Log.v("PRO", this.getClass().getName() + "=>Sobreescribir metodo subIniActionBar() y poner titulo");
                break;
            case 2:
                if (msgOk.equals(""))
                    Log.v("PRO", this.getClass().getName() + "=>falta implementar this.setMsgOk(getString(R.string.actxxx_dlglogok)); en metodo onCreate");
                if (msgError.equals(""))
                    Log.v("PRO", this.getClass().getName() + "=>falta implementar this.setMsgError(getString(R.string.actxxx_dlglogerror)); en metodo onCreate");
                break;
        }

    }

    /**
     * Variable global para Di&#225;logo de excepci&#243;n</br>
     */
    protected static final int CONSDLGEXCEPTION = -1;
    /**
     * Variable global para Caso de error por defecto
     */
    public static final int CONSCASEERROR = -1;
    /**
     * Instancia del mensaje de la excepci&#243;n
     */
    protected String isMessageException = "";
    /**
     * Instancia de Caso de error
     *
     * @author jroeder
     */
    private int iiCaseError = 0;

    /**
     * M&#233;todo que retorna la instancia de caso de error
     *
     * @return int - Caso de error
     * @author jroeder
     */
    private int getCaseError() {
        return iiCaseError;
    }

    /**
     * M&#233;todo que coloca una instancia de caso de error
     *
     * @param piCaseError - Caso de error
     * @author jroeder
     */
    private void setCaseError(int piCaseError) {
        this.iiCaseError = piCaseError;
    }

    /**
     * Invocar al m&#233;todo {@link #onCreateDialog} de la clase superior, cuando se realice un <b>@Override</b>
     */
    @SuppressWarnings("deprecation")
    @Override
    protected Dialog onCreateDialog(int id) {
//		switch (id){
        EnumDialog loId = EnumDialog.get(id);
        if (loId != null) {
            switch (loId) {
                case EXCEPTION:
                    //	    	case CONSDLGEXCEPTION:
                    final AlertDialog loAlertDialogError = new AlertDialog.Builder(this).create();
                    loAlertDialogError.setCancelable(false);
                    loAlertDialogError.setButton(getString(R.string.dlg_btnok), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            loAlertDialogError.dismiss();
                            subMakeCaseError(getCaseError());
                        }
                    });
                    loAlertDialogError.setTitle(getString(R.string.dlg_titerror));
                    loAlertDialogError.setMessage(isMessageException);
                    loAlertDialogError.setIcon(android.R.drawable.ic_dialog_alert);
                    return loAlertDialogError;
                case PROGRESS:
                    final NexProgressDialog loProgressDialog = new NexProgressDialog(this);
                    loProgressDialog.setCancelable(false);
                    loProgressDialog.setMessage(ioPressDialogMessage);
                    if (isUsingDialogFragment()) {
                        loProgressDialog.setUsesNewLayout(true);
                    }

                    return loProgressDialog;
                default:
                    return super.onCreateDialog(id);
            }
        } else {
            return super.onCreateDialog(id);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onPrepareDialog(int id, Dialog dialog, Bundle args) {
        EnumDialog loId = EnumDialog.get(id);
        if (loId != null) {
            switch (loId) {
                case EXCEPTION:
                    AlertDialog loAlert = (AlertDialog) dialog;
                    loAlert.setMessage(isMessageException);
                    break;
                case PROGRESS:
                    NexProgressDialog loProgress = (NexProgressDialog) dialog;
                    loProgress.setMessage(ioPressDialogMessage);
                    break;
                default:
                    super.onPrepareDialog(id, dialog, args);
                    break;
            }
        } else {
            super.onPrepareDialog(id, dialog, args);
        }
    }

    /**
     * Es invocado cuando se cierra el di&#225;logo de excepciones
     *
     * @param piCaseError Caso de error
     */
    protected void subMakeCaseError(int piCaseError) {
        Log.v("PRO", this.getClass().getName() + "=>falta implementar 'protected void subMakeCaseError(int piCaseError){}'");
    }

    /**
     * Despliega el di&#225;logo de muestra de la excepci&#243;n
     *
     * @param poE Excepci&#243;n ocurrida
     */
    @SuppressWarnings("deprecation")
    public void subShowException(Exception poE) {
        Log.e("PRO", "subShowException", poE);
        if (Utilitario.hasMethodInStackTrace("onCreate")) {
            postException = new Pair<Exception, Integer>(poE, CONSCASEERROR);
            return;
        }
        if (Build.VERSION.SDK_INT >= 11 && isUsingDialogFragment()) {
            if (Utilitario.hasMethodInStackTrace("onBackPressed")) {
                Intent _intent = new Intent(this, NexDialogFragmentOnBackActivity.class);
                _intent.putExtra("subShowException", poE);
                _intent.putExtra("CONSCASEERROR", CONSCASEERROR);
                try {
                    startActivityForResult(_intent, REQ_DIALOGFRAGMENTONBACK);
                } catch (Exception e) {
                    Log.e("PRO", "startActivityForResult.NexDialogFragmentOnBack", e);
                }
                return;
            }
            DialogFragmentYesNo loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                    DialogFragmentYesNo.TAG.concat("Exception"),
                    getString(R.string.dlg_titerror),
                    "" + poE,
                    ioDialogFragmentDesign.getIconError(this),
                    false,
                    ioDialogFragmentDesign);
            if (loDialog.showDialog(getFragmentManager(), DialogFragmentYesNo.TAG.concat("Exception")) == DialogFragmentYesNo.YES) {
                subMakeCaseError(CONSCASEERROR);
            }
        } else {
            setCaseError(CONSCASEERROR);
            isMessageException = "" + poE;
            showDialog(EnumDialog.EXCEPTION.getValue());
        }
    }

    /**
     * Despliega el di&#225;logo de muestra de la excepci&#243;n
     *
     * @param poE         Excepci&#243;n ocurrida
     * @param piCaseError Caso de error
     */
    @SuppressWarnings("deprecation")
    public void subShowException(Exception poE, int piCaseError) {
        Log.e("PRO", "subShowException", poE);
        if (Utilitario.hasMethodInStackTrace("onCreate")) {
            postException = new Pair<Exception, Integer>(poE, piCaseError);
            return;
        }
        if (Build.VERSION.SDK_INT >= 11 && isUsingDialogFragment()) {
            if (Utilitario.hasMethodInStackTrace("onBackPressed")) {
                Intent _intent = new Intent(this, NexDialogFragmentOnBackActivity.class);
                _intent.putExtra("subShowException", poE);
                _intent.putExtra("CONSCASEERROR", piCaseError);
                try {
                    startActivityForResult(_intent, REQ_DIALOGFRAGMENTONBACK);
                } catch (Exception e) {
                    Log.e("PRO", "startActivityForResult.NexDialogFragmentOnBack", e);
                }
                return;
            }
            DialogFragmentYesNo loDialog = DialogFragmentYesNo.newInstance(getFragmentManager(),
                    DialogFragmentYesNo.TAG.concat("Exception"),
                    getString(R.string.dlg_titerror),
                    "" + poE,
                    ioDialogFragmentDesign.getIconError(this),
                    false,
                    ioDialogFragmentDesign);
            if (loDialog.showDialog(getFragmentManager(), DialogFragmentYesNo.TAG.concat("Exception")) == DialogFragmentYesNo.YES) {
                subMakeCaseError(piCaseError);
            }
        } else {
            setCaseError(piCaseError);
            isMessageException = "" + poE;
//			showDialog(CONSDLGEXCEPTION);
            showDialog(EnumDialog.EXCEPTION.getValue());
        }
    }

    /**
     * Objeto del tipo {@link HttpConexion}
     */
    private HttpConexion ioHttpConexion = null;
    /**
     * Secuencia de caracteres del Di&#225;logo de Progreso
     */
    private static CharSequence ioPressDialogMessage = null;
    /**
     * Variable global para Di&#225;logo de Progreso</br>
     * CONSPROGRESSDIALOG: {@value}
     */
    public static final int CONSPROGRESSDIALOG = -2;

    /**
     * Retorna el objeto de {@link HttpConexion} de la actividad
     *
     * @return {@link HttpConexion}
     */
    public HttpConexion getHttpConexion() {
        return ioHttpConexion;
    }

    /**
     * Instancia el objeto {@link HttpConexion} de la actividad
     *
     * @param poHttpConexion Objeto del tipo {@link HttpConexion}
     */
    public void setHttpConexion(HttpConexion poHttpConexion) {
        this.ioHttpConexion = poHttpConexion;
    }

    /**
     * Objeto del tipo {@link HttpActualizar}
     */
    private HttpActualizar ioHttpActualizar = null;

    /**
     * Retorna el objeto de {@link HttpActualizar} de la actividad
     *
     * @return {@link HttpActualizar}
     */
    public HttpActualizar getHttpActualizar() {
        return ioHttpActualizar;
    }

    /**
     * Instancia el objeto {@link HttpActualizar} de la actividad
     *
     * @param poHttpActualizar Objeto del tipo {@link HttpActualizar}
     */
    public void setHttpActualizar(HttpActualizar poHttpActualizar) {
        this.ioHttpActualizar = poHttpActualizar;
    }

    /**
     * Guarda alg&#250;n objeto al momento de entrar en pausa o al destruirse la actividad
     *
     * @return {Object}
     */
    public Object fnRetainNexNonConfigurationInstance() {
        return null;
    }

    /**
     * Guarda un objeto del tipo {@link StateHolder} al momento de entrar en pausa o al destruirse la actividad
     *
     * @return {@link StateHolder}
     */
    public StateHolder fnRetainNexStatHolderNonConfigInstance() {
        return null;
    }

    /**
     * Invoca y muestra un di&#225;logo de progreso
     *
     * @param poMessage Secuencia de caracteres con el mensaje a mostrar en el di&#225;logo de progreso
     */
    @SuppressWarnings("deprecation")
    public void showProgressDialog(CharSequence poMessage) {
        ioPressDialogMessage = poMessage;
//		showDialog(CONSPROGRESSDIALOG);
        showDialog(EnumDialog.PROGRESS.getValue());
    }

    /**
     * Retorna un valor booleano si la actividad est&#225; usando {@link DialogFragment} por defecto para mensajes
     *
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si usa {@link DialogFragment} por defecto</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public boolean isUsingDialogFragment() {
        return ibUsingDialogFragment;
    }

    /**
     * Instancia si la actividad est&#225; usando {@link DialogFragment} por defecto para mensajes
     *
     * @param pbUsingDialogFragment boolean
     *                              <ul>
     *                              <li><b>true</b> si usa {@link DialogFragment} por defecto</li>
     *                              <li><b>false</b> de lo contrario</li>
     *                              </ul>
     * @deprecated Sobreescribir el m&#233;todo {@link #isUsingDialogFragment}
     */
    public void setUsingDialogFragment(boolean pbUsingDialogFragment) {
        this.ibUsingDialogFragment = pbUsingDialogFragment;
        this.ioDialogFragmentDesign = EnumLayoutResource.obtainLayoutResource(this);
    }

    /**
     * Instancia si la actividad est&#225; usando {@link DialogFragment} por defecto para mensajes
     *
     * @param pbUsingDialogFragment  boolean </br>
     * @param poDialogFragmentDesign {@link EnumLayoutResource}
     *                               <ul>
     *                               <li><b>true</b> si usa {@link DialogFragment} por defecto</li>
     *                               <li><b>false</b> de lo contrario</li>
     *                               </ul>
     * @deprecated Sobreescribir el m&#233;todo {@link #isUsingDialogFragment}
     */
    public void setUsingDialogFragment(boolean pbUsingDialogFragment, EnumLayoutResource poDialogFragmentDesign) {
        this.ibUsingDialogFragment = pbUsingDialogFragment;
        if (poDialogFragmentDesign == null)
            this.ioDialogFragmentDesign = EnumLayoutResource.DESIGN01;
        this.ioDialogFragmentDesign = poDialogFragmentDesign;
    }

    /**
     * Instancia de Bandera para verificar si actividad usa {@link DialogFragment} por defecto para mensajes
     */
    private boolean ibUsingDialogFragment = false;

    private EnumLayoutResource ioDialogFragmentDesign = EnumLayoutResource.DESIGN01;

    /**
     * Instancia de {@link NexMenuCallbacks} para implementaci&#243;n de Men&#250; deslizable
     */
    private NexMenuCallbacks ioNexMenuCallbacks;

    /**
     * Inicializa el {@link NexMenuDrawerLayout}
     *
     * @param callbacks - Interfaz {@link NexMenuCallbacks}
     */
    protected void setUsingNexMenuDrawerLayout(NexMenuCallbacks callbacks) {
        this.ioNexMenuCallbacks = callbacks;
    }

    @Override
    public void setActionBarContentView(int resID) {
        if (ioNexMenuCallbacks == null) {
            super.setActionBarContentView(resID);
        } else {
            try {
                setActionBarMenuContentView(resID, ioNexMenuCallbacks);
            } catch (Exception e) {
                subShowException(e);
            }
        }
    }

    @Override
    public void setActionBarContentView(View poView) {
        if (ioNexMenuCallbacks == null) {
            super.setActionBarContentView(poView);
        } else {
            try {
                setActionBarMenuContentView(poView, ioNexMenuCallbacks);
            } catch (Exception e) {
                subShowException(e);
            }
        }
    }

    private void setActionBarMenuContentView(View poView, NexMenuCallbacks poCallbacks) throws Exception {
        super.setActionBarContentView(R.layout.nexmenudrawerlayout);
        ioNexMenuDrawerLayout = findViewById(R.id.nexmenudrawerlayout_root);
        ioNexMenuDrawerLayout.setNexMenuCallbacks(poView, poCallbacks);
        subCreateDrawerMenu();
    }


    /**
     * Instancia del Tipo {@link NexMenuDrawerLayout}
     */
    private NexMenuDrawerLayout ioNexMenuDrawerLayout;

    /**
     * Retorna la instancia del tipo {@link NexMenuDrawerLayout}
     *
     * @return NexMenuDrawerLayout - Instancia del tipo {@link NexMenuDrawerLayout}
     */
    public NexMenuDrawerLayout getNexMenuDrawerLayout() {
        return ioNexMenuDrawerLayout;
    }

    /**
     * Inicializa la actividad con un men&#250; deslizable
     *
     * @param resID     - Id de recurso Layout de la actividad
     * @param callbacks - Interfaz con m&#233;todos de inicializaci&#243;n de men&#250; y layout de la actividad
     */
    public void setActionBarMenuContentView(int resID, NexMenuCallbacks callbacks) throws Exception {
        super.setActionBarContentView(R.layout.nexmenudrawerlayout);
        ioNexMenuDrawerLayout = findViewById(R.id.nexmenudrawerlayout_root);
        ioNexMenuDrawerLayout.setNexMenuCallbacks(resID, callbacks);
        subCreateDrawerMenu();
    }

    private void subCreateDrawerMenu() {
        getActionBarGD().setType(Type.Empty);

        LinearLayout.LayoutParams loParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        loParams.gravity = Gravity.CENTER | Gravity.CENTER_VERTICAL;

        int px = getResources().getDimensionPixelSize(R.dimen.nexmenudrawerlayout_sizeindicator);
        LinearLayout.LayoutParams loParamsImage = new LinearLayout.LayoutParams(px, px);
        loParams.gravity = Gravity.CENTER | Gravity.CENTER_VERTICAL;

        ImageView loIndicator = new ImageView(this);
        loIndicator.setPadding(7, 0, 10, 0);
        loIndicator.setAdjustViewBounds(true);
        loIndicator.setImageResource(Utilitario.obtainResourceId(this, R.attr.NexMenuDrawerLayoutIndicatorSrc));
        loIndicator.setLayoutParams(loParamsImage);

        LinearLayout loLayout = new LinearLayout(this);
        loLayout.setLayoutParams(loParams);
        loLayout.setBackgroundResource(Utilitario.obtainResourceId(this, R.attr.NexMenuDrawerLayoutIndicatorBackground));
        loLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getNexMenuDrawerLayout().isDrawerVisible(Gravity.START)) {
                    getNexMenuDrawerLayout().closeDrawer(Gravity.START);
                } else {
                    getNexMenuDrawerLayout().openDrawer(Gravity.START);
                }
            }
        });
        loLayout.addView(loIndicator);

        getActionBarGD().addView(loLayout, 0);

        View loSeparator = new View(this);
        LinearLayout.LayoutParams loParams2 = new LinearLayout.LayoutParams(1, LinearLayout.LayoutParams.MATCH_PARENT);
        loParams2.gravity = Gravity.CENTER | Gravity.CENTER_VERTICAL;
        loSeparator.setLayoutParams(loParams2);
        loSeparator.setPadding(5, 0, 5, 0);
        loSeparator.setBackgroundColor(getResources().getColor(R.color.gd_action_bar_divider_tint));

        getActionBarGD().addView(loSeparator, 1);
    }

    /**
     * Muestra un di&#225;logo definido en el m&#233;todo {@link #onCreateNexDialog}.
     * </br>En caso no se haya activado la opci&#243;n de usar {@link DialogFragment}, el m&#233;todo invoca al {@link #showDialog}
     * </br>Se debe declarar en el <b>AndroidManifest.xml</b> la siguiente l&#237;nea:
     * </br>{@link NexDialogFragmentOnBackActivity &#60;activity android:name="pe.com.nextel.android.actividad.NexDialogFragmentOnBackActivity"/&#62;}
     *
     * @param id Identificador del di&#225;logo a mostrar.
     */
    @SuppressWarnings("deprecation")
    public final void showNexDialog(int id) {
        if (!isUsingDialogFragment()) {
            showDialog(id);
        } else {
            try {
                if (Utilitario.hasMethodInStackTrace("onBackPressed")) {
                    Intent _intent = new Intent(this, NexDialogFragmentOnBackActivity.class);
                    _intent.putExtra("DIALOG_ID", id);
                    try {
                        startActivityForResult(_intent, REQ_DIALOGFRAGMENTONBACK);
                    } catch (Exception e) {
                        Log.e("PRO", "startActivityForResult.NexDialogFragmentOnBack", e);
                    }
                    return;
                }
                DialogFragmentYesNoPairListener loDialogPair = onCreateNexDialog(id);
                if (loDialogPair.getDialogFragment().showDialog(getFragmentManager(), loDialogPair.getDialogFragment().getTagName()) == DialogFragmentYesNo.YES) {
                    if (loDialogPair.getOnClickListener() != null)
                        loDialogPair.getOnClickListener().performButtonYes(loDialogPair.getDialogFragment());
                } else {
                    if (loDialogPair.getOnClickListener() != null)
                        loDialogPair.getOnClickListener().performButtonNo(loDialogPair.getDialogFragment());
                }
            } catch (NullPointerException e) {
                Log.e("PRO", "No existe un di&#225logo creado para el id " + id, e);
            } catch (Exception e) {
                Log.e("PRO", "showNexDialog " + id, e);
            }
        }
    }

    /**
     * Crea un di&#225;logo {@link DialogFragmentYesNo} invocado con el m&#233;todo {@link #showNexDialog}.
     *
     * @param id Identidicador del di&#225;logo a mostrar
     * @return {@link DialogFragmentYesNoPairListener}
     */
    protected DialogFragmentYesNoPairListener onCreateNexDialog(int id) {
        Log.v("PRO", this.getClass().getName() + "=>falta implementar 'protected DialogFragmentYesNoPairListener onCreateNexDialog(int id){}'");
        return null;
    }

    private static final int Integer_MAX_VALUE = Integer.MAX_VALUE;

    /**
     * Invocar al m&#233;todo {@link #startActivityForResult} de la clase superior, cuando se realice un <b>@Override</b>
     */
    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        if (requestCode == REQ_DIALOGFRAGMENTONBACK
                && !(Utilitario.hasMethodInStackTrace("showNexDialog") || Utilitario.hasMethodInStackTrace("subShowException")))
            throw new IllegalArgumentException(getString(R.string.msg_errorrequestcodereservado) + "REQ_DIALOGFRAGMENTONBACK");
        if (requestCode == REQ_MAPS
                && !Utilitario.hasMethodInStackTrace("startMapsActivity"))
            throw new IllegalArgumentException(getString(R.string.msg_errorrequestcodereservado) + "REQ_MAPS");
        if (requestCode == REQ_NEXCAMERAACTIVITY
                && !Utilitario.hasMethodInStackTrace("subIniNexCameraActivity"))
            throw new IllegalArgumentException(getString(R.string.msg_errorrequestcodereservado) + "REQ_NEXCAMERAACTIVITY");
        super.startActivityForResult(intent, requestCode);
    }

    /**
     * Invocar al m&#233;todo {@link #startActivityForResult} de la clase superior, cuando se realice un <b>@Override</b>
     */
    @Override
    public void startActivityForResult(Intent intent, int requestCode,
                                       Bundle options) {
        if (requestCode == REQ_DIALOGFRAGMENTONBACK
                && !(Utilitario.hasMethodInStackTrace("showNexDialog") || Utilitario.hasMethodInStackTrace("subShowException")))
            throw new IllegalArgumentException(getString(R.string.msg_errorrequestcodereservado) + "REQ_DIALOGFRAGMENTONBACK");
        if (requestCode == REQ_MAPS
                && !Utilitario.hasMethodInStackTrace("startMapsActivity"))
            throw new IllegalArgumentException(getString(R.string.msg_errorrequestcodereservado) + "REQ_MAPS");
        if (requestCode == REQ_NEXCAMERAACTIVITY
                && !Utilitario.hasMethodInStackTrace("subIniNexCameraActivity"))
            throw new IllegalArgumentException(getString(R.string.msg_errorrequestcodereservado) + "REQ_NEXCAMERAACTIVITY");
        super.startActivityForResult(intent, requestCode, options);
    }

    /**
     * Valor global de  la petici&#243;n para invocar a la actividad {@link NexDialogFragmentOnBackActivity} en el {@link #startActivityForResult}
     */
    public static final int REQ_DIALOGFRAGMENTONBACK = Integer_MAX_VALUE;

    private OptionsMenuFragmentListener usingOptionsListener = null;

    /**
     * Activa el uso del men&#250; de opciones en un {@link Fragment}, creando un bot&#243;n de opciones (3 puntos verticales) en el {@link ActionBarGD}.
     * </br>Combina todos los &#237;tems del {@link ActionBarGD} y las opciones creadas en el {@link #onCreateOptionsMenu} en un s&#243;lo men&#250; de opciones.
     *
     * @param listener Interfaz con la implementaci&#243;n de los m&#233;todos de acci&#243;n de un &#237;tem del men&#250; de opciones
     */
    public void setUsingOptionMenu(OptionsMenuFragmentListener listener) {
        this.usingOptionsListener = listener;
    }

    /**
     * Verifica si se ha activado la opci&#243;n de men&#250; de opciones en un {@link Fragment}.
     *
     * @return boolean
     * <ul>
     * <li><b>true</b> si verifica que est&#225; activido</li>
     * <li><b>false</b> si verifica que est&#225; desactivido</li>
     * </ul>
     */
    public boolean isUsingOptionMenu() {
        return this.usingOptionsListener != null;
    }

    private List<ActionBarItem> mLstActionBar = null;

    /**
     * Retorna la lista de {@link ActionBarItem} del {@link ActionBarGD} creados en el men&#250; de opciones con {@link Fragment}
     *
     * @return {@link List}&#60;{@link ActionBarItem}&#62;
     */
    public List<ActionBarItem> getActionBarItemMenuOption() {
        return mLstActionBar;
    }

    private OptionsMenuFragment fragOpt;

    /**
     * Este m&#233;todo debe ser invocado en el {@link #subIniActionBar}
     */
    @Override
    public ActionBarItem addActionBarItem(
            greendroid.widget.ActionBarItem.Type actionBarItemType, int itemId) {
        if (!(Utilitario.hasMethodInStackTrace("subIniActionBar")
                || Utilitario.hasMethodInStackTrace("subCreateMenuOptionsActionBarItem")))
            return null;
        if (!isUsingOptionMenu()) {
            return super.addActionBarItem(actionBarItemType, itemId);
        } else {
            ActionBarItem item = ActionBarItem.createWithType(this.getActionBarGD(), actionBarItemType);
            item.setItemId(itemId);

            return addActionBarItem(item, itemId);
        }
    }

    /**
     * Este m&#233;todo debe ser invocado en el {@link #subIniActionBar}
     */
    @Override
    public ActionBarItem addActionBarItem(ActionBarItem item, int itemId) {
        if (!(Utilitario.hasMethodInStackTrace("subIniActionBar")
                || Utilitario.hasMethodInStackTrace("subCreateMenuOptionsActionBarItem")))
            return null;
        if (!isUsingOptionMenu()) {
            return super.addActionBarItem(item, itemId);
        } else {
            if (mLstActionBar == null)
                mLstActionBar = new ArrayList<ActionBarItem>();

            if (!mLstActionBar.contains(item))
                mLstActionBar.add(item);

            Log.v("PRO", "item: " + item.getContentDescription().toString() + " - " + mLstActionBar.contains(item));

            if (mLstActionBar.size() == 1) {
                super.addActionBarItem(item, itemId);
            } else {
                getActionBarGD().removeItem(0);
                super.addActionBarItem(greendroid.widget.ActionBarItem.Type.Nex_OptionsMenu, 0);
            }

            return item;
        }
    }

//	Agrega un {@link ActionBarItem} al men&#250; de opciones con {@link Fragment}

    /**
     * Agrega un {@link ActionBarItem} al {@link ActionBarGD}
     * </br>Este m&#233;todo debe ser invocado en el {@link #subIniActionBar}
     *
     * @param itemId   Identificador del &#237;tem
     * @param drawable {@link Drawable} a mostrar
     * @param title    T&#237;tulo o descripci&#243;n a mostrar
     * @return {@link ActionBarItem}
     */
//	@throws MethodNotSupportedException En caso no se ha haya activado la opci&#243;n de usar men&#250; de opciones con {@link Fragment}*/
    public ActionBarItem addActionBarItem(int itemId, Drawable drawable, String title) /*throws MethodNotSupportedException*/ {
        if (!(Utilitario.hasMethodInStackTrace("subIniActionBar")
                || Utilitario.hasMethodInStackTrace("subCreateMenuOptionsActionBarItem")))
            return null;
//		if(!isUsingOptionMenu()){
//			throw new MethodNotSupportedException(getString(R.string.msg_errormenuoptionmetodonosoportado));
//		}else{
        NormalActionBarItem _item = new NormalActionBarItem();
        _item.setActionBar(getActionBarGD());
        _item.setDrawable(drawable);
        _item.setContentDescription(title);
        _item.setItemId(itemId);
        return addActionBarItem(_item, itemId);
//		}
    }

    /**
     * Agrega un {@link ActionBarItem} al {@link ActionBarGD}
     * </br>Este m&#233;todo debe ser invocado en el {@link #subIniActionBar}
     *
     * @param itemId      Identificador del &#237;tem
     * @param resDrawable id de recurso de {@link Drawable} a mostrar
     * @param title       T&#237;tulo o descripci&#243;n a mostrar
     * @return {@link ActionBarItem}
     */
    public ActionBarItem addActionBarItem(int itemId, int resDrawable, String title) {
        return addActionBarItem(itemId, getResources().getDrawable(resDrawable), title);
    }

    /**
     * Este m&#233;todo debe ser invocado en el {@link #subIniActionBar}
     */
    @Override
    public ActionBarItem addActionBarItem(ActionBarItem item) {
        if (!(Utilitario.hasMethodInStackTrace("subIniActionBar")
                || Utilitario.hasMethodInStackTrace("subCreateMenuOptionsActionBarItem")))
            return null;
        if (!isUsingOptionMenu()) {
            return super.addActionBarItem(item);
        } else {
            return addActionBarItem(item, ActionBarGD.NONE);
        }
    }

    /**
     * Este m&#233;todo debe ser invocado en el {@link #subIniActionBar}
     */
    @Override
    public ActionBarItem addActionBarItem(
            greendroid.widget.ActionBarItem.Type actionBarItemType) {
        if (!(Utilitario.hasMethodInStackTrace("subIniActionBar")
                || Utilitario.hasMethodInStackTrace("subCreateMenuOptionsActionBarItem")))
            return null;
        if (!isUsingOptionMenu()) {
            return super.addActionBarItem(actionBarItemType);
        } else {
            return addActionBarItem(actionBarItemType, ActionBarGD.NONE);
        }
    }

    private void showMenuOptions(boolean hasElements) {
        try {
            if (fragOpt == null && hasElements) {
                fragOpt = OptionsMenuFragment.newInstance(getFragmentManager(), OptionsMenuFragment.TAG, usingOptionsListener, mLstActionBar);
                fragOpt.show(getFragmentManager(), OptionsMenuFragment.TAG);
            } else if (!fragOpt.isMenuOpened() && hasElements) {
                fragOpt = OptionsMenuFragment.newInstance(getFragmentManager(), OptionsMenuFragment.TAG, usingOptionsListener, mLstActionBar);
                fragOpt.show(getFragmentManager(), OptionsMenuFragment.TAG);
            } else {
                fragOpt.dismiss();
            }
        } catch (Exception e) {
            Log.e("PRO", "NexActivity.showMenuOptions", e);
        }
    }

    /**
     * Invocar al m&#233;todo {@link #onMenuOpened} de la clase superior, cuando se realice un <b>@Override</b>
     */
    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        if (getActionBarGD().getVisibility() == View.VISIBLE && isUsingOptionMenu()) {
            subCreateMenuOptionsActionBarItem(menu);
            showMenuOptions(getActionBarItemMenuOption().size() > 0);
            super.closeOptionsMenu();
            return false;
        }
        return super.onMenuOpened(featureId, menu);
    }

    private void resetMenuOptionsActionBar() {
        if (mLstActionBar != null)
            mLstActionBar.clear();
        mLstActionBar = null;
        do {
            getActionBarGD().removeItem(0);
        } while (getActionBarGD().getItems().size() > 0);
        subIniActionBar();
    }

    private void subCreateMenuOptionsActionBarItem(Menu menu) {
        try {
            resetMenuOptionsActionBar();
            if (menu.size() <= 0)
                return;
            do {
                MenuItem _item = menu.getItem(0);
                ActionBarItem _actionBarItem = ActionBarItem.createWithType(getActionBarGD(), ActionBarItem.Type.Nex_OptionsMenu);
                _actionBarItem.setDrawable(_item.getIcon());
                _actionBarItem.setContentDescription(_item.getTitle());
                addActionBarItem(_actionBarItem, _item.getItemId());
                menu.removeItem(0);
            } while (menu.size() > 0);
        } catch (Exception e) {
            Log.e("PRO", "NexActivity.subCreateMenuOptionsActionBarItem", e);
        }
    }

    /**
     * Invocar al m&#233;todo {@link #onHandleActionBarItemClick} de la clase superior, cuando se realice un <b>@Override</b>
     */
    @Override
    public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
        if (isUsingOptionMenu()) {
            if (item.getContentDescription().equals(getString(R.string.gd_nex_menu_options))) {
                showMenuOptions(getActionBarItemMenuOption().size() > 0);
                return true;
            }
        }
        return super.onHandleActionBarItemClick(item, position);
    }

    private boolean usingContextListener = false;

    /**
     * Activa el uso del men&#250; de contexto en un {@link Fragment}.
     *
     * @param using <ul>
     *              <li><b>true</b> activa el uso</li>
     *              <li><b>false</b> desactivido el uso</li>
     *              </ul>
     */
    public void setUsingContextFragment(boolean using) {
        this.usingContextListener = using;
    }

    /**
     * Verifica si se ha activado la opci&#243;n de men&#250; de contexto en un {@link Fragment}.
     *
     * @return boolean
     * <ul>
     * <li><b>true</b> si verifica que est&#225; activido</li>
     * <li><b>false</b> si verifica que est&#225; desactivido</li>
     * </ul>
     */
    public boolean isUsingContextFragment() {
        return usingContextListener;
    }

    private ContextMenuFragment fragCtx = null;

    private void subCreateMenuContextFragment(ContextMenu menu) {
        try {
            if (menu.size() <= 0)
                return;
            fragCtx = ContextMenuFragment.newInstance(getFragmentManager(), ContextMenuFragment.TAG, menu);
        } catch (Exception e) {
            fragCtx = null;
            Log.e("PRO", "NexActivity.subCreateMenuOptionsActionBarItem", e);
        }
    }

    /**
     * Invocar al m&#233;todo {@link #registerForContextMenu} de la clase superior, cuando se realice un <b>@Override</b>
     */
    @Override
    public void registerForContextMenu(View view) {
        if (!isUsingContextFragment()) {
            super.registerForContextMenu(view);
            return;
        }

        if (view instanceof AdapterView) {
            ((AdapterView<?>) view).setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                @Override
                public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                    try {
                        return onLongClickForContextMenu(adapterView, view, position, id);
                    } catch (Exception e) {
                        Log.e("PRO", "NexActivity.registerForContextMenu.AdapterView.onItemLongClick", e);
                        return false;
                    }
                }
            });
        } else if (view instanceof ExpandableListView) {
            ((ExpandableListView) view).setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                @Override
                public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                    try {
                        return onLongClickForContextMenu(adapterView, view, position, id);
                    } catch (Exception e) {
                        Log.e("PRO", "NexActivity.registerForContextMenu.ExpandableListView.onItemLongClick", e);
                        return false;
                    }
                }
            });
        } else {
            view.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    try {
                        return onLongClickForContextMenu(null, v, -1, v.getId());
                    } catch (Exception e) {
                        Log.e("PRO", "NexActivity.registerForContextMenu.onLongClick", e);
                        return false;
                    }
                }
            });
        }
    }

    private boolean onLongClickForContextMenu(AdapterView<?> adapterview, View view, int position, long id) {
        AdapterView.AdapterContextMenuInfo _menuInfo = new AdapterView.AdapterContextMenuInfo(view, position, id);
        ContextMenu _menu = new ContextMenuBuilder(NexActivity.this);
        ((MenuBuilder) _menu).setCurrentMenuInfo(_menuInfo);
        onCreateContextMenu(_menu, view, _menuInfo);
        subCreateMenuContextFragment(_menu);
        showContextMenu();
        return true;
    }

    private void showContextMenu() {
        if (fragCtx != null) {
            fragCtx.show(getFragmentManager(), ContextMenuFragment.TAG);
        }
    }

    /**
     * Invocar al m&#233;todo {@link #openContextMenu} de la clase superior, cuando se realice un <b>@Override</b>
     */
    @Override
    public void openContextMenu(View view) {
        if (!isUsingContextFragment()) {
            super.openContextMenu(view);
            return;
        }
        view.performLongClick();
    }

    /**
     * Invocar al m&#233;todo {@link #openContextMenu} de la clase superior, cuando se realice un <b>@Override</b>
     */
    @Override
    public void unregisterForContextMenu(View view) {
        if (!isUsingContextFragment()) {
            super.unregisterForContextMenu(view);
            return;
        }

        if (view instanceof AdapterView) {
            ((AdapterView<?>) view).setOnItemLongClickListener(null);
        } else if (view instanceof ExpandableListView) {
            ((ExpandableListView) view).setOnItemLongClickListener(null);
        } else {
            view.setOnLongClickListener(null);
        }
    }

    /**
     * Valor global de  la petici&#243;n para invocar a la actividad {@link NexCameraActivity} en el {@link #startActivityForResult}
     */
    public static final int REQ_NEXCAMERAACTIVITY = Integer_MAX_VALUE - 1;

    private BeanPhoto _beanPhoto = null;

    /**
     * Se invoca cuando se retorna de la actividad {@link NexCameraActivity}
     *
     * @param bean      {@link BeanPhoto} de la foto tomada
     * @param savedPath Ruta f&#237;sica de la foto almacenada
     */
    protected void onNexCameraActivityResult(BeanPhoto bean, String savedPath) {
        Log.v("PRO", this.getClass().getName() + "=>falta implementar 'protected void onNexCameraActivityResult(BeanPhoto bean, String savedPath){}'");
    }

    /**
     * Se invoca cuando se cancela la toma de foto
     *
     * @param bean {@link BeanPhoto} de la foto cancelada
     */
    protected void onNexCameraActivityCancel(BeanPhoto bean) {
        Log.v("PRO", this.getClass().getName() + "=>falta implementar 'protected void onNexCameraActivityCancel(BeanPhoto bean){}'");
    }

    /**
     * Valor global de  la petici&#243;n para invocar a una actividad de Visualizaci&#243;n de Mapa en el {@link #startActivityForResult}
     */
    public static final int REQ_MAPS = Integer_MAX_VALUE - 2;

    /**
     * Es invocado en el m&#233;todo {@link #onActivityResult} como resultado de la petici&#243;n con el c&#243;digo {@link #REQ_MAPS}
     *
     * @param resultCode C&#243;digo de resultado
     * @param data       {@link Intent} con la informaci&#243;n de resultado
     */
    protected void onNexMapsActivityResult(int resultCode, Intent data) {
        Log.v("PRO", this.getClass().getName() + "=>falta implementar 'protected void onNexMapsActivityResult(int resultCode, Intent data){}'");
    }

    private static final String SAVEINSTANCE_PROCESS_LOCKED = "processLocked";
    private static final String SAVEINSTANCE_MESSAGE_OK = "save_msgok";
    private static final String SAVEINSTANCE_MESSAGE_ERROR = "save_msgerror";
    private boolean processLocked = false;

    /**
     * Verifica si se ha bloqueado un proceso por alg&#250;n evento realizado por el usuario.</br>
     * El programador es quien verifica en qu&#233; evento es necesario bloquear.
     *
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si verifica que est&#225; bloqueado</li>
     * <li><b>false</b> si verifica que no est&#225; bloqueado</li>
     * </ul>
     */
    public boolean isProcessLocked() {
        return processLocked;
    }

    /**
     * Bloquea el proceso.</br>
     * El programador es quien realiza el bloqueo.
     */
    public void lockProcess() {
        processLocked = true;
    }

    /**
     * Desbloquea el proceso.</br>
     * El programador es quien realiza el desbloqueo.
     */
    public void unlockProcess() {
        processLocked = false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(SAVEINSTANCE_PROCESS_LOCKED, processLocked);
        outState.putString(SAVEINSTANCE_MESSAGE_OK, getMsgOk());
        outState.putString(SAVEINSTANCE_MESSAGE_ERROR, getMsgError());
        subSaveNexInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        processLocked = savedInstanceState.getBoolean(SAVEINSTANCE_PROCESS_LOCKED);
        setMsgOk(savedInstanceState.getString(SAVEINSTANCE_MESSAGE_OK));
        setMsgError(savedInstanceState.getString(SAVEINSTANCE_MESSAGE_ERROR));
        subRestoreNexInstanceState(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }

    /**
     * Invoca el m&#233;todo {@link #subShowException} en el m&#233;todo {@link #runOnUiThread} de la actividad
     *
     * @param e Excepci&#243;n ocurrida
     */
    public void subShowExptionOnRunUI(final Exception e) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                subShowException(e);
            }
        });
    }

    /**
     * Invoca el m&#233;todo {@link #subShowException} en el m&#233;todo {@link #runOnUiThread} de la actividad
     *
     * @param e         Excepci&#243;n ocurrida
     * @param caseError Caso de error
     */
    public void subShowExptionOnRunUI(final Exception e, final int caseError) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                subShowException(e, caseError);
            }
        });
    }

    private Pair<Exception, Integer> postException = null;

    /**
     * Invocar al m&#233;todo {@link #onPostResume} de la clase superior, cuando se realice un <b>@Override</b>
     */
    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (postException != null) {
            subShowException(postException.first, postException.second);
            postException = null;
        }
    }

    private IntentResultCallback _resultScanCallback = null;

    @Override
    public boolean registerForScan(IntentResultCallback callback) {
        _resultScanCallback = callback;
        return true;
    }

    /**
     * Invocar al m&#233;todo {@link #onActivityResult} de la clase superior, cuando se realice un <b>@Override</b>
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQ_DIALOGFRAGMENTONBACK) {
            if (data.getExtras().containsKey("subShowException")) {
                data.getSerializableExtra("subShowException");
                subShowException((Exception) data.getSerializableExtra("subShowException"), data.getIntExtra("CONSCASEERROR", CONSCASEERROR));
                return;
            }
            showNexDialog(data.getIntExtra("DIALOG_ID", -1));
        } else if (requestCode == REQ_NEXCAMERAACTIVITY && resultCode == RESULT_OK) {
            Bundle _result = data.getBundleExtra(NexCameraActivity.BUNDLE_RESULT);
            onNexCameraActivityResult(_beanPhoto, _result.getString(NexCameraActivity.URI_SAVED));
            _beanPhoto = null;
        } else if (requestCode == REQ_NEXCAMERAACTIVITY) {
            onNexCameraActivityCancel(_beanPhoto);
            _beanPhoto = null;
        } else if (requestCode == REQ_MAPS) {
            onNexMapsActivityResult(resultCode, data);
        } else if (requestCode == IntentIntegrator.REQUEST_CODE) {
            if (_resultScanCallback != null)
                _resultScanCallback.setScanResult(IntentIntegrator.parseActivityResult(this, requestCode, resultCode, data));
            _resultScanCallback = null;
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public Object onRetainNonConfigurationInstance() {
        if (getHttpActualizar() != null) {
            getHttpActualizar().detach();
            return getHttpActualizar();
        }
        if (getHttpConexion() != null) {
            getHttpConexion().detach();
            return getHttpConexion();
        }
        if (_resultScanCallback != null) {
            return _resultScanCallback;
        }
        if (fnRetainNexStatHolderNonConfigInstance() != null) {
            fnRetainNexStatHolderNonConfigInstance().dettach(this);
            return fnRetainNexStatHolderNonConfigInstance();
        }
        return fnRetainNexNonConfigurationInstance();
    }
}
