package pe.com.nextel.android.widget;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.text.Editable;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.util.HashMap;
import java.util.Map;

import pe.com.nextel.android.R;
import pe.com.nextel.android.util.Utilitario;

public class NexKeyboardView extends KeyboardView {
    private boolean shownAlways = true;
    private boolean querty = false;

//	private OnPerformReturn listenerReturn;

    public NexKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NexKeyboardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void registerKeyboard(int resid) {
        registerKeyboard(resid, initializeActionListenerDefault());
    }

    public void registerKeyboard(int resid, OnKeyboardActionListener listener) {
        if (getKeyboard() == null) {
            registerKeyboard(new Keyboard(getContext(), resid), listener);
        } else {
            throw new IllegalStateException(getContext().getString(R.string.NexKeyboardView_RegKeyException));
        }
    }

    public void registerKeyboard(Keyboard keyboard) {
        registerKeyboard(keyboard, initializeActionListenerDefault());
    }

    public void registerKeyboard(Keyboard keyboard, OnKeyboardActionListener listener) {
        if (getKeyboard() == null) {
            setKeyboard(keyboard);
            setPreviewEnabled(false);
            setOnKeyboardActionListener(listener);
            ((Activity) getContext()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } else {
            throw new IllegalStateException(getContext().getString(R.string.NexKeyboardView_RegKeyException));
        }
    }

    private void initializeKeyboard() {
        if (getKeyboard() == null) {
            int resid = Utilitario.obtainResourceId(getContext(), R.attr.NexKeyboardView_Keyboard);
            if (resid == R.xml.nexqwerty
                    || resid == R.xml.nexqwerty_blue
                    || resid == R.xml.nexqwerty_orange) {
                querty = true;
            }
            setTag(R.string.NexKeyboardView_TagQuerty, querty);
            registerKeyboard(resid);
        }
    }

    public void registerEditText(NexEditText editText, OnPerformReturn listenerReturn) {
        editText.setTag(R.string.NexKeyboardView_TagPerformReturn, listenerReturn);
        registerEditText(editText);
    }

    public void registerEditText(int resid, OnPerformReturn listenerReturn) {
        registerEditText((NexEditText) ((Activity) getContext()).findViewById(resid), listenerReturn);
    }

    public void registerEditText(NexEditText editText) {
        initializeKeyboard();
        final OnFocusChangeListener _focusListener = editText.getOnFocusChangeListener();
        editText.setOnFocusChangeListener(new OnFocusChangeListener() {
            // NOTE By setting the on focus listener, we can show the custom keyboard when the edit box gets focus, but also hide it when the edit box loses focus
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    showCustomKeyboard(v);
                    isComposing = false;
                    if (_focusListener != null)
                        _focusListener.onFocusChange(v, hasFocus);
                } else {
                    hideCustomKeyboard();
                }
            }
        });
        final OnClickListener _clickListener = Utilitario.getOnClickListener(editText);
        editText.setOnClickListener(new OnClickListener() {
            // NOTE By setting the on click listener, we can show the custom keyboard again, by tapping on an edit box that already had focus (but that had the keyboard hidden).
            @Override
            public void onClick(View v) {
                showCustomKeyboard(v);
                if (_clickListener != null) {
                    _clickListener.onClick(v);
                }
            }
        });
        editText.setCursorVisible(true);
    }

    public void registerEditText(int resid) {
        registerEditText((NexEditText) ((Activity) getContext()).findViewById(resid));
    }

    public void hideCustomKeyboard() {
        if (!isShownAlways()) {
            setVisibility(View.GONE);
            setEnabled(false);
        }
    }

    public void showCustomKeyboard(View v) {
        setVisibility(View.VISIBLE);
        setEnabled(true);
        if (v != null)
            ((InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public boolean isCustomKeyboardVisible() {
        return getVisibility() == View.VISIBLE;
    }

    private OnKeyboardActionListener initializeActionListenerDefault() {
        return new OnKeyboardActionListener() {

            public final static int CodeDelete = -5; // Keyboard.KEYCODE_DELETE
            public final static int CodeReturn = 10; // Keyboard.KEYCODE_RETURN

            @Override
            public void onKey(int primaryCode, int[] keyCodes) {
                try {
                    // NOTE We can say '<Key android:codes="49,50" ... >' in the xml file; all codes come in keyCodes, the first in this list in primaryCode
                    // Get the EditText and its Editable
                    View focusCurrent = ((Activity) getContext()).getWindow().getCurrentFocus();
                    if (focusCurrent == null || focusCurrent.getClass() != NexEditText.class)
                        return;
                    NexEditText edittext = (NexEditText) focusCurrent;
                    int inputType = edittext.getInputType();

                    Editable editable = edittext.getText();
                    int start = edittext.getSelectionStart();
                    // Apply the key to the edittext
                    if (primaryCode == CodeDelete) {
                        if (editable != null && start > 0) {
                            editable.delete(start - 1, start);
                            lastIndx = 0;
                            lastCodeKey = -123;
                        }
                    } else if (primaryCode == CodeReturn) {
                        @SuppressLint("WrongConstant") View focusNew = edittext.focusSearch(View.FOCUS_FORWARD);
                        OnPerformReturn listenerReturn = (OnPerformReturn) edittext.getTag(R.string.NexKeyboardView_TagPerformReturn);
                        if (listenerReturn != null) {
                            listenerReturn.onPerformReturn(edittext);
                        } else if (focusNew != null) {
                            focusNew.requestFocus();
                        }
                    } else { // insert character
//		            	Log.d("PRO", "Character: " + Character.toString((char) primaryCode).toUpperCase());
//		            	editable.insert(start, Character.toString((char) primaryCode).toUpperCase());
                        composeChar(editable, inputType, start, primaryCode);
                    }
                } catch (ClassCastException e) {
                    Log.i("PRO", "Vista no es un edittext");
                }
            }

            @Override
            public void onPress(int primaryCode) {
            }

            @Override
            public void onRelease(int primaryCode) {
            }

            @Override
            public void onText(CharSequence text) {
            }

            @Override
            public void swipeDown() {
            }

            @Override
            public void swipeLeft() {
            }

            @Override
            public void swipeRight() {
            }

            @Override
            public void swipeUp() {
            }
        };
    }

    private boolean isComposing = false;
    private long thisTime;
    private long lastTime;
    private int lastCodeKey = -123;
    private int lastIndx = 0;
    private static long composingInterval = 1400;
    private static final Map<String, char[]> MAPCHARACTER = new HashMap<String, char[]>();

    static {
        MAPCHARACTER.put("2", new char[]{'2', 'A', 'B', 'C'});
        MAPCHARACTER.put("3", new char[]{'3', 'D', 'E', 'F'});
        MAPCHARACTER.put("4", new char[]{'4', 'G', 'H', 'I'});
        MAPCHARACTER.put("5", new char[]{'5', 'J', 'K', 'L'});
        MAPCHARACTER.put("6", new char[]{'6', 'M', 'N', 'O'});
        MAPCHARACTER.put("7", new char[]{'7', 'P', 'Q', 'R', 'S'});
        MAPCHARACTER.put("8", new char[]{'8', 'T', 'U', 'V'});
        MAPCHARACTER.put("9", new char[]{'9', 'W', 'X', 'Y', 'Z'});
        MAPCHARACTER.put(".", new char[]{'.', ',', ':', ';', '+', '-', '*', '/', '\\', '=',
                '\'', '"', '#', '$', '%', '&', '_', '|', '¿', '?',

                '¡', '!', '(', ')', '{', '}', '[', ']', '<', '>'});

    }


    private boolean isInputTypeNumeric(int inputType) {
        return ((inputType & InputType.TYPE_CLASS_NUMBER) > 0)
                || ((inputType & InputType.TYPE_NUMBER_FLAG_DECIMAL) > 0)
                || ((inputType & InputType.TYPE_NUMBER_FLAG_SIGNED) > 0)
                || ((inputType & InputType.TYPE_NUMBER_VARIATION_NORMAL) > 0)
                || ((inputType & InputType.TYPE_NUMBER_VARIATION_PASSWORD) > 0);
    }

    private void composeChar(Editable editable, int inputType, int start, int codeKey) {
        if (isQuerty()
                || (char) codeKey == '1'
                || (char) codeKey == '0'
                || isInputTypeNumeric(inputType)) {
            editable.insert(start, Character.toString((char) codeKey).toUpperCase());
            return;
        }

        lastTime = thisTime;
        thisTime = System.currentTimeMillis();
        if (lastCodeKey == codeKey
                && ((thisTime - lastTime) <= composingInterval)
                && isComposing) {
            String character = Character.toString((char) codeKey);
            char[] characters = MAPCHARACTER.get(character);
            if (characters != null) {
                if (lastIndx >= characters.length)
                    lastIndx = 0;
                char realKey = characters[lastIndx++];
                editable.replace(start - 1, start, Character.toString(realKey));
                return;
            } else {
                lastIndx = 0;
            }
        } else {
            lastIndx = 0;
        }
        lastCodeKey = codeKey;
        lastIndx++;
        isComposing = true;
        editable.insert(start, Character.toString((char) codeKey).toUpperCase());
    }

//	public void setOnPerformReturn(OnPerformReturn listenerReturn) {
//		this.listenerReturn = listenerReturn;
//	}

    public boolean isShownAlways() {
        return shownAlways;
    }

    public void setShownAlways(boolean shownAlways) {
        this.shownAlways = shownAlways;
        if (isShownAlways())
            showCustomKeyboard(null);
    }

    public boolean isQuerty() {
        if (getTag(R.string.NexKeyboardView_TagQuerty) == null)
            return false;
        this.querty = ((Boolean) getTag(R.string.NexKeyboardView_TagQuerty)).booleanValue();
        return querty;
    }

    public static interface OnPerformReturn {
        public void onPerformReturn(NexEditText editText);
    }
}
