package pe.com.nextel.android.http;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.Html;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import pe.com.nextel.android.R;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.actividad.NexListActivityBasic;
import pe.com.nextel.android.actividad.NexListActivityTweaked;
import pe.com.nextel.android.actividad.NexPreferenceActivity;
import pe.com.nextel.android.actividad.support.NexFragmentActivity;
import pe.com.nextel.android.actividad.support.NexFragmentListActivityBasic;
import pe.com.nextel.android.actividad.support.NexFragmentListActivityTweaked;
import pe.com.nextel.android.dal.BDFramework;
import pe.com.nextel.android.util.ConfiguracionNextel;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumDialog;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.Utilitario;

/**
 * Tarea As&#233;ncrona que realiza una conexi&#243;n HTTP
 *
 * @author jroeder
 */
@SuppressWarnings("deprecation")
public abstract class HttpConexion extends AsyncTask<Void, Void, String> {
    /**
     * Constante de Time Out de conexi&#243;n
     */
    public final static int TIEMPOTIMEOUT = 120000;
    /**
     * Contexto de la aplicaci&#243;n
     */
    protected Context ioContext;
    /**
     * Di&#225;logo de Progreso
     */
    protected ProgressDialog ioProgressDialog;
    /**
     * Mensaje del Di&#225;logo de Progreso
     */
    protected String isMensaje;
    /**
     * Tipo de actividad
     */
    protected EnumTipActividad ioEnumTipActividad;

    private String isHttpResponseMessage;
    private int iiHttpResponseId;
    private boolean ibMakeConnection = false;
    private Object ioHttpResponseObject = null;
    private boolean ibShowProgress = false;
    private String URL;

    /**
     * Constructor de la clase
     *
     * @param poContext          - Contexto de la aplicaci&#243;n
     * @param psMensaje          - Mensaje a mostrar en el di&#225;logo de progreso
     * @param poEnumTipActividad - Tipo de actividad pre-definida
     */
    public HttpConexion(Context poContext, String psMensaje, EnumTipActividad poEnumTipActividad) {
        this(poContext, psMensaje, poEnumTipActividad, true);
    }

    /**
     * Constructor de la clase.</br>Por defecto obtiene el tipo de actividad seg&#250;n el contexto del argumento. En caso no pertenezca a
     * las actividades pre-definidas, se tiene que es una actividad del tipo {@link EnumTipActividad#NEXCUSTOMACTIVITY NEXCUSTOMACTIVITY}
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @param psMensaje - Mensaje a mostrar en el di&#225;logo de progreso
     */
    public HttpConexion(Context poContext, String psMensaje) {
        this(poContext, psMensaje, true);
    }

    /**
     * Constructor de la clase
     *
     * @param poContext          - Contexto de la aplicaci&#243;n
     * @param psMensaje          - Mensaje a mostrar en el di&#225;logo de progreso
     * @param poEnumTipActividad - Tipo de actividad pre-definida
     * @param showProgress       - Indica si el objeto muestra el di&#225;logo de progreso
     */
    public HttpConexion(Context poContext, String psMensaje, EnumTipActividad poEnumTipActividad, boolean showProgress) {
        attach(poContext);
        isMensaje = psMensaje;
        ioEnumTipActividad = poEnumTipActividad != null ? poEnumTipActividad : EnumTipActividad.NEXCUSTOMACTIVITY;
        setHttpResponseMessage("");
//		setHttpResponseId(ConfiguracionNextel.CONSRESSERVIDOROKNOMSG);
        setHttpResponseId(ConfiguracionNextel.EnumServerResponse.OKNOMSG.getValue());
        ibShowProgress = showProgress;
    }

    /**
     * Constructor de la clase.</br>Por defecto obtiene el tipo de actividad seg&#250;n el contexto del argumento. En caso no pertenezca a
     * las actividades pre-definidas, se tiene que es una actividad del tipo {@link EnumTipActividad#NEXCUSTOMACTIVITY NEXCUSTOMACTIVITY}
     *
     * @param poContext    - Contexto de la aplicaci&#243;n
     * @param psMensaje    - Mensaje a mostrar en el di&#225;logo de progreso
     * @param showProgress - Indica si el objeto muestra el di&#225;logo de progreso
     */
    public HttpConexion(Context poContext, String psMensaje, boolean showProgress) {
        attach(poContext);
        isMensaje = psMensaje;
        ioEnumTipActividad = EnumTipActividad.get(poContext);
        setHttpResponseMessage("");
//		setHttpResponseId(ConfiguracionNextel.CONSRESSERVIDOROKNOMSG);
        setHttpResponseId(ConfiguracionNextel.EnumServerResponse.OKNOMSG.getValue());
        ibShowProgress = showProgress;
    }

    /**
     * Adjunta el nuevo contexto de la aplicaci&#243;n
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     */
    public void attach(Context poContext) {
        ioContext = poContext;
    }

    /**
     * Desajunta el contexto de la aplicaci&#243;n del objeto actual
     */
    public void detach() {
        ioContext = null;
    }

    /**
     * Retorn el contexto de la aplicaci&#243;n
     *
     * @return Context
     */
    public Context getContext() {
        return ioContext;
    }

    /**
     * Retorna el objeto de respuesta
     *
     * @return Object
     */
    public Object getHttpResponseObject() {
        return ioHttpResponseObject;
    }

    /**
     * Instancia el objeto de respuesta
     *
     * @param poHttpResponseObject - Objeto de respuesta
     */
    public void setHttpResponseObject(Object poHttpResponseObject) {
        this.ioHttpResponseObject = poHttpResponseObject;
    }

    /**
     * M&#233;todo utilizado para realizar un proceso previo a la conexi&#243;n.</br>
     * Este m&#233;todo es invocado en el {@link #onPreExecute} del {@link AsyncTask}.</br>
     *
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si realiza conexi&#243;n</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public abstract boolean OnPreConnect();

    /**
     * M&#233;todo utilizado para realizar un proceso durante la conexi&#243;n.</br>
     * Este m&#233;todo es invocado en el {@link #doInBackground} del {@link AsyncTask}.</br>
     * Como buena pr&#225;ctica, no hay que invocar o instanciar objetos
     * utilizando el contexto o la actividad desde &#233;ste. Puede causar errores y
     * posibles cierres forzados de la aplicaci&#243;n.
     */
    public abstract void OnConnect();

    /**
     * M&#233;todo utilizado para realizar un proceso posterior a la conexi&#243;n.</br>
     * Este m&#233;todo es invocado en el {@link #onPostExecute} del {@link AsyncTask}.</br>
     */
    public abstract void OnPostConnect();

    @Override
    protected void onPreExecute() {
        try {
            switch (ioEnumTipActividad) {
                case NEXACTIVITY:
                    ((NexActivity) getContext()).setHttpConexion(this);
                    if (ibShowProgress) {
                        ((NexActivity) getContext()).showProgressDialog(Html.fromHtml("<font color='white'>" + isMensaje + "</font>"));
                    }
                    break;
                case NEXLISTACTIVITYBASIC:
                    ((NexListActivityBasic) getContext()).setHttpConexion(this);
                    if (ibShowProgress) {
                        ((NexListActivityBasic) getContext()).showProgressDialog(Html.fromHtml("<font color='white'>" + isMensaje + "</font>"));
                    }
                    break;
                case NEXLISTACTIVITYTWEAKED:
                    ((NexListActivityTweaked) getContext()).setHttpConexion(this);
                    if (ibShowProgress) {
                        ((NexListActivityTweaked) getContext()).showProgressDialog(Html.fromHtml("<font color='white'>" + isMensaje + "</font>"));
                    }
                    break;
                case NEXFRAGMENTACTIVITY:
                    ((NexFragmentActivity) getContext()).setHttpConexion(this);
                    if (ibShowProgress) {
                        ((NexFragmentActivity) getContext()).showProgressDialog(Html.fromHtml("<font color='white'>" + isMensaje + "</font>"));
                    }
                    break;
                case NEXFRAGMENTLISTACTIVITYBASIC:
                    ((NexFragmentListActivityBasic) getContext()).setHttpConexion(this);
                    if (ibShowProgress) {
                        ((NexFragmentListActivityBasic) getContext()).showProgressDialog(Html.fromHtml("<font color='white'>" + isMensaje + "</font>"));
                    }
                    break;
                case NEXFRAGMENTLISTACTIVITYTWEAKED:
                    ((NexFragmentListActivityTweaked) getContext()).setHttpConexion(this);
                    if (ibShowProgress) {
                        ((NexFragmentListActivityTweaked) getContext()).showProgressDialog(Html.fromHtml("<font color='white'>" + isMensaje + "</font>"));
                    }
                    break;
                case NEXPREFERENCEACTIVITY:
                    ((NexPreferenceActivity) getContext()).setHttpConexion(this);
                    if (ibShowProgress) {
                        ((NexPreferenceActivity) getContext()).showProgressDialog(Html.fromHtml("<font color='white'>" + isMensaje + "</font>"));
                    }
                    break;
                default:
                    /*if(!Utilitario.fnVerOperador(ioContext)){
						setMsgError(ioContext.getString(R.string.actopeinvalido_bartitulo));
						throw new NexInvalidOperatorException(ioContext);
					}*/
                    subInitializeHttpConexion(Html.fromHtml("<font color='white'>" + isMensaje + "</font>"), ibShowProgress);
                    break;
            }
            setHttpResponseObject(null);
            isPrefixError = iniPrefixError();
            isPrefixErrorHTTP = iniPrefixErrorHTTP();
            ibMakeConnection = OnPreConnect();
        } catch (Exception e) {
            subSetHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse.ERROR, e.getMessage());
        }
    }

    /**
     * Inicializa la conexi&#243;n Http. Es usado para inciar el di&#225;logo de progeso
     *
     * @param poMessageProgress - Mensaje del di&#225;logo de progreso
     * @param showProgress      - Indica si el di&#225;logo de progreso est&#225; debe de mostrarse
     */
    protected void subInitializeHttpConexion(CharSequence poMessageProgress, boolean showProgress) {
        Log.v("PRO", this.getClass().getName() + "=>Falta implementar subInitializeHttpConexion");
    }

    @Override
    protected String doInBackground(Void... params) {
        if (ibMakeConnection)
            OnConnect();
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        OnPostConnect();
        subSetHttpResponseIdMessageObject(getHttpResponseIdEnum(), getHttpResponseMessage(), getHttpResponseObject());
        switch (ioEnumTipActividad) {
            case NEXACTIVITY:
                try {
                    if (ibShowProgress) {
                        ((NexActivity) ioContext).dismissDialog(EnumDialog.PROGRESS.getValue());
                    }
                } catch (Exception e) {
                    Log.e("PRO", "HttpConexion.onPostExecute " + e, e);
                }
                ((NexActivity) ioContext).subHttpResultado();
                ((NexActivity) ioContext).setHttpConexion(null);
                break;
            case NEXLISTACTIVITYBASIC:
                try {
                    if (ibShowProgress) {
                        ((NexListActivityBasic) ioContext).dismissDialog(EnumDialog.PROGRESS.getValue());
                    }
                } catch (Exception e) {
                    Log.e("PRO", "HttpConexion.onPostExecute " + e, e);
                }
                ((NexListActivityBasic) ioContext).subHttpResultado();
                ((NexListActivityBasic) ioContext).setHttpConexion(null);
                break;
            case NEXLISTACTIVITYTWEAKED:
                try {
                    if (ibShowProgress) {
                        ((NexListActivityTweaked) ioContext).dismissDialog(EnumDialog.PROGRESS.getValue());
                    }
                } catch (Exception e) {
                    Log.e("PRO", "HttpConexion.onPostExecute " + e, e);
                }
                ((NexListActivityTweaked) ioContext).subHttpResultado();
                ((NexListActivityTweaked) ioContext).setHttpConexion(null);
                break;
            case NEXFRAGMENTACTIVITY:
                try {
                    if (ibShowProgress) {
                        ((NexFragmentActivity) ioContext).dismissDialog(EnumDialog.PROGRESS.getValue());
                    }
                } catch (Exception e) {
                    Log.e("PRO", "HttpConexion.onPostExecute " + e, e);
                }
                ((NexFragmentActivity) ioContext).subHttpResultado();
                ((NexFragmentActivity) ioContext).setHttpConexion(null);
                break;
            case NEXFRAGMENTLISTACTIVITYBASIC:
                try {
                    if (ibShowProgress) {
                        ((NexFragmentListActivityBasic) ioContext).dismissDialog(EnumDialog.PROGRESS.getValue());
                    }
                } catch (Exception e) {
                    Log.e("PRO", "HttpConexion.onPostExecute " + e, e);
                }
                ((NexFragmentListActivityBasic) ioContext).subHttpResultado();
                ((NexFragmentListActivityBasic) ioContext).setHttpConexion(null);
                break;
            case NEXFRAGMENTLISTACTIVITYTWEAKED:
                try {
                    if (ibShowProgress) {
                        ((NexFragmentListActivityTweaked) ioContext).dismissDialog(EnumDialog.PROGRESS.getValue());
                    }
                } catch (Exception e) {
                    Log.e("PRO", "HttpConexion.onPostExecute " + e, e);
                }
                ((NexFragmentListActivityTweaked) ioContext).subHttpResultado();
                ((NexFragmentListActivityTweaked) ioContext).setHttpConexion(null);
                break;
            case NEXPREFERENCEACTIVITY:
                try {
                    if (ibShowProgress) {
                        ((NexPreferenceActivity) ioContext).dismissDialog(EnumDialog.PROGRESS.getValue());
                    }
                } catch (Exception e) {
                    Log.e("PRO", "HttpConexion.onPostExecute " + e, e);
                }
                ((NexPreferenceActivity) ioContext).subHttpResultado();
                ((NexPreferenceActivity) ioContext).setHttpConexion(null);
                break;
            default:
                subCallHttpResponse(ibShowProgress);
                break;
        }
    }

    /**
     * M&#233;todo que es llamado cuando finaliza el {@link AsyncTask} cuando el {@link Activity} es un {@link EnumTipActividad#NEXCUSTOMACTIVITY NEXCUSTOMACTIVITY}</br>
     * Hay que sobreescribir el m&#233;todo (no hay que llamar al padre)</br>
     * Utilizar los m&#233;todos {@link #getHttpResponseMessage} y {@link #getHttpResponseId} para obtener las respuestas de conexi&#243;n
     *
     * @param showProgress - Indicador de si el di&#225;logo est&#225; mostr&#225;ndose
     * @see #getHttpResponseMessage
     * @see #setHttpResponseMessage
     * @see #getHttpResponseId
     * @see #setHttpResponseId
     */
    protected void subCallHttpResponse(boolean showProgress) {
        Log.v("PRO", this.getClass().getName() + "=>Falta implementar subCallHttpResultado");
    }

    /**
     * Obtiene el archivo plano del {@link InputStream} de la conexi&#243;n, guard&#225;ndolo en la memoria SD o en la memoria interna del m&#243;vil
     *
     * @param poInputStream   {@link InputStream} de la conexi&#243;n
     * @param psDBName        Nombre de la base de datos SQLite
     * @param piDBVersion     versi&#243;n de la base de datos SQLite
     * @param poBDFramework   Objeto {@link BDFramework}
     * @param pbMemoriaSD     Indicador de uso de memoria SD o memoria Interna
     * @param poContext       Contexto de la aplicaci&#243;n
     * @param psNomAplicacion Nombre de la aplicaci&#243;n
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si el proceso se realiz&#243; sin problemas</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     * @throws IOException
     */
    protected synchronized boolean fnSinSD(InputStream poInputStream, String psDBName, int piDBVersion, BDFramework poBDFramework, boolean pbMemoriaSD, Context poContext, String psNomAplicacion) throws IOException {
        boolean lbResultado = false;
        File file = null;
        String PATH = "";
        if (pbMemoriaSD) {
            PATH = Environment.getExternalStorageDirectory() + Utilitario.fnNomCarTxt(psNomAplicacion);
            file = new File(PATH);
        } else {
            ContextWrapper cw = new ContextWrapper(poContext);
            file = cw.getDir("media", Context.MODE_PRIVATE);
        }

        Log.v("XXX", "PATH: " + PATH);

        file.mkdirs();
        File outputFile = new File(file, ConfiguracionNextel.CONSSINNOMBRETXT);
        FileOutputStream fos = new FileOutputStream(outputFile);
        byte[] buffer = new byte[8192];
        int len1 = 0;
        while ((len1 = poInputStream.read(buffer)) != -1) {
            fos.write(buffer, 0, len1);
        }
        fos.close();
        poInputStream.close();
        lbResultado = fnCrearDBInsertar(outputFile, psDBName, piDBVersion, poBDFramework);

        return lbResultado;
    }

    private String LAST_ERROR_DECOMPRESS;

    /**
     * Realiza la lectura del archivo plano de sincronizaci&#243;n y ejecuta los Scripts de sentencias en SQLite
     *
     * @param file          Archivo plano
     * @param psDBName      Nombre de la base de datos SQLite
     * @param piDBVersion   Versi&#243;n de la base de datos SQLite
     * @param poBDFramework Objeto {@link BDFramework}
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si el proceso se realiz&#243; sin problemas</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    protected synchronized boolean fnCrearDBInsertar(File file, String psDBName, int piDBVersion, BDFramework poBDFramework) {
        poBDFramework.setDB_NOMBRE(psDBName);
        poBDFramework.setDB_VERSION(piDBVersion);
        boolean resultado = poBDFramework.verificaExistencia();
        if (resultado) {
            resultado = poBDFramework.insertBatch(file);
            if (resultado == true) {
                return true;
            } else {

                return false;
            }
        } else {

            return false;
        }
    }

    /**
     * Retorna el mensaje de respuesta de la conexi&#243;n
     *
     * @return Mensaje de respuesta
     */
    public String getHttpResponseMessage() {
        return isHttpResponseMessage;
    }

    /**
     * Setea el mensaje de respuesta de la conexi&#243;n
     *
     * @param psHttpMessageResponse
     */
    public void setHttpResponseMessage(String psHttpMessageResponse) {
        this.isHttpResponseMessage = psHttpMessageResponse;
    }

    /**
     * Retorna el Id de respuesta de la conexi&#243;n
     *
     * @return Id de respuesta
     * @deprecated Utilizar el m&#233;todo {@link #getHttpResponseIdEnum}
     */
    public int getHttpResponseId() {
        return iiHttpResponseId;
    }

    /**
     * Retorna el Id de respuesta de la conexi&#243;n
     *
     * @return Id de respuesta
     */
    public ConfiguracionNextel.EnumServerResponse getHttpResponseIdEnum() {
        return ConfiguracionNextel.EnumServerResponse.get(iiHttpResponseId);
    }

    /**
     * Setea el Id de respuesta de la conexi&#243;n
     *
     * @param piHttpIdResponse
     * @deprecated Utilizar el m&#233;todo {@link #setHttpResponseId(ConfiguracionNextel.EnumServerResponse) setHttpResponseId(EnumServerResponse)}
     */
    public void setHttpResponseId(int piHttpIdResponse) {
        this.iiHttpResponseId = piHttpIdResponse;
    }

    /**
     * Setea el Id de respuesta de la conexi&#243;n
     *
     * @param poHttpIdResponse
     */
    public void setHttpResponseId(ConfiguracionNextel.EnumServerResponse poHttpIdResponse) {
        this.iiHttpResponseId = poHttpIdResponse.getValue();
    }

    /**
     * M&#233;todo para setear el Id y Mensaje de Respuesta</br>
     * El m&#233;todo discrimina el tipo de {@link Activity} o {@link Context}
     *
     * @param piHttpResponseId      Id de Respuesta
     * @param psHttpResponseMessage Mensaje de Respuesta
     */
    private void subSetHttpResponseIdMessage(int piHttpResponseId, String psHttpResponseMessage) {
        setHttpResponseMessage(psHttpResponseMessage);
        setHttpResponseId(piHttpResponseId);
        switch (ioEnumTipActividad) {
            case NEXACTIVITY:
                ((NexActivity) ioContext).setHttpResultado(psHttpResponseMessage);
                ((NexActivity) ioContext).setIdHttpResultado(piHttpResponseId);
                break;
            case NEXLISTACTIVITYBASIC:
                ((NexListActivityBasic) ioContext).setHttpResultado(psHttpResponseMessage);
                ((NexListActivityBasic) ioContext).setIdHttpResultado(piHttpResponseId);
                break;
            case NEXLISTACTIVITYTWEAKED:
                ((NexListActivityTweaked) ioContext).setHttpResultado(psHttpResponseMessage);
                ((NexListActivityTweaked) ioContext).setIdHttpResultado(piHttpResponseId);
                break;
            case NEXFRAGMENTACTIVITY:
                ((NexFragmentActivity) ioContext).setHttpResultado(psHttpResponseMessage);
                ((NexFragmentActivity) ioContext).setIdHttpResultado(piHttpResponseId);
                break;
            case NEXFRAGMENTLISTACTIVITYBASIC:
                ((NexFragmentListActivityBasic) ioContext).setHttpResultado(psHttpResponseMessage);
                ((NexFragmentListActivityBasic) ioContext).setIdHttpResultado(piHttpResponseId);
                break;
            case NEXFRAGMENTLISTACTIVITYTWEAKED:
                ((NexFragmentListActivityTweaked) ioContext).setHttpResultado(psHttpResponseMessage);
                ((NexFragmentListActivityTweaked) ioContext).setIdHttpResultado(piHttpResponseId);
                break;
            case NEXPREFERENCEACTIVITY:
                ((NexPreferenceActivity) ioContext).setHttpResultado(psHttpResponseMessage);
                ((NexPreferenceActivity) ioContext).setIdHttpResultado(piHttpResponseId);
                break;
            default:
                setHttpResponseMessage(psHttpResponseMessage);
                setHttpResponseId(piHttpResponseId);
                break;
        }
    }

    /**
     * M&#233;todo para setear el Id y Mensaje de Respuesta</br>
     * El m&#233;todo discrimina el tipo de {@link Activity} o {@link Context}
     *
     * @param poHttpResponseId      Id de Respuesta
     * @param psHttpResponseMessage Mensaje de Respuesta
     */
    private void subSetHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse poHttpResponseId, String psHttpResponseMessage) {
        setHttpResponseMessage(psHttpResponseMessage);
        setHttpResponseId(poHttpResponseId);
        switch (ioEnumTipActividad) {
            case NEXACTIVITY:
                ((NexActivity) ioContext).setHttpResultado(psHttpResponseMessage);
                ((NexActivity) ioContext).setEnumIdHttpResultado(poHttpResponseId);
                break;
            case NEXLISTACTIVITYBASIC:
                ((NexListActivityBasic) ioContext).setHttpResultado(psHttpResponseMessage);
                ((NexListActivityBasic) ioContext).setEnumIdHttpResultado(poHttpResponseId);
                break;
            case NEXLISTACTIVITYTWEAKED:
                ((NexListActivityTweaked) ioContext).setHttpResultado(psHttpResponseMessage);
                ((NexListActivityTweaked) ioContext).setEnumIdHttpResultado(poHttpResponseId);
                break;
            case NEXFRAGMENTACTIVITY:
                ((NexFragmentActivity) ioContext).setHttpResultado(psHttpResponseMessage);
                ((NexFragmentActivity) ioContext).setEnumIdHttpResultado(poHttpResponseId);
                break;
            case NEXFRAGMENTLISTACTIVITYBASIC:
                ((NexFragmentListActivityBasic) ioContext).setHttpResultado(psHttpResponseMessage);
                ((NexFragmentListActivityBasic) ioContext).setEnumIdHttpResultado(poHttpResponseId);
                break;
            case NEXFRAGMENTLISTACTIVITYTWEAKED:
                ((NexFragmentListActivityTweaked) ioContext).setHttpResultado(psHttpResponseMessage);
                ((NexFragmentListActivityTweaked) ioContext).setEnumIdHttpResultado(poHttpResponseId);
                break;
            case NEXPREFERENCEACTIVITY:
                ((NexPreferenceActivity) ioContext).setHttpResultado(psHttpResponseMessage);
                ((NexPreferenceActivity) ioContext).setEnumIdHttpResultado(poHttpResponseId);
                break;
            default:
                setHttpResponseMessage(psHttpResponseMessage);
                setHttpResponseId(poHttpResponseId);
                break;
        }
    }

    /**
     * M&#233;todo para setear el Id, Mensaje y Objeto de Respuesta</br>
     * El m&#233;todo discrimina el tipo de {@link Activity} o {@link Context}
     *
     * @param poHttpResponseId      Id de Respuesta
     * @param psHttpResponseMessage Mensaje de Respuesta
     * @param poHttpResponseObject  Objeto de respuesta
     */
    private void subSetHttpResponseIdMessageObject(ConfiguracionNextel.EnumServerResponse poHttpResponseId, String psHttpResponseMessage, Object poHttpResponseObject) {
        subSetHttpResponseIdMessage(poHttpResponseId, psHttpResponseMessage);
        setHttpResponseObject(poHttpResponseObject);
    }

    /**
     * M&#233;todo para setear el Id, Mensaje y Objeto de Respuesta</br>
     * El m&#233;todo discrimina el tipo de {@link Activity} o {@link Context}
     *
     * @param piHttpResponseId      Id de Respuesta
     * @param psHttpResponseMessage Mensaje de Respuesta
     * @param poHttpResponseObject  Objeto de respuesta
     */
    @SuppressWarnings("unused")
    private void subSetHttpResponseIdMessageObject(int piHttpResponseId, String psHttpResponseMessage, Object poHttpResponseObject) {
        subSetHttpResponseIdMessage(piHttpResponseId, psHttpResponseMessage);
        setHttpResponseObject(poHttpResponseObject);
    }

    /**
     * Instancia el t&#237;tulo del di&#225;logo de mensaje satisfactorio en el contexto de la aplicaci&#243;n, cuando este no pertenece a las actividades pre-definidas.
     *
     * @param msgOk - t&#237;tulo del mensaje
     */
    protected void setCustomMsgOk(String msgOk) {
        Log.v("PRO", this.getClass().getName() + "=>Falta implementar setCustomMsgOk");
    }

    /**
     * Instancia el t&#237;tulo del di&#225;logo de mensaje de error en el contexto de la aplicaci&#243;n, cuando este no pertenece a las actividades pre-definidas
     *
     * @param msgError - t&#237;tulo del mensaje
     */
    protected void setCustomMsgError(String msgError) {
        Log.v("PRO", this.getClass().getName() + "=>Falta implementar setCustomMsgError");
    }

    /**
     * Instancia el t&#237;tulo del di&#225;logo de mensaje satisfactorio en el contexto de la aplicaci&#243;n
     *
     * @param msgOk - t&#237;tulo del mensaje
     */
    protected void setMsgOk(String msgOk) {
        switch (ioEnumTipActividad) {
            case NEXACTIVITY:
                ((NexActivity) getContext()).setMsgOk(msgOk);
                break;
            case NEXFRAGMENTACTIVITY:
                ((NexFragmentActivity) getContext()).setMsgOk(msgOk);
                break;
            case NEXLISTACTIVITYBASIC:
                ((NexListActivityBasic) getContext()).setMsgOk(msgOk);
                break;
            case NEXLISTACTIVITYTWEAKED:
                ((NexListActivityTweaked) getContext()).setMsgOk(msgOk);
                break;
            case NEXFRAGMENTLISTACTIVITYBASIC:
                ((NexFragmentListActivityBasic) getContext()).setMsgOk(msgOk);
                break;
            case NEXFRAGMENTLISTACTIVITYTWEAKED:
                ((NexFragmentListActivityTweaked) getContext()).setMsgOk(msgOk);
                break;
            case NEXPREFERENCEACTIVITY:
                ((NexPreferenceActivity) getContext()).setMsgOk(msgOk);
                break;
            default:
                setCustomMsgOk(msgOk);
                break;
        }
    }

    /**
     * Instancia el t&#237;tulo del di&#225;logo de mensaje de error en el contexto de la aplicaci&#243;n
     *
     * @param msgError - t&#237;tulo del mensaje
     */
    protected void setMsgError(String msgError) {
        switch (ioEnumTipActividad) {
            case NEXACTIVITY:
                ((NexActivity) getContext()).setMsgError(msgError);
                break;
            case NEXFRAGMENTACTIVITY:
                ((NexFragmentActivity) getContext()).setMsgError(msgError);
                break;
            case NEXLISTACTIVITYBASIC:
                ((NexListActivityBasic) getContext()).setMsgError(msgError);
                break;
            case NEXLISTACTIVITYTWEAKED:
                ((NexListActivityTweaked) getContext()).setMsgError(msgError);
                break;
            case NEXFRAGMENTLISTACTIVITYBASIC:
                ((NexFragmentListActivityBasic) getContext()).setMsgError(msgError);
                break;
            case NEXFRAGMENTLISTACTIVITYTWEAKED:
                ((NexFragmentListActivityTweaked) getContext()).setMsgError(msgError);
                break;
            case NEXPREFERENCEACTIVITY:
                ((NexPreferenceActivity) getContext()).setMsgError(msgError);
                break;
            default:
                setCustomMsgError(msgError);
                break;
        }
    }

    private String isPrefixError = "";
    private String isPrefixErrorHTTP = "";

    /**
     * Retorna el Prefijo Error en general
     *
     * @return {String}
     */
    public final String getPrefixError() {
        return isPrefixError;
    }

    /**
     * Retorna el Prefijo Error de HTTP
     *
     * @return {String}
     */
    public final String getPrefixErrorHTTP() {
        return isPrefixErrorHTTP;
    }

    /**
     * Inicializa el Prefijo de Error general.</br>
     * Este m&#233;todo es invocado en el {@link #onPreExecute} del {@link AsyncTask}.</br>
     * Por defecto retorna el prefijo <b>R.string.msg_error</b>
     *
     * @return {String}
     */
    protected String iniPrefixError() {
        return ioContext.getString(R.string.msg_error);
    }

    /**
     * Inicializa el Prefijo de Error HTTP.</br>
     * Este m&#233;todo es invocado en el {@link #onPreExecute} del {@link AsyncTask}.</br>
     * Por defecto retorna el prefijo <b>R.string.msg_httperror</b>
     *
     * @return {String}
     */
    protected String iniPrefixErrorHTTP() {
        return ioContext.getString(R.string.msg_httperror);
    }

    /**
     * Retorna la URL a conectar
     *
     * @return {String}
     */
    public String getURL() {
        return URL;
    }

    /**
     * Instancia la URL a conectar
     *
     * @param URL URL a conectar
     */
    public void setURL(String URL) {
        this.URL = URL;
    }

    /**
     * Instancia el Id y mensaje de respuesta de conexi&#243;n
     *
     * @param piHttpIdResponse      Id de respuesta
     * @param psHttpMessageResponse Mensaje de respuesta
     */
    public void setHttpResponseIdMessage(int piHttpIdResponse, String psHttpMessageResponse) {
        setHttpResponseId(piHttpIdResponse);
        setHttpResponseMessage(psHttpMessageResponse);
        setHttpResponseObject(null);
    }

    /**
     * Instancia el Id y mensaje de respuesta de conexi&#243;n
     *
     * @param poHttpIdResponse      Id de respuesta
     * @param psHttpMessageResponse Mensaje de respuesta
     */
    public void setHttpResponseIdMessage(ConfiguracionNextel.EnumServerResponse poHttpIdResponse, String psHttpMessageResponse) {
        setHttpResponseId(poHttpIdResponse);
        setHttpResponseMessage(psHttpMessageResponse);
        setHttpResponseObject(null);
    }

    /**
     * Instancia el Id, mensaje y objeto de respuesta de conexi&#243;n
     *
     * @param piHttpIdResponse      Id de respuesta
     * @param psHttpMessageResponse Mensaje de respuesta
     * @param poHttpResponseObject  Objeto de respuesta
     */
    public void setHttpResponseIdMessageObject(int piHttpIdResponse, String psHttpMessageResponse, Object poHttpResponseObject) {
        setHttpResponseId(piHttpIdResponse);
        setHttpResponseMessage(psHttpMessageResponse);
        setHttpResponseObject(poHttpResponseObject);
    }

    /**
     * Instancia el Id, mensaje y objeto de respuesta de conexi&#243;n
     *
     * @param poHttpIdResponse      Id de respuesta
     * @param psHttpMessageResponse Mensaje de respuesta
     * @param poHttpResponseObject  Objeto de respuesta
     */
    public void setHttpResponseIdMessageObject(ConfiguracionNextel.EnumServerResponse poHttpIdResponse, String psHttpMessageResponse, Object poHttpResponseObject) {
        setHttpResponseId(poHttpIdResponse);
        setHttpResponseMessage(psHttpMessageResponse);
        setHttpResponseObject(poHttpResponseObject);
    }
}

