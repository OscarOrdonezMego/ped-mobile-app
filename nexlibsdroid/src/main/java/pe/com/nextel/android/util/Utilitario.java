package pe.com.nextel.android.util;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Environment;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Properties;
import java.util.StringTokenizer;

import pe.com.nextel.android.R;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumPreferenceKey;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTheme;

/**
 * Clase est&#225;tica con m&#233;todos utilitarios.
 *
 * @author jroeder
 */
public class Utilitario {

    /**
     * Verifica el estado de la se&#241;al del equipo
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> en caso exista se&#241;al</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnVerSignal(Context poContext) {
        return isNetworkConnected(poContext);
    }

    /**
     * Verifica el estado de la se&#241;al del equipo
     *
     * @param c - Contexto de la aplicaci&#243;n
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> en caso exista se&#241;al</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean isNetworkConnected(Context c) {
        //Log.d("XXX", "isNetworkConnected.BEGIN");
        ConnectivityManager connectivityManager
                = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        @SuppressLint("MissingPermission") NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Verifica si el operador actual es el de <b>NEXTEL</b> o <b>ENTEL</b>.
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> en caso sea correcto</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public static boolean fnVerOperador(Context poContext) {
        return fnVerOperador(poContext, false);
    }

    private static boolean fnVerOperador(Context poContext, boolean fromException) {
        //if (Utilitario.hasClassInStackTrace(ConfigurationServiceGestor.class))
        //    return true;
        boolean lbResultado = true;
        try {
            String lsOperator = EnumTheme.getNetworkOperatorName(poContext);
            String simOperatorName = ConfiguracionNextel.EnumTheme.getSimOperatorName(poContext);
            TelephonyManager loTelephonyManager = (TelephonyManager) poContext.getSystemService(Context.TELEPHONY_SERVICE);
            if (!fromException && fnVerSignal(poContext)) {
                ConnectivityManager connectivityManager = (ConnectivityManager) poContext.getSystemService(Context.CONNECTIVITY_SERVICE);
                @SuppressLint("MissingPermission") NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                Log.d("XXX", "fnVerOperador - activeNetworkInfo.isAvailable() : " + activeNetworkInfo.isAvailable());
                Log.d("XXX", "fnVerOperador - activeNetworkInfo.getTypeName() : " + activeNetworkInfo.getTypeName());
                if (activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED) {
                    Log.d("XXX", "fnVerOperador - NetworkInfo.State : CONNECTED");
                } else if (activeNetworkInfo.getState() == NetworkInfo.State.CONNECTING) {
                    Log.d("XXX", "fnVerOperador - NetworkInfo.State : CONNECTING");
                } else if (activeNetworkInfo.getState() == NetworkInfo.State.DISCONNECTED) {
                    Log.d("XXX", "fnVerOperador - NetworkInfo.State : DISCONNECTED");
                } else if (activeNetworkInfo.getState() == NetworkInfo.State.DISCONNECTING) {
                    Log.d("XXX", "fnVerOperador - NetworkInfo.State : DISCONNECTING");
                } else if (activeNetworkInfo.getState() == NetworkInfo.State.SUSPENDED) {
                    Log.d("XXX", "isNetworkConnected - NetworkInfo.State : SUSPENDED");
                } else if (activeNetworkInfo.getState() == NetworkInfo.State.UNKNOWN) {
                    Log.d("XXX", "isNetworkConnected - NetworkInfo.State : UNKNOWN");
                }

                Log.d("XXX", "fnVerOperador lsOperator : " + lsOperator);
                Log.d("XXX", "fnVerOperador loTelephonyManager.getNetworkOperator().toUpperCase() : " + loTelephonyManager.getNetworkOperator().toUpperCase());
                //Log.d("XXX", "fnVerOperador loTelephonyManager.getSimOperator().toUpperCase() : " + loTelephonyManager.getSimOperator().toUpperCase());
                //Log.d("XXX", "fnVerOperador loTelephonyManager.getSimSerialNumber().toUpperCase() : " + loTelephonyManager.getSimSerialNumber().toUpperCase());
                //Log.d("XXX", "fnVerOperador loTelephonyManager.getSimOperatorName().toUpperCase() : " + loTelephonyManager.getSimOperatorName().toUpperCase());

                lsOperator = loTelephonyManager.getNetworkOperator().toUpperCase();
                SharedPreferenceGestor.subIniSharedPrefer(poContext, EnumPreferenceKey.OPERATOR, EnumPreferenceKey.OPERATOR.toString(), lsOperator);
                /*if (loTelephonyManager.getNetworkOperator().toUpperCase().equals(lsOperator)
                        || loTelephonyManager.getSimOperatorName().toUpperCase().equals("ANDROID")) {
                    lbResultado = true;
                    SharedPreferenceGestor.subIniSharedPrefer(poContext, EnumPreferenceKey.OPERATOR, EnumPreferenceKey.OPERATOR.toString(), lsOperator);
                } else if (loTelephonyManager.getSimOperator().toUpperCase().equals(lsOperator)) {
                    lbResultado = true;
                    SharedPreferenceGestor.subIniSharedPrefer(poContext, EnumPreferenceKey.OPERATOR, EnumPreferenceKey.OPERATOR.toString(), lsOperator);
                } else if (loTelephonyManager.getSimOperatorName().toUpperCase().equals(simOperatorName)) {
                    lbResultado = true;
                    SharedPreferenceGestor.subIniSharedPrefer(poContext, EnumPreferenceKey.OPERATOR, EnumPreferenceKey.OPERATOR.toString(), lsOperator);
                } else {
                    lbResultado = false;
                }*/
            } else if (!fromException
                    && (loTelephonyManager.getSimOperator().toUpperCase().equals(lsOperator)
                    || loTelephonyManager.getSimOperatorName().toUpperCase().equals("ANDROID"))) {
                lbResultado = true;
                SharedPreferenceGestor.subIniSharedPrefer(poContext, EnumPreferenceKey.OPERATOR, EnumPreferenceKey.OPERATOR.toString(), lsOperator);
            } else {
                if (SharedPreferenceGestor.fnSharedPrefer(poContext, EnumPreferenceKey.OPERATOR, EnumPreferenceKey.OPERATOR.toString(), String.class).equals(lsOperator)) {
                    lbResultado = true;
                } else {
                    lbResultado = false;
                }
            }

            return lbResultado;
        } catch (Exception e) {
            Log.e("PRO", "Utilitario.fnVerOperador", e);
            return fnVerOperador(poContext, true);
        }
    }

    /**
     * Retorna el modelo del equipo, en el formato: <b>[Marca]-[Modelo]</b>
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @return {String}
     */
    public static String fnModeloEquipo(Context poContext) {
        String lsDispositivo = android.os.Build.BRAND + "-" + android.os.Build.MODEL;
        return Uri.encode(lsDispositivo);
    }

    /**
     * Retorna el <b>IMEI</b> del equipo.
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @return {String}
     */
    @SuppressLint("MissingPermission")
    public static String fnNumIMEI(Context poContext) {
        String lsRespuesta = "";
        TelephonyManager loTelephonyManager = (TelephonyManager) poContext.getSystemService(Context.TELEPHONY_SERVICE);
        //lsRespuesta = loTelephonyManager.getDeviceId();
        if (loTelephonyManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                lsRespuesta = loTelephonyManager.getImei();
            } else {
                lsRespuesta = loTelephonyManager.getDeviceId();
            }
        }
        return lsRespuesta;
    }

    /**
     * Retorna el <b>SIM</b> del chip del equipo-
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @return {String}
     */
    @SuppressLint("MissingPermission")
    public static String fnNumSim(Context poContext) {
        String lsRespuesta;
        TelephonyManager loTelephonyManager = (TelephonyManager) poContext.getSystemService(Context.TELEPHONY_SERVICE);
        lsRespuesta = loTelephonyManager.getSimSerialNumber();
        return lsRespuesta;
    }

    /**
     * Realiza un backup de la base de datos del SQLite de un paquete
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @throws IOException
     */
    @Deprecated
    public static void backupDatabase(Context poContext) throws IOException {
        //Open your local db as the input stream
        String inFileName = "/data/data/" + poContext.getPackageName() + "/databases/database.sqlite";
        File dbFile = new File(inFileName);
        FileInputStream fis = new FileInputStream(dbFile);

        String outFileName = Environment.getExternalStorageDirectory() + "/database.sqlite";
        //Open the empty db as the output stream
        OutputStream output = new FileOutputStream(outFileName);
        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = fis.read(buffer)) > 0) {
            output.write(buffer, 0, length);
        }
        //Close the streams
        output.flush();
        output.close();
        fis.close();
    }

    /**
     * Redondea un decimal
     *
     * @param pdValor     - N&#250;mero decimal
     * @param piPrecision - Precisi&#243;n de redondeo
     * @return double
     */
    @Deprecated
    public static double fnRedondeo(double pdValor, int piPrecision) {
        BigDecimal bd = new BigDecimal(pdValor);
        BigDecimal rounded = bd.setScale(piPrecision, BigDecimal.ROUND_HALF_UP);
        return rounded.doubleValue();
    }

    /**
     * Retorna una fecha en formato DD/MM/YYYY
     *
     * @param psFecha - Fecha en formato militar YYYYMMDD
     * @return {String}
     */
    @Deprecated
    public static String fnFechaFormato(String psFecha) {
        String loSB = psFecha.substring(6, 8) +
                "/" +
                psFecha.substring(4, 6) +
                "/" +
                psFecha.substring(0, 4);
        return loSB;
    }

    /**
     * Devuelve el c&#243;digo de versi&#243;n completo, formado por:</br>
     * <ul>
     * <li>Versi&#243;n actual (Manifest)</li>
     * <li>Versi&#243;n base (Properties)</li>
     * </ul>
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @return {String}
     */
    public static String fnVersion(Context poContext) {
        String lsVersion = "";
        PackageInfo loPackageInfo = null;
        try {
            loPackageInfo = poContext.getPackageManager().getPackageInfo(
                    poContext.getPackageName(), 0);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            Log.v("XXX", "Verificar el manifest: " + e.getMessage());
            return "ERROR VERSION";
        }

        lsVersion = loPackageInfo.versionName;
        try {
            String lsVersionProperties = fnVersionBase(poContext);
            if (!lsVersionProperties.equals("")) {
                lsVersion = lsVersion + " (" + lsVersionProperties + ")";
            }
        } catch (Exception e) {
            Log.v("XXX", "Verificar el properties: " + e.getMessage());
            //lsVersion = "ERROR DE LECTURA VERSION";
        }
        lsVersion = String.format(poContext.getString(R.string.actlogin_lblversion), lsVersion);
        //lsVersion=poContext.getString(R.string.actlogin_lblversion) + " : "+
        //lsVersion;
        return lsVersion;
    }

    /**
     * Devuelve la versi&#243;n actual de desarrollo
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @return {String}
     */
    @Deprecated
    public static String fnVersionActual(Context poContext) {
        String lsVersionBase = "";
        try {
            Properties loProperties = readProperties(poContext);
            lsVersionBase = loProperties.getProperty("VERSION");
        } catch (Exception e) {
            lsVersionBase = " ";
        }
        Log.v("XXX", lsVersionBase);
        return lsVersionBase;
    }

    /**
     * Devuelve la versi&#243;n base de la aplicaci&#243;n
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @return {String}
     */
    public static String fnVersionBase(Context poContext) {
        String lsVersionRelease = "";
        try {
            Properties loProperties = readProperties(poContext);
            lsVersionRelease = loProperties.getProperty("VERSION_BASE");
        } catch (Exception e) {
            lsVersionRelease = "";
        }
        Log.v("XXX", lsVersionRelease);
        return lsVersionRelease;
    }

    /**
     * Retorna la ruta de la carpeta del archivo plano de sincronizaci&#243;n
     *
     * @param psNomAplicacion - Nombre de la aplicaci&#243;n
     * @return {String}
     */
    public static String fnNomCarTxt(String psNomAplicacion) {
        return "/NextelApp/" + psNomAplicacion + ConfiguracionNextel.CONSSINNOMBRECARPETA;
    }

    /**
     * Retorna la ruta de la carpeta a crear o utilizar en el directorio de la aplicaci&#243;n</br>
     * La carpeta debe de ser del formato "<b>/&#60;psNomCarpeta&#62;/</b>"
     *
     * @param psNomAplicacion - Nombre de la aplicaci&#243;n
     * @param psNomCarpeta    - Nombre de carpeta a crear o utilizar
     * @return {String}
     */
    public static String fnNomCarpeta(String psNomAplicacion, String psNomCarpeta) {
        return "/NextelApp/" + psNomAplicacion + psNomCarpeta;
    }

    /**
     * Retorna la ruta de la carpeta de toma de fotos
     *
     * @param psNomAplicacion - Nombre de la aplicaci&#243;n
     * @return {String}
     */
    public static String fnNomCarFoto(String psNomAplicacion) {
        return "/NextelApp/" + psNomAplicacion + ConfiguracionNextel.CONSFOTNOMBRECARPETA;
    }

    /**
     * Redondea y da formato de decimales a cadena con punto flotante
     *
     * @param psValue   - El valor a redondear
     * @param piDecimal - N&#250;mero de decimales
     * @return {String}
     */
    public static String fnRound(String psValue, int piDecimal) {
        try {
            BigDecimal bd = new BigDecimal(psValue);
            StringBuilder lsDecimalPattern = new StringBuilder(piDecimal > 0 ? "." : "");
            for (int i = 0; i < piDecimal; i++) {
                lsDecimalPattern.append("#");
            }
            DecimalFormat df = new DecimalFormat("#0" + lsDecimalPattern);
            DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
            simbolos.setDecimalSeparator('.');
            df.setDecimalFormatSymbols(simbolos);
            StringBuilder lsNumberFormat = new StringBuilder(df.format(bd));
            StringTokenizer loToken = new StringTokenizer(lsNumberFormat.toString(), ".");
            loToken.nextToken();
            if (loToken.hasMoreTokens()) {
                for (int i = loToken.nextToken().length(); i < piDecimal; i++) {
                    lsNumberFormat.append("0");
                }
            } else {
                lsNumberFormat.append(piDecimal > 0 ? "." : "");
                for (int i = 0; i < piDecimal; i++) {
                    lsNumberFormat.append("0");
                }
            }
            return lsNumberFormat.toString();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Redondea y da formato de decimales a cadena con punto flotante
     *
     * @param pdValue   - El valor a redondear
     * @param piDecimal - N&#250;mero de decimales
     * @return {String}
     */
    public static String fnRound(double pdValue, int piDecimal) {
        return Utilitario.fnRound(String.valueOf(pdValue), piDecimal);
    }

    /**
     * Carga un objeto {@link Properties} de la carpeta <b>Assets</b>.
     *
     * @param ctx - Contexto de la aplicaci&#243;n
     * @param arc - Nombre del archivo
     * @return {@link Properties}
     * @since 1.6.4
     */
    public static Properties readProperties(Context ctx, String arc) {
        Properties prop = null;
        try {
            AssetManager am = ctx.getAssets();
            InputStream is = am.open(arc);
            prop = new Properties();
            prop.load(is);
        } catch (Exception ex) {
            Log.e("readProperties", "Error", ex);
        }
        return prop;
    }

    /**
     * Carga el archivo {@link Properties} <b>params.properties</b> por defecto de la carpeta <b>Assets</b>
     *
     * @param ctx - Contexto de la aplicaci&#243;n
     * @return {@link Properties}
     * @since 1.6.4
     */
    public static Properties readProperties(Context ctx) {
        return readProperties(ctx, ConfiguracionNextel.CONSPROPERTYFILE);
    }

    /**
     * Retorna una propiedad de un archvivo {@link Properties} de la carpeta <b>Assets</b>
     *
     * @param poContext    - Contexto de la aplicaci&#243;n
     * @param arc          - Nombre del archivo {@link Properties}
     * @param propertyName - Nombre de la propiedad a recuperar
     * @return {String}
     */
    public static String fnReadProperty(Context poContext, String arc, String propertyName) {
        String lsvalor = "";
        try {
            Properties loProperties = readProperties(poContext, arc);
            lsvalor = loProperties.getProperty(propertyName, "");
        } catch (Exception e) {
            lsvalor = "";
        }
        //Log.v("XXX", lsvalor);
        return lsvalor;
    }

    /**
     * Retorna una propiedad de un archivo {@link Properties} de la carpeta <b>Assets</b>
     *
     * @param poContext    - Contexto de la aplicaci&#243;n
     * @param arc          - Nombre del archivo {@link Properties}
     * @param propertyName - Nombre de la propiedad a recuperar
     * @return {String}
     */
    public static String fnReadProperty(Context poContext, String arc, Object propertyName) {
        String lsvalor = "";
        try {
            Properties loProperties = readProperties(poContext, arc);
            lsvalor = loProperties.getProperty(propertyName.toString(), "");
        } catch (Exception e) {
            lsvalor = "";
        }
        //Log.v("XXX", lsvalor);
        return lsvalor;
    }

    /**
     * Retorna una propiedad del archivo {@link Properties} <b>params.properties</b> de la carpeta <b>Assets</b>
     *
     * @param poContext    - Contexto de la aplicaci&#243;n
     * @param propertyName - Nombre de la propiedad a recuperar
     * @return {String}
     */
    public static String fnReadParamProperty(Context poContext, String propertyName) {
        return fnReadProperty(poContext, ConfiguracionNextel.CONSPROPERTYFILE, propertyName);
    }

    /**
     * Transforma una cadena en su forma cifrada en <b>SHA1</b>
     *
     * @param var - Variable a ser cifrada
     * @return {String}
     */
    public static String convertToSHA1(String var) {
        try {
            StringBuilder datHex = new StringBuilder();
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] arrData = var.getBytes();

            md.update(arrData, 0, arrData.length);
            byte[] digest = new byte[100];
            int val = md.digest(digest, 0, digest.length);

            for (int i = 0; i < val; i++) {
                String hex = Integer.toHexString(digest[i]);
                if (hex.length() == 1) hex = "0" + hex;
                hex = hex.substring(hex.length() - 2);
                datHex.append(hex);
            }
            return datHex.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Retorna el porcentaje de bater&#237;a en una escala del 1 al 100.
     *
     * @param context - Contexto de la aplicaci&#243;n
     * @return {String}
     */
    public static String fnBattery(Context context) {
        return fnBattery(context, 100);
    }

    /**
     * Retorna el porcentaje de bater&#237;a, de acuerdo a una escala personalizada.
     *
     * @param context - Contexto de la aplicaci&#243;n
     * @param piScale - Entero equivalente al 100% de bater&#237;a.
     * @return Nivel de bater&#237;a en una escala de 0 a piScale (entero).
     */
    public static String fnBattery(Context context, int piScale) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        float status = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
        float scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, 0);
        int res = Math.round((piScale * status) / scale);
        return "" + res;
    }

    /**
     * Retorna la se&#241; actual del equipo
     *
     * @return {String}
     */
    @Deprecated
    public static String fnSignal() {
        return "" + PhoneStateListener.LISTEN_SIGNAL_STRENGTH;
    }

    private static NexPhoneStateListener ioListenerTelefono = null;

    /**
     * Retorna un {@link NexPhoneStateListener} para obtener el estado de la se&#241;al
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @return {@link NexPhoneStateListener}
     */
    public static NexPhoneStateListener fnSignalState(Context poContext) {
        if (Utilitario.ioListenerTelefono == null) {
            ioListenerTelefono = new NexPhoneStateListener(poContext);
            TelephonyManager loManagerTelefono = (TelephonyManager) poContext.getSystemService(Context.TELEPHONY_SERVICE);
            loManagerTelefono.listen(ioListenerTelefono, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        }
        return ioListenerTelefono;
    }

    /**
     * Convierte un valor de densidad por p&#237;xel (dpi) a su valor en p&#237;xeles.
     *
     * @param value Valor de densidad por p&#237;xel
     * @return <b>int</b> valor en p&#237;xeles
     */
    public static int fnDensityToPixel(int value) {
        return (int) (value * Resources.getSystem().getDisplayMetrics().density);
    }

    private static boolean hasMethodInStackTrace(String methodName, StackTraceElement[] stackTraceElements, Boolean found) {
        if (found)
            return found;
        if (stackTraceElements.length == 1) {
            found = methodName.equals(stackTraceElements[0].getMethodName());
            return found;
        }
        int _middle = stackTraceElements.length / 2;
        StackTraceElement[] _first = new StackTraceElement[_middle];
        System.arraycopy(stackTraceElements, 0, _first, 0, _first.length);
        StackTraceElement[] _second = new StackTraceElement[stackTraceElements.length - _middle];
        System.arraycopy(stackTraceElements, _middle, _second, 0, _second.length);
        return hasMethodInStackTrace(methodName, _first, found) | hasMethodInStackTrace(methodName, _second, found);
    }

    private static boolean hasClassInStackTrace(Class<?> klass, StackTraceElement[] stackTraceElements, Boolean found) {
        if (found)
            return found;
        if (stackTraceElements.length == 1) {
            found = klass.getName().equals(stackTraceElements[0].getClassName());
            return found;
        }
        int _middle = stackTraceElements.length / 2;
        StackTraceElement[] _first = new StackTraceElement[_middle];
        System.arraycopy(stackTraceElements, 0, _first, 0, _first.length);
        StackTraceElement[] _second = new StackTraceElement[stackTraceElements.length - _middle];
        System.arraycopy(stackTraceElements, _middle, _second, 0, _second.length);
        return hasClassInStackTrace(klass, _first, found) | hasClassInStackTrace(klass, _second, found);
    }

    /**
     * Verifica si el nombre de un m&#233;todo se encuentra en el <b>StackTrace</b> .
     *
     * @param methodName Nombre del m&#233;todo
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si verifica que existe</li>
     * <li><b>false</b> si verifica que no existe</li>
     * </ul>
     */
    public static boolean hasMethodInStackTrace(String methodName) {
        boolean _exists = hasMethodInStackTrace(methodName, Thread.currentThread().getStackTrace(), false);
        Log.i("PRO", "Utilitario.hasMethodInStackTrace: " + methodName + " - " + _exists);
        return _exists;
    }

    /**
     * Verifica si el nombre de un m&#233;todo se encuentra en el <b>StackTrace</b> .
     *
     * @param methodName         Nombre del m&#233;todo
     * @param stackTraceElements Arreglo de {StackTraceElement StackTraceElement's}
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si verifica que existe</li>
     * <li><b>false</b> si verifica que no existe</li>
     * </ul>
     */
    public static boolean hasMethodInStackTrace(String methodName, StackTraceElement[] stackTraceElements) {
        boolean _exists = hasMethodInStackTrace(methodName, stackTraceElements, false);
        Log.i("PRO", "Utilitario.hasMethodInStackTrace: " + methodName + " - " + _exists);
        return _exists;
    }

    /**
     * Verifica si el nombre de una clase se encuentra en el <b>StackTrace</b> .
     *
     * @param klass Nombre de la clase
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si verifica que existe</li>
     * <li><b>false</b> si verifica que no existe</li>
     * </ul>
     */
    public static boolean hasClassInStackTrace(Class<?> klass) {
        boolean _exists = hasClassInStackTrace(klass, Thread.currentThread().getStackTrace(), false);
        Log.i("PRO", "Utilitario.hasClassInStackTrace: " + klass + " - " + _exists);
        return _exists;
    }

    /**
     * Elimina una foto de la ruta f&#237;sica
     *
     * @param path Ruta f&#237;sica de la foto tomada
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> Si la foto ha sido eliminada</li>
     * <li><b>false</b> Si la foto no ha sido eliminada</li>
     * </ul>
     */
    public static boolean fnDeletePhoto(String path) throws Exception {
        File _file = new File(path);
        return fnDeletePhoto(_file);
    }

    /**
     * Elimina una foto de la ruta f&#237;sica
     *
     * @param file {@link File} de la foto tomada
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> Si la foto ha sido eliminada</li>
     * <li><b>false</b> Si la foto no ha sido eliminada</li>
     * </ul>
     */
    public static boolean fnDeletePhoto(File file) throws Exception {
        boolean _answer = false;
        if (file.exists())
            _answer = file.delete();
        file = null;
        return _answer;
    }

    /**
     * Crea un {@link Intent} a la aplicaci&#243;n Google Maps del m&#243;vil
     *
     * @param latitude    Latitud en grados decimales
     * @param longitude   Longitud en grados decimales
     * @param description Descripci&#243;n a mostrar en la posici&#243;n creada en el GoogleMaps
     * @return {@link Intent}
     */
    public static Intent createIntentMaps(String latitude, String longitude, String description) {
        return createIntentMaps(latitude, longitude, description, true);
    }

    /**
     * Crea un {@link Intent} a la aplicaci&#243;n Google Maps del m&#243;vil o a otra aplicaci&#243;n.
     * </br>La otra aplicaci&#243;n es a selecci&#243;n del usuario.
     *
     * @param latitude    Latitud en grados decimales
     * @param longitude   Longitud en grados decimales
     * @param description Descripci&#243;n a mostrar en la posici&#243;n creada en la aplicaci&#243;n Google Maps del m&#243;vil o en la otra aplicaci&#243;n
     * @param usingGMaps  Indica si utiliza la aplicaci&#243;n Google Maps del m&#243;vil o en la otra aplicaci&#243;n
     *                    <ul>
     *                    <li><b>true</b> - Utiliza la aplicaci&#243;n Google Maps del m&#243;vil</li>
     *                    <li><b>false</b> - Utiliza otra aplicaci&#243;n</li>
     *                    </ul>
     * @return {@link Intent}
     */
    public static Intent createIntentMaps(String latitude, String longitude, String description, boolean usingGMaps) {
        if (usingGMaps) {
            return createIntentMaps(latitude, longitude, description, "com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        }
        return createIntentMaps(latitude, longitude, description, null, null);
    }

    /**
     * Crea un {@link Intent} de Maps a un paquete de una aplicaci&#243;n del m&#243;vil
     *
     * @param latitude    Latitud en grados decimales
     * @param longitude   Longitud en grados decimales
     * @param description Descripci&#243;n a mostrar en la posici&#243;n creada en la aplicaci&#243;n del paquete
     * @param packageName Nombre del paquete
     * @param className   Nombre de la clase
     * @return {@link Intent}
     */
    public static Intent createIntentMaps(String latitude, String longitude, String description, String packageName, String className) {
        String _url = "geo:" + latitude + "," + longitude + "?q=" + latitude + "," + longitude + "(" + description + ")";
        Intent _intent = new Intent(Intent.ACTION_VIEW, Uri.parse(_url));
        if (packageName != null && className != null)
            _intent.setClassName(packageName, className);
        return _intent;
    }

    /**
     * Obtiene el Id de recurso de referencia de un id de atributo.</br>
     * Este m&#233;todo debe ser invocado desde una actividad.
     *
     * @param context {@link Context} de la aplicaci&#243;n
     * @param attrRes Id de recurso del atributo
     * @return <b>int</b>
     */
    public static int obtainResourceId(Context context, int attrRes) {
        TypedArray loTypedArray = context.obtainStyledAttributes(new int[]{attrRes});
        try {
            return loTypedArray.getResourceId(0, -1);
        } finally {
            loTypedArray.recycle();
            loTypedArray = null;
        }
    }

    /**
     * Obtiene el Id de recurso de referencia de un id de atributo.</br>
     * Este m&#233;todo debe ser invocado desde un servicio.</br>
     * Utiliza el estilo R.style.Theme_Master por defecto.
     *
     * @param context   {@link Context} de la aplicaci&#243;n
     * @param attrStyle Id de recurso del atributo del estilo del {@link EnumTheme} actual
     * @param attr      Id de recurso del atributo
     * @return <b>int</b>
     */
    public static int obtainResourceId(Context context, int attrStyle, int attr) {
        TypedArray loTypedArray = null;
        TypedArray loTypedArrayStyle = null;
        try {
            loTypedArrayStyle = context.getTheme().obtainStyledAttributes(R.style.Theme_Master, new int[]{attrStyle});
            int liResStyle = loTypedArrayStyle.getResourceId(0, -1);
            loTypedArray = context.getTheme().obtainStyledAttributes(liResStyle, new int[]{attr});
            return loTypedArray.getResourceId(0, -1);
        } finally {
            if (loTypedArrayStyle != null)
                loTypedArrayStyle.recycle();
            loTypedArrayStyle = null;
            if (loTypedArray != null)
                loTypedArray.recycle();
            loTypedArray = null;
        }
    }

    /**
     * Obtiene el Id de recurso de referencia de un id de atributo.</br>
     * Este m&#233;todo debe ser invocado desde un servicio.
     *
     * @param context   {@link Context} de la aplicaci&#243;n
     * @param resMaster Id de recurso del estilo que hereda del R.style.Theme_Master
     * @param attrStyle Id de recurso del atributo del estilo del {@link EnumTheme} actual
     * @param attr      Id de recurso del atributo
     * @return <b>int</b>
     */
    public static int obtainResourceId(Context context, int resMaster, int attrStyle, int attr) {
        TypedArray loTypedArray = null;
        TypedArray loTypedArrayStyle = null;
        try {
            loTypedArrayStyle = context.getTheme().obtainStyledAttributes(resMaster, new int[]{attrStyle});
            int liResStyle = loTypedArrayStyle.getResourceId(0, -1);
            loTypedArray = context.getTheme().obtainStyledAttributes(liResStyle, new int[]{attr});
            return loTypedArray.getResourceId(0, -1);
        } finally {
            if (loTypedArrayStyle != null)
                loTypedArrayStyle.recycle();
            loTypedArrayStyle = null;
            if (loTypedArray != null)
                loTypedArray.recycle();
            loTypedArray = null;
        }
    }

    /**
     * Obtiene un <b>float</b> de referencia de un id de atributo.</br>
     * Este m&#233;todo debe ser invocado desde una actividad.
     *
     * @param context {@link Context} de la aplicaci&#243;n
     * @param attrRes Id de recurso del atributo
     * @return <b>float</b>
     */
    public static float obtainFloatResource(Context context, int attrRes) {
        TypedArray loTypedArray = context.obtainStyledAttributes(new int[]{attrRes});
        try {
            return loTypedArray.getFloat(0, -1);
        } finally {
            loTypedArray.recycle();
            loTypedArray = null;
        }
    }

    /**
     * Verifica si existe un paquete de aplicaci&#243;n instalada
     *
     * @param context {@link Context} de la aplicaci&#243;n
     * @param pakage  Nombre de paquere a verificar
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si existe</li>
     * <li><b>false</b> si no existe</li>
     * </ul>
     */
    public static boolean verifyPackageInstalled(Context context, String pakage) throws Exception {
        PackageManager loManager = context.getPackageManager();
        try {
            loManager.getPackageInfo(pakage, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (NameNotFoundException e) {
            return false;
        }
    }

    /**
     * Crea un {@link Intent} para realizar una instalaci&#243;n de un APK
     *
     * @param context {@link Context} de la aplicaci&#243;n
     * @param path    Ruta del archivo APK a instalar
     * @return {@link Intent}
     */
    public static Intent obtainIntentInstallApplication(Context context, String path) throws Exception {
        Intent loIntent = new Intent(Intent.ACTION_VIEW);
        loIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        loIntent.setDataAndType(Uri.parse("file://" + path), "application/vnd.android.package-archive");
        return loIntent;
    }

    /**
     * Retorna el valor de un <b>R.string.*</b> seg&#250;n su nombre en el XML de recursos
     *
     * @param context      {@link Context} de la aplicaci&#243;n
     * @param nameResource Nombre del recurso creado en el XML de recursos
     * @return {String}
     */
    public static String getStringResourceByName(Context context, String nameResource) {
        int resId = context.getResources()
                .getIdentifier(nameResource, "string", context.getPackageName());
        if (resId == 0) {
            return null;
        } else {
            return context.getString(resId);
        }
    }

    /**
     * Verifica si un servicio del paquete de la aplicaci&#243;n se est&#225; ejecutando
     *
     * @param context      {@link Context} de la aplicaci&#243;n
     * @param serviceClass Clase de servicio a verificar
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> Si verifica que se est&#225; ejecutando</li>
     * <li><b>false</b> Si verifica que no se est&#225; ejecutando</li>
     * </ul>
     */
    public static boolean verifyRunningService(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the current View.OnClickListener for the given View
     *
     * @param view the View whose click listener to retrieve
     * @return the View.OnClickListener attached to the view; null if it could not be retrieved
     */
    public static View.OnClickListener getOnClickListener(View view) {
        if (Build.VERSION.SDK_INT >= 14) {
            return getOnClickListenerV14(view);
        } else {
            return getOnClickListenerV(view);
        }
    }

    //Used for APIs lower than ICS (API 14)
    private static View.OnClickListener getOnClickListenerV(View view) {
        View.OnClickListener retrievedListener = null;
        String viewStr = "android.view.View";
        Field field;

        try {
            field = Class.forName(viewStr).getDeclaredField("mOnClickListener");
            retrievedListener = (View.OnClickListener) field.get(view);
        } catch (NoSuchFieldException ex) {
            Log.e("Reflection", "No Such Field.");
        } catch (IllegalAccessException ex) {
            Log.e("Reflection", "Illegal Access.");
        } catch (ClassNotFoundException ex) {
            Log.e("Reflection", "Class Not Found.");
        }

        return retrievedListener;
    }

    //Used for new ListenerInfo class structure used beginning with API 14 (ICS)
    private static View.OnClickListener getOnClickListenerV14(View view) {
        View.OnClickListener retrievedListener = null;
        String viewStr = "android.view.View";
        String lInfoStr = "android.view.View$ListenerInfo";

        try {
            Field listenerField = Class.forName(viewStr).getDeclaredField("mListenerInfo");
            Object listenerInfo = null;

            if (listenerField != null) {
                listenerField.setAccessible(true);
                listenerInfo = listenerField.get(view);
            }

            Field clickListenerField = Class.forName(lInfoStr).getDeclaredField("mOnClickListener");

            if (clickListenerField != null && listenerInfo != null) {
                retrievedListener = (View.OnClickListener) clickListenerField.get(listenerInfo);
            }
        } catch (NoSuchFieldException ex) {
            Log.e("Reflection", "No Such Field.");
        } catch (IllegalAccessException ex) {
            Log.e("Reflection", "Illegal Access.");
        } catch (ClassNotFoundException ex) {
            Log.e("Reflection", "Class Not Found.");
        }

        return retrievedListener;
    }

    /**
     * Retorna el n&#250;mero del equipo de la l&#237;nea 1.
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @return {String} - Retorna <b>null</b> en caso no se encuentre asignado en la l&#237;nea 1. Contactar al Operador, para realizar un aprovisionamiento.
     */
    @SuppressLint("MissingPermission")
    public static String fnNumEquipo(Context poContext) {
        String lsRespuesta;
        TelephonyManager loTelephonyManager = (TelephonyManager) poContext.getSystemService(Context.TELEPHONY_SERVICE);
        lsRespuesta = loTelephonyManager.getLine1Number();
        return lsRespuesta;
    }
}