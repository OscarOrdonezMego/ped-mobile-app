package pe.com.nextel.android.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import pe.com.nextel.android.R;
import pe.com.nextel.android.widget.NexToast.EnumType;

/**
 * Vista utilizada por la clase {@link NexToast}
 *
 * @author jroeder
 */
public class NexToastView extends LinearLayout {
    private LinearLayout layout;
    private ImageView image;
    private TextView text;

    /**
     * Constructor de la clase
     *
     * @param context - Contexto de la aplicaci&#243;n
     */
    public NexToastView(Context context) {
        super(context);
        try {
            ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.nextoastview, this);
        } catch (Exception e) {
            Log.e("PRO", Log.getStackTraceString(e));
        }
    }

    /**
     * Constructor de la clase
     *
     * @param context - Contexto de la aplicaci&#243;n
     * @param attrs   - Atributos del contexto
     */
    public NexToastView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Constructor de la clase
     *
     * @param context  - Contexto de la aplicaci&#243;n
     * @param attrs    - Atributos del contexto
     * @param defStyle - Estilo del contexto
     */
    public NexToastView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Instancia el tipo de mensaje de la vista
     *
     * @param type - Tipo de mensaje
     */
    public void setType(EnumType type) {
        onSetupView();
        switch (type) {
            case ACCEPT:
                layout.setBackgroundResource(R.drawable.nextoast_accept);
                image.setVisibility(VISIBLE);
                image.setImageResource(R.drawable.ic_nex_accept_1);
                break;
            case CANCEL:
                layout.setBackgroundResource(R.drawable.nextoast_cancel);
                image.setVisibility(VISIBLE);
                image.setImageResource(R.drawable.ic_nex_cancel_1);
                break;
            case ERROR:
                layout.setBackgroundResource(R.drawable.nextoast_error);
                image.setVisibility(VISIBLE);
                image.setImageResource(R.drawable.ic_nex_error_1);
                break;
            case INFORMATION:
                layout.setBackgroundResource(R.drawable.nextoast_information);
                image.setVisibility(VISIBLE);
                image.setImageResource(R.drawable.ic_nex_information_1);
                break;
            default:
                layout.setBackgroundResource(R.drawable.nextoast_default);
                break;
        }
    }

    /**
     * Instancia el texto de mensaje
     *
     * @param text - Texto de mensaje
     */
    public void setText(String text) {
        onSetupView();
        this.text.setText(text);
    }

    /**
     * Instancia la imagen o &#237;cono del mensaje
     *
     * @param resourceId - Id de recurso de la imagen
     */
    public void setImage(Integer resourceId) {
        onSetupView();
        if (resourceId != null) {
            image.setVisibility(VISIBLE);
            image.setImageResource(resourceId.intValue());
        } else {
            image.setVisibility(GONE);
        }
    }

    private void onSetupView() {
        if (layout == null)
            layout = findViewById(R.id.nextoast_layout);
        if (image == null)
            image = findViewById(R.id.nextoast_image);
        if (text == null)
            text = findViewById(R.id.nextoast_text);
    }
}
