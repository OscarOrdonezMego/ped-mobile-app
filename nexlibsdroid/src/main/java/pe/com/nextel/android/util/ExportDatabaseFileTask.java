package pe.com.nextel.android.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.Html;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import pe.com.nextel.android.R;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;

/**
 * Clase que realiza una tarea as&#237;ncrona de exportaci&#243;n de base de datos <b>SQLite</b></br>
 * Hereda del {@link AsyncTask}
 *
 * @author jroeder
 */
public class ExportDatabaseFileTask extends AsyncTask<String, Void, Boolean> {
    private final Context ioContext;
    private String isDatabaseNambe;
    private ProgressDialog ioProgressDialog;
    private boolean verOperador;

    /**
     * Constructor de la clase
     *
     * @param poContext      - Contexto de la aplicaci&#243;n
     * @param psDatabaseName - Nombre de la base de datos <b>SQLite</b>
     */
    public ExportDatabaseFileTask(Context poContext, String psDatabaseName) {
        ioContext = poContext;
        isDatabaseNambe = psDatabaseName;
    }

    // can use UI thread here
    protected void onPreExecute() {
        verOperador = true; //Utilitario.fnVerOperador(ioContext);
        ioProgressDialog = ProgressDialog.show(ioContext, "", ioContext.getString(R.string.msg_exportandobasedatos), true);
        ioProgressDialog.setMessage(Html.fromHtml("<font color='white'>" + ioContext.getString(R.string.msg_exportando) + "</font>"));
    }

    // automatically done on worker thread (separate from UI thread)
    protected Boolean doInBackground(final String... args) {
        if (!verOperador)
            return verOperador;
        File dbFile = new File(Environment.getDataDirectory() + "/data/" + ioContext.getApplicationContext().getPackageName() + "/databases/" + isDatabaseNambe);
        Log.v("XXX", Environment.getDataDirectory() + "/data/" + ioContext.getApplicationContext().getPackageName() + "/databases/" + isDatabaseNambe);

        File exportDir = new File(Environment.getExternalStorageDirectory(), "");
        if (!exportDir.exists()) {
            exportDir.mkdirs();
        }
        File file = new File(exportDir, dbFile.getName());

        try {
            file.createNewFile();
            this.copyFile(dbFile, file);
            return true;
        } catch (IOException e) {
            Log.e("mypck", e.getMessage(), e);
            return false;
        }
    }

    // can use UI thread here
    protected void onPostExecute(final Boolean success) {
        if (this.ioProgressDialog.isShowing()) {
            this.ioProgressDialog.dismiss();
        }
        if (success) {
//    		Toast.makeText(ioContext, ioContext.getString(R.string.msg_exportacionok), Toast.LENGTH_SHORT).show();
            new NexToast(ioContext, R.string.msg_exportacionok, EnumType.ACCEPT).show();
        } else if (verOperador) {
//    		Toast.makeText(ioContext, ioContext.getString(R.string.msg_exportacionerror), Toast.LENGTH_SHORT).show();
            new NexToast(ioContext, R.string.msg_exportacionerror, EnumType.ERROR).show();
        } else {
            new NexToast(ioContext, R.string.actopeinvalido_lblmensaje, EnumType.ERROR).show();
        }
    }

    /**
     * Copia un archivo a otro archivo
     *
     * @param src Archivo origen
     * @param dst Archivo destino
     * @throws IOException
     */
    @SuppressWarnings("resource")
    void copyFile(File src, File dst) throws IOException {
        FileChannel inChannel = new FileInputStream(src).getChannel();
        FileChannel outChannel = new FileOutputStream(dst).getChannel();
        try {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } finally {
            if (inChannel != null)
                inChannel.close();
            if (outChannel != null)
                outChannel.close();
        }
    }
}

