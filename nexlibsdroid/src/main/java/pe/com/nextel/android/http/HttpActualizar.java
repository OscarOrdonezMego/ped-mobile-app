package pe.com.nextel.android.http;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.text.Html;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import pe.com.nextel.android.R;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.actividad.NexListActivityBasic;
import pe.com.nextel.android.actividad.NexListActivityTweaked;
import pe.com.nextel.android.actividad.NexPreferenceActivity;
import pe.com.nextel.android.actividad.support.NexFragmentActivity;
import pe.com.nextel.android.actividad.support.NexFragmentListActivityBasic;
import pe.com.nextel.android.actividad.support.NexFragmentListActivityTweaked;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumDialog;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumServerResponse;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipConexion;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;

/**
 * Tarea As&#237;ncrona que realiza una conexi&#243;n HTTP para descargar una actualizaci&#243;n de la aplicaci&#243;n
 *
 * @author jroeder
 */
public class HttpActualizar extends AsyncTask<Void, Void, String> {
    protected Context ioContext;
    private String isMensaje;
    private String isUrl;
    private String isHttpResponseMessage = null;
    private int iiHttpResponseId = EnumServerResponse.OKNOMSG.getValue();
    private Intent ioIntent;
    private boolean ibMakeConnection = false;

    /**
     * Contructor de la clase
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @param psMensaje - Mensaje a mostrar en el di&#225;logo
     * @param psUrl     - URL de actualizaci&#243;n.
     */
    public HttpActualizar(Context poContext, String psMensaje, String psUrl) {
        ioContext = poContext;
        isMensaje = psMensaje;
        isUrl = psUrl;
    }

    /**
     * Adjunta el nuevo contexto de la aplicaci&#243;n
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     */
    public void attach(Context poContext) {
        ioContext = poContext;
    }

    /**
     * Desajunta el contexto de la aplicaci&#243;n del objeto actual
     */
    public void detach() {
        ioContext = null;
    }

    /**
     * Retorn el contexto de la aplicaci&#243;n
     *
     * @return Context
     */
    public Context getContext() {
        return ioContext;
    }

    /**
     * Inicializa la conexi&#243;n Http. Es usado para inciar el di&#225;logo de progeso
     *
     * @param poMessageProgress
     */
    protected void subInitializeHttpConexion(CharSequence poMessageProgress) {
        Log.v("PRO", this.getClass().getName() + "=>Falta implemetar subInitializeHttpConexion");
    }

    /**
     * M&#233;todo que es llamado cuando finaliza el {@link AsyncTask} cuando el {@link Activity} es un {@link EnumTipActividad#NEXCUSTOMACTIVITY NEXCUSTOMACTIVITY}</br>
     * Hay que sobreescribir el m&#233;todo (no hay que llamar al padre)</br>
     * Utilizar los m&#233;todos {@link #getHttpResponseMessage} y {@link #getHttpResponseId} para obtener las respuestas de conexi&#243;n
     *
     * @see #getHttpResponseMessage
     * @see #setHttpResponseMessage
     * @see #getHttpResponseId
     * @see #setHttpResponseId
     */
    protected void subCallHttpResponse() {
        Log.v("PRO", this.getClass().getName() + "=>Falta implemetar subCallHttpResultado");
    }

    private void showProgressDialog(CharSequence poMessage) {
        if (getContext() instanceof NexActivity) {
            ((NexActivity) getContext()).setHttpActualizar(this);
            ((NexActivity) getContext()).showProgressDialog(poMessage);
        } else if (getContext() instanceof NexListActivityBasic) {
            ((NexListActivityBasic) getContext()).setHttpActualizar(this);
            ((NexListActivityBasic) getContext()).showProgressDialog(poMessage);
        } else if (getContext() instanceof NexListActivityTweaked) {
            ((NexListActivityTweaked) getContext()).setHttpActualizar(this);
            ((NexListActivityTweaked) getContext()).showProgressDialog(poMessage);
        } else if (getContext() instanceof NexFragmentActivity) {
            ((NexFragmentActivity) getContext()).setHttpActualizar(this);
            ((NexFragmentActivity) getContext()).showProgressDialog(poMessage);
        } else if (getContext() instanceof NexPreferenceActivity) {
            ((NexPreferenceActivity) getContext()).setHttpActualizar(this);
            ((NexPreferenceActivity) getContext()).showProgressDialog(poMessage);
        } else if (getContext() instanceof NexFragmentListActivityBasic) {
            ((NexFragmentListActivityBasic) getContext()).setHttpActualizar(this);
            ((NexFragmentListActivityBasic) getContext()).showProgressDialog(poMessage);
        } else if (getContext() instanceof NexFragmentListActivityTweaked) {
            ((NexFragmentListActivityTweaked) getContext()).setHttpActualizar(this);
            ((NexFragmentListActivityTweaked) getContext()).showProgressDialog(poMessage);
        } else {
            subInitializeHttpConexion(poMessage);
        }
//		return EnumDialog.PROGRESS.getValue();
    }

    @SuppressWarnings("deprecation")
    private void dismissProgressDialog() {
        if (getContext() instanceof NexActivity) {
            try {
                ((NexActivity) ioContext).dismissDialog(EnumDialog.PROGRESS.getValue());
            } catch (Exception e) {
                Log.v("PRO", "HttpActualizar.dismissProgressDialog " + e);
            }
            ((NexActivity) ioContext).setHttpActualizar(null);
        } else if (getContext() instanceof NexListActivityBasic) {
            try {
                ((NexListActivityBasic) ioContext).dismissDialog(EnumDialog.PROGRESS.getValue());
            } catch (Exception e) {
                Log.v("PRO", "HttpActualizar.dismissProgressDialog " + e);
            }
            ((NexListActivityBasic) ioContext).setHttpActualizar(null);
        } else if (getContext() instanceof NexListActivityTweaked) {
            try {
                ((NexListActivityTweaked) ioContext).dismissDialog(EnumDialog.PROGRESS.getValue());
            } catch (Exception e) {
                Log.v("PRO", "HttpActualizar.dismissProgressDialog " + e);
            }
            ((NexListActivityTweaked) ioContext).setHttpActualizar(null);
        } else if (getContext() instanceof NexFragmentActivity) {
            try {
                ((NexFragmentActivity) ioContext).dismissDialog(EnumDialog.PROGRESS.getValue());
            } catch (Exception e) {
                Log.v("PRO", "HttpActualizar.dismissProgressDialog " + e);
            }
            ((NexFragmentActivity) ioContext).setHttpActualizar(null);
        } else if (getContext() instanceof NexPreferenceActivity) {
            try {
                ((NexPreferenceActivity) ioContext).dismissDialog(EnumDialog.PROGRESS.getValue());
            } catch (Exception e) {
                Log.v("PRO", "HttpActualizar.dismissProgressDialog " + e);
            }
            ((NexPreferenceActivity) ioContext).setHttpActualizar(null);
        } else if (getContext() instanceof NexFragmentListActivityBasic) {
            try {
                ((NexFragmentListActivityBasic) ioContext).dismissDialog(EnumDialog.PROGRESS.getValue());
            } catch (Exception e) {
                Log.v("PRO", "HttpActualizar.dismissProgressDialog " + e);
            }
            ((NexFragmentListActivityBasic) ioContext).setHttpActualizar(null);
        } else if (getContext() instanceof NexFragmentListActivityTweaked) {
            try {
                ((NexFragmentListActivityTweaked) ioContext).dismissDialog(EnumDialog.PROGRESS.getValue());
            } catch (Exception e) {
                Log.v("PRO", "HttpActualizar.dismissProgressDialog " + e);
            }
            ((NexFragmentListActivityTweaked) ioContext).setHttpActualizar(null);
        } else {
            subCallHttpResponse();
        }
    }

    /**
     * Retorna el mensaje de respuesta de la conexi&#243;n
     *
     * @return Mensaje de respuesta
     */
    public String getHttpResponseMessage() {
        return isHttpResponseMessage;
    }

    /**
     * Setea el mensaje de respuesta de la conexi&#243;n
     *
     * @param psHttpMessageResponse
     */
    public void setHttpResponseMessage(String psHttpMessageResponse) {
        this.isHttpResponseMessage = psHttpMessageResponse;
    }

    /**
     * Retorna el Id de respuesta de la conexi&#243;n
     *
     * @return Id de respuesta
     */
    public int getHttpResponseId() {
        return iiHttpResponseId;
    }

    /**
     * Setea el Id de respuesta de la conexi&#243;n
     *
     * @param piHttpIdResponse
     */
    public void setHttpResponseId(int piHttpIdResponse) {
        this.iiHttpResponseId = piHttpIdResponse;
    }

    /**
     * M&#233;todo para setear el Id y Mensaje de Respuesta</br>
     * El m&#233;todo discrimina el tipo de {@link Activity} o {@link Context}
     *
     * @param piHttpResponseId      Id de Respuesta
     * @param psHttpResponseMessage Mensaje de Respuesta
     */
    public void subSetHttpResponseIdMessage(int piHttpResponseId, String psHttpResponseMessage) {
        setHttpResponseMessage(psHttpResponseMessage);
        setHttpResponseId(piHttpResponseId);
    }

    @Override
    protected void onPreExecute() {
        ibMakeConnection = false;
        if (!Utilitario.fnVerSignal(ioContext)) {
            subSetHttpResponseIdMessage(EnumServerResponse.ERROR.getValue(), getContext().getString(R.string.msg_fueracobertura));
            return;
        }
        if (isUrl == null) {
            subSetHttpResponseIdMessage(EnumServerResponse.ERROR.getValue(), getContext().getString(R.string.msg_urlnoencontrada));
            return;
        }
        if (!isUrl.equals("")) {
            showProgressDialog(Html.fromHtml("<font color='white'>" + isMensaje + "</font>"));
        } else {
            subSetHttpResponseIdMessage(EnumServerResponse.ERROR.getValue(), getContext().getString(R.string.msg_urlnoencontrada));
            return;
        }
        ibMakeConnection = true;
    }

    @Override
    protected String doInBackground(Void... params) {
        if (ibMakeConnection)
            fnConectar();
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        if (EnumServerResponse.get(getHttpResponseId()) != EnumServerResponse.OKNOMSG) {
            if (getHttpResponseMessage() == null) {
                setHttpResponseMessage(ioContext.getString(R.string.msg_updateerror));
            }
            new NexToast(ioContext, getHttpResponseMessage(), EnumType.ERROR).show();
        }
        try {
            dismissProgressDialog();
        } catch (Exception e) {
            Log.e("PRO", "HttpActualizar.onPostExecute", e);
        }
        try {
            ioContext.startActivity(ioIntent);
        } catch (Exception e) {
            Log.e("PRO", "HttpActualizar.onPostExecute", e);
        }
    }

    /**
     * Realiza la conexi&#243;n HTTP
     */
    public void fnConectar() {
        try {
            Log.v("URL", isUrl);
            URL url = new URL(isUrl);
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setRequestMethod(EnumTipConexion.CONHTTP.getRequestGET());
            if (Build.VERSION.SDK_INT < 14)
                c.setDoOutput(true);
            c.connect();

            String PATH = Environment.getExternalStorageDirectory() + "/download/";
            File file = new File(PATH);
            file.mkdirs();
            File outputFile = new File(file, "app.apk");
            FileOutputStream fos = new FileOutputStream(outputFile);

            InputStream is = c.getInputStream();

            byte[] buffer = new byte[1024];
            int len1 = 0;
            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);
            }
            fos.close();
            is.close();

            ioIntent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
            //ioIntent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/download/" + "app.apk")), "application/vnd.android.package-archive");
            File fileApk = new File(Environment.getExternalStorageDirectory() + "/download/" + "app.apk");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Uri uri = FileProvider.getUriForFile(ioContext, ioContext.getPackageName() + ".provider", fileApk);
                //ioContext.grantUriPermission(ioContext.getPackageName(), uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
                //ioIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                ioIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_ACTIVITY_NEW_TASK);
                //ioIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                ioIntent.setDataAndType(uri, "application/vnd.android.package-archive");
                //ioIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            } else {
                ioIntent.setDataAndType(Uri.fromFile(fileApk), "application/vnd.android.package-archive");
            }
        } catch (IOException e) {
            Log.e("PRO", "HttpActualizar.fnConectar", e);
            subSetHttpResponseIdMessage(EnumServerResponse.ERROR.getValue(), null);
        }
    }
}


