package pe.com.nextel.android.item;

import android.support.v4.util.LongSparseArray;

import java.util.HashMap;
import java.util.Map;

/**
 * Clase utilizada para almacenar la informaci&#243;n de un elemento
 * de un Spinner o de Lista Simple
 *
 * @author jroeder
 * @version 2.0
 */
public class BeanSpinner {
    /**
     * Instancia del Id del &#205;tem
     */
    private long iiId;
    /**
     * Instancia de la Clave del &#205;tem
     */
    private String isKey;
    /**
     * Instancia de la Descripci&#243;n del &#205;tem
     */
    private String isDescription;

    /**
     * Devuelve el Id del &#205;tem
     *
     * @return long - Id del &#205;tem
     */
    public long getId() {
        return iiId;
    }

    /**
     * Asigna el Id del &#205;tem
     *
     * @param piId - Id del &#205;tem
     */
    public void setId(long piId) {
        this.iiId = piId;
    }

    /**
     * Devuelve la Clave del &#205;tem
     *
     * @return String - Clave del &#205;tem
     */
    public String getKey() {
        return isKey;
    }

    /**
     * Asigna la Clave del &#205;tem
     *
     * @param psKey - Clave del &#205;tem
     */
    public void setKey(String psKey) {
        this.isKey = psKey;
    }

    /**
     * Devuelve la Descripci&#243;n del &#205;tem
     *
     * @return String - Decripci&#243;n del &#205;tem
     */
    public String getDescription() {
        return isDescription;
    }

    /**
     * Asigna la Descripci&#243;n del &#205;tem
     *
     * @param psDescription - Descripci&#243;n del &#205;tem
     */
    public void setDescription(String psDescription) {
        this.isDescription = psDescription;
    }

    /**
     * Constructor de la clase.
     * Inicializa el objeto con valores vac&#237;os
     */
    public BeanSpinner() {
        setId(0);
        setKey("");
        setDescription("");
    }

    /**
     * Constructor de la clase.
     *
     * @param piId          - Id del &#205;tem
     * @param psKey         - Clave del &#205;tem
     * @param psDescription - Descripci&#243;n del &#205;tem
     */
    public BeanSpinner(long piId, String psKey, String psDescription) {
        setId(piId);
        setKey(psKey);
        setDescription(psDescription);
    }

    @Override
    public String toString() {
        return getDescription();
    }

    @Override
    public boolean equals(Object o) {
        BeanSpinner loBeanSpinner = (BeanSpinner) o;
        boolean _isKeyNull = getKey() == null && loBeanSpinner.getKey() == null ? true : false;
        boolean _isDescNull = getDescription() == null && loBeanSpinner.getDescription() == null ? true : false;
        if (getId() == loBeanSpinner.getId()
                && ((getKey() != null && loBeanSpinner.getKey() != null && getKey().equals(loBeanSpinner.getKey())) || _isKeyNull)
                && ((getDescription() != null && loBeanSpinner.getDescription() != null && getDescription().equals(loBeanSpinner.getDescription()))) || _isDescNull)
            return true;
        return false;
    }

    private Object _tag;

    /**
     * Retorna el objeto adjunto al &#237;tem
     *
     * @return {Object}
     */
    public Object getTag() {
        return _tag;
    }

    /**
     * Instancia un objeto adjunto al &#237;
     *
     * @param tag - Objeto adjunto al &#237;tem
     */
    public void setTag(Object tag) {
        this._tag = tag;
    }

    private LongSparseArray<Object> _mapTag;

    /**
     * Retorna un objeto adjunto al &#237;tem identificado con una clave
     *
     * @param key - Clave del objeto adjunto al &#237;tem
     * @return {Object}
     */
    public Object getTag(long key) {
        return _tag;
    }

    /**
     * Instancia un objeto adjunto al &#237;tem identific&#225;ndolo con una clave
     *
     * @param key - Clave del objeto adjunto al &#237;tem
     * @param tag - Objeto adjunto al &#237;tem
     */
    public void setTag(long key, Object tag) {
        if (_mapTag == null)
            _mapTag = new LongSparseArray<Object>(); // HashMap<Long, Object>();
        _mapTag.put(key, tag);
    }
}
