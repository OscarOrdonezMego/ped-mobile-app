package pe.com.nextel.android.actividad;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import pe.com.nextel.android.R;
import pe.com.nextel.android.util.CameraSurface;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTheme;
import pe.com.nextel.android.util.CustomComparator;
import pe.com.nextel.android.util.NexActivityGestor;
import pe.com.nextel.android.util.Utilitario;

/**
 * Actividad de toma de fotos.
 * </br>Se debe declarar en el <b>AndroidManifest.xml</b> la siguiente l&#237;nea:
 * </br></br>&#60;activity android:name="pe.com.nextel.android.actividad.NexCameraActivity" android:configChanges="orientation"/&#62;
 *
 * @author jroeder
 */
public final class NexCameraActivity extends Activity {

    public static final String URI_PICTURE = "uriPicture";
    public static final String URI_SAVED = "uriGrabado";
    public static final String BUNDLE_RESULT = "result";
    private Camera ioCamera;
    private CameraSurface ioCameraSurface;
    private RelativeLayout ioLayout;
    private FrameLayout ioFrame;
    private Button ioButton;
    private Button ioButtonConfig;

    private SharedPreferences ioSharedPrefs;
    private static Uri ioUriPicture;
    private static int iiAlturaFoto;
    private static int iiAnchoFoto;
    private static boolean ibFlashSoportado = false;
    private static boolean ibAutoFocusSoportado = false;
    private static String isFlash;
    private static String isFocusMode;
    private static String[] ioLstSizes = null;
    private static String[] ioLstFlash = null;
    private static String[] ioLstAutoFocus = null;
    private static final int MEDIA_TYPE_IMAGE = 1;
    private static final int CONSMENCONFIGURACION = 0;
    private static String maxResolucion = null;
    private static String minResolucion = null;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NexActivityGestor.getInstance().finishActivity(this);
    }

    /**
     * Retorna el id de recurso del estilo del tema a utilizar en la actividad
     *
     * @return <b>int</b>
     */
    protected int getThemeResource() {
        return EnumTheme.BLUE.getResourceStyle(this);
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("XXX", "NexCameraActivity -> onCreate");
        NexActivityGestor.getInstance().addActivity(this);
        setTheme(getThemeResource());

        Log.v("CAMERA", "onCreate");
        Properties loProperties = Utilitario.readProperties(NexCameraActivity.this);
        maxResolucion = loProperties.getProperty("MAXRESOLUCION");
        minResolucion = loProperties.getProperty("MINRESOLUCION");
        Log.i("XXX", "NexCameraActivity -> maxResolucion : " + maxResolucion);
        Log.i("XXX", "NexCameraActivity -> minResolucion : " + minResolucion);
        Bundle loBundle = getIntent().getExtras();
        ioUriPicture = loBundle.getParcelable(URI_PICTURE);
        ioSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        subSetCameraParams();
        subSetControles();

        getLastNonConfigurationInstance();
    }

    private void subSetCameraParams() {
        Log.i("XXX", "NexCameraActivity -> subSetCameraParams");
        PackageManager pm = getPackageManager();
        ibFlashSoportado = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        ibAutoFocusSoportado = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS);

        String lsResolucion = ioSharedPrefs.getString(getString(R.string.keypre_resolucion), getString(R.string.resoluciondefecto));
        String[] anchoAlto = lsResolucion.split("x");
        iiAnchoFoto = Integer.parseInt(anchoAlto[0]);
        iiAlturaFoto = Integer.parseInt(anchoAlto[1]);

        Log.i("XXX", "NexCameraActivity -> iiAnchoFoto : " + iiAnchoFoto);
        Log.i("XXX", "NexCameraActivity -> iiAlturaFoto : " + iiAlturaFoto);

        isFocusMode = ioSharedPrefs.getString(getString(R.string.keypre_autofocus), fnAutoFocusDefecto());
        isFlash = ioSharedPrefs.getString(getString(R.string.keypre_flash), fnFlashDefecto());
    }

    @SuppressWarnings("deprecation")
    private void subSetControles() {
        Log.i("XXX", "NexCameraActivity -> subSetControles");
        ioLayout = new RelativeLayout(this);
        ioFrame = new FrameLayout(this);
        ioButton = new Button(this);
        ioButtonConfig = new Button(this);
        // ioButton.setBackgroundColor(Color.WHITE);
        ioButton.setBackgroundResource(R.drawable.btn_camera_shutter);
        ioButtonConfig.setBackgroundResource(R.drawable.gd_action_bar_settings);
        ioButton.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT));
        ioButtonConfig.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));

        subSetCameraFrame();

        ioButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ioCamera.takePicture(null, null, ioPicture);
                ioButton.setEnabled(false);
            }
        });

        ioButtonConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configurarCamara();
            }
        });

        ioFrame.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ioCamera.takePicture(null, null, ioPicture);
                return true;
            }
        });
        ioFrame.setLayoutParams(new FrameLayout.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, Gravity.CENTER));

        ioButton.setGravity(Gravity.CENTER);
        RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        relativeParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        relativeParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        ioButton.setLayoutParams(relativeParams);

        ioButtonConfig.setGravity(Gravity.CENTER);
        RelativeLayout.LayoutParams relativeParamsConfig = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        relativeParamsConfig.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        relativeParamsConfig.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        relativeParamsConfig.setMargins(15, 15, 15, 15);
        ioButtonConfig.setLayoutParams(relativeParamsConfig);

        ioLayout.setLayoutParams(new FrameLayout.LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, Gravity.CENTER));
        ioLayout.addView(ioFrame);
        ioLayout.addView(ioButton);
        ioLayout.addView(ioButtonConfig);
        this.setContentView(ioLayout);
    }

    private void subSetCameraFrame() {
        Log.i("XXX", "NexCameraActivity -> subSetCameraFrame");
        ioCamera = getCameraInstance();
        if (ioCamera != null) {
            ioCameraSurface = new CameraSurface(this, ioCamera);
            ioFrame.addView(ioCameraSurface);
        }
    }

    private static void subSetSupportedParams(Parameters p) {
        Log.i("XXX", "NexCameraActivity -> subSetSupportedParams");
        List<Size> loSizes = p.getSupportedPictureSizes();

        Collections.sort(loSizes, new CustomComparator());

        String[] loArrSizes = new String[loSizes.size()];
        Size loSize = null;
        int j = 0;

        for (int i = 0; i < loSizes.size(); i++) {
            loSize = loSizes.get(i);
            Integer res = loSize.width * loSize.height;

            if ((res < Integer.parseInt(maxResolucion)) &&
                    (res > Integer.parseInt(minResolucion))) {
                loArrSizes[i] = loSize.width + "x" + loSize.height;
                j++;
            }
        }

        ioLstSizes = new String[j];
        j = 0;
        for (int i = 0; i < loSizes.size(); i++) {
            loSize = loSizes.get(i);
            if (loSize.width * loSize.height < Integer.parseInt(maxResolucion) &&
                    loSize.width * loSize.height > Integer.parseInt(minResolucion)) {
                if (j == ioLstSizes.length - 1 && iiAnchoFoto == 0 && iiAlturaFoto == 0) {
                    iiAnchoFoto = loSize.width;
                    iiAlturaFoto = loSize.height;
                }
                ioLstSizes[j] = loSize.width + "x" + loSize.height;
                Log.i("XXX", "NexCameraActivity -> ioLstSizes[j] : " + loSize.width + "x" + loSize.height);
                j++;
            }
        }

        //ioLstSizes = loArrSizes;

        if (ibFlashSoportado) {
            List<String> loFlashModes = p.getSupportedFlashModes();
            String[] loArrFlash = new String[loFlashModes.size()];
            for (int i = 0; i < loFlashModes.size(); i++) {
                loArrFlash[i] = loFlashModes.get(i);
            }
            ioLstFlash = loArrFlash;
        }

        if (ibAutoFocusSoportado) {
            List<String> loAutoFocusModes = p.getSupportedFocusModes();
            String[] loArrFocus = new String[loAutoFocusModes.size()];
            for (int i = 0; i < loAutoFocusModes.size(); i++) {
                loArrFocus[i] = loAutoFocusModes.get(i);
            }
            ioLstAutoFocus = loArrFocus;
        }
    }

    private String fnFlashDefecto() {
        if (ibFlashSoportado) {
            return getString(R.string.flashdefecto);
        } else {
            return "no";
        }
    }

    private String fnAutoFocusDefecto() {
        if (ibAutoFocusSoportado) {
            return getString(R.string.autofocodefecto);
        } else {
            return "no";
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.e("CAMARA", "Config habilitada: " + ioSharedPrefs.getBoolean(getString(R.string.keypre_configcamara), false));
        if (ioSharedPrefs.getBoolean(getString(R.string.keypre_configcamara), false)) {
            menu.add(0, CONSMENCONFIGURACION, 0, getString(R.string.keypremenu_configcamara)).setIcon(
                    android.R.drawable.ic_menu_preferences);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case CONSMENCONFIGURACION:
                configurarCamara();
                break;
            default:
                break;
        }
        return false;
    }

    private void configurarCamara() {
        Intent loIntent = new Intent(this, NexCameraConfigActivity.class);
        Bundle loBundle = new Bundle();
        loBundle.putStringArray("supportedRes", ioLstSizes);
        loBundle.putStringArray("supportedFlash", ioLstFlash);
        loBundle.putStringArray("supportedAutoFocus", ioLstAutoFocus);
        loIntent.putExtras(loBundle);
        // finish();
        startActivity(loIntent);
    }

    /**
     * Retorna una instancia de la clase {@link Camera}
     *
     * @return {@link Camera}
     */
    private static Camera getCameraInstance() {
        Log.i("XXX", "NexCameraActivity -> getCameraInstance");
        Camera c = null;
        try {
            c = Camera.open();
            Log.e("CAMARA", "CAMERA.OPEN");
            Parameters loParams = c.getParameters();
            subSetSupportedParams(loParams);
            if (ibFlashSoportado)
                loParams.setFlashMode(isFlash);
            Log.e("CAMARA", "SETEANDO FLASH MODE: " + isFlash);

            Log.i("XXX", "NexCameraActivity -> iiAnchoFoto : " + iiAnchoFoto);
            Log.i("XXX", "NexCameraActivity -> iiAlturaFoto : "+ iiAlturaFoto);

            loParams.setPictureSize(iiAnchoFoto, iiAlturaFoto);
            Log.e("CAMARA", "SETEANDO RESOLUCION: " + iiAnchoFoto + "x"
                    + iiAlturaFoto);
            if (ibAutoFocusSoportado)
                loParams.setFocusMode(isFocusMode);
            Log.e("CAMARA", "SETEANDO FOCUS MODE: " + isFocusMode);

            c.setParameters(loParams);

        } catch (Exception e) {
            Log.e("CAMARA", "Camera no disponible o no existe");
        }
        return c;
    }

    private PictureCallback ioPicture = new PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            if (pictureFile == null) {
                Log.d("CAMARA", "Error creating media file, check storage permissions");
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                Log.d("CAMARA", "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d("CAMARA", "Error accessing file: " + e.getMessage());
            }

            Bundle loBundleRetorno = new Bundle();
            loBundleRetorno.putString(URI_SAVED, ioUriPicture.getPath());
            Intent resultIntent = new Intent();
            resultIntent.putExtra(BUNDLE_RESULT, loBundleRetorno);
            setResult(RESULT_OK, resultIntent);
            finish();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("CAMARA", "onResume");
        if (ioCamera != null) {
            ioCamera.setPreviewCallback(null);
        }
        if (ioCamera == null) {
            subSetCameraParams();
            ioCamera = getCameraInstance();

        }
        if (ioCamera != null) {
            // solo si esta regresando
            ioCameraSurface = new CameraSurface(this, ioCamera);
            ioFrame.removeAllViews();
            ioFrame.addView(ioCameraSurface);
            // ioFrame.addView(ioButton);
        }

        ioButton.setEnabled(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.v("CAMARA", "onPause");
        releaseCamera();
    }

    private void releaseCamera() {
        if (ioCamera != null) {
            ioCamera.release();
            ioCamera = null;
            Log.v("CAMARA", "release the camera for other applications");
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private static File getOutputMediaFile(int type) {
        // crea el archivo en el URI especificado
        File loFile = new File(ioUriPicture.getPath());
        return loFile;
    }

    private static NexCameraConfiguration obtainNexCameraConfiguration(Context context) {
        Log.i("XXX", "NexCameraActivity -> obtainNexCameraConfiguration");
        if (maxResolucion == null || minResolucion == null) {
            Properties loProperties = Utilitario.readProperties(context);
            maxResolucion = loProperties.getProperty("MAXRESOLUCION");
            minResolucion = loProperties.getProperty("MINRESOLUCION");
            Log.i("XXX", "NexCameraActivity -> maxResolucion : " + maxResolucion);
            Log.i("XXX", "NexCameraActivity -> minResolucion : " + minResolucion);
        }

        PackageManager pm = context.getPackageManager();
        ibFlashSoportado = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        ibAutoFocusSoportado = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS);

        NexCameraConfiguration loConfiguration = new NexCameraConfiguration();
        loConfiguration.setCamera(getCameraInstance());
        loConfiguration.setSupportedFlash(ibFlashSoportado);
        loConfiguration.setSupportedAutoFocus(ibAutoFocusSoportado);
        loConfiguration.setFlash(ioLstFlash);
        loConfiguration.setSizes(ioLstSizes);
        loConfiguration.setAutoFocus(ioLstAutoFocus);
        loConfiguration.setMaxResolution(maxResolucion);
        loConfiguration.setMinResolution(minResolucion);
        return loConfiguration;
    }

    /**
     * Clase que contiene la configuraci&#243;n de c&#225;mara del equipo
     *
     * @author jroeder
     */
    public static class NexCameraConfiguration {
        private String[] ioLstSizes = null;
        private String[] ioLstFlash = null;
        private String[] ioLstAutoFocus = null;
        private boolean ibFlashSoportado = false;
        private boolean ibAutoFocusSoportado = false;
        private String maxResolucion;
        private String minResolucion;
        private Camera camera;

        private NexCameraConfiguration() {
        }

        /**
         * Retorna una instancia con la configuraci&#243;n de c&#225;mara del equipo
         *
         * @return {@link NexCameraConfiguration}
         */
        public static NexCameraConfiguration getInstance(Context context) {
            return obtainNexCameraConfiguration(context);
        }

        /**
         * Retorna un arreglo con las resoluciones disponibles en el equipo
         *
         * @return {String}[]
         */
        public String[] getSizes() {
            return ioLstSizes;
        }

        /**
         * Retorna un arreglo con los tipos de flash disponibles en el equipo
         *
         * @return {String}[]
         */
        public String[] getFlash() {
            return ioLstFlash;
        }

        /**
         * Retorna un arreglo con los tipos de Auto Focus disponibles en el equipo
         *
         * @return {String}[]
         */
        public String[] getAutoFocus() {
            return ioLstAutoFocus;
        }

        /**
         * Retorna si el equipo soporta Flash
         *
         * @return boolean
         */
        public boolean isSupportedFlash() {
            return ibFlashSoportado;
        }

        /**
         * Retorna si el equipo soporta Auto Focus
         *
         * @return <b>boolean</b>
         * <ul>
         * <li><b>true</b> si soporta auto focus</li>
         * <li><b>false</b> si no soporta auto focus</li>
         * </ul>
         */
        public boolean isSupportedAutoFocus() {
            return ibAutoFocusSoportado;
        }

        /**
         * Retorna la m&#225;xima resoluci&#243;n inscrita en el par&#225;metro <b>MAXRESOLUCION</b> del <b>Assets</b>
         *
         * @return {String}
         */
        public String getMaxResolution() {
            return maxResolucion;
        }

        /**
         * Retorna la m&#237;nima resoluci&#243;n inscrita en el par&#225;metro <b>MINRESOLUCION</b> del <b>Assets</b>
         *
         * @return {String}
         */
        public String getMinResolution() {
            return minResolucion;
        }

        /**
         * Retorna una instancia de la clase {@link Camera} con las configuraciones disponibles
         *
         * @return {@link Camera}
         */
        public Camera getCamera() {
            return camera;
        }

        private void setSizes(String[] array) {
            this.ioLstSizes = array;
        }

        private void setFlash(String[] array) {
            this.ioLstFlash = array;
        }

        private void setAutoFocus(String[] array) {
            this.ioLstAutoFocus = array;
        }

        private void setSupportedFlash(boolean supported) {
            this.ibFlashSoportado = supported;
        }

        private void setSupportedAutoFocus(boolean supported) {
            this.ibAutoFocusSoportado = supported;
        }

        private void setMaxResolution(String resolution) {
            this.maxResolucion = resolution;
        }

        private void setMinResolution(String resolution) {
            this.minResolucion = resolution;
        }

        private void setCamera(Camera camera) {
            this.camera = camera;
        }
    }
}
