package pe.com.nextel.android.actividad;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import pe.com.nextel.android.R;
import pe.com.nextel.android.http.HttpActualizar;
import pe.com.nextel.android.http.HttpConexion;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumDialog;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumPreferenceKey;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumRolPreferencia;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumServerResponse;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTheme;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTipActividad;
import pe.com.nextel.android.util.ExportDatabaseFileTask;
import pe.com.nextel.android.util.NexActivityGestor;
import pe.com.nextel.android.util.SharedPreferenceGestor;
import pe.com.nextel.android.util.StateHolder;
import pe.com.nextel.android.util.Utilitario;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;
import pe.com.nextel.android.ws.ConfigurationServiceGestor.BeanConfiguration.EnumConfigurationKey;

/**
 * Actividad de preferencias. Lista las opciones de preferencias de administraci&#243;n de la aplicaci&#243;n
 *
 * @author jroeder
 */
public abstract class NexPreferenceActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {
    /**
     * Clase enumerable con los valores de Men&#250; de Preferencias
     * <ul>
     * <li>BACKUP: -1</li>
     * <li>ADMIN: -2</li>
     * </ul>
     *
     * @author jroeder
     */
    private static enum EnumPreferenceMenu {
        BACKUP(-1),
        ADMIN(-2),
        THEME(-3);

        private static final Map<Integer, EnumPreferenceMenu> lookup = new HashMap<Integer, EnumPreferenceMenu>();

        static {
            for (EnumPreferenceMenu loEnum : EnumSet.allOf(EnumPreferenceMenu.class)) {
                lookup.put(loEnum.getValue(), loEnum);
            }
        }

        private int value;

        private EnumPreferenceMenu(int value) {
            this.value = value;
        }

        /**
         * Obtiene el valor del enumerable
         *
         * @return int Valor del enumerable
         */
        public int getValue() {
            return value;
        }

        /**
         * Obtiene el enumerable dado un valor. Si el valor no coincide, retorna <b>null</b>
         *
         * @param value Valor del enumerable
         * @return {@link EnumPreferenceMenu} Enumerable
         */
        public static EnumPreferenceMenu get(int value) {
            return lookup.get(value);
        }

        /**
         * Obtiene el enumerable dado el nombre del enumerable. Si el nombre no coincide, retorna null
         *
         * @param name - nombre del enumerable
         * @return EnumPreferenceMenu - Enumerable
         */

        public static EnumPreferenceMenu get(String name) {
            for (EnumPreferenceMenu loEnum : lookup.values()) {
                if (loEnum.toString().equals(name))
                    return loEnum;
            }
            return null;
        }
    }

    /**
     * Nombre clave del c&#243;digo de petici&#243;n cuando la actividad es invocada por un {@link #startActivityForResult}</br>
     * REQUESTCODE: {@value}
     */
    public final static String REQUESTCODE = "requestCode";
    private EnumRolPreferencia ioEnumRolPreferencia = EnumRolPreferencia.USUARIO;

    private boolean ibThemeConnection = false;

    /**
     * Retorna el id de recurso del estilo del tema a utilizar en la actividad
     *
     * @return <b>int</b>
     */
    protected int getThemeResource() {
        return EnumTheme.getCurrentTheme(this).getResourceStyle(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        restoreInstanceState(savedInstanceState);
        super.onCreate(savedInstanceState);
        NexActivityGestor.getInstance().addActivity(NexPreferenceActivity.this);

        setTheme(getThemeResource());

        //if(!Utilitario.fnVerOperador(this))
        //	startActivity(new Intent(this, NexInvalidOperatorActivity.class));

        addPreferencesFromResource(getNexPreferenceResourceId());
        getListView().setBackgroundColor(Color.rgb(0, 0, 0));
        setNexPreferenceControls();
        Object loObject = getLastNonConfigurationInstance();
        if (loObject != null) {
            if (loObject instanceof HttpActualizar) {
                setHttpActualizar((HttpActualizar) loObject);
                getHttpActualizar().attach(this);
            } else if (loObject instanceof HttpConexion) {
                setHttpConexion((HttpConexion) loObject);
                getHttpConexion().attach(this);
            } else if (loObject instanceof StateHolder) {
                subSetLastNexStateHolderNonConfigInstance((StateHolder) loObject);
            } else {
                subSetLastNexNonConfigurationInstance(loObject);
            }
        }
    }

    /**
     * Inicializa valores del &#250;ltimo {@link StateHolder} configurado
     *
     * @param poStateHolder {@link StateHolder} obtenido del {@link #getLastNonConfigurationInstance}
     */
    protected void subSetLastNexStateHolderNonConfigInstance(StateHolder poStateHolder) {
        Log.v("PRO", this.getClass().getName() + "=>Sobreescribir metodo subSetLastNexStateHolderNonConfigInstance()");
    }

    /**
     * Inicializa valores del objeto obtenido del {@link #getLastNonConfigurationInstance}
     *
     * @param poObject Objeto obtenido del {@link #getLastNonConfigurationInstance}
     */
    protected void subSetLastNexNonConfigurationInstance(Object poObject) {
        Log.v("PRO", this.getClass().getName() + "=>Sobreescribir metodo subSetLastNexNonConfigurationInstance()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NexActivityGestor.getInstance().finishActivity(this);
    }

    /**
     * Retorna a la actividad anterior. Realiza uno de los siguientes casos:
     * <ul>
     * <li>Si el {@link Intent} que invoca a la actividad tiene el nombre clave {@link #REQUESTCODE}, utiliza el m&#233;todo {@link #setResult}</li>.
     * <li>Si existe una actividad que retorna en el m&#233;todo {@link #onBackActivity}, utiliza un {@link #startActivity} a la actividad obtenida.
     * <li>Si no coinciden los casos anteriores, se realiza la acci&#243;n por defecto del {@link #onBackPressed}
     * </ul>
     */
    @Override
    public void onBackPressed() {
        if (getIntent().hasExtra(REQUESTCODE)) {
            setResult(RESULT_OK);
        } else {
            if (onBackActivity() != null)
                startActivity(new Intent(this, onBackActivity()));
            else
                super.onBackPressed();
        }
        finish();
    }

    /**
     * Retorna el tipo de actividad {@link EnumTipActividad#NEXPREFERENCEACTIVITY NEXPREFERENCEACTIVITY}
     *
     * @return {@link EnumTipActividad}
     */
    public EnumTipActividad fnTipactividad() {
        return EnumTipActividad.NEXPREFERENCEACTIVITY;
    }

    /**
     * Retorna la actividad de retorna al presionar {@link #onBackPressed}
     *
     * @return {Class}&#60;&#63;&#62; Clase de actividad de retorno
     */
    protected Class<?> onBackActivity() {
        return null;
    }

    /**
     * Retorna el Id de recurso de preferencias
     *
     * @return <b>int</b> Id de recurso de preferencias
     */
    protected abstract int getNexPreferenceResourceId();

    /**
     * Implementa la inicializaci&#243;n de los controles de la actividad.</br>
     * En este m&#233;todo se debe de instanciar o inicializar los controles del {@link PreferenceActivity}
     */
    protected abstract void setNexPreferenceControls();

    /**
     * Utilizado para verificar el c&#243;digo y clave de usuario administrador de preferencias.
     * Este m&#233;todo se debe de implementar en la clase hija.</br>
     *
     * @param psAdminUser C&#243;digo de usuario administrador
     * @param psAdminPass Clave de usuario administrador
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si es satisfactoria la verificaci&#243;n.</li>
     * <li><b>false</b> de lo contrario.</li>
     * </ul>
     */
    protected boolean OnVerifyAdministrator(String psAdminUser, String psAdminPass) {
        return false;
    }

    /**
     * Retorna el nombre de la base de datos sqlite.</br>
     * Este m&#233;todo se debe de implementar en la clase hija.</br>
     *
     * @return {String} Nombre de la base de datos sqlite.
     */
    protected String getDatabaseName() {
        return null;
    }

    /**
     * Retorna la clave de verificaci&#243;n para obtener el backup sqlite.</br>
     * Por defecto obtiene el valor de <b>R.string.Backup</b>.</br>
     * Este m&#233;todo puede set sobrecargado en la clase hija.
     *
     * @return {String} - Clave de verificaci&#243;n de backup
     */
    protected String getBackupKey() {
        return getString(R.string.Backup);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, EnumPreferenceMenu.ADMIN.getValue(), 0, getString(R.string.nexpreferences_administrador)).setIcon(android.R.drawable.ic_lock_lock);
        menu.add(0, EnumPreferenceMenu.BACKUP.getValue(), 0, getString(R.string.nexpreferences_backup)).setIcon(android.R.drawable.stat_sys_download_done);
        try {
//	  		if(NexDate.Compare(NexDate.getCurrentStringDate(FORMAT.MILITAR_DATE), FORMAT.MILITAR_DATE, 
//	  						EnumTheme.getThemeEffectiveDate(this), FORMAT.MILITAR_DATE) >= 0)
//	  			menu.add(0, EnumPreferenceMenu.THEME.getValue(), 0, getString(R.string.nexpreferences_sinc_tema)).setIcon(android.R.drawable.ic_popup_sync);
            if (SharedPreferenceGestor.fnExiSharedPrefer(this, EnumPreferenceKey.THEME, EnumConfigurationKey.THEME))
                menu.add(0, EnumPreferenceMenu.THEME.getValue(), 0, getString(R.string.nexpreferences_sinc_tema)).setIcon(android.R.drawable.ic_popup_sync);
        } catch (Exception e) {
            Log.e("PRO", "NexPreferenceActivity.onCreateOptionsMenu", e);
        }
        return true;
    }

    /**
     * Crear la opci&#243;n de Verificar Usuario Administrador
     *
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si crea la opci&#243;n.</li>
     * <li><b>false</b> de los contrario.</li>
     * </ul>
     */
    protected boolean OnPreVerifyAdministrator() {
        return true;
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        try {
            if (OnPreVerifyAdministrator()) {
                if (ioEnumRolPreferencia == EnumRolPreferencia.ADMINISTRADOR) {
                    menu.removeItem(EnumPreferenceMenu.ADMIN.getValue());
                }
            } else {
                menu.removeItem(EnumPreferenceMenu.ADMIN.getValue());
            }
        } catch (Exception e) {
            Log.e("PRO", "onMenuOpened", e);
        }
        menu.removeItem(EnumPreferenceMenu.BACKUP.getValue());
        if (getDatabaseName() != null)
            if (!getDatabaseName().equals(""))
                menu.add(0, EnumPreferenceMenu.BACKUP.getValue(), 0, getString(R.string.nexpreferences_backup)).setIcon(android.R.drawable.stat_sys_download_done);
        return super.onMenuOpened(featureId, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (EnumPreferenceMenu.get(item.getItemId())) {
            case BACKUP:
                try {
                    showDialog(EnumDialog.BACKUP.getValue());
                } catch (Exception e) {
                    subShowException(e);
                }
                break;
            case ADMIN:
                try {
                    showDialog(EnumDialog.ADMIN.getValue());
                } catch (Exception e) {
                    subShowException(e);
                }
                break;
            case THEME:
                try {
                    ibThemeConnection = true;
                    EnumTheme.obtainThemeFromServer(this, false);
                } catch (Exception e) {
                    subShowException(e);
                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return false;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        EnumDialog loId = EnumDialog.get(id);
        if (loId != null) {
            switch (loId) {
                case EXCEPTION:
                    final AlertDialog loAlertDialogError = new AlertDialog.Builder(this).create();
                    loAlertDialogError.setCancelable(false);
                    loAlertDialogError.setButton(getString(R.string.dlg_btnok), new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            loAlertDialogError.dismiss();
                            subMakeCaseError(getCaseError());
                        }
                    });
                    loAlertDialogError.setTitle(getString(R.string.dlg_titerror));
                    loAlertDialogError.setMessage(isMessageException);
                    loAlertDialogError.setIcon(android.R.drawable.ic_dialog_alert);
                    return loAlertDialogError;
                case PROGRESS:
                    final ProgressDialog loProgressDialog = new ProgressDialog(this);
                    loProgressDialog.setMessage(ioPressDialogMessage);
                    loProgressDialog.setCancelable(false);
                    return loProgressDialog;
                case ADMIN:
                    final AlertDialog.Builder loAlertAdministrador = new AlertDialog.Builder(NexPreferenceActivity.this);
                    loAlertAdministrador.setTitle(getString(R.string.nexpreferences_dlgadministrador));
                    loAlertAdministrador.setIcon(android.R.drawable.ic_lock_lock);

                    final LinearLayout loLayoutAdm = new LinearLayout(NexPreferenceActivity.this);
                    loLayoutAdm.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                    loLayoutAdm.setOrientation(LinearLayout.VERTICAL);

                    final EditText loEditTextAdmCod = new EditText(NexPreferenceActivity.this);
                    loEditTextAdmCod.setHint(getString(R.string.nexpreferences_hintadmcodigo));
                    //	    		loEditTextCodBackup.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                    loEditTextAdmCod.setInputType(InputType.TYPE_CLASS_NUMBER);
                    loEditTextAdmCod.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

                    final EditText loEditTextAdmPass = new EditText(NexPreferenceActivity.this);
                    loEditTextAdmPass.setHint(getString(R.string.nexpreferences_hintadmpass));
                    loEditTextAdmPass.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    loEditTextAdmPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    loEditTextAdmPass.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

                    loLayoutAdm.addView(loEditTextAdmCod);
                    loLayoutAdm.addView(loEditTextAdmPass);
                    loAlertAdministrador.setView(loLayoutAdm);
                    loAlertAdministrador.setPositiveButton(getString(R.string.dlg_btnok), new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            try {
                                if (OnVerifyAdministrator(loEditTextAdmCod.getText().toString(), loEditTextAdmPass.getText().toString())) {
                                    ioEnumRolPreferencia = EnumRolPreferencia.ADMINISTRADOR;
                                }
                                dismissDialog(EnumDialog.ADMIN.getValue());
                            } catch (Exception e) {
                                subShowException(e);
                            }
                        }
                    });
                    loAlertAdministrador.setNegativeButton(getString(R.string.act_btncancelar), new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            dismissDialog(EnumDialog.ADMIN.getValue());
                        }
                    });

                    return loAlertAdministrador.create();
                case BACKUP:
                    final AlertDialog.Builder loAlertBackup = new AlertDialog.Builder(NexPreferenceActivity.this);
                    loAlertBackup.setTitle(getString(R.string.nexpreferences_dlgbackup));
                    loAlertBackup.setIcon(android.R.drawable.stat_sys_download_done);
                    final EditText loEditTextCodBackup = new EditText(NexPreferenceActivity.this);
                    loEditTextCodBackup.setHint(getString(R.string.nexpreferences_hintbackuppassword));
                    //	    		loEditTextCodBackup.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                    loEditTextCodBackup.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    loEditTextCodBackup.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    loEditTextCodBackup.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
                    loAlertBackup.setView(loEditTextCodBackup);
                    loAlertBackup.setPositiveButton(getString(R.string.dlg_btnok), new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            try {
                                if (loEditTextCodBackup.getText().toString().equals(getBackupKey())) {
                                    new ExportDatabaseFileTask(NexPreferenceActivity.this, getDatabaseName()).execute();
                                } else {
                                    //								Toast loToast = Toast.makeText(NexPreferenceActivity.this, getString(R.string.nexpreferences_toabackuppasswordincorrecto), Toast.LENGTH_LONG);
                                    //								loToast.setGravity(Gravity.CENTER|Gravity.CENTER_VERTICAL, 0, 0);
                                    //								loToast.show();
                                    new NexToast(NexPreferenceActivity.this, R.string.nexpreferences_toabackuppasswordincorrecto, EnumType.CANCEL).show();
                                }
                                dismissDialog(EnumDialog.BACKUP.getValue());
                            } catch (Exception e) {
                                subShowException(e);
                            }
                        }
                    });
                    loAlertBackup.setNegativeButton(getString(R.string.act_btncancelar), new OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {
                            dismissDialog(EnumDialog.BACKUP.getValue());
                        }
                    });

                    return loAlertBackup.create();
                default:
                    return super.onCreateDialog(id);
            }
        } else {
            return super.onCreateDialog(id);
        }
    }

    @Override
    protected void onPrepareDialog(int id, Dialog dialog, Bundle args) {
        EnumDialog loId = EnumDialog.get(id);
        if (loId != null) {
            switch (loId) {
                case EXCEPTION:
                    AlertDialog loAlert = (AlertDialog) dialog;
                    loAlert.setMessage(isMessageException);
                    break;
                case PROGRESS:
                    ProgressDialog loProgress = (ProgressDialog) dialog;
                    loProgress.setMessage(ioPressDialogMessage);
                    break;
                default:
                    super.onPrepareDialog(id, dialog, args);
                    break;
            }
        } else {
            super.onPrepareDialog(id, dialog, args);
        }
    }

    /**
     * Variable global para Caso de error por defecto
     */
    public static final int CONSCASEERROR = -1;
    /**
     * Instancia del mensaje de la excepci&#243;n
     */
    protected String isMessageException = "";
    /**
     * Instancia de Caso de error
     */
    private int iiCaseError = 0;

    /**
     * Retorna la instancia de caso de error
     *
     * @return int - Caso de error
     */
    private int getCaseError() {
        return iiCaseError;
    }

    /**
     * Instancia el caso de error
     *
     * @param piCaseError - Caso de error
     */
    private void setCaseError(int piCaseError) {
        this.iiCaseError = piCaseError;
    }

    private String httpResultado;
    private int idHttpResultado;

    /**
     * Retorna el mensaje de resultado de la conexi&#243;n
     *
     * @return {String}
     */
    public String getHttpResultado() {
        return httpResultado;
    }

    /**
     * Instancia el mensaje de resultado de la conexi&#243;n
     *
     * @param psHttpResultado Mensaje de resultado
     */
    public void setHttpResultado(String psHttpResultado) {
        this.httpResultado = psHttpResultado;
    }

    /**
     * Instancia el Id de resultado de la conexi&#243;n
     *
     * @param idHttpResultado Id de resultado
     * @deprecated Usar el m&#233;todo {@link #setEnumIdHttpResultado}
     */
    public void setIdHttpResultado(int idHttpResultado) {
        this.idHttpResultado = idHttpResultado;
    }

    /**
     * Instancia el Id de resultado de la conexi&#243;n
     *
     * @param idHttpResultado Id de resultado
     */
    public void setEnumIdHttpResultado(EnumServerResponse idHttpResultado) {
        this.idHttpResultado = idHttpResultado.getValue();
    }

    /**
     * Retorna el Id de resultado de la conexi&#243;n
     *
     * @return int
     * @deprecated Usar el m&#243;todo {@link #getEnumIdHttpResultado}
     */
    public int getIdHttpResultado() {
        return idHttpResultado;
    }

    /**
     * Retorna el Id de resultado de la conexi&#243;n
     *
     * @return {@link EnumServerResponse}
     */
    public EnumServerResponse getEnumIdHttpResultado() {
        return EnumServerResponse.get(idHttpResultado);
    }

    private String msgOk = "";
    private String msgError = "";

    /**
     * Instancia el t&#237tulo de di&#225;logo de conexi&#243;n satisfactoria
     *
     * @param msgOk T&#237;tulo de di&#225;logo
     */
    public void setMsgOk(String msgOk) {
        this.msgOk = msgOk;
    }

    /**
     * Retorna el t&#237;tulo de di&#225;logo de conexi&#243;n satisfactoria
     *
     * @return {String}
     */
    public String getMsgOk() {
        return msgOk;
    }

    /**
     * Instancia el t&#237;tulo de di&#225;logo de conexi&#243;n con errores
     *
     * @param msgError T&#237;tulo de di&#225;logo
     */
    public void setMsgError(String msgError) {
        this.msgError = msgError;
    }

    /**
     * Instancia el t&#237;tulo de di&#225;logo de conexi&#243;n con errores
     *
     * @return {String}
     */
    public String getMsgError() {
        return msgError;
    }

    private AlertDialog ioAlertDialog;

    /**
     * Realiza la acci&#243;n posterior al mensaje de respuesta de conexi&#243;n
     */
    protected void subAccDesMensaje() {
        Log.v("PRO", this.getClass().getName() + "=>Sobreescribir metodo subAccDesMensaje()");
    }

    private void subAccDesMensajeThemeConnection() {
        ibThemeConnection = false;
        NexActivityGestor.getInstance().restartApplication(this);
        finish();
    }

    /**
     * Invoca el m&#233;todo {@link #subAccDesMensaje} que se ejecuta luego de mostrar un di&#225;logo con el mensaje de respuesta de la conexi&#243;n
     */
    public void subHttpResultado() {
        ioAlertDialog = new AlertDialog.Builder(this).create();
        ioAlertDialog.setCancelable(false);
        ioAlertDialog.setButton(getString(R.string.dlg_btnok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                subAccDesMensaje();
            }
        });
        switch (getEnumIdHttpResultado()) {
            case OK:
                ioAlertDialog.setTitle(getMsgOk());
                ioAlertDialog.setMessage(getHttpResultado());
                ioAlertDialog.setIcon(android.R.drawable.ic_dialog_info);
                break;
            case ERROR:
                ioAlertDialog.setTitle(getMsgError());
                ioAlertDialog.setMessage(getHttpResultado());
                ioAlertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                break;
            case OKNOMSG:
                if (ibThemeConnection)
                    subAccDesMensajeThemeConnection();
                else
                    subAccDesMensaje();
                return;
        }
        ioAlertDialog.show();
        ibThemeConnection = false;
    }

    /**
     * Secuencia de caracteres del Di&#225;logo de Progreso
     */
    private static CharSequence ioPressDialogMessage = null;

    /**
     * Objeto del tipo {@link HttpActualizar}
     */
    private HttpActualizar ioHttpActualizar = null;

    /**
     * Retorna el objeto de {@link HttpActualizar} de la actividad
     *
     * @return {@link HttpActualizar}
     */
    public HttpActualizar getHttpActualizar() {
        return ioHttpActualizar;
    }

    /**
     * Instancia el objeto {@link HttpActualizar} de la actividad
     *
     * @param poHttpActualizar Objeto del tipo {@link HttpActualizar}
     */
    public void setHttpActualizar(HttpActualizar poHttpActualizar) {
        this.ioHttpActualizar = poHttpActualizar;
    }

    /**
     * Objeto del tipo {@link HttpConexion}
     */
    private HttpConexion ioHttpConexion = null;

    /**
     * Retorna el objeto de {@link HttpConexion} de la actividad
     *
     * @return {@link HttpConexion}
     */
    public HttpConexion getHttpConexion() {
        return ioHttpConexion;
    }

    /**
     * Instancia el objeto {@link HttpConexion} de la actividad
     *
     * @param poHttpConexion Objeto del tipo {@link HttpConexion}
     */
    public void setHttpConexion(HttpConexion poHttpConexion) {
        this.ioHttpConexion = poHttpConexion;
    }

    @Override
    public Object onRetainNonConfigurationInstance() {
        if (getHttpActualizar() != null) {
            getHttpActualizar().detach();
            return getHttpActualizar();
        }
        if (getHttpConexion() != null) {
            getHttpConexion().detach();
            return getHttpConexion();
        }
        if (fnRetainNexStatHolderNonConfigInstance() != null) {
            fnRetainNexStatHolderNonConfigInstance().dettach(this);
            return fnRetainNexStatHolderNonConfigInstance();
        }
        return fnRetainNexNonConfigurationInstance();
    }

    /**
     * Guarda alg&#250;n objeto al momento de entrar en pausa o al destruirse la actividad
     *
     * @return {Object}
     */
    public Object fnRetainNexNonConfigurationInstance() {
        return null;
    }

    /**
     * Guarda un objeto del tipo {@link StateHolder} al momento de entrar en pausa o al destruirse la actividad
     *
     * @return {@link StateHolder}
     */
    public StateHolder fnRetainNexStatHolderNonConfigInstance() {
        return null;
    }

    /**
     * Invoca y muestra un di&#225;logo de progreso
     *
     * @param poMessage Secuencia de caracteres con el mensaje a mostrar en el di&#225;logo de progreso
     */
    public void showProgressDialog(CharSequence poMessage) {
        ioPressDialogMessage = poMessage;
        showDialog(EnumDialog.PROGRESS.getValue());
    }

    /**
     * Despliega el di&#225;logo de muestra de la excepci&#243;n
     *
     * @param poE Excepci&#243;n ocurrida
     */
    public void subShowException(Exception poE) {
        Log.e("PRO", "subShowException", poE);
        if (Utilitario.hasMethodInStackTrace("onCreate")) {
            postException = new Pair<Exception, Integer>(poE, CONSCASEERROR);
            return;
        }
        setCaseError(CONSCASEERROR);
        isMessageException = "" + poE;
        showDialog(EnumDialog.EXCEPTION.getValue());
    }

    /**
     * Despliega el di&#225;logo de muestra de la excepci&#243;n
     *
     * @param poE         Excepci&#243;n ocurrida
     * @param piCaseError Caso de error
     */
    public void subShowException(Exception poE, int piCaseError) {
        Log.e("PRO", "subShowException", poE);
        if (Utilitario.hasMethodInStackTrace("onCreate")) {
            postException = new Pair<Exception, Integer>(poE, piCaseError);
            return;
        }
        setCaseError(piCaseError);
        isMessageException = "" + poE;
        showDialog(EnumDialog.EXCEPTION.getValue());
    }

    /**
     * Es invocado cuando se cierra el di&#225;logo de excepciones
     *
     * @param piCaseError Caso de error
     */
    protected void subMakeCaseError(int piCaseError) {
        Log.v("PRO", this.getClass().getName() + "=>falta implementar 'protected void subMakeCaseError(int piCaseError){}'");
    }

    private static final String SAVEINSTANCE_PROCESS_LOCKED = "processLocked";
    private static final String SAVEINSTANCE_MESSAGE_OK = "save_msgok";
    private static final String SAVEINSTANCE_MESSAGE_ERROR = "save_msgerror";
    private boolean processLocked = false;

    /**
     * Verifica si se ha bloqueado un proceso por alg&#250;n evento realizado por el usuario.</br>
     * El programador es quien verifica en qu&#233; evento es necesario bloquear.
     *
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si verifica que est&#225; bloqueado</li>
     * <li><b>false</b> si verifica que no est&#225; bloqueado</li>
     * </ul>
     */
    public boolean isProcessLocked() {
        return processLocked;
    }

    /**
     * Bloquea el proceso.</br>
     * El programador es quien realiza el bloqueo.
     */
    public void lockProcess() {
        processLocked = true;
    }

    /**
     * Desbloquea el proceso.</br>
     * El programador es quien realiza el desbloqueo.
     */
    public void unlockProcess() {
        processLocked = false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("ibThemeConnection", ibThemeConnection);
        outState.putBoolean(SAVEINSTANCE_PROCESS_LOCKED, processLocked);
        outState.putString(SAVEINSTANCE_MESSAGE_OK, getMsgOk());
        outState.putString(SAVEINSTANCE_MESSAGE_ERROR, getMsgError());
        subSaveNexInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        ibThemeConnection = savedInstanceState.getBoolean("ibThemeConnection");
        processLocked = savedInstanceState.getBoolean(SAVEINSTANCE_PROCESS_LOCKED);
        setMsgOk(savedInstanceState.getString(SAVEINSTANCE_MESSAGE_OK));
        setMsgError(savedInstanceState.getString(SAVEINSTANCE_MESSAGE_ERROR));
        subRestoreNexInstanceState(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }

    /**
     * Invoca el m&#233;todo {@link #subShowException} en el m&#233;todo {@link #runOnUiThread} de la actividad
     *
     * @param e Excepci&#243;n ocurrida
     */
    public void subShowExptionOnRunUI(final Exception e) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                subShowException(e);
            }
        });
    }

    /**
     * Invoca el m&#233;todo {@link #subShowException} en el m&#233;todo {@link #runOnUiThread} de la actividad
     *
     * @param e         Excepci&#243;n ocurrida
     * @param caseError Caso de error
     */
    public void subShowExptionOnRunUI(final Exception e, final int caseError) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                subShowException(e, caseError);
            }
        });
    }

    /**
     * Restaura valores de la instancia salvada
     *
     * @param poBundle {@link Bundle} con datos de instancia salvada
     */
    protected void subRestoreNexInstanceState(Bundle poBundle) {
        Log.v("PRO", this.getClass().getName() + "=>Sobreescribir metodo subRestoreNexInstanceState()");
    }

    /**
     * Salva valores de instancia
     *
     * @param poBundle {@link Bundle} con datos de instancia
     */
    protected void subSaveNexInstanceState(Bundle poBundle) {
        Log.v("PRO", this.getClass().getName() + "=>Sobreescribir metodo subSaveNexInstanceState()");
    }

    /**
     * Restaura valores de instancia salvada
     *
     * @param poBundle {@link Bundle} con datos de instancia salvada
     */
    private void restoreInstanceState(Bundle poBundle) {
        if (poBundle != null) {
            subRestoreNexInstanceState(poBundle);
        }
    }

    private Pair<Exception, Integer> postException = null;

    /**
     * Invocar al m&#233;todo {@link #onPostResume} de la clase superior, cuando se realice un <b>@Override</b>
     */
    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (postException != null) {
            subShowException(postException.first, postException.second);
            postException = null;
        }
    }
}
