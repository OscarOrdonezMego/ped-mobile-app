package pe.com.nextel.android.util;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import pe.com.nextel.android.R;

/**
 * {@link DialogFragment} que muestra &#237;tems de un men&#250 de contexto
 *
 * @author jroeder
 */
public class ContextMenuFragment extends DialogFragment {

    /**
     * Variable global con el nombre adjunto por defecto del {@link ContextMenuFragment}
     */
    public static final String TAG = "loContextFragment";
    private static Menu ioMenu;

    /**
     * Crea una nuestra instancia del {@link ContextMenuFragment}
     *
     * @param poManager Administrador de {@link Fragment}
     * @param psTag     Nombre adjunto al {@link DialogFragment}
     * @param poMenu    {@link Menu} con la lista de &#237;tems
     * @return {@link ContextMenuFragment}
     */
    public static ContextMenuFragment newInstance(FragmentManager poManager, String psTag,
                                                  Menu poMenu) {
        ContextMenuFragment.subRemovePrevDialog(poManager, psTag);
        ContextMenuFragment loFrag = new ContextMenuFragment();
        ioMenu = poMenu;
        return loFrag;
    }

    /**
     * Adjunta un nuevo {@link MenuItem}
     *
     * @param _menuItem {@link MenuItem} con la informaci&#243;n del &#237tem
     */
    public void addItem(MenuItem _menuItem) {
        ioMenu.add(_menuItem.getGroupId(), _menuItem.getItemId(), _menuItem.getOrder(), _menuItem.getTitle());
        ioMenu.getItem(ioMenu.size() - 1).setIcon(_menuItem.getIcon());
    }

    @Override
    public void onResume() {
        getDialog().getWindow().setLayout(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        getDialog().getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        setStyle(ContextMenuFragment.STYLE_NO_FRAME, android.R.style.Theme);
        super.onResume();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog loDialog = super.onCreateDialog(savedInstanceState);
        loDialog.setCancelable(false);
        return loDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);

        View v = inflater.inflate(R.layout.contextfragment, container, true);
        ListView loLstOpciones = v
                .findViewById(R.id.contextfragment_listview);

        MarginLayoutParams loParams = (MarginLayoutParams) loLstOpciones.getLayoutParams();
        loParams.width = (int) (275 * Resources.getSystem().getDisplayMetrics().density);

        subCrearLista(loLstOpciones);
        return v;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        getActivity().onContextMenuClosed(ioMenu);
    }

    private void subCrearLista(ListView poView) {
        try {
            if (ioMenu != null) {
                ContextFragmentItemAdapter loMotivoRechazoAdapter = new ContextFragmentItemAdapter(
                        getActivity());
                poView.setSelected(false);
                poView.setAdapter(loMotivoRechazoAdapter);
                poView.invalidate();
                poView.setCacheColorHint(Color.TRANSPARENT);
                poView.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        if (ioMenu != null) {
                            Log.v("PRO", "ContextMenuFragment.onItemClick: " + ioMenu.getItem(position));
                            getActivity().onContextItemSelected(ioMenu.getItem(position));
                        }
                        dismiss();
                    }
                });
            } else {
                Logger.w("Lista contextual nula");
            }
        } catch (Exception e) {
            Logger.e(e);
        }
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            super.show(manager, tag);
        } catch (IllegalStateException e) {
            Log.v("PRO", "Exception: " + e.getMessage());
        }
    }

    private static void subRemovePrevDialog(FragmentManager poManager,
                                            String psTag) {
        FragmentTransaction loTransaction = poManager.beginTransaction();
        Fragment loPrevFragment = poManager.findFragmentByTag(psTag);
        if (loPrevFragment != null) {
            loTransaction.remove(loPrevFragment);
        }
        loTransaction.addToBackStack(null);
    }

    private class ContextFragmentItemAdapter extends BaseAdapter {

        private Context ioContext;

        public ContextFragmentItemAdapter(Context poContext) {
            Logger.i("ContextFragmentItemAdapter");
            this.ioContext = poContext;
        }

        @Override
        public int getCount() {
            int loCant = 0;
            if (ioMenu != null) {
                loCant = ioMenu.size();
            }
            return loCant;
        }

        @Override
        public Object getItem(int piPosition) {
            return ioMenu.getItem(piPosition);
        }

        @Override
        public long getItemId(int position) {
            return ioMenu.getItem(position).getItemId();
        }

        @Override
        public View getView(int piPosition, View poConvertView,
                            ViewGroup poParent) {
            LayoutInflater loInflater = (LayoutInflater) ioContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (poConvertView == null) {
                poConvertView = loInflater.inflate(
                        R.layout.contextfragment_item, null);
            }

            LinearLayout loLayout = poConvertView.findViewById(R.id.contextfragment_outerlayout);
            TextView txtMenu = poConvertView
                    .findViewById(R.id.contextfragment_texto);
            ImageView imgMenu = poConvertView
                    .findViewById(R.id.contextfragment_imagen);

            int margenes = (int) (20 * Resources.getSystem().getDisplayMetrics().density);

            LayoutParams loParams = (LayoutParams) loLayout.getLayoutParams();
            loParams.setMargins(margenes, margenes, margenes, margenes);
            if (ioMenu != null) {
                txtMenu.setText(ioMenu.getItem(piPosition).getTitle().toString());
                if (ioMenu.getItem(piPosition).getIcon() != null) {
                    imgMenu.setImageDrawable(ioMenu.getItem(piPosition).getIcon());
                } else {
                    imgMenu.setImageDrawable(getActivity().getResources()
                            .getDrawable(R.drawable.bullet_orange));
                }
            }

            poConvertView.setTag(piPosition);
            return poConvertView;
        }
    }
}