package pe.com.nextel.android.util;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase que permite crear y escribir archivos de texto en la SD / memoria del
 * equipo.
 *
 * @author azamora
 * @version 1.0.1
 * @since 1.6.4
 */
public class Logger {

    private static String fullPath;
    private static Writer logWriter;
    private static String rootPath = "NextelLog/";
    private static String absolutePath;
    private static LoggerListener logListener;
    private static String folderName = "Nextel";
    private static int writeMode = 0;
    private static int level = 0;
    private static final SimpleDateFormat FORMATO_HHMMSS = new SimpleDateFormat("KK:mm:ss");
    private static final SimpleDateFormat FORMATO_YYYYMMDD = new SimpleDateFormat("yyyyMMdd");
    private static String modeloEquipo = "";

    /**
     * No permite la impresi&#243;n de logs.<br />
     * Valor: 0
     */
    public static final int MODE_NONE = 0;
    /**
     * Permite la impresi&#243;n a trav&#233;s de Log.x().<br />
     * Valor: 1
     */
    public static final int MODE_NATIVE = 1;
    /**
     * Permite la impresi&#243;n escribiendo en la tarjeta SD.<br />
     * Valor: 2
     */
    public static final int MODE_SDCARD = 2;
    /**
     * Permite la impresi&#243;n a trav&#233;s de Log.x() y en la tarjeta SD.<br />
     * Valor: 3
     */
    public static final int MODE_BOTH = 3;

    /**
     * Log con nivel de severidad 0 (m&#225;ximo).<br />
     */
    public final static int LEVEL_ERROR = 0;

    /**
     * Log con nivel de severidad 1.<br />
     */
    public final static int LEVEL_WARNING = 1;

    /**
     * Log con nivel de severidad 2.<br />
     */
    public final static int LEVEL_INFORMATION = 2;

    /**
     * Log con nivel de severidad 3.<br />
     */
    public final static int LEVEL_DEBUG = 3;

    /**
     * Log con nivel de severidad 4 (menor).<br />
     */
    public final static int LEVEL_VERBOSE = 4;

    public Logger() {
    }

    /**
     * Inicializa atributos para capturas de log posteriores.
     * <p>
     *
     * @param poContext
     * @since 1.0.1
     */
    public static void initialize(Context poContext) throws Exception {
        //if(!Utilitario.fnVerOperador(poContext))
        //	throw new NexInvalidOperatorActivity.NexInvalidOperatorException(poContext);
        try {
            Logger.folderName = poContext.getString(poContext.getApplicationInfo().labelRes);
        } catch (Exception ex) {
            Log.e(folderName, "Logger.initialize(): folderName incorrecto.", ex);
        }
        Log.v(Logger.folderName, "Inicializando logger.");
        try {
            absolutePath = mkdir(folderName);
            Log.v(folderName, "Absolute path: " + absolutePath);
        } catch (Exception ex) {
            Log.e(folderName, "Logger.initialize(): absolutePath incorrecto.", ex);
        }
        try {
            Logger.writeMode = Integer.parseInt(Utilitario.fnReadParamProperty(poContext, "LOG_MODE"));
        } catch (Exception ex) {
            Log.e(folderName, "Logger.initialize(): writeMode incorrecto.", ex);
        }
        try {
            Logger.level = Integer.parseInt(Utilitario.fnReadParamProperty(poContext, "LOG_LEVEL"));
        } catch (Exception ex) {
            Log.e(folderName, "Logger.initialize(): logLevel incorrecto.", ex);
        }
        setModeloEquipo(poContext);
    }

    /**
     * Inicializa atributos para capturas de log posteriores.
     * <p>
     * La ruta por defecto es NextelLog/<i>appName</i>/yyyyMMDD-Log.txt
     * <p>
     * <b>Debe ser llamado la primera vez que se use la clase.</b>
     *
     * @param poContext  Context (necesario para guardar el modelo de equipo)
     * @param folderName Subcarpeta donde se guardan los logs. Se recomienda usar
     *                   getString(R.string.app_name).
     * @param writeMode  Indica el tipo de captura:
     *                   <ul>
     *                   <li>MODE_NONE (0): No imprime ning&#250;n log</li> <li>MODE_NATIVE
     *                   (1): Imprime s&#243;lo Log.x().</li> <li>MODE_SDCARD (2): Imprime
     *                   s&#243;lo en la carpeta SD.</li> <li>MODE_BOTH (3): Imprime Log.x()
     *                   y en la carpeta SD.</li>
     *                   </ul>
     * @param logLevel   Indica hasta que nivel de log se imprime.
     *                   <ul>
     *                   <li>LEVEL_ERROR (0): Imprime s&#243;lo <i>LEVEL_ERROR</i></li> <li>
     *                   LEVEL_WARNING (1): Imprime <i>LEVEL_ERROR</i>,
     *                   <i>LEVEL_WARNING</i></li> <li>LEVEL_INFORMATION (2): Imprime
     *                   <i>LEVEL_ERROR</i>, <i>LEVEL_WARNING</i>,
     *                   <i>LEVEL_INFORMATION</i></li> <li>LEVEL_DEBUG (3): Imprime
     *                   <i>LEVEL_ERROR</i>, <i>LEVEL_WARNING</i>,
     *                   <i>LEVEL_INFORMATION</i>, <i>LEVEL_DEBUG</i></li> <li>
     *                   LEVEL_VERBOSE (4): Imprime todos los niveles.</li>
     *                   </ul>
     * @since 1.0.1
     */
    public static void initialize(Context poContext, String folderName, int writeMode, int logLevel) {
        Logger.folderName = folderName;
        Log.v(folderName, "Inicializando logger.");
        try {
            absolutePath = mkdir(folderName);
            Log.v(folderName, "Absolute path: " + absolutePath);
        } catch (Exception ex) {
            Log.e(folderName, "Logger.initialize(): folderName incorrecto.", ex);
        }
        try {
            Logger.writeMode = writeMode;
        } catch (Exception ex) {
            Log.e(folderName, "Logger.initialize(): writeMode incorrecto.", ex);
        }
        try {
            Logger.level = logLevel;
        } catch (Exception ex) {
            Log.e(folderName, "Logger.initialize(): logLevel incorrecto.", ex);
        }
        setModeloEquipo(poContext);
    }

    /**
     * Inicializa atributos para capturas de log posteriores.
     * <p>
     * La ruta por defecto es NextelLog/<i>appName</i>/yyyyMMDD-Log.txt
     * <p>
     * <b>Debe ser llamado la primera vez que se use la clase.</b>
     *
     * @param folderName Subcarpeta donde se guardan los logs. Se recomienda usar
     *                   getString(R.string.app_name).
     * @param writeMode  Indica el tipo de captura:
     *                   <ul>
     *                   <li>MODE_NONE (0): No imprime ning&#250;n log</li> <li>MODE_NATIVE
     *                   (1): Imprime s&#243;lo Log.x().</li> <li>MODE_SDCARD (2): Imprime
     *                   s&#243;lo en la carpeta SD.</li> <li>MODE_BOTH (3): Imprime Log.x()
     *                   y en la carpeta SD.</li>
     *                   </ul>
     * @param logLevel   Indica hasta que nivel de log se imprime.
     *                   <ul>
     *                   <li>LEVEL_ERROR (0): Imprime s&#243;lo <i>LEVEL_ERROR</i></li> <li>
     *                   LEVEL_WARNING (1): Imprime <i>LEVEL_ERROR</i>,
     *                   <i>LEVEL_WARNING</i></li> <li>LEVEL_INFORMATION (2): Imprime
     *                   <i>LEVEL_ERROR</i>, <i>LEVEL_WARNING</i>,
     *                   <i>LEVEL_INFORMATION</i></li> <li>LEVEL_DEBUG (3): Imprime
     *                   <i>LEVEL_ERROR</i>, <i>LEVEL_WARNING</i>,
     *                   <i>LEVEL_INFORMATION</i>, <i>LEVEL_DEBUG</i></li> <li>
     *                   LEVEL_VERBOSE (4): Imprime todos los niveles.</li>
     *                   </ul>
     */
    public static void initialize(String folderName, int writeMode, int logLevel) {
        Logger.folderName = folderName;
        Log.v(folderName, "Inicializando logger.");
        try {
            absolutePath = mkdir(folderName);
            Log.v(folderName, "Absolute path: " + absolutePath);
        } catch (Exception ex) {
            Log.e(folderName, "Logger.initialize(): folderName incorrecto.", ex);
        }
        try {
            Logger.writeMode = writeMode;
        } catch (Exception ex) {
            Log.e(folderName, "Logger.initialize(): writeMode incorrecto.", ex);
        }
        try {
            Logger.level = logLevel;
        } catch (Exception ex) {
            Log.e(folderName, "Logger.initialize(): logLevel incorrecto.", ex);
        }
    }

    /**
     * Inicializa atributos para capturas de log posteriores.
     * <p>
     * La ruta por defecto es NextelLog/<i>appName</i>/yyyyMMDD-Log.txt
     * <p>
     * <b>Debe ser llamado la primera vez que se use la clase.</b>
     *
     * @param folderName Subcarpeta donde se guardan los logs. Se recomienda usar
     *                   getString(R.string.app_name).
     * @param writeMode  Indica el tipo de captura:
     *                   <ul>
     *                   <li>MODE_NONE (0): No imprime ning&#250;n log</li> <li>MODE_NATIVE
     *                   (1): Imprime s&#243;lo Log.x().</li> <li>MODE_SDCARD (2): Imprime
     *                   s&#243;lo en la carpeta SD.</li> <li>MODE_BOTH (3): Imprime Log.x()
     *                   y en la carpeta SD.</li>
     *                   </ul>
     * @param logLevel   Indica hasta que nivel de log se imprime.
     *                   <ul>
     *                   <li>LEVEL_ERROR (0): Imprime s&#243;lo <i>LEVEL_ERROR</i></li> <li>
     *                   LEVEL_WARNING (1): Imprime <i>LEVEL_ERROR</i>,
     *                   <i>LEVEL_WARNING</i></li> <li>LEVEL_INFORMATION (2): Imprime
     *                   <i>LEVEL_ERROR</i>, <i>LEVEL_WARNING</i>,
     *                   <i>LEVEL_INFORMATION</i></li> <li>LEVEL_DEBUG (3): Imprime
     *                   <i>LEVEL_ERROR</i>, <i>LEVEL_WARNING</i>,
     *                   <i>LEVEL_INFORMATION</i>, <i>LEVEL_DEBUG</i></li> <li>
     *                   LEVEL_VERBOSE (4): Imprime todos los niveles.</li>
     *                   </ul>
     */
    public static void initialize(String folderName, String writeMode,
                                  String logLevel) {
        initialize(folderName, Integer.parseInt(writeMode),
                Integer.parseInt(logLevel));
    }

    /**
     * Si no existe, crea la ruta especificada en la tarjeta SD.
     *
     * @param path Ruta a crear.
     * @return Ruta absoluta creada.
     * @throws IOException
     */
    protected static String mkdir(String path) throws Exception {
        //Log.d("XXX", "Logger -> mkdir");
        File sdcard = Environment.getExternalStorageDirectory();
        File logDir = new File(sdcard, rootPath + path + "/");
        if (!logDir.exists()) {
            boolean bMkdirs = logDir.mkdirs();
            // archivo en blanco para asegurar que se cree la carpeta
            bMkdirs = new File(logDir, ".nomedia").createNewFile();
        }
        return logDir.getAbsolutePath();
    }

    /**
     * Crea el PrintWriter para el archivo en la ruta especificada.
     *
     * @param basePath Ruta donde crear el archivo.
     * @throws IOException
     */
    protected static void open(String basePath) throws Exception {
        File f = new File(basePath);
        if (!f.exists() || logWriter == null) {
            fullPath = f.getAbsolutePath();
            logWriter = new PrintWriter(fullPath);
        }
    }

    /**
     * Escribe una nueva l&#237;nea en el archivo, en nivel Verbose.
     *
     * @param message Mensaje a escribir.
     */
    public static void v(String message) {
        println(message, folderName, Logger.LEVEL_VERBOSE);
    }

    /**
     * Escribe una nueva l&#237;nea en el archivo, en nivel Verbose.
     *
     * @param tag     Identificador del mensaje.
     * @param message Mensaje a escribir.
     */
    public static void v(String tag, String message) {
        println(message, tag, Logger.LEVEL_VERBOSE);
    }

    /**
     * Escribe el stacktrace en el archivo, en nivel Verbose.
     *
     * @param message Excepci&#243;n a obtener stacktrace.
     */
    public static void v(Throwable message) {
        println(Log.getStackTraceString(message), folderName,
                Logger.LEVEL_VERBOSE);
    }

    /**
     * Escribe el stacktrace en el archivo, en nivel Verbose.
     *
     * @param message Excepci&#243;n a obtener stacktrace.
     * @param tag     Identificador del mensaje.
     */
    public static void v(String tag, Throwable message) {
        println(Log.getStackTraceString(message), tag, Logger.LEVEL_VERBOSE);
    }

    /**
     * Escribe una nueva l&#237;nea en el archivo, en nivel Information.
     *
     * @param message Mensaje a escribir.
     */
    public static void i(String message) {
        println(message, folderName, Logger.LEVEL_INFORMATION);
    }

    /**
     * Escribe una nueva l&#237;nea en el archivo, en nivel Information.
     *
     * @param tag     Identificador del mensaje.
     * @param message Mensaje a escribir.
     */
    public static void i(String tag, String message) {
        println(message, tag, Logger.LEVEL_INFORMATION);
    }

    /**
     * Escribe el stacktrace en el archivo, en nivel Information.
     *
     * @param message Excepci&#243;n a obtener stacktrace.
     */
    public static void i(Throwable message) {
        println(Log.getStackTraceString(message), folderName,
                Logger.LEVEL_INFORMATION);
    }

    /**
     * Escribe el stacktrace en el archivo, en nivel Information.
     *
     * @param message Excepci&#243;n a obtener stacktrace.
     * @param tag     Identificador del mensaje.
     */
    public static void i(String tag, Throwable message) {
        println(Log.getStackTraceString(message), tag, Logger.LEVEL_INFORMATION);
    }

    /**
     * Escribe una nueva l&#237;nea en el archivo, en nivel Debug.
     *
     * @param message Mensaje a escribir.
     */
    public static void d(String message) {
        println(message, folderName, Logger.LEVEL_DEBUG);
    }

    /**
     * Escribe una nueva l&#237;nea en el archivo, en nivel Debug.
     *
     * @param tag     Identificador del mensaje.
     * @param message Mensaje a escribir.
     */
    public static void d(String tag, String message) {
        println(message, tag, Logger.LEVEL_DEBUG);
    }

    /**
     * Escribe el stacktrace en el archivo, en nivel Debug.
     *
     * @param message Excepci&#243;n a obtener stacktrace.
     */
    public static void d(Throwable message) {
        println(Log.getStackTraceString(message), folderName,
                Logger.LEVEL_DEBUG);
    }

    /**
     * Escribe el stacktrace en el archivo, en nivel Debug.
     *
     * @param message Excepci&#243;n a obtener stacktrace.
     * @param tag     Identificador del mensaje.
     */
    public static void d(String tag, Throwable message) {
        println(Log.getStackTraceString(message), tag, Logger.LEVEL_DEBUG);
    }

    /**
     * Escribe una nueva l&#237;nea en el archivo, en nivel Warning.
     *
     * @param message Mensaje a escribir.
     */
    public static void w(String message) {
        println(message, folderName, Logger.LEVEL_WARNING);
    }

    /**
     * Escribe una nueva l&#237;nea en el archivo, en nivel Warning.
     *
     * @param tag     Identificador del mensaje.
     * @param message Mensaje a escribir.
     */
    public static void w(String tag, String message) {
        println(message, tag, Logger.LEVEL_WARNING);
    }

    /**
     * Escribe el stacktrace en el archivo, en nivel Warning.
     *
     * @param message Excepci&#243;n a obtener stacktrace.
     */
    public static void w(Throwable message) {
        println(Log.getStackTraceString(message), folderName,
                Logger.LEVEL_WARNING);
    }

    /**
     * Escribe el stacktrace en el archivo, en nivel Warning.
     *
     * @param message Excepci&#243;n a obtener stacktrace.
     * @param tag     Identificador del mensaje.
     */
    public static void w(String tag, Throwable message) {
        println(Log.getStackTraceString(message), tag, Logger.LEVEL_WARNING);
    }

    /**
     * Escribe una nueva l&#237;nea en el archivo, en nivel Error.
     *
     * @param message Mensaje a escribir.
     */
    public static void e(String message) {
        println(message, folderName, Logger.LEVEL_ERROR);
    }

    /**
     * Escribe una nueva l&#237;nea en el archivo, en nivel Error.
     *
     * @param tag     Identificador del mensaje.
     * @param message Mensaje a escribir.
     */
    public static void e(String tag, String message) {
        println(message, tag, Logger.LEVEL_ERROR);
    }

    /**
     * Escribe el stacktrace en el archivo, en nivel Error.
     *
     * @param message Excepci&#243;n a obtener stacktrace.
     */
    public static void e(Throwable message) {
        println(Log.getStackTraceString(message), folderName,
                Logger.LEVEL_ERROR);
    }

    /**
     * Escribe el stacktrace en el archivo, en nivel Error.
     *
     * @param message Excepci&#243;n a obtener stacktrace.
     * @param tag     Identificador del mensaje.
     */
    public static void e(String tag, Throwable message) {
        println(Log.getStackTraceString(message), tag, Logger.LEVEL_ERROR);
    }

    /**
     * Escribe una nueva l&#237;nea en el archivo, en nivel Verbose.
     *
     * @param message Mensaje a escribir.
     */
    public static void log(String message) {
        println(message, folderName, Logger.LEVEL_VERBOSE);
    }

    /**
     * Escribe una nueva l&#237;nea en el archivo.
     *
     * @param message  Mensaje a escribir.
     * @param logLevel Nivel de log a escribir, i.e: Logger.LEVEL_VERBOSE.
     */
    public static void log(String message, int logLevel) {
        println(message, folderName, logLevel);
    }

    /**
     * Escribe una nueva l&#237;nea en el archivo, especificando un tag y nivel de
     * log.
     *
     * @param message  Mensaje a escribir.
     * @param tag      Identificador del mensaje.
     * @param logLevel Nivel de log a escribir, i.e: Logger.LEVEL_VERBOSE.
     */
    public static void log(String message, String tag, int logLevel) {
        println(message, tag, logLevel);
    }

    /**
     * Escribe una nueva l&#237;nea en el archivo en nivel Verbose, especificando un
     * tag.
     *
     * @param message Mensaje a escribir.
     * @param tag     Identificador del mensaje.
     */
    public static void log(String message, String tag) {
        println(message, folderName, Logger.LEVEL_VERBOSE);
    }

    /**
     * Escribe el stacktrace de una excepci&#243;n en el archivo, en nivel Error.
     *
     * @param e Excepci&#243;n a escribir.
     */
    public static void log(Throwable e) {
        println(Log.getStackTraceString(e), folderName, Logger.LEVEL_ERROR);
    }

    /**
     * Escribe el stacktrace de una excepci&#243;n en el archivo, en nivel Error.
     *
     * @param e   Excepci&#243;n a escribir.
     * @param tag Identificador del mensaje.
     */
    public static void log(Throwable e, String tag) {
        println(Log.getStackTraceString(e), tag, Logger.LEVEL_ERROR);
    }

    /**
     * Escribe el stacktrace de una excepci&#243;n en el archivo.
     *
     * @param e        Excepci&#243;n a escribir.
     * @param tag      Identificador del mensaje.
     * @param logLevel Nivel de log a escribir, i.e: Logger.LEVEL_VERBOSE.
     */
    public static void log(Throwable e, String tag, int logLevel) {
        println(Log.getStackTraceString(e), tag, logLevel);
    }

    /**
     * Escribe el stacktrace de una excepci&#243;n en el archivo, en el nivel
     * especificado.
     *
     * @param e        Excepci&#243;n a escribir.
     * @param logLevel Nivel de log a escribir, i.e: Logger.LEVEL_VERBOSE.
     */
    public static void log(Throwable e, int logLevel) {
        println(Log.getStackTraceString(e), folderName, logLevel);
    }

    /**
     * Escribe la l&#237;nea por LogCat y/o en el archivo, seg&#250;n configuraci&#243;n.
     *
     * @param message  Mensaje a escribir.
     * @param tag      Identificador del mensaje.
     * @param logLevel Nivel de log.
     */
    private static synchronized void println(String message, String tag,
                                             int logLevel) {
        if (writeMode != MODE_NONE && level >= logLevel) {
            if (logListener != null) {
                logListener.notifyMessage(message);
            }
            try {
                switch (writeMode) {
                    case MODE_NATIVE:
                        logMessage(message, tag, logLevel);
                        break;
                    case MODE_SDCARD:
                        writeMessage(message, tag, logLevel);
                        break;
                    case MODE_BOTH:
                        logMessage(message, tag, logLevel);
                        writeMessage(message, tag, logLevel);
                        break;
                }
            } catch (Exception io) {
                Log.e(folderName, "Error al acceder a la tarjeta SD.");
            }
        }
    }

    private static void logMessage(String message, String tag, int logLevel) {
        if (!modeloEquipo.startsWith("Huawei-HUAWEI%20Y300")) {
            switch (logLevel) {
                case LEVEL_ERROR:
                    Log.e(tag, message);
                    break;
                case LEVEL_INFORMATION:
                    Log.i(tag, message);
                    break;
                case LEVEL_VERBOSE:
                    Log.v(tag, message);
                    break;
                case LEVEL_DEBUG:
                    Log.d(tag, message);
                    break;
                case LEVEL_WARNING:
                    Log.w(tag, message);
                    break;
                default:
                    Log.v(tag, message);
                    break;
            }
        } else {
            switch (logLevel) {
                case LEVEL_ERROR:
                    System.err.println(message);
                    break;
                default:
                    System.out.println(message);
                    break;
            }
        }
    }

    private static void writeMessage(String message, String tag, int logLevel)
            throws Exception {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            open(absolutePath + "/" + FORMATO_YYYYMMDD.format(new Date())
                    + "Log.txt");
            logWriter.write(FORMATO_HHMMSS.format(new Date()));
            logWriter.write("|");
            switch (logLevel) {
                case LEVEL_ERROR:
                    logWriter.write("E");
                    break;
                case LEVEL_INFORMATION:
                    logWriter.write("I");
                    break;
                case LEVEL_VERBOSE:
                    logWriter.write("V");
                    break;
                case LEVEL_DEBUG:
                    logWriter.write("D");
                    break;
                case LEVEL_WARNING:
                    logWriter.write("W");
                    break;
                default:
                    logWriter.write("V");
                    break;
            }
            logWriter.write("|");
            logWriter.write(tag);
            logWriter.write("|");
            logWriter.write(message);
            logWriter.write('\n');
            logWriter.flush();
        }
    }

    /**
     * Cierra la conexi&#243;n del Writer.
     *
     * @throws IOException
     */
    public static void close() throws Exception {
        logWriter.close();
    }

    /**
     * Obtiene la ruta completa configurada para escribir el log.
     *
     * @return Ruta completa.
     */
    public static String getPath() {
        return fullPath;
    }

    /**
     * @return Listener de cuando se escribe un mensaje.
     */
    public static LoggerListener getListener() {
        return logListener;
    }

    /**
     * @param listener de cuando se escribe un mensaje.
     */
    public static void setListener(LoggerListener listener) {
        Logger.logListener = listener;
    }

    /**
     * Retorna el modelo de equipo almacenado previamente.
     *
     * @return Modelo del equipo almacenado.
     * @since 1.0.1
     */
    public static String getModeloEquipo() {
        return modeloEquipo;
    }

    /**
     * Almacena el modelo del equipo para futuras validaciones, obtenido
     * automaticamente.
     *
     * @param poContext Context para hallar el modelo del equipo.
     * @since 1.0.1
     */
    public static void setModeloEquipo(Context poContext) {
        Logger.modeloEquipo = Utilitario.fnModeloEquipo(poContext);
    }

    /**
     * Almacena el modelo del equipo para futuras validaciones.
     *
     * @param psModelo Cadena conteniendo el modelo del equipo.
     * @since 1.0.1
     */
    public static void setModeloEquipo(String psModelo) {
        Logger.modeloEquipo = psModelo;
    }

}
