package pe.com.nextel.android.actividad;


import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

import pe.com.nextel.android.R;
import pe.com.nextel.android.util.ConfiguracionNextel.EnumTheme;
import pe.com.nextel.android.util.NexActivityGestor;

//import android.preference.ListPreference;

/**
 * Actividad de preferencias de la c&#225;mara de toma de fotos
 * </br>Se debe declarar en el <b>AndroidManifest.xml</b> la siguiente l&#237;nea:
 * </br></br>&#60;activity android:name="pe.com.nextel.android.actividad.NexCameraConfigActivity"/&#62;
 *
 * @author jroeder
 */
public final class NexCameraConfigActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {
    private ListPreference preResolucion;
    private ListPreference preUsarFlash;
    private ListPreference preUsarAutoFocus;
    SharedPreferences ioSharedPreferences;
    private String[] ioLstResolucion;
    private String[] ioLstFlash;
    private String[] ioLstAutoFocus;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        NexActivityGestor.getInstance().finishActivity(this);
    }

    /**
     * Retorna el id de recurso del estilo del tema a utilizar en la actividad
     *
     * @return <b>int</b>
     */
    protected int getThemeResource() {
        return EnumTheme.BLUE.getResourceStyle(this);
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NexActivityGestor.getInstance().addActivity(this);

        setTheme(getThemeResource());

        //if(!Utilitario.fnVerOperador(this))
        //	startActivity(new Intent(this, NexInvalidOperatorActivity.class));
        addPreferencesFromResource(R.xml.nexcamerapreferences);
        getListView().setBackgroundColor(Color.rgb(0, 0, 0));

        ioSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Bundle loExtras = getIntent().getExtras();

        //resoluciones soportadas
        ioLstResolucion = loExtras.getStringArray("supportedRes");
        ioLstFlash = loExtras.getStringArray("supportedFlash");
        ioLstAutoFocus = loExtras.getStringArray("supportedAutoFocus");

        preResolucion = (ListPreference) getPreferenceScreen().findPreference(getString(R.string.keypre_resolucion));

        if (ioLstResolucion != null) {
            preResolucion.setEntries(ioLstResolucion);
            preResolucion.setEntryValues(ioLstResolucion);
            preResolucion.setDefaultValue(ioLstResolucion[ioLstResolucion.length - 1]);
        }

        preUsarFlash = (ListPreference) getPreferenceScreen().findPreference(getString(R.string.keypre_flash));

        if (ioLstFlash != null) {
            preUsarFlash.setEntries(ioLstFlash);
            preUsarFlash.setEntryValues(ioLstFlash);
        } else {
            preUsarFlash.setEnabled(false);
        }

        preUsarAutoFocus = (ListPreference) getPreferenceScreen().findPreference(getString(R.string.keypre_autofocus));

        if (ioLstAutoFocus != null) {
            preUsarAutoFocus.setEntries(ioLstAutoFocus);
            preUsarAutoFocus.setEntryValues(ioLstAutoFocus);
        } else {
            preUsarAutoFocus.setEnabled(false);
        }
    }


    @SuppressWarnings("deprecation")
    @Override
    protected void onResume() {
        super.onResume();
        preUsarFlash.setSummary(ioSharedPreferences.getString(getString(R.string.keypre_flash), getString(R.string.flashdefecto)));
        if (ioLstResolucion != null) {
            preResolucion.setSummary(ioSharedPreferences.getString(getString(R.string.keypre_resolucion), ioLstResolucion[ioLstResolucion.length - 1]));
        } else {
            preResolucion.setSummary(ioSharedPreferences.getString(getString(R.string.keypre_resolucion), "minima"));
        }
        preUsarAutoFocus.setSummary(ioSharedPreferences.getString(getString(R.string.keypre_autofocus), getString(R.string.autofocodefecto)));
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {
        if (key.equals(getString(R.string.keypre_resolucion))) {
            preResolucion.setSummary(ioSharedPreferences.getString(key, getString(R.string.resoluciondefecto)));
        } else if (key.equals(getString(R.string.keypre_autofocus))) {
            preUsarAutoFocus.setSummary(ioSharedPreferences.getString(key, getString(R.string.autofocodefecto)));
        } else if (key.equals(getString(R.string.keypre_flash))) {
            preUsarFlash.setSummary(ioSharedPreferences.getString(key, getString(R.string.flashdefecto)));
        }
    }

}
