package pe.com.nextel.android.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView.BufferType;

import pe.com.nextel.android.R;
import pe.com.nextel.android.util.Utilitario;

public class NexSwitch extends LinearLayout implements OnCheckedChangeListener {

    private Switch _switch;

    public NexSwitch(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        // TODO Auto-generated constructor stub
    }

    public NexSwitch(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    public NexSwitch(Context context) {
        super(context);
        inflate(context);
    }

    private void inflate(Context context) {
        LayoutInflater.from(context).inflate(R.layout.nexswitch, this);
    }

    private void OnCreateView() {
        if (_switch == null) {
            View _view = null;
            do {
                if (_view == null) {
                    _view = getChildAt(0);
                } else {
                    _view = ((ViewGroup) _view).getChildAt(0);
                }
                if (_view == null)
                    return;
            } while (!(_view instanceof Switch));
            _switch = (Switch) _view;
        }
        if (_onCheckedListener == null) {
            _onCheckedListener = (OnCheckedChangeListener) getTag(R.string.NexSwitch_keytag_checkedlistener);
        }
    }

    private OnCheckedChangeListener _onCheckedListener;

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        OnCreateView();
        _switch.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        View view = ((Activity) getContext()).getWindow().getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
            view.clearFocus();
        }

        if (_onCheckedListener != null)
            _onCheckedListener.onCheckedChanged(buttonView, isChecked);
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        OnCreateView();

        _onCheckedListener = listener;

        setTag(R.string.NexSwitch_keytag_checkedlistener, _onCheckedListener);

        _switch.setOnCheckedChangeListener(this);
    }

    public void setOnClickListener(OnClickListener l) {
        OnCreateView();
        _switch.setOnClickListener(l);
    }

    public void setEnabled(boolean enabled) {
        OnCreateView();
        _switch.setEnabled(enabled);
    }

    public boolean isEnabled() {
        OnCreateView();
        return _switch.isEnabled();
    }

    public CharSequence getText() {
        OnCreateView();
        return _switch.getText();
    }

    public void setTextScaleX(float size) {
        OnCreateView();
        _switch.setTextScaleX(size);
    }

    public void setTextSize(float size) {
        OnCreateView();
        _switch.setTextSize(size);
    }

    public void setTextSize(int unit, float size) {
        OnCreateView();
        _switch.setTextSize(unit, size);
    }

    public void setTextColor(ColorStateList colors) {
        OnCreateView();
        _switch.setTextColor(colors);
    }

    public void setTextColor(int color) {
        OnCreateView();
        _switch.setTextColor(color);
    }

    public void setBackground(Drawable background) {
        OnCreateView();
        try {
            _switch.setBackground(background);
        } catch (Exception e) {
            if (!Utilitario.hasMethodInStackTrace("<init>", e.getStackTrace()))
                throw new IllegalStateException(e);
        }
    }

    public void setBackgroundColor(int color) {
        OnCreateView();
        _switch.setBackgroundColor(color);
    }

    @Deprecated
    public void setBackgroundDrawable(Drawable background) {
        OnCreateView();
        _switch.setBackgroundDrawable(background);
    }

    public void setBackgroundResource(int resid) {
        OnCreateView();
        _switch.setBackgroundResource(resid);
    }

    @Override
    @ExportedProperty
    public boolean isActivated() {
        OnCreateView();
        return _switch.isActivated();
    }

    @Override
    public void setActivated(boolean activated) {
        OnCreateView();
        _switch.setActivated(activated);
    }

    public void setChecked(boolean checked) {
        OnCreateView();
        _switch.setChecked(checked);
    }

    public boolean isChecked() {
        OnCreateView();
        return _switch.isChecked();
    }

    public CharSequence getTextOff() {
        OnCreateView();
        return _switch.getTextOff();
    }

    public CharSequence getTextOn() {
        OnCreateView();
        return _switch.getTextOn();
    }

    public void setText(CharSequence text) {
        OnCreateView();
        _switch.setText(text);
    }

    public void setText(int resid) {
        OnCreateView();
        _switch.setText(resid);
    }

    public void setText(CharSequence text, BufferType type) {
        OnCreateView();
        _switch.setText(text, type);
    }

    public void setText(int resid, BufferType type) {
        OnCreateView();
        _switch.setText(resid, type);
    }

    public void setText(char[] text, int start, int len) {
        OnCreateView();
        _switch.setText(text, start, len);
    }
}
