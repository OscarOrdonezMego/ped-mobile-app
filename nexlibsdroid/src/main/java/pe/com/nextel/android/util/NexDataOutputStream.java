package pe.com.nextel.android.util;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Clase que hereda del {@link DataOutputStream}.</br>
 * Es usada para escribir datos en un OutputStream
 *
 * @author jroeder
 */
public class NexDataOutputStream extends DataOutputStream {

    /**
     * Contructor de la clase
     *
     * @param out - OutputStream
     */
    public NexDataOutputStream(OutputStream out) {
        super(out);
    }

    /**
     * Escribe una cadena en el OutputStream
     *
     * @param psValue - Cadena a escribir
     * @throws IOException
     */
    public final void writeString(String psValue) throws IOException {
        writeInt(psValue.length());
        writeChars(psValue);
    }
}
