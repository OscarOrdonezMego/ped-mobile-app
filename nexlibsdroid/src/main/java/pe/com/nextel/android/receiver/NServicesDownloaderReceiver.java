package pe.com.nextel.android.receiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import pe.com.nextel.android.R;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.actividad.NexListActivityBasic;
import pe.com.nextel.android.actividad.NexListActivityTweaked;
import pe.com.nextel.android.actividad.NexPreferenceActivity;
import pe.com.nextel.android.actividad.support.NexFragmentActivity;
import pe.com.nextel.android.actividad.support.NexFragmentListActivityBasic;
import pe.com.nextel.android.actividad.support.NexFragmentListActivityTweaked;
import pe.com.nextel.android.util.NServicesConfiguration;
import pe.com.nextel.android.util.Utilitario;

/**
 * Implementa un {@link BroadcastReceiver} que est&#225; a la escucha del t&#233;rmino de la descarga del instalador del NServices</br>
 * Realiza un {@link Activity#unregisterReceiver} en forma autom&#243;tica cuando recibe el <b>broadcast</b></br>
 * Utilizar un {@link NServicesDownloadedIntentFilter} en el m&#233;todo {@link Activity#registerReceiver} de la actividad
 *
 * @author jroeder
 */
public final class NServicesDownloaderReceiver extends BroadcastReceiver {

    private Context _context;

    private void subShowException(Exception e) {
        if (_context instanceof NexActivity) {
            ((NexActivity) _context).subShowExptionOnRunUI(e);
        } else if (_context instanceof NexListActivityBasic) {
            ((NexListActivityBasic) _context).subShowExptionOnRunUI(e);
        } else if (_context instanceof NexListActivityTweaked) {
            ((NexListActivityTweaked) _context).subShowExptionOnRunUI(e);
        } else if (_context instanceof NexPreferenceActivity) {
            ((NexPreferenceActivity) _context).subShowExptionOnRunUI(e);
        } else if (_context instanceof NexFragmentActivity) {
            ((NexFragmentActivity) _context).subShowExptionOnRunUI(e);
        } else if (_context instanceof NexFragmentListActivityBasic) {
            ((NexFragmentListActivityBasic) _context).subShowExptionOnRunUI(e);
        } else if (_context instanceof NexFragmentListActivityTweaked) {
            ((NexFragmentListActivityTweaked) _context).subShowExptionOnRunUI(e);
        } else {
            Log.e("PRO", "NServicesDownloaderReceiver.onReceive", e);
        }
        unregister(_context);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getBooleanExtra(context.getString(R.string.nservices_keyPreExitoDescarga), false)) {
            _context = context;
            try {
                Runnable action = new Runnable() {

                    @Override
                    public void run() {
                        try {
                            _context.startActivity(Utilitario.obtainIntentInstallApplication(_context,
                                    Environment.getExternalStorageDirectory() +
                                            Utilitario.fnNomCarpeta(Utilitario.getStringResourceByName(_context, "app_name"),
                                                    NServicesConfiguration.NSERVICES_DOWNLOAD_FOLDER) +
                                            NServicesConfiguration.NSERVICES_DOWNLOAD_APK));
                        } catch (Exception e) {
                            subShowException(e);
                        }
                    }
                };
                if (context instanceof Activity) {
                    ((Activity) context).runOnUiThread(action);
                } else if (context instanceof FragmentActivity) {
                    ((FragmentActivity) context).runOnUiThread(action);
                } else {
                    action.run();
                }
                unregister(context);
            } catch (Exception e) {
                subShowException(e);
            }
        } else {
            unregister(context);
        }
    }

    /**
     * Registra el {@link NServicesDownloaderReceiver} utilizando un {@link NServicesDownloadedIntentFilter}
     *
     * @param context {@link Context} de la aplicaci&#243;n
     */
    public void register(Context context) {
        context.getApplicationContext().registerReceiver(this, new NServicesDownloadedIntentFilter(context));
    }

    /**
     * Anula el registro del {@link NServicesDownloaderReceiver}
     *
     * @param context {@link Context} de la aplicaci&#243;n
     */
    public void unregister(Context context) {
        context.getApplicationContext().unregisterReceiver(this);
    }

    /**
     * Implementa un {@link IntentFilter} con la acci&#243;n del <b>broadcast</b> de la descarga
     *
     * @author jroeder
     */
    public final static class NServicesDownloadedIntentFilter extends IntentFilter {
        /**
         * Constructor del {@link NServicesDownloadedIntentFilter}
         *
         * @param poContext {@link Context} de la aplicaci&#243;n
         */
        public NServicesDownloadedIntentFilter(Context poContext) {
            addAction(poContext.getPackageName() + NServicesConfiguration.NSERVICES_BROADCAST_DOWNLOADED);
        }
    }
}
