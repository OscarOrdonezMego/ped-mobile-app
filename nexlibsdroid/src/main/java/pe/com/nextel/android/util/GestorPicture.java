package pe.com.nextel.android.util;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.File;

/**
 * Clase gestor de im&#225;genes para la captura de fotos
 *
 * @author jroeder
 * @version 1.1
 */
public class GestorPicture {
    /**
     * Crea un objeto del tipo {@link Uri}, haciendo referencia a la ruta de la captura de foto.
     * Utiliza la carpeta por defecto de foto de la aplicaci&#243;n.
     *
     * @param poContext         Contexto de la aplicaci&#243;n
     * @param piResourceAppName &#205;ndice del recurso del nombre de la aplicaci&#243;n (R.string.app_name)
     * @param psFileName        Nombre del archivo de captura de foto
     * @return {@link Uri} Objeto con referencia a la captura de foto
     */
    public static Uri fnSavePath(Context poContext, int piResourceAppName, String psFileName) throws Exception {
        Log.v("GestorPicture", "fnSavePath " + psFileName);
        return GestorPicture.fnSavePath(poContext, piResourceAppName, psFileName, psFileName.contains(poContext.getString(piResourceAppName)));
    }

    /**
     * Crea un objeto del tipo {@link Uri}, haciendo referencia a la ruta de la captura de foto.
     * Utiliza la carpeta por defecto de foto de la aplicaci&#243;n.
     *
     * @param poContext         Contexto de la aplicaci&#243;n
     * @param piResourceAppName &#205;ndice del recurso del nombre de la aplicaci&#243;n (R.string.app_name)
     * @param psFileName        Nombre del archivo de captura de foto
     * @param pathComplete      Verifica si el nombre de archivo contiene la ruta completa
     *                          <ul>
     *                          <li><b>true</b> En caso tenga la ruta completa</li>
     *                          <li><b>false</b> En caso tenga la ruta incompleta</li>
     *                          </ul>
     * @return {@link Uri} Objeto con referencia a la captura de foto
     */
    public static Uri fnSavePath(Context poContext, int piResourceAppName, String psFileName, boolean pathComplete) throws Exception {
        Log.v("GestorPicture", "fnSavePath " + psFileName);
        String lsPath = "";
        lsPath = Environment.getExternalStorageDirectory() + Utilitario.fnNomCarFoto(poContext.getString(piResourceAppName));
        File loDirectory = new File(lsPath);
        loDirectory.mkdirs();
        lsPath = !pathComplete ? (lsPath + psFileName) : psFileName;
        File loFile = new File(lsPath);
        Uri loUri = Uri.fromFile(loFile);
        Log.v("PRO", "image's path: " + lsPath);
        return loUri;
    }

    /**
     * Crea un objeto del tipo {@link Uri}, haciendo referencia a la ruta de la captura de foto.
     *
     * @param psFolderName Nombre de la carpeta de foto
     * @param psFileName   Nombre del archivo de captura de foto
     * @return {@link Uri} Objeto con referencia a la captura de foto
     */
    public static Uri fnSavePath(String psFolderName, String psFileName) throws Exception {
        Log.v("GestorPicture", "fnSavePath " + psFileName);
        return GestorPicture.fnSavePath(psFolderName, psFileName, psFileName.contains(psFolderName));
    }

    /**
     * Crea un objeto del tipo {@link Uri}, haciendo referencia a la ruta de la captura de foto.
     *
     * @param psFolderName Nombre de la carpeta de foto
     * @param psFileName   Nombre del archivo de captura de foto
     * @param pathComplete Verifica si el nombre de archivo contiene la ruta completa
     *                     <ul>
     *                     <li><b>true</b> En caso tenga la ruta completa</li>
     *                     <li><b>false</b> En caso tenga la ruta incompleta</li>
     *                     </ul>
     * @return {@link Uri} Objeto con referencia a la captura de foto
     */
    public static Uri fnSavePath(String psFolderName, String psFileName, boolean pathComplete) throws Exception {
        Log.v("GestorPicture", "fnSavePath " + psFileName);
        String lsPath = "";
        lsPath = Environment.getExternalStorageDirectory() + psFolderName;
        File loDirectory = new File(lsPath);
        loDirectory.mkdirs();
        lsPath = !pathComplete ? (lsPath + psFileName) : psFileName;
        File loFile = new File(lsPath);
        Uri loUri = Uri.fromFile(loFile);
        Log.v("PRO", "image's path: " + lsPath);
        return loUri;
    }
}
