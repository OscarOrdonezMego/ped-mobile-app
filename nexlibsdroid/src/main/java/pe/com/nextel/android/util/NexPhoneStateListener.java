package pe.com.nextel.android.util;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.lang.reflect.Method;

/**
 * Clase que verifica el estado del tel&#233;fono.</br>
 * Extiende del PhoneStateListener
 *
 * @author jroeder
 */
public class NexPhoneStateListener extends PhoneStateListener {
    private int iiNivelSenal = 0;
    private Context localContext;
    private final static String LTE_SIGNAL_STRENGTH = "getLteAsuLevel"; //"getLteSignalStrength";

    public NexPhoneStateListener(Context ctx) {
        super();
        localContext = ctx;
    }

    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        super.onSignalStrengthsChanged(signalStrength);
        int level = 0;
        int strengthLTE = 0;

        if (signalStrength.isGsm()) {

            TelephonyManager manager = (TelephonyManager) localContext.getSystemService(Context.TELEPHONY_SERVICE);
            int asu;
            if (manager.getNetworkType() == TelephonyManager.NETWORK_TYPE_LTE) {

                strengthLTE = getLTEsignalStrength(signalStrength);
                asu = strengthLTE;
            } else {
                asu = signalStrength.getGsmSignalStrength();
            }

            if (asu <= 2 || asu == 99) level = 0;
            else if (asu >= 12) level = 4;
            else if (asu >= 8) level = 3;
            else if (asu >= 5) level = 2;
            else level = 1;
        } else {
            final int snr = signalStrength.getEvdoSnr();
            final int cdmaDbm = signalStrength.getCdmaDbm();
            final int cdmaEcio = signalStrength.getCdmaEcio();
            int levelDbm;
            int levelEcio;

            if (snr == -1) {
                if (cdmaDbm >= -75) levelDbm = 4;
                else if (cdmaDbm >= -85) levelDbm = 3;
                else if (cdmaDbm >= -95) levelDbm = 2;
                else if (cdmaDbm >= -100) levelDbm = 1;
                else levelDbm = 0;

                if (cdmaEcio >= -90) levelEcio = 4;
                else if (cdmaEcio >= -110) levelEcio = 3;
                else if (cdmaEcio >= -130) levelEcio = 2;
                else if (cdmaEcio >= -150) levelEcio = 1;
                else levelEcio = 0;

                level = (levelDbm < levelEcio) ? levelDbm : levelEcio;
            } else {
                if (snr == 7 || snr == 8) level = 4;
                else if (snr == 5 || snr == 6) level = 3;
                else if (snr == 3 || snr == 4) level = 2;
                else if (snr == 1 || snr == 2) level = 1;
            }
        }
        iiNivelSenal = level + 1;
    }

    /**
     * Retorna el nivel de Se&#241;al del Tel&#233;fono
     *
     * @param poContext - Contexto de la aplicaci&#243;n
     * @return int
     */
    public int getSignalLevel(Context poContext) {
        if (Utilitario.fnVerSignal(poContext)) {
            return iiNivelSenal;
        } else {
            return 0;
        }
    }

    private int getLTEsignalStrength(SignalStrength signalStrength) {
        int LTEsignal = 0;
        try {
            Method[] methods = android.telephony.SignalStrength.class.getMethods();

            for (Method mthd : methods) {
                if (mthd.getName().equals(LTE_SIGNAL_STRENGTH)) {
                    LTEsignal = (Integer) mthd.invoke(signalStrength, new Object[]{});
                    return LTEsignal;
                }
            }
        } catch (Exception e) {
            Log.e("ERROR", "Exception: " + e.toString());
        }
        return LTEsignal;
    }

}
