package pe.com.nextel.android.widget;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import pe.com.nextel.android.R;
import pe.com.nextel.android.util.Utilitario;

public class NexProgressDialog extends ProgressDialog {

    private Context ioContext;
    private String isMessage = "";
    private TextView ioMessage = null;
    private boolean ibUsesNewLayout = false;

    public NexProgressDialog(Context context) {
        super(context);
        this.ioContext = context;
    }

    @Override
    protected void onCreate(Bundle poSavedInstanceState) {
        super.onCreate(poSavedInstanceState);

        if (ibUsesNewLayout) {
            LayoutInflater loInflater = LayoutInflater.from(ioContext);
            View loView = loInflater.inflate(R.layout.progressfragment, null);
//			TextView loMessage = (TextView)loView.findViewById(R.id.progressfragment_message);
//			loMessage.setText(isMessage);
            ioMessage = loView.findViewById(R.id.progressfragment_message);
            ioMessage.setText(isMessage);

            ImageView loIcon = loView
                    .findViewById(R.id.progressfragment_icon);
            loIcon.setBackgroundResource(Utilitario.obtainResourceId(ioContext, R.attr.NexProgressDialogImageBackground));

            Drawable loDrawable = loIcon.getBackground();
            if (loDrawable instanceof AnimationDrawable) {
                AnimationDrawable animationDrawable = (AnimationDrawable) loIcon
                        .getBackground();
                animationDrawable.start();
            }

            setContentView(loView);
        }
    }

    @Override
    public void setMessage(CharSequence message) {
        super.setMessage(message);
        this.isMessage = message + "";
        if (ibUsesNewLayout && ioMessage != null) {
            ioMessage.setText(isMessage);
        }
    }

    public void setUsesNewLayout(boolean pbNewLayout) {
        this.ibUsesNewLayout = pbNewLayout;
    }

}
