package pe.com.nextel.android.util.support;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import greendroid.widget.ActionBarItem;
import pe.com.nextel.android.R;
import pe.com.nextel.android.util.Logger;

/**
 * {@link DialogFragment} que muestra &#237;tems de un men&#250; de opciones
 *
 * @author jroeder
 */
public class OptionsMenuFragment extends DialogFragment {

    /**
     * Variable global con el nombre adjunto por defecto del {@link OptionsMenuFragment}
     */
    public static final String TAG = "loOptionsFragment";
    private static List<Drawable> ioLstDrawables;
    private static List<String> ioLstTitulos;
    private static OptionsMenuFragmentListener ioListener;

    private boolean menuOpened = false;

    /**
     * Crea una instancia del {@link OptionsMenuFragment}
     *
     * @param poManager      Administrador de {@link Fragment}
     * @param psTag          Nombre adjunto al {@link OptionsMenuFragment}
     * @param poListener     Interfaz {@link OptionsMenuFragmentListener}
     * @param poLstTitulos   {@link List}&#60;{String}&#62; con lo t&#237;tulos o descripciones de los &#237;tems
     * @param poLstDrawables {@link List}&#60;{@link Drawable}&#62; con los &#237;conos de los &#237;tems
     * @return {@link OptionsMenuFragment}
     */
    public static OptionsMenuFragment newInstance(FragmentManager poManager,
                                                  String psTag, OptionsMenuFragmentListener poListener, List<String> poLstTitulos,
                                                  List<Drawable> poLstDrawables) {
        OptionsMenuFragment.subRemovePrevDialog(poManager, psTag);
        OptionsMenuFragment loFrag = new OptionsMenuFragment();
        ioLstTitulos = poLstTitulos;
        ioLstDrawables = poLstDrawables;
        ioListener = poListener;
        return loFrag;
    }

    private static Pair<List<String>, List<Drawable>> ObtainStringDrawable(List<ActionBarItem> items) {
        List<String> _titulos = new ArrayList<String>();
        List<Drawable> _icons = new ArrayList<Drawable>();
        for (ActionBarItem item : items) {
            _titulos.add(item.getContentDescription().toString());
            _icons.add(item.getDrawable());
        }
        return new Pair<List<String>, List<Drawable>>(_titulos, _icons);
    }

    /**
     * Crea una instancia del {@link OptionsMenuFragment}
     *
     * @param poManager  Administrador de {@link Fragment}
     * @param psTag      Nombre adjunto al {@link OptionsMenuFragment}
     * @param poListener Interfaz {@link OptionsMenuFragmentListener}
     * @param items      {@link List}&#60;{@link ActionBarItem}&#62; con los &#237;tems a mostrar
     * @return {@link OptionsMenuFragment}
     */
    public static OptionsMenuFragment newInstance(FragmentManager poManager,
                                                  String psTag, OptionsMenuFragmentListener poListener, List<ActionBarItem> items) {
        OptionsMenuFragment.subRemovePrevDialog(poManager, psTag);
        OptionsMenuFragment loFrag = new OptionsMenuFragment();
        Pair<List<String>, List<Drawable>> pair = ObtainStringDrawable(items);
        ioLstTitulos = pair.first;
        ioLstDrawables = pair.second;
        ioListener = poListener;
        return loFrag;
    }

    /**
     * Crea una instancia del {@link OptionsMenuFragment}
     *
     * @param poManager  Administrador de {@link Fragment}
     * @param psTag      Nombre adjunto al {@link OptionsMenuFragment}
     * @param poListener Interfaz {@link OptionsMenuFragmentListener}
     * @return {@link OptionsMenuFragment}
     */
    public static OptionsMenuFragment newInstance(FragmentManager poManager,
                                                  String psTag, OptionsMenuFragmentListener poListener) {
        OptionsMenuFragment.subRemovePrevDialog(poManager, psTag);
        OptionsMenuFragment loFrag = new OptionsMenuFragment();
        ioLstTitulos = new ArrayList<String>();
        ioLstDrawables = new ArrayList<Drawable>();
        ioListener = poListener;
        return loFrag;
    }

    /**
     * Adjunta un nuevo t&#237;tulo o descripci&#243;n con su respectivo &#237cono
     *
     * @param psTitulo T&#237;tulo o descripci&#243;n del &#237;tem
     * @param poImagen &#205;cono del &#237;tem
     */
    public void addItem(String psTitulo, Drawable poImagen) {
        ioLstTitulos.add(psTitulo);
        ioLstDrawables.add(poImagen);
    }

    @Override
    public void onAttach(Activity activity) {
        ioListener = (OptionsMenuFragmentListener) activity;
        super.onAttach(activity);
    }

    @Override
    public void onResume() {
        getDialog().getWindow().setLayout(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        getDialog().getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.TRANSPARENT));
        setStyle(OptionsMenuFragment.STYLE_NO_FRAME, android.R.style.Theme);
        super.onResume();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog loDialog = super.onCreateDialog(savedInstanceState);
        return loDialog;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        setMenuOpened(false);
        super.onCancel(dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setGravity(Gravity.TOP | Gravity.RIGHT);
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        View v = inflater.inflate(R.layout.contextfragment, container, true);
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_MENU) {
                    dismiss();
                }
                return false;
            }


        });
        ListView loLstOpciones = v
                .findViewById(R.id.contextfragment_listview);
        MarginLayoutParams loParams = (MarginLayoutParams) loLstOpciones.getLayoutParams();
        loParams.topMargin = (int) (46 * Resources.getSystem().getDisplayMetrics().density);

        subCrearLista(loLstOpciones);
        return v;
    }

    private void subCrearLista(ListView poView) {
        try {
            if (ioLstTitulos != null) {
                OptionsFragmentItemAdapter loMotivoRechazoAdapter = new OptionsFragmentItemAdapter(getActivity());
                poView.setSelected(false);
                poView.setAdapter(loMotivoRechazoAdapter);
                poView.invalidate();
                poView.setCacheColorHint(Color.TRANSPARENT);
                poView.setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        ioListener.OnOptionsMenuFragmentItemClick(position);
                        dismiss();
                    }
                });
            } else {
                Logger.w("Lista contextual nula");
            }
        } catch (Exception e) {
            Logger.e(e);
        }
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            setMenuOpened(true);
            super.show(manager, tag);
        } catch (IllegalStateException e) {
            Log.v("PRO", "Exception: " + e.getMessage());
        }
    }

    @Override
    public void dismiss() {
        setMenuOpened(false);
        super.dismiss();
    }

    private static void subRemovePrevDialog(FragmentManager poManager,
                                            String psTag) {
        FragmentTransaction loTransaction = poManager.beginTransaction();
        Fragment loPrevFragment = poManager.findFragmentByTag(psTag);
        if (loPrevFragment != null) {
            loTransaction.remove(loPrevFragment);
        }
        loTransaction.addToBackStack(null);
    }

    /**
     * Verifica si el men&#250; est&#225; abierto o mostr&#225;ndose
     *
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> si el men&#250; est&#225; abierto o mostr&#225;ndose</li>
     * <li><b>false</b> el men&#250; est&#225; cerrado</li>
     * </ul>
     */
    public boolean isMenuOpened() {
        return menuOpened;
    }

    /**
     * Instancia cuando el men&#250; est&#225; abierto o mostr&#225;ndose
     *
     * @param menuOpened Estado del men&#250;
     */
    protected void setMenuOpened(boolean menuOpened) {
        this.menuOpened = menuOpened;
        Log.i("PRO", "OptionsMenuFragment.menuOpened: " + menuOpened);
    }

    private class OptionsFragmentItemAdapter extends BaseAdapter {

        private Context ioContext;

        public OptionsFragmentItemAdapter(Context poContext) {
            Logger.i("OptionsFragmentItemAdapter");
            this.ioContext = poContext;
        }

        @Override
        public int getCount() {
            int loCant = 0;
            if (ioLstTitulos != null) {
                loCant = ioLstTitulos.size();
            }
            return loCant;
        }

        @Override
        public Object getItem(int piPosition) {
            return ioLstTitulos.get(piPosition);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int piPosition, View poConvertView,
                            ViewGroup poParent) {
            LayoutInflater loInflater = (LayoutInflater) ioContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (poConvertView == null) {
                poConvertView = loInflater.inflate(
                        R.layout.contextfragment_item, null);
            }

            TextView txtMenu = poConvertView
                    .findViewById(R.id.contextfragment_texto);
            ImageView imgMenu = poConvertView
                    .findViewById(R.id.contextfragment_imagen);
            txtMenu.setText(ioLstTitulos.get(piPosition));
            if (ioLstDrawables != null) {
                if (ioLstDrawables.get(piPosition) != null) {
                    imgMenu.setImageDrawable(ioLstDrawables.get(piPosition));
                } else {
                    imgMenu.setImageDrawable(getActivity().getResources()
                            .getDrawable(R.drawable.bullet_orange));
                }
            }

            poConvertView.setTag(piPosition);
            return poConvertView;
        }
    }

    /**
     * Implementa la acci&#243;n a realizar un &#237;tem del men&#250; de opciones
     *
     * @author jroeder
     */
    public interface OptionsMenuFragmentListener {
        /**
         * Es invocado cuando se realiza clic sobre un &#237;tem del men&#250; de opciones
         *
         * @param position Posici&#243;n del &#237;tem del men&#250; de opciones
         */
        public void OnOptionsMenuFragmentItemClick(int position);
    }
}