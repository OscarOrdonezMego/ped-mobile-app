package pe.com.nextel.android.widget;

import android.content.Context;
import android.preference.PreferenceActivity;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Clase que hereda de Toast. Muestra un mensaje emergente del tipo Toast con un dise&#241;o especial por el tipo de mensaje.</br>
 * En caso que el contexto fuese un PreferenceActivity, se muestra un {@link Toast} simple.
 *
 * @author jroeder
 */
public class NexToast extends Toast {
    /**
     * Enumerable de tipo de mensaje a mostrar
     *
     * @author jroeder
     */
    public static enum EnumType {
        /**
         * Tipo de mensaje de aceptaci&#243;n
         */
        ACCEPT,
        /**
         * Tipo de mensaje de cancelaci&#243;n
         */
        CANCEL,
        /**
         * Tipo de mensaje de informaci&#243;n
         */
        INFORMATION,
        /**
         * Tipo de mensaje de error
         */
        ERROR,
        /**
         * Tipo de mensaje por defecto
         */
        DEFAULT
    }

    private Context context;
    private String text;
    private Integer resourceImage;
    private int length = Toast.LENGTH_LONG;
    private EnumType type = EnumType.DEFAULT;

    /**
     * Constructor de la clase.</br>
     * El tipo de mensaje es el por defecto.</br>
     * El {@link Gravity} del {@link Toast} es el CENTER|VERTICAL con valores 0 en xOffset y yOffset.
     *
     * @param context       - Contexto de la aplicaci&#243;n
     * @param resourceImage - Id de recurso de la imagen
     * @param text          - Texto del mensaje
     * @param length        - Tiempo de duraci&#243;n del {@link Toast}
     */
    public NexToast(Context context, int resourceImage, String text, int length) {
        super(context);
        this.context = context;
        this.resourceImage = resourceImage;
        this.text = text;
        this.length = length;
        this.type = EnumType.DEFAULT;
        onCreateToast();
    }

    /**
     * Constructor de la clase
     * El tipo de mensaje es el por defecto.</br>
     * El {@link Gravity} del {@link Toast} es el CENTER|VERTICAL con valores 0 en xOffset y yOffset.
     *
     * @param context       - Contexto de la aplicaci&#243;n
     * @param resourceImage - Id de recurso de la imagen
     * @param resourceText  - Id de recurso del texto del mensaje
     * @param length        - Tiempo de duraci&#243;n del {@link Toast}
     */
    public NexToast(Context context, int resourceImage, int resourceText, int length) {
        super(context);
        this.context = context;
        this.resourceImage = resourceImage;
        this.text = context.getString(resourceText);
        this.length = length;
        this.type = EnumType.DEFAULT;
        onCreateToast();
    }

    /**
     * Constructor de la clase.</br>
     * El tipo de mensaje es el por defecto.</br>
     * El {@link Gravity} del {@link Toast} es el CENTER|VERTICAL con valores 0 en xOffset y yOffset.
     *
     * @param context - Contexto de la aplicaci&#243;n
     * @param text    - Texto del mensaje
     * @param length  - Tiempo de duraci&#243;n del {@link Toast}
     */
    public NexToast(Context context, String text, int length) {
        super(context);
        this.context = context;
        this.resourceImage = null;
        this.text = text;
        this.length = length;
        this.type = EnumType.DEFAULT;
        onCreateToast();
    }

    /**
     * Constructor de la clase.</br>
     * El tipo de mensaje es el por defecto.</br>
     * El {@link Gravity} del {@link Toast} es el CENTER|VERTICAL con valores 0 en xOffset y yOffset.
     *
     * @param context      - Contexto de la aplicaci&#243;n
     * @param resourceText - Id de recurso del texto del mensaje
     * @param length       - Tiempo de duraci&#243;n del {@link Toast}
     */
    public NexToast(Context context, int resourceText, int length) {
        super(context);
        this.context = context;
        this.resourceImage = null;
        this.text = context.getString(resourceText);
        this.length = length;
        this.type = EnumType.DEFAULT;
        onCreateToast();
    }

    /**
     * Constructor de la clase.</br>
     * El tipo de mensaje es el por defecto.</br>
     * El tiempo de duraci&#243;n del {@link Toast} es el LENGTH_LONG.</br>
     * El {@link Gravity} del {@link Toast} es el CENTER|VERTICAL con valores 0 en xOffset y yOffset.
     *
     * @param context       - Contexto de la aplicaci&#243;n
     * @param resourceImage - Id de recurso de la imagen
     * @param text          - Texto del mensaje
     */
    public NexToast(Context context, int resourceImage, String text) {
        super(context);
        this.context = context;
        this.resourceImage = resourceImage;
        this.text = text;
        this.length = Toast.LENGTH_LONG;
        this.type = EnumType.DEFAULT;
        onCreateToast();
    }

    /**
     * Constructor de la clase.</br>
     * El tipo de mensaje es el por defecto.</br>
     * El tiempo de duraci&#243;n del {@link Toast} es el LENGTH_LONG.</br>
     * El {@link Gravity} del {@link Toast} es el CENTER|VERTICAL con valores 0 en xOffset y yOffset.</br>
     * No se muestra imagen.
     *
     * @param context - Contexto de la aplicaci&#243;n
     * @param text    - Texto del mensaje
     */
    public NexToast(Context context, String text) {
        super(context);
        this.context = context;
        this.resourceImage = null;
        this.text = text;
        this.length = Toast.LENGTH_LONG;
        this.type = EnumType.DEFAULT;
        onCreateToast();
    }

    /**
     * Constructor de la clase.</br>
     * El tipo de mensaje es el por defecto.</br>
     * El tiempo de duraci&#243;n del {@link Toast} es el LENGTH_LONG.</br>
     * El {@link Gravity} del {@link Toast} es el CENTER|VERTICAL con valores 0 en xOffset y yOffset.</br>
     * No se muestra imagen.
     *
     * @param context      - Contexto de la aplicaci&#243;n
     * @param resourceText - Id de recurso del texto del mensaje
     */
    public NexToast(Context context, int resourceText) {
        super(context);
        this.context = context;
        this.resourceImage = null;
        this.text = context.getString(resourceText);
        this.length = Toast.LENGTH_LONG;
        this.type = EnumType.DEFAULT;
        onCreateToast();
    }

    /**
     * Constructor de la clase.</br>
     * Si el tipo de mensaje es el por defecto, no se muestra imagen.</br>
     * El tiempo de duraci&#243;n del {@link Toast} es el LENGTH_LONG.</br>
     * El {@link Gravity} del {@link Toast} es el CENTER|VERTICAL con valores 0 en xOffset y yOffset.
     *
     * @param context      - Contexto de la aplicaci&#243;n
     * @param resourceText - Id de recurso del texto del mensaje
     * @param type         - Tipo de mensaje
     */
    public NexToast(Context context, int resourceText, EnumType type) {
        super(context);
        this.context = context;
        this.resourceImage = null;
        this.text = context.getString(resourceText);
        this.length = Toast.LENGTH_LONG;
        this.type = type;
        onCreateToast();
    }

    /**
     * Constructor de la clase.</br>
     * Si el tipo de mensaje es el por defecto, no se muestra imagen.</br>
     * El tiempo de duraci&#243;n del {@link Toast} es el LENGTH_LONG.</br>
     * El {@link Gravity} del {@link Toast} es el CENTER|VERTICAL con valores 0 en xOffset y yOffset.
     *
     * @param context - Contexto de la aplicaci&#243;n
     * @param text    - Texto del mensaje
     * @param type    - Tipo de mensaje
     */
    public NexToast(Context context, String text, EnumType type) {
        super(context);
        this.context = context;
        this.resourceImage = null;
        this.text = text;
        this.length = Toast.LENGTH_LONG;
        this.type = type;
        onCreateToast();
    }

    /**
     * Constructor de la clase.</br>
     * Si el tipo de mensaje es el por defecto, no se muestra imagen.
     *
     * @param context      - Contexto de la aplicaci&#243;n
     * @param resourceText - Id de recurso del texto del mensaje
     * @param type         - Tipo de mensaje
     * @param length       - Tiempo de duraci&#243;n del {@link Toast}
     */
    public NexToast(Context context, int resourceText, EnumType type, int length) {
        super(context);
        this.context = context;
        this.resourceImage = null;
        this.text = context.getString(resourceText);
        this.length = length;
        this.type = type;
        onCreateToast();
    }

    /**
     * Constructor de la clase.</br>
     * Si el tipo de mensaje es el por defecto, no se muestra imagen.
     *
     * @param context - Contexto de la aplicaci&#243;n
     * @param text    - Texto del mensaje
     * @param type    - Tipo de mensaje
     * @param length  - Tiempo de duraci&#243;n del {@link Toast}
     */
    public NexToast(Context context, String text, EnumType type, int length) {
        super(context);
        this.context = context;
        this.resourceImage = null;
        this.text = text;
        this.length = length;
        this.type = type;
        onCreateToast();
    }

    /**
     * Constructor de la clase.</br>
     * Si el tipo de mensaje es el por defecto, no se muestra imagen.
     *
     * @param context      - Contexto de la aplicaci&#243;n
     * @param resourceText - Id de recurso del texto del mensaje
     * @param type         - Tipo de mensaje
     * @param length       - Tiempo de duraci&#243;n del {@link Toast}
     * @param gravity      - {@link Gravity} del {@link Toast}
     */
    public NexToast(Context context, int resourceText, EnumType type, int length, int gravity) {
        super(context);
        this.context = context;
        this.resourceImage = null;
        this.text = context.getString(resourceText);
        this.length = length;
        this.type = type;
        onCreateToast(gravity);
    }

    /**
     * Constructor de la clase.</br>
     * Si el tipo de mensaje es el por defecto, no se muestra imagen.
     *
     * @param context - Contexto de la aplicaci&#243;n
     * @param text    - Texto del mensaje
     * @param type    - Tipo de mensaje
     * @param length  - Tiempo de duraci&#243;n del {@link Toast}
     * @param gravity - {@link Gravity} del {@link Toast}
     */
    public NexToast(Context context, String text, EnumType type, int length, int gravity) {
        super(context);
        this.context = context;
        this.resourceImage = null;
        this.text = text;
        this.length = length;
        this.type = type;
        onCreateToast(gravity);
    }

    private void onCreateToast() {
//		NexToastView loNexToastView = new NexToastView(context);
        NexToastView loNexToastView = new NexToastView(context);
        loNexToastView.setImage(resourceImage);
        loNexToastView.setText(text);
        loNexToastView.setType(type);
//		if(context instanceof PreferenceActivity){
//			setText(text);
//		}else{
//			setView(loNexToastView);
//		}
        setView(loNexToastView);
        setGravity(Gravity.CENTER | Gravity.CENTER_VERTICAL, 0, 0);
        setDuration(length);
    }

    private void onCreateToast(int gravity) {
//		NexToastView loNexToastView = new NexToastView(context);
        NexToastView loNexToastView = new NexToastView(context);
        loNexToastView.setImage(resourceImage);
        loNexToastView.setText(text);
        loNexToastView.setType(type);
//		if(context instanceof PreferenceActivity){
//			setText(text);
//		}else{
//			setView(loNexToastView);
//		}
        setView(loNexToastView);
        setGravity(gravity, 0, 0);
        setDuration(length);
    }

    private int gravity, xOffset, yOffset;

    @Override
    public void setGravity(int gravity, int xOffset, int yOffset) {
        super.setGravity(gravity, xOffset, yOffset);
        this.gravity = gravity;
        this.xOffset = xOffset;
        this.yOffset = yOffset;
    }

    @Override
    public void show() {
        if (context instanceof PreferenceActivity) {
            Toast loToast = makeText(context, text, length);
            loToast.setGravity(this.gravity, this.xOffset, this.yOffset);
            loToast.show();
        } else {
            super.show();
        }
    }
}
