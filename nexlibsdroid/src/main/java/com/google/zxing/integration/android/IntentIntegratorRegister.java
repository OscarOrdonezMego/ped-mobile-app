package com.google.zxing.integration.android;

/**
 * Interfaz que implementa el registro de un {@link IntentResultCallback} cuando se invoca a la aplicaci&#243;n
 * del ZXING para escanear c&#243;digos de barra, QR, etc.
 *
 * @author jroeder
 */
public interface IntentIntegratorRegister {
    /**
     * Registra un {@link IntentResultCallback}
     *
     * @param callback {@link IntentResultCallback} con el valor resultado de devuelto por la aplicaci&#243;n ZXING
     * @return <b>boolean</b>
     * <ul>
     * <li><b>true</b> Si el registro fue exitoso</li>
     * <li><b>false</b> de lo contrario</li>
     * </ul>
     */
    public boolean registerForScan(IntentResultCallback callback);
}
