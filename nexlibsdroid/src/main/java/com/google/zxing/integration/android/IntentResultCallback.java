package com.google.zxing.integration.android;

/**
 * Interfaz que instancia el {@link IntentResult} de la aplicaci&#243;n ZXING en la clase que implementa esta interfaz
 *
 * @author jroeder
 */
public interface IntentResultCallback {
    /**
     * Instancia el {@link IntentResult}
     *
     * @param result {@link IntentResult} de la aplicaci&#243;n ZXING
     */
    public void setScanResult(IntentResult result);
}
