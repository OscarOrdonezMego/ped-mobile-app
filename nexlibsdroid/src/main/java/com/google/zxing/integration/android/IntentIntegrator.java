/*
 * Copyright 2009 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.zxing.integration.android;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;

import pe.com.nextel.android.R;
import pe.com.nextel.android.actividad.NexActivity;
import pe.com.nextel.android.actividad.NexListActivityBasic;
import pe.com.nextel.android.actividad.NexListActivityTweaked;
import pe.com.nextel.android.actividad.support.NexFragmentActivity;
import pe.com.nextel.android.actividad.support.NexFragmentListActivityBasic;
import pe.com.nextel.android.actividad.support.NexFragmentListActivityTweaked;
import pe.com.nextel.android.util.AlertDialogYesNo;
import pe.com.nextel.android.util.DialogFragmentYesNo;
import pe.com.nextel.android.util.support.DialogFragmentYesNo.EnumLayoutResource;
import pe.com.nextel.android.widget.NexToast;
import pe.com.nextel.android.widget.NexToast.EnumType;

/**
 * <p>A utility class which helps ease integration with Barcode Scanner via {@link Intent}s. This is a simple
 * way to invoke barcode scanning and receive the result, without any need to integrate, modify, or learn the
 * project's source code.</p>
 * <p>
 * <h2>Initiating a barcode scan</h2>
 * <p>
 * <p>Integration is essentially as easy as calling initiateScan(Activity) and waiting
 * for the result in your app.</p>
 * <p>
 * <p>It does require that the Barcode Scanner application is installed. The
 * {initiateScan(Activity) method will prompt the user to download the application, if needed.</p>
 * <p>
 * <p>There are a few steps to using this integration. First, your {@link Activity} must implement
 * the method {Activity#onActivityResult(int, int, Intent)} and include a line of code like this:</p>
 * <p>
 * <p>{@code
 * public void onActivityResult(int requestCode, int resultCode, Intent intent) {
 * IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
 * if (scanResult != null) {
 * // handle scan result
 * }
 * // else continue with any other code you need in the method
 * ...
 * }
 * }</p>
 * <p>
 * <p>This is where you will handle a scan result.
 * Second, just call this in response to a user action somewhere to begin the scan process:</p>
 * <p>
 * <p>{@code IntentIntegrator.initiateScan(yourActivity);}</p>
 * <p>
 * <p>You can use initiateScan(Activity, String, String, String, String) or
 * initiateScan(Activity, int, int, int, int) to customize the download prompt with
 * different text labels.</p>
 * <p>
 * <h2>Sharing text via barcode</h2>
 * <p>
 * <p>To share text, encoded as a QR Code on-screen, similarly, see {shareText(Activity, String)}.</p>
 * <p>
 * <p>Some code, particularly download integration, was contributed from the Anobiit application.</p>
 *
 * @author Sean Owen
 * @author Fred Lin
 * @author Isaac Potoczny-Jones
 * @author Brad Drehmer
 */
public final class IntentIntegrator {

    public static final int REQUEST_CODE = 0x0ba7c0de; // get it?

    public static final String DEFAULT_TITLE = "Install Barcode Scanner?";
    public static final String DEFAULT_MESSAGE =
            "This application requires Barcode Scanner. Would you like to install it?";
    public static final String DEFAULT_YES = "Yes";
    public static final String DEFAULT_NO = "No";

    // supported barcode formats
    public static final String PRODUCT_CODE_TYPES = "UPC_A,UPC_E,EAN_8,EAN_13";
    public static final String ONE_D_CODE_TYPES = PRODUCT_CODE_TYPES + ",CODE_39,CODE_128";
    public static final String QR_CODE_TYPES = "QR_CODE";
    public static final String ALL_CODE_TYPES = null;

    private IntentIntegrator() {
    }

    /**
     * See initiateScan(activity, callback) --
     * same, but uses default English labels.
     */
    public static void initiateScan(Activity activity,
                                    IntentResultCallback callback) {
//    initiateScan(activity, DEFAULT_TITLE, DEFAULT_MESSAGE, DEFAULT_YES, DEFAULT_NO, callback);
        initiateScan(activity, activity.getString(R.string.IntentIntegrator_DefaultTittle), activity.getString(R.string.IntentIntegrator_DefaultMessage), DEFAULT_YES, DEFAULT_NO, callback);
    }

    /**
     * See initiateScan(Activity, String, String, String, String, String) --
     * same, but scans for all supported barcode types.
     *
     * @param stringTitle     title of dialog prompting user to download Barcode Scanner
     * @param stringMessage   text of dialog prompting user to download Barcode Scanner
     * @param stringButtonYes text of button user clicks when agreeing to download
     *                        Barcode Scanner (e.g. "Yes")
     * @param stringButtonNo  text of button user clicks when declining to download
     *                        Barcode Scanner (e.g. "No")
     */
    public static void initiateScan(Activity activity,
                                    String stringTitle,
                                    String stringMessage,
                                    String stringButtonYes,
                                    String stringButtonNo,
                                    IntentResultCallback callback) {

        initiateScan(activity,
                stringTitle,
                stringMessage,
                stringButtonYes,
                stringButtonNo,
                ALL_CODE_TYPES,
                callback);
    }

    /**
     * Invokes scanning.
     *
     * @param stringTitle                 title of dialog prompting user to download Barcode Scanner
     * @param stringMessage               text of dialog prompting user to download Barcode Scanner
     * @param stringButtonYes             text of button user clicks when agreeing to download
     *                                    Barcode Scanner (e.g. "Yes")
     * @param stringButtonNo              text of button user clicks when declining to download
     *                                    Barcode Scanner (e.g. "No")
     * @param stringDesiredBarcodeFormats a comma separated list of codes you would
     *                                    like to scan for.
     * @return the contents of the barcode that was scanned, or null if none was found
     */
    public static void initiateScan(Activity activity,
                                    String stringTitle,
                                    String stringMessage,
                                    String stringButtonYes,
                                    String stringButtonNo,
                                    String stringDesiredBarcodeFormats,
                                    IntentResultCallback callback) {

        if (!(activity instanceof IntentIntegratorRegister)) {
            new NexToast(activity, activity.getString(R.string.IntentIntegrator_FailedRegister) + IntentIntegratorRegister.class.getSimpleName(), EnumType.ERROR).show();
            return;
        }

        if (((IntentIntegratorRegister) activity).registerForScan(callback)) {
            Intent intentScan = new Intent();
            intentScan.setComponent(new ComponentName("com.google.zxing.client.android", "com.google.zxing.client.android.CaptureActivity"));
            intentScan.setAction("com.google.zxing.client.android.SCAN");
            intentScan.addCategory(Intent.CATEGORY_DEFAULT);

            // check which types of codes to scan for
            if (stringDesiredBarcodeFormats != null) {
                // set the desired barcode types
                intentScan.putExtra("SCAN_FORMATS", stringDesiredBarcodeFormats);
            }

            try {
                activity.startActivityForResult(intentScan, REQUEST_CODE);
            } catch (ActivityNotFoundException e) {
                showDownloadDialog(activity, stringTitle, stringMessage, stringButtonYes, stringButtonNo);
            }

        }
    }

    private static void startGPlayWithZXing(final Activity activity) {
        Uri uri = Uri.parse("market://search?q=pname:com.google.zxing.client.android");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        activity.startActivity(intent);
    }

    private static void showDownloadDialog(final Activity activity,
                                           String stringTitle,
                                           String stringMessage,
                                           String stringButtonYes,
                                           String stringButtonNo) {

        if (activity instanceof NexActivity
                && ((NexActivity) activity).isUsingDialogFragment()) {
            DialogFragmentYesNo _dialog = DialogFragmentYesNo.newInstance(activity.getFragmentManager(),
                    DialogFragmentYesNo.TAG + "IntentIntegrator",
                    stringTitle,
                    stringMessage,
                    EnumLayoutResource.getDefaultIconHelp(activity));
            if (_dialog.showDialog(activity.getFragmentManager(), DialogFragmentYesNo.TAG + "IntentIntegrator") == DialogFragmentYesNo.YES) {
                startGPlayWithZXing(activity);
            }
        } else if (activity instanceof NexListActivityBasic
                && ((NexListActivityBasic) activity).isUsingDialogFragment()) {
            DialogFragmentYesNo _dialog = DialogFragmentYesNo.newInstance(activity.getFragmentManager(),
                    DialogFragmentYesNo.TAG + "IntentIntegrator",
                    stringTitle,
                    stringMessage,
                    EnumLayoutResource.getDefaultIconHelp(activity));
            if (_dialog.showDialog(activity.getFragmentManager(), DialogFragmentYesNo.TAG + "IntentIntegrator") == DialogFragmentYesNo.YES) {
                startGPlayWithZXing(activity);
            }
        } else if (activity instanceof NexListActivityTweaked
                && ((NexListActivityBasic) activity).isUsingDialogFragment()) {
            DialogFragmentYesNo _dialog = DialogFragmentYesNo.newInstance(activity.getFragmentManager(),
                    DialogFragmentYesNo.TAG + "IntentIntegrator",
                    stringTitle,
                    stringMessage,
                    EnumLayoutResource.getDefaultIconHelp(activity));
            if (_dialog.showDialog(activity.getFragmentManager(), DialogFragmentYesNo.TAG + "IntentIntegrator") == DialogFragmentYesNo.YES) {
                startGPlayWithZXing(activity);
            }
        } else if (activity instanceof NexFragmentActivity
                && ((NexFragmentActivity) activity).isUsingDialogFragment()) {
            pe.com.nextel.android.util.support.DialogFragmentYesNo _dialog = pe.com.nextel.android.util.support
                    .DialogFragmentYesNo.newInstance(((NexFragmentActivity) activity).getSupportFragmentManager(),
                            pe.com.nextel.android.util.support
                                    .DialogFragmentYesNo.TAG + "IntentIntegrator",
                            stringTitle,
                            stringMessage,
                            pe.com.nextel.android.util.support.DialogFragmentYesNo
                                    .EnumLayoutResource.getDefaultIconHelp(activity));
            if (_dialog.showDialog(((NexFragmentActivity) activity).getSupportFragmentManager(), DialogFragmentYesNo.TAG + "IntentIntegrator") == DialogFragmentYesNo.YES) {
                startGPlayWithZXing(activity);
            }
        } else if (activity instanceof NexFragmentListActivityBasic
                && ((NexFragmentListActivityBasic) activity).isUsingDialogFragment()) {
            pe.com.nextel.android.util.support.DialogFragmentYesNo _dialog = pe.com.nextel.android.util.support
                    .DialogFragmentYesNo.newInstance(((NexFragmentListActivityBasic) activity).getSupportFragmentManager(),
                            pe.com.nextel.android.util.support
                                    .DialogFragmentYesNo.TAG + "IntentIntegrator",
                            stringTitle,
                            stringMessage,
                            pe.com.nextel.android.util.support.DialogFragmentYesNo
                                    .EnumLayoutResource.getDefaultIconHelp(activity));
            if (_dialog.showDialog(((NexFragmentListActivityBasic) activity).getSupportFragmentManager(), DialogFragmentYesNo.TAG + "IntentIntegrator") == DialogFragmentYesNo.YES) {
                startGPlayWithZXing(activity);
            }
        } else if (activity instanceof NexFragmentListActivityTweaked
                && ((NexFragmentListActivityTweaked) activity).isUsingDialogFragment()) {
            pe.com.nextel.android.util.support.DialogFragmentYesNo _dialog = pe.com.nextel.android.util.support
                    .DialogFragmentYesNo.newInstance(((NexFragmentListActivityTweaked) activity).getSupportFragmentManager(),
                            pe.com.nextel.android.util.support
                                    .DialogFragmentYesNo.TAG + "IntentIntegrator",
                            stringTitle,
                            stringMessage,
                            pe.com.nextel.android.util.support.DialogFragmentYesNo
                                    .EnumLayoutResource.getDefaultIconHelp(activity));
            if (_dialog.showDialog(((NexFragmentListActivityTweaked) activity).getSupportFragmentManager(), DialogFragmentYesNo.TAG + "IntentIntegrator") == DialogFragmentYesNo.YES) {
                startGPlayWithZXing(activity);
            }
        } else {
            AlertDialogYesNo _alert = new AlertDialogYesNo(activity, stringTitle, stringMessage);
            if (_alert.showDialog() == AlertDialogYesNo.YES) {
                startGPlayWithZXing(activity);
            }
        }
    }


    /**
     * <p>Call this from your {@link Activity}'s
     * {Activity#onActivityResult(int, int, Intent)} method.</p>
     *
     * @return null if the event handled here was not related to {@link IntentIntegrator}, or
     * else an {@link IntentResult} containing the result of the scan. If the user cancelled scanning,
     * the fields will be null.
     */
    public static IntentResult parseActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                String contents = intent.getStringExtra("SCAN_RESULT");
                String formatName = intent.getStringExtra("SCAN_RESULT_FORMAT");
                return new IntentResult(contents, formatName);
            } else {
                new NexToast(activity, R.string.IntentIntegrator_FailedRetrieve, EnumType.CANCEL).show();
                return new IntentResult(null, null);
            }
        }
        return null;
    }

}